#pragma once

#include <functional>
#include <optional>
#include <string>
#include <string_view>
#include <system_error>
#include <utility>
#include <iostream>

namespace utils
{
    template<typename E>
    class Failure
    {
      public:
        explicit Failure(const E& value)
            : m_value(value)
        {
        }

        explicit Failure(E&& value)
            : m_value(std::move(value))
        {
        }

        template<typename... Args>
        explicit Failure(Args&&... args)
            : m_value(E{args...})
        {
        }

        constexpr const E& value() const&
        {
            return m_value;
        }

        constexpr E& value() &
        {
            return m_value;
        }

        constexpr E&& value() &&
        {
            return std::move(m_value);
        }

      private:
        E m_value;
    };

    template<typename E>
    Failure(E) -> Failure<E>;

    template<typename T, typename E>
    class Result;

    template<typename T, typename E>
    class Result
    {
      public:
        using value_type = T;

        Result()
            : m_value(T{}),
              m_error(std::nullopt)
        {
        }

        //! Construct Result object
        //! \param val
        Result(const T& val)
            : m_value(val)
        {
        }

        //! Construct Result object
        //! \param val
        Result(T&& val)
            : m_value(std::move(val))
        {
        }

        //! Construct Result from Failure object
        //! \param m_error
        template<typename G>
        explicit Result(const Failure<G>& fail)
            : m_value{std::nullopt},
              m_error{fail.value()}
        {
        }

        //! Construct Result from Failure object
        //! \param m_error
        template<typename G>
        explicit(!std::is_convertible_v<G, E>) Result(Failure<G>&& fail)
            : m_value{std::nullopt},
              m_error{std::move(fail.value())}
        {
        }

        //! Construct Result from empty Result - for passing errors
        //! \param empty_result
        explicit Result(Result<void, E> empty_result);

        friend constexpr bool operator==(const Result<T, E>& result, const T& value)
        {
            if (result.hasError())
            {
                return false;
            }

            return *result.m_value == value;
        }

        friend constexpr bool operator==(const Result<T, E>& result, const Failure<E>& error)
        {
            if (result.hasValue())
            {
                return false;
            }

            return *result.m_error == error.value();
        }

        friend constexpr bool operator==(const Result<T, E>& result1, const Result<T, E>& result2)
        {
            if (result2.hasValue())
            {
                return result1 == result2.value();
            }
            else
            {
                return result1 == utils::Failure{result2.error()};
            }
        }

        //! Use to check if operation was successful - there is no m_error and a m_value.
        //! \return true if no m_error (== m_value present)
        constexpr bool hasValue() const noexcept
        {
            return not hasError();
        }

        //! Use to check if operation was unsuccessful - there is an m_error and no m_value.
        //! \return true if m_error (== no m_value present)
        constexpr bool hasError() const noexcept
        {
            return m_error.has_value();
        }

        //! Use to check if operation was successful - there is no m_error and a m_value.
        //! \return true if no m_error (== m_value present)
        constexpr explicit operator bool() const noexcept
        {
            return hasValue();
        }

        constexpr E& error()
        {
            return m_error.value();
        }

        constexpr const E& error() const
        {
            return m_error.value();
        }

        constexpr T& value()
        {
            return m_value.value();
        }

        constexpr const T& value() const
        {
            return m_value.value();
        }


        //! Call a function which takes m_value of this object as first argument. Get matching Result object.
        //! \param func function taking 1 argument, m_value of Result object
        //! \return Result object with type matching return type of func
        template<typename Func>
        auto map(Func&& func) &&
        {
            using ResultType = std::invoke_result_t<Func, T>;
            if (hasError())
            {
                return Result<ResultType, E>{Failure{std::move(*m_error)}};
            }
            return Result<ResultType, E>{std::invoke(func, std::move(*m_value))};
        }

        //! Call a function which takes m_value of this object as first argument. Get matching Result object.
        //! \param func function taking 1 argument, m_value of Result object
        //! \return Result object with type matching return type of func
        template<typename Func>
        auto map(Func&& func) const&
        {
            using ResultType = std::invoke_result_t<Func, T>;
            if (hasError())
            {
                return Result<ResultType, E>{Failure{*m_error}};
            }
            return Result<ResultType, E>{std::invoke(func, *m_value)};
        }

        T& operator*()
        {
            return *m_value;
        }

        const T& operator*() const
        {
            return *m_value;
        }

        T* operator->()
        {
            return m_value.operator->();
        }

        const T* operator->() const
        {
            return m_value.operator->();
        }

      private:
        std::optional<T> m_value;   //!< Value of the result
        std::optional<E> m_error;   //!< Error object
    };

    template<typename E>
    class Result<void, E>
    {
      public:
        //! Construct empty Result object (only Error) from any Result
        //! \tparam T m_value of Result
        //! \param result Result object from which the m_error will be taken
        template<typename T>
        explicit Result(const Result<T, E>& result)
            : m_error(result.m_error)
        {
        }

        //! Construct empty Result object (only Error) from any Result
        //! \tparam T m_value of Result
        //! \param result Result object from which the m_error will be taken
        template<typename T>
        explicit Result(Result<T, E>&& result)
            : m_error(std::move(result.m_error))
        {
        }

        //! Construct Result from Failure object
        //! \param m_error
        template<typename G>
        explicit(!std::is_convertible_v<G, E>) Result(Failure<G>&& fail)
            : m_error{std::move(fail.value())}
        {
        }

        //! Construct Result from Failure object
        //! \param m_error
        template<typename G>
        explicit(!std::is_convertible_v<G, E>) Result(const Failure<G>& fail)
            : m_error{fail.value()}
        {
        }

        //! Empty Result. Everything OK!
        Result() = default;

        friend constexpr bool operator==(const Result<void, E>& result, const Failure<E>& error)
        {
            if (not result.hasError())
            {
                return false;
            }

            return *result.m_error == error.value();
        }

        //! Use to check if operation was successful - there is no m_error.
        //! \return true if no m_error
        constexpr bool hasValue() const noexcept
        {
            return not hasError();
        }

        //! Use to check if operation was unsuccessful - there is an m_error and no m_value.
        //! \return true if m_error (== no m_value present)
        constexpr bool hasError() const noexcept
        {
            return m_error.has_value();
        }

        //! Use to check if operation was successful - there is no m_error.
        //! \return true if no m_error
        constexpr explicit operator bool() const noexcept
        {
            return hasValue();
        }

        const E& error() const
        {
            return m_error.value();
        }

        E& error()
        {
            return m_error.value();
        }

      private:
        std::optional<E> m_error;   //!< Error object
    };

    template<typename T, typename E>
    Result<T, E>::Result(Result<void, E> result)
        : Result(std::move(result.m_error))
    {
    }

    template<typename T, typename E>
    std::ostream& operator<<(std::ostream& stream, const Result<T, E>& value)
    {
        if(value.hasError())
        {
            stream << "Error: " << value.error();
        }
        else
        {
            stream << "Value: " << value.value();
        }

        return stream;
    }

    template<typename E>
    std::ostream& operator<<(std::ostream& stream, const Result<void, E>& value)
    {
        if(value.hasError())
        {
            stream << "Error: " << value.error();
        }
        else
        {
            stream << "Value: void";
        }

        return stream;
    }

}
