# C++ Utils Library 

A collection of generic methods and utilities that can be used by any C++ project. 


## Bits 

This module contains functions to perform bitwise operations on numbers, like setting, clearing or testing bits or generating
bit masks. Use of the functions should be straightforward.


## Circular buffer

This is a thread-safe FIFO queue with customizable maximum number of elements (hence circular). 

It provides classic buffer operations `push` and `pop` - in a blocking and non-blocking versions. The `pop` methods return
the element using `std::optional` - for non-blocking version, if the queue is empty, std::optional resolves to false.
For the blocking version, `std::optional` will only be false if the queue was disabled during the `popBlock` method.

Additionally, the buffer contains `unblockAndDisable` method that can be used to release blocking functions and 
disable the buffer. Disabled buffer cannot be re-enabled and this method is useful during destructors. If the buffer
is disabled, the `push` functions do nothing and `pop` function return a false `std::optional`.


## EnumStr

This is a utility that attempts to facilitate enum serialization and parsing. See the example:

```cpp
enum class Direction
{
    left,
    right,
    top,
    bottom
};

MAKE_ENUM_STR(DirectionStr, Direction,
    ENUM_STR(Direction, left),
    ENUM_STR(Direction, right),
    ENUM_STR(Direction, top),
    ENUM_STR(Direction, bottom),
);
```

The `MAKE_ENUM_STR` macro defines a new type `DirectionStr` that can be used to serialize and parse the enum. 
Additionally, a `<<` operator is defined for `std::ostream` causing the `Direction` enum values to be automatically 
serialized.  
By using `ENUM_STR`, names of the enum fields (left, right, ...) will be used to print and parse values.  
For example, the code `std::cout << Direction::bottom;` will print `Direction::bottom`.

The resulting type `DirectionStr` has to static methods `toString` and `fromString` that can be used to 
print or parse enum values.  
To parse it, you can write:
```cpp
auto value = *DirectionStr::fromString("left");
std::cout << value;
```

The code above will print `Direction::left`.
The `fromString` method returns an `std::optional` object, which will resolve to false if the conversion was not possible.

Additionally, instead of using a convenience macro `ENUM_STR`, you can explicitly provide string values used to 
serialize and parse the enum values:

```cpp 
enum class Direction
{
    left,
    right,
    top,
    bottom
};

MAKE_ENUM_STR(DirectionStr, Direction,
    { Direction::left,   "LEFT",   "l"},
    { Direction::right,  "RIGHT",  "r"},
    { Direction::top,    "TOP",    "t"},
    { Direction::bottom, "BOTTOM", "b"}
);

int main(int argc, char *argv[])
{
    auto value = *DirectionStr::fromString("r");
    std::cout << value;
}
```

The above code uses upper case values to print enum values and a single letter to parse them. It will print `RIGHT`.


## Files

Contains a few simple function to read a content of a file into a string or a vector of string. Or to read a file
in a binary mode and provide result in a form of byte vector. A corresponding function to write to a file are also 
available. 


## FlagSet

This utility facilitates a common use case of enums - a flag set. If an enum represents some collection of flags
(e.g. options to some function), the classical way to use it as flags is to assign each enum value a number that is a
power of 2 (1, 2, 4, 8...) and use bitwise operations to check which bits were set.
This is error-prone and not type safe. Consider using FlagSet:

```cpp
enum class ForEach
{
    parent_after,
    reverse,
    COUNT
};

using ForEachFlags = FlagSet<ForEach>;
```

The `ForEach` enum defines options for the `forEach` functions. If you want to use an enum with `FlagSet`, the last
element of the enum has to be `COUNT` - if you forget to define it, you will get a descriptive compilation error.

After the above code, you can use `ForEachFlags` type, for example, as a function parameter. To check if the flag set
contains a given flag you can write:

```cpp
if (flags.has(ForEach::reverse))
{ ... }
```

Beside `has` the FlagSet has other variants, as `hasAny` or `hasAll` that can be used to check presence of multiple flags
at the same time. Many more functions can be used (see `flagSet.h` to explore more). See an additional example:

```cpp
enum class Permissions
{
    read,
    write,
    execute,
    COUNT
};

using PermissionsFlags = FlagSet<Permissions>;

void performOperation(PermissionsFlags flags)
{
    if (flags.hasAll(Permissions::read, Permissions::execute)) { /* true */  }
    if (flags.hasAll(Permissions::read, Permissions::write))   { /* false */ }
    if (flags.hasAny(Permissions::read, Permissions::write))   { /* true */  }

    PermissionsFlags new_set = Permissions::read;   // Create a new set containing `read` flag
    new_set += Permissions::write;                  // Add 'write' flag
    new_set -= Permissions::read;                   // Remove 'read' flag

    flags += new_set;                               // Now it contains read, write and execute.

    flags.clear();
    if (flags.none())   { /* true */ }
}


int main(int argc, char *argv[])
{
    performOperation(Permissions::read | Permissions::execute);
}
```


# FSM - Finite State Machine

This utility helps to use the FSM idiom. Consider the example:

For the following state machine:
```
    //!                               |>>>>>>>>>>>>>>>>>>>>>>> xxToLastPacket >>>>>>>>>>>>>>>>>>>>>>>|
    //!                               |                                                              |
    //!     |> idleToFirstPacket >|   |   |> firstToNextPacket >|     |>>>> xxToLastPacket >>>>|     |
    //!     |                     |   |   |                     |     |                        |     |
    //!  ********              ****************             ***************                ***************
    //!  * IDLE *              * FIRST PACKET *             * NEXT PACKET *                * LAST PACKET *
    //!  ********              ****************             ***************                ***************
    //!     |                                                                                     |
    //!     |<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< lastPacketToIdle <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<|
    //!
```

You can use FSM utility to represent it in the code. First the header file:
```cpp
    class FgcEtherResponse
    {
        enum class State
        {
            idle,
            first_packet,
            next_packet,
            last_packet
        };

        using TransRes = utils::FsmTransitionResult<State>;

        public: (...)

        private:
            void idleState();                             // State function

            void firstPacketState();                      // State function

            void nextPacketState();                       // State function

            void lastPacketState();                       // State function

            TransRes idleToFirstPacket();                 // Transition function

            TransRes firstToNextPacket();                 // Transition function

            TransRes xxToLastPacket();                    // Transition function

            TransRes lastPacketToIdle();                  // Transition function

            // The finite state machine using FSM utility:
            utils::Fsm<State, FgcEtherResponse> m_fsm;
    };
```

In the constructor or the example class (`FgcEtherResponse`) you can define the mapping between states and functions:

```cpp
FgcEtherResponse::FgcEtherResponse()
{
    m_fsm.addState(State::idle,         &FgcEtherResponse::idleState,        { &FgcEtherResponse::idleToFirstPacket });
    m_fsm.addState(State::first_packet, &FgcEtherResponse::firstPacketState, { &FgcEtherResponse::xxToLastPacket, &FgcEtherResponse::firstToNextPacket });
    m_fsm.addState(State::next_packet,  &FgcEtherResponse::nextPacketState,  { &FgcEtherResponse::xxToLastPacket });
    m_fsm.addState(State::last_packet,  &FgcEtherResponse::lastPacketState,  { &FgcEtherResponse::lastPacketToIdle });
}
```

You use `addState` to add mapping. First argument is a state, then state function and then a list of transition functions
that are available for this state (see graph at the beginning).

In the state functions you can do whatever is needed to do for the given state. They are executed when the state machine
enters given state. After the state function, all transition functions are executed until a function returns an object
that says that a transition should occur. Consider an example transition function:

```cpp
FgcEtherResponse::TransRes FgcEtherResponse::idleToFirstPacket()
{
    if (m_fsm.getState() == FgcEtherState::response_pending)
    {
        return {State::first_packet, utils::FsmCascade};
    }

    return {};
}
```

If some condition is met, the transition function returns an object `{State::first_packet, utils::FsmCascade}` (it could also 
simply return just `State::first_packet`). The `utils::FsmCascade` (which resolves to `true`) means that the next state
(in that case `IDLE`) should be invoked right away, in the same iteration of the state machine. 

To execute state machine transition checking, you use `update` method. For example:

```cpp
void FgcEtherResponse::packetReceived()
{
    m_fsm.update();
}
```

## Text

Text utility contains some simple functions to process strings, like changing the case (lower/upper case) or trimming
the string out of whitespace characters.


## Timing

This utility contains profiling timers: a regular timer, scoped timer and chained timer. Reading the function description
in the `timing.h` file should be sufficient to understand how to use them. 

Besides, the module contains `formatDuration` function that can be used to format an `std::duration` into `std::string`.


## Object serialization 

To enable serialization for your class, take a look at the following example:

```cpp
struct Point
{
    SERIALIZABLE
    (
        Item("X", x),
        Item("Y", y)
    );

    int x;
    int y;
};

struct Example
{
    SERIALIZABLE
    (
        Item("points", points),
        Item("name",   name),
        Item("param",  param)
    )

    std::vector<Point> points = { {1, 2}, {5, 6} };
    std::string        name   = "Example";
    double             param  = 2.5;
};


int main(int argc, char *argv[])
{
    Example obj;

    obj.serialize<utils::StdSerializer>(std::cout);
}
```

This shows that serializer handles both containers and recursive serializing of custom types (like `Point`). Or even
containers of serializable types. The following program would print to the standard output:  
`{points = [{X = 1, Y = 2}, {X = 5, Y = 6}], name = "Example", param = 2.5}`

If desired, you can provide definition for the `<<` operator. The following code will have the same effect as the above:
```cpp
std::ostream& operator<<(std::ostream& stream, const Example& obj)
{
    return obj.serialize<utils::StdSerializer>(stream);
}

int main(int argc, char *argv[])
{
    Example obj;

    std::cout << obj;
}
```

To define a custom serializer, you need to create a class that defines a public method serialize(). See file `StreamSerilizer.h` or the below example:
```cpp
class JsonSerializer
{
    public:
        template<typename... Args>
        static json& serialize(json& data, Args&&... args)
        {
            ((data[args.name] = printJson(args.obj) ), ...);

            return data;
        }

    private:
        template<typename T>
        auto printJson(const T& object)
        {
            if constexpr(is_serializable_v<T>)
            {
                json data;
                object.serialize<JsonSerializer>(data);
                return data;
            }
            else
            {
                return object;
            }
        }
};
```

## Miscellaneous (Misc)

### Noncopyable and NoncopyableNonmovable

If you want to make your class non-copyable or non-copyable and non-movable you can inherit from these classes. 

### setTimeout

This function will block program execution, until a given predicate yields true or a given time-out is reached. 
The result of the function allows to check the reason. 

### Finally

This is a simple helper class that leverages RAII to execute a given function during destruction `Finally` object.  

```cpp
void someFunction()
{
    utils::Finally enable_watchdog([&]()
    {
        startWatchdog();
    });
    
    stopWatchdog();
    
    if (!doSomething1()) return;   // Some operation that can return from function
    
    if (!doSomething2()) return;   // Some another operation that can return from function
    
    if (!doSomething3()) return;   // Some yet another operation that can return from function
}
```

In the example above, independently of where the function exits (or why: `return` or an exception), the `startWatchdog`
will be executed at the end of the function. 

Note that the function passed to finally can be executed as a result of an exception being thrown and
therefore the function itself should not throw any exceptions. 