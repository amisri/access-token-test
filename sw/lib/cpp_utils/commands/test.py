from projectControl.unitTests import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("test", help="Runs unit tests.")
        UnitTests.add_arguments(cls.subparser)

    @classmethod
    def run(cls, args, cfg, project_args):
        # Configure and build test target
        UnitTests.build(cfg.source_dir, cfg.build_dir, args)

        # Run tests
        result = UnitTests.run(project_args['unit_tests'], args.filter, args.number, args.stop_on_failure,
                               args.leaks, args.args)

        if result.returncode == 0:
            logging.success(f"All unit tests passed")
        else:
            fatal("Unit tests failed")
    