from projectControl.utils import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("clean", help="Removes build directory.")

    @classmethod
    def run(cls, args, cfg, project_args):
        remove_dir(cfg.build_dir)
        logging.success(f"Removed {cfg.build_dir}")
    