/*!
 * @file  libblkbuf.h
 * @brief Converter Control Block Buffer library header file
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "cclibs.h"

typedef union blk_buf_t       blk_buf_t;
typedef struct blk_buf_mgr_t  blk_buf_mgr_t;
typedef struct blk_buf_iter_t blk_buf_iter_t;

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Creates a new block buffer manager structure, containing a list of free blocks.
 * Optionally, can receive an external memory array to be used for the blocks construction.
 * If this array is NULL, memory will be internally allocated based on the block size and number.
 *
 * @param[in]  ext_buffer   External memory array (can be NULL if memory is to be internally allocated)
 * @param[in]  buffer_size  External memory array size or expected memory to allocate.
 * @param[in]  blk_size     Size of blocks
 *
 * @return  Pointer to block buf manager or NULL in case of failure
 */
blk_buf_mgr_t * blkbufMgrInit(uint64_t * ext_buffer, uint32_t buffer_size, uint32_t blk_size);

/*!
 * Destroys a block buffer manager and respective list of blocks.
 * All blocks previously created using this manager are freed as well, becoming unusable.
 * Therefore, it is advisable to return all of them to the manager before calling it.
 *
 * @param[in]  blk_buf_mgr  Pointer to block buffer manager
 */
void blkbufMgrDestroy(blk_buf_mgr_t * blk_buf_mgr);

/*!
 * Returns a single empty block, with all its space available for generic usage.
 * Can be used for any purpose, before being returned properly using 'blkbufDestroyVoidBlk()'.
 * It is not compatible with other blk buffer functions, except 'blkbufDestroyVoidBlk()'.
 *
 * @param  blk_buf_mgr Pointer to block buffer manager
 *
 * @return             Pointer to generic empty block
 */
void * blkbufGetVoidBlk(blk_buf_mgr_t * const blk_buf_mgr);

/*!
 * Destroy a single block returned by 'blkbufGetVoidBlk()'.
 * This function SHOULD NOT be used for 'blk_buf_t' types.
 *
 * @param blk_buf_mgr Pointer to block buffer manager
 * @param blk         Generic block to be returned
 */
void blkbufReturnVoidBlk(blk_buf_mgr_t * const blk_buf_mgr, void * blk);

/*!
 * Retrieves a new block buffer from the block buffer manager.
 *
 * @param[in]  blk_buf_mgr Pointer to block buf manager
 *
 * @return                 Block buffer or NULL (in case of failure)
 */
blk_buf_t * blkbufInit(blk_buf_mgr_t * blk_buf_mgr);

/*!
 * Duplicates a blk buffer, allowing different threads to access the same data simultanously.
 * Is designed to be data efficient, by sharing the same data blocks betten both blk buffers.
 * This data optimization only stays valid if no data is written to any of the buffers.
 *
 * @param[in]  blk_buf  Pointer to block buffer to duplicate
 *
 * @return              Block buffer or NULL (in case of failure)
 */
blk_buf_t * blkbufDuplicate(blk_buf_t * blk_buf);

/*!
 * Returns the block buffer to the block buffer manager free list.
 * The data blocks will only be destroyed if no other blk buffer is using them.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 */
void blkbufDestroy(blk_buf_t * blk_buf);

/*!
 * Returns the number of data bytes contained by the block buffer blocks.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 *
 * @return               Number of bytes
 */
uint32_t blkbufDataLen(const blk_buf_t * blk_buf);

/*!
 * Returns the block size of each element of the block buffer.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 *
 * @return               Size of each block, in bytes
 */
uint32_t blkbufBlockSize(const blk_buf_t * blk_buf);

/*!
 * Returns the number of blocks used by a block buffer.
 * Will count all blocks used by this blk buffer, without considering if the data is shared with other blk buffers or not.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 *
 * @return               Number of blocks used by block buffer
 */
uint32_t blkbufNumBlocks(const blk_buf_t * blk_buf);

/*!
 * Returns the number of free blocks available at a certain moment in the block buffer manager.
 *
 * @param[in]  blk_buf_mgr Pointer to block buffer manager
 *
 * @return                 Number of free blocks
 */
uint32_t blkbufNumFreeBlocks(blk_buf_mgr_t * const blk_buf_mgr);

/*!
 * Returns the minimum number of free blocks ever registered on the block buffer manager.
 *
 * @param[in]  blk_buf_mgr Pointer to block buffer manager
 *
 * @return                 Minimum number of free blocks
 */
uint32_t blkbufNumMinBlocks(blk_buf_mgr_t * const blk_buf_mgr);

/*!
 * Returns the maximum percentage (0..1) of blocks ever used simultanously.
 *
 * @param  blk_buf_mgr Pointer to block buffer manager
 *
 * @return             Percentage (0..1) corresponding to the maximum amount of blocks ever used simultaneously
 */
float blkbufMaxDataUsed(blk_buf_mgr_t * const blk_buf_mgr);

/*!
 * Appends binary data to a block buffer.
 * This operation will preserve all previous data.
 *
 * @param[in]  blk_buf    Pointer to block buffer
 * @param[in]  new_data   New binary data to be appended
 * @param[in]  data_size  Size of the new data
 *
 * @return                Number of bytes that were appended to the block buffer
 */
int32_t blkbufAppendBin(blk_buf_t * blk_buf, const void * new_data, const uint32_t data_size);

/*!
 * Appends a string to a block buffer.
 * This operation will preserve all previous data.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 * @param[in]  new_str   New string to be appended
 *
 * @return               Number of characters that were appended to the block buffer, excluding the terminaiton character
 */
int32_t blkbufAppendStr(blk_buf_t * blk_buf, const char * new_str);

/*!
 * Writes binary data to a block buffer.
 * This operation will erase all previous data.
 *
 * @param[in]  blk_buf    Pointer to block buffer
 * @param[in]  new_data   New binary data
 * @param[in]  data_size  Size of the new data
 *
 * @return                Number of bytes that were written to the block buffer
 */
int32_t blkbufReplaceBin(blk_buf_t * const blk_buf, const void * new_data, const uint32_t data_size);

/*!
 * Writes a string to a block buffer.
 * This operation will erase all previous data.
 *
 * @param[in]  blk_buf   Pointer to block buffer
 * @param[in]  new_str   New string
 *
 * @return               Number of characters that were written to the block buffer, excluding the terminaiton character
 */
int32_t blkbufReplaceStr(blk_buf_t * const blk_buf, const char * new_str);

/*!
 * Copies the data from a block buffer to a void address.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  dest_ptr       Pointer to memory that will receive data
 * @param[in]  blk_buf        Pointer to block buffer
 * @param[in]  dest_max_size  Maximum size of data that can be written to 'dest_ptr'
 *
 * @return                    Number of bytes that were successfully copied
 */
int32_t blkbufCopyBin(void * dest_ptr, const blk_buf_t * blk_buf, uint32_t dest_max_size);

/*!
 * Copies the string from a block buffer to a character array.
 * A termination character is always added to the end of the destination buffer.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  dest_ptr       Pointer to memory that will receive data
 * @param[in]  blk_buf        Pointer to block buffer
 * @param[in]  dest_max_size  Maximum size data that can be written to 'dest_ptr'
 *
 * @return                    Number of characters that were successfully copied, not including termination character
 */
int32_t blkbufCopyStr(char * dest_ptr, const blk_buf_t * blk_buf, uint32_t dest_max_size);

/*!
 * (Re)Starts the blk buffer iterator, to be able to copy the data segment by segment.
 * Allows O(1) access time to the next data.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  blk_buf      Pointer to block buffer
 */
void  blkbufIteratorStart(blk_buf_t * blk_buf);

/*!
 * Get the number of characters/bytes remaining to be read by the iterator.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  blk_buf Pointer to blk buf
 *
 * @return             Number of characters/bytes remaining to be read
 */
int32_t blkbufIteratorRemaining(blk_buf_t * blk_buf);

/*!
 * Iterates over a segment of data from a block buffer, copying it to a void address.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  dest_ptr       Pointer to memory that will receive data
 * @param[in]  blk_buf        Pointer to blk buffer
 * @param[in]  dest_max_size  Maximum size of data that can be written to 'dest_ptr'
 *
 * @return                    Number of bytes that were successfully copied
 *
 */
int32_t blkbufIterateBin(void * dest_ptr, blk_buf_t * blk_buf, uint32_t dest_max_size);

/*!
 * Iterates over a segment of a string from the block buffer, copying it to a void address.
 * A termination character is always added to the end of the destination buffer.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  dest_ptr       Pointer to memory that will receive data
 * @param[in]  blk_buf        Pointer to blk buffer
 * @param[in]  dest_max_size  Maximum size data that can be written to 'dest_ptr'
 *
 * @return                    Number of characters that were successfully copied, not including termination character
 */
int32_t blkbufIterateStr(char * dest_ptr, blk_buf_t * blk_buf, uint32_t dest_max_size);

/*!
 * Writes the data from a block buffer to a file descriptor.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  fd        File descriptor
 * @param[in]  blk_buf   Pointer to block buffer
 *
 * @return               Number of bytes that were successfully written, -1 if error
 */
int32_t blkbufWriteToFd(int fd, const blk_buf_t * blk_buf);

/*!
 * Writes the data from a block buffer to a file.
 * This operation will not modify any data on the block buffer.
 *
 * @param[in]  file      FILE pointer
 * @param[in]  blk_buf   Pointer to block buffer
 *
 * @return               Number of bytes that were successfully written, -1 if error
 */
int32_t blkbufWriteToFile(FILE * file, const blk_buf_t * blk_buf);

/*!
 * Append new string data to a block buffer, using printf format.
 * It is limited to 1024 bytes.
 *
 * @param[in]  base_blk Pointer to block buffer
 * @param[in]  format   Format string
 * @param[in]  ...      Arguments for format string
 *
 * @return              Number of elements that were successfully written to the array
 */
int32_t blkbufAppendPrintf(blk_buf_t * base_blk, const char * format, ...) __attribute__ ((format (printf, 2, 3)));

/*!
 * Append new string data to a block buffer, using printf format.
 * It requires a free array as input, to be used as an intermediary buffer.
 * The size of this array will limit the maximum number of characters that can be printed.
 *
 * @param[in]  base_blk    Pointer to block buffer
 * @param[in]  buffer      Temporary buffer required for printf
 * @param[in]  buffer_size Size of the buffer
 * @param[in]  format      Format string
 * @param[in]  ...         Arguments for format string
 *
 * @return                 Number of elements that were successfully written to the array
 */
int32_t blkbufAppendPrintfBuf(blk_buf_t * base_blk, char * buffer, uint32_t buffer_size, const char * format, ...) __attribute__ ((format (printf, 4, 5)));

#ifdef __cplusplus
}
#endif

// EOF
