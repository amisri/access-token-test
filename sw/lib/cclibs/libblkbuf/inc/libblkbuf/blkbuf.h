/*!
 * @file  blkbuf.h
 * @brief Converter Control Block Buffer library type declaration
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "cclibs.h"

#define PRINTF_BUFFER_SIZE 1024
#define FD_READ_BUF_SIZE   1024

union data_t                            //!< Allows straightforward access to 8 bit data, while keeping 64 bit alignment.
{
    uint8_t                 u8[1];
    uint64_t                u64[1];
};

struct blk_buf_mgr_t                    //!< Structure used to manage the pre-allocated memory blocks
{
    blk_buf_t             * free_blk_list;          //!< Pointer to list of free blocks.
    uint32_t                num_total_blocks;       //!< Total number of blocks.
    uint32_t                num_free_blocks;        //!< Number of blocks on the free block list.
    uint32_t                lowest_num_free_blks;   //!< Minimum number detected on the free block list since its creation.
    uint32_t                blk_size;               //!< Size of each block on the memory pool, in bytes.
    uint32_t                blk_avail_size;         //!< Available bytes (for input data) on the second and next blocks of each list.
    uint64_t              * allocked_mem;           //!< Pointer to internally allocated buffer (for the blocks). NULL if external buffer is used.
    pthread_mutex_t         free_blk_mutex;         //!< To avoid race conditions while manipulating free block list.
    pthread_mutex_t         copy_blk_mutex;         //!< To avoid race conditions while manipulating free block list.
};

struct blk_header_t                     //!< Structure for block buffer header.
{
    blk_buf_t             * data_blk_next;          //!< Pointer to next block.
    blk_buf_t             * data_blk_last;          //!< Pointer to last block on this list.

    blk_buf_t             * list_left;              //!< Pointer to left header on double linked list (sharing same data).
    blk_buf_t             * list_right;             //!< Pointer to right header on double linked list (sharing same data).

    blk_buf_mgr_t         * blk_buf_mgr;            //!< Pointer to block buffer manager.

    uint32_t                blk_size;               //!< Size of blocks.
    uint32_t                blk_avail_size;         //!< Available data size.
    uint32_t                full_len;               //!< Length of data stored on this and next blocks.
    uint32_t                num_blocks;             //!< Number of blocks on the list.

    blk_buf_t             * iter_curr_blk;          //!< Current data block that iterator reading.
    uint32_t                iter_curr_pos;          //!< Index of next byte to read using iterator.
};

struct blk_data_t                       //!< Structure for block buffer data.
{
    blk_buf_t            * data_blk_next;           //!< Pointer to next block.
    union data_t           data;                    //!< Binary data, with 64-bit alignment.
};

union blk_buf_t                         //!< Block buffer union.
{
    blk_buf_t            * data_blk_next;           //!< Pointer to next block.
    struct blk_header_t    blk_header;              //!< Header structure format.
    struct blk_data_t      blk_data;                //!< Data structure format.
    struct blk_buf_mgr_t   blk_buf_mgr;             //!< Block buffer manager structure format.
};

// EOF
