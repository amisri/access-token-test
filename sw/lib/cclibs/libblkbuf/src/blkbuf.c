/*!
 * @file  blkbuf.c
 * @brief Block Buffer library functions.
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <stddef.h>
#include <pthread.h>
#include <unistd.h>

#include "cclibs.h"
#include "libblkbuf.h"
#include "libblkbuf/blkbuf.h"



// Static functions



/*!
 * Constructs a linked list of free blocks (pool), from a preallocated memory array.
 * Buffer must be aligned to 64-bit (uint64_t).
 *
 * @param[in]  ext_buffer Preallocated data buffer.
 * @param[in]  num_blks   Number of blocks to be created suing the buffer.
 * @param[in]  blk_size   Size of each block (including headers).
 *
 * @return                Pointer to the first element of the list.
 */
static blk_buf_t * constructBlkPool(uint64_t * const ext_buffer, const uint32_t num_blks, const uint32_t blk_size)
{
    blk_buf_t * blk_ptr = (blk_buf_t *) ext_buffer;

    for(uint32_t i = 1; i < num_blks; i++)
    {
        blk_ptr->blk_data.data_blk_next = (blk_buf_t *) (ext_buffer + (i * (blk_size / 8)));
        blk_ptr = blk_ptr->blk_data.data_blk_next;
    }

    blk_ptr->blk_data.data_blk_next = NULL;

    return (blk_buf_t *) ext_buffer;
}



/*!
 * Removes and returns the first element from the list of free blocks.
 * Protected by mutex, thus allowing simultaneous calls.
 *
 * @param[in]  blk_buf_mgr   Pointer to block buffer manager.
 *
 * @return                   Pointer to the element returned from the pool.
 *                           NULL if no blocks available.
 */
static blk_buf_t * getBlkFromMgr(blk_buf_mgr_t * const blk_buf_mgr)
{
    pthread_mutex_lock(&blk_buf_mgr->free_blk_mutex);

    blk_buf_t * blk_ptr = blk_buf_mgr->free_blk_list;

    if(blk_ptr != NULL)
    {
        // If (first) element exits, remove from list

        blk_buf_mgr->free_blk_list = blk_buf_mgr->free_blk_list->blk_data.data_blk_next;
        blk_buf_mgr->num_free_blocks--;

        if(blk_buf_mgr->num_free_blocks < blk_buf_mgr->lowest_num_free_blks)
        {
            blk_buf_mgr->lowest_num_free_blks = blk_buf_mgr->num_free_blocks;
        }

        blk_ptr->blk_data.data_blk_next = NULL;
    }

    pthread_mutex_unlock(&blk_buf_mgr->free_blk_mutex);

    return blk_ptr;
}



/*!
 * Removes and returns a list of elements from the list of free blocks.
 *
 * @param[in]   blk_buf_mgr Pointer to block buffer manager.
 * @param[out]  first_blk   Pointer to pointer to first element of new data block list.
 * @param[out]  last_blk    Pointer to pointer to last element of new data block list.
 * @param[in]   num_blks    Number of blocks to be returned
 *
 * @return                  0 in case of success, -1 in case of failure
 */
static int32_t getBlkChainFromMgr(blk_buf_mgr_t * const blk_buf_mgr, blk_buf_t ** first_blk, blk_buf_t ** last_blk, uint32_t num_blks)
{
    pthread_mutex_lock(&blk_buf_mgr->free_blk_mutex);

    // Do nothing if zero blocks are copied

    if(num_blks == 0)
    {
        *first_blk = NULL;
        *last_blk  = NULL;
        return 0;
    }

    // Check if enough free blocks are available

    if(blk_buf_mgr->num_free_blocks < num_blks)
    {
        pthread_mutex_unlock(&blk_buf_mgr->free_blk_mutex);

        return -1;
    }

    blk_buf_t * blk_ptr = blk_buf_mgr->free_blk_list;
    uint32_t    counter = 0;

    while(++counter < num_blks)
    {
        blk_ptr = blk_ptr->blk_data.data_blk_next;
    }

    *first_blk = blk_buf_mgr->free_blk_list;
    *last_blk  = blk_ptr;

    blk_buf_mgr->free_blk_list    = blk_ptr->blk_data.data_blk_next;
    blk_buf_mgr->num_free_blocks -= num_blks;

    if(blk_buf_mgr->num_free_blocks < blk_buf_mgr->lowest_num_free_blks)
    {
        blk_buf_mgr->lowest_num_free_blks = blk_buf_mgr->num_free_blocks;
    }

    pthread_mutex_unlock(&blk_buf_mgr->free_blk_mutex);

    blk_ptr->blk_data.data_blk_next = NULL;

    return 0;
}



/*!
 * Returns a blk_buf_t element to the list of free blocks.
 * Protected by mutex, thus allowing simultaneous calls.
 *
 * @param[in]  blk_buf_mgr  Pointer to block buffer manager.
 * @param[in]  blk          Pointer to block to be returned.
 */
static void returnBlkToMgr(blk_buf_mgr_t * const blk_buf_mgr, blk_buf_t * blk)
{
    pthread_mutex_lock(&blk_buf_mgr->free_blk_mutex);

    blk->blk_data.data_blk_next = blk_buf_mgr->free_blk_list;
    blk_buf_mgr->free_blk_list  = blk;
    blk_buf_mgr->num_free_blocks++;

    pthread_mutex_unlock(&blk_buf_mgr->free_blk_mutex);
}



/*!
 * Returns a chain of blk_buf_t elements to the list of free blocks.
 * Protected by mutex, thus allowing simultaneous calls.
 *
 * @param[in]  blk_buf_mgr  Pointer to block buffer manager.
 * @param[in]  first_blk    Pointer to first block on the chain to be returned.
 * @param[in]  last_blk     Pointer to last block on the chain to be returned.
 * @param[in]  num_blks     Number of blocks on the chain.
 */
static void returnBlkChainToMgr(blk_buf_mgr_t * const blk_buf_mgr, blk_buf_t * first_blk, blk_buf_t * last_blk, uint32_t num_blks)
{
    // Returns all blocks at once

    pthread_mutex_lock(&blk_buf_mgr->free_blk_mutex);

    last_blk->blk_data.data_blk_next = blk_buf_mgr->free_blk_list;
    blk_buf_mgr->free_blk_list       = first_blk;
    blk_buf_mgr->num_free_blocks    += num_blks;

    pthread_mutex_unlock(&blk_buf_mgr->free_blk_mutex);
}



/*!
 * Given a new data block, this functions fills it with the data of a previous data block chain.
 * New data blocks will be appended as necessary.
 *
 * @param[in]  blk_buf_mgr  Pointer to block buffer manager.
 * @param[out] dest_first   Pointer to pointer to first element of new data block list.
 * @param[out] dest_last    Pointer to pointer to last element of new data block list.
 * @param[in]  source_first Pointer to first block of data block list expected to be copyied.
 * @param[in]  num_blks     Number of blocks expected to be copyied.
 *
 * @return                  0 if successful, -1 if failed
 */
static int32_t duplicateDataBlocks(blk_buf_mgr_t * const blk_buf_mgr, blk_buf_t ** dest_first, blk_buf_t ** dest_last, blk_buf_t * source_first, uint32_t num_blks)
{
    // Do nothing if zero blocks are copied

    if(num_blks == 0)
    {
        *dest_first = NULL;
        *dest_last  = NULL;
        return 0;
    }

    // Create a new data block chain, with same size as origin
    // Will not change output in case of error

    if(getBlkChainFromMgr(blk_buf_mgr, dest_first, dest_last, num_blks) < 0)
    {
        return -1;
    }

    // Copy data blocks

    blk_buf_t * src_ptr = source_first;
    blk_buf_t * dst_ptr = *dest_first;

    while(src_ptr != NULL)
    {
        memcpy(dst_ptr->blk_data.data.u8, src_ptr->blk_data.data.u8, blk_buf_mgr->blk_avail_size);

        src_ptr = src_ptr->blk_data.data_blk_next;
        dst_ptr = dst_ptr->blk_data.data_blk_next;
    }

    return 0;
}



/*!
 * Removes a head from the list of block headers sharing the same data.
 * Only the connections between the headers are removed. They other data remains the same, including the pointers to data blocks.
 *
 * @param blk_head Pointer to block buffer to be removed
 */
static void removeHeaderFromPeers(blk_buf_t * blk_head)
{
    blk_buf_t * list_left  = blk_head->blk_header.list_left;
    blk_buf_t * list_right = blk_head->blk_header.list_right;

    blk_head->blk_header.list_left  = NULL;
    blk_head->blk_header.list_right = NULL;

    if(list_left != NULL)
    {
        list_left->blk_header.list_right = list_right;
    }

    if(list_right != NULL)
    {
        list_right->blk_header.list_left = list_left;
    }
}



/*!
 * Adds an header to a list of block headers, all pointing to the same data.
 *
 * @param peer_header   Pointer to one of the headers already on the list
 * @param new_header    Pointer to the header to add to the list
 */
static void addHeaderToPeers(blk_buf_t * peer_header, blk_buf_t * new_header)
{
    // Connect to left and rigth headers

    blk_buf_t * list_left  = peer_header;
    blk_buf_t * list_right = peer_header->blk_header.list_right;

    new_header->blk_header.list_right = list_right;

    if(list_right != NULL)
    {
        list_right->blk_header.list_left = new_header;
    }

    new_header->blk_header.list_left = list_left;
    list_left->blk_header.list_right = new_header;

    // Point to already existing data blocks

    new_header->blk_header.data_blk_next  = peer_header->blk_header.data_blk_next;
    new_header->blk_header.data_blk_last  = peer_header->blk_header.data_blk_last;

    // Copy header info

    new_header->blk_header.blk_buf_mgr    = peer_header->blk_header.blk_buf_mgr;
    new_header->blk_header.blk_size       = peer_header->blk_header.blk_size;
    new_header->blk_header.blk_avail_size = peer_header->blk_header.blk_avail_size;
    new_header->blk_header.full_len       = peer_header->blk_header.full_len;
    new_header->blk_header.num_blocks     = peer_header->blk_header.num_blocks;

    new_header->blk_header.iter_curr_blk  = peer_header->blk_header.data_blk_next;
    new_header->blk_header.iter_curr_pos  = 0;
}



blk_buf_mgr_t * blkbufMgrInit(uint64_t * const ext_buffer, const uint32_t buffer_size, const uint32_t blk_size)
{
    // Enforce 64-bit alignment (8 bytes).

    CC_ASSERT((blk_size & 0x7) == 0);

    // Check minimum block size (enough for 1st block header and 8 byte data).

    CC_ASSERT(blk_size >= sizeof(blk_buf_t));

    // Check minimum external buffer size (enough for 1 block of data, used for blk buf manager).

    CC_ASSERT(buffer_size >= blk_size);

    const uint32_t num_blks = buffer_size / blk_size;

    uint64_t  * allocated_mem;
    blk_buf_t * free_blk_list;

    if(ext_buffer != NULL)
    {
        allocated_mem = NULL;

        memset(ext_buffer, 0, buffer_size);

        free_blk_list = constructBlkPool(ext_buffer, num_blks, blk_size);
    }
    else
    {
        allocated_mem = (uint64_t *) calloc (num_blks, blk_size);

        if(allocated_mem == NULL)
        {
            return NULL;
        }

        free_blk_list = constructBlkPool(allocated_mem, num_blks, blk_size);
    }

    // Remove first element from list to use as block buffer manager

    blk_buf_mgr_t * blk_buf_mgr = (blk_buf_mgr_t *) free_blk_list;
    free_blk_list = free_blk_list->blk_data.data_blk_next;

    const uint32_t num_free_blks = num_blks - 1;

    // Initialize blk buf manager

    blk_buf_mgr->blk_size         = blk_size;
    blk_buf_mgr->num_free_blocks  = num_free_blks;
    blk_buf_mgr->num_total_blocks = num_free_blks;
    blk_buf_mgr->lowest_num_free_blks     = num_free_blks;
    blk_buf_mgr->blk_avail_size   = blk_size - offsetof(blk_buf_t, blk_data.data.u8);
    blk_buf_mgr->allocked_mem     = allocated_mem; // Pointer to previously allocated mem

    // Initialize mutexes

    if(pthread_mutex_init(&blk_buf_mgr->free_blk_mutex, NULL) != 0)
    {
        if(blk_buf_mgr->allocked_mem != NULL)
        {
            free(blk_buf_mgr->allocked_mem);
        }

        return NULL;
    }

    if(pthread_mutex_init(&blk_buf_mgr->copy_blk_mutex, NULL) != 0)
    {
        pthread_mutex_destroy(&blk_buf_mgr->free_blk_mutex);

        if(blk_buf_mgr->allocked_mem != NULL)
        {
            free(blk_buf_mgr->allocked_mem);
        }

        return NULL;
    }

    return blk_buf_mgr;
}



void blkbufMgrDestroy(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);
    CC_ASSERT(blk_buf_mgr->num_total_blocks == blk_buf_mgr->num_free_blocks);

    pthread_mutex_destroy(&blk_buf_mgr->free_blk_mutex);
    pthread_mutex_destroy(&blk_buf_mgr->copy_blk_mutex);

    // Free buffer only if it was internally allocated.

    if(blk_buf_mgr->allocked_mem != NULL)
    {
        free(blk_buf_mgr->allocked_mem);
    }
}



void * blkbufGetVoidBlk(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);

    return getBlkFromMgr(blk_buf_mgr);
}



void blkbufReturnVoidBlk(blk_buf_mgr_t * const blk_buf_mgr, void * blk)
{
    CC_ASSERT(blk_buf_mgr != NULL);
    CC_ASSERT(blk != NULL);

    returnBlkToMgr(blk_buf_mgr, (blk_buf_t*) blk);
}



blk_buf_t * blkbufInit(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);

    // Get 1 block for the header

    blk_buf_t * new_blk_head = getBlkFromMgr(blk_buf_mgr);

    if(new_blk_head == NULL)
    {
        return NULL;
    }

    // Initialize header block

    new_blk_head->blk_header.list_right     = NULL;
    new_blk_head->blk_header.list_left      = NULL;

    new_blk_head->blk_header.data_blk_next  = NULL;
    new_blk_head->blk_header.data_blk_last  = NULL;

    new_blk_head->blk_header.blk_buf_mgr    = blk_buf_mgr;
    new_blk_head->blk_header.blk_size       = blk_buf_mgr->blk_size;
    new_blk_head->blk_header.blk_avail_size = blk_buf_mgr->blk_avail_size;
    new_blk_head->blk_header.full_len       = 0;
    new_blk_head->blk_header.num_blocks     = 1; // Starts with 1 block

    new_blk_head->blk_header.iter_curr_blk  = NULL;
    new_blk_head->blk_header.iter_curr_pos  = 0;

    return new_blk_head;
}



blk_buf_t * blkbufDuplicate(blk_buf_t * blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    blk_buf_mgr_t * blk_buf_mgr  = blk_buf->blk_header.blk_buf_mgr;

    // Get only a new header - the data blocks will be reused.

    blk_buf_t * new_blk_head = getBlkFromMgr(blk_buf_mgr);

    if(new_blk_head == NULL)
    {
        return NULL;
    }

    // Insert header in the double linked list of the headers that point to the same data.

    pthread_mutex_lock(&blk_buf_mgr->copy_blk_mutex);

    addHeaderToPeers(blk_buf, new_blk_head);

    pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);

    return new_blk_head;
}



void blkbufDestroy(blk_buf_t * blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    blk_buf_mgr_t * blk_buf_mgr = blk_buf->blk_header.blk_buf_mgr;

    // Remove header from the list of headers sharing the same data

    pthread_mutex_lock(&blk_buf_mgr->copy_blk_mutex);

    if(blk_buf->blk_header.list_left != NULL || blk_buf->blk_header.list_right != NULL)
    {
        removeHeaderFromPeers(blk_buf);

        pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);
    }
    else
    {
        pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);

        if(blk_buf->blk_header.data_blk_next != NULL)
        {
            returnBlkChainToMgr(blk_buf_mgr,
                                blk_buf->blk_header.data_blk_next,
                                blk_buf->blk_header.data_blk_last,
                                blk_buf->blk_header.num_blocks - 1);
        }
    }

    // Destroy the header

    returnBlkToMgr(blk_buf_mgr, blk_buf);
}



uint32_t blkbufDataLen(const blk_buf_t * const blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    return blk_buf->blk_header.full_len;
}



uint32_t blkbufBlockSize(const blk_buf_t * const blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    return blk_buf->blk_header.blk_size;
}



uint32_t blkbufNumBlocks(const blk_buf_t * const blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    return blk_buf->blk_header.num_blocks;
}



uint32_t blkbufNumFreeBlocks(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);

    return blk_buf_mgr->num_free_blocks;
}



uint32_t blkbufNumMinBlocks(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);

    return blk_buf_mgr->lowest_num_free_blks;
}

float blkbufMaxDataUsed(blk_buf_mgr_t * const blk_buf_mgr)
{
    CC_ASSERT(blk_buf_mgr != NULL);

    float lowest_num_free_blks = blk_buf_mgr->lowest_num_free_blks;
    float max_blks             = (float)(blk_buf_mgr->num_total_blocks + 1);

    return (max_blks - lowest_num_free_blks) / max_blks;
}

int32_t blkbufAppendBin(blk_buf_t * const blk_buf, const void * new_data, const uint32_t data_size)
{
    CC_ASSERT(blk_buf != NULL);
    CC_ASSERT(new_data != NULL);

    blk_buf_mgr_t * const blk_buf_mgr = blk_buf->blk_header.blk_buf_mgr;

    // Appendin zero bytes is supported behaviour. Nothing is done.

    if(data_size == 0)
    {
        return 0;
    }

    // If more than one header is pointing to the same data blocks, all those data blocks need to be copied to a new list before appending the new data.
    // This will preserve the data unchanged on the other blk buffers, and keep all blocks independent from the user point of view.

    if(blk_buf->blk_header.list_left != NULL || blk_buf->blk_header.list_right != NULL)
    {
        pthread_mutex_lock(&blk_buf_mgr->copy_blk_mutex);

        // Double check if there is another header sharing the same data.

        if(blk_buf->blk_header.list_left != NULL || blk_buf->blk_header.list_right != NULL)
        {
            // Try to duplicate data blocks

            if(blk_buf->blk_header.num_blocks > 1)
            {
                if(duplicateDataBlocks(blk_buf_mgr,
                                      &blk_buf->blk_header.data_blk_next,
                                      &blk_buf->blk_header.data_blk_last,
                                       blk_buf->blk_header.data_blk_next,
                                       blk_buf->blk_header.num_blocks - 1) < 0)
                {
                    pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);
                    return -1;
                }
            }

            // Finally, if all data copy was successfull, remove headers from its neighbours.
            // It will already be pointing to the copyied data.

            removeHeaderFromPeers(blk_buf);
        }

        pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);
    }

    // Get the new data blocks required to append all the data
    // For integer division: ceil(x/y) <=> (x + y - 1) / y

    uint32_t    data_size_after = blk_buf->blk_header.full_len + data_size;
    uint32_t    num_blks_after  = (data_size_after + blk_buf->blk_header.blk_avail_size - 1) / blk_buf->blk_header.blk_avail_size;
    uint32_t    num_new_blks    = num_blks_after - (blk_buf->blk_header.num_blocks - 1);

    blk_buf_t * curr_blk        = blk_buf->blk_header.data_blk_next != NULL ?
                                    blk_buf->blk_header.data_blk_last : blk_buf; // Last data block or header

    if(num_new_blks > 0)
    {
        if(getBlkChainFromMgr(blk_buf_mgr,
                             &curr_blk->blk_data.data_blk_next,
                             &blk_buf->blk_header.data_blk_last,
                             num_new_blks) < 0)
        {
            return -1;
        }
    }

    // Identify the starting point for appending the new data, in the current block

    uint32_t offset = blk_buf->blk_header.full_len % blk_buf->blk_header.blk_avail_size;   // Offset for the new data in the last block

    offset = offset == 0 ? blk_buf->blk_header.blk_avail_size : offset;

    // In the case the offset is zero, means that the last block is already full (or no data blocks exist)
    // Only append if offset is different than zero

    const uint8_t * read_ptr_u8    = (const uint8_t *) new_data;                    // Pointer (iterator) over new data
    uint32_t        remaining_data = data_size;                                     // Counter

    uint32_t        avail_size     = blk_buf->blk_header.blk_avail_size - offset;   // Number of free bytes available on the next data block.
    uint32_t        data_to_copy;                                                   // How much data to copy on next block

    // Append data on first and next blocks

    while(remaining_data > 0)
    {
        avail_size   = blk_buf->blk_header.blk_avail_size - offset;
        data_to_copy = (remaining_data < avail_size) ? remaining_data : avail_size;

        memcpy(curr_blk->blk_data.data.u8 + offset, read_ptr_u8, data_to_copy);

        read_ptr_u8    += data_to_copy;
        remaining_data -= data_to_copy;
        curr_blk        = curr_blk->blk_data.data_blk_next;

        // Configure parameters for 2nd and next blocks

        avail_size = blk_buf->blk_header.blk_avail_size;
        offset     = 0;
    }

    // Update header information

    blk_buf->blk_header.num_blocks += num_new_blks;
    blk_buf->blk_header.full_len   += data_size;

    // Return number of bytes written to block buffer

    return data_size;
}



int32_t blkbufAppendStr(blk_buf_t * const blk_buf, const char * new_str)
{
    return blkbufAppendBin(blk_buf, new_str, strlen(new_str));
}



int32_t blkbufReplaceBin(blk_buf_t * const blk_buf, const void * new_data, const uint32_t data_size)
{
    // The overall aproach is to free this blk buffer from all its previous data, and then append the new data.

    CC_ASSERT(blk_buf != NULL);
    CC_ASSERT(new_data != NULL);

    blk_buf_mgr_t * blk_buf_mgr = blk_buf->blk_header.blk_buf_mgr;
    uint32_t clear_data = 1;

    if(blk_buf->blk_header.list_right != NULL || blk_buf->blk_header.list_left != NULL)
    {
        // Do not erase the data if more than one header is pointing the same data blocks.
        // Instead, remove the header block from the double linked list.

        pthread_mutex_lock(&blk_buf_mgr->copy_blk_mutex);

        if(blk_buf->blk_header.list_right != NULL || blk_buf->blk_header.list_left != NULL)
        {
            removeHeaderFromPeers(blk_buf);
            clear_data = 0;
        }

        pthread_mutex_unlock(&blk_buf_mgr->copy_blk_mutex);
    }

    if(clear_data != 0 && blk_buf->blk_header.data_blk_next != NULL)
    {
        returnBlkChainToMgr(blk_buf_mgr,
                            blk_buf->blk_header.data_blk_next,
                            blk_buf->blk_header.data_blk_last,
                            blk_buf->blk_header.num_blocks - 1);
    }

    // Reconfigure header, to match empty data.

    blk_buf->blk_header.data_blk_next = NULL;
    blk_buf->blk_header.data_blk_last = NULL;

    blk_buf->blk_header.full_len      = 0;
    blk_buf->blk_header.num_blocks    = 1;

    blk_buf->blk_header.iter_curr_blk = NULL;
    blk_buf->blk_header.iter_curr_pos = 0;

    // Append the new data to the empty blk buffer.

    return blkbufAppendBin(blk_buf, new_data, data_size);
}



int32_t blkbufReplaceStr(blk_buf_t * const blk_buf, const char * new_str)
{
    return blkbufReplaceBin(blk_buf, new_str, strlen(new_str));
}



int32_t blkbufCopyBin(void * const dest_ptr, const blk_buf_t * const blk_buf, const uint32_t dest_max_size)
{
    CC_ASSERT(dest_ptr != NULL);
    CC_ASSERT(blk_buf  != NULL);

    const uint32_t    data_size_blk = blk_buf->blk_header.blk_avail_size;                       // Maximum available data in each data block.
    uint32_t          read_size     = blk_buf->blk_header.full_len < dest_max_size ?
                                        blk_buf->blk_header.full_len : dest_max_size;           // Length of data to read.
    uint32_t          next_blk_size = read_size < data_size_blk ? read_size : data_size_blk;    // Length of data to read in the first data block.

    const blk_buf_t * curr_blk      = blk_buf->blk_header.data_blk_next;                        // Current (first) data block.
    const uint8_t   * read_ptr      = curr_blk != NULL ? curr_blk->blk_data.data.u8 : NULL;     // Pointer to next data to be read.
    uint32_t          write_idx     = 0;                                                        // To track next writting position on destination buffer.

    uint32_t          read_data_c   = read_size;

    // Iterate over all blocks and copy their data one by one to the array

    while(read_data_c > 0)
    {
        memcpy(&((uint8_t *) dest_ptr)[write_idx], read_ptr, next_blk_size);

        read_data_c -= next_blk_size;
        write_idx   += next_blk_size;

        next_blk_size = read_data_c < data_size_blk ? read_data_c : data_size_blk;

        curr_blk = curr_blk->blk_data.data_blk_next;
        read_ptr = curr_blk->blk_data.data.u8;
    }

    return read_size;
}



int32_t blkbufCopyStr(char * const dest_ptr, const blk_buf_t * const blk_buf, const uint32_t dest_max_size)
{
    CC_ASSERT(dest_ptr != NULL);
    CC_ASSERT(blk_buf != NULL);

    // Check destination size, to avoid underflow

    if(dest_max_size > 1)
    {
        int32_t read_size = blkbufCopyBin(dest_ptr, blk_buf, dest_max_size - 1);

        dest_ptr[read_size] = '\0';

        return read_size;
    }
    else
    {
        if(dest_max_size > 0)
        {
           dest_ptr[0] = '\0';
        }

        return 0;
    }
}



void  blkbufIteratorStart(blk_buf_t * blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    blk_buf->blk_header.iter_curr_blk = blk_buf->blk_header.data_blk_next;
    blk_buf->blk_header.iter_curr_pos = 0;
}



int32_t blkbufIteratorRemaining(blk_buf_t * blk_buf)
{
    CC_ASSERT(blk_buf != NULL);

    return blk_buf->blk_header.full_len - blk_buf->blk_header.iter_curr_pos;
}



int32_t blkbufIterateBin(void * dest_ptr, blk_buf_t * blk_buf, const uint32_t dest_max_size)
{
    CC_ASSERT(dest_ptr != NULL);
    CC_ASSERT(blk_buf  != NULL);

    const uint32_t    data_size_blk  =  blk_buf->blk_header.blk_avail_size;                                         // Maximum available data in each data block.
    uint32_t          read_size      =  blkbufIteratorRemaining(blk_buf) < dest_max_size ?
                                            blkbufIteratorRemaining(blk_buf) : dest_max_size;                       // Length of remaining data.
    uint32_t          read_offset    =  blk_buf->blk_header.iter_curr_pos % data_size_blk;                          // Offset of the next data in the current data block
    uint32_t          next_blk_size  =  read_size < data_size_blk - read_offset ?
                                            read_size : data_size_blk - read_offset;                                // Length of remaining data in the current data block.
    uint8_t           must_iter_next =  read_size < data_size_blk - read_offset ? 0 : 1;                            // Check if more than one data block will be read during this iteration.

    blk_buf_t       * curr_blk       =  blk_buf->blk_header.iter_curr_blk != NULL ?
                                            blk_buf->blk_header.iter_curr_blk : blk_buf->blk_header.data_blk_next;  // Current (first) data block.

    uint8_t         * read_ptr       =  curr_blk != NULL ?
                                            curr_blk->blk_data.data.u8 + read_offset : NULL;                        // Pointer to next data to be read.

    uint32_t          write_idx      =  0;                                                                          // To track next writting position on destination buffer.
    uint32_t          read_size_c    =  read_size;                                                                  // Decrementing counter for copied data

    // Iterate over all blocks and copy their data one by one to the array

    while(read_size_c > 0)
    {
        memcpy(&((uint8_t *) dest_ptr)[write_idx], read_ptr, next_blk_size);

        // If last data block was completely read, move to next data block.
        // If not, remain pointing to current data block.

        if(next_blk_size == data_size_blk || must_iter_next)
        {
            curr_blk = curr_blk->blk_data.data_blk_next;
            read_ptr = curr_blk->blk_data.data.u8;
            must_iter_next = 0;
        }

        read_size_c -= next_blk_size;
        write_idx   += next_blk_size;

        next_blk_size = read_size_c < data_size_blk ? read_size_c : data_size_blk;
    }

    // Update iterator

    blk_buf->blk_header.iter_curr_blk  = curr_blk;
    blk_buf->blk_header.iter_curr_pos += read_size;

    return read_size;
}



int32_t blkbufIterateStr(char * dest_ptr, blk_buf_t * blk_buf, const uint32_t dest_max_size)
{
    CC_ASSERT(dest_ptr != NULL);
    CC_ASSERT(blk_buf  != NULL);

    // Check destination size, to avoid underflow

    if(dest_max_size > 1)
    {
        int32_t read_size = blkbufIterateBin(dest_ptr, blk_buf, dest_max_size - 1);

        dest_ptr[read_size] = '\0';

        return read_size;
    }
    else
    {
        if(dest_max_size > 0)
        {
           dest_ptr[0] = '\0';
        }

        return 0;
    }
}



int32_t blkbufWriteToFd(const int fd, const blk_buf_t * const blk_buf)
{
    CC_ASSERT(blk_buf  != NULL);

    const uint32_t    data_size_blk = blk_buf->blk_header.blk_avail_size;
    uint32_t          read_size_c   = blk_buf->blk_header.full_len;
    uint32_t          next_blk_size = read_size_c < data_size_blk ? read_size_c : data_size_blk;

    const blk_buf_t * curr_blk      = blk_buf->blk_header.data_blk_next;
    const uint8_t   * read_ptr      = curr_blk != NULL ? curr_blk->blk_data.data.u8 : NULL;

    int32_t           ret_val;

    // Iterate over all blocks and copy their data one by one to the file descriptor

    while(read_size_c > 0)
    {
        if((ret_val = write(fd, read_ptr, next_blk_size)) < 0)
        {
            return -1; 
        }

        read_size_c -= ret_val;

        next_blk_size = read_size_c < data_size_blk ? read_size_c : data_size_blk;

        curr_blk = curr_blk->blk_data.data_blk_next;
        read_ptr = curr_blk->blk_data.data.u8;
    }

    return blk_buf->blk_header.full_len;
}



int32_t blkbufWriteToFile(FILE * const file, const blk_buf_t * const blk_buf)
{
    CC_ASSERT(file     != NULL);
    CC_ASSERT(blk_buf  != NULL);

    const uint32_t    data_size_blk = blk_buf->blk_header.blk_avail_size;
    uint32_t          read_size_c   = blk_buf->blk_header.full_len;
    uint32_t          next_blk_size = read_size_c < data_size_blk ? read_size_c : data_size_blk;

    const blk_buf_t * curr_blk      = blk_buf->blk_header.data_blk_next;
    const uint8_t   * read_ptr      = curr_blk != NULL ? curr_blk->blk_data.data.u8 : NULL;

    int32_t           ret_val;

    // Iterate over all blocks and copy their data one by one to the file

    while(read_size_c > 0)
    {
        if((ret_val = fwrite(read_ptr, 1, next_blk_size, file)) < 0)
        {
            return -1;
        }

        read_size_c -= ret_val;

        next_blk_size = read_size_c < data_size_blk ? read_size_c : data_size_blk;

        curr_blk = curr_blk->blk_data.data_blk_next;
        read_ptr = curr_blk->blk_data.data.u8;
    }

    return blk_buf->blk_header.full_len;
}



/*!
 * Printf to a block buf structure (append).
 * It requires a free array as input, to be used as an intermediary buffer.
 * The size of this array will limit the maximum number of chars that can be printed.
 *
 * @param[in]  blk_buf     Pointer to block buf structure
 * @param[in]  buffer      Temporary buffer required for printf
 * @param[in]  buffer_size Size of the buffer
 * @param[in]  format      Format string
 * @param[in]  args        Variadic arguments for format string
 *
 * @return  Number of elements that were written to the array
 */
static int32_t blkbufPrintf(blk_buf_t * const blk_buf, char * const buffer, const uint32_t buffer_size, const char * const format, va_list args)
{
    int32_t len = vsnprintf(buffer, buffer_size, format, args);

    return blkbufAppendBin(blk_buf, buffer, len < buffer_size ? len : buffer_size);
}



int32_t blkbufAppendPrintf(blk_buf_t * const blk_buf, const char * const format, ...)
{
    va_list args;
    va_start(args, format);

    char buffer[PRINTF_BUFFER_SIZE];

    int32_t ret_val = blkbufPrintf(blk_buf, buffer, sizeof(buffer), format, args);

    va_end(args);

    return ret_val;
}



int32_t blkbufAppendPrintfBuf(blk_buf_t * const blk_buf, char * const buffer, const uint32_t buffer_size, const char * const format, ...)
{
    va_list args;
    va_start(args, format);

    int32_t ret_val = blkbufPrintf(blk_buf, buffer, buffer_size, format, args);

    va_end(args);

    return ret_val;
}


// EOF
