/*!
 * @file  blkbuft_list.c
 * @brief Block Buffer library test functions.
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 * 
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libblkbuf.h>
#include "libblkbuf/blkbuf.h"
#include "blkbuft_list.h"

#include <poll.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>

#include "pthread_barrier_mac.h"


#define BUF_SIZE 1<<24

char buffer_1[BUF_SIZE];
char buffer_2[BUF_SIZE];

char template[] =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit amet porta erat, eu laoreet nulla. Etiam at urna vitae dui aliquet lacinia. Proin eget enim ut augue vestibulum blandit. Sed lobortis mattis facilisis. Mauris a purus ac augue elementum faucibus sit amet at dui. Curabitur nec lectus pulvinar, iaculis neque a, convallis elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce vel sem eros.\n"
        "Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam et odio sit amet ante auctor pretium rhoncus in eros. Vivamus tincidunt placerat neque id scelerisque. Quisque vulputate vehicula ipsum, blandit consequat velit eleifend eget. Aliquam tempor nisl ut dictum elementum. Sed eget mi magna. Suspendisse potenti. Sed sit amet suscipit arcu. Vivamus ante velit, faucibus quis est id, mollis commodo erat. Quisque mattis quis magna vel scelerisque. Praesent purus dolor, ornare at felis ut, volutpat tincidunt ipsum. Aliquam id viverra ante, vel posuere mi. Nullam elementum, tellus non scelerisque convallis, velit metus tempor nulla, in luctus elit arcu ut sapien. Aenean nulla metus, vestibulum sed nibh eget, iaculis luctus nulla.\n"
        "Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed non metus eget nulla ultricies posuere. Aenean quis egestas nulla. Maecenas congue libero nec dolor dictum, ultrices malesuada elit luctus. Sed ultricies dui in porta luctus. Cras convallis ornare pharetra. Integer laoreet consectetur porttitor. Nam mollis neque in mauris tempor, non tristique dolor volutpat. Vivamus semper fermentum imperdiet. Vivamus ac orci eget nisl condimentum dapibus a porttitor sapien. Duis vel felis lectus. Quisque nec nisi sed elit tempus pellentesque non ac lacus. Donec suscipit sit amet erat vestibulum cursus.\n"
        "In volutpat ipsum quis purus tincidunt, facilisis imperdiet nunc consequat. Aliquam enim libero, fringilla in molestie ut, viverra ac nisl. Ut efficitur pretium eros, eu placerat nisl pellentesque et. Mauris a quam tortor. Ut nec ipsum euismod velit cursus consectetur non sed mauris. Pellentesque sit amet vulputate ipsum. Praesent accumsan et metus eu feugiat. Sed quam lorem, ultrices quis magna et, eleifend viverra nisi. Duis et odio imperdiet, lobortis erat id, accumsan dui. Curabitur nec erat ornare, semper enim id, gravida nunc. Morbi eget mollis nulla.\n"
        "Morbi pulvinar ante vel sem viverra blandit. Sed nec augue vestibulum, tincidunt nunc ut, hendrerit magna. Sed at consequat ante. Donec vulputate nibh sit amet sem fermentum dapibus. Curabitur hendrerit ut mauris nec pharetra. Proin pulvinar iaculis nulla, et cursus magna rhoncus vitae. Fusce a metus aliquet, rutrum diam a, consequat dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut aliquet ornare neque et ultrices. Proin eget tristique arcu, ut ultricies ex. Nullam nec sapien et odio convallis convallis. Ut fringilla mauris vel pulvinar condimentum. Vestibulum bibendum dolor purus, vitae mollis urna luctus in. Vivamus dictum, odio ut iaculis porttitor, velit neque mollis felis, vel auctor risus sapien id nulla. Aenean ac condimentum elit, in tincidunt ex. Curabitur commodo lectus turpis.\n"
        "Aenean nec luctus tellus, a pharetra quam. In fringilla sit amet lacus ac ullamcorper. Vestibulum non leo convallis lacus gravida euismod. Nam eget convallis nibh, in porttitor eros. Nam tincidunt, diam sed tincidunt porta, dui enim pulvinar nisl, at ultricies quam ligula in turpis. Donec dapibus, sapien quis pellentesque pulvinar, est dui egestas diam, sed pellentesque metus sapien et risus. Fusce pretium tincidunt eros a commodo. Nam nec euismod est, rutrum molestie urna. Duis non nulla consectetur ligula mattis aliquam ut ut tellus. Nunc venenatis accumsan neque. Proin pharetra ante in ultrices commodo. In sollicitudin nisl sit amet ligula porttitor interdum.\n"
        ;

int t_read_fd;
int t_close;

blkbuf_test_result blkbuf_test_1(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_2(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_3(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_4(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_5(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_6(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_7(char * message, uint32_t message_max);
blkbuf_test_result blkbuf_test_8(char * message, uint32_t message_max);

struct blkbuf_test tests[] =
{
    {blkbuf_test_1, "Testing blk buffer manager initialization and destruction"},
    {blkbuf_test_2, "Testing blk buffer structure manipulation"},
    {blkbuf_test_3, "Testing append/replace and copy functions"},
    {blkbuf_test_4, "Testing copy functions with iterator"},
    {blkbuf_test_5, "Testing file writing functions"},
    {blkbuf_test_6, "Testing block buffer printf"},
    {blkbuf_test_7, "Testing block buffer duplication and data reuse"},
    {blkbuf_test_8, "Testing multithreaded behaviour"},
    {NULL, ""}
};

// To test usage by multiple threads

#define MAX_DUP_COUNT 1024

struct thread_params
{
    pthread_mutex_t     mutex;
    pthread_barrier_t   barrier;
    pthread_t           master_thread; // Thread responsible for checking the correctness of each operation
    uint32_t            dup_per_thread;
    uint32_t            num_threads;
    char              * orig_array;
    uint32_t            orig_array_size;
    blk_buf_t         * blk_buf;
    blk_buf_t         * empty_blk;
    uint32_t            num_total_blks;
    uint32_t            failed;
    char              * message;
    uint32_t            message_max;
};

void     * read_from_fd(void * arg);
void     * test_duplicated_blk_buf(void * arg);

// Helper functions

uint32_t countBlks(blk_buf_t * blk)
{
    uint32_t count = 0;

    while(blk != NULL)
    {
        count++;
        blk = blk->blk_data.data_blk_next;
    }

    return count;
}

uint32_t countFreeList(blk_buf_mgr_t * mgr)
{
    return countBlks(mgr->free_blk_list);
}

uint32_t checkParallelHeaders(blk_buf_t * blk)
{
    uint32_t    count = 1;

    blk_buf_t * previous = blk;
    blk_buf_t * blk_ptr  = blk->blk_header.list_left;

    while(blk_ptr != NULL)
    {
        count++;

        if(blk_ptr->blk_header.list_right != previous)
        {
            return 0;
        }

        previous = blk_ptr;
        blk_ptr  = blk_ptr->blk_header.list_left;
    }

    previous = blk;
    blk_ptr  = blk->blk_header.list_right;

    while(blk_ptr != NULL)
    {
        count++;

        if(blk_ptr->blk_header.list_left != previous)
        {
            return 0;
        }

        previous = blk_ptr;
        blk_ptr = blk_ptr->blk_header.list_right;
    }

    return count;
}


// Testing functions

// Testing blk buffer manager initialization & destruction

blkbuf_test_result blkbuf_test_1(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr;

    /****************************************************/

    // TEST 1.1 - Block buffer manager creation / destruction using internal allocation.

    mgr = blkbufMgrInit(NULL, (1024 + 1) * sizeof(blk_buf_t), sizeof(blk_buf_t));

    if(blkbufNumFreeBlocks(mgr) != 1024 || countFreeList(mgr) != 1024)
    {
        snprintf(message, message_max, "Failed test 1.1: Block buffer manager creation / destruction using internal allocation.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    /****************************************************/

    // TEST 1.2 - Block buffer manager creation / destruction using externally allocated buffer.

    uint64_t test_buffer[512];

    mgr = blkbufMgrInit(test_buffer, sizeof(test_buffer), sizeof(blk_buf_t));

    if(blkbufNumFreeBlocks(mgr) != (sizeof(test_buffer) / sizeof(blk_buf_t)) - 1 || countFreeList(mgr) != (sizeof(test_buffer) / sizeof(blk_buf_t)) - 1 )
    {
        snprintf(message, message_max, "Failed test 1.2: Block buffer manager creation / destruction using externally allocated buffer.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    /****************************************************/

    // TEST 1.3 - Block buffer using the minimum block size allowed

    const uint32_t head_overhead = sizeof(blk_buf_t); //head_data_offset;

    mgr = blkbufMgrInit(NULL, 1024, head_overhead);

    if(mgr == NULL)
    {
        snprintf(message, message_max, "Failed test 1.3: Block buffer using the minimum block size allowed.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    /****************************************************/

    // TEST 1.4 - Block buffer using the minimum block size and minimum number of blocks.

    mgr = blkbufMgrInit(NULL, head_overhead , head_overhead);

    if(mgr == NULL)
    {
        snprintf(message, message_max, "Failed test 1.4: Block buffer using the minimum block size and minimum number of blocks.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    /****************************************************/

    return OK;
}

// Testing blk buffer structure manipulation

blkbuf_test_result blkbuf_test_2(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, (1024 + 1) * sizeof(blk_buf_t), sizeof(blk_buf_t));

    /****************************************************/

    // TEST 2.1 - Check number of free blocks after removing one.

    blk_buf_t * blk = blkbufInit(mgr);

    if(blkbufNumFreeBlocks(mgr) != 1023 || countFreeList(mgr) != 1023)
    {
        snprintf(message, message_max, "Failed test 2.1: Check number of free blocks after removing one.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.2 - Check number of blocks in empty block buffer.

    if(blkbufNumBlocks(blk) != 1 || countBlks(blk) != 1)
    {
        snprintf(message, message_max, "Failed test 2.2: Check number of blocks in empty block buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.3 - Fill one block completely, and stop before a new free block is used. Check if number of blocks is still one.

    uint32_t head_size_data_blk = offsetof(blk_buf_t, blk_data.data.u8);
    uint32_t empty_data_in_blk  = blkbufBlockSize(blk) - head_size_data_blk;

    for(uint32_t i = 0; i < empty_data_in_blk; i++)
    {
        blkbufAppendBin(blk, "+", 1);
    }

    if(blkbufNumBlocks(blk) != 2 || countBlks(blk) != 2)
    {
        snprintf(message, message_max, "Failed test 2.3: Fill one block completely, and stop before a new free block is used. Check if number of blocks is still one.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.4 - The appending of a new character should trigger the usage of a new block.

    blkbufAppendBin(blk, "+", 1);

    if(blkbufNumBlocks(blk) != 3 || countBlks(blk) != 3)
    {
        snprintf(message, message_max, "Failed test 2.4: The appending of a new character should trigger the usage of a new block.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.5 - Fill the second block completely, and stop before a new free block is used. Check if number of blocks is two.

    for(uint32_t i = 1; i < empty_data_in_blk; i++)
    {
        blkbufAppendStr(blk, "+");
    }

    if(blkbufNumBlocks(blk) != 3 || countBlks(blk) != 3)
    {
        snprintf(message, message_max, "Failed test 2.5: Fill the second block completely, and stop before a new free block is used. Check if number of blocks is two.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.6 - The appending of a new character should trigger the usage of a third data block.

    blkbufAppendStr(blk, "+");

    if(blkbufNumBlocks(blk) != 4 || countBlks(blk) != 4)
    {
        snprintf(message, message_max, "Failed test 2.6: The appending of a new character should trigger the usage of a third data block.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.7 - Check if the 3 data blocks were removed from block buffer manager.

    if(blkbufNumFreeBlocks(mgr) != 1020 || countFreeList(mgr) != 1020)
    {
        snprintf(message, message_max, "Failed test 2.7: Check if the 3 data blocks were removed from block buffer manager.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.8 - Check if block buffer can be correctly destroyed, and all blocks returned to free block list.

    blkbufDestroy(blk);

    if(blkbufNumFreeBlocks(mgr) != 1024 || countFreeList(mgr) != 1024)
    {
        snprintf(message, message_max, "Failed test 2.8: Check if block buffer can be correctly destroyed, and all blocks returned to free block list.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.9 - Check minimum number of free blocks reached, after returning 4 blocks

    if(blkbufNumMinBlocks(mgr) != 1020)
    {
        snprintf(message, message_max, "Failed test 2.9: Check minimum number of free blocks reached, after returning 4 blocks.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.10 - Check maximum data used, after returning 4 blocks

    if(fabs(blkbufMaxDataUsed(mgr) - (5.0 / (1024.0 + 1.0))) > 0.000000001)
    {
        snprintf(message, message_max, "Failed test 2.10: Check maximum data percentage used, after returning 4 blocks.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.11 - Fill the block buffer to its maximum capacity! This requires the usage of all free blocks.

    blk = blkbufInit(mgr);

    for(uint32_t j = 1; j < 1024; j++)
    {
        for(uint32_t i = 0; i < empty_data_in_blk; i++)
        {
            if(i % 2)
            {
                blkbufAppendStr(blk, "+");
            }
            else
            {
                blkbufAppendBin(blk, "+", 1);
            }
        }
    }

    if(blkbufNumBlocks(blk) != 1024 || countBlks(blk) != 1024)
    {
        snprintf(message, message_max, "Failed test 2.11: Fill the block buffer to its maximum capacity! This requires the usage of all free blocks.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.12 - There should be no more free blocks available.

    if(blkbufNumFreeBlocks(mgr) != 0 || countFreeList(mgr) != 0)
    {
        snprintf(message, message_max, "Failed test 2.12: There should be no more free blocks available.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.13 - It should not be possible to add a new byte/character, because block buffer is full and there are no extra free blocks.

    if(blkbufAppendBin(blk, "+", 1) >= 0 || blkbufAppendStr(blk, "+") >= 0)
    {
        snprintf(message, message_max, "Failed test 2.13: It should not be possible to add a new character, because block buffer is full and there are no extra free blocks.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.14 - It should be possible to append zero bytes/characters, every time.

    if(blkbufAppendBin(blk, "", 0) != 0 || blkbufAppendStr(blk, "") != 0)
    {
        snprintf(message, message_max, "Failed test 2.14: It should be possible to append zero characters, every time.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 2.15 - Test removal of a void block

    char * array = (char *) blkbufGetVoidBlk(mgr);

    if(array == NULL || blkbufNumFreeBlocks(mgr) != 1023 || countFreeList(mgr) != 1023)
    {
        snprintf(message, message_max, "Failed test 2.15: Failed to get a void block correctly.");
        return FAILURE;
    }

    /****************************************************/    

    // TEST 2.16 - Test return of a void block

    blkbufReturnVoidBlk(mgr, blk);

    if(blkbufNumFreeBlocks(mgr) != 1024 || countFreeList(mgr) != 1024)
    {
        snprintf(message, message_max, "Failed test 2.16: Failed to return a void block correctly.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.17 - Fill the block buffer to its maximum capacity! Then write a character and verify if previous data was freed

    blk = blkbufInit(mgr);

    for(uint32_t j = 1; j < 1024; j++)
    {
        for(uint32_t i = 0; i < empty_data_in_blk; i++)
        {
            blkbufAppendBin(blk, "+", 1);
        }
    }

    blkbufReplaceBin(blk, "+", 1);

    if(blkbufNumBlocks(blk) != 2 || countBlks(blk) != 2)
    {
        snprintf(message, message_max, "Failed test 2.17: Fill the block buffer to its maximum capacity! Then write a character and verify if previous data was freed.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 2.18 - Fill the block buffer to its maximum capacity! Then write zero characters and verify if all data was freed

    blk = blkbufInit(mgr);

    for(uint32_t j = 1; j < 1024; j++)
    {
        for(uint32_t i = 0; i < empty_data_in_blk; i++)
        {
            blkbufAppendBin(blk, "+", 1);
        }
    }

    blkbufReplaceStr(blk, "");

    if(blkbufNumBlocks(blk) != 1 || countBlks(blk) != 1)
    {
        snprintf(message, message_max, "Failed test 2.18: Fill the block buffer to its maximum capacity! Then write zero characters and verify if all data was freed.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 2.19 - Write several strings to blk buffer, and verify used blocks at end

    blk = blkbufInit(mgr);

    uint32_t avail_size = sizeof(blk_buf_t) - sizeof(blk_buf_t*);

    for(uint32_t i = 1; i < avail_size * 64; i++)
    {
        blkbufReplaceBin(blk, template, i);
    }

    if(blkbufNumBlocks(blk) != (64 + 1) || blkbufNumFreeBlocks(mgr) != (1024 - 64 - 1) || countFreeList(mgr) != (1024 - 64 - 1))
    {
        snprintf(message, message_max, "Failed test 2.19: Write several strings to blk buffer, and verify used blocks at end.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 2.20 - Check minimum number of free blocks reached

    if(blkbufNumMinBlocks(mgr) != 0)
    {
        snprintf(message, message_max, "Failed test 2.20: Check minimum number of free blocks reached.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 2.21 - Check maximum data used

    if(fabs(blkbufMaxDataUsed(mgr) - 1.0) > 0.000000001)
    {
        snprintf(message, message_max, "Failed test 2.21: Check maximum data percentage used.");
        return FAILURE;
    }

    /****************************************************/

    blkbufMgrDestroy(mgr);

    return OK;
}

// Testing append/replace and copy functions

blkbuf_test_result blkbuf_test_3(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, 1000000, sizeof(blk_buf_t));

    blk_buf_t * blk = blkbufInit(mgr);

    uint32_t counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        blkbufAppendBin(blk, template, i);
        memcpy(&buffer_1[counter], template, i);

        counter += i;
    }

    // TEST 3.1 - Check if correct number of bytes was copied to the output buffer.

    if(blkbufCopyBin(buffer_2, blk, sizeof(buffer_2)) != counter)
    {
        snprintf(message, message_max, "Failed test 3.1: Check if correct number of bytes was copied to the output buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 3.2 - Validate data obtained from block buffer.

    if(memcmp(buffer_1, buffer_2, counter) != 0)
    {
        snprintf(message, message_max, "Failed test 3.2: Validate data obtained from block buffer.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        memcpy(buffer_2, template, i);
        buffer_2[i] = '\0';

        blkbufAppendStr(blk, buffer_2);
        memcpy(&buffer_1[counter], template, i);

        counter += i;

        buffer_1[counter] = '\0';
    }

    // TEST 3.3 - Check if correct number of characters was copied to the output buffer.

    if(blkbufCopyStr(buffer_2, blk, sizeof(buffer_2)) != counter)
    {
        snprintf(message, message_max, "Failed test 3.3: Check if correct number of characters was copied to the output buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 3.4 - Validate string obtained from block buffer.

    if(strcmp(buffer_1, buffer_2) != 0)
    {
        snprintf(message, message_max, "Failed test 3.4: Validate string obtained from block buffer.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        if(i % 2)
        {
            memcpy(buffer_2, template, i);
            buffer_2[i] = '\0';
            blkbufAppendStr(blk, buffer_2);
        }
        else
        {
            blkbufAppendBin(blk, template, i);
        }

        memcpy(&buffer_1[counter], template, i);

        counter += i;

        buffer_1[counter] = '\0';
    }

    // TEST 3.5 - Check if correct number of bytes & characters was copied to the output buffer.

    if(blkbufCopyBin(buffer_2, blk, sizeof(buffer_2)) != counter)
    {
        snprintf(message, message_max, "Failed test 3.5: Check if correct number of bytes & characters was copied to the output buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 3.6 - Validate data obtained from block buffer.

    if(memcmp(buffer_1, buffer_2, counter) != 0)
    {
        snprintf(message, message_max, "Failed test 3.6: Validate data obtained from block buffer.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    for(uint32_t i = 0; i <= 1024; i++)
    {
        blkbufReplaceBin(blk, template, i);
    }

    memcpy(buffer_1, template, 1024);

    // TEST 3.7 - Check if correct number of bytes was copied to the output buffer.

    if(blkbufCopyBin(buffer_2, blk, sizeof(buffer_2)) != 1024)
    {
        snprintf(message, message_max, "Failed test 3.7: Check if correct number of bytes was copied to the output buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 3.8 - Validate data obtained from block buffer.

    if(memcmp(buffer_1, buffer_2, 1024) != 0)
    {
        snprintf(message, message_max, "Failed test 3.8: Validate data obtained from block buffer.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    for(uint32_t i = 0; i <= 1024; i++)
    {
        memcpy(buffer_2, template, i);
        buffer_2[i] = '\0';

        blkbufReplaceStr(blk, buffer_2);
    }

    memcpy(buffer_1, template, 1024);
    buffer_1[1024] = '\0';

    // TEST 3.9 - Check if correct number of characters was copied to the output buffer.

    if(blkbufCopyStr(buffer_2, blk, sizeof(buffer_2)) != 1024)
    {
        snprintf(message, message_max, "Failed test 3.9: Check if correct number of characters was copied to the output buffer.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 3.10 - Validate string obtained from block buffer.

    if(strcmp(buffer_1, buffer_2) != 0)
    {
        snprintf(message, message_max, "Failed test 3.10: Validate string obtained from block buffer.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 3.11 - Test out of bounds writing on output buffer!

    blk = blkbufInit(mgr);

    // Fill with known values

    for(uint32_t i = 0; i < 10000; i++)
    {
        buffer_1[i] = i & 0xFF;
        buffer_2[i] = i & 0xFF;
    }

    blkbufAppendStr(blk, template);

    blkbufCopyBin(buffer_1, blk, 4);
    memcpy(buffer_2, template, 4);

    if(memcmp(buffer_1, buffer_2, 10000) != 0)
    {
        snprintf(message, message_max, "Failed test 3.11: Out of bounds writing.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blkbufMgrDestroy(mgr);

    return OK;
}

// Testing copy functions with iterator

blkbuf_test_result blkbuf_test_4(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, (10000 + 1) * sizeof(blk_buf_t), sizeof(blk_buf_t));

    /****************************************************/

    // TEST 4.1 - Check if iterator was correctly initialized from block buffer manager.
    
    blk_buf_t * blk = blkbufInit(mgr);   
    blkbufIteratorStart(blk);

    if(blkbufNumFreeBlocks(mgr) != 9999)
    {
        snprintf(message, message_max, "Failed test 4.1: Check if iterator was correctly initialized from block buffer manager.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);
    blkbufIteratorStart(blk);

    uint32_t counter      = 0;
    uint32_t iter_counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        blkbufAppendBin(blk, template, i);
        memcpy(&buffer_1[counter], template, i);

        counter += i;
    }

    // TEST 4.2 - Check if remaining number of bytes is correct during iteration.

    for(uint32_t i = 0; i < 1024 + 256; i++)
    {
        if(blkbufIteratorRemaining(blk) != (counter - iter_counter))
        {
            snprintf(message, message_max, "Failed test 4.2: Check if remaining number of bytes is correct during iteration.");
            return FAILURE;
        }

        iter_counter += blkbufIterateBin(&buffer_2[iter_counter], blk, i);
    }

    // TEST 4.3 - Check if correct number of bytes was copied to the output buffer using iterator.

    if(iter_counter != counter)
    {
        snprintf(message, message_max, "Failed test 4.3: Check if correct number of bytes was copied to the output buffer using iterator.");
        return FAILURE;
    }

    // TEST 4.4 - Validate data obtained from block buffer.

    if(memcmp(buffer_1, buffer_2, counter) != 0)
    {
        snprintf(message, message_max, "Failed test 4.4: Validate data obtained from block buffer using iterator.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);
    blkbufIteratorStart(blk);

    counter      = 0;
    iter_counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        memcpy(buffer_2, template, i);
        buffer_2[i] = '\0';

        blkbufAppendStr(blk, buffer_2);
        memcpy(&buffer_1[counter], template, i);

        counter += i;
    }

    // TEST 4.5 - Check if remaining number of characters is correct during iteration.

    for(uint32_t i = 0; i < 1024 + 256; i++)
    {
        if(blkbufIteratorRemaining(blk) != (counter - iter_counter))
        {
            snprintf(message, message_max, "Failed test 4.5: Check if remaining number of characters is correct during iteration.");
            return FAILURE;
        }

        iter_counter += blkbufIterateStr(&buffer_2[iter_counter], blk, i);
    }

    // TEST 4.6 - Check if correct number of characters was copied to the output buffer using iterator.

    if(iter_counter != counter)
    {
        snprintf(message, message_max, "Failed test 4.6: Check if correct number of characters was copied to the output buffer using iterator.");
        return FAILURE;
    }

    // TEST 4.7 - Validate string obtained from block buffer using iterator.

    if(strcmp(buffer_1, buffer_2) != 0)
    {
        snprintf(message, message_max, "Failed test 4.7: Validate string obtained from block buffer using iterator.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 4.8 - Test out of bounds iteration!

    blk = blkbufInit(mgr);
    blkbufIteratorStart(blk);

    // Fill with known values

    for(uint32_t i = 0; i < 10000; i++)
    {
        buffer_1[i] = i & 0xFF;
        buffer_2[i] = i & 0xFF;
    }

    blkbufAppendStr(blk, template);
    blkbufIterateBin(buffer_1, blk, 4);
    memcpy(buffer_2, template, 4);

    if(memcmp(buffer_1, buffer_2, 10000) != 0)
    {
        snprintf(message, message_max, "Failed test 4.8: Out of bounds iteration.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 4.9 - Test out of bounds iteration one by one!

    blk = blkbufInit(mgr);
    blkbufIteratorStart(blk);

    blkbufAppendStr(blk, template);

    // Fill with known values

    for(uint32_t i = 0; i < 10000; i++)
    {
        buffer_1[i] = i & 0xFF;
        buffer_2[i] = i & 0xFF;
    }

    for(uint32_t i = 0; i < 5000; i++)
    {
        // Should stop at end of iteration and do not write any other values

        blkbufIterateBin(buffer_1 + i, blk, 1);
    }

    memcpy(buffer_2, template, strlen(template));

    if(memcmp(buffer_1, buffer_2, 10000) != 0)
    {
        snprintf(message, message_max, "Failed test 4.9: Out of bounds iteration one by one.");
        return FAILURE;
    }

    /****************************************************/

    // TEST 4.10 - Test out of bounds iteration one by one for a second time, using same blk_buf!

    blkbufIteratorStart(blk);

    // Fill with known values

    for(uint32_t i = 0; i < 10000; i++)
    {
        buffer_1[i] = i & 0xFF;
        buffer_2[i] = i & 0xFF;
    }

    for(uint32_t i = 0; i < 5000; i++)
    {
        // Should stop at end of iteration and do not write any other values

        blkbufIterateBin(buffer_1 + i, blk, 1);
    }

    memcpy(buffer_2, template, strlen(template));


    if(memcmp(buffer_1, buffer_2, 10000) != 0)
    {
        snprintf(message, message_max, "Failed test 4.10: Out of bounds iteration one by one for the second time, using same blk_buf.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    // TEST 4.11 - Check if iterator was correctly returned to block buffer manager.

    if(blkbufNumFreeBlocks(mgr) != 10000)
    {
        snprintf(message, message_max, "Failed test 4.11: Check if iterator was correctly returned to block buffer manager.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    return OK;
}

// Testing file writing functions

blkbuf_test_result blkbuf_test_5(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, 1000000, sizeof(blk_buf_t));

    int p[2];
    pthread_t pthread;

    FILE * f;

    if(pipe(p) != 0)
    {
        snprintf(message, message_max, "Failed test 5.X: Pipe cration failed.");
        return FAILURE;
    }

    t_read_fd = p[0];

    /****************************************************/

    blk_buf_t * blk = blkbufInit(mgr);

    t_close = 0;

    pthread_create(&pthread, NULL, read_from_fd, NULL);

    blkbufAppendStr(blk, template);
    if(blkbufWriteToFd(p[1], blk) < 0)
    {
        snprintf(message, message_max, "Failed test 5.1: Writting to file descriptor failed. Data in file descriptor may be corrupted as a result.");
        return FAILURE; 
    }

    t_close = 1;

    pthread_join(pthread, NULL);

    // TEST 5.1 - Check if string was correctly written to file descriptor.

    if(strcmp(buffer_1, template) != 0)
    {
        snprintf(message, message_max, "Failed test 5.1: Check if string was correctly written to file descriptor.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    t_close = 0;

    pthread_create(&pthread, NULL, read_from_fd, NULL);

    blkbufAppendBin(blk, template, sizeof(template));
    if(blkbufWriteToFd(p[1], blk) < 0)
    {
        snprintf(message, message_max, "Failed test 5.2: Writting to file descriptor failed. Data in file descriptor may be corrupted as a result.");
        return FAILURE; 
    }

    t_close = 1;

    pthread_join(pthread, NULL);

    // TEST 5.2 - Check if binary data was correctly written to file descriptor.

    if(memcmp(buffer_1, template, sizeof(template)) != 0)
    {
        snprintf(message, message_max, "Failed test 5.2: Check if binary data was correctly written to file descriptor.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    f = fopen("temporary_test_file_1", "w+");

    blkbufAppendStr(blk, template);
    blkbufWriteToFile(f, blk);

    fclose(f);

    f = fopen("temporary_test_file_1", "r");

    // TEST 5.3 -  Check if string was correctly written to file.

    if(fread(buffer_1, 1, sizeof(buffer_1), f) != (sizeof(template) - 1) || strcmp(buffer_1, template) != 0)
    {
        snprintf(message, message_max, "Failed test 5.3:  Check if string was correctly written to file.");
        return FAILURE;
    }

    fclose(f);
    remove("temporary_test_file_1");

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    f = fopen("temporary_test_file_2", "w+");

    blkbufAppendBin(blk, template, sizeof(template));
    blkbufWriteToFile(f, blk);

    fclose(f);

    f = fopen("temporary_test_file_2", "r");

    // TEST 5.4 - Check if binary data was correctly written to file.
    
    if(fread(buffer_1, 1, sizeof(buffer_1), f) != sizeof(template) || memcmp(buffer_1, template, sizeof(template)) != 0)
    {
        snprintf(message, message_max, "Failed test 5.4: Check if binary data was correctly written to file.");
        return FAILURE;
    }

    fclose(f);
    remove("temporary_test_file_2");

    blkbufDestroy(blk);

    /****************************************************/

    blkbufMgrDestroy(mgr);

    close(p[0]);
    close(p[1]);

    return OK;
}

// Testing block buffer printf

blkbuf_test_result blkbuf_test_6(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, 1000000, sizeof(blk_buf_t));

    /****************************************************/

    blk_buf_t * blk = blkbufInit(mgr);

    char buff_aux[] = "this is an array with a string \0 This part should not be printed";
    char * ptr_to_buff_aux = &buff_aux[strlen(buff_aux) + 1];
    int m_1, m_2;

    blkbufAppendPrintfBuf(blk, buffer_2, sizeof(buffer_2), "%d %.3f %-10u %-2s %3s %4s %.4s %-.5s %n", 1234, 1.23456, 12345, "k\r\n\0\n", buff_aux, ptr_to_buff_aux, ptr_to_buff_aux, "    \0 ", &m_1);
    sprintf(buffer_2,                                      "%d %.3f %-10u %-2s %3s %4s %.4s %-.5s %n", 1234, 1.23456, 12345, "k\r\n\0\n", buff_aux, ptr_to_buff_aux, ptr_to_buff_aux, "    \0 ", &m_2);

    blkbufCopyStr(buffer_1, blk, sizeof(buffer_1));

    // TEST 6.1 - Validate block buffer printf function.

    if(strcmp(buffer_1, buffer_2) != 0)
    {
        snprintf(message, message_max, "Failed test 6.1: Validate block buffer printf function.");
        return FAILURE;
    }

    // TEST 6.2 - Validate number of characters written.

    if(m_1 != m_2)
    {
        snprintf(message, message_max, "Failed test 6.2: Validate number of characters written.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blk = blkbufInit(mgr);

    blkbufAppendPrintf(blk, "%d %.3f %-10u %-2s %3s %4s %.4s %-.5s %n", 1234, 1.23456, 12345, "k\r\n\0\n", buff_aux, ptr_to_buff_aux, ptr_to_buff_aux, "    \0 ", &m_1);
    sprintf(buffer_2,       "%d %.3f %-10u %-2s %3s %4s %.4s %-.5s %n", 1234, 1.23456, 12345, "k\r\n\0\n", buff_aux, ptr_to_buff_aux, ptr_to_buff_aux, "    \0 ", &m_2);

    blkbufCopyStr(buffer_1, blk, sizeof(buffer_1));

    // TEST 6.3 - Validate block buffer printf function, with buffered input.

    if(strcmp(buffer_1, buffer_2) != 0)
    {
        snprintf(message, message_max, "Failed test 6.3: Validate block buffer printf function, with buffered input.");
        return FAILURE;
    }

    // TEST 6.4 - Validate number of characters written.

    if(m_1 != m_2)
    {
        snprintf(message, message_max, "Failed test 6.4: Validate number of characters written.");
        return FAILURE;
    }

    blkbufDestroy(blk);

    /****************************************************/

    blkbufMgrDestroy(mgr);

    return OK;
}

// Testing block buffer duplication and data reuse

blkbuf_test_result blkbuf_test_7(char * message, uint32_t message_max)
{
    blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, 10000000, sizeof(blk_buf_t));

    uint32_t total_blks  = blkbufNumFreeBlocks(mgr);
    uint32_t header_blks = 0;

    blk_buf_t * blk_1 = blkbufInit(mgr);

    uint32_t counter = 0;

    for(uint32_t i = 0; i < 1024; i++)
    {
        blkbufAppendBin(blk_1, template, i);
        memcpy(&buffer_1[counter], template, i);

        counter += i;
    }

    uint32_t data_blks_1 = blkbufNumBlocks(blk_1) - 1;
    header_blks++;
    
    blk_buf_t * blk_2 = blkbufDuplicate(blk_1);

    uint32_t data_blks_2 = blkbufNumBlocks(blk_2) - 1;
    header_blks++;

    // TEST 7.1 - Validate number of of blocks after duplication.

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_1 - header_blks) || data_blks_1 != data_blks_2)
    {
        snprintf(message, message_max, "Failed test 7.1: Number of blocks used after duplication.");
        return FAILURE;
    }

    // TEST 7.2 - Validate lenght of duplicated block buffer.

    if(blkbufCopyBin(buffer_2, blk_2, sizeof(buffer_2)) != counter)
    {
        snprintf(message, message_max, "Failed test 7.2: Length of duplicated blk buf.");
        return FAILURE;
    }

    // TEST 7.3 - Validate data of duplicated block buffer.

    if(memcmp(buffer_1, buffer_2, counter) != 0)
    {
        snprintf(message, message_max, "Failed test 7.3: Data in duplicated blk buf.");
        return FAILURE;
    }

    // TEST 7.4 - Test number of blocks consumed when 1 is changed.

    blkbufAppendBin(blk_1, template, 1);
    memcpy(&buffer_1[counter], template, 1);

    data_blks_1 = blkbufNumBlocks(blk_1) - 1;
    data_blks_2 = blkbufNumBlocks(blk_2) - 1;

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_1 - data_blks_2 - header_blks))
    {
        snprintf(message, message_max, "Failed test 7.4: Number of blocks used after writing to a duplicated block.");
        return FAILURE;
    }

    // TEST 7.5 - Compare size of new and old blocks.

    if(blkbufDataLen(blk_1) != blkbufDataLen(blk_2) + 1)
    {
        snprintf(message, message_max, "Failed test 7.5: New size of (previously) duplicated block.");
        return FAILURE;
    }

    // TEST 7.6 - Test different data size of (previously) duplicated blocks.

    if(blkbufCopyBin(buffer_2, blk_1, sizeof(buffer_2)) != counter + 1)
    {
        snprintf(message, message_max, "Failed test 7.6: Copy of (previously) duplicated block to array.");
        return FAILURE;
    }

    // TEST 7.7 - Validate data of duplicated block buffer.

    if(memcmp(buffer_1, buffer_2, counter + 1) != 0)
    {
        snprintf(message, message_max, "Failed test 7.7: Data in (previously) duplicated blk buf.");
        return FAILURE;
    }

    // TEST 7.8 - Validate number of of blocks after duplication.

    blk_buf_t * blk_3       = blkbufDuplicate(blk_2);
    uint32_t    data_blks_3 = blkbufNumBlocks(blk_3) - 1;
    header_blks++;

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_1 - data_blks_2 - header_blks) || data_blks_2 != data_blks_3)
    {
        snprintf(message, message_max, "Failed test 7.8: Number of blocks used after second duplication.");
        return FAILURE;
    }

    // TEST 7.9 - Validate lenght of duplicated block buffer.

    if(blkbufCopyBin(buffer_2, blk_3, sizeof(buffer_2)) != counter)
    {
        snprintf(message, message_max, "Failed test 7.9: Length of second duplicated blk buf.");
        return FAILURE;
    }

    // TEST 7.10 - Validate data of duplicated block buffer.

    if(memcmp(buffer_1, buffer_2, counter) != 0)
    {
        snprintf(message, message_max, "Failed test 7.10: Data in second duplicated blk buf.");
        return FAILURE;
    }

    // TEST 7.11 - Validate blocks used after writing ot duplicated blk.

    blk_buf_t * blk_4       = blkbufDuplicate(blk_2);
    header_blks++;

    blkbufReplaceBin(blk_4, template, 0);

    uint32_t    data_blks_4 = 0;

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_1 - data_blks_2 - data_blks_4 - header_blks))
    {
        snprintf(message, message_max, "Failed test 7.11: Validate blocks used after writing ot duplicated blk.");
        return FAILURE;
    }

    blkbufDestroy(blk_4);
    header_blks--;

    // TEST 7.12 - Validate data of duplicated block buffer.

    blkbufDestroy(blk_2);
    header_blks--;

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_1 - data_blks_2 - header_blks)) // Blocks still used for blk_3
    {
        snprintf(message, message_max, "Failed test 7.12: Number of blocks used after returning blk buf 2 after second duplication.");
        return FAILURE;
    }

    // TEST 7.13 - Validate data of duplicated block buffer.

    blkbufDestroy(blk_1);
    header_blks--;

    if(blkbufNumFreeBlocks(mgr) != (total_blks - data_blks_2 - header_blks)) 
    {
        snprintf(message, message_max, "Failed test 7.13: Number of blocks used after returning blk buf 1 after second duplication.");
        return FAILURE;
    }

    // TEST 7.14 - Validate data of duplicated block buffer.

    blkbufDestroy(blk_3);
    header_blks--;

    CC_ASSERT(header_blks == 0);

    if(blkbufNumFreeBlocks(mgr) != total_blks - header_blks)
    {
        snprintf(message, message_max, "Failed test 7.14: Number of blocks used after returning blk buf 3.");
        return FAILURE;
    }

    blkbufMgrDestroy(mgr);

    return OK;
}

// To test random usage of the block buffer library

void * read_from_fd(void * arg)
{
    static struct pollfd poll_opts;

    uint32_t buff_size  = sizeof(buffer_1);
    char   * buffer     = buffer_1;
    int      read_fd    = t_read_fd;
    uint32_t char_count = 0;

    poll_opts.fd      = t_read_fd;
    poll_opts.events  = POLLIN;
    poll_opts.revents = 0;

    do
    {
        poll(&poll_opts, 1, 100);

        if(poll_opts.revents & POLLIN)
        {
            poll_opts.revents = 0;

            if(char_count >= buff_size) break;

            char_count += read(read_fd, &buffer[char_count], buff_size - char_count);
        }

    }
    while(!t_close);

    if(char_count < buff_size)
    {
        buffer[char_count] = '\0';
    }

    return NULL;
}

#define NR_THREADS      64
#define NR_DUPS         128
#define NR_ITERATIONS   128

blkbuf_test_result blkbuf_test_8(char * message, uint32_t message_max)
{
    blk_buf_mgr_t *         mgr        = blkbufMgrInit(NULL, 100000000, sizeof(blk_buf_t));
    pthread_t               pthread[NR_THREADS];
    struct thread_params    params;

    /****************************************************/

    uint32_t    num_total_blks = blkbufNumFreeBlocks(mgr) + 1;

    blk_buf_t * blk       = blkbufInit(mgr);
    blk_buf_t * empty_blk = blkbufInit(mgr);

    blkbufAppendStr(blk, template);

    pthread_mutex_init(&params.mutex, NULL);
    pthread_barrier_init(&params.barrier, NULL, NR_THREADS);

    //params->master_thread;

    params.dup_per_thread  = NR_DUPS;
    params.num_threads     = NR_THREADS;

    params.blk_buf         = blk;
    params.empty_blk       = empty_blk;
    params.num_total_blks  = num_total_blks;

    params.failed          = 0;

    params.message         = message;
    params.message_max     = message_max;

    for(uint32_t i = 0; i < NR_ITERATIONS && params.failed == 0; i++)
    {
        for(uint32_t i = 0; i < NR_THREADS; i++)
        {
            pthread_create(&pthread[i], NULL, test_duplicated_blk_buf, (void *) &params);
        }

        for(uint32_t i = 0; i < NR_THREADS; i++)
        {
            pthread_join(pthread[i], NULL);
        }
    }

    if(params.failed)
    {
        return FAILURE;
    }
    else
    {
        blkbufDestroy(blk);
        blkbufDestroy(empty_blk);
        blkbufMgrDestroy(mgr);
        return OK;
    }

}

void * test_duplicated_blk_buf(void * arg)
{
    struct thread_params * params = (struct thread_params *) arg;

    CC_ASSERT(params->dup_per_thread <= MAX_DUP_COUNT);

    pthread_mutex_lock(&params->mutex);

    // The last thread to set this will be the master

    params->master_thread = pthread_self();

    pthread_mutex_unlock(&params->mutex);

    pthread_barrier_wait(&params->barrier);

    blk_buf_t     * blk[MAX_DUP_COUNT + 1];
    pthread_t       tid             = pthread_self();
    pthread_t       tid_master      = params->master_thread;
    blk_buf_mgr_t * blk_buf_mgr     = params->blk_buf->blk_header.blk_buf_mgr;
    uint32_t        num_dups        = params->dup_per_thread;
    uint32_t        num_threads     = params->num_threads;
    uint32_t        num_total_blks  = params->num_total_blks;
    uint32_t        num_free_blks   = params->num_total_blks - 2;
    char          * message         = params->message;
    uint32_t        message_max     = params->message_max;

    uint32_t        data_chain_blks;
    uint32_t        header_blks;
    uint32_t        max_blks_used   = 0;

    blk[0] = params->blk_buf;

    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = 1; i <= num_dups; i++)
    {
        blk[i] = blkbufDuplicate(blk[0]);
    }

    pthread_barrier_wait(&params->barrier);

    // TEST 8.1 - Validate total number of blocks used, by thread #0
    
    if(pthread_equal(tid, tid_master))
    {   
        data_chain_blks = blkbufNumBlocks(blk[0]) - 1;
        header_blks     = 1 + num_threads * num_dups;
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || checkParallelHeaders(blk[0]) != header_blks)
        {
            snprintf(message, message_max, "Failed test 8.1: Number of blocks after duplicating blk_buf.");
            params->failed = 1;
        }
    }
     
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = (num_dups / 2) + 1; i <= num_dups; i++)
    {
        blkbufDestroy(blk[i]);
    }

    pthread_barrier_wait(&params->barrier);

    // TEST 8.2 - Validate total number of blocks used after deletion, by thread #0
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = blkbufNumBlocks(blk[0]) - 1;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || checkParallelHeaders(blk[0]) != header_blks)
        {
            snprintf(message, message_max, "Failed test 8.2: Number of blocks after deletion of half of duplicated blk_buf.");
            params->failed = 1;
        }
    } 
    
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = 1; i <= num_dups / 2; i++)
    {
        blkbufAppendBin(blk[i], "", 0);
        blkbufAppendStr(blk[i], "");
    }
    
    pthread_barrier_wait(&params->barrier);

    // TEST 8.3 - Validate total number of blocks used after appending zero data, should be the same
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = blkbufNumBlocks(blk[0]) - 1;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks  || checkParallelHeaders(blk[0]) != header_blks)
        {
            snprintf(message, message_max, "Failed test 8.3: Number of blocks after appending zero data.");
            params->failed = 1;
        }
    }  
     
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    blkbufAppendBin(blk[1], "new data 1", 10);

    pthread_barrier_wait(&params->barrier);

    // TEST 8.4 - Validate total number of blocks used after appending 10 bytes data, should be higher since all the data blocks need to be replicated
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = (blkbufNumBlocks(blk[0]) - 1) + (blkbufNumBlocks(blk[1]) - 1) * num_threads;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || 
           checkParallelHeaders(blk[0]) != header_blks - num_threads                         || 
           checkParallelHeaders(blk[1]) != 1)
        {
            snprintf(message, message_max, "Failed test 8.4: Number of blocks after appending new binary data (requires a new copy of all data blks).");
            params->failed = 1;
        }
    }  
    
    
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    blkbufAppendStr(blk[2], "new data 2");

    pthread_barrier_wait(&params->barrier);

    // TEST 8.5 - Validate total number of blocks used after appending 10 characters, should be higher since all the data blocks need to be replicated
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = (blkbufNumBlocks(blk[0]) - 1) + (blkbufNumBlocks(blk[1]) - 1) * num_threads + (blkbufNumBlocks(blk[2]) - 1) * num_threads;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || 
           checkParallelHeaders(blk[0]) != header_blks - 2 * num_threads                     ||
           checkParallelHeaders(blk[2]) != 1)
        {
            snprintf(message, message_max, "Failed test 8.5: Number of blocks after appending new string data (requires a new copy of all data blks).");
            params->failed = 1;
        }
    }   
    
    
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    blkbufReplaceBin(blk[3], "new data 3", 10);

    pthread_barrier_wait(&params->barrier);

    // TEST 8.6 - Validate total number of blocks used after replacing data by 10 bytes. Only the header should have been removed
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = (blkbufNumBlocks(blk[0]) - 1) + (blkbufNumBlocks(blk[1]) - 1) * num_threads + (blkbufNumBlocks(blk[2]) - 1) * num_threads + 1 * num_threads;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || 
           checkParallelHeaders(blk[0]) != header_blks - 3 * num_threads                     || 
           checkParallelHeaders(blk[3]) != 1)
        {
            snprintf(message, message_max, "Failed test 8.6: Number of blocks after replacing binary data.");
            params->failed = 1;
        }
    }   

    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    blkbufReplaceStr(blk[4], "new data 4");

    pthread_barrier_wait(&params->barrier);

    // TEST 8.7 - Validate total number of blocks used after replacing tata by 10 characters. Only the header should have been removed
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = (blkbufNumBlocks(blk[0]) - 1) + (blkbufNumBlocks(blk[1]) - 1) * num_threads + (blkbufNumBlocks(blk[2]) - 1) * num_threads + 2 * num_threads;
        header_blks     = 1 + num_threads * (num_dups / 2);
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks || 
           checkParallelHeaders(blk[0]) != header_blks - 4 * num_threads                     || 
           checkParallelHeaders(blk[4]) != 1)
        {
            snprintf(message, message_max, "Failed test 8.7: Number of blocks after replacing string data.");
            params->failed = 1;
        }
    } 

    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = 1; i <= num_dups / 2; i++)
    {
        blkbufDestroy(blk[i]);
    }

    pthread_barrier_wait(&params->barrier);

    // TEST 8.8 - Validate total number of blocks used after deletion, by thread #0
    
    if(pthread_equal(tid, tid_master))
    {
        data_chain_blks = blkbufNumBlocks(blk[0]) - 1;
        header_blks     = 1;
        max_blks_used   = (data_chain_blks + header_blks) > max_blks_used ? data_chain_blks + header_blks : max_blks_used;

        if(blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - data_chain_blks)
        {
            snprintf(message, message_max, "Failed test 8.8: Number of blocks after destroying all duplicated blk_buf.");
            params->failed = 1;
        }
    } 

    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    // TEST 8.9 - Duplication of empty block buffer

    // Get the empty block buffer at #0

    blk[0] = params->empty_blk;

    if(pthread_equal(tid, tid_master))
    {
        blkbufReplaceStr(blk[0], "");
    }
    
    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = 1; i <= num_dups; i++)
    {
        blk[i] = blkbufDuplicate(blk[0]);
    }

    pthread_barrier_wait(&params->barrier);
    
    if(pthread_equal(tid, tid_master))
    {   
        // We still need to count with the params->blk_buf header blocks

        uint32_t other_data_blks = blkbufNumBlocks(params->blk_buf) - 1;
        data_chain_blks          = blkbufNumBlocks(blk[0]) - 1;
        header_blks              = 1 + num_threads * num_dups;
        max_blks_used            = (other_data_blks + header_blks) > max_blks_used ? other_data_blks + header_blks : max_blks_used;

        if(data_chain_blks != 0 || blkbufNumFreeBlocks(blk_buf_mgr) != num_free_blks - header_blks - other_data_blks || checkParallelHeaders(blk[0]) != header_blks)
        {
            snprintf(message, message_max, "Failed test 8.9: Duplication of empty block buffer.");
            params->failed = 1;
        }
    }
     
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    for(uint32_t i = 1; i <= num_dups; i++)
    {
        blkbufDestroy(blk[i]);
    }

    pthread_barrier_wait(&params->barrier);

    // TEST 8.10 - Validate largest percentage of blocks used during execution
    
    if(pthread_equal(tid, tid_master))
    {
        float f_total_blks    = num_total_blks;
        float f_max_blks_used = max_blks_used + 2;

        if(fabs(blkbufMaxDataUsed(blk_buf_mgr) - (f_max_blks_used / f_total_blks)) > 0.000000001)
        {
            snprintf(message, message_max, "Failed test 8.10: Validate largest percentage of blocks used during execution.");
            params->failed = 1;
        }
    }
    
    pthread_barrier_wait(&params->barrier);
    if(params->failed) pthread_exit(message);
    pthread_barrier_wait(&params->barrier);

    return NULL;
}

// EOF
