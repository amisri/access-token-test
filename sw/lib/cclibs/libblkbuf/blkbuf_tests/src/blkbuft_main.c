/**
 * @file  blkbuft_main.c
 * @brief Block Buffer library test program.
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 * 
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libblkbuf.h>
#include "libblkbuf/blkbuf.h"
#include "blkbuft_list.h"
#include "blkbuft_perf.h"

int main(int argc, char* argv[])
{

    char message[256];
    uint32_t i; 

    if(argc > 1 && strcmp(argv[1], "-p") == 0)
    {
        test_performance();
        return 0;
    }

    for(i = 0; tests[i].func != NULL; i++)
    {
        if(tests[i].func(message, sizeof(message)) == OK)
        {
            printf("ok%8d - %s\n", i + 1, tests[i].description);
        }
        else
        {
            printf("not ok%4d - %s\n", i + 1, message);
        }
    }
    
    printf("1..%d\n", i);

    return 0;
}

// EOF
