/**
 * @file  blkbuft_list.c
 * @brief Block Buffer library performance analysis.
 *
 * <h4> Contact </h4>
 *
 * cclibs-devs@cern.ch
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2018. This project is released under the GNU Lesser General
 * Public License version 3.
 * 
 * <h4> License </h4>
 *
 * This file is part of libblkbuf.
 *
 * libblkbuf is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libblkbuf.h>
#include "libblkbuf/blkbuf.h"
#include "blkbuft_perf.h"

#include <time.h>

#define BUF_SIZE 1<<28

char buffer_1[BUF_SIZE];
char buffer_2[BUF_SIZE];

void print_results(const uint32_t * string_len, const uint32_t * block_size, const uint32_t string_len_l, const uint32_t block_size_l, double ** time, const char * name);

void test_performance()
{
    /****************************************************/
    /****************************************************/

    printf("Analysing block buffer performance...\n");

    /****************************************************/

    const uint32_t string_len[] = {1<<5, 1<<10, 1<<15, 1<<20, 1<<25};
    const uint32_t block_size[] = {1<<6, 1<<8, 1<<10, 1<<12, 1<<14, 1<<16};

    const uint32_t string_len_l = sizeof(string_len)/sizeof(string_len[0]);
    const uint32_t block_size_l = sizeof(block_size)/sizeof(block_size[0]);

    double * time[ 2 ][ block_size_l ];

    struct timespec time_counter_1, time_counter_2, time_counter_3, time_counter_4;

    // 'buffer_1' contains junk data which can be used here

    for(uint32_t i = 0; i < block_size_l; i++)
    {
        time[0][i] = (double *) calloc (string_len_l, sizeof(double));
        time[1][i] = (double *) calloc (string_len_l, sizeof(double));
    }

    for(uint32_t i = 0; i < block_size_l; i++)
    {
        blk_buf_mgr_t * mgr = blkbufMgrInit(NULL, BUF_SIZE, block_size[i]);

        for(uint32_t j = 0; j < string_len_l; j++)
        {
            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time_counter_1);

            for(uint32_t k = 0;  k < 10; k++)
            {
                blk_buf_t * blk = blkbufInit(mgr);

                blkbufAppendBin(blk, buffer_1, string_len[j]);

                blkbufDestroy(blk);
            }

            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time_counter_2);

            blk_buf_t * blk = blkbufInit(mgr);

            blkbufAppendBin(blk, buffer_1, string_len[j]);

            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time_counter_3);

            for(uint32_t k = 0;  k < 10; k++)
            {
                blkbufCopyBin(buffer_2, blk, string_len[j]);
            }

            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time_counter_4);

            blkbufDestroy(blk);

            time[0][i][j] = 1000000000L * (time_counter_2.tv_sec - time_counter_1.tv_sec) + (time_counter_2.tv_nsec - time_counter_1.tv_nsec);
            time[1][i][j] = 1000000000L * (time_counter_4.tv_sec - time_counter_3.tv_sec) + (time_counter_4.tv_nsec - time_counter_3.tv_nsec);

            time[0][i][j] = (time[0][i][j] / string_len[j]) / 10.0;
            time[1][i][j] = (time[1][i][j] / string_len[j]) / 10.0;
        }

        blkbufMgrDestroy(mgr);
    }

    print_results(string_len, block_size, string_len_l, block_size_l, time[0], "Writing");
    print_results(string_len, block_size, string_len_l, block_size_l, time[1], "Reading");

    for(uint32_t i = 0; i < block_size_l; i++)
    {
        free(time[0][i]);
        free(time[1][i]);
    }
}

void print_results(const uint32_t * string_len, const uint32_t * block_size, const uint32_t string_len_l, const uint32_t block_size_l, double ** time, const char * name)
{
    printf("|%15s|", name);

    for(uint32_t j = 0; j < block_size_l; j++)
    {
        printf("%15d|", block_size[j]);
    }

    printf("\n");

    printf("|---------------|");

    for(uint32_t j = 0; j < block_size_l; j++)
    {
        printf("---------------|");
    }

    printf("\n");

    for(uint32_t i = 0; i < string_len_l; i++)
    {
        printf("|%15d|", string_len[i]);

        for(uint32_t j = 0; j < block_size_l; j++)
        {
            printf("%12f ns|", time[j][i]);
        }

        printf("\n");
    }

    printf("|---------------|");

    for(uint32_t j = 0; j < block_size_l; j++)
    {
        printf("---------------|");
    }

    printf("\n");
}

// EOF
