/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   blkbuft_perf.h
 * @brief  Block buffer library performance analysis
 * @author Joao Afonso
 */

#pragma once

#include "cclibs.h"

void test_performance();

//EOF
