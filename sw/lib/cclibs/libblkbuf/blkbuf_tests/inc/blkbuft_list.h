/*!
 * © Copyright CERN 2018. All rights not expressly granted are reserved.
 * @file   blkbuft_list.h
 * @brief  Block buffer library test functions
 * @author Joao Afonso
 */

#pragma once

#include "cclibs.h"


typedef enum _blkbuf_test_result
{
    OK,
    FAILURE
} blkbuf_test_result;

struct blkbuf_test {
    blkbuf_test_result (* func) (char *, uint32_t);
    char description[256];
};

extern struct blkbuf_test tests[];

//EOF
