//! @file  evtlogInit.c
//! @brief Converter Control Event Logging library initialization functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!
//! <h4>Notes</h4>
//!
//! The event log data for a device contains four sections:
//!
//!     +------------------------------------------------------------------------------------------------------------+
//!     | struct EVTLOG_device    evtlog_device                                                                      |
//!     |                                                                                                            |
//!     | Header structure for the device containing pointers to the three other sections and the newest record      |
//!     | indexes for the normal and frequent events buffers.                                                        |
//!     +------------------------------------------------------------------------------------------------------------+
//!     | struct EVTLOG_prop_data prop_data[num_properties]                                                          |
//!     |                                                                                                            |
//!     | Array of property data structures. Contains the pointers to the property values and the last value of the  |
//!     | properties.                                                                                                |                                                                              |
//!     |                                                                                                            |
//!     | For bit masks, the last_value is initialized to zero, so all initially set bits will be reported.          |
//!     | All other properties will have last_value set to 0xFFFFFFFF to force an initial entry in the event log.    |
//!     +------------------------------------------------------------------------------------------------------------+
//!     | struct EVTLOG_last_evt  last_evt[num_last_evt]                                                             |
//!     |                                                                                                            |
//!     | Array of last event structures. Contains the event time stamp, iterations since last event, frequent state |
//!     | and filtered flags for the last event.                                                                     |
//!     |                                                                                                            |
//!     | The last_evt data is cleared during initialization.                                                        |
//!     +------------------------------------------------------------------------------------------------------------+
//!     | struct EVTLOG_record    record[num_records]                                                                |
//!     |                                                                                                            |
//!     | Array of event log record structures. The event log has num_freq_event_records for the frequent events at  |
//!     | the start of the event log. They are followed by the normal event log records. If the event log records    |
//!     | are recovered when the system resets, then the library can preserve the old records.                       |
//!     +------------------------------------------------------------------------------------------------------------+

#include "libevtlog.h"

// Constants

static uint32_t const magic_number        = 0xCAFEF00D;       //!< Magic number to validate device data
static uint32_t const valid_time_window_s = 3000000;          //!< To be valid, an event log record's time stamp
                                                              //!< must be within this time window of the present


//! Scans an event log circular buffer (normal or frequent) to find the record index of the newest valid record.
//!
//! A record is considered valid if the time stamp lies in the past, compared to unix_time, but is not
//! older than valid_time_window_s. The function cancels all invalid records by setting their
//! time stamps to zero.
//!
//! @param[in]      evtlog_device            Pointer to the event log device structure.
//! @param[in]      unix_time                Current Unix time.
//! @param[in]      start_index              Index of start of the circular buffer.
//! @param[in]      end_index                Index of the end of the circular buffer.
//! @returns        Index of the newest valid record, or zero if no valid records present.

static uint16_t evtlogInitRecordIndex(struct EVTLOG_device * const evtlog_device,
                                      uint32_t               const unix_time,
                                      uint16_t               const start_index,
                                      uint16_t               const end_index)
{
    struct EVTLOG_record * const evtlog                   = evtlog_device->record;
    uint32_t               const oldest_valid_unix_time   = unix_time - valid_time_window_s;
    uint16_t                     newest_record_index      = 0;
    struct CC_us_time            newest_record_time_stamp = { .secs.abs = 0 };

    // Scan all record in the given range, looking for the record with the newest valid time stamp

    uint16_t record_index;

    for(record_index = start_index ; record_index < end_index ; record_index++)
    {
        // Convert record Unix time to host byte order

        struct CC_us_time record_time_stamp = { .secs.abs = ntohl(evtlog[record_index].time_stamp.secs.abs) };

        if(record_time_stamp.secs.abs != 0 && record_time_stamp.secs.abs >= oldest_valid_unix_time && record_time_stamp.secs.abs <= unix_time)
        {
            // Convert record microsecond time to host byte order

            record_time_stamp.us = ntohl(evtlog[record_index].time_stamp.us);

            if(record_time_stamp.secs.abs  > newest_record_time_stamp.secs.abs ||
              (record_time_stamp.secs.abs == newest_record_time_stamp.secs.abs && record_time_stamp.us > newest_record_time_stamp.us))
            {
                // Record is the newest seen so register the index and time stamp

                newest_record_index      = record_index;
                newest_record_time_stamp = record_time_stamp;
            }
        }
        else
        {
            // Record time stamp is not valid so cancel the record by reseting the time stamp Unix time field

            evtlog[record_index].time_stamp.secs.abs = 0;
        }
    }

    // Return the index of the newest record

    return newest_record_index;
}



void evtlogInitDevice(struct EVTLOG_device * const evtlog_device, uint32_t const unix_time, uint32_t const device_index)
{
    // Calculate the sizes of the different sections

    size_t const sizeof_device    = sizeof(struct EVTLOG_device);
    size_t const sizeof_prop_data = sizeof(struct EVTLOG_prop_data) * evtlog_def.num_properties;
    size_t const sizeof_last_evt  = sizeof(struct EVTLOG_last_evt)  * evtlog_def.num_last_evt;
    size_t const sizeof_evtlog    = sizeof(struct EVTLOG_record)    * evtlog_def.num_records;

    // Calculate addresses of the different sections

    struct EVTLOG_prop_data * const prop_data = (struct EVTLOG_prop_data *)((char *)evtlog_device + sizeof_device);
    struct EVTLOG_last_evt  * const last_evt  = (struct EVTLOG_last_evt  *)((char *)evtlog_device + sizeof_device + sizeof_prop_data);
    struct EVTLOG_record    * const record    = (struct EVTLOG_record    *)((char *)evtlog_device + sizeof_device + sizeof_prop_data + sizeof_last_evt);

    // If log is not valid, or device data version is mismatched, or a valid Unix time is not supplied

    if(evtlog_device->magic   != magic_number   ||
       evtlog_device->version != EVTLOG_VERSION ||
       unix_time < valid_time_window_s)
    {
        // Reset the complete data buffer

        memset(evtlog_device, 0, sizeof_device + sizeof_prop_data + sizeof_last_evt + sizeof_evtlog);

        evtlog_device->newest_record_index = evtlog_def.num_freq_event_records;
    }
    else
    {
        // Find newest valid frequent event record - the frequent events are stored at the start of the event log

        evtlog_device->newest_freq_record_index = evtlogInitRecordIndex(evtlog_device,
                                                                        unix_time,
                                                                        0,                                    // start record index
                                                                        evtlog_def.num_freq_event_records);   // end record index

        // Find newest valid normal event record - the normal events are stored after the frequent event records

        evtlog_device->newest_record_index = evtlogInitRecordIndex(evtlog_device,
                                                                   unix_time,
                                                                   evtlog_def.num_freq_event_records,         // start record index
                                                                   evtlog_def.num_records);                   // end record index

        // Only reset the last_evt data

        memset(last_evt, 0, sizeof_last_evt);
    }

    // Initialize the last_value data - bit masks start at zero and all other values start at 0xFFFFFFFF

    uint32_t prop_index;

    for(prop_index = 0 ; prop_index < evtlog_def.num_properties ; prop_index++)
    {
        prop_data[prop_index].last_value = (evtlog_def.property[prop_index].format == EVTLOG_BITMASK) ? 0x00000000 : 0xFFFFFFFF;
    }

    // Set the remaining device data fields

    evtlog_device->version      = EVTLOG_VERSION;
    evtlog_device->device_index = device_index;
    evtlog_device->prop_data    = prop_data;
    evtlog_device->last_evt     = last_evt;
    evtlog_device->record       = record;
    evtlog_device->magic        = magic_number;
}

// EOF
