//! @file  evtlogRead.c
//! @brief Converter Control Event Logging library read functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libevtlog.h"

//! Transfer event log field to output string for CSV format.
//!
//! This function will prepare the field to be part of a CSV record. This requires the use of double quotes in case
//! field contains commas, so double quotes in the field must be converted to single quotes.
//!
//! @param[in]      output                   Pointer to string to receive event log field without terminating nul
//! @param[in]      input                    Pointer to field in event log to transfer to output
//! @param[in]      max_size                 Maximum size of the input field. The field may be nul terminated
//!                                          if it is shorter than the maximum size, but the nul will be missing
//!                                          if the field is full.
//! @returns        Pointer to character in output following the last character copied to output.

static inline char * evtlogReadPrintCsvField(char * output, char const * input, uint32_t max_size)
{
    char ch;

    while(max_size-- && (ch = *(input++)) != 0)
    {
        if(ch == '"')
        {
            ch = '\'';
        }

        *(output++) = ch;
    }

    return output;
}



//! Transfer event log field to output string for FGC format.
//!
//! This function will prepare the field to be part of a FGC response. The FGC protocol uses commas as record separators
//! and colons as field separators. So this function will convert commas to single quotes and colons to pipes.
//!
//! @param[in]      output                   Pointer to string to receive event log field without terminating nul
//! @param[in]      input                    Pointer to field in event log to transfer to output
//! @param[in]      max_size                 Maximum size of the input field. The field may be nul terminated
//!                                          if it is shorter than the maximum size, but the nul will be missing
//!                                          if the field is full.
//! @returns        Pointer to character in output following the last character copied to output.

static inline char * evtlogReadPrintFgcField(char * output, char const * input, uint32_t max_size)
{
    char ch;

    while(max_size-- && (ch = *(input++)) != 0)
    {
        // Copy input to output replacing command with single quote and colon with pipe

        switch(ch)
        {
            case ',': ch = '\'';    break;
            case ':': ch = '|';     break;
        }

        *(output++) = ch;
    }

    return output;
}



bool evtlogReadCsv(struct EVTLOG_device * const evtlog_device,
                   uint16_t             * const record_index,
                   char                         read_buf[EVTLOG_READ_CSV_BUF_LEN])
{
    struct EVTLOG_record * const evtlog = evtlog_device->record;

    // Search for the next valid event log record to read out

    uint16_t index;

    while((index = *record_index) < evtlog_def.num_records)
    {
        // Advance record index to the next record

        (*record_index)++;

        // If the time stamp of the current record is valid

        if(evtlog[index].time_stamp.secs.abs > 0)
        {
            // Take a copy of the complete current record

            struct EVTLOG_record const record = evtlog[index];

            // Memory barrier to ensure this is done before re-copying the time stamp

            CC_MEMORY_BARRIER;

            struct CC_us_time const time_stamp = evtlog[index].time_stamp;

            // Check that both copies of the time stamp match and are valid

            if(time_stamp.secs.abs > 0 && time_stamp.secs.abs == record.time_stamp.secs.abs && time_stamp.us == record.time_stamp.us)
            {
                char * buf = read_buf;

                // Format the record as a string: Unixtime.Ustime,Property,"Value",Action,Status
                // Value can contain commas so it must be in double quotes.
                // Double quotes in any field will be converted to single quotes.

                buf += sprintf(read_buf, "%u.%06u,", (unsigned int)ntohl(record.time_stamp.secs.abs), (unsigned int)ntohl(record.time_stamp.us));

                buf = evtlogReadPrintCsvField(buf, record.property, EVTLOG_PROPERTY_LEN);

                *(buf++) = ',';
                *(buf++) = '"';

                buf = evtlogReadPrintCsvField(buf, record.value, EVTLOG_VALUE_LEN);

                *(buf++) = '"';
                *(buf++) = ',';

                buf = evtlogReadPrintCsvField(buf, record.action, EVTLOG_ACTION_LEN);

                *(buf++) = ',';
                *(buf++) = record.status;
                *(buf++) = '\0';

                // Report success

                return true;
            }
        }
    }

    // No more valid records available so return false

    return false;
}



uint32_t evtlogReadFgc(struct EVTLOG_device * const evtlog_device,
                       uint16_t             * const record_index,
                       char                         read_buf[EVTLOG_READ_FGC_BUF_LEN])
{
    struct EVTLOG_record * const evtlog    = evtlog_device->record;

    // Search for the next valid event log record to read out

    uint16_t index;

    while((index = *record_index) < evtlog_def.num_records)
    {
        // Advance record index to the next record

        (*record_index)++;

        // If the time stamp of the current record is valid

        if(evtlog[index].time_stamp.secs.abs > 0)
        {
            // Take a copy of the complete current record

            struct EVTLOG_record const record = evtlog[index];

            // Memory barrier to ensure this is done before re-copying the time stamp

            CC_MEMORY_BARRIER;

            struct CC_us_time const time_stamp = evtlog[index].time_stamp;

            // Check that both copies of the time stamp match and are valid

            if(time_stamp.secs.abs > 0 && time_stamp.secs.abs == record.time_stamp.secs.abs && time_stamp.us == record.time_stamp.us)
            {
                char * buf = read_buf;

                // Format the record as a string: Unixtime.Ustime:Property:Value:ActionStatus
                // Commas in the fields are replaced by single quotes (').
                // Colons in the fields are replaced by pipes (|).

                buf += sprintf(read_buf, "%u%06u:", (unsigned int)ntohl(record.time_stamp.secs.abs), (unsigned int)ntohl(record.time_stamp.us));

                buf = evtlogReadPrintFgcField(buf, record.property, EVTLOG_PROPERTY_LEN);

                *(buf++) = ':';

                buf = evtlogReadPrintFgcField(buf, record.value, EVTLOG_VALUE_LEN);

                *(buf++) = ':';

                // Copy action (this is padded with spaces by evtlogStoreRecord() so it always fills the field)

                buf = evtlogReadPrintFgcField(buf, record.action, EVTLOG_ACTION_LEN);

                // Add status character

                *(buf++) = record.status;

                *buf = '\0';

                // Report success

                return (uint32_t)(buf-read_buf);
            }
        }
    }

    // No more valid records available so return false

    return 0;
}



uint32_t evtlogReadBin(struct EVTLOG_device  * evtlog_device,
                       uint16_t              * record_index,
                       char                    read_buf[EVTLOG_RECORD_LEN],
                       enum EVTLOG_byte_order  byte_order)
{
    if (*record_index >= evtlog_def.num_records)
    {
        return 0;
    }

    struct EVTLOG_record * record = &evtlog_device->record[*record_index];

    // Advance record index to the next record

    (*record_index)++;

    // Transfer records individually, in case the compiler pads struct EVTLOG_record to an 8-byte boundary

    // The time stamp words are stored at the start of record in network byte order

    if(byte_order == EVTLOG_NETWORK_BYTE_ORDER)
    {
        // Network byte order required - copy complete record to destination

        memcpy(read_buf, record, EVTLOG_RECORD_LEN);
    }
    else
    {
        // Host byte order required - first translate then copy time stamp words

        struct CC_us_time const time_stamp = { .secs.abs = ntohl(record->time_stamp.secs.abs),
                                               .us       = ntohl(record->time_stamp.us) };

        // Use memcpy because the alignment of read_buf isn't known

        memcpy(read_buf, &time_stamp, sizeof(time_stamp));

        // Copy the rest of the record - it contains byte arrays so no swapping is required

        memcpy(read_buf + sizeof(time_stamp), record->property, EVTLOG_RECORD_LEN - sizeof(time_stamp));
    }

    // Report the number of bytes returned

    return EVTLOG_RECORD_LEN;
}

// EOF
