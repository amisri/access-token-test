//! @file  evtlogStore.c
//! @brief Converter Control Event Logging library store functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libevtlog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libevtlog.h"

// #define CCPRINTF

// Constants

enum EVTLOG_freq_state
{
    EVTLOG_FREQ_STATE_NORMAL    ,
    EVTLOG_FREQ_STATE_TO_FREQ   ,
    EVTLOG_FREQ_STATE_FREQ      ,
    EVTLOG_FREQ_STATE_FROM_FREQ ,
};

enum EVTLOG_filtered_flags_bit_masks
{
    EVTLOG_FILTERED_FLAGS_PREV = 0x01,
    EVTLOG_FILTERED_FLAGS_LAST = 0x02,
};

enum EVTLOG_action
{
    EVTLOG_ACTION_SET,
    EVTLOG_ACTION_SET_BIT,
    EVTLOG_ACTION_CLR_BIT,
};

// Static arrays

static const char * const action_string[] = { "SET    ", "SET_BIT", "CLR_BIT" };

static const char status_char[] =            //        ----FILTERED---      --------FREQ_STATE--------
{                                            // IDX    LAST       PREV      BIT1       BIT0      STATE
    EVTLOG_STATUS_NORMAL,                    // [ 0]     0          0         0          0         NL  = NORMAL
    EVTLOG_STATUS_NORMAL_TO_FREQ,            // [ 1]     0          0         0          1         TF  = TO FREQUENT
    EVTLOG_STATUS_FREQ,                      // [ 2]     0          0         1          0         FR  = FREQUENT
    EVTLOG_STATUS_NORMAL_FROM_FREQ,          // [ 3]     0          0         1          1         FF  = FROM FREQUENT
    EVTLOG_STATUS_DROPPED,                   // [ 4]     0          1         0          0         NL
    EVTLOG_STATUS_DROPPED,                   // [ 5]     0          1         0          1         TF
    EVTLOG_STATUS_DROPPED,                   // [ 6]     0          1         1          0         FR
    EVTLOG_STATUS_DROPPED,                   // [ 7]     0          1         1          1         FF
    EVTLOG_STATUS_NORMAL_FILTERED,           // [ 8]     1          0         0          0         NL
    EVTLOG_STATUS_NORMAL_TO_FREQ_FILTERED,   // [ 9]     1          0         0          1         TF
    EVTLOG_STATUS_FREQ_FILTERED,             // [10]     1          0         1          0         FR
    EVTLOG_STATUS_NORMAL_FROM_FREQ_FILTERED, // [11]     1          0         1          1         FF
    EVTLOG_STATUS_DROPPED,                   // [12]     1          1         0          0         NL
    EVTLOG_STATUS_DROPPED,                   // [13]     1          1         0          1         TF
    EVTLOG_STATUS_DROPPED,                   // [14]     1          1         1          0         FR
    EVTLOG_STATUS_DROPPED,                   // [15]     1          1         1          1         FF
};

// Static functions

//! Finds and locks and returns a pointer to the next record to fill in the normal or frequent event log.
//!
//! This function uses the application's lock/unlock callback functions (if provided) to ensure a conflict-free
//! allocation of the next free record in the normal or frequent log (as identified by the status character).
//!
//! @param[in,out]  evtlog_device            Pointer to the event log device structure.
//! @param[in]      status                   Record status character (identifies normal or frequent log)
//! @returns        Pointer to the next record to fill

static struct EVTLOG_record * evtlogStoreGetNextRecordPointer(struct EVTLOG_device * const evtlog_device, char const status)
{
    // Prepare to advance the newest record index for either the frequent events log or the normal events log

    uint16_t * newest_record_index;
    uint16_t   start_index;
    uint16_t   end_index;

    if(evtlog_def.num_freq_event_records > 0 &&
      (status == EVTLOG_STATUS_FREQ || status == EVTLOG_STATUS_FREQ_FILTERED))
    {
        // Frequent event

        newest_record_index = &evtlog_device->newest_freq_record_index;
        start_index         = 0;
        end_index           = evtlog_def.num_freq_event_records;
    }
    else
    {
        // Normal event

        newest_record_index = &evtlog_device->newest_record_index;
        start_index         = evtlog_def.num_freq_event_records;
        end_index           = evtlog_def.num_records;
    }

    // Lock access to the newest_record_index

    if(evtlog_def.lock_function)
    {
        (*evtlog_def.lock_function)(evtlog_device->device_index, evtlog_def.lock_data);
    }

    // Increment the newest record index and wrap if needed

    if(++(*newest_record_index) >= end_index)
    {
        *newest_record_index = start_index;
    }

    // Use the newest record as the next record to fill

    struct EVTLOG_record * const next_record = evtlog_device->record + *newest_record_index;

    // Cancel this record by reseting the unix_time time stamp

    next_record->time_stamp.secs.abs = 0;

    // Unlock access

    if(evtlog_def.unlock_function)
    {
        (*evtlog_def.unlock_function)(evtlog_device->device_index, evtlog_def.lock_data);
    }

    // Return the record_index

    return next_record;
}



//! Potentially stores a property event.
//!
//! This function is responsible for determining the status character for the event, based on the filtered flags
//! and frequent event state machine. It also sets the action field, according to the formatting and data type.
//!
//! @param[in,out]  evtlog_device            Pointer to the event log device structure.
//! @param[in]      property                 Pointer to property structure.
//! @param[in]      last_evt                 Pointer to last event structure.
//! @param[in]      symbol_index             Property symbol index (negative if no symbol to display).

static void evtlogStore(struct EVTLOG_device   * const evtlog_device,
                        struct EVTLOG_property * const property,
                        struct EVTLOG_last_evt * const last_evt,
                        int16_t                  const symbol_index)
{
    // Set status character based on the filtered flags and frequent event state machine

    uint16_t const status_idx = (last_evt->filtered_flags << 2) | last_evt->freq_state;
    char     const status     = status_char[status_idx];
    uint32_t const value      = last_evt->value;

    // If event is not to be dropped

    if(status != EVTLOG_STATUS_DROPPED)
    {
        enum EVTLOG_action action_index;

        char         value_buf[EVTLOG_VALUE_LEN];
        char const * value_string;

        // If symbol_index is negative, then report value as numeric. This might be a property
        // with a NUMERIC format, or an ENUM where there is no symbol matching the value.

        if(symbol_index < 0)
        {
            // Translate value into decimal and hex

            snprintf(value_buf, EVTLOG_VALUE_LEN, "%u (0x%X)", (unsigned int)value, (unsigned int)value);

            action_index = EVTLOG_ACTION_SET;
            value_string = value_buf;
        }

        // else symbol_index identifies the value

        else
        {
            value_string = evtlog_def.symbol[symbol_index].string;

            // Define action according to whether symlist is a bit mask or enum

            if(property->format == EVTLOG_BITMASK)
            {
                action_index = (value == 0 ? EVTLOG_ACTION_CLR_BIT : EVTLOG_ACTION_SET_BIT);
            }
            else
            {
                action_index = EVTLOG_ACTION_SET;
            }
        }

        // Store event in the next record

        ccprintf("calling evtlogStoreRecord: value_string=%s  value=%x  action_string[action_index]=%s\n",
                value_string, (unsigned int)value, action_string[action_index]);

        evtlogStoreRecord(evtlog_device,
                          last_evt->time_stamp,
                          property->name,
                          value_string,
                          action_string[action_index],
                          status);
    }
}



//! Drive the event's frequent state machine
//!
//! This function manages the frequent state machine for a property event. It is called when the
//! property value changes.
//!
//! @param[in]      freq_state               Previous state.
//! @param[in]      iters_since_last_evt     Iterations counter since last event for this property/value.
//! @returns        New state.

static uint8_t evtlogFreqState(uint8_t const freq_state, uint16_t const iters_since_last_evt)
{
    switch(freq_state)
    {
        case EVTLOG_FREQ_STATE_NORMAL:

            if(iters_since_last_evt > evtlog_def.filtering_iters)
            {
                return EVTLOG_FREQ_STATE_TO_FREQ;
            }
            break;

        case EVTLOG_FREQ_STATE_TO_FREQ:

            if(iters_since_last_evt == 0)
            {
                return EVTLOG_FREQ_STATE_FROM_FREQ;
            }
            else if(iters_since_last_evt > evtlog_def.filtering_iters)
            {
                return EVTLOG_FREQ_STATE_FREQ;
            }
            break;

        case EVTLOG_FREQ_STATE_FREQ:

            if(iters_since_last_evt == 0)
            {
                return EVTLOG_FREQ_STATE_FROM_FREQ;
            }
            break;

        case EVTLOG_FREQ_STATE_FROM_FREQ:

            if(iters_since_last_evt == 0)
            {
                return EVTLOG_FREQ_STATE_NORMAL;
            }
            else if(iters_since_last_evt > evtlog_def.filtering_iters)
            {
                return EVTLOG_FREQ_STATE_TO_FREQ;
            }
            break;
    }

    // State unchanged

    return freq_state;
}



//! Check iterations since last event for a property.
//!
//! This function is called for every property event on every iteration. It keep track of the number
//! of iterations since the event occurred. It also checks if the value has changed again, since the
//! event. It will store the event in a record if and when required.
//!
//! @param[in,out]  evtlog_device            Pointer to the event log device structure.
//! @param[in]      time_stamp               Pointer to time stamp for this iteration.
//! @param[in]      property                 Pointer to property structure.
//! @param[in]      last_evt                 Pointer to last event structure.
//! @param[in]      symbol_index             Property symbol index (negative if no symbol to display).
//! @param[in]      value                    Property value on this iteration.
//! @param[in]      last_value               Previous property value.
//! @param[in]      value_changed            True if the value has just changed.

static void evtlogStorePropCheckItersSinceLastEvt(struct EVTLOG_device    * const evtlog_device,
                                                  struct CC_us_time const * const time_stamp,
                                                  struct EVTLOG_property  * const property,
                                                  struct EVTLOG_last_evt  * const last_evt,
                                                  int16_t                   const symbol_index,
                                                  uint32_t                  const value,
                                                  uint32_t                  const last_value,
                                                  bool                      const value_changed)
{
    // Increment/reset the iterations since last event counter

    if(last_evt->iters_since_last_evt > 0)
    {
        if(++last_evt->iters_since_last_evt > evtlog_def.freq_evt_iters)
        {
            last_evt->iters_since_last_evt = 0;
        }
    }

    uint16_t const iters_since_last_evt = last_evt->iters_since_last_evt;

    // Process property value according to whether it has changed since the last iteration

    if(value_changed)
    {
        ccprintf("value=%d (0x%X)\n", (int)value, (unsigned int)value);

        // Value changed - this is a new event - check if it should be filtered

        if(iters_since_last_evt > 0 && iters_since_last_evt <= evtlog_def.filtering_iters)
        {
            // Filtering is enabled and the new event is within the filtering time window

            last_evt->filtered_flags |= EVTLOG_FILTERED_FLAGS_LAST;

            // Potentially store last event

            ccprintf("New event filtered - Calling evtlogStore for last event\n");

            evtlogStore(evtlog_device, property, last_evt, symbol_index);

            // Set filtered flags to PREV

            last_evt->filtered_flags = EVTLOG_FILTERED_FLAGS_PREV;
        }
        else
        {
            // New event is not filtered so save the value, so it can potentially be stored later

            last_evt->filtered_flags = 0;
            last_evt->value          = value;

            ccprintf("Value changed after filtering time. iters_since_last_evt=%d\n",iters_since_last_evt);
        }

        // Run frequent events state machine

        last_evt->freq_state = evtlogFreqState(last_evt->freq_state, iters_since_last_evt);

        // Restart the counter of 'iterations since last event' and save the time stamp for the new event

        last_evt->iters_since_last_evt = 1;
        last_evt->time_stamp = *time_stamp;

        // If filtering is disabled then store the new event immediately

        if(evtlog_def.filtering_iters == 0)
        {
            // Filtering is disabled so store new event immediately

            ccprintf("2. Filtering disabled - calling evtlogStore\n");

            evtlogStore(evtlog_device, property, last_evt, symbol_index);
        }
    }
    else
    {
        // Value unchanged

        if(evtlog_def.filtering_iters > 0 && iters_since_last_evt == evtlog_def.filtering_iters)
        {
            // Filtering is enabled and value is still unchanged at the end of the filtering window so potentially store last event

            ccprintf("value unchanged iters_since_last_evt=%d  3. Calling evtlogStore\n", iters_since_last_evt);

            evtlogStore(evtlog_device, property, last_evt, symbol_index);
        }
    }
}



//! Processes an Enumerated Type property.
//!
//! This function is called for every enumerated type property on every iteration. It treats the events associated
//! with each symbol, keeping track of how many iterations have elapsed since the last time that symbol
//! became the active value for the property.
//!
//! @param[in,out]  evtlog_device            Pointer to the event log device structure.
//! @param[in]      time_stamp               Pointer to time stamp for this iteration.
//! @param[in]      property                 Pointer to property structure.
//! @param[in]      value                    Property value on this iteration.
//! @param[in]      last_value               Previous property value.

static void evtlogStoreEnumProp(struct EVTLOG_device    * const evtlog_device,
                                struct CC_us_time const * const time_stamp,
                                struct EVTLOG_property  * const property,
                                uint32_t                  const value,
                                uint32_t                  const last_value)
{
    uint16_t                 num_labels   =  evtlog_def.symlist[property->symlist_index].num_symbols;
    int16_t                  symbol_index =  evtlog_def.symlist[property->symlist_index].symbol_base_index;
    struct EVTLOG_last_evt * last_evt     = &evtlog_device->last_evt[property->last_evt_base_index];
    bool               const value_changed = value != last_value;
    bool                     match_enum    = false;

    // Scan all symbols for this ENUM format

    while(num_labels--)
    {
        bool const symbol_matched = (value == evtlog_def.symbol[symbol_index].value);

        // Check time since last event for this symbol

        evtlogStorePropCheckItersSinceLastEvt(evtlog_device,
                                              time_stamp,
                                              property,
                                              last_evt++,
                                              symbol_index++,
                                              value,
                                              last_value,
                                              value_changed && symbol_matched);

        // Remember if any label matches the value

        match_enum |= symbol_matched;
    }

    // Treat any values that don't match a label value as numeric

    evtlogStorePropCheckItersSinceLastEvt(evtlog_device,
                                          time_stamp,
                                          property,
                                          last_evt,
                                          -1,
                                          value,
                                          last_value,
                                          (match_enum == false) && value_changed);
}


//! Processes a bit mask property.
//!
//! This function is called for every bit mask property on every iteration. It treats the events associated
//! with each bit for which a symbol is defined, keeping track of how many iterations have elapsed since the last time
//! that bit was set or cleared.
//!
//! @param[in,out]  evtlog_device            Pointer to the event log device structure.
//! @param[in]      time_stamp               Pointer to time stamp for this iteration.
//! @param[in]      property                 Pointer to property structure.
//! @param[in]      value                    Property value on this iteration.
//! @param[in]      last_value               Previous property value.

static void evtlogStoreBitmaskProp(struct EVTLOG_device    * const evtlog_device,
                                   struct CC_us_time const * const time_stamp,
                                   struct EVTLOG_property  * const property,
                                   uint32_t                  const value,
                                   uint32_t                  const last_value)
{
    uint16_t           const num_labels   =  evtlog_def.symlist[property->symlist_index].num_symbols;
    int16_t                  symbol_index =  evtlog_def.symlist[property->symlist_index].symbol_base_index;
    struct EVTLOG_last_evt * last_evt     = &evtlog_device->last_evt[property->last_evt_base_index];

    // Scan all symbols (SET_BIT and CLR_BIT) for this BIT MASK format

    // There are twice as many last_evt records as there are labels.
    // The first half are used to track the case where the bit value becomes 0 (CLR_BIT).
    // The second half tracks when the bit value becomes 1 (SET_BIT).

    uint16_t label_index = num_labels;

    while(label_index--)
    {
        uint32_t const symbol_bit_mask   = evtlog_def.symbol[symbol_index].value;
        uint32_t const bit_value         = value      & symbol_bit_mask;
        uint32_t const last_bit_value    = last_value & symbol_bit_mask;
        bool     const bit_value_changed = bit_value != last_bit_value;

        if(bit_value_changed)
        {
            ccprintf("value=0x%X != last_value=0x%X  symbol_bit_mask=%x  symbol=%s  bit_value=0x%X  last_bit_value=0x%X\n",
                    (unsigned int)value, 
                    (unsigned int)last_value, 
                    (unsigned int)symbol_bit_mask,
                    evtlog_def.symbol[symbol_index].string, 
                    (unsigned int)bit_value,
                    (unsigned int)last_bit_value);
        }

        // Check for CLR_BIT (last_evt[0] - last_evt[num_labels-1])

        evtlogStorePropCheckItersSinceLastEvt(evtlog_device,
                                              time_stamp,
                                              property,
                                              last_evt,
                                              symbol_index,
                                              bit_value,
                                              last_bit_value,
                                              bit_value_changed == true && bit_value == 0);

        // Check for SET_BIT (last_evt[num_labels] - last_evt[2*num_labels-1])

        evtlogStorePropCheckItersSinceLastEvt(evtlog_device,
                                              time_stamp,
                                              property,
                                              last_evt + num_labels,
                                              symbol_index,
                                              bit_value,
                                              last_bit_value,
                                              bit_value_changed == true && bit_value != 0);

        symbol_index++;
        last_evt++;
    }
}



// Global functions

void evtlogStoreProperties(struct EVTLOG_device * const evtlog_device, struct CC_us_time const time_stamp)
{
    struct EVTLOG_property  * property  = evtlog_def.property;
    struct EVTLOG_prop_data * prop_data = evtlog_device->prop_data;
    uint16_t                  prop_index = 0;

    while(prop_index < evtlog_def.num_properties)
    {
        // Get current property value

        uint32_t value = 0;

        switch(property->sizeof_value)
        {
            case 1: value = *prop_data->source.u8;   break;
            case 2: value = *prop_data->source.u16;  break;
            case 4: value = *prop_data->source.u32;  break;
            default:                                 break;
        }

        // Process property value compared to last value

        uint32_t const last_value = prop_data->last_value;

        switch(property->format)
        {
            case EVTLOG_NUMERIC:

                evtlogStorePropCheckItersSinceLastEvt(evtlog_device,
                                                      &time_stamp,
                                                      property,
                                                      &evtlog_device->last_evt[property->last_evt_base_index],
                                                      -1,       // symbol_index = -1 to request NUMERIC format
                                                      value,
                                                      last_value,
                                                      value != last_value);
                break;

            case EVTLOG_ENUM:

                evtlogStoreEnumProp(evtlog_device, &time_stamp, property, value, last_value);
                break;

            case EVTLOG_BITMASK:

                evtlogStoreBitmaskProp(evtlog_device, &time_stamp, property, value, last_value);
                break;
        }

        // If value changed then call user's call back function if provided

        if(evtlog_def.changed_function != NULL && prop_data->last_value != value)
        {
            (*evtlog_def.changed_function)(evtlog_device->device_index, property->name, property->changed_user_data);
        }

        // Remember property value

        prop_data->last_value = value;

        // Advance to next property

        prop_index++;
        property++;
        prop_data++;
    }
}


struct EVTLOG_record * evtlogPrepareRecord(struct EVTLOG_device * const evtlog_device,
                                           char           const * const property,
                                           char           const * const value,
                                           char                   const status)

{
    // Get pointer to next event log record

    struct EVTLOG_record * const record = evtlogStoreGetNextRecordPointer(evtlog_device, status);

    // Store property and value fields in the record - null terminate if shorter than the field length

    strncpy(record->property, property, EVTLOG_PROPERTY_LEN);
    strncpy(record->value   , value   , EVTLOG_VALUE_LEN   );

    // Record the status character and return the record

    record->status = status;

    return record;
}



void evtlogFinishRecord(struct EVTLOG_record * const record,
                        struct CC_us_time      const time_stamp,
                        char           const *       action)
{
    // Store action field in the record and pad with spaces

    char     action_char;
    char   * action_buf = record->action;
    uint32_t n_chars = EVTLOG_ACTION_LEN;

    while(n_chars && (action_char = *(action++)) != '\0')
    {
        *(action_buf++) = action_char;
        n_chars--;
    }

    while(n_chars--)
    {
        *(action_buf++) = ' ';
    }

    // Store the time stamp in network byte order

    record->time_stamp.us = htonl(time_stamp.us);

    CC_MEMORY_BARRIER;

    // Store the Unix time part of the time stamp last to validate the rest of the record

    record->time_stamp.secs.abs = htonl(time_stamp.secs.abs);
}



void evtlogStoreRecord(struct EVTLOG_device * const evtlog_device,
                       struct CC_us_time      const time_stamp,
                       char           const * const property,
                       char           const * const value,
                       char           const *       action,
                       char                   const status)
{
    evtlogFinishRecord(evtlogPrepareRecord(evtlog_device, property, value, status), time_stamp, action);
}

// EOF
