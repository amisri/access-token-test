//! @file  evtlogRead.h
//! @brief Converter Control Event Logging library read functions header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

//! The read event CSV log format is:
//!
//! {time_stamp.s}.{time_stamp.us},{property},"{value}",{action},{status}\0
//!
//!     time_stamp.s     : 10
//!     Decimal point    :  1
//!     time_stamp.us    :  6
//!     Status character :  1
//!     Comma delimiters :  4
//!     Value quotes     :  2
//!     Null termination :  1
//!     TOTAL CHARACTERS : 25


#define EVTLOG_READ_CSV_BUF_LEN     (25+EVTLOG_PROPERTY_LEN+EVTLOG_VALUE_LEN+EVTLOG_ACTION_LEN)

//! The read event FGC log format is:
//!
//! {time_stamp.s}.{time_stamp.us}:{property}:{value}:{action}{status}\0
//!
//!     time_stamp.s     : 10
//!     Decimal point    :  1
//!     time_stamp.us    :  6
//!     Status character :  1
//!     Colon delimiters :  3
//!     Null termination :  1
//!     TOTAL CHARACTERS : 22


#define EVTLOG_READ_FGC_BUF_LEN     (22+EVTLOG_PROPERTY_LEN+EVTLOG_VALUE_LEN+EVTLOG_ACTION_LEN)

//! Time stamps are recorded in network byte order, but can be read out in either network or host byte order.
//! The other fields in the record are strings, which are not effected by the byte order.


enum EVTLOG_byte_order
{
     EVTLOG_NETWORK_BYTE_ORDER,
     EVTLOG_HOST_BYTE_ORDER
};

// Global function declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Readout event log in CSV format
//!
//! This function will return the next valid event log record formatted as a string in read_buf
//! in CSV format.
//!
//!     {time_stamp.s}.{time_stamp.us},{property},"{value}",{action},{status}\0
//!
//! The value field may contain commas, so it is enclosed in double quotes as a precaution.
//!
//! The application should set *record_index to zero before the first call. The function will
//! increment *record_index and will return true if read_buf is returned with a valid event log record string.
//! Once no more records remain, it will return false.
//!
//! The application must sort all the records it reads, based on their time stamps, before presenting
//! to the user.
//!
//! @param[in]      evtlog_device          Pointer to the event log device structure
//! @param[in]      record_index           Pointer to record_index for next record (start at zero). Incremented after each call.
//! @param[out]     read_buf               Character array to receive event log record string
//! @retval         True                   if read_buf returned with an event log record.
//! @retval         False                  if no more records to return.

bool evtlogReadCsv(struct EVTLOG_device * evtlog_device,
                   uint16_t             * record_index,
                   char                   read_buf[EVTLOG_READ_CSV_BUF_LEN]);


//! Readout event log in FGC format
//!
//! This function will return the next valid event log record formatted as a string in read_buf
//! in FGC format.
//!
//!     {time_stamp.s}{time_stamp.us}:{property}:{value}:{action}{status}\0
//!
//! Commas and colons are used by the FGC protocol as record and field separators.
//! So commas are replaced by single quotes and colons by pipes in the property,
//! value and action fields.
//!
//! The application should set *record_index to zero before the first call. The function will
//! return the number of characters written into read_buf excluding the nul terminator.
//! Once no more records remain, it will return zero.
//!
//! The application must sort all the records it reads, based on their time stamps, before presenting
//! to the user.
//!
//! @param[in]      evtlog_device          Pointer to the event log device structure
//! @param[in]      record_index           Pointer to record_index for next record (start at zero). Incremented after each call.
//! @param[out]     read_buf               Character array to receive event log record string
//! @returns        Number of characters in read_buf excluding nul terminator

uint32_t evtlogReadFgc(struct EVTLOG_device * evtlog_device,
                       uint16_t             * record_index,
                       char                   read_buf[EVTLOG_READ_FGC_BUF_LEN]);


//! Readout event log in binary, with support to change byte order of the time stamps
//!
//! This function will copy the event log record identified by record_index to the destination buffer.
//! The length of the buffer must be at least EVTLOG_RECORD_LEN bytes. No check is made.
//!
//! @param[in]      evtlog_device          Pointer to the event log device structure
//! @param[in]      record_index           Pointer to record_index for next record (start at zero). Incremented after each call.
//! @param[in]      read_buf               Pointer to destination buffer that should be at least EVTLOG_SIZE_OF_RECORDS bytes long.
//! @param[in]      byte_order             Byte order for the time stamp long words (the rest are byte arrays anyway)
//! @returns        Number of characters in read_buf. Zero once all records returned.

uint32_t evtlogReadBin(struct EVTLOG_device  * evtlog_device,
                       uint16_t              * record_index,
                       char                    read_buf[EVTLOG_RECORD_LEN],
                       enum EVTLOG_byte_order  byte_order);

#ifdef __cplusplus
}
#endif

// EOF
