//! @file  evtlogInit.h
//! @brief Converter Control Event Logging library initialization functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global function declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize event log data for a device
//!
//! The function should be called initialize the evtlog_device data for a device. This should be allocated by the application
//! with the size equal to EVTLOG_SIZE_OF_DEV_DATA bytes.
//!
//! If unix_time is provided, then logs with time stamps up to 3M seconds in the past will be preserved and the newest record
//! indexes will be set to the most recent valid log records in the frequent and normal events buffers.
//! Note that since records can be entered slightly out of time order, it is possible that the most recent event may be
//! by events recorded just before, and these will be overwritten by any new events that are recorded.
//!
//! @param[in,out]  evtlog_device            Pointer to EVTLOG_SIZE_OF_DEV_DATA bytes allocated by the application for the device's event log.
//!                                          The zone must start long-word aligned.
//! @param[in]      unix_time                Set to current unix_time to preserve valid event log records, or zero to always reset event log.
//! @param[in]      device_index             Index for the device being initialized.

void evtlogInitDevice(struct EVTLOG_device * evtlog_device, uint32_t unix_time, uint32_t device_index);

#ifdef __cplusplus
}
#endif

// EOF
