//! @file  evtlogStore.h
//! @brief Converter Control Event Logging library store functions header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global function declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Poll properties for changes and record as events in the log
//!
//! This function should be called repetitively at the appropriate rate, with the timestamp for the iteration.
//! It will check the value for all properties configured for the class against the value from the previous
//! iteration. If the property value changes it will be registered as an event. This may be recorded
//! in either the normal or frequent events log, or be filtered, according to the time since the last
//! occurrence of the same event. For each changed property, the function will call the changed_function
//! callback (if provided in the configuration CSV file for the class).
//!
//! @param[in,out]  evtlog_device              Pointer to the event log device structure
//! @param[in]      time_stamp                 Time stamp for this iteration

void evtlogStoreProperties(struct EVTLOG_device * evtlog_device, struct CC_us_time time_stamp);



//! Prepare a new record in the event log
//!
//! This allows the application to prepare an arbitrary entry into either the normal or frequent events log.
//! The record pointer is returned and to be completed, it must be passed to evtlogFinishRecord().
//!
//! @param[in,out]  evtlog_device              Pointer to the event log device structure
//! @param[in]      property                   Property string - up to 24 characters can be saved in the event log record
//! @param[in]      value                      Value string    - up to 36 characters can be saved in the event log record
//! @param[in]      status                     Record status character
//! @returns        Pointer to the event log record prepared with the property, value and status.
//!                 This should be used with evtlogFinishRecord() to finish the record.

struct EVTLOG_record * evtlogPrepareRecord(struct EVTLOG_device * const evtlog_device,
                                           const char           * property,
                                           const char           * value,
                                           char                   status);



//! Finish a record in the event log
//!
//! This allows the application to finish a record that was prepared with evtlogPrepareRecord().
//! This might be a set command, where the action field will report the result of the command
//! (OK or ERROR), and this is only known after the command is run.
//!
//! @param[in,out]  record                     Pointer to the event log record returned by evtlogPrepareRecord()
//! @param[in]      time_stamp                 Time stamp for this iteration
//! @param[in]      action                     Action string - up to 7 characters can be saved in the event log record

void evtlogFinishRecord(struct EVTLOG_record * record,
                        struct CC_us_time      time_stamp,
                        const char           * action);



//! Store record in the event log
//!
//! This allows the application to write an arbitrary entry into either the normal or frequent events log.
//! This calls evtlogPrepareRecord() and evtlogFinishRecord() in one go.
//!
//! @param[in,out]  evtlog_device              Pointer to the event log device structure
//! @param[in]      time_stamp                 Time stamp for this iteration
//! @param[in]      property                   Property string - up to 24 characters can be saved in the event log record
//! @param[in]      value                      Value string    - up to 36 characters can be saved in the event log record
//! @param[in]      action                     Action string   - up to 7 characters can be saved in the event log record
//! @param[in]      status                     Record status character

void evtlogStoreRecord(struct EVTLOG_device * evtlog_device,
                       struct CC_us_time      time_stamp,
                       const char           * property,
                       const char           * value,
                       const char           * action,
                       char                   status);

#ifdef __cplusplus
}
#endif

// EOF
