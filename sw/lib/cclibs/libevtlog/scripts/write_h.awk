# libevtlog/scripts/write_h.awk
#
# Converter Control Event Log library : Structures header files writer
#
# The script will write evlogStructs.h and evlogStructsInit.h.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libevtlog.
#
# libevtlog is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    # Get output path from command line

    if(ARGC < 3)
    {
        printf "usage: write_h.awk input_csv_file1 input_csv_file2 ... output_path\n"
        exit 1
    }

    ARGC--

    gsub(/\/+$/, "", ARGV[ARGC]) # remove trailing slashes from output_path

    output_path = ARGV[ARGC]

    # Set field separater to semicolon to read event log definition CSV file (fields can contain commas)

    FS = ";"

    # List of keys - the values are not used - key[] is just used to check if a key is known

    key["CLASS"]                    = 1
    key["NUM_DEVICES"]              = 2
    key["NUM_RECORDS"]              = 3
    key["NUM_FREQ_EVT_RECORDS"]     = 4
    key["FREQ_EVT_ITERATIONS"]      = 5
    key["FILTERING_ITERATIONS"]     = 6
    key["CHANGED_FUNCTION"]         = 8
    key["LOCK_FUNCTION"]            = 9
    key["UNLOCK_FUNCTION"]          = 10
    key["LOCK_DATA"]                = 11

    # List of options for property_format

    prop_format_key["NUMERIC"]      = 1
    prop_format_key["ENUM"]         = 2
    prop_format_key["BITMASK"]      = 3

    # Identify the columns in the definition CSV file

    prop_name_column                = 2
    prop_changed_user_data_column   = 3
    prop_source_column              = 4
    prop_type_column                = 5
    prop_format_column              = 6
    prop_symlist_column             = 7

    symlist_name_column             = 2

    symbol_name_column              = 2
    symbol_value_column             = 3

    # Read event log definitions from input file

    num_props    = 0
    num_symlists = 0
    num_symbols  = 0
    symlist      = ""
    symlist_index["not_used"] = 0

    for(i = 1 ; i < ARGC ; i++)
    {
        # Replace slashes and dots in the file name to work with CC_STATIC_ASSERT

        filename = ARGV[i]
        gsub(/[\/\.]/,"_",filename)

        # Read next file to the end

        line_num = 0

        while((getline < ARGV[i]) > 0)
        {
            line_num++

            # Skip blank lines or comment lines

            if($1 == "" || substr($1,1,1) == "#")
            {
                continue
            }

            # Parse line based on first field

            if($1 in key)
            {
                value[$1] = $2
            }
            else if($1 == "PROPERTY")
            {
                ReadProperty()
            }
            else if($1 == "SYMLIST")
            {
                ReadSymlist()
            }
            else if($1 == "SYMBOL")
            {
                ReadSymbol()
            }
            else
            {
                Error(filename, line_num, "invalid first field: " $1)
            }
        }

        close(ARGV[i])
    }

    # Check global values

    if(value["NUM_FREQ_EVT_RECORDS"] >= value["NUM_RECORDS"])
    {
        Error("",0,"NUM_FREQ_EVT_RECORDS >= NUM_RECORDS")
    }

    if(value["NUM_FREQ_EVT_RECORDS"] == 0)
    {
        value["FREQ_EVT_ITERATIONS"] = value["FILTERING_ITERATIONS"]
    }

    if(value["CHANGED_FUNCTION"] == "")
    {
        value["CHANGED_FUNCTION"] = "NULL"
    }

    if(value["LOCK_FUNCTION"] == "")
    {
        value["LOCK_FUNCTION"] = "NULL"
    }

    if(value["UNLOCK_FUNCTION"] == "")
    {
        value["UNLOCK_FUNCTION"] = "NULL"
    }

    if(value["LOCK_DATA"] == "")
    {
        value["LOCK_DATA"] = "NULL"
    }

    # Process the event log definition

    ProcessProperties()

    ProcessSymlistsAndSymbols()

    # Generate header files

    WriteStructsHeader(output_path "/evtlogStructs.h")

    WriteStructsInitHeader(output_path "/evtlogStructsInit.h")
}



function ReadProperty()
{
    # Check that property name is unique

    if($prop_name_column in prop_name_index)
    {
        Error(filename, line_num, "Property " $prop_name_column " already defined")
    }

    prop_name_index[$prop_name_column] = num_props

    # Check if changed user data is defined

    if($prop_changed_user_data_column == "" || $prop_changed_user_data_column == "NULL")
    {
        prop_changed_user_data[num_props] = "NULL"
    }
    else
    {
        prop_changed_user_data[num_props] = "(void *)" $prop_changed_user_data_column
    }

    # Check that property format is valid

    if(!($prop_format_column in prop_format_key))
    {
        Error(filename, line_num, "Property " $prop_name_column " format is invalid: " $prop_format_column)
    }

    # Check symlist is defined if

    if($prop_format_column != "NUMERIC")
    {
        # Property format is a symlist so index the name

        if($prop_symlist_column == "")
        {
            Error(filename, line_num, "Property " $prop_name_column " symlist name is missing")
        }

        prop_symlist_index[$prop_symlist_column] = num_props
        prop_symlist[num_props] = $prop_symlist_column
    }
    else
    {
        prop_symlist[num_props] = "not_used"
    }

    prop_line_num[num_props] = line_num
    prop_filename[num_props] = filename
    prop_name    [num_props] = $prop_name_column
    prop_source  [num_props] = $prop_source_column
    prop_type    [num_props] = $prop_type_column
    prop_format  [num_props] = $prop_format_column

    num_props++
}



function ReadSymlist()
{
    if($symlist_name_column in prop_symlist_index)
    {
        symlist = $symlist_name_column

        if(symlist in symlist_index)
        {
            Error(filename, line_num, "Symlist " symlist " repeated")
        }

        symlist_index      [symlist] = num_symlists
        symlist_num_symbols[symlist] = 0

        symlist_line_num         [num_symlists] = line_num
        symlist_filename         [num_symlists] = filename
        symlist_name             [num_symlists] = symlist
        symlist_symbol_base_index[num_symlists] = num_symbols

        symbol_idx = 0
        num_symlists++
    }
    else
    {
        symlist = ""
    }
}



function ReadSymbol()
{
    if(symlist != "")
    {
        symbol_line_num[symlist, symbol_idx] = line_num
        symbol_filename[symlist, symbol_idx] = filename
        symbol_name    [symlist, symbol_idx] = $symbol_name_column
        symbol_value   [symlist, symbol_idx] = $symbol_value_column

        symlist_num_symbols[symlist] = ++symbol_idx
        num_symbols++
    }
}



function ProcessProperties(  prop_idx, format)
{
    #  Calculate prop_num_last_evt[prop_idx] and from this prop_last_evt_base_index[prop_idx]

    num_last_evt = 0

    for(prop_idx = 0 ; prop_idx < num_props ; prop_idx++)
    {
        prop_last_evt_base_index[prop_idx] = num_last_evt

        format = prop_format[prop_idx]

        # Empty format means that property is numeric

        if(format == "NUMERIC")
        {
            # NUMERIC format : only 1 last_evt structure is needed per property

            prop_num_last_evt[prop_idx] = 1
        }
        else
        {
            # ENUM or BITMASK format

            symlist = prop_symlist[prop_idx]

            if(symlist in symlist_index)
            {
                if(symlist_type[symlist] == "")
                {
                    symlist_type[symlist] = format
                }
                else
                {
                    if(symlist_type[symlist] != format)
                    {
                        Error(prop_filename[prop_idx], prop_line_num[prop_idx],"symlist " symlist " already defined to be a " symlist_type[symlist])
                    }
                }

                if(format == "ENUM")
                {
                    # For ENUM types, one extra last_evt structure is needed to cover any value that doesn't match
                    # a known ENUM value. In this case, the unknown value will be reported as a NUMERIC format.

                    prop_num_last_evt[prop_idx] = symlist_num_symbols[symlist] + 1
                }
                else
                {
                    # For BITMASK types, two last_evt structures are needed per symbol, one for sets and the other for clears

                    prop_num_last_evt[prop_idx] = 2 * symlist_num_symbols[symlist]
                }

            }
            else
            {
                Error(prop_filename[prop_idx], prop_line_num[prop_idx],"unknown symlist for property " prop_name[prop_idx] " : " symlist)
            }
        }

        num_last_evt += prop_num_last_evt[prop_idx]
    }
}



function ProcessSymlistsAndSymbols(  symlist_idx)
{
    for(symlist_idx = 0 ; symlist_idx < num_symlists ; symlist_idx++)
    {
        symlist = symlist_name[symlist_idx]

        if(symlist_num_symbols[symlist] == 0)
        {
            Error(symlist_filename[symlist_idx], symlist_line_num[symlist_idx], "symlist " symlist_name[symlist_idx] " has no symbols defined")
        }
    }
}



function LabelPointer(label)
{
    if(substr(label,1,1) == "&")
    {
        return label
    }

    return "\"" label "\""
}



function WriteStructsHeader(of)
{
    # Generate evtlog structures header file

    WriteGnuLicense("libevtlog", "Event Log", "Structures header file", of)

    print "#pragma once\n"                                                                                           > of
    print "#include \"libevtlog.h\"\n"                                                                               > of
    print "// Constants\n"                                                                                           > of
    printf "#define EVTLOG_NUM_PROPS         %u\n", num_props                                                        > of
    printf "#define EVTLOG_NUM_LAST_EVT      %u\n", num_last_evt                                                     > of
    printf "#define EVTLOG_NUM_RECORDS       %u\n", value["NUM_RECORDS"]                                             > of
    print  "#define EVTLOG_SIZE_OF_RECORDS   (EVTLOG_NUM_RECORDS*EVTLOG_RECORD_LEN)\n"                               > of
    print "// Structures\n"                                                                                          > of
    print "struct EVTLOG_data"                                                                                       > of
    print "{"                                                                                                        > of
    print "    struct EVTLOG_device    device;"                                                                      > of
    print "    struct EVTLOG_prop_data prop_data[EVTLOG_NUM_PROPS];"                                                 > of
    print "    struct EVTLOG_last_evt  last_evt [EVTLOG_NUM_LAST_EVT];"                                              > of
    print "    struct EVTLOG_record    record   [EVTLOG_NUM_RECORDS];"                                               > of
    print "};\n"                                                                                                     > of
    print "// EOF"                                                                                                   > of

    close(of)
}



function WriteStructsInitHeader(of,  prop_idx, symlist_idx, symbol_idx)
{
    # Generate signal structures initialization file

    WriteGnuLicense("libevtlog", "Event Log", "Structures initialization header file", of)

    print "#include \"libevtlog.h\"\n"                                                                               > of

    # Properties

    printf "// Properties\n\n"                                                                                       > of
    printf "static struct EVTLOG_property evtlog_properties[] =\n{\n"                                                > of

    for(prop_idx = 0 ; prop_idx < num_props ; prop_idx++)
    {
        printf "    { // [%u] Property %s\n", prop_idx, prop_name[prop_idx]                                          > of
        printf "        %s,\n",               LabelPointer(prop_name[prop_idx])                                      > of
        printf "        %s,\n",               prop_changed_user_data[prop_idx]                                       > of
        printf "        EVTLOG_%s,\n",        prop_format[prop_idx]                                                  > of
        printf "        %d,\n",               symlist_index[prop_symlist[prop_idx]]                                  > of
        printf "        sizeof(%s),\n",       prop_type[prop_idx]                                                    > of
        printf "        %s,\n",               prop_last_evt_base_index[prop_idx]                                     > of
        printf "        %s,\n",               prop_num_last_evt[prop_idx]                                            > of
        printf "    },\n"                                                                                            > of
    }

    # Symlists

    printf "};\n\n// Symlists\n\n"                                                                                   > of
    printf "static struct EVTLOG_symlist evtlog_symlists[] =\n{\n"                                                   > of

    for(symlist_idx = 0 ; symlist_idx < num_symlists ; symlist_idx++)
    {
        symlist = symlist_name[symlist_idx]

        printf "    { // [%u] Symlist %s\n", symlist_idx, symlist                                                    > of
        printf "        %s,\n", symlist_symbol_base_index[symlist_idx]                                               > of
        printf "        %u,\n", symlist_num_symbols[symlist]                                                         > of
        printf "    },\n"                                                                                            > of
    }

    # Symbols

    printf "};\n\n// Symbols\n\n"                                                                                    > of
    printf "static struct EVTLOG_symbol evtlog_symbols[] =\n{\n"                                                     > of

    num_symbols = 0

    for(symlist_idx = 0 ; symlist_idx < num_symlists ; symlist_idx++)
    {
        symlist = symlist_name[symlist_idx]

        for(symbol_idx = 0 ; symbol_idx < symlist_num_symbols[symlist] ; symbol_idx++)
        {
            printf "    { // [%u] %s [%u]\n", num_symbols++, symlist, symbol_idx                                     > of
            printf "        %s,\n", LabelPointer(symbol_name[symlist, symbol_idx])                                   > of
            printf "        %s,\n", symbol_value[symlist, symbol_idx]                                                > of
            printf "    },\n"                                                                                        > of
        }
    }

   # Initialize event log definition structure

    printf "};\n\n// Event log definition\n\n"                                                                       > of
    printf "struct EVTLOG_def evtlog_def =\n{\n"                                                                     > of
    printf "    /* property               */   evtlog_properties,\n"                                                 > of
    printf "    /* symlist                */   evtlog_symlists,\n"                                                   > of
    printf "    /* symbol                 */   evtlog_symbols,\n"                                                    > of
    printf "    /* changed_function       */   %s,\n", value["CHANGED_FUNCTION"]                                     > of
    printf "    /* lock_function          */   %s,\n", value["LOCK_FUNCTION"]                                        > of
    printf "    /* unlock_function        */   %s,\n", value["UNLOCK_FUNCTION"]                                      > of
    printf "    /* lock_data              */   %s,\n", value["LOCK_DATA"]                                            > of
    printf "    /* num_properties         */   %u,\n", num_props                                                     > of
    printf "    /* num_records            */   %u,\n", value["NUM_RECORDS"]                                          > of
    printf "    /* num_freq_event_records */   %u,\n", value["NUM_FREQ_EVT_RECORDS"]                                 > of
    printf "    /* num_last_evt           */   %u,\n", num_last_evt                                                  > of
    printf "    /* freq_evt_iters         */   %u,\n", value["FREQ_EVT_ITERATIONS"]                                  > of
    printf "    /* filtering_iters        */   %u,\n", value["FILTERING_ITERATIONS"]                                 > of

    # Static assertions

    printf "};\n\n// Static assertions - check that bit mask values have only one set bit\n\n"                       > of

    for(symlist_idx = 0 ; symlist_idx < num_symlists ; symlist_idx++)
    {
        symlist = symlist_name[symlist_idx]

        if(symlist_type[symlist] == "BITMASK")
        {
            for(symbol_idx = 0 ; symbol_idx < symlist_num_symbols[symlist] ; symbol_idx++)
            {
                printf "CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(%s), error_in_%s_line_%s_invalid_bit_mask_value);\n",
                    symbol_value[symlist, symbol_idx],
                    symbol_filename[symlist, symbol_idx],
                    symbol_line_num[symlist, symbol_idx] > of
            }
        }
    }

    # Generate static inline function for multi-class devices

    print "\n/*!"                                                                                                    > of
    print " * Initialize event log structure per device"                                                             > of
    print " *"                                                                                                       > of
    print " * @param[in]    evtlog_device  Pointer to device's event log structure to initialize"                    > of
    print " * @param[in]    unix_time      Unix time if old data in event log should be preserved, 0 to always reset"> of
    print " * @param[in]    device_index   Hook for calling application, to allow initialization of multiple devices"> of
    print " */"                                                                                                      > of
    print "static void evtlogStructsInitDevice(struct EVTLOG_device * const evtlog_device,"                          > of
    print "                                    const uint32_t  unix_time,"                                           > of
    print "                                    const uint32_t  device_index)"                                        > of
    print "{"                                                                                                        > of
    print "    CC_ASSERT(evtlog_device != NULL);\n"                                                                  > of
    print "    // Initialize device's event log data structure\n"                                                    > of
    print "    evtlogInitDevice(evtlog_device, unix_time, device_index);\n"                                          > of
    print "    // Initialize property source pointers for the device\n"                                              > of

    for(prop_idx = 0 ; prop_idx < num_props ; prop_idx++)
    {
        printf "    evtlog_device->prop_data[%3u].source.cp = (char *)%s;\n", prop_idx, prop_source[prop_idx]        > of
    }

    print "}\n"                                                                                                      > of

    # Generate static inline for mono-device classes

    print "/*!"                                                                                                      > of
    print " * Initialize event log device structure for mono-device applications"                                    > of
    print " *"                                                                                                       > of
    print " * Wrapper around evtlogStructsInitDevice() to initialize libevtlog for applications with only one device"> of
    print " *"                                                                                                       > of
    print " * @param[in]    evtlog_device  Pointer to event log structure to initialize"                             > of
    print " * @param[in]    unix_time      Unix time if old data in event log should be preserved, 0 to always reset"> of
    print " */"                                                                                                      > of
    print "static inline void evtlogStructsInit(struct EVTLOG_device * evtlog_device, const uint32_t unix_time)"     > of
    print "{"                                                                                                        > of
    print "    evtlogStructsInitDevice(evtlog_device, unix_time, 0);"                                                > of
    print "}\n\n// EOF"                                                                                              > of

    close(of)
}

# EOF
