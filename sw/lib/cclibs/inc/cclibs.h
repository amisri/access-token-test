//! @file  cclibs.h
//! @brief Converter Control libraries global header file
//!
//! This header file is used by all the CCLIBS libraries. It declares all the
//! commonly used resources and includes the common set of standard header file.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of cclibs.
//!
//! cclibs is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Include standard header files

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <math.h>

//! Include public constants for all CCLIBS libraries

#include "cclibsConsts.h"

//! Typedef floating point types for used by cclibs - it requires double to be 64-bit.

typedef float       cc_float;
typedef double      cc_double;

//! Calculates array length at compile time

#define CC_ARRAY_LEN(array) (sizeof(array) / sizeof((array)[0]))

//! ccprintf() - debug printf to stderr
//! #define CCPRINTF before including cclibs.h to enable debug printing.
//! Source: https://stackoverflow.com/questions/1644868/define-macro-for-debug-printing-in-c

#if defined (CCPRINTF)
#define _CCPRINTF_ 1
#else
#define _CCPRINTF_ 0
#endif

#if defined (__TMS320C6727__) || defined (__RX610__)
  void ccprintfdummy (const char *format, ...);
  #if defined (DEFINECCPRINTFDUMMY)
    void ccprintfdummy (const char *format, ...)  { ; }
  #endif
  #define ccprintf(fmt, ...)          do { ccprintfdummy("" fmt, ##__VA_ARGS__); } while (0)
  #define ccsprintf(strbuf, fmt, ...) do { ccsprintfdummy(strbuf, "" fmt, ##__VA_ARGS__); } while (0)
#else
  #define ccprintf(fmt, ...) \
      do { if (_CCPRINTF_)  fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while (0)
#endif


// Functionality specific to TMS320C6727 DSP compiler for the FGC3.1

#if defined (__TMS320C6727__)

#define CC_MEMORY_BARRIER  _restore_interrupts(_disable_interrupts())

#include "../../../fgc/fgc3/c6727/inc/panic.h"

#define CC_ASSERT(expr) dsp_assert(expr)

#include "../../../fgc/fgc3/inc/inet.h"

// Functionality specific to the RX610 MCU compiler for the FGC3.1

#elif defined (__RX610__) || defined(NOT_HAVE_NETINET_IN_H)

#define CC_MEMORY_BARRIER      __sync_synchronize()
#define CC_MEMORY_CTZ(bitmask) __builtin_ctz(bitmask)

#include <assert.h>

#define CC_ASSERT(expr)

#include "../../../fgc/fgc3/inc/inet.h"

// Functionality specific to llvm on Mac (__GNUC__ is also set so this must come before the __GNUC__ test below)

#elif defined (__llvm__)

#define CC_MEMORY_BARRIER      __sync_synchronize()
#define CC_MEMORY_CTZ(bitmask) __builtin_ctz(bitmask)

#include <assert.h>

#define CC_ASSERT(expr) assert(expr);

#include <netinet/in.h>

// Functionality specific to gcc

#elif defined (__GNUC__)

#define CC_MEMORY_BARRIER      __sync_synchronize()
#define CC_MEMORY_CTZ(bitmask) __builtin_ctz(bitmask)

#include <assert.h>

#define CC_ASSERT(expr) assert(expr)

#include <netinet/in.h>

#endif

// Define static assert according to C version

#if __STDC_VERSION__ == 201112L

// C11 has built-in static assertion

#include <assert.h>

#define CC_STATIC_ASSERT(expr, msg) static_assert(expr, #msg)

#else

// Before C11 we need to improvise a static assertion macro

#define CC_STATIC_ASSERT(expr, msg) extern int (*static_assert_failed__##msg(void)) [expr ? 1 : -1]

#endif

// Test macro for single/multi-bit set in an integer - false if multiple bits are set - true if zero or 1 bit is set

#define CC_ARE_MULTIPLE_BITS_SET(bit_mask) (((bit_mask) & (bit_mask - 1)) == 0)

//! Measurement signal structure

struct CC_meas_signal
{
    cc_float    signal;             //!< Measurement signal
    bool        is_valid;           //!< Measurement signal is valid flag
};

//! Debug union

union CC_debug
{
    uint32_t                    u[CC_DEBUG_LEN];
    cc_float                    f[CC_DEBUG_LEN];
};

//! Nanosecond resolution time structure - can describe an absolute unsigned Unix time or a signed relative time

struct CC_ns_time                   //! Unix time = secs.abs + ns * 1E-9,  Relative time = secs.rel + ns * 1E-9
{
    union {
        uint32_t    abs;            //!< Unsigned absolute time (Unix time)
        int32_t     rel;            //!< Signed relative time
    } secs;

    int32_t         ns;             //!< Time in nanoseconds - always positive (0-999999999)
};

//! Microsecond resolution time structure - can describe an absolute unsigned Unix time or a signed relative time

struct CC_us_time                   //! Unix time = secs.abs + us * 1E-6,  Relative time = secs.rel + us * 1E-6
{
    union {
        uint32_t    abs;            //!< Unsigned absolute time (Unix time)
        int32_t     rel;            //!< Signed relative time
    } secs;

    int32_t         us;             //!< Time in nanoseconds - always positive (0-999999)
};

//! Millisecond resolution time structure - can describe an absolute unsigned Unix time or a signed relative time

struct CC_ms_time                   //! Unix time = secs.abs + ms * 1E-3,  Relative time = secs.rel + ms * 1E-3
{
    union {
        uint32_t    abs;            //!< Unsigned absolute time (Unix time)
        int32_t     rel;            //!< Signed relative time
    } secs;

    int32_t         ms;             //!< Time in milliseconds - always positive (0-999)
};


// Static inline functions

//! Some DSP libraries do not implement nearbyint(), so this is an alternative that is used in cclibs.
//!
//! @param[in]   value         Double value
//! @returns     nearest signed integer

static inline int32_t ccNearbyint(cc_double const value)
{
    return (int32_t)(  value >= 0.0
                     ? value  + 0.5
                     : value  - 0.5 );
}


//! Convert a float value to integer with a scale factor of 1000 (e.g. for mA or mG)
//! and clip to signed 16-bit number range [-32768..+32767].
//!
//! @param[in]   value         Float value
//! @return      value*1000 clipped to [-32768..+32767]

static inline int16_t ccMilliShort(cc_float const value)
{
    int32_t milli_value = (int32_t)(1000.0 * value);

    if(milli_value > 32767)
    {
        return 32767;
    }
    else if(milli_value < -32768)
    {
        return -32768;
    }

    return (int16_t)milli_value;
}

// EOF
