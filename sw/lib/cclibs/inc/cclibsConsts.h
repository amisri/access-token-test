//! @file  cclibsConsts.h
//! @brief Converter Control libraries Public Constants header file
//!
//! This header file declares all the public constants for all CCLIBS libraries.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of cclibs.
//!
//! cclibs is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Define M_PI for older compilers which do not have M_PI in math.h

#ifndef M_PI
#define M_PI            3.1415926535897932384626
#endif

#define M_TWO_PI        (2.0*M_PI)
#define CC_DEBUG_LEN    16

//! DISABLED/ENABLED constants

enum CC_enabled_disabled
{
    CC_DISABLED,                    //!< Disabled
    CC_ENABLED                      //!< Enabled
};

//! Constants from other CCLIBS libraries

#include "../libintlk/inc/libintlk/intlkConsts.h"
#include "../libpolswitch/inc/libpolswitch/polswitchConsts.h"
#include "../libref/inc/libref/refConsts.h"
#include "../libreg/inc/libreg/regConsts.h"
#include "../libsig/inc/libsig/sigConsts.h"

// EOF
