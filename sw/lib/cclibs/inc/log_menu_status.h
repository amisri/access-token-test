//! @file  log_menu_status.h
//! @brief Constants for the LOG.MENU.STATUS bit mask

#ifndef LOG_MENU_STATUS_H
#define LOG_MENU_STATUS_H

// Bit index constants

enum LOG_menu_status_bit
{
    // Liblog status bits

    LOG_MENU_STATUS_CYCLIC     ,                                     //!< Static flag  : Log menu is for a cyclic log
    LOG_MENU_STATUS_ANALOG     ,                                     //!< Static flag  : Log menu is for a analog log
    LOG_MENU_STATUS_POSTMORTEM ,                                     //!< Static flag  : Log menu is in the post-mortem
    LOG_MENU_STATUS_CONTINUOUS ,                                     //!< Static flag  : Log menu is for a continuous log
    LOG_MENU_STATUS_FREEZABLE  ,                                     //!< Static flag  : Log menu is for a freezable log
    LOG_MENU_STATUS_MPX        ,                                     //!< Static flag  : Log menu has a multiplexer
    LOG_MENU_STATUS_PM_BUF     ,                                     //!< Static flag  : Log menu should be part of the post-mortem buf
    LOG_MENU_STATUS_DISABLED   ,                                     //!< Dynamic flag : Log menu is disabled
    LOG_MENU_STATUS_RUNNING    ,                                     //!< Dynamic flag : Log menu us for a running log

    // Application status bits

    LOG_MENU_STATUS_SAVE       ,                                     //!< Dynamic flag : Log menu should be read-out and stored by the logger now
    LOG_MENU_STATUS_FUTURE     ,                                     //!< Static flag  : Log menu is for a future data log
    LOG_MENU_STATUS_DIM        ,                                     //!< Static flag  : Log menu is for a DIM log
    LOG_MENU_STATUS_VS1        ,                                     //!< Static flag  : Log menu is for a VS 1 log
    LOG_MENU_STATUS_VS2        ,                                     //!< Static flag  : Log menu is for a VS 2 log
    LOG_MENU_STATUS_TABLE      ,                                     //!< Static flag  : Log menu is for a log returning a table
    LOG_MENU_STATUS_ALIAS      ,                                     //!< Static flag  : Log menu is for an ALIAS log
    LOG_NUM_MENU_STATUS_BITS
};

// Bit mask constants

enum LOG_menu_status_bit_mask
{
    LOG_MENU_STATUS_CYCLIC_BIT_MASK     = (1 << LOG_MENU_STATUS_CYCLIC    )  ,
    LOG_MENU_STATUS_ANALOG_BIT_MASK     = (1 << LOG_MENU_STATUS_ANALOG    )  ,
    LOG_MENU_STATUS_POSTMORTEM_BIT_MASK = (1 << LOG_MENU_STATUS_POSTMORTEM)  ,
    LOG_MENU_STATUS_CONTINUOUS_BIT_MASK = (1 << LOG_MENU_STATUS_CONTINUOUS)  ,
    LOG_MENU_STATUS_FREEZABLE_BIT_MASK  = (1 << LOG_MENU_STATUS_FREEZABLE )  ,
    LOG_MENU_STATUS_MPX_BIT_MASK        = (1 << LOG_MENU_STATUS_MPX       )  ,
    LOG_MENU_STATUS_PM_BUF_BIT_MASK     = (1 << LOG_MENU_STATUS_PM_BUF    )  ,
    LOG_MENU_STATUS_DISABLED_BIT_MASK   = (1 << LOG_MENU_STATUS_DISABLED  )  ,
    LOG_MENU_STATUS_RUNNING_BIT_MASK    = (1 << LOG_MENU_STATUS_RUNNING   )  ,

    LOG_MENU_STATUS_SAVE_BIT_MASK       = (1 << LOG_MENU_STATUS_SAVE      )  ,
    LOG_MENU_STATUS_FUTURE_BIT_MASK     = (1 << LOG_MENU_STATUS_FUTURE    )  ,
    LOG_MENU_STATUS_DIM_BIT_MASK        = (1 << LOG_MENU_STATUS_DIM       )  ,
    LOG_MENU_STATUS_VS1_BIT_MASK        = (1 << LOG_MENU_STATUS_VS1       )  ,
    LOG_MENU_STATUS_VS2_BIT_MASK        = (1 << LOG_MENU_STATUS_VS2       )  ,
    LOG_MENU_STATUS_TABLE_BIT_MASK      = (1 << LOG_MENU_STATUS_TABLE     )  ,
    LOG_MENU_STATUS_ALIAS_BIT_MASK      = (1 << LOG_MENU_STATUS_ALIAS     )  ,
};

#endif

// EOF
