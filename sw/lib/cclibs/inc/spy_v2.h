//! @file  spy_v2.h
//! @brief Version 2 Spy log data header
//!
//! This file defines the constants and structures for the Version 2 Spy data format.
//!
//! The Spy v2 buffer structure is:
//!
//!  [struct LOG_spy_v2_buf_header]
//!  [struct LOG_spy_v2_sig_header for signal 1]
//!  [struct LOG_spy_v2_sig_header for signal 2]
//!                      ...
//!  [struct LOG_spy_v2_sig_header for signal N]
//!  [Signal data...]
//!
//! Where analog [Signal data] can be in rows:
//!  [array of all samples for signal 1]
//!  [array of all samples for signal 2]
//!                ...
//!  [array of all samples for signal N]
//!
//! or in columns if SPY_BUF_META_ANA_SIGS_IN_COLUMNS is set in the buffer meta byte:
//!  [first sample for signals 1-N]
//!  [second sample for signals 1-N]
//!                ...
//!  [last sample for signals 1-N]
//!
//! For digital data, the log will always contain one uint32_t per sample, which limits
//! the number of digital signals per log to 32.

#ifndef SPY_V2_H
#define SPY_V2_H

// Include files

#include <stdint.h>
#include <stdbool.h>

// Constants

#define SPY_VERSION                  2                                  //!< Spy version
#define SPY_SIG_NAME_LEN             24                                 //!< Length of then name field in the signal header (1 byte is needed for nul termination)
#define SPY_BUF_HEADER_SIZE          sizeof(struct SPY_v2_buf_header)   //! Size of Spy buffer header
#define SPY_SIG_HEADER_SIZE          sizeof(struct SPY_v2_sig_header)   //! Size of Spy signal header

// Support far strings if SPY_FAR is defined, otherwise define it to be empty

#ifndef SPY_FAR
#define SPY_FAR
#endif

// Enums

enum SPY_buf_meta_bit_mask
{
    SPY_BUF_META_LITTLE_ENDIAN       = 0x01 ,                           //!< Log header and data are little endian
    SPY_BUF_META_ANA_SIGNALS         = 0x02 ,                           //!< Log contains analog signals instead of digital signals
    SPY_BUF_META_ANA_SIGS_IN_COLUMNS = 0x04 ,                           //!< Analog signal data is in columns instead of rows
    SPY_BUF_META_TIMESTAMP_IS_LST    = 0x08 ,                           //!< Timestamp is the Last Sample Time (LST) instead of the First Sample Time (FST)
};

enum SPY_sig_meta_bit_mask
{
    SPY_SIG_META_STEPS               = 0x01 ,                           //!< Analog signal should be displayed with trailing step interpolation
    SPY_SIG_META_USE_SCALING         = 0x02 ,                           //!< Analog signal should be transformed with gain and offset
};

enum SPY_sig_type
{
    SPY_TYPE_FLOAT    ,                                                 //!< Analog signal data is floating point
    SPY_TYPE_SIGNED   ,                                                 //!< Analog signal data is signed integer
    SPY_TYPE_UNSIGNED ,                                                 //!< Analog signal data is unsigned integer
};

// Structures

struct SPY_v2_buf_header
{
    uint8_t                     version;                                //!< [ 0] Buffer version
    uint8_t                     meta;                                   //!< [ 1] Bit mask for SPY_BUF_META_XXXX
    uint16_t                    num_signals;                            //!< [ 2] Number of signals
    uint32_t                    num_samples;                            //!< [ 4] Number of samples
    uint32_t                    time_origin_s;                          //!< [ 8] Time origin (Unix time)
    uint32_t                    time_origin_us;                         //!< [12] Time origin (microseconds)
    uint32_t                    timestamp_s;                            //!< [16] Time of the first or last sample (Unix time)
    uint32_t                    timestamp_us;                           //!< [20] Time of the first or last sample (microseconds)
    uint32_t                    period_us;                              //!< [24] Sampling period
};                                                                      //    28 bytes

struct SPY_v2_sig_header
{
    uint8_t                     meta;                                   //!< [ 0] Bit mask for SPY_SIG_META_XXXX
    uint8_t                     dig_bit_index;                          //!< [ 1] Digital bit index (0-31)
    uint8_t                     type;                                   //!< [ 2] SPY_TYPE_XXXX (for analog data only)
    uint8_t                     size;                                   //!< [ 3] Element size in bytes (2 or 4) (for analog data only)
    float                       gain;                                   //!< [ 4] Displayed signal = gain * signal_value + offset if SPY_SIG_META_USE_SCALING is set in meta
    float                       offset;                                 //!< [ 8] See above
    int32_t                     time_offset_us;                         //!< [12] Time offset for this signal (microseconds)
    char                        name[SPY_SIG_NAME_LEN];                 //!< [16] Signal name and (optional) units "NAME_(Units)" (null terminated)
};                                                                      //    40 bytes

// Spy functions

#ifndef SPY_GLOBALS

// SPY_GLOBALS is NOT defined so declare the global Spy functions

//! Set the Spy buffer header structure.
//!
//! No parameter checks are included.
//!
//! @param[out]  buf_header           Pointer to Spy buffer header to set
//! @param[in]   little_endian        True if spy buffer is little endian (header and data)
//! @param[in]   ana_signals          True if spy buffer contains analog signals (rather than digital)
//! @param[in]   ana_sigs_in_columns  True if spy buffer contains analog signals in columns (rather than rows)
//! @param[in]   timestamp_is_lst     True if the timestamp refers to the last sample
//! @param[in]   num_signals          Number of signals
//! @param[in]   num_samples          Number of samples
//! @param[in]   time_origin_s        Time origin (Unix time)
//! @param[in]   time_origin_us       Time origin (microseconds)
//! @param[in]   timestamp_s          Time of first or last sample of this signal (Unix time)
//! @param[in]   timestamp_us         Time of first or last sample of this signal (microseconds),
//! @param[in]   period_us            Log sampling period in microseconds

extern void spySetBufHeader(struct SPY_v2_buf_header * buf_header,
                            bool                       little_endian,
                            bool                       ana_signals,
                            bool                       ana_sigs_in_columns,
                            bool                       timestamp_is_lst,
                            uint16_t                   num_signals,
                            uint32_t                   num_samples,
                            uint32_t                   time_origin_s,
                            uint32_t                   time_origin_us,
                            uint32_t                   timestamp_s,
                            uint32_t                   timestamp_us,
                            uint32_t                   period_us);


//! Copies the name and optionally the units to the name field of the Spy signal header
//! and assures that name string is null-terminated/null-padded.
//!
//! If the units are specified, they will be added to the name as a suffix: "_(units)"
//! The name and/or units will be truncated if they are too long.
//!
//! @param[out]  sig_header  Pointer to Spy signal header
//! @param[in]   name        Pointer to name string
//! @param[in]   units       Pointer to units string (NULL or "" if not required)

extern void spySetSignalName(struct SPY_v2_sig_header * sig_header,
                             char               const * name,
                             char               const * units);


//! Set the Spy signal header for an analog signal.
//!
//! No parameter checks are included.
//!
//! @param[out]  sig_header      Pointer to Spy signal header to set
//! @param[in]   step            True if the signal is to be shown with trailing step interpolation
//! @param[in]   use_scaling     True if the signal should be scaled using the gain and offset supplied
//! @param[in]   sig_type        Signal type (Float, Signed or Unsigned integer)
//! @param[in]   size            Data size (2 or 4 bytes)
//! @param[in]   gain            displayed_signal = gain * signal_value + offset if use_scaling is true
//! @param[in]   offset          See above
//! @param[in]   time_offset_us  Time offset when showing the signal in PowerSpy
//! @param[in]   name            Signal name
//! @param[in]   units           Signal units (NULL or "" if not required)

extern void spySetAnaSigHeader(struct SPY_v2_sig_header * sig_header,
                               bool                       step,
                               bool                       use_scaling,
                               enum SPY_sig_type          sig_type,
                               uint8_t                    size,
                               float                      gain,
                               float                      offset,
                               int32_t                    time_offset_us,
                               char               const * name,
                               char               const * units);


//! Set the Spy signal header for a digital signal.
//!
//! No parameter checks are included.
//!
//! @param[out]  sig_header      Pointer to Spy signal header to set
//! @param[in]   dig_bit_index   Signal bit index (0-31)
//! @param[in]   time_offset_us  Time origin (microseconds)
//! @param[in]   name            First sample time (Unix time)

extern void spySetDigSigHeader(struct SPY_v2_sig_header * sig_header,
                               uint8_t                    dig_bit_index,
                               int32_t                    time_offset_us,
                               char               const * name);
#else

// SPY_GLOBALS is defined so define the global Spy functions

void spySetBufHeader(struct SPY_v2_buf_header * const buf_header,
                     bool                       const little_endian,
                     bool                       const ana_signals,
                     bool                       const ana_sigs_in_columns,
                     bool                       const timestamp_is_lst,
                     uint16_t                   const num_signals,
                     uint32_t                   const num_samples,
                     uint32_t                   const time_origin_s,
                     uint32_t                   const time_origin_us,
                     uint32_t                   const timestamp_s,
                     uint32_t                   const timestamp_us,
                     uint32_t                   const period_us)
{
    buf_header->version = SPY_VERSION;

    // Set the buffer header meta bit mask

    buf_header->meta = 0;

    if(little_endian == true)
    {
        buf_header->meta |= SPY_BUF_META_LITTLE_ENDIAN;
    }

    if(ana_signals == true)
    {
        buf_header->meta |= SPY_BUF_META_ANA_SIGNALS;

        if(ana_sigs_in_columns == true)
        {
            buf_header->meta |= SPY_BUF_META_ANA_SIGS_IN_COLUMNS;
        }
    }

    if(timestamp_is_lst == true)
    {
        buf_header->meta |= SPY_BUF_META_TIMESTAMP_IS_LST;
    }

    // Set the rest of the buffer header

    buf_header->num_signals    = num_signals;
    buf_header->num_samples    = num_samples;
    buf_header->time_origin_s  = time_origin_s;
    buf_header->time_origin_us = time_origin_us;
    buf_header->timestamp_s    = timestamp_s;
    buf_header->timestamp_us   = timestamp_us;
    buf_header->period_us      = period_us;
}



void spySetSignalName(struct SPY_v2_sig_header * const sig_header,
                      char               const *       SPY_FAR name,
                      char               const *       SPY_FAR units)
{
    char * dest = sig_header->name;

    // Copy the name, start counting characters from 1 to keep one space for a nul

    uint16_t n;

    if(name == 0)
    {
        return;
    }

    for(n = 1 ; n < SPY_SIG_NAME_LEN && *name != '\0'; n++)
    {
        *(dest++) = *(name++);
    }

    // If units are provided

    if(units != 0 && *units != '\0' && n <= (SPY_SIG_NAME_LEN - 3))
    {
        // Add the units prefix

        *(dest++) = '_';
        *(dest++) = '(';

        // Copy as much of the units as possible

        for(n += 3 ; n < SPY_SIG_NAME_LEN && *units != '\0' ; n++)
        {
            *(dest++) = *(units++);
        }

        // Add units suffix

        *(dest++) = ')';
    }

    // Terminate and pad to the end with nuls

    while(n++ < SPY_SIG_NAME_LEN)
    {
        *(dest++) = '\0';
    }
}



void spySetAnaSigHeader(struct SPY_v2_sig_header * const sig_header,
                        bool                       const step,
                        bool                       const use_scaling,
                        enum SPY_sig_type          const sig_type,
                        uint8_t                    const size,
                        float                      const gain,
                        float                      const offset,
                        int32_t                    const time_offset_us,
                        char               const * const SPY_FAR name,
                        char               const * const SPY_FAR units)
{
    // Set the signal header meta bit mask and the gain and offset when scaling is used

    sig_header->meta = 0;

    if(step == true)
    {
        sig_header->meta |= SPY_SIG_META_STEPS;
    }

    if(use_scaling == true)
    {
        sig_header->meta |= SPY_SIG_META_USE_SCALING;

        sig_header->gain   = gain;
        sig_header->offset = offset;
    }
    else
    {
        sig_header->gain   = 1.0F;
        sig_header->offset = 0.0F;
    }

    // Set the rest of the signal header

    sig_header->dig_bit_index  = 0;
    sig_header->type           = sig_type;
    sig_header->size           = size;
    sig_header->time_offset_us = time_offset_us;

    // Set to name field to combine the signal name and (optional) units

    spySetSignalName(sig_header, name, units);
}



void spySetDigSigHeader(struct SPY_v2_sig_header * const sig_header,
                        uint8_t                    const dig_bit_index,
                        int32_t                    const time_offset_us,
                        char               const * const SPY_FAR name)
{
    // Set the signal header meta bit mask and the rest of the signal header

    sig_header->meta           = 0;
    sig_header->dig_bit_index  = dig_bit_index;
    sig_header->type           = SPY_TYPE_UNSIGNED;
    sig_header->size           = 4;
    sig_header->gain           = 0.0F;
    sig_header->offset         = 0.0F;
    sig_header->time_offset_us = time_offset_us;

    // Set to name field - no units apply to digital signals

    spySetSignalName(sig_header, name, NULL);
}
#endif

#endif

// EOF
