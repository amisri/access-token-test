//! @file      intlkConsts.h
//! @brief     Converter Control Interlocks library public constants header file
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libintlk.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define INTLK_NUM_CHANS         4                                       //!< Number of interlock channels
#define INTLK_LATCH_LEN        24                                       //!< Length of tests_ok_mask latch array

// Enums

enum INTLK_chan_ctrl                                                    //!  Interlock channel control
{
    INTLK_CHAN_OFF,                                                     //!< Turn off the channel (it will be NOT OK)
    INTLK_CHAN_NOT_OK,                                                  //!< Channel is active but force to be NOT OK
    INTLK_CHAN_OK,                                                      //!< Channel is active but force to be OK
    INTLK_CHAN_CONDITIONAL                                              //!< Channel will be OK if all six tests are OK or overridden
};

// EOF
