//! @file  intlkPars.h
//! @brief Converter Control Interlocks library : Parameters header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libintlk.
//!
//! libintlk is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global parameters

#define intlkParValue(INTLK_MGR, PAR_NAME)    ((INTLK_MGR)->INTLK_PAR_ ## PAR_NAME)
#define intlkParPointer(INTLK_MGR, PAR_NAME)  (&(INTLK_MGR)->INTLK_PAR_ ## PAR_NAME)

#define INTLK_PAR_USE_CYC_SEL                  use_cyc_sel

// Channel parameters

#define intlkChanParArray(INTLK_PARS,PAR_NAME)               ((INTLK_PARS)->INTLK_PAR_ ## PAR_NAME)
#define intlkChanParValue(INTLK_PARS,PAR_NAME,CHAN_INDEX)   *((INTLK_PARS)->INTLK_PAR_ ## PAR_NAME+CHAN_INDEX)
#define intlkChanParPointer(INTLK_PARS,PAR_NAME,CHAN_INDEX)  ((INTLK_PARS)->INTLK_PAR_ ## PAR_NAME+CHAN_INDEX)

#define INTLK_PAR_CHAN_CONTROL                 chan_control
#define INTLK_PAR_REF_STATE                    ref_state
#define INTLK_PAR_MAX_REG_ERR                  max_reg_err
#define INTLK_PAR_V_MID                        v_meas.mid
#define INTLK_PAR_V_DELTA                      v_meas.delta
#define INTLK_PAR_V_MAX_RATE                   v_meas.max_rate
#define INTLK_PAR_I_MID                        i_meas.mid
#define INTLK_PAR_I_DELTA                      i_meas.delta
#define INTLK_PAR_I_MAX_RATE                   i_meas.max_rate

// EOF
