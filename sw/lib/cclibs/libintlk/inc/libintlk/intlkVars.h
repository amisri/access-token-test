//! @file  intlkVars.h
//! @brief Converter Control Interlocks library : Variables header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libintlk.
//!
//! libintlk is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#define intlkVarValue(INTLK_MGR, VAR_NAME)    ((INTLK_MGR)->INTLK_VAR_ ## VAR_NAME)
#define intlkVarPointer(INTLK_MGR, VAR_NAME)  (&(INTLK_MGR)->INTLK_VAR_ ## VAR_NAME)

// Latched values

#define intlkLatchVarArray(INTLK_LATCH, VAR_NAME)  ((INTLK_LATCH)->INTLK_VAR_LATCH_ ## VAR_NAME)


#define INTLK_VAR_LIMIT_0_MAX_REG_ERR          limits[0].max_reg_err
#define INTLK_VAR_LIMIT_0_MIN_V_MEAS           limits[0].min_v_meas
#define INTLK_VAR_LIMIT_0_MAX_V_MEAS           limits[0].max_v_meas
#define INTLK_VAR_LIMIT_0_MAX_V_RATE           limits[0].max_v_rate
#define INTLK_VAR_LIMIT_0_MIN_I_MEAS           limits[0].min_i_meas
#define INTLK_VAR_LIMIT_0_MAX_I_MEAS           limits[0].max_i_meas
#define INTLK_VAR_LIMIT_0_MAX_I_RATE           limits[0].max_i_rate

#define INTLK_VAR_LIMIT_1_MAX_REG_ERR          limits[1].max_reg_err
#define INTLK_VAR_LIMIT_1_MIN_V_MEAS           limits[1].min_v_meas
#define INTLK_VAR_LIMIT_1_MAX_V_MEAS           limits[1].max_v_meas
#define INTLK_VAR_LIMIT_1_MAX_V_RATE           limits[1].max_v_rate
#define INTLK_VAR_LIMIT_1_MIN_I_MEAS           limits[1].min_i_meas
#define INTLK_VAR_LIMIT_1_MAX_I_MEAS           limits[1].max_i_meas
#define INTLK_VAR_LIMIT_1_MAX_I_RATE           limits[1].max_i_rate

#define INTLK_VAR_LIMIT_2_MAX_REG_ERR          limits[2].max_reg_err
#define INTLK_VAR_LIMIT_2_MIN_V_MEAS           limits[2].min_v_meas
#define INTLK_VAR_LIMIT_2_MAX_V_MEAS           limits[2].max_v_meas
#define INTLK_VAR_LIMIT_2_MAX_V_RATE           limits[2].max_v_rate
#define INTLK_VAR_LIMIT_2_MIN_I_MEAS           limits[2].min_i_meas
#define INTLK_VAR_LIMIT_2_MAX_I_MEAS           limits[2].max_i_meas
#define INTLK_VAR_LIMIT_2_MAX_I_RATE           limits[2].max_i_rate

#define INTLK_VAR_LIMIT_3_MAX_REG_ERR          limits[3].max_reg_err
#define INTLK_VAR_LIMIT_3_MIN_V_MEAS           limits[3].min_v_meas
#define INTLK_VAR_LIMIT_3_MAX_V_MEAS           limits[3].max_v_meas
#define INTLK_VAR_LIMIT_3_MAX_V_RATE           limits[3].max_v_rate
#define INTLK_VAR_LIMIT_3_MIN_I_MEAS           limits[3].min_i_meas
#define INTLK_VAR_LIMIT_3_MAX_I_MEAS           limits[3].max_i_meas
#define INTLK_VAR_LIMIT_3_MAX_I_RATE           limits[3].max_i_rate

#define INTLK_VAR_OP_REF_STATE                 vars.op_ref_state
#define INTLK_VAR_ABS_REG_ERR                  vars.abs_reg_err
#define INTLK_VAR_V_MEAS                       vars.v_meas
#define INTLK_VAR_ABS_V_RATE                   vars.abs_v_rate
#define INTLK_VAR_I_MEAS                       vars.i_meas
#define INTLK_VAR_ABS_I_RATE                   vars.abs_i_rate

#define INTLK_VAR_TESTS_OK_MASK                tests_ok_mask
#define INTLK_VAR_CHANNELS_OK_MASK             channels_ok_mask

#define INTLK_VAR_LATCH_TESTS_OK_MASK          tests_ok_mask
#define INTLK_VAR_LATCH_V_MEAS                 v_meas
#define INTLK_VAR_LATCH_I_MEAS                 i_meas
#define INTLK_VAR_LATCH_COUNT                  count

// EOF
