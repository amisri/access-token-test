//! @file  libintlk.h
//! @brief Converter Control Interlocks library header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libintlk.
//!
//! libintlk is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!
//! @mainpage CERN Converter Control Library: Interlocks
//!
//! Libintlk supports interlocks on current, voltage and regulation error signals and the
//! reference state machine. It processes four interlock channels with six tests for each channel.
//! The Boolean result for each channel can be forced to OK, NOT OK, or can be CONDITIONAL
//! on the results of all six tests, as illustrated in this figure:
//!
//! @image html libintlk.png
//!
//! The five input signals are common to the four channels, but the test limits parameters are defined per channel.
//!
//! <h3> Input signals </h3>
//!
//! Five signals are passed to the library by reference. Pointers to the signals are provided
//! when the library is initialised:
//!
//!     1. CYC_SEL            - cycle selector
//!     2. REF_STATE_BIT_MASK - reference state machine as a bit mask
//!     3. ABS_REG_ERR        - absolute regulation error (field, current or voltage)
//!     4. V_MEAS             - measured voltage
//!     5. I_MEAS             - measured current
//!
//! A global parameter called USE_CYC_SEL controls how CYC_SEL is used:
//!
//!  * If USE_CYC_SEL is ENABLED,  then the channel parameters are selected using CYC_SEL.
//!  * If USE_CYC_SEL is DISABLED, then the channel parametersare taken from the first element
//!    in the parameters structure array, i.e. the cycle selector is forced to zero.
//!
//! The rate of change of current and voltage are computed internally by taking the difference
//! of the measurements on consecutive iterations. This results in two more signals:
//!
//!     6. ABS_V_RATE - absolute rate of change of measured voltage
//!     7. ABS_I_RATE - absolute rate of change of measured current
//!
//! <h3> Interlock Tests </h3>
//!
//! The input signals are used in the six interlock tests for each channel:
//!
//!     T1. Is REF_STATE an operational state?
//!
//! The REF_STATE test checks if the actual reference state is part of the set of operational
//! reference states, defined in the global parameter OP_REF_STATES_MASK. The channel parameter
//! REF_STATE allows the REF_STATE test be overridden by setting it to DISABLED.
//!
//!     T2. Is ABS_REG_ERR within limits?
//!
//! The ABS_REG_ERR test checks if the absolute regulation error is less than
//! or equal to a maximum limit.
//!
//!     T3. Is V_MEAS within limits?
//!
//! The V_MEAS test checks if the measured voltage is within a window around a mid-point
//! voltage value. The window is defined as mid +/- delta. The window includes the
//! limiting values.
//!
//!     T4. Is ABS_V_RATE within limits?
//!
//! The ABS_V_RATE test checks if the absolute rate of change of voltage is less than
//! or equal to a maximum limit.
//!
//!     T5. Is I_MEAS within limits?
//!
//! The I_MEAS test checks if the measured current is within a window around a mid-point
//! current value. The window is defined as mid +/- delta. The window includes the
//! limiting values.
//!
//!     T6. Is ABS_I_RATE within limits?
//!
//! The ABS_I_RATE test checks if the absolute rate of change of current is less than
//! or equal to a maximum limit.
//!
//! <h3> Global Parameters </h3>
//!
//! The library has two global parameters that apply to all channels and all cycle selectors:
//!
//!     USE_CYC_SEL        | intlkmgr->use_cyc_sel        | enum CC_enabled_disabled
//!     OP_REF_STATES_MASK | intlkmgr->op_ref_states_mask | uint32_t
//!
//! When USE_CYC_SEL set to DISABLED, the cycle selector used to identify the parameters will
//! be forced to zero, so the same limits will apply to all cycles.
//!
//! The OP_REF_STATES_MASK allows the application to specify the set of reference states that the
//! library will consider operational.
//!
//! <h3> Channel Parameters </h3>
//!
//! The parameters for the six tests for four channel are stored in a struct INTLK_pars.
//! The application can define an array of these structures, to cover all the cycle selectors required.
//! The parameter structure can be embedded in a larger structure provided the step between elements
//! is provided when the library is initialised.
//!
//! If the application defines the parameters as:
//!
//!   struct INTLK_pars intlk_pars[NUM_CYC_SELS];
//!
//! Then the parameters controlling the tests are:
//!
//!      CHAN_CONTROL | intlk_pars[cyc_sel].chan_control   [chan_index] | enum INTLK_chan_ctrl
//!      REF_STATE    | intlk_pars[cyc_sel].ref_state      [chan_index] | enum CC_enabled_disabled
//!      MAX_REG_ERR  | intlk_pars[cyc_sel].max_reg_err    [chan_index] | cc_float
//!      V_MID        | intlk_pars[cyc_sel].v_meas.mid     [chan_index] | cc_float
//!      V_DELTA      | intlk_pars[cyc_sel].v_meas.delta   [chan_index] | cc_float
//!      V_MAX_RATE   | intlk_pars[cyc_sel].v_meas.max_rate[chan_index] | cc_float
//!      I_MID        | intlk_pars[cyc_sel].i_meas.mid     [chan_index] | cc_float
//!      I_DELTA      | intlk_pars[cyc_sel].i_meas.delta   [chan_index] | cc_float
//!      I_MAX_RATE   | intlk_pars[cyc_sel].i_meas.max_rate[chan_index] | cc_float
//!
//! The parameters array can be in slow memory as it is not used on every real-time iteration.
//!
//! <h3> Overriding an interlock test </h3>
//!
//! Every test can be overridden per channel, meaning that the test result will be OK irrespective of the input value.
//! Tests are overridden by using the parameter values:
//!
//!      T1. REF_STATE    |  ref_state == DISABLED
//!      T2. ABS_REG_ERR  |  max_reg_err     <= 0.0
//!      T3. V_MEAS       |  v_meas.delta    <= 0.0
//!      T4. ABS_V_RATE   |  v_meas.max_rate <= 0.0
//!      T5. I_MEAS       |  i_meas.delta    <= 0.0
//!      T6. ABS_I_RATE   |  i_meas.max_rate <= 0.0
//!
//! <h3> Channel Control </h3>
//!
//! The Channel Control Parameter array defines whether the status of the channel is always OK, always not OK,
//! or is conditional and is based on the result of the tests.
//!
//! <h3> Tests OK bit mask </h3>
//!
//! In order to compute the channels OK mask, intlkRT() compiles the results of all the tests for the four
//! channels into a single 32-bit variable inside struct INTLK_mgr called tests_ok_mask:
//!
//!      +------------+--------+--------+--------+--------+------------------------------------------+
//!      | BIT LABEL  | Chan 0 | Chan 1 | Chan 2 | Chan 3 | Meaning if bit is one                    |
//!      +------------+--------+--------+--------+--------+------------------------------------------+
//!      |REF_STATE_OK| Bit  0 | Bit  8 | Bit 16 | Bit 24 | Reference state is operational*          |
//!      | REG_ERR_OK | Bit  1 | Bit  9 | Bit 17 | Bit 25 | Regulation error is within limits*       |
//!      | V_MEAS_OK  | Bit  2 | Bit 10 | Bit 18 | Bit 26 | Voltage measurement is within limits*    |
//!      | V_RATE_OK  | Bit  3 | Bit 11 | Bit 19 | Bit 27 | Voltage rate is within limits*           |
//!      | I_MEAS_OK  | Bit  4 | Bit 12 | Bit 20 | Bit 28 | Current measurement is within limits*    |
//!      | I_RATE_OK  | Bit  5 | Bit 13 | Bit 21 | Bit 29 | Current rate within is limits*           |
//!      +------------+--------+--------+--------+--------+------------------------------------------+
//!      |  TESTS_OK  | Bit  6 | Bit 14 | Bit 22 | Bit 30 | Channel's tests are all OK or overridden |
//!      +------------+--------+--------+--------+--------+------------------------------------------+
//!      |  CHAN_OK   | Bit  7 | Bit 15 | Bit 23 | Bit 31 | Channel is OK                            |
//!      +------------+--------+--------+--------+--------+------------------------------------------+
//!                                                         * or the test is overridden
//!
//!  Bits 7, 15, 23 and 31 are concatenated into the channels_ok_mask as bits 0-3.
//!
//! <h3> Channels OK bit mask </h3>
//!
//! The real-time function, intlkRT() will return a bit mask corresponding to overall status of each channel.
//! Bits 0-3 will be 1 if the corresponding channel is OK (either because it's forced to OK
//! or because all the tests are OK or overridden).
//!

#pragma once

#include "cclibs.h"

//! Include header files

#include "libintlk/intlkPars.h"
#include "libintlk/intlkVars.h"

//! Measurement interlock parameters structure

struct INTLK_meas_pars
{
    cc_float                    mid     [INTLK_NUM_CHANS];              //!< Measurement interlock mid-point
    cc_float                    delta   [INTLK_NUM_CHANS];              //!< Measurement interlock threshold (mid +/- delta)
    cc_float                    max_rate[INTLK_NUM_CHANS];              //!< Max (absolute) rate of measurement
};

//! Interlock parameters structure

struct INTLK_pars
{
    enum INTLK_chan_ctrl        chan_control[INTLK_NUM_CHANS];          //!< Channel control parameter
    enum CC_enabled_disabled    ref_state   [INTLK_NUM_CHANS];          //!< Enabled if ref_state must be an operational state
    cc_float                    max_reg_err [INTLK_NUM_CHANS];          //!< Max (absolute) regulation error
    struct INTLK_meas_pars      v_meas;                                 //!< Voltage measurement interlock parameters
    struct INTLK_meas_pars      i_meas;                                 //!< Current measurement interlock parameters
};

//! Interlock limits structure

struct INTLK_limits
{
    cc_float                    max_reg_err;                            //!< Maximum (absolute) regulation error
    cc_float                    min_v_meas;                             //!< Minimum measured voltage
    cc_float                    max_v_meas;                             //!< Maximum measured voltage
    cc_float                    max_v_rate;                             //!< Maximum (absolute) rate of measured voltage
    cc_float                    min_i_meas;                             //!< Minimum measured current
    cc_float                    max_i_meas;                             //!< Maximum measured current
    cc_float                    max_i_rate;                             //!< Maximum (absolute) rate of measured current
};

//! Interlock variables structure

struct INTLK_vars
{
    bool                        op_ref_state;                           //!< True if ref state is an operational state
    cc_float                    abs_reg_err;                            //!< Absolute regulation error
    cc_float                    v_meas;                                 //!< Measured voltage
    cc_float                    abs_v_rate;                             //!< Absolute measured rate of change of voltage
    cc_float                    i_meas;                                 //!< Measured current
    cc_float                    abs_i_rate;                             //!< Absolute measured rate of change of current
};

//! Latch array for tests_ok_mask

struct INTLK_latch_values
{
    uint32_t                    tests_ok_mask[INTLK_LATCH_LEN];         //!< Array of latched tests_ok_mask values
    cc_float                    v_meas       [INTLK_LATCH_LEN];         //!< Array of latched voltage measurement values
    cc_float                    i_meas       [INTLK_LATCH_LEN];         //!< Array of latched current measurement values 
    uint32_t                    count;                                  //!< Number of elements used in latched_tests_ok_mask
};

struct INTLK_latch
{
    bool                        previous;                               //!< Previous value of latch signal (allows transition detection)
    struct INTLK_latch_values * values;                                 //!< Step in bytes to access the latched values for a given cycle selector
    uint32_t                    values_step;                            //!< Pointer to the array of latched values structures (for cyc_sels)
    struct INTLK_latch_values * active;                                 //!< Pointer to the struct INTLK_latch_values of the active cycle selector
};

//! Interlock manager structure

struct INTLK_mgr
{
    uint32_t                  * sub_sel;                                //!< Pointer to the actual sub-device selector
    uint32_t                  * cyc_sel;                                //!< Pointer to the actual cycle selector
    uint32_t                  * ref_state_bit_mask;                     //!< Pointer to the actual reference state bit mask
    cc_float                  * abs_reg_err;                            //!< Pointer to the actual absolute regulation error
    cc_float                  * v_meas;                                 //!< Pointer to the actual voltage measurement
    cc_float                  * i_meas;                                 //!< Pointer to the actual current measurement
    struct INTLK_pars         * pars;                                   //!< Pointer to the array of parameter structures (for cyc_sels)
    uint32_t                    intlk_pars_step;                        //!< Step in bytes to access the parameters for a given cycle selector
    cc_float                    inv_iter_period;                        //!< 1/iteration period

    enum CC_enabled_disabled    use_cyc_sel;                            //!< USE_CYC_SEL global parameter
    uint32_t                    op_ref_states_mask;                     //!< OP_REF_STATES_MASK global parameter

    enum INTLK_chan_ctrl        chan_control      [INTLK_NUM_CHANS];    //!< Cached channel control parameter
    uint32_t                    test_override_mask[INTLK_NUM_CHANS];    //!< Cached test override masks: six bits (0-5) - bit is set to 1 if corresponding test is overridden
    struct INTLK_limits         limits            [INTLK_NUM_CHANS];    //!< Cached interlock limits

    struct INTLK_vars           vars;                                   //!< Interlock variables
    struct INTLK_latch          latch;                                  //!< Latch for tests_ok_mask

    uint32_t                    tests_ok_mask;                          //!< Tests OK mask for all four channels (8 bits per channel)
    uint32_t                    channels_ok_mask;                       //!< Channels OK mask (4 bits)
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize interlock manager structure.
//!
//! This function is called at startup to initialize some constants and pointers to the variables.
//! The application must allocate memory for the parameters for the interlock channels for all
//! possible cycle selectors.
//!
//! @param[out]   intlk_mgr                Pointer to the interlock manager structure.
//! @param[in]    sub_sel                  Pointer to actual sub-device selector
//! @param[in]    cyc_sel                  Pointer to actual cycle selector
//! @param[in]    ref_state_bit_mask       Pointer to actual reference state bit mask
//! @param[in]    abs_reg_err              Pointer to actual absolute regulation error
//! @param[in]    v_meas                   Pointer to the measured voltage
//! @param[in]    i_meas                   Pointer to the measured current
//! @param[in]    iter_period              Iteration period in seconds (to compute rates of change)
//! @param[in]    intlk_pars               Pointer to array of parameter structures (per cyc_sel)
//! @param[in]    intlk_pars_step          intlk_pars array step in bytes (set to zero if array is contiguous)
//! @param[in]    intlk_latch_values       Pointer to array of latched values structures (per cyc_sel)
//! @param[in]    intlk_latch_values_step  intlk_latch_values array step in bytes (set to zero if array is contiguous)
//! @param[in]    op_ref_states_mask       Mask of operational reference states

void intlkInit(struct INTLK_mgr          * intlk_mgr,
               uint32_t                  * sub_sel,
               uint32_t                  * cyc_sel,
               uint32_t                  * ref_state_bit_mask,
               cc_float                  * abs_reg_err,
               cc_float                  * v_meas,
               cc_float                  * i_meas,
               cc_float                    iter_period,
               struct INTLK_pars         * intlk_pars,
               uint32_t                    intlk_pars_step,
               struct INTLK_latch_values * intlk_latch_values,
               uint32_t                    intlk_latch_values_step,
               uint32_t                    op_ref_states_mask);


//! Check if a channel is active.
//!
//! If USE_CYC_SEL is DISABLED then only the channel control for specified channel in pars[0] is checked.
//! Otherwise, active_cyc_sel_mask indicates which cycle selectors to check for the channel. It returns true
//! if any cycle selector is active for the channel.
//!
//! NOTE: Because active_cyc_sel_mask is 32-bits, this limits the maximum cycle selector to 32.
//!
//! @param[out]   intlk_mgr              Pointer to the interlock manager structure.
//! @param[in]    active_cyc_sel_mask    Mask of active cycle selectors where bit 0 is for cyc_sel 1 and bit 31 is for cyc_sel 32
//! @param[in]    chan_index             Channel index to check (0..3)
//!
//! @retval       false                  Channel is not active
//! @retval       true                   At least one cycle selector is active for the channel

bool intlkCheckChanIsActive(struct INTLK_mgr * intlk_mgr,
                            uint32_t           active_cyc_sel_mask,
                            uint32_t           chan_index);


//! Process the interlocks for current and voltage measurements, regulation error and reference state
//!
//! The function should be called repetitively at the measurement rate. It computes the
//! rate of change of voltage and current based on the differences with the values from the
//! previous iteration.
//!
//! If process_parameters is true, the function will dedicate the iteration to calculating and
//! caching the limits for the four channels from the parameters indentified by the actual cycle selector
//! (or [0] if USE_CYC_SEL is DISABLED). Tests can be overridden if the thresholds are less than
//! or equal to zero. On these iterations, the interlock tests are not executed (to limit the maximum
//! execution time) and the returned channel OK mask will be a repeat of the previous iteration.
//!
//! If process_parameters is false, then the function will use the cached limits to test the
//! measured current, voltage and regulation error. It can also check if the reference
//! state is an operational state. The result for a channel will depend on the channel control
//! parameter, which can be OK, NOT OK or CONDITIONAL. When be OK if all the tests
//! are OK or overridden.
//!
//! The function returns a bit mask corresponding to result for each channel. Bits 0-3 will be
//! 1 if the corresponding channel is OK.
//!
//! @param[in]    intlk_mgr              Pointer to interlock manager structure.
//! @param[in]    process_parameters     When true, the parameters are processed and the limits cached and the tests_ok_mask latch count is reset.
//!                                      When false, the cached limits are used to run the interlock tests.
//! @param[in]    latch                  Transition from false to true will trigger the latching of tests_ok_mask in
//!                                      the latch array, provided space remains.
//!
//! @returns     Channel OK mask with bits 0-3 for the four channels.

uint32_t intlkRT(struct INTLK_mgr * intlk_mgr, bool process_parameters, bool latch);

#ifdef __cplusplus
}
#endif

// EOF
