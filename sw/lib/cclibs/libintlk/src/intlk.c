 //! @file  intlk.c
 //! @brief Converter Control Interlocks library functions
 //!
 //! <h4> Copyright </h4>
 //!
 //! Copyright CERN 2017. This project is released under the GNU Lesser General
 //! Public License version 3.
 //!
 //! <h4> License </h4>
 //!
 //! This file is part of libintlk.
 //!
 //! libintlk is free software: you can redistribute it and/or modify it under the
 //! terms of the GNU Lesser General Public License as published by the Free
 //! Software Foundation, either version 3 of the License, or (at your option)
 //! any later version.
 //!
 //! This program is distributed in the hope that it will be useful, but WITHOUT
 //! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 //! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 //! for more details.
 //!
 //! You should have received a copy of the GNU Lesser General Public License
 //! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libintlk.h"


// NON-RT   intlkInit

void intlkInit(struct INTLK_mgr          * const intlk_mgr,
               uint32_t                  * const sub_sel,
               uint32_t                  * const cyc_sel,
               uint32_t                  * const ref_state_bit_mask,
               cc_float                  * const abs_reg_err,
               cc_float                  * const v_meas,
               cc_float                  * const i_meas,
               cc_float                    const iter_period,
               struct INTLK_pars         * const intlk_pars,
               uint32_t                    const intlk_pars_step,
               struct INTLK_latch_values * const intlk_latch_values,
               uint32_t                    const intlk_latch_values_step,
               uint32_t                    const op_ref_states_mask)
{
    intlk_mgr->sub_sel             = sub_sel;
    intlk_mgr->cyc_sel             = cyc_sel;
    intlk_mgr->ref_state_bit_mask  = ref_state_bit_mask;
    intlk_mgr->abs_reg_err         = abs_reg_err;
    intlk_mgr->v_meas              = v_meas;
    intlk_mgr->i_meas              = i_meas;
    intlk_mgr->inv_iter_period     = 1.0F / iter_period;
    intlk_mgr->pars                = intlk_pars;
    intlk_mgr->intlk_pars_step     = intlk_pars_step > 0 ? intlk_pars_step : sizeof(struct INTLK_pars);
    intlk_mgr->latch.values        = intlk_latch_values;
    intlk_mgr->latch.values_step   = intlk_latch_values_step > 0 ? intlk_latch_values_step : sizeof(struct INTLK_latch_values);
    intlk_mgr->op_ref_states_mask  = op_ref_states_mask;
}


// NON-RT   intlkCheckChanIsActive

bool intlkCheckChanIsActive(struct INTLK_mgr * const intlk_mgr,
                            uint32_t                 active_cyc_sel_mask,
                            uint32_t           const chan_index)
{
    // If use of cyc_sel is disabled then only pars[0] channel control is important

    if(intlkParValue(intlk_mgr, USE_CYC_SEL) == CC_DISABLED)
    {
        return intlkChanParValue(intlk_mgr->pars, CHAN_CONTROL, chan_index) != INTLK_CHAN_OFF;
    }

    // cyc_sel is in use so scan the channel control for all active cycle selectors

    uint32_t cyc_sel;

    for(cyc_sel = 1 ; active_cyc_sel_mask > 0 ; cyc_sel++)
    {
        if((active_cyc_sel_mask & 0x1) != 0x0)
        {
            // cyc_sel is active to check the channel control

            struct INTLK_pars * const pars = (struct INTLK_pars *)((uint8_t *)intlk_mgr->pars + intlk_mgr->intlk_pars_step * cyc_sel);

            // If any active user is not OFF for this channel then return true

            if(intlkChanParValue(pars, CHAN_CONTROL, chan_index) != INTLK_CHAN_OFF)
            {
                return true;
            }
        }

        active_cyc_sel_mask >>= 1;
    }

    // Channel control is off for all active cycle selectors

    return false;
}


//! Convert the interlock parameters for all channels into limits and identify which tests
//! are overridden.
//!
//! @param[in,out]    intlk_mgr            Pointer to interlock manager structures.
//! @param[in]        pars                 Pointer to interlock parameters structure.

static void intlkCacheParsRT(struct INTLK_mgr * const intlk_mgr, struct INTLK_pars * const pars)
{
    struct INTLK_limits * limits = intlk_mgr->limits;
    uint32_t              chan_index;

    // Loop for each channel

    for(chan_index = 0 ; chan_index < INTLK_NUM_CHANS ; chan_index++, limits++)
    {
        // Cache the channel control parameter

        intlk_mgr->chan_control[chan_index] = (  chan_index == *intlk_mgr->sub_sel
                                               ? intlkChanParValue(pars, CHAN_CONTROL, chan_index)
                                               : INTLK_CHAN_OFF);

        // Only process the channel parameters if the channel is not OFF

        if(intlk_mgr->chan_control[chan_index] != INTLK_CHAN_OFF)
        {
            // Extract the parameters for the channel

            bool     const ref_state       = intlkChanParValue(pars, REF_STATE,   chan_index) == CC_ENABLED;
            cc_float const max_reg_err     = intlkChanParValue(pars, MAX_REG_ERR, chan_index);
            cc_float const v_meas_mid      = intlkChanParValue(pars, V_MID,       chan_index);
            cc_float const v_meas_delta    = intlkChanParValue(pars, V_DELTA,     chan_index);
            cc_float const v_meas_max_rate = intlkChanParValue(pars, V_MAX_RATE,  chan_index);
            cc_float const i_meas_mid      = intlkChanParValue(pars, I_MID,       chan_index);
            cc_float const i_meas_delta    = intlkChanParValue(pars, I_DELTA,     chan_index);
            cc_float const i_meas_max_rate = intlkChanParValue(pars, I_MAX_RATE,  chan_index);


            // Prepare the test override mask for the six tests

            intlk_mgr->test_override_mask[chan_index] = (ref_state      == false) << 0 |
                                                        (max_reg_err     <= 0.0F) << 1 |
                                                        (v_meas_delta    <= 0.0F) << 2 |
                                                        (v_meas_max_rate <= 0.0F) << 3 |
                                                        (i_meas_delta    <= 0.0F) << 4 |
                                                        (i_meas_max_rate <= 0.0F) << 5;

            // Calculate and cache the limits for regulation error, voltage and current tests

            limits->max_reg_err = max_reg_err;

            limits->max_v_meas = v_meas_mid + v_meas_delta;
            limits->min_v_meas = v_meas_mid - v_meas_delta;
            limits->max_v_rate = v_meas_max_rate;

            limits->max_i_meas = i_meas_mid + i_meas_delta;
            limits->min_i_meas = i_meas_mid - i_meas_delta;
            limits->max_i_rate = i_meas_max_rate;
        }
    }
}



//! Process the interlocks for current and voltage measurements, regulation error and reference state for one channel
//!
//! @param[in]  intlk_mgr            Pointer to interlock manager structures.
//! @param[in]  chan_index           Channel index (0-3)
//!
//! @returns    Bit mask with the 8 bits: CHAN_OK:TESTS_OK:I_RATE_OK:I_MEAS_OK:V_RATE_OK:V_MEAS_OK:REG_ERR_OK:REF_STATE_OK

static uint32_t intlkChannelRT(struct INTLK_mgr * const intlk_mgr, uint32_t const chan_index)
{
    enum INTLK_chan_ctrl const chan_control = intlk_mgr->chan_control[chan_index];
    uint32_t                   results_mask = 0;

    // Only evaluate the channel if it is not OFF

    if(chan_control != INTLK_CHAN_OFF)
    {
        // Prepare the six interlock test bits in the mask and OR with the test override mask

        struct INTLK_limits * const limits = &intlk_mgr->limits[chan_index];

        results_mask = (intlk_mgr->vars.op_ref_state                                                                       ) << 0 // REF_STATE_OK
                     | (intlk_mgr->vars.abs_reg_err <= limits->max_reg_err                                                 ) << 1 // REG_ERR_OK
                     | (intlk_mgr->vars.v_meas      >= limits->min_v_meas  &&  intlk_mgr->vars.v_meas <= limits->max_v_meas) << 2 // V_MEAS_OK
                     | (intlk_mgr->vars.abs_v_rate  <= limits->max_v_rate                                                  ) << 3 // V_RATE_OK
                     | (intlk_mgr->vars.i_meas      >= limits->min_i_meas  &&  intlk_mgr->vars.i_meas <= limits->max_i_meas) << 4 // I_MEAS_OK
                     | (intlk_mgr->vars.abs_i_rate  <= limits->max_i_rate                                                  ) << 5 // I_RATE_OK
                     |  intlk_mgr->test_override_mask[chan_index];

        // Set the TESTS_OK bit (bit 6) to 1 only if all six tests are OK or overridden

        results_mask |= (results_mask == 0x3F) << 6;

        // Set CHAN_OK bit (bit 7) according to the channel control parameter

        switch(chan_control)
        {
            case INTLK_CHAN_OFF:    // To satisfy the compiler - it can never have this value at this point

            case INTLK_CHAN_NOT_OK:

                // Leave bit 7 as 0 to force the channel to be always NOT OK

                break;

            case INTLK_CHAN_OK:

                // Set bit 7 to 1 to force the channel to be always OK

                results_mask |= (1 << 7);
                break;

            case INTLK_CHAN_CONDITIONAL:

                // Set bit 7 to the TESTS_OK (bit 6)

                results_mask |= (results_mask & 0x40) << 1;
                break;
        }
    }

    return results_mask;
}



uint32_t intlkRT(struct INTLK_mgr * const intlk_mgr, bool const process_parameters, bool const latch)
{
    // Acquire analog input signals

    intlk_mgr->vars.abs_reg_err = *intlk_mgr->abs_reg_err;

    cc_float const v_meas = *intlk_mgr->v_meas;
    cc_float const i_meas = *intlk_mgr->i_meas;

    // Compute absolute rates of change of the measurements

    intlk_mgr->vars.abs_v_rate = fabsf(v_meas - intlk_mgr->vars.v_meas) * intlk_mgr->inv_iter_period;
    intlk_mgr->vars.abs_i_rate = fabsf(i_meas - intlk_mgr->vars.i_meas) * intlk_mgr->inv_iter_period;

    intlk_mgr->vars.v_meas = v_meas;
    intlk_mgr->vars.i_meas = i_meas;

    // Either process and cache the parameters or use the cached parameters to run the interlock tests

    if(process_parameters)
    {
        // Default cycle selector is zero (for when USE_CYC_SEL is DISABLED)

        uint32_t cyc_sel = 0;

        if(intlkParValue(intlk_mgr, USE_CYC_SEL) == CC_ENABLED)
        {
            cyc_sel = *intlk_mgr->cyc_sel;
        }

        // Update the pointer to the active latched values structure

        intlk_mgr->latch.active = (struct INTLK_latch_values *)((uint8_t *)intlk_mgr->latch.values + cyc_sel * intlk_mgr->latch.values_step);

        // Process parameters and cache limits

        intlkCacheParsRT(intlk_mgr, (struct INTLK_pars *)((uint8_t *)intlk_mgr->pars + intlk_mgr->intlk_pars_step * cyc_sel));

        // Reset the latch counter

        intlk_mgr->latch.active->count = 0;
    }
    else
    {
        // Calculate if reference state is operational

        intlk_mgr->vars.op_ref_state = (*intlk_mgr->ref_state_bit_mask & intlk_mgr->op_ref_states_mask) != 0;

        // Use cached limits to run interlock tests on all channels

        uint32_t const tests_ok_mask = intlkChannelRT(intlk_mgr, 0) << 0  |
                                       intlkChannelRT(intlk_mgr, 1) << 8  |
                                       intlkChannelRT(intlk_mgr, 2) << 16 |
                                       intlkChannelRT(intlk_mgr, 3) << 24;

        intlk_mgr->tests_ok_mask = tests_ok_mask;

        // Check for request to latch tests_ok_mask in the latch array

        if(   intlk_mgr->latch.active->count < INTLK_LATCH_LEN
           && latch == true
           && intlk_mgr->latch.previous == false)
        {
            intlk_mgr->latch.active->tests_ok_mask[intlk_mgr->latch.active->count] = tests_ok_mask;
            intlk_mgr->latch.active->v_meas       [intlk_mgr->latch.active->count] = v_meas;
            intlk_mgr->latch.active->i_meas       [intlk_mgr->latch.active->count] = i_meas;

            intlk_mgr->latch.active->count++;
        }

        // Extract channels OK mask from the tests OK mask

        intlk_mgr->channels_ok_mask = ((tests_ok_mask >>  7) & 0x01) |
                                      ((tests_ok_mask >> 14) & 0x02) |
                                      ((tests_ok_mask >> 21) & 0x04) |
                                      ((tests_ok_mask >> 28) & 0x08);
    }

    // Save latch flag to allow transition detection on the next call

    intlk_mgr->latch.previous = latch;

    // Return channels OK mask (bits 0-3 for the four channels)

    return intlk_mgr->channels_ok_mask;
}

// EOF
