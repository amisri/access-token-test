//! @file  cyclog.c
//! @brief Converter Control Cycle Logging library initialization functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libcyclog.
//!
//! libcyclog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdint.h>
#include <stdbool.h>

#include "libcyclog.h"


void cyclogInit(struct CYCLOG_device          * cyclelog_device,
                CYCLOG_func_type_callback     * str_func_type_function,
                CYCLOG_status_callback        * str_status_function,
                uint32_t                  const bitmask_ok,
                uint32_t                  const bitmask_warnings,
                uint32_t                  const bitmask_faults)
{
    CC_ASSERT(str_func_type_function != NULL);
    CC_ASSERT(str_status_function    != NULL);

    memset(cyclelog_device, 0, sizeof(struct CYCLOG_device));

    cyclelog_device->str_func_type_function = str_func_type_function;
    cyclelog_device->str_status_function    = str_status_function;
    cyclelog_device->bitmask_ok             = bitmask_ok;
    cyclelog_device->bitmask_warnings       = bitmask_warnings;
    cyclelog_device->bitmask_faults         = bitmask_faults;
}



void cyclogStore(struct CYCLOG_device       * cyclelog_device,
                 struct CC_us_time    const * time_stamp,
                 uint8_t              const   sub_sel,
                 uint8_t              const   cyc_sel,
                 uint16_t             const   func_type,
                 uint16_t             const   status_bitmask,
                 bool                 const   start_super_cycle)
{
    struct CYCLOG_record * record = &cyclelog_device->records[cyclelog_device->head_index];

    // Cancel this record by reseting the unix_time time stamp

    record->time_stamp.secs.abs = 0;

    // Only increment the cycle index after the first start of super-cycle

    if (cyclelog_device->cycle_index > 0)
    {
        cyclelog_device->cycle_index++;
    }

    if (start_super_cycle == true)
    {
        cyclelog_device->cycle_index = 1;
    }

    // Add the record

    record->cyc_idx        = cyclelog_device->cycle_index;
    record->sub_sel        = sub_sel;
    record->cyc_sel        = cyc_sel;
    record->func_type      = func_type;
    record->status_bitmask = status_bitmask;

    if ((status_bitmask & cyclelog_device->bitmask_faults) != 0)
    {
        strncpy(record->status, "FLT", 3);
    }
    else if ((status_bitmask & cyclelog_device->bitmask_warnings) != 0)
    {
        strncpy(record->status, "WRN", 3);
    }
    else if (status_bitmask == 0 || (status_bitmask & cyclelog_device->bitmask_ok) != 0)
    {
        strncpy(record->status, "OK ", 3);
    }
    else
    {
        strncpy(record->status, "UNK ", 3);
    }

    record->time_stamp.us = time_stamp->us;

    CC_MEMORY_BARRIER;

    // Store the Unix time part of the time stamp last to validate the rest of the record

    record->time_stamp.secs.abs  = time_stamp->secs.abs;

    // Only advance head_index after the boundary check to prevent cyclelogRead() from
    // using an invalid index

    uint16_t head_index = cyclelog_device->head_index + 1;

    cyclelog_device->head_index = head_index >= CYCLOG_NUM_RECORDS ? 0 : head_index;
}



void cyclogRead(struct CYCLOG_device * cyclelog_device,
                uint32_t        const  record_index,
                char                 * read_buf,
                uint32_t        const  read_buf_len)
{
    CC_ASSERT(read_buf_len >= CYCLOG_READ_FGC_FIXED_LEN);

    // Initialize or advance record index to the next record

    if (record_index >= CYCLOG_NUM_RECORDS)
    {
        return;
    }

    uint32_t               buf_index = (cyclelog_device->head_index + record_index) % CYCLOG_NUM_RECORDS;
    struct CYCLOG_record * record    = &cyclelog_device->records[buf_index];
    char                 * buf       = read_buf;

    // Make sure the record is valid

    if (record->time_stamp.secs.abs != 0)
    {
        buf += snprintf(buf, CYCLOG_READ_FGC_FIXED_LEN, "%2u %10u.%06u %2u|%2u %3s %-8s",
                                                         (unsigned int)record->cyc_idx,
                                                         (unsigned int)record->time_stamp.secs.abs,
                                                         (unsigned int)record->time_stamp.us,
                                                         (unsigned int)record->sub_sel,
                                                         (unsigned int)record->cyc_sel,
                                                         record->status,
                                                         cyclelog_device->str_func_type_function(record->func_type));

        // The log status is a bitmask. Report all the bit conditions asserted

        uint32_t status_bitmask = record->status_bitmask;
        uint32_t buf_size       = read_buf_len - CYCLOG_READ_FGC_FIXED_LEN;

        while (status_bitmask != 0 && buf_size > 0)
        {
            // Find the least significant non-zero bit

            uint32_t log_status = 1 << CC_MEMORY_CTZ(status_bitmask);

            status_bitmask &= ~log_status;

            // Add the trailing space. The while() condition guarantees that the buffer
            // has at least one character available.

            *buf++ = ' ';
            buf_size--;

            // Retrieve the string representation of the status

            char const * status     = cyclelog_device->str_status_function(log_status);
            uint32_t     status_len = strlen(status);

            if (buf_size < status_len)
            {
                status_len = buf_size;
            }

            strncpy(buf, status, status_len);

            buf_size -= status_len;
            buf      += status_len;
        }
    }

    *buf = '\0';
}


// EOF
