//! @file  libcyclog.h
//! @brief Converter Control Cycle Logging library header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libcyclog.
//!
//! libcyclog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "cclibs.h"


// Constants

//! The read event FGC log format is:
//!
//! {cyc_idx} {time_stamp.s}.{time_stamp.us} {sub_sel}|{cyc_sel} {status} {func_type} {cycle_status}\0
//!
//!     cyc_idx          :  2
//!     time_stamp.s     : 10
//!     decimal point    :  1
//!     time_stamp.us    :  6
//!     sub_sel          :  2
//!     vertical line    :  1
//!     cyc_sel          :  2
//!     status           :  3
//!     func_type        :  8
//!     space delimiters :  5
//!     Null termination :  1
//!     cycle_status     :  user specific
//!     TOTAL FIXED CHARACTERS : 41

#define CYCLOG_READ_FGC_FIXED_LEN   41
#define CYCLOG_NUM_RECORDS          64


// typedefs

typedef char * CYCLOG_func_type_callback(uint32_t const func_type);  //!< Type for function type string callback
typedef char * CYCLOG_status_callback   (uint32_t const status);     //!< Type for status string callback


//!< Cycle log record

struct CYCLOG_record
{
    struct CC_us_time     time_stamp;                          //!< Timestamp of this cycle
    uint8_t               sub_sel;                             //!< Sub-device selector
    uint8_t               cyc_sel;                             //!< Cycle selector
    uint16_t              cyc_idx;                             //!< Cycle index within the super-cycle
    uint16_t              func_type;                           //!< Cycle function type symbol index
    uint16_t              status_bitmask;                      //!< Status bitmask
    char                  status[3];                           //!< Status string
};


//!< Cycle log data for a device

struct CYCLOG_device
{
    CYCLOG_func_type_callback * str_func_type_function;        //!< Function type string callback
    CYCLOG_status_callback    * str_status_function;           //!< Status string callback
    uint32_t                    bitmask_ok;                    //!< Ok bitmask
    uint32_t                    bitmask_warnings;              //!< Warnings bitmask
    uint32_t                    bitmask_faults;                //!< Faults bitmask
    uint16_t                    cycle_index;                   //!< Cycle index within the super-cycle
    uint16_t                    head_index;                    //!< Buffer head index
    struct CYCLOG_record        records[CYCLOG_NUM_RECORDS];   //!< Timing records
};


#ifdef __cplusplus
extern "C" {
#endif


//! Initialize the cycle log device structure
//!
//! @param[in]    cyclelog_device           Pointer to cycle log structure to initialize
//! @param[in]    str_func_type_function    Pointer to function string types
//! @param[in]    str_status_function       Pointer to function for string status
//! @param[in]    bitmask_ok                Bitmask with OK status
//! @param[in]    bitmask_warnings          Bitmask with warning status
//! @param[in]    bitmask_faults            Bitmask with fault status

void cyclogInit(struct CYCLOG_device          * cyclelog_device,
                CYCLOG_func_type_callback     * str_func_type_function,
                CYCLOG_status_callback        * str_status_function,
                uint32_t                  const bitmask_ok,
                uint32_t                  const bitmask_warnings,
                uint32_t                  const bitmask_faults);


//! Store one record in the cycle log
//!
//! This funciton is not thread-safe
//!
//! @param[in]    cyclelog_device   Pointer to cycle log structure
//! @param[in]    time_stamp        Cycle time stamp
//! @param[in]    sub_sel           Sub-device selector
//! @param[in]    cyc_sel           Cycle selector
//! @param[in]    func_type         Function type
//! @param[in]    status_bitmask    Status bitmask
//! @param[in]    start_super_cycle True if the super-cycle starts

void cyclogStore(struct CYCLOG_device      * cyclelog_device,
                 struct CC_us_time   const * time_stamp,
                 uint8_t                     sub_sel,
                 uint8_t                     cyc_sel,
                 uint16_t                    func_type,
                 uint16_t                    status_bitmask,
                 bool                        start_super_cycle);


//! This function will copy the cycle log record identified by record_index into the destination buffer.
//! This function is not thread safe. The destination buffer must be at least CYCLOG_READ_FGC_FIXED_LEN
//! characters long.
//!
//! @param[in]      cyclelog_device Pointer to the cycle log device structure
//! @param[in]      record_index    Record_index to copy
//! @param[in]      read_buf        Pointer to destination buffer
//! @param[in]      read_buf_len    Destination buffer length

void cyclogRead(struct CYCLOG_device * cyclelog_device,
                uint32_t               record_index,
                char                 * read_buf,
                uint32_t               read_buf_len);


#ifdef __cplusplus
}
#endif

// EOF
