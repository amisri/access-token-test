//! @file  libpolswitch.h
//! @brief Converter Control Polarity Switch Library header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2018. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libpolswitch.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!/

#pragma once

#include "cclibs.h"
#include "libpolswitch/polswitchConsts.h"
#include "libpolswitch/polswitchPars.h"
#include "libpolswitch/polswitchVars.h"


//! Polarity switch parameters structure

struct POLSWITCH_pars
{
    uint32_t                        timeout;                    //!< Polarity switch movement timeout (s) - set zero if there is no polarity switch
    enum POLSWITCH_mode             mode_user;                  //!< Switch mode (i.e the requested switch state) from the user
    enum POLSWITCH_mode             mode_auto;                  //!< Switch mode from libref or application when under automatic control
    enum CC_enabled_disabled        automatic;                  //!< Automatic switch mode
    cc_float                        movement_time;              //!< Simulated switch movement delay (set zero to use default of 0.1 x timeout)
};

//! Polarity switch simulation structure

struct POLSWITCH_sim
{
    cc_float                        movement_time;              //!< Simulated switch movement time
    cc_float                        time_remaining;             //!< Time remaining timer for simulated switch movement
    enum POLSWITCH_state            requested_state;            //!< Requested state in simulation to drive hardware
};

//! Polarity switch variables

struct POLSWITCH_vars
{
    cc_float                        time_remaining;             //!< Time remaining waiting for the switch to arrive in the requested state
    cc_float                        unexpected_timer;           //!< Timer to filter unexpected changes of polarity switch positive/negative signals
    enum POLSWITCH_state            requested_state;            //!< Requested state to drive hardware
    enum POLSWITCH_state            expected_state;             //!< Last requested state once movement completes
    enum POLSWITCH_state            state;                      //!< Polarity switch state
    uint32_t                        timeout;                    //!< Polarity switch timeout in seconds: 0 = no switch
    uint32_t                        signals;                    //!< Polarity switch signals bit mask
    uint32_t                        status;                     //!< Polarity switch status bit mask
    uint32_t                        unlatched_faults;           //!< Polarity switch unlatched faults bit mask
    uint32_t                        latched_faults;             //!< Polarity switch latched faults bit mask
    uint32_t                        locked_extended;            //!< Shifter to extend LOCKED signal
};


//! Polarity switch management structure

struct POLSWITCH_mgr
{
    bool                          * zero_current_flag;          //!< Pointer to the zero current flag (NULL to disable interlock)
    bool                          * low_current_flag;           //!< Pointer to the low current flag (used to simulate LOCKED signal)
    cc_float                        period;                     //!< Iteration period (s)

    struct POLSWITCH_pars           pars;                       //!< Polarity switch parameters structure
    struct POLSWITCH_vars           vars;                       //!< Polarity switch variables structure
    struct POLSWITCH_sim            sim;                        //!< Switch simulation variables
};

#ifdef __cplusplus
extern "C" {
#endif


//! Initialize polarity switch library
//!
//! This function will be called to initialize the polarity switch library
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @param[in]     period               Iteration period for calls to polswitchMgr() in seconds
//! @param[in]     zero_current_flag    Pointer to zero current flag or NULL if no interlock needed
//! @param[in]     low_current_flag     Pointer to low current flag or NULL (used when simulating for the LOCKED status)

void polswitchMgrInit(struct POLSWITCH_mgr  * polswitch_mgr,
                      cc_float                period,
                      bool                  * zero_current_flag,
                      bool                  * low_current_flag);


//! This function is called when the application wants to reset the polarity switch control.
//!
//! It will reset the POLSWITCH MODE_USER parameter, the requested and expected states variables and
//! the time_remaining based on the actual polarity switch state according to this table:
//!
//! |     ACTUAL STATE   |         MODE_USER       |requested_state |   expected_state   |time_ramaining|
//! |--------------------|-------------------------|----------------|--------------------|--------------|
//! | POLSWITCH_POSITIVE | POLSWITCH_MODE_POSITIVE | POLSWITCH_NONE | POLSWITCH_POSITIVE |      0.0     |
//! | POLSWITCH_NEGATIVE | POLSWITCH_MODE_NEGATIVE | POLSWITCH_NONE | POLSWITCH_NEGATIVE |      0.0     |
//! | POLSWITCH_FAULT    |        No change        |   MODE_USER    |   POLSWITCH_NONE   |    TIMEOUT   |
//! | POLSWITCH_MOVING   |        No change        |   MODE_USER    |   POLSWITCH_NONE   |    TIMEOUT   |
//!
//! It also resets the polarity switch faults latch and clears the UNEXPECTED FAULT flag.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.

void polswitchReset(struct POLSWITCH_mgr * polswitch_mgr);


//! Polarity switch manager.
//!
//! This function will interpret the polarity switch status signal to determine the polarity switch state.
//! It will simulate the polarity switch if required and it was control the setting of the requested state for
//! the polarity switch depending upon the control mode (automatic or manual).
//!
//! This function should be called at a steady rate matching the period declared to polswitchMgrInit().
//! It manages the requested state, which should be used by the application to drive the polarity switch
//! control signals. This can be accessed by value using polswitchVarValue(&polswitch_mgr,REQ_STATE),
//! or by pointer using polswitchVarPointer(&polswitch_mgr,REQ_STATE).
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @param[in]     simulate             Set to true if the polarity switch should be simulated
//! @param[in]     positive             Mandatory switch positive signal
//! @param[in]     negative             Mandatory switch negative signal
//! @param[in]     fault                Optional switch fault signal
//! @param[in]     locked               Optional switch locked signal

void polswitchMgr(struct POLSWITCH_mgr * polswitch_mgr,
                  bool                   simulate,
                  bool                   positive,
                  bool                   negative,
                  bool                   fault,
                  bool                   locked);

#ifdef __cplusplus
}
#endif

// Static inline functions to test polarity switch status bits

static inline bool polswitchIsStarting(struct POLSWITCH_mgr const * const polswitch_mgr)
{
    return (polswitch_mgr->vars.status & POLSWITCH_IS_STARTING_BIT_MASK) != 0;
}

static inline bool polswitchIsSimulated(struct POLSWITCH_mgr const * const polswitch_mgr)
{
    return (polswitch_mgr->vars.status & POLSWITCH_IS_SIMULATED_BIT_MASK) != 0;
}

static inline bool polswitchIsAutomatic(struct POLSWITCH_mgr const * const polswitch_mgr)
{
    return (polswitch_mgr->vars.status & POLSWITCH_IS_AUTOMATIC_BIT_MASK) != 0;
}

static inline bool polswitchIsNegative(struct POLSWITCH_mgr const * const polswitch_mgr)
{
    return (polswitch_mgr->vars.status & POLSWITCH_IS_NEGATIVE_BIT_MASK) != 0;
}

static inline bool polswitchIsLocked(struct POLSWITCH_mgr const * const polswitch_mgr)
{
    return (polswitch_mgr->vars.status & POLSWITCH_IS_LOCKED_BIT_MASK) != 0;
}

// EOF
