//! @file  polswitchConsts.h
//! @brief Converter Control Polarity Switch Library public constants header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2018. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libpolswitch.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!/

#pragma once

//! Polarity switch state - the order must not be changed

enum POLSWITCH_state
{
    POLSWITCH_STATE_NONE     ,              // 0x0
    POLSWITCH_STATE_POSITIVE ,              // 0x1
    POLSWITCH_STATE_NEGATIVE ,              // 0x2
    POLSWITCH_STATE_FAULT    ,              // 0x3
    POLSWITCH_STATE_MOVING   ,              // 0x4
    POLSWITHC_NUM_STATES
};

//! Polarity switch mode (this is the requested polarity switch state)

enum POLSWITCH_mode
{
    POLSWITCH_MODE_NOT_SET,
    POLSWITCH_MODE_POSITIVE = POLSWITCH_STATE_POSITIVE,
    POLSWITCH_MODE_NEGATIVE = POLSWITCH_STATE_NEGATIVE,
    POLSWITCH_NUM_MODES
};

//! Polarity switch signal bit indexes

enum POLSWITCH_signal_bits
{
    POLSWITCH_SIGNAL_POSITIVE_BIT   ,       //!< Polarity switch positive position signal bit
    POLSWITCH_SIGNAL_NEGATIVE_BIT   ,       //!< Polarity switch negative position signal bit
    POLSWITCH_SIGNAL_FAULT_BIT      ,       //!< Polarity switch fault signal bit
    POLSWITCH_SIGNAL_LOCKED_BIT     ,       //!< Polarity switch locked signal bit
    POLSWITCH_NUM_SIGNAL_BITS
};

//! Polarity switch signal bit masks - the order must not be changed

enum POLSWITCH_signal_bit_masks
{
    POLSWITCH_SIGNAL_POSITIVE_BIT_MASK = (1<<POLSWITCH_SIGNAL_POSITIVE_BIT) ,
    POLSWITCH_SIGNAL_NEGATIVE_BIT_MASK = (1<<POLSWITCH_SIGNAL_NEGATIVE_BIT) ,
    POLSWITCH_SIGNAL_FAULT_BIT_MASK    = (1<<POLSWITCH_SIGNAL_FAULT_BIT   ) ,
    POLSWITCH_SIGNAL_LOCKED_BIT_MASK   = (1<<POLSWITCH_SIGNAL_LOCKED_BIT  )
};

//! Polarity switch status bit indexes

enum POLSWITCH_status_bits
{
    POLSWITCH_IS_STARTING_BIT    ,          //!< Polarity switch is starting
    POLSWITCH_IS_SIMULATED_BIT   ,          //!< Polarity switch is being simulated
    POLSWITCH_IS_AUTOMATIC_BIT   ,          //!< Polarity switch is automatic
    POLSWITCH_IS_NEGATIVE_BIT    ,          //!< Polarity switch is in negative position
    POLSWITCH_IS_LOCKED_BIT      ,          //!< Polarity switch is locked
    POLSWITCH_NUM_STATUS_BITS
};

//! Polarity switch status bit masks

enum POLSWITCH_status_bit_masks
{
    POLSWITCH_IS_STARTING_BIT_MASK   = (1<<POLSWITCH_IS_STARTING_BIT  ) ,
    POLSWITCH_IS_SIMULATED_BIT_MASK  = (1<<POLSWITCH_IS_SIMULATED_BIT ) ,
    POLSWITCH_IS_AUTOMATIC_BIT_MASK  = (1<<POLSWITCH_IS_AUTOMATIC_BIT ) ,
    POLSWITCH_IS_NEGATIVE_BIT_MASK   = (1<<POLSWITCH_IS_NEGATIVE_BIT  ) ,
    POLSWITCH_IS_LOCKED_BIT_MASK     = (1<<POLSWITCH_IS_LOCKED_BIT    )
};

//! Polarity switch faults bit indexes

enum POLSWITCH_fault_bits
{
    POLSWITCH_EXTERNAL_FAULT_BIT   ,        //!< External FAULT status is active (not all polarity switches provide this status)
    POLSWITCH_INVALID_FAULT_BIT    ,        //!< Polarity switch POSITIVE and NEGATIVE statuses are both active
    POLSWITCH_UNEXPECTED_FAULT_BIT ,        //!< Polarity switching without request
    POLSWITCH_TIMEOUT_FAULT_BIT    ,        //!< Polarity switch didn't reach requested state within the timeout
    POLSWITCH_NUM_FAULT_BITS
};

//! Polarity switch fault bit masks

enum POLSWITCH_fault_bit_masks
{
    POLSWITCH_EXTERNAL_FAULT_BIT_MASK   = (1<<POLSWITCH_EXTERNAL_FAULT_BIT  ) ,
    POLSWITCH_INVALID_FAULT_BIT_MASK    = (1<<POLSWITCH_INVALID_FAULT_BIT   ) ,
    POLSWITCH_UNEXPECTED_FAULT_BIT_MASK = (1<<POLSWITCH_UNEXPECTED_FAULT_BIT) ,
    POLSWITCH_TIMEOUT_FAULT_BIT_MASK    = (1<<POLSWITCH_TIMEOUT_FAULT_BIT   )
};

// EOF
