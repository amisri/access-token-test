//! @file  polswitchPars.h
//! @brief Converter Control Polarity Switch library : Parameters header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2018. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libpolswitch.
//!
//! libintlk is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global parameters

#define polswitchParValue(POLSWITCH_MGR, PAR_NAME)         ((POLSWITCH_MGR)->POLSWITCH_PAR_ ## PAR_NAME)
#define polswitchParPointer(POLSWITCH_MGR, PAR_NAME)       (&(POLSWITCH_MGR)->POLSWITCH_PAR_ ## PAR_NAME)

#define POLSWITCH_PAR_TIMEOUT            pars.timeout            //!< Polarity switch movement timeout (s) - set zero if there is no polarity switch
#define POLSWITCH_PAR_MODE_USER          pars.mode_user          //!< Switch mode (i.e the requested switch state) from the user
#define POLSWITCH_PAR_MODE_AUTO          pars.mode_auto          //!< Switch mode from libref or application when under automatic control
#define POLSWITCH_PAR_AUTOMATIC          pars.automatic          //!< Automatic switch mode
#define POLSWITCH_PAR_MOVEMENT_TIME      pars.movement_time      //!< Simulated switch movement delay (set zero to use default of 0.1 x timeout)

// EOF
