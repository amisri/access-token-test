//! @file  polswitchVars.h
//! @brief Converter Control Polarity Switch library : Variables header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libpolswitch.
//!
//! libintlk is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global parameters

#define polswitchVarValue(POLSWITCH_MGR, VAR_NAME)    ((POLSWITCH_MGR)->POLSWITCH_VAR_ ## VAR_NAME)
#define polswitchVarPointer(POLSWITCH_MGR, VAR_NAME)  (&(POLSWITCH_MGR)->POLSWITCH_VAR_ ## VAR_NAME)

#define POLSWITCH_VAR_TIME_REMAINING     vars.time_remaining     //!< Time remaining waiting for the switch to arrive in the requested state
#define POLSWITCH_VAR_UNEXPECTED_TIMER   vars.unexpected_timer   //!< Timer to filter unexpected changes of polarity switch positive/negative signals
#define POLSWITCH_VAR_REQUESTED_STATE    vars.requested_state    //!< Requested state to drive hardware
#define POLSWITCH_VAR_EXPECTED_STATE     vars.expected_state     //!< Last requested state once movement completes
#define POLSWITCH_VAR_STATE              vars.state              //!< Polarity switch state
#define POLSWITCH_VAR_TIMEOUT            vars.timeout            //!< Polarity switch timeout in seconds: 0 = no switch
#define POLSWITCH_VAR_SIGNALS            vars.signals            //!< Polarity switch signals bit mask
#define POLSWITCH_VAR_STATUS             vars.status             //!< Polarity switch status bit mask
#define POLSWITCH_VAR_UNLATCHED_FAULTS   vars.unlatched_faults   //!< Polarity switch unlatched faults bit mask
#define POLSWITCH_VAR_LATCHED_FAULTS     vars.latched_faults     //!< Polarity switch latched faults bit mask

// EOF
