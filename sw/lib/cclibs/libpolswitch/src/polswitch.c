//! @file  polswitch.c
//! @brief Converter Control Polarity Switch Library functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2018. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!
//! @mainpage CERN Converter Control Polarity Switch Manager Library
//!
//! This library can manage or simulate a polarity switch. It can support the
//! polarity switches developed at CERN by the EPC-LPC section. These require
//! a particular startup sequence following a power cycle. This is shown in the
//! figure. The switch can start in either negative or positive position, but
//! in the figure it is shown starting in positive.
//!
//! @image html polswitch_startup.png

#include "libpolswitch.h"

// Static constants

static cc_float const POLSWITCH_UNEXPECTED_FAULT_FILTER_TIME = 0.1F;    //!< Unexpected movement fault filter time
static cc_float const POLSWITCH_STARTUP_REQUEST_TIME         = 0.1F;    //!< Time during startup to



void polswitchMgrInit(struct POLSWITCH_mgr * const polswitch_mgr,
                      cc_float               const period,
                      bool                 * const zero_current_flag,
                      bool                 * const low_current_flag)
{
    // Reset manager structure

    memset(polswitch_mgr, 0, sizeof(struct POLSWITCH_mgr));

    // Save the arguments

    polswitch_mgr->period            = period;
    polswitch_mgr->zero_current_flag = zero_current_flag;
    polswitch_mgr->low_current_flag  = low_current_flag;
}



void polswitchReset(struct POLSWITCH_mgr * const polswitch_mgr)
{
    // Reset the polarity switch faults latch and force clear the UNEXPECTED fault

    polswitch_mgr->vars.latched_faults = polswitch_mgr->vars.unlatched_faults & ~POLSWITCH_UNEXPECTED_FAULT_BIT_MASK;

    // Reset MODE_AUTO

    polswitchParValue(polswitch_mgr, MODE_AUTO) = POLSWITCH_MODE_NOT_SET;

    // Reset requested state, expected_state, time_remaining and MODE_USER

    polswitch_mgr->vars.expected_state = POLSWITCH_STATE_NONE;

    switch(polswitch_mgr->vars.state)
    {
        case POLSWITCH_STATE_POSITIVE:
        case POLSWITCH_STATE_NEGATIVE:

            // Only launch the "reset" pulse on requested_state if a start-up sequence is not in progress

            if(polswitchIsStarting(polswitch_mgr) == false)
            {
                // Request a 100ms start-up requested_state pulse after 100ms delay for the reset action

                polswitch_mgr->vars.requested_state = POLSWITCH_STATE_NONE;
                polswitch_mgr->vars.time_remaining  = POLSWITCH_STARTUP_REQUEST_TIME;
                polswitch_mgr->vars.status         |= POLSWITCH_IS_STARTING_BIT_MASK;

                polswitchParValue(polswitch_mgr, MODE_USER) = (enum POLSWITCH_mode)polswitch_mgr->vars.state;
            }
            break;

        default: // MOVING or FAULT

            polswitch_mgr->vars.requested_state = (enum POLSWITCH_state)polswitchParValue(polswitch_mgr, MODE_USER);
            polswitch_mgr->vars.time_remaining  = (cc_float)polswitch_mgr->vars.timeout;

            // Set signals to POSITIVE in case we are simulating, to reset the simulation

            polswitch_mgr->vars.signals = POLSWITCH_STATE_POSITIVE;
            break;
    }
}

//! Sets or resets bits in a bit mask variable according to a boolean condition
//!
//! @param[in]     var            Pointer to bit mask variable to set/clear bit.
//! @param[in]     bit_mask       Bit mask to set/clear in var according to the condition.
//! @param[in]     condition      When true, set the bit_mask in var, when false, reset the bit_mask.
//! @returns       Bit mask variable value after the bits in bit_mask have been set or reset.

static uint32_t polswitchBitMask(uint32_t * const var,
                                 uint32_t   const bit_mask,
                                 bool       const condition)
{
    // Set or clear bit mask in the bit mask variable according to the condition

    if(condition == true)
    {
        *var |=  bit_mask;
    }
    else
    {
        *var &= ~bit_mask;
    }

    // Return the updated bit mask variable value

    return *var;
}

//! Provided a movement request is not pending, this function will check to see if the
//! TIMEOUT parameter has changed.
//!
//! The management of the polarity switch is controlled by the timeout parameter. Setting zero disables
//! polarity switch management. Setting a non-zero value enables polarity switch control.
//! When being enabled, the starting sequence is launched by setting the IS_STARTING status bit and
//! arming the time_remaining timer.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @retval        true                 Polarity switch is present (timeout is non zero).
//! @retval        false                Polarity switch is not in use (timeout is zero).


static bool polswitchMgrTimeout(struct POLSWITCH_mgr * const polswitch_mgr)
{
    // If no switch request is active

    if(polswitch_mgr->vars.requested_state == POLSWITCH_STATE_NONE)
    {
        // Check if polarity switch timeout has changed

        uint32_t new_timeout = polswitchParValue(polswitch_mgr, TIMEOUT);

        if(new_timeout != polswitch_mgr->vars.timeout)
        {
            if(new_timeout == 0)
            {
                // Timeout has just been reset so reset manager structure to disable polswitch

                memset(&polswitch_mgr->vars, 0, sizeof(struct POLSWITCH_vars));
            }
            else
            {
                // The timeout should be set to 10 times the typical movement time

                cc_float const movement_time = 0.1F * (cc_float)new_timeout;

                // Initialise the simulation variables

                polswitch_mgr->sim.movement_time = polswitchParValue(polswitch_mgr, MOVEMENT_TIME);

                // If SIM_DELAY is <= zero then set sim_delay to movement time, which is 10% of the timeout

                if(polswitch_mgr->sim.movement_time <= 0.0F)
                {
                    polswitch_mgr->sim.movement_time = movement_time;
                }

                // If enabling the polarity switch then initialise the start-up sequence

                if(polswitch_mgr->vars.timeout == 0.0F)
                {
                    // Launch start up by setting IS_STARTING status bit and time_remaining to the typical
                    // movement time plus 1 second

                    polswitch_mgr->vars.status        |= POLSWITCH_IS_STARTING_BIT_MASK;
                    polswitch_mgr->vars.time_remaining = 1.0F + polswitch_mgr->sim.movement_time;

                    // Initialise the signals in case we are simulating the switch

                    polswitch_mgr->vars.signals = POLSWITCH_STATE_POSITIVE;
                }
            }

            polswitch_mgr->vars.timeout = new_timeout;
        }

        // Set AUTOMATIC bit in the status bit mask

        polswitchBitMask(&polswitch_mgr->vars.status,
                         POLSWITCH_IS_AUTOMATIC_BIT_MASK,
                         (   polswitch_mgr->vars.timeout != 0
                          && polswitchParValue(polswitch_mgr, AUTOMATIC) == CC_ENABLED));
    }

    // Return true if polarity switch is active

    return polswitch_mgr->vars.timeout != 0;
}

//! Check for enabling or disabling simulation mode.
//!
//! This function sets/clears the IS_SIMUALTED status bit.
//! It also clears the FAULT signal when entering simulation mode, in case it was active
//! from the real polarity switch.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @param[in]     simulate             True when simulating the switch signals, false to use the real switch signals.
//! @retval        true                 Simulation enabled.
//! @retval        false                Simulation disabled.

static bool polswitchSimulate(struct POLSWITCH_mgr * const polswitch_mgr,
                              bool                   const simulate)
{
    if(polswitchIsSimulated(polswitch_mgr) == true)
    {
        // Currently simulating

        if(simulate == false)
        {
            // Disable simulation

             polswitch_mgr->vars.status &= ~POLSWITCH_IS_SIMULATED_BIT_MASK;
        }
    }
    else
    {
        // Currently not simulating

        if(simulate == true)
        {
            // Enable simulation

            polswitch_mgr->vars.status |= POLSWITCH_IS_SIMULATED_BIT_MASK;

            // Reset FAULT signal in case it is active from the real polarity switch

            polswitch_mgr->vars.signals &= ~POLSWITCH_SIGNAL_FAULT_BIT_MASK;
        }
    }

    return simulate;
}

//! Simulate the polarity switch signals.
//!
//! * POSITIVE and NEGATIVE follow the requested state after a delay given by the simulated movement time.
//! * LOCKED can optionally be simulated using the low current flag.
//! * FAULT is never set by the simulation.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @param[in]     requested_state      Passed by value to avoid unnecessarily accessing polswitch_mgr structure
//! @returns       signals bit mask

static uint32_t polswitchSimulateSignals(struct POLSWITCH_mgr * const polswitch_mgr,
                                         enum POLSWITCH_state         requested_state)
{
    // Set/Reset LOCKED signal based on low current flag if supplied

    uint32_t const polswitch_signals = polswitchBitMask(&polswitch_mgr->vars.signals,
                                                        POLSWITCH_SIGNAL_LOCKED_BIT_MASK,
                                                        (    polswitch_mgr->low_current_flag != NULL
                                                         && *polswitch_mgr->low_current_flag == false));

    // Simulate the POSITIVE and NEGATIVE signals

    if(polswitch_signals == 0)
    {
        // Simulated switch is moving (POSITIVE=0 and NEGATIVE=0 so signals=0)

        polswitch_mgr->sim.time_remaining -= polswitch_mgr->period;

        if(polswitch_mgr->sim.time_remaining < 0.0F)
        {
            // Movement timeout expired so set signals to reflect requested state

            return polswitch_mgr->sim.requested_state;
        }
    }
    else
    {
        // Simulated switch is not moving or is locked

        if(  (polswitch_signals & POLSWITCH_SIGNAL_LOCKED_BIT_MASK) == 0
           && polswitch_signals != requested_state
           && requested_state != POLSWITCH_STATE_NONE)
        {
            // Simulated switch is not locked and is not moving and
            // requested state does not match actual simulated state so start to simulate movement

            polswitch_mgr->sim.requested_state = requested_state;
            polswitch_mgr->sim.time_remaining  = polswitch_mgr->sim.movement_time;

            return 0;
        }
    }

    return polswitch_signals;
}

//! Combines the four polarity switch signals into a 4-bit bit mask
//!
//! The order of the positive and negative bits are chosen so that they correspond to the
//! polarity switch state and mode enums. Thus, the order of the bits is important and must
//! not be changed.
//!
//! @param[in]     positive       Polarity switch positive position signal
//! @param[in]     negative       Polarity switch negative position signal
//! @param[in]     fault          Polarity switch optional fault signal
//! @param[in]     locked         Polarity switch optional locked signal
//! @returns       signal bit mask

static uint32_t polswitchRealSignals(bool const positive,
                                     bool const negative,
                                     bool const fault,
                                     bool const locked)
{
    return (positive << POLSWITCH_SIGNAL_POSITIVE_BIT)
         | (negative << POLSWITCH_SIGNAL_NEGATIVE_BIT)
         | (fault    << POLSWITCH_SIGNAL_FAULT_BIT   )
         | (locked   << POLSWITCH_SIGNAL_LOCKED_BIT  );
}

//! Check for polarity switch faults and report in latched fault bit mask
//!
//! This function manages the four polarity switch faults: EXTERNAL_FAULT, INVALID_FAULT, UNEXPECTED_FAULT and TIMEOUT_FAULT
//! The function also handles the starting phase for a polarity switch. This is activated when the
//! timeout is set by setting the IS_STARTING status bit. After the initial timeout, the requested state is
//! set to the actual state for 100ms. This is needed to unblock some polarity switches used at CERN.
//!
//! The signals, state and requested state are passed by value, even though they are in polswitch_mgr,
//! to reduce the number of accesses to polswitch_mgr. This is because polswitch_mgr can be in
//! slow external dual-port RAM.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.
//! @param[in]     polswitch_signals    Polarity switch signals mask
//! @param[in]     polswitch_state      Polarity switch state
//! @param[in]     requested_state      Requested switch state
//! @returns       requested_state

static enum POLSWITCH_state polswitchFaults(struct POLSWITCH_mgr * const polswitch_mgr,
                                            uint32_t               const polswitch_signals,
                                            enum POLSWITCH_state   const polswitch_state,
                                            enum POLSWITCH_state         requested_state)
{
    uint32_t unlatched_faults = 0;

    // 1. EXTERNAL_FAULT : optional external fault signal from polarity switch controls

    if((polswitch_signals & POLSWITCH_SIGNAL_FAULT_BIT_MASK) != 0)
    {
        unlatched_faults = POLSWITCH_EXTERNAL_FAULT_BIT_MASK;
    }

    // 2. TIMEOUT_FAULT : down count the time remaining while ignoring the user's polswitch mode

    if(polswitch_mgr->vars.time_remaining > 0.0F)
    {
        polswitch_mgr->vars.time_remaining -= polswitch_mgr->period;

        // If timeout has expired

        if(polswitch_mgr->vars.time_remaining <= 0.0F)
        {
            // If during the starting phase and state positive or negative

            if(   polswitchIsStarting(polswitch_mgr) == true
               && (   polswitch_state == POLSWITCH_STATE_POSITIVE
                   || polswitch_state == POLSWITCH_STATE_NEGATIVE))
            {
                // Switch has arrived in the initial state after starting

                if(requested_state == POLSWITCH_STATE_NONE)
                {
                    // Request the actual state for 100ms after the initial timeout to unblock the CERN LPC polarity switch

                    requested_state = polswitch_state;

                    polswitch_mgr->vars.time_remaining = POLSWITCH_STARTUP_REQUEST_TIME;
                }
                else
                {
                    // Second 100ms timeout has expired so starting phase is complete

                    requested_state = POLSWITCH_STATE_NONE;

                    polswitch_mgr->vars.expected_state = polswitch_state;
                    polswitch_mgr->vars.status        &= ~POLSWITCH_IS_STARTING_BIT_MASK;

                    polswitchParValue(polswitch_mgr, MODE_USER) = (enum POLSWITCH_mode)polswitch_state;
                }
            }
            else // set timeout fault and cancel starting phase and any switch movement request
            {
                polswitch_mgr->vars.status &= ~POLSWITCH_IS_STARTING_BIT_MASK;

                unlatched_faults |= POLSWITCH_TIMEOUT_FAULT_BIT_MASK;
                requested_state   = POLSWITCH_STATE_NONE;
            }
        }
    }

    // 3. INVALID_FAULT : while timeout not running, status signals are both positive and negative (fault), or neither (moving) while locked

    else if(   polswitch_state == POLSWITCH_STATE_FAULT
            || (   polswitch_state == POLSWITCH_STATE_MOVING
                && (polswitch_signals & POLSWITCH_SIGNAL_LOCKED_BIT_MASK) != 0))
    {
        unlatched_faults |= POLSWITCH_INVALID_FAULT_BIT_MASK;
    }

    // 4. UNEXPECTED_FAULT : no other faults present and switch state doesn't match expected state

    if(   unlatched_faults == 0
       && polswitch_mgr->vars.latched_faults == 0
       && polswitch_mgr->vars.expected_state != POLSWITCH_STATE_NONE
       && polswitch_mgr->vars.expected_state != polswitch_state)
    {
        // Filter to allow for bouncing of the polarity switch signals

        if(polswitch_mgr->vars.unexpected_timer > POLSWITCH_UNEXPECTED_FAULT_FILTER_TIME)
        {
            unlatched_faults |= POLSWITCH_UNEXPECTED_FAULT_BIT_MASK;
        }

        polswitch_mgr->vars.unexpected_timer += polswitch_mgr->period;
    }
    else
    {
        polswitch_mgr->vars.unexpected_timer = 0.0F;
    }

    // Latch polarity switch faults

    polswitch_mgr->vars.unlatched_faults = unlatched_faults;
    polswitch_mgr->vars.latched_faults  |= unlatched_faults;

    // If any fault other than EXTERNAL has been latched, then cancel any existing switch request

    if(   (polswitch_mgr->vars.latched_faults & ~POLSWITCH_EXTERNAL_FAULT_BIT_MASK) != 0
       && requested_state != POLSWITCH_STATE_NONE)
    {
        requested_state = POLSWITCH_STATE_NONE;

        polswitch_mgr->vars.time_remaining = 0.0F;
    }

    // Return requested_state for efficiency

    return requested_state;
}

//! Check for a switch change request
//!
//! Libpolswitch can accept a user request or an automatic request from the application.
//!
//! @param[in,out] polswitch_mgr        Pointer to polarity switch manager structure.

static void polswitchRequest(struct POLSWITCH_mgr * const polswitch_mgr)
{
    // Take polswitch mode from the automatic mode or the previously captured user mode

    enum POLSWITCH_mode polswitch_mode = polswitchIsAutomatic(polswitch_mgr) == true
                                       ? polswitchParValue(polswitch_mgr, MODE_AUTO)
                                       : polswitchParValue(polswitch_mgr, MODE_USER);

    // If new polswitch mode is not NOT_SET and not the actual state then make this the requested state to be sent to the switch

    if(   polswitch_mode != POLSWITCH_MODE_NOT_SET
       && (enum POLSWITCH_state)polswitch_mode != polswitch_mgr->vars.state)
    {
        // If requested state is different to actual state then issue request and start timer

        polswitch_mgr->vars.requested_state = (enum POLSWITCH_state)polswitch_mode;
        polswitch_mgr->vars.expected_state  = POLSWITCH_STATE_NONE;
        polswitch_mgr->vars.time_remaining  = (cc_float)polswitch_mgr->vars.timeout;
    }
}



void polswitchMgr(struct POLSWITCH_mgr * const polswitch_mgr,
                  bool                   const simulate,
                  bool                   const positive,
                  bool                   const negative,
                  bool                   const fault,
                  bool                   const locked)
{
    // Check if the polarity switch timeout has changed and return immediately if switch is not in use

    if(polswitchMgrTimeout(polswitch_mgr) == false)
    {
        return;
    }

    // Cache requested state to reduce accesses to polswitch_mgr, which might be in DPRAM

    enum POLSWITCH_state requested_state = polswitch_mgr->vars.requested_state;

    // Interpret the real polarity switch signals, or simulate them: POSITIVE, NEGATIVE, FAULT, LOCKED

    polswitch_mgr->vars.signals = polswitchSimulate(polswitch_mgr, simulate) == true
                                ? polswitchSimulateSignals(polswitch_mgr, requested_state)
                                : polswitchRealSignals(positive, negative, fault, locked);

    // Cache polarity switch signals to reduce accesses to polswitch_mgr, which might be in DPRAM

    uint32_t const polswitch_signals = polswitch_mgr->vars.signals;

    // Translate polarity switch signals into the switch state

    static enum POLSWITCH_state const polswitch_signals_to_state[] =
    {                                     //         POLSWITCH_SIGNAL_NEGATIVE | POLSWITCH_SIGNAL_POSITIVE
            POLSWITCH_STATE_MOVING   ,    // 0x0               0                               0
            POLSWITCH_STATE_POSITIVE ,    // 0x1               0                               1
            POLSWITCH_STATE_NEGATIVE ,    // 0x2               1                               0
            POLSWITCH_STATE_FAULT    ,    // 0x3               1                               1
    };

    polswitch_mgr->vars.state = polswitch_signals_to_state[polswitch_signals & 0x3];

    // Cache switch state to reduce accesses to polswitch_mgr, which might be in DPRAM

    enum POLSWITCH_state const polswitch_state = polswitch_mgr->vars.state;

    // If starting phase complete then check if a switch movement has finished

    if(   polswitchIsStarting(polswitch_mgr) == false
       && polswitch_state == requested_state)
    {
        // Switch movement has just finished - clear switch movement request

        requested_state                    = POLSWITCH_STATE_NONE;
        polswitch_mgr->vars.expected_state = polswitch_state;
        polswitch_mgr->vars.time_remaining = 0.0F;
    }

    // Check for polarity switch faults

    polswitch_mgr->vars.requested_state = polswitchFaults(polswitch_mgr, polswitch_signals, polswitch_state, requested_state);

    //  Extend LOCKED signal by to allow for propagation delays in the CERN LPC polarity switches

    polswitch_mgr->vars.locked_extended = polswitch_mgr->vars.locked_extended >> 1
                                        | ((polswitch_signals & POLSWITCH_SIGNAL_LOCKED_BIT_MASK) != 0) << 2;

    // Set IS_LOCKED status bit if faults are active, the switch state doesn't match the expected state,
    // the switch reports that it is locked or the zero current flag is not active

    polswitchBitMask(&polswitch_mgr->vars.status,
                     POLSWITCH_IS_LOCKED_BIT_MASK,
                     (   polswitch_mgr->vars.latched_faults != 0
                      || polswitch_mgr->vars.expected_state != polswitch_state
                      || polswitch_mgr->vars.locked_extended != 0
                      || (polswitch_mgr->zero_current_flag != NULL && *polswitch_mgr->zero_current_flag == false)));

    // If polarity switch isn't locked then check if a new switch request is pending

    if(polswitchIsLocked(polswitch_mgr) == false)
    {
        polswitchRequest(polswitch_mgr);
    }

    // Set polarity switch IS_NEGATIVE status bit when the switch position is negative

    polswitchBitMask(&polswitch_mgr->vars.status,
                     POLSWITCH_IS_NEGATIVE_BIT_MASK,
                     (polswitch_state == POLSWITCH_STATE_NEGATIVE));
}

// EOF
