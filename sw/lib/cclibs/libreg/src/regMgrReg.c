//! @file  regMgrReg.c
//! @brief Converter Control Regulation library regulation management functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// Constants

static cc_float const LOAD_MEAS_CLIP_RATIO = 3.0F;

// Real-time functions

void regMgrRegBIsignalPrepareRT(struct REG_mgr          * const reg_mgr,
                                struct REG_mgr_signal   * const reg_signal,
                                struct CC_ns_time         const iter_time)
{
    struct REG_rst_pars * rst_pars;

    // Check delay down counter for new operation RST parameters

    if(reg_signal->op_rst_pars.use_next == 1)
    {
        // New operational RST parameters are available so switch to make them active

        rst_pars                         = reg_signal->op_rst_pars.next;
        reg_signal->op_rst_pars.next     = reg_signal->op_rst_pars.active;
        reg_signal->op_rst_pars.active   = rst_pars;
    }

    if(reg_signal->op_rst_pars.use_next > 0)
    {
        reg_signal->op_rst_pars.use_next--;
    }

    // Check delay down counter for new test RST parameters

    if(reg_signal->test_rst_pars.use_next == 1)
    {
        // New test RST parameters are available so switch to make them active

        rst_pars                           = reg_signal->test_rst_pars.next;
        reg_signal->test_rst_pars.next     = reg_signal->test_rst_pars.active;
        reg_signal->test_rst_pars.active   = rst_pars;
    }

    if(reg_signal->test_rst_pars.use_next > 0)
    {
        reg_signal->test_rst_pars.use_next--;
    }

    // Set rst_pars pointer to link to the active RST parameters (operational or test)

    reg_signal->rst_pars = reg_mgr->reg_rst_source == REG_OPERATIONAL_RST_PARS
                         ? reg_signal->op_rst_pars.active
                         : reg_signal->test_rst_pars.active;

    // Set regulation iteration counter from the time when reg_mode is NONE

    if(reg_mgr->reg_mode == REG_NONE)
    {
        //! Sometimes it may be important for multiple systems to synchronize the phase of their regulation periods.
        //! This is possible if:
        //!
        //!  - All the systems have synchronized UTC clocks
        //!  - They all use the same regulation period
        //!  - This regulation period divides exactly into a common synchronisation period
        //!
        //! This function enables this synchronization by initializing the regulation iteration counter from the UTC time
        //! identically on all systems when reg_mode is NONE.
        //!
        //! The iteration counter is incremented every iteration. When it reaches the regulation period (in iterations) it is
        //! reset to zero and the regulation algorithm runs on that iteration.
        //!
        //! The synchronisation period is chosen to be 3s because this allows for many different regulation periods that can be
        //! synchronized, while keeping the calculation simple and fast for a processor with only 32-bit integers.
        //!
        //! If the user selects a regulation period that does not divide exactly into the basic period, it will work fine
        //! but different systems using this regulation period are not guaranteed to run their regulation algorithms in phase.
        //!
        //! This awk command lists the periods in microseconds from 100us to 10000us (10kHz to 10Hz) in steps of 100us, that do this:
        //!
        //!  awk 'BEGIN{for(ns=100000;ns<=100000000;ns+=100000){g=3E9/ns;if(int(g)==g)printf "%4u,%6u us,%10.4f Hz,%6g per 3s\n",ns/100000,ns/1000,1E9/ns,12E9/ns}}'
        //!
        //! This awk command lists the periods in microseconds from 10us to 10000us (100kHz to 10Hz) in steps of 8ns, that do this:
        //!
        //!  awk 'BEGIN{for(ns=10000;ns<=100000000;ns+=8){g=3E9/ns;if(int(g)==g)printf "%8u,%6u us,%11.4f Hz,%7u per 3s\n",ns/8,ns/1000,1E9/ns,12E9/ns}}'

        reg_signal->iteration_index = ((((iter_time.secs.abs % 3) * 1000000000) + iter_time.ns)
                                       / regMgrParValue(reg_mgr, ITER_PERIOD_NS)) % reg_signal->reg_period_iters;
    }
    else
    {
        // REG_MODE is not NONE or iter_time not supplied so increment (and wrap) the iteration counter for this new iteration

        if(++reg_signal->iteration_index >= reg_signal->reg_period_iters)
        {
            reg_signal->iteration_index = 0;
        }
    }
}



static inline void regMgrRegBIsignalPreDecouplingRT(struct REG_mgr        * const reg_mgr,
                                                    struct REG_mgr_signal * const reg_signal,
                                                    enum REG_mode           const reg_mode,
                                                    cc_float                const ref_adv,
                                                    cc_float                const ref_ilc,
                                                    bool                    const enable_reg_err)
{
    // Voltage reference must be calculated by regulating the measured signal (current or field)

    reg_signal->ref_adv = ref_adv;

    // Closed-loop: Apply reference clip limits always, but rate limits only if reg error calculation is enabled.
    // This allows pre/post-function ramps to use all the available voltage to go as fast as possible.

    if(reg_mgr->openloop == true)
    {
        // Open-loop: do not apply reference limits and do not add ILC reference

        reg_signal->ref_limited = ref_adv;

        // Reset the signal's limits flags

        reg_signal->lim_ref.flags.clipped      = false;
        reg_signal->lim_ref.flags.rate_clipped = false;
    }
    else
    {
        // Closed-loop: Apply reference clip limits after adding the ILC reference, but only aply the
        // rate limits if reg error calculation is enabled. This allows pre/post-function ramps to use
        // all the available voltage to go as fast as possible.

        reg_signal->ref_limited = regLimRefRT(&reg_signal->lim_ref,
                                               reg_mgr->reg_period,
                                               ref_adv + ref_ilc,
                                               reg_signal->ref_limited,
                                               enable_reg_err);
    }

    // Calculate actuation (voltage reference) using RST algorithm for closed loop and Forward Euler algorithm for open loop

    reg_mgr->v.ref_reg = regRstCalcActRT(reg_signal->rst_pars, &reg_signal->rst_vars, reg_signal->ref_limited, reg_mgr->openloop);

    // Calculate the regulation track delay

    reg_signal->track_delay_periods = regRstTrackDelayRT(&reg_signal->rst_vars);

    // Compensate magnet saturation only when regulating current

    reg_mgr->v.ref_sat = reg_mode == REG_CURRENT
                       ? regLoadVrefSatRT(&reg_mgr->load_pars,
                                           reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED],
                                           reg_mgr->load_sat_vars.sat_factor,
                                           reg_mgr->v.ref_reg)
                       : reg_mgr->v.ref_reg;

    // Add the filtered feed forward voltage

    reg_mgr->v.ref_ff = reg_mgr->v.ref_sat + reg_mgr->v.v_ff_fltr.signal;
}



static inline void regMgrRegBIsignalPostDecouplingRT(struct REG_mgr        * const reg_mgr,
                                                     struct REG_mgr_signal * const reg_signal,
                                                     enum REG_mode           const reg_mode,
                                                     cc_float              * const ref,
                                                     bool                    const force_openloop)
{
    // Calculate voltage limits to respect the power limits

    regLimVrefPowerRT(&reg_mgr->v.lim_ref,
                      &reg_mgr->v.lim_v_ref,
                       reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED]);

    // Apply voltage reference clip and rate limits and calculate limited v_ref rate

    cc_float v_ref = regLimRefRT(&reg_mgr->v.lim_ref, reg_mgr->reg_period, reg_mgr->v.ref_deco, reg_mgr->v.ref, true);

    reg_mgr->v.ref_rate = (v_ref - reg_mgr->v.ref) * reg_signal->inv_reg_period;
    reg_mgr->v.ref      =  v_ref;

    // If voltage reference has been limited then back-calculate v_ref_reg

    bool const v_ref_is_limited = reg_signal->lim_ref.flags.rate_clipped = (reg_mgr->v.lim_ref.flags.clipped || reg_mgr->v.lim_ref.flags.rate_clipped);

    cc_float   v_ref_reg;

    if(v_ref_is_limited == true)
    {
        // Remove the decoupling compensation

        v_ref -= (reg_mgr->v.ref_deco - reg_mgr->v.ref_ff);

        // Remove the filtered feed forward voltage

        v_ref -= reg_mgr->v.v_ff_fltr.signal;

        // Remove the magnet saturation compensation when regulating current only

        v_ref_reg = reg_mode == REG_CURRENT
                  ? regLoadInverseVrefSatRT(&reg_mgr->load_pars,
                                             reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED],
                                             reg_mgr->load_sat_vars.sat_factor,
                                             v_ref)
                  : v_ref;

        // Mark regulated reference as rate limited

        reg_signal->lim_ref.flags.rate_clipped = true;
    }
    else
    {
        // Voltage reference was not limited

        v_ref_reg = reg_mgr->v.ref_reg;
    }

    // Save the signal limits flags in the common reg_mgr limits flags

    reg_mgr->ref_clipped_flag      = reg_signal->lim_ref.flags.clipped;
    reg_mgr->ref_rate_clipped_flag = reg_signal->lim_ref.flags.rate_clipped;

    // Back calculate new current reference to keep RST histories balanced

    uint32_t const history_index = regRstCalcRefRT(reg_signal->rst_pars, &reg_signal->rst_vars, v_ref_reg, v_ref_is_limited, reg_mgr->openloop);

    // Save the latest open and closed loop references and the reference rate

    reg_signal->ref_open   = reg_signal->rst_vars.ref_open  [history_index];
    reg_signal->ref_closed = reg_signal->rst_vars.ref_closed[history_index];
    reg_mgr->ref_rate      = reg_signal->rst_vars.ref_rate;

    // Switch between open/closed loop according to closeloop / openloop thresholds and force_openloop

    if(reg_mgr->openloop == true)
    {
        // Open loop - derive closed loop flag from the reference and the limits

        if(   force_openloop == false
           && (   (   reg_signal->lim_ref.flags.unipolar == false
                   && fabsf(reg_signal->ref_closed) > reg_signal->lim_ref.closeloop)
               || (   reg_signal->lim_ref.flags.unipolar == true
                   && (   (reg_mgr->invert_limits == false && reg_signal->ref_closed >  reg_signal->lim_ref.closeloop)
                       || (reg_mgr->invert_limits == true  && reg_signal->ref_closed < -reg_signal->lim_ref.closeloop)))))
        {
            reg_mgr->openloop = false;

            *ref = reg_signal->ref_limited = reg_signal->ref_adv = reg_signal->ref_closed;
        }
        else
        {
            *ref = reg_signal->ref_open;
        }
    }
    else
    {
        // Closed loop - derive openloop flag from the reference and the limits

        if(   force_openloop == true
           || (   reg_signal->lim_ref.flags.unipolar == false
               && fabsf(reg_signal->ref_closed) < reg_signal->lim_ref.openloop)
           || (   reg_signal->lim_ref.flags.unipolar == true
                && (   (reg_mgr->invert_limits == false && reg_signal->ref_closed <  reg_signal->lim_ref.openloop)
                    || (reg_mgr->invert_limits == true  && reg_signal->ref_closed > -reg_signal->lim_ref.openloop))))
        {
            // Open loop : set the open loop reference to the closed loop reference

            reg_mgr->openloop = true;

            *ref = reg_signal->ref_limited = reg_signal->ref_adv = reg_signal->ref_open
                 = reg_signal->rst_vars.ref_open[history_index] = reg_signal->ref_closed;

            if(   force_openloop == true
               && reg_signal->rst_vars.ref_closed[history_index] == 0.0F)
            {
                reg_signal->rst_vars.ref_open[history_index] = 0.0F;
                reg_signal->rst_vars.act     [history_index] = 0.0F;
            }
        }
        else
        {
            *ref = reg_signal->ref_closed;
        }
    }

    // Store the limited signal reference in a dedicated history in order to calculated the delayed reference.

    reg_signal->rst_vars.ref_limited[history_index] = reg_signal->ref_limited;
}



static void regMgrSaveRegErrRT(struct REG_mgr * const reg_mgr,
                               struct REG_err * const reg_err)
{
    bool const zero_max_abs_err = reg_mgr->max_abs_reg_err == 0.0F;

    reg_mgr->abs_reg_err     = reg_err->abs_err;
    reg_mgr->max_abs_reg_err = reg_err->max_abs_err;
    reg_mgr->milli_reg_err   = ccMilliShort(reg_err->err);
    reg_mgr->milli_rms_err   = ccMilliShort(reg_err->rms_err);

    // If max_abs_reg_err was zeroed, then zero the source of the value (V, I or B regulation max_abs_err)

    if(zero_max_abs_err == true)
    {
        reg_err->max_abs_err = 0.0F;
    }
}



static inline void regMgrRegErrRT(struct REG_mgr        * const reg_mgr,
                                  struct REG_mgr_signal * const reg_signal,
                                  bool                    const enable_reg_err,
                                  bool                    const ilc_active)
{
    bool const reg_iteration = reg_signal->iteration_index == 0;

    // Monitor regulation error on regulation iterations, or
    // every iteration if the error rate is MEASUREMENT and ILC is not active

    if(   reg_iteration
       || (   reg_signal->rst_pars->reg_err_rate == REG_ERR_RATE_MEASUREMENT
           && ilc_active == false))
    {
        float actual;
        float expected;

        if(ilc_active == false)
        {
             // ILC inactive: regulation error = meas[err_meas_select] - ref_delayed

            actual   = reg_signal->meas.signal[reg_signal->rst_pars->reg_err_meas_select];
            expected = reg_signal->ref_delayed;
        }
        else
        {
            // ILC is active: regulation error = reg_meas - ref_adv

            actual   = reg_signal->meas.reg;
            expected = reg_signal->ref_adv;
        }

        // Calculate and check regulation error

        regErrCheckLimitsRT(&reg_signal->reg_err,
                            actual,
                            expected,
                            enable_reg_err,         // true to check the regulation error limits
                            reg_iteration);         // true to calculate RMS error

        // Register the current or field regulation error

        regMgrSaveRegErrRT(reg_mgr, &reg_signal->reg_err);
    }
}



static inline void regMgrSaveActForFieldRT(struct REG_mgr * const reg_mgr)
{
    // Save voltage reference in the field regulation actuation history if regulating field is a possibility

    if(reg_mgr->b.iteration_index == reg_mgr->deco.phase)
    {
        // Magnet saturation compensation is not required for field regulation

        reg_mgr->b.rst_vars.act[reg_mgr->b.rst_vars.history_index] = reg_mgr->v.ref;
    }
}



static inline void regMgrSaveActForCurrentRT(struct REG_mgr * const reg_mgr)
{
    // Save voltage reference in the current regulation actuation history if regulating current is a possibility.

    if(reg_mgr->i.iteration_index == reg_mgr->deco.phase)
    {
        // Compensate for saturation of the magnet

        reg_mgr->i.rst_vars.act[reg_mgr->i.rst_vars.history_index] = regLoadInverseVrefSatRT(&reg_mgr->load_pars,
                                                                                              reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED],
                                                                                              reg_mgr->load_sat_vars.sat_factor,
                                                                                              reg_mgr->v.ref);
    }
}



static inline void regMgrRegVsignalRT(struct REG_mgr * const reg_mgr, cc_float * const ref)
{
    // Calculate voltage limits to respect the power limits

    regLimVrefPowerRT(&reg_mgr->v.lim_ref,
                      &reg_mgr->v.lim_v_ref,
                       reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED]);

    // *ref is a voltage reference so always apply the voltage reference clip and rate limits

    reg_mgr->v.ref_sat = reg_mgr->v.ref_reg = *ref;

    // Add the feed forward voltage

    reg_mgr->v.ref_deco = reg_mgr->v.ref_ff = reg_mgr->v.ref_sat + reg_mgr->inputs.v_ff.unfiltered;

    // Apply voltage rate and clip limits

    cc_float v_ref = regLimRefRT(&reg_mgr->v.lim_ref, reg_mgr->iter_period_fp32, reg_mgr->v.ref_deco, reg_mgr->v.ref, true);

    // Calculate the voltage reference rate of change

    reg_mgr->ref_rate = reg_mgr->v.ref_rate = (v_ref - reg_mgr->v.ref) * reg_mgr->inv_iter_period;
    reg_mgr->v.ref    = v_ref;

    // Remove the feed forward reference if the voltage reference is limited

    reg_mgr->ref_clipped_flag      = reg_mgr->v.lim_ref.flags.clipped;
    reg_mgr->ref_rate_clipped_flag = reg_mgr->v.lim_ref.flags.rate_clipped;

    bool const v_ref_is_limited  = (reg_mgr->v.lim_ref.flags.clipped || reg_mgr->v.lim_ref.flags.rate_clipped);

    if(v_ref_is_limited == true)
    {
        // Remove feed forward voltage

        v_ref -= reg_mgr->inputs.v_ff.unfiltered;
    }

    // Return voltage reference so function generator can respond if the RAMP function is playing

    *ref = v_ref;

    // Save the voltage regulation error

    regMgrSaveRegErrRT(reg_mgr, &reg_mgr->v.reg_err);
}



static inline void regMgrRegMeasLoadRT(struct REG_mgr * const reg_mgr)
{
    // Use the simulated measurement as the V_REF_SAT that corresponds to the latest current measurement

    cc_float const i_meas = reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED];
    cc_float const i_rate = reg_mgr->i.meas.rate;
    cc_float const v_ref_sat = reg_mgr->v.ref_sat;

    // Estimate the resistance when the current isn't zero without magnet saturation compensation

    if(i_meas != 0.0F)
    {
        reg_mgr->load_pars.meas_ohms = (v_ref_sat - i_rate * reg_mgr->load_pars.henrys) / i_meas;

        if(   reg_mgr->load_pars.meas_ohms < 0.0F
           || reg_mgr->load_pars.meas_ohms > (LOAD_MEAS_CLIP_RATIO * reg_mgr->sim_pars.load.pars.ohms))
        {
            reg_mgr->load_pars.meas_ohms = 0.0F;
        }
    }

    // Estimate the inductance when the rate of change of current isn't zero

    if(i_rate != 0.0F)
    {
        cc_float const ri    = i_meas * reg_mgr->load_pars.ohms;
        cc_float const max_l = LOAD_MEAS_CLIP_RATIO * reg_mgr->load_pars.henrys;

        // Henrys estimated without magnet saturation compensation

        reg_mgr->load_pars.meas_henrys = (v_ref_sat - ri) / i_rate;

        if(   reg_mgr->load_pars.meas_henrys < 0.0F
           || reg_mgr->load_pars.meas_henrys > max_l)
        {
            reg_mgr->load_pars.meas_henrys = 0.0F;
        }

        // Henrys estimated with magnet saturation compensation

        cc_float const v_ref_reg = regLoadInverseVrefSatRT(&reg_mgr->load_pars, i_meas, reg_mgr->load_sat_vars.sat_factor, v_ref_sat);

        reg_mgr->load_pars.meas_henrys_sat = (v_ref_reg - ri) / i_rate;

        if(   reg_mgr->load_pars.meas_henrys_sat < 0.0F
           || reg_mgr->load_pars.meas_henrys_sat > max_l)
        {
            reg_mgr->load_pars.meas_henrys_sat = 0.0F;
        }
    }
}



static inline cc_float regMgrRegVoltageRT(struct REG_mgr * const reg_mgr)
{
    struct REG_v_pars * const pars = &reg_mgr->v.reg_pars;
    struct REG_v_vars * const vars = &reg_mgr->v.reg_vars;

    // 1. Calculate voltage regulation error using measured voltage always (not v_meas_reg)

    vars->v_reg_err = reg_mgr->v.ref_vs - reg_mgr->v.unfiltered;

    // 2. Integrate the regulation error

    vars->v_integrator = regMgrParValue(reg_mgr, VREG_EXT_K_INT) != 0.0F
                       ? vars->v_integrator + reg_mgr->iter_period_fp32 * vars->v_reg_err
                       : 0.0F;

    // 3. Calculate damping reference

    vars->d_ref = (   reg_mgr->v.ref_vs * regMgrParValue(reg_mgr, VREG_EXT_K_FF)
                   +    vars->v_reg_err * regMgrParValue(reg_mgr, VREG_EXT_K_P)
                   + vars->v_integrator * regMgrParValue(reg_mgr, VREG_EXT_K_INT))
                * pars->k_w;

    // 4. Prepare partial i_capa from i_capa measurement (full to partial translation when needed)

    vars->i_capa = regMgrParPointer(reg_mgr, VFILTER_I_CAPA_FILTER)[0] * reg_mgr->inputs.i_capa.unfiltered
                 + regMgrParPointer(reg_mgr, VFILTER_I_CAPA_FILTER)[1] * vars->i_capa;

    // 5. Select the v_meas_reg signals from measurement or simulation

    vars->v_meas_reg = regMgrParValue(reg_mgr, VFILTER_V_MEAS_SOURCE) == REG_MEASUREMENT
                     ? reg_mgr->v.unfiltered
                     : reg_mgr->sim_vars.v_meas;

    // 6. Select the i_capa_reg signal from measurement or simulation

    vars->i_capa_reg = regMgrParValue(reg_mgr, VFILTER_I_CAPA_SOURCE) == REG_MEASUREMENT
                     ? vars->i_capa
                     : reg_mgr->sim_vars.i_capa_meas;

    // 7. Calculate the damping measurement using v_meas_reg and i_capa_reg and i_meas

    vars->d_meas = regMgrParValue(reg_mgr, VFILTER_EXT_K_U) * vars->v_meas_reg
                 + regMgrParValue(reg_mgr, VFILTER_EXT_K_I) * vars->i_capa_reg
                 + regMgrParValue(reg_mgr, VFILTER_EXT_K_D) * reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED];

    // 8. Calculate firing reference

    vars->f_ref_reg = vars->d_ref - vars->d_meas;

    // 9. Limit firing reference

    vars->f_ref_limited = vars->f_ref_reg;

    if(*reg_mgr->v.lim_ref.invert_limits == false)
    {
        // No polarity inversion : do NOT invert limits

        if(vars->f_ref_reg > pars->f_pos)
        {
            vars->f_ref_limited = pars->f_pos;
        }
        else if(vars->f_ref_reg < pars->f_neg)
        {
            vars->f_ref_limited = pars->f_neg;
        }
    }
    else
    {
        // Polarity inverted : invert limits

        if(vars->f_ref_reg > -pars->f_neg)
        {
            vars->f_ref_limited = -pars->f_neg;
        }
        else if(vars->f_ref_reg < -pars->f_pos)
        {
            vars->f_ref_limited = -pars->f_pos;
        }
    }

    // 10. Update integrator to prevent wind-up when f_ref is limited

    vars->v_integrator = vars->v_integrator - pars->k_aw * (vars->f_ref_reg - vars->f_ref_limited);

    // 11. Linearize the firing reference when required and return F_REF

    vars->f_ref = reg_mgr->v.reg_pars.linearisation_enabled == false
                ? vars->f_ref_limited
                : regVregLinearizeRT(pars, vars->f_ref_limited);

    return vars->f_ref;
}


bool regMgrRegRT(struct REG_mgr * const reg_mgr,
                 cc_float       * const ref,
                 cc_float         const ref_ilc,
                 bool             const enable_reg_err,
                 bool             const force_openloop,
                 bool             const generate_harmonics,
                 bool             const ilc_active)
{
    enum REG_mode           const reg_mode     = reg_mgr->reg_mode;
    struct REG_mgr_signal * const reg_signal   = reg_mgr->reg_signal;
    bool reg_complete = true;

    // If the V_AC signal is connected then generate the feedforward harmonics of the V_AC signal (will be zero if not enabled)

    if(reg_mgr->inputs.v_ac.meas.is_valid == true)
    {
        regHarmonicsRT(&reg_mgr->v.harmonics, reg_mgr->inputs.v_ac.meas.signal, generate_harmonics);
    }

    // Regulate according to the regulation mode

    switch(reg_mode)
    {
        default:

            // Converter is off so set ref_actuation using LUT if enabled

            reg_mgr->ref_actuation = reg_mgr->v.reg_pars.linearisation_enabled == true
                                   ? regVregLinearizeRT(&reg_mgr->v.reg_pars, 0.0F)
                                   : 0.0F;

             // Return true to allow libref to run the reference state machine

            return true;

        case REG_VOLTAGE:

            // Voltage regulation mode: set the voltage reference directly from the function generator at the iteration rate

            regMgrRegVsignalRT(reg_mgr, ref);

            // Save voltage reference in the current and field regulation actuation histories

            regMgrSaveActForFieldRT(reg_mgr);
            regMgrSaveActForCurrentRT(reg_mgr);

            break;

        case REG_CURRENT:
        case REG_FIELD:

            // Field or Current regulation - start the regulation processing on the first iteration of the regulation period

            if(reg_signal->iteration_index == 0)
            {
                // Set the decoupling phase for this regulation period in case it has changed during the previous period

                reg_mgr->deco.phase = reg_mgr->deco.new_phase;

                // Run the regulation processing up to the point where decoupling requires an exchange of data

                regMgrRegBIsignalPreDecouplingRT(reg_mgr, reg_signal, reg_mode, *ref, force_openloop == false ? ref_ilc : 0.0F, enable_reg_err);

                // Save the voltage reference and regulated current measurement in case they need to be shared
                // for symmetric decoupling with other controllers

                regDecoSaveDataRT(&reg_mgr->deco, reg_mgr->v.ref_ff, reg_mgr->i.meas.reg);
            }

            // Finish regulation processing on the iteration defined by the decoupling phase (0 if decoupling is disabled)

            if(reg_signal->iteration_index == reg_mgr->deco.phase)
            {
                // Calculate voltage reference using decoupling algorithm if enabled

                reg_mgr->v.ref_deco = regDecoRT(&reg_mgr->deco);

                // Complete the regulation processing for this regulation period

                regMgrRegBIsignalPostDecouplingRT(reg_mgr, reg_signal, reg_mode, ref, force_openloop);
            }
            else
            {
                // Clear reg_complete - this is returned to libref

                reg_complete = false;
            }

            // Calculate the regulation error

            regMgrRegErrRT(reg_mgr, reg_signal, enable_reg_err, ilc_active);

            // Save voltage reference in the other signal's actuation history

            if(reg_mode == REG_FIELD)
            {
                regMgrSaveActForCurrentRT(reg_mgr);
            }
            else
            {
                regMgrSaveActForFieldRT(reg_mgr);
            }

            break;
    }

    // Estimate the power and load at the current regulation rate

    if(reg_mgr->i.iteration_index == 0)
    {
        // Estimate the power

        reg_mgr->power = reg_mgr->sim_vars.v_meas * reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED];

        // Estimate the load inductance and resistance.

        regMgrRegMeasLoadRT(reg_mgr);
    }

    // Calculate the RMS of the rate of change of the voltage reference and check against limits

    regLimRmsRT(&reg_mgr->lim_v_rate_rms, reg_mgr->v.ref_rate);

    // Share the voltage reference between the number of voltage sources connected in series and add harmonics generator reference

    reg_mgr->v.ref_vs = reg_mgr->v.ref * reg_mgr->v.reg_pars.inv_num_vsources + reg_mgr->v.harmonics.ref;

    // Calculate the resulting actuation according to VS_ACTUATION

    reg_mgr->ref_actuation = regMgrParValue(reg_mgr, VS_ACTUATION) == REG_VOLTAGE_REF
                           ? reg_mgr->v.ref_vs
                           : regMgrRegVoltageRT(reg_mgr);

    // Return reg complete flag - this is true if the regulation processing completed on this iteration

    return reg_complete;
}

// EOF
