//! @file  regMgrMode.c
//! @brief Converter Control Regulation library regulation mode management functions.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


//! Set the regulation mode to NONE or VOLTAGE.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! This is a Real-Time function.
//!
//! @param[in,out] reg_mgr              Pointer to regulation manager structure.
//! @param[in]     reg_mode             Regulation mode to set (#REG_NONE, #REG_VOLTAGE).
//! @param[in]     use_average_v_ref    If reg_mode is set to #REG_VOLTAGE then this flag controls how the voltage reference
//!                                     is initialized. If true, v_ref will be set to the average v_ref in the RST history.
//!                                     If false, it will be set to the most recent v_ref.

static void regMgrModeSetNoneOrVoltageRT(struct REG_mgr * const reg_mgr, enum REG_mode const reg_mode, bool const use_average_v_ref)
{
    if(reg_mode == REG_VOLTAGE)
    {
        // Voltage regulation - Initialize voltage references according to previous regulation mode

        switch(reg_mgr->reg_mode)
        {
            case REG_FIELD:

                reg_mgr->v.ref_reg = use_average_v_ref ? regRstAverageVrefRT(&reg_mgr->b.rst_vars) : regRstPrevActRT(&reg_mgr->b.rst_vars);
                reg_mgr->v.ref_sat = reg_mgr->v.ref_reg;
                break;

            case REG_CURRENT:

                reg_mgr->v.ref_reg = use_average_v_ref ? regRstAverageVrefRT(&reg_mgr->i.rst_vars) : regRstPrevActRT(&reg_mgr->i.rst_vars);

                reg_mgr->v.ref_sat = regLoadVrefSatRT(&reg_mgr->load_pars,
                                                       reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED],
                                                       reg_mgr->load_sat_vars.sat_factor,
                                                       reg_mgr->v.ref_reg);
                break;

            default:    // NONE

                break;
        }

        reg_mgr->v.ref = reg_mgr->v.ref_deco = reg_mgr->v.ref_sat;
    }
    else // REG_NONE
    {
        reg_mgr->v.ref_reg                = 0.0F;
        reg_mgr->v.ref_sat                = 0.0F;
        reg_mgr->v.ref                    = 0.0F;
        reg_mgr->v.ref_vs                 = 0.0F;
        reg_mgr->v.ref_rate               = 0.0F;

        memset(&reg_mgr->v.reg_vars, 0, sizeof(reg_mgr->v.reg_vars));

        // Reset voltage regulation error variables

        regErrResetLimitsVarsRT(&reg_mgr->v.reg_err);

        // Reset voltage reference RMS limit

        regLimRmsResetRT(&reg_mgr->lim_v_rate_rms);

        // Reset the load measurement and power

        reg_mgr->power                 = 0.0F;
        reg_mgr->load_pars.meas_ohms   = 0.0F;
        reg_mgr->load_pars.meas_henrys = 0.0F;
    }

    // In voltage mode we want to always start with rate of 0
    // This is to prevent overshoot with ramp manager when inheriting huge reference rate of change

    reg_mgr->ref_rate = 0.0F;

    // In both NONE and VOLTAGE mode, reset V_REF_DECO and V_REF_FF as these are not implemented in these modes

    reg_mgr->v.ref_ff   = 0.0F;
    reg_mgr->v.ref_deco = 0.0F;

    // Clear field and current regulation variables

    reg_mgr->ref_advance_ns = reg_mgr->v.reg_pars.ref_advance_ns;
    reg_mgr->ref_advance    = reg_mgr->v.reg_pars.ref_advance;
    reg_mgr->track_delay_ns = reg_mgr->ref_advance_ns;
    reg_mgr->track_delay    = reg_mgr->ref_advance;

    reg_mgr->ref_clipped_flag      = false;
    reg_mgr->ref_rate_clipped_flag = false;

    reg_mgr->reg_signal    = &reg_mgr->i;
    reg_mgr->lim_ref       = &reg_mgr->v.lim_ref;
    reg_mgr->ref           = &reg_mgr->v.ref_reg;
    reg_mgr->reg_period    = reg_mgr->iter_period_fp32;
    reg_mgr->reg_period_ns = regMgrParValue(reg_mgr, ITER_PERIOD_NS);
    reg_mgr->openloop      = false;
}



static void regMgrModeSetFieldOrCurrentRT(struct REG_mgr * const reg_mgr, enum REG_mode const reg_mode)
{
    // Get points to RST parameters and variables

    struct REG_mgr_signal * const reg_signal = reg_mgr->reg_signal = (reg_mode == REG_FIELD ? &reg_mgr->b : &reg_mgr->i);
    struct REG_rst_pars   * const rst_pars   = reg_signal->rst_pars;
    struct REG_rst_vars   * const rst_vars   = &reg_signal->rst_vars;

    reg_mgr->ref_advance_ns = rst_pars->ref_advance_ns;
    reg_mgr->ref_advance    = rst_pars->ref_advance;
    reg_mgr->track_delay_ns = rst_pars->track_delay_ns;
    reg_mgr->track_delay    = rst_pars->track_delay;

    // Set ref_rate to zero for 1 and 2-quadrant converters when the current is below LOW threshold

    reg_mgr->ref_rate = reg_signal->lim_ref.flags.unipolar && reg_signal->lim_meas.flags.low ? 0.0F : reg_signal->meas.rate;

    regRstInitRefRT(rst_pars, rst_vars, reg_mgr->ref_rate);

    reg_signal->ref_closed  = regRstPrevRefRT(rst_vars);
    reg_mgr->openloop       = (fabsf(reg_signal->ref_closed) < reg_signal->lim_ref.closeloop);

    reg_signal->ref_open    = regRstPrevOpenRefRT(rst_vars);
    reg_signal->ref_adv     = reg_mgr->openloop == true ? reg_signal->ref_open : reg_signal->ref_closed;
    reg_signal->ref_delayed = 0.0F;
    reg_signal->ref_limited = reg_signal->ref_adv;

    reg_mgr->lim_ref       = &reg_signal->lim_ref;
    reg_mgr->ref           = &reg_signal->ref_adv;
    reg_mgr->reg_period_ns = reg_signal->reg_period_ns;
    reg_mgr->reg_period    = reg_signal->reg_period;
}



static void regMgrModeSignalResetRT(struct REG_mgr_signal * const reg_signal)
{
    reg_signal->ref_adv             = 0.0F;
    reg_signal->ref_limited         = 0.0F;
    reg_signal->ref_closed          = 0.0F;
    reg_signal->ref_open            = 0.0F;
    reg_signal->track_delay_periods = 0.0F;
    reg_signal->ref_delayed         = 0.0F;
}



enum REG_mode regMgrModeSetRT(struct REG_mgr * const reg_mgr, enum REG_mode const reg_mode, bool const use_average_v_ref)
{
    // Return immediately if regulation mode has not changed or requested mode is not enabled

    if(   reg_mode == reg_mgr->reg_mode
       || (reg_mode == REG_CURRENT && reg_mgr->i.lim_meas.flags.enabled == false)
       || (reg_mode == REG_FIELD   && reg_mgr->b.lim_meas.flags.enabled == false))
    {
        return reg_mgr->reg_mode;
    }

    // Switch on old regulation mode - reset old regulation variables

    switch(reg_mgr->reg_mode)
    {
        case REG_CURRENT:

            regMgrModeSignalResetRT(&reg_mgr->i);
            regErrResetLimitsVarsRT(&reg_mgr->i.reg_err);
            break;

        case REG_FIELD:

            regMgrModeSignalResetRT(&reg_mgr->b);
            regErrResetLimitsVarsRT(&reg_mgr->b.reg_err);
            break;

        default:    // VOLTAGE or NONE

            break;
    }

    // Switch on new regulation mode

    switch(reg_mode)
    {
        case REG_NONE:
        case REG_VOLTAGE:

            regMgrModeSetNoneOrVoltageRT(reg_mgr, reg_mode, use_average_v_ref);
            break;

        case REG_CURRENT:
        case REG_FIELD:

            regMgrModeSetFieldOrCurrentRT(reg_mgr, reg_mode);
            break;

        default:

            break;
    }

    // Clip the initial rate to the rate limit.
    // Clipping will be disabled when the rate limit is not a positive value.

    cc_float const rate_limit = reg_mgr->lim_ref->rate;

    if(rate_limit > 0.0F)
    {
        if(reg_mgr->ref_rate > rate_limit)
        {
            reg_mgr->ref_rate = rate_limit;
        }
        else if(reg_mgr->ref_rate < -rate_limit)
        {
            reg_mgr->ref_rate = -rate_limit;
        }
    }

    // Store the new regulation mode

    reg_mgr->reg_mode = reg_mode;

    return reg_mode;
}

// EOF
