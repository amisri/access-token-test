//! @file  regErr.c
//! @brief Converter Control Regulation library regulation error functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


// NON-RT   regErrInitLimits

void regErrInitLimits(struct REG_err * const err,
                      cc_float         const warning_threshold,
                      cc_float         const fault_threshold,
                      uint32_t         const filter_time_iters)
{
    // Set the new fault and warning thresholds and save filter_time_iters

    err->warning.threshold = warning_threshold;
    err->fault.threshold   = fault_threshold;
    err->filter_time_iters = filter_time_iters;

    // If the fault or warning is disabled (not a positive value), then reset the corresponding flag and counter.

    if(err->warning.threshold <= 0.0F)
    {
        err->warning.flag    = false;
        err->warning.counter = 0;
    }

    if(err->fault.threshold <= 0.0F)
    {
        err->fault.flag    = false;
        err->fault.counter = 0;
    }
}



void regErrResetLimitsVarsRT(struct REG_err * const err)
{
    err->err             = 0.0F;
    err->abs_err         = 0.0F;
    err->max_abs_err     = 0.0F;
    err->err2_filter     = 0.0F;
    err->rms_err         = 0.0F;

    err->warning.flag    = false;
    err->warning.counter = 0;

    err->fault.flag      = false;
    err->fault.counter   = 0;
}


//! Manage the warning and fault limits by applying a time filter.
//!
//! The limit must be passed (in either direction) for filter_time_iters iterations for the limit flag to be set or reset.
//! This will ignore glitches and reduces the rate of toggling.
//!
//!
//! @param[in,out] err_limit         Pointer to regulation error limit threshold and flags structure
//! @param[in]     filter_time_iters Iteration counter limit to filter glitches
//! @param[in]     abs_err           Absolute error

static void regErrLimitRT(struct REG_err_limit * const err_limit, uint32_t const filter_time_iters, cc_float const abs_err)
{
    if(err_limit->flag == true)
    {
        // Limit flag is set

        if(abs_err <= err_limit->threshold)
        {
            // Absolute error is below the threshold so increment counter

            if(++err_limit->counter > filter_time_iters)
            {
                // Counter has passed the counter limit so reset the flag

                err_limit->flag = false;
            }
        }
        else
        {
            // Absolute error is above the threshold so reset counter

            err_limit->counter = 0;
        }
    }
    else
    {
        // Limit flag is not set

        if(abs_err > err_limit->threshold)
        {
            // Absolute error is above the threshold so increment counter

            if(++err_limit->counter > filter_time_iters)
            {
                // Counter has passed the counter limit so set the flag

                err_limit->flag = true;
            }
        }
        else
        {
            // Absolute error is above the threshold so reset counter

            err_limit->counter = 0;
        }
    }
}



void regErrCheckLimitsRT(struct REG_err * const err,
                         cc_float         const actual,
                         cc_float         const expected,
                         bool             const limits_enabled,
                         bool             const calc_rms)
{
    // Always calculate the error and abs_err. The error is (actual - expected),
    // so it is positive if the measurement is greater than the reference.

    err->err = actual - expected;

    cc_float const abs_error = fabsf(err->err);

    err->abs_err = abs_error;

    // Run RMS algorithm on regulation iterations only

    if(calc_rms == true)
    {
        // First order filter of the error squared, with time constant of about 50 regulation iterations

        err->err2_filter += (err->err * err->err - err->err2_filter) * 0.02F;

        // Calculate the RMS error from the filtered error squared. Because of the sqrt()
        // the time constant of the rms_err will be about 100 regulation iterations

        err->rms_err = sqrtf(err->err2_filter);
    }

    // Test error limits and record the max absolute error only when limits_enabled

    if(limits_enabled == true)
    {
        // Regulation error limits are enabled - record maximum absolute error

        if(abs_error > err->max_abs_err)
        {
            err->max_abs_err = abs_error;
        }

        // Check error warning and fault thresholds only if the threshold level is greater than zero

        if(err->warning.threshold > 0.0F)
        {
            regErrLimitRT(&err->warning, err->filter_time_iters, abs_error);
        }

        if(err->fault.threshold > 0.0F)
        {
            regErrLimitRT(&err->fault, err->filter_time_iters, abs_error);
        }
    }
    else
    {
        // Limits and max_abs_err are disabled

        err->max_abs_err     = 0.0F;
        err->warning.flag    = false;
        err->warning.counter = 0;
        err->fault.flag      = false;
        err->fault.counter   = 0;
    }
 }

// EOF
