//! @file   regLim.c
//! @brief  Converter Control Regulation library limit functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// NON-RT   regLimMeasInit

void regLimMeasInit(struct REG_lim_meas * const lim_meas,
                    bool                * const invert_limits,
                    cc_float              const pos_lim,
                    cc_float              const neg_lim,
                    cc_float                    low_lim,
                    cc_float              const zero_lim)
{
    // Force low current limit to be 1% of positive limit in case it was set to 0 or less

    if(low_lim <= 0.0F)
    {
        low_lim = 0.01F * pos_lim;
    }

    lim_meas->invert_limits   = invert_limits;
    lim_meas->pos_trip        = pos_lim  * (1.0F + REG_LIM_TRIP);
    lim_meas->neg_trip        = neg_lim  * (1.0F + REG_LIM_TRIP);
    lim_meas->low             = low_lim;
    lim_meas->zero            = zero_lim;
    lim_meas->zero_hysteresis = zero_lim * REG_LIM_MEAS_HYSTERESIS;
    lim_meas->low_hysteresis  = low_lim  * REG_LIM_MEAS_HYSTERESIS;

    // Enable limits checking only if any limit has been defined.
    // We don't reset other limit flags, they will be evaluated in the real-time function

    lim_meas->flags.enabled = (pos_lim > 0.0F || neg_lim < 0.0F);
}


// NON-RT   regLimRmsInit

void regLimRmsInit(struct REG_lim_rms * const lim_rms,
                   cc_float             const rms_warning,
                   cc_float             const rms_fault,
                   cc_float             const rms_tc,
                   cc_float             const iter_period)
{
    if(rms_tc > 0.0F)
    {
        // RMS calculation is enabled

        lim_rms->filter_factor      = iter_period / (rms_tc + 0.5F * iter_period);
        lim_rms->fault              = rms_fault;
        lim_rms->warning            = rms_warning;
        lim_rms->warning_hysteresis = lim_rms->warning * REG_LIM_RMS_HYSTERESIS;

        // If warning limit is disabled then reset the warning flag

        if(lim_rms->warning <= 0.0F)
        {
            lim_rms->flags.warning = false;
        }
    }
    else
    {
        // RMS calculation is disabled so reset the variables

        lim_rms->filter_factor = 0.0F;
        lim_rms->fault         = 0.0F;
        lim_rms->warning       = 0.0F;

        regLimRmsResetRT(lim_rms);
    }
}


// NON-RT   regLimRefInit

void regLimRefInit(struct REG_lim_ref * const lim_ref,
                   bool               * const invert_limits,
                   cc_float             const pos_lim,
                   cc_float             const standby_lim,
                   cc_float             const neg_lim,
                   cc_float             const rate_lim,
                   cc_float             const closeloop)
{
    lim_ref->invert_limits = invert_limits;

    // Keep raw limits as they are used by libref

    lim_ref->standby = standby_lim;
    lim_ref->pos     = pos_lim;
    lim_ref->neg     = neg_lim;
    lim_ref->rate    = rate_lim;

    // Set clip limits by expanding the user limits

    lim_ref->rate_clip = rate_lim * (1.0F + REG_LIM_CLIP);
    lim_ref->max_clip  = pos_lim  * (1.0F + REG_LIM_CLIP);

    // Determine if reference is unipolar or bipolar

    if(neg_lim < 0.0F)
    {
        lim_ref->flags.unipolar = false;
        lim_ref->min_clip       = neg_lim * (1.0F + REG_LIM_CLIP);
        lim_ref->closeloop      = -1.0E30F;
        lim_ref->openloop       = -1.0E30F;
    }
    else
    {
        lim_ref->flags.unipolar = true;
        lim_ref->min_clip       = 0.0F;
        lim_ref->closeloop      = closeloop;
        lim_ref->openloop       = closeloop * REG_LIM_OPENLOOP_HYSTERESIS;
    }
}


// NON-RT   regLimVrefInit

void regLimVrefInit(struct REG_lim_ref   * const lim_ref,
                    struct REG_lim_v_ref * const lim_v_ref,
                    cc_float               const i_quadrants41[2],
                    cc_float               const v_quadrants41[2],
                    cc_float               const p_pos,
                    cc_float               const p_neg)
{
    // Save power limits

    lim_v_ref->p_pos = p_pos;
    lim_v_ref->p_neg = p_neg;

    // Initialize user clip limits from initial lim_ref clip limits (regLimRefInit() must be called first)

    lim_v_ref->max_clip_user = lim_ref->max_clip;
    lim_v_ref->min_clip_user = lim_ref->min_clip;

    // Disable Q41 exclusion zone before changing to avoid real-time thread having inconsistent values

    lim_v_ref->i_quadrants41_max = -1.0E10F;

    // Quadrants 41 exclusion zone: At least a 1A spread is needed to activate Q41 limiter

    cc_float delta_i_quadrants41 = i_quadrants41[1] - i_quadrants41[0];

    if(delta_i_quadrants41 >= 1.0F)
    {
        lim_v_ref->dvdi = (v_quadrants41[1] - v_quadrants41[0]) / delta_i_quadrants41;
        lim_v_ref->v0   = (v_quadrants41[0] - lim_v_ref->dvdi * i_quadrants41[0]) * (1.0F + REG_LIM_CLIP);

        // Enable quadrants 41 exclusion after setting v0 and dvdi

        lim_v_ref->i_quadrants41_max = i_quadrants41[1];
    }

    // Initialize Vref limits for zero current

    regLimVrefCalcRT(lim_ref, lim_v_ref, 0.0F, false);
}



// Real-Time Functions

void regLimMeasRT(struct REG_lim_meas * const lim_meas, cc_float meas)
{
    // Return immediately if no measurement limits are enabled

    if(lim_meas->flags.enabled == false)
    {
        // Set low flag because it is needed to exit from TO_OFF state

        lim_meas->flags.trip = false;
        lim_meas->flags.zero = false;
        lim_meas->flags.low  = true;

        return;
    }

    cc_float abs_meas = fabsf(meas);

    // Invert measurement if limits are inverted

    if(*lim_meas->invert_limits == true)
    {
        meas = -meas;
    }

    // Trip level - negative limit is only active if less than zero

    if((meas > lim_meas->pos_trip) || (lim_meas->neg_trip < 0.0F && meas < lim_meas->neg_trip))
    {
        lim_meas->flags.trip = true;
    }
    else
    {
        lim_meas->flags.trip = false;
    }

    // Zero flag - using hysteresis to reduce toggling

    if(lim_meas->zero > 0.0F)
    {
        if(abs_meas > lim_meas->zero_hysteresis)
        {
            lim_meas->flags.zero = false;
        }
        else if(abs_meas < lim_meas->zero)
        {
            lim_meas->flags.zero = true;
        }
    }

    // Low flag - using hysteresis to reduce toggling

    if(abs_meas > lim_meas->low_hysteresis)
    {
        lim_meas->flags.low = false;
    }
    else if(abs_meas < lim_meas->low)
    {
        lim_meas->flags.low = true;
    }
}



void regLimRmsRT(struct REG_lim_rms * const lim_rms, cc_float const input)
{
    if(lim_rms->filter_factor > 0.0F)
    {
        // Use first order filter on measurement squared

        lim_rms->input2_filter += (input * input - lim_rms->input2_filter) * lim_rms->filter_factor;

        // Calculate the root of the filtered value

        lim_rms->rms = sqrtf(lim_rms->input2_filter);

        // Apply fault limit if defined

        lim_rms->flags.fault = (lim_rms->fault > 0.0F && lim_rms->rms > lim_rms->fault);

        // Apply warning limit if defined (with hysteresis)

        if(lim_rms->warning > 0.0F)
        {
            if(lim_rms->rms > lim_rms->warning)
            {
                lim_rms->flags.warning = true;
            }
            else if(lim_rms->rms < lim_rms->warning_hysteresis)
            {
                lim_rms->flags.warning = false;
            }
        }
    }
}



void regLimRmsResetRT(struct REG_lim_rms * const lim_rms)
{
    lim_rms->input2_filter = 0.0F;
    lim_rms->rms           = 0.0F;
    lim_rms->flags.warning = false;
    lim_rms->flags.fault   = false;
}


//! Power limits are applied through the voltage limits, based on the assumption that the current
//! changes more slowly than the voltage for high power magnet circuits. The power limits can be
//! helpful in the event of a fault that requires the circuit to ramp down as fast as possible while
//! respecting power limits.

void regLimVrefPowerRT(struct REG_lim_ref   * const lim_ref,
                       struct REG_lim_v_ref * const lim_v_ref,
                       cc_float               const i_meas)
{
    // Calculate voltage limits from power limits

    cc_float v_max_power = 0.0F;
    cc_float v_min_power = 0.0F;

    if(i_meas > 1.0F)
    {
        v_max_power = lim_v_ref->p_pos / i_meas;
        v_min_power = lim_v_ref->p_neg / i_meas;
    }
    else if(i_meas < -1.0F)
    {
        v_max_power = lim_v_ref->p_neg / i_meas;
        v_min_power = lim_v_ref->p_pos / i_meas;
    }

    lim_v_ref->max_clip_power = v_max_power;
    lim_v_ref->min_clip_power = v_max_power;

    // Find the most restrictive maximum limits

    cc_float max_clip = lim_v_ref->max_clip_user;

    if(v_max_power > 0.0F && v_max_power < max_clip)
    {
        max_clip = v_max_power;
    }

    lim_v_ref->max_clip = max_clip;

    // Find the most restrictive minimum limits

    cc_float min_clip = lim_v_ref->min_clip_user;

    if(v_min_power < 0.0F && v_min_power > min_clip)
    {
        min_clip = v_min_power;
    }

    lim_v_ref->min_clip = min_clip;

    // Apply limits to active voltage limits for this iteration

    if(max_clip < lim_ref->max_clip)
    {
        lim_ref->max_clip = max_clip;
    }

    if(min_clip > lim_ref->min_clip)
    {
        lim_ref->min_clip = min_clip;
    }
}


//!  Voltage reference limits for some circuits with large stored energy need to protect against
//!  excessive power losses in the output stage when ramping down the current. This can be done by
//!  defining an exclusion zone for positive voltages in quadrants 4 and 1. regLimVrefCalcRT
//!  will rotate the zone by 180 degrees to also define the exclusion zone for negative voltages
//!  in quadrants 3 and 2.  For example:
//!                                          ^ Voltage
//!                                          |
//!                          +---------------+---------------+  <- V_POS
//!                          |excl./         |               |
//!                          |zone/          |               |
//!                          |   /           |               |
//!                          |  /            |               |
//!                          | / Quadrant 4  |  Quadrant 1   |
//!                          |/              |               |
//!                          |               |               |
//!                          +---------------+---------------+-> Current
//!                          |               |               |
//!                          |               |              /|
//!                          |   Quadrant 3  |  Quadrant 2 / |
//!                          |               |            /  |
//!                          |               |           /   |
//!                          |               |          /excl|
//!                          |               |         / zone|
//!                          +---------------+---------------+ <- V_NEG
//!                        I_NEG                           I_POS

void regLimVrefCalcRT(struct REG_lim_ref         * const lim_ref,
                      struct REG_lim_v_ref const * const lim_v_ref,
                      cc_float                           i_meas,
                      bool                         const circuit_open)
{
    // Clamp voltage reference at zero when the circuit is open

    if(circuit_open == true)
    {
        lim_ref->max_clip = lim_ref->min_clip = 0.0F;
    }
    else
    {
        // Invert i_meas when limits are inverted

        if(*lim_ref->invert_limits == true)
        {
            i_meas = -i_meas;
        }

        // Calculate max positive voltage (Quadrants 41)

        lim_ref->max_clip = lim_v_ref->max_clip;

        if(i_meas < lim_v_ref->i_quadrants41_max)
        {
            cc_float v_lim = lim_v_ref->v0 + lim_v_ref->dvdi * i_meas;

            if(v_lim < 0.0F)
            {
                v_lim = 0.0F;
            }

            if(v_lim < lim_ref->max_clip)
            {
                lim_ref->max_clip = v_lim;
            }
        }

        // Calculate min negative voltage (Quadrants 32 uses the Q41 limits rotated by 180 degrees)

        lim_ref->min_clip = lim_v_ref->min_clip;

        if(i_meas > -lim_v_ref->i_quadrants41_max)
        {
            cc_float v_lim = -lim_v_ref->v0 + lim_v_ref->dvdi * i_meas;

            if(v_lim > 0.0F)
            {
                v_lim = 0.0F;
            }

            if(v_lim > lim_ref->min_clip)
            {
                lim_ref->min_clip = v_lim;
            }
        }
    }
}



cc_float regLimRefRT(struct REG_lim_ref * const lim_ref,
                     cc_float             const period,
                     cc_float                   ref,
                     cc_float             const prev_ref,
                     bool                 const limit_rate)

//! <h3> Implementation Notes </h3>
//!
//! On equipment where the rate limit is several orders of magnitude smaller than the
//! reference limit, it is possible that #REG_LIM_CLIP (margin on the rate limit,
//! usually 1 per mil) is too small compared to the relative precision of 32-bit floating-point
//! arithmetic (considered bounded by #REG_LIM_FP32_MARGIN = 2.0E-07 in this library).
//! A consequence of that was observed as a false positive on the rate clipping. That
//! can happen if:
//!
//! #REG_LIM_CLIP  * reg_lim_ref::rate_clip * period < #REG_LIM_FP32_MARGIN * reg_lim_ref::max_clip
//!
//! That is the reason why a margin equal to (#REG_LIM_FP32_MARGIN * prev_ref)
//! is kept on the rate clip limit in this function. In most cases it is insignificant,
//! but it will prevent the false positive in the rare cases mentioned above.

{
    bool rate_clipped_flag = false;

    // Clip reference to rate of change limits if required and if rate limit is positive

    if(limit_rate == true && lim_ref->rate_clip > 0.0F)
    {
        cc_float const delta_ref = ref - prev_ref;

        cc_float rate_lim_ref;

        if(delta_ref > 0.0F)
        {
            rate_lim_ref = prev_ref * (1.0F + REG_LIM_FP32_MARGIN) + lim_ref->rate_clip * period;

            if(ref > rate_lim_ref)
            {
                ref = rate_lim_ref;
                rate_clipped_flag = true;
            }
        }
        else if(delta_ref < 0.0F)
        {
            rate_lim_ref = prev_ref * (1.0F - REG_LIM_FP32_MARGIN) - lim_ref->rate_clip * period;

            if(ref < rate_lim_ref)
            {
                ref = rate_lim_ref;
                rate_clipped_flag = true;
            }
        }
    }

    // Clip reference to absolute limits taking into account the invert flag

    if(*lim_ref->invert_limits == true)
    {
        // Limits are inverted

        if(ref > -lim_ref->min_clip)
        {
            ref = -lim_ref->min_clip;
            lim_ref->flags.clipped = true;
            rate_clipped_flag      = false;
        }
        else if(ref < -lim_ref->max_clip)
        {
            ref = -lim_ref->max_clip;
            lim_ref->flags.clipped = true;
            rate_clipped_flag      = false;
        }
        else
        {
            lim_ref->flags.clipped = false;
        }
    }
    else
    {
        // Limits are NOT inverted

        if(ref < lim_ref->min_clip)
        {
            ref = lim_ref->min_clip;
            lim_ref->flags.clipped = true;
            rate_clipped_flag      = false;
        }
        else if(ref > lim_ref->max_clip)
        {
            ref = lim_ref->max_clip;
            lim_ref->flags.clipped = true;
            rate_clipped_flag      = false;
        }
        else
        {
            lim_ref->flags.clipped = false;
        }
    }

    lim_ref->flags.rate_clipped = rate_clipped_flag;

    return ref;
}

// EOF
