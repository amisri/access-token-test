//! @file  regHarmonics.c
//! @brief Converter Control Regulation library symmetric harmonicsupling functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


// NON-RT   regHarmonicsInit

void regHarmonicsInit(struct REG_harmonics * const harmonics,
                      cc_float             * const v_ac_buf,
                      uint32_t               const buf_len)
{
    // Save the pointers in the circular buffer and the buffer length

    harmonics->v_ac_buf = v_ac_buf;
    harmonics->buf_len  = buf_len;
}


// NON-RT   regHarmonicsPars

void regHarmonicsPars(struct REG_harmonics * const harmonics,
                      uint32_t               const ac_period_iters,
                      cc_float               const iter_period,
                      cc_float               const v_pos,
                      cc_float             * const gain,
                      cc_float             * const phase)
{
    if(harmonics->v_ac_buf != NULL && ac_period_iters > 10 && ac_period_iters <= harmonics->buf_len)
    {
        // Save the AC period and iteration period

        harmonics->ac_period_iters = ac_period_iters;
        harmonics->iter_period     = iter_period;

        cc_float const ac_period = (cc_float)ac_period_iters;
        cc_double const ac_period_iters_per_degree = (double)ac_period_iters / 360.0;

        // Calculate V_AC period measurement min/max limits for wrapping

        harmonics->v_ac_period_iters_limits[0] = ac_period * 0.5F;
        harmonics->v_ac_period_iters_limits[1] = harmonics->v_ac_period_iters_limits[0] * 3.0F;

        // Calculate clip limit to be 1% of the positive voltage limit

        harmonics->clip_limit = v_pos * 0.01F;

        // Process gain and phase for each harmonic

        uint32_t h;

        for(h = 0 ; h < REG_NUM_HARMONICS ; h++)
        {
            // Save the gain

            harmonics->gain[h] = gain[h];

            // Transform the phase from degrees to iterations and save it.
            // The phase must be positive and will be added to the buffer index so -10 degrees will be transformed into +350 degrees.
            // This ignores the potential change in the V_AC frequency (+/-0.4% in Europe).

            harmonics->phase[h] = fmodf((fmodf(phase[h], 360.0F) + 360.0F) * ac_period_iters_per_degree, ac_period);
        }

        // Enable harmonics generation

        harmonics->enabled = true;
    }
    else
    {
        harmonics->enabled = false;
    }

}



void regHarmonicsRT(struct REG_harmonics * const harmonics, cc_float const v_ac, bool const generate)
{
    cc_float ref_harmonics = 0.0F;

    if(harmonics->enabled == true)
    {
        // Harmonics generator is active - update buffer index

        uint32_t const ac_period_iters = harmonics->ac_period_iters;
        uint32_t       buf_index       = harmonics->buf_index;
        cc_float const prev_v_ac       = harmonics->v_ac_buf[buf_index];
        cc_float const ac_period       = (cc_float)ac_period_iters;

        buf_index = (buf_index + 1) % ac_period_iters;

        // Save the new measurement of V_AC

        harmonics->v_ac_buf[buf_index] = v_ac;

        // Check for positive zero crossing of V_AC

        if(prev_v_ac < 0.0F && v_ac >= 0.0F)
        {
            // In Europe the frequency can range from 49.8Hz to 50.2Hz (+/-0.4%), so measure the frequency using the zero
            // crossing buffer indexes.

            cc_float const zero_crossing = (cc_float)harmonics->buf_index + prev_v_ac / (prev_v_ac - v_ac);

            // Measure the V_AC period using the previous zero crossing measurement

            cc_float meas_ac_period_iters = ac_period + zero_crossing - harmonics->zero_crossing;

            harmonics->zero_crossing = zero_crossing;

            // Correct measured V_AC period when it is affected by buf_index wrapping

            if(meas_ac_period_iters < harmonics->v_ac_period_iters_limits[0])
            {
                meas_ac_period_iters += ac_period;
            }
            else if(meas_ac_period_iters > harmonics->v_ac_period_iters_limits[1])
            {
                meas_ac_period_iters -= ac_period;
            }

            // Convert V_AC period into a measured frequency and filter it using an accumulator

            harmonics->meas_v_ac_hz = 1.0F / (meas_ac_period_iters * harmonics->iter_period);
            harmonics->meas_v_ac_hz_acc += harmonics->meas_v_ac_hz;

            if(++harmonics->meas_v_ac_hz_counter >= REG_V_AC_HZ_FLTR_PERIOD)
            {
                harmonics->meas_v_ac_hz_fltr = harmonics->meas_v_ac_hz_acc * (1.0F / REG_V_AC_HZ_FLTR_PERIOD);
                harmonics->meas_v_ac_hz_acc = 0.0F;
                harmonics->meas_v_ac_hz_counter = 0;
            }
        }

        harmonics->buf_index = buf_index;

        // Only compute the harmonics when the generator is required to run (e.g. during fixed target beam spill)

        if(generate == true)
        {
            // Calculate the feed forward harmonics

            uint32_t h;

            for(h = 0 ; h < REG_NUM_HARMONICS ; h++)
            {
                // Use interpolation to improve phase resolution

                cc_float const index_fp = fmodf(  (cc_float)(buf_index * (h + 1))
                                                + (ac_period - harmonics->zero_crossing) * (cc_float)h
                                                + harmonics->phase[h], ac_period);
                cc_float       index_int;
                cc_float const index_frac = modff(index_fp,&index_int);
                uint32_t const index      = (uint32_t)index_int;
                cc_float const v_ac_0     = harmonics->v_ac_buf[index];
                cc_float const v_ac_1     = harmonics->v_ac_buf[(index + 1) % ac_period_iters];

                ref_harmonics += harmonics->gain[h] * (v_ac_0 + index_frac * (v_ac_1 - v_ac_0));
            }

            // Apply clip limits

            if(ref_harmonics > harmonics->clip_limit)
            {
                ref_harmonics = harmonics->clip_limit;
            }
            else if(ref_harmonics < -harmonics->clip_limit)
            {
                ref_harmonics = -harmonics->clip_limit;
            }
        }
    }

    // Save ref_harmonics

    harmonics->ref = ref_harmonics;
}

// EOF
