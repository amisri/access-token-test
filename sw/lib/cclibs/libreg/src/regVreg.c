//! @file  regVreg.c
//! @brief Converter Control Regulation library voltage regulation algorithm functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// NON-RT    regVregInit

void regVregInit(struct REG_v_pars * const pars,
                 cc_float            const k_int,
                 cc_float            const k_u,
                 cc_float            const k_d,
                 cc_float            const inv_load_ohms_dc)
{
    // Calculate damping input gain (Kw)

    pars->k_w = 1.0F + k_u + k_d * inv_load_ohms_dc;

    // Calculate the integrator anti-wind-up gain Kaw - protecting against Inf

    cc_float const k_w_k_int = pars->k_w * k_int;

    pars->k_aw = k_w_k_int != 0.0F
               ? 1.0F / k_w_k_int
               : 0.0F;
}


// NON-RT    regVregFiringLutInit

// If this is called, then regVregLimitsInit will also be called afterward

void regVregFiringLutInit(struct REG_v_pars * const pars,
                          enum REG_actuation  const actuation,
                          cc_float          * const firing_lut)
{
    // Save link to LUT in pars

    pars->firing_lut = firing_lut;

    // Scan LUT if actuation is FIRING_REF to enable/disable linearisation

    uint32_t i;
    bool     enabled = false;

    if(actuation == REG_FIRING_REF)
    {
        for(i = 0 ; i < REG_FIRING_LUT_LEN ; i++)
        {
            if(firing_lut[i] != 0.0F)
            {
                enabled = true;
                break;
            }
        }
    }

    pars->lut_enabled = enabled;
}


// NON-RT    regVregLimitsInit

// If regVregFiringLutInit is called, then this will also be called afterward

void regVregLimitsInit(struct REG_v_pars  * const pars,
                       enum REG_actuation   const actuation,
                       enum REG_firing_mode const firing_mode,
                       uint32_t             const num_vsources,
                       cc_float             const vs_gain,
                       cc_float             const v_pos,
                       cc_float             const v_neg)
{
    if(vs_gain > 0.0F && v_pos > 0.0F)
    {
        // Set 1/num_vsouces and protected against a zero setting

        pars->inv_num_vsources = num_vsources > 0
                               ? 1.0F / num_vsources
                               : 1.0F;
        // Converter gain is valid so cache the gain and inverse gain

        pars->vs_gain     = vs_gain;
        pars->inv_vs_gain = 1.0F / vs_gain;

        // Set firing limits using vs_gain - this will be true if the full DAC range is used by the firing card
        // then further restrict the firing limits to the voltage limits

        pars->f_pos = 10.0F * vs_gain;

        if(v_pos < pars->f_pos)
        {
            pars->f_pos = v_pos;
        }

        pars->f_neg = -pars->f_pos;

        if(v_neg > pars->f_neg)
        {
            pars->f_neg = v_neg;
        }

        // Calculate LUT index factor

        pars->lut_factor = (cc_float)(REG_FIRING_LUT_LEN - 1) / (pars->f_pos - pars->f_neg);

        // Set flag if actuation is FIRING_REF and LUT is not set for a 1-quadrant converter in ASAD mode

        pars->asad_1q_without_lut =    actuation == REG_FIRING_REF
                                    && pars->lut_enabled == false
                                    && v_neg >= 0.0F
                                    && firing_mode == REG_ASAD;

        // Set linearisation enabled flag

        pars->linearisation_enabled = pars->lut_enabled || pars->asad_1q_without_lut;
    }
    else
    {
        // Converter gain is invalid so reset all the parameters

        pars->inv_num_vsources = 0.0F;
        pars->vs_gain          = 0.0F;
        pars->inv_vs_gain      = 0.0F;
        pars->f_pos            = 0.0F;
        pars->f_neg            = 0.0F;
        pars->lut_factor       = 0.0F;

        pars->linearisation_enabled = false;
    }
}



cc_float regVregLinearizeRT(struct REG_v_pars * const pars, cc_float f_ref_limited)
{
    if(pars->asad_1q_without_lut)
    {
        // 1-quadrant converter with ASAD firing mode and no LUT defined
        // Map [0..f_pos] to [-f_pos..f_pos]

        return 2.0F * f_ref_limited - pars->f_pos;
    }

    // Convert f_ref_limited to the LUT index and offset

    cc_float const f_ref_index = (f_ref_limited - pars->f_neg) * pars->lut_factor;

    cc_float lut_index_float;

    cc_float lut_offset = modff(f_ref_index, &lut_index_float);
    int32_t  lut_index  = (int32_t)lut_index_float;

    // Check that lut_index_float is within the valid range

    if(lut_index < 0)
    {
        return pars->firing_lut[0];
    }

    if(lut_index >= (REG_FIRING_LUT_LEN - 1))
    {
        return pars->firing_lut[REG_FIRING_LUT_LEN - 1];
    }

    // Use linear interpolation to return the linearised f_ref_limited

    return   pars->firing_lut[lut_index+1] * lut_offset
           + pars->firing_lut[lut_index]   * (1.0 - lut_offset);
}

// EOF
