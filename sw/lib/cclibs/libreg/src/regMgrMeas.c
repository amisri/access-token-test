//! @file  regMgrMeas.c
//! @brief Converter Control Regulation library measurement management functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


//! Set measurement inputs to simulated values and set validity to true for all signals
//!
//! This is real-time function.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.

static inline void regMgrMeasSimulatedRT(struct REG_mgr * const reg_mgr)
{
    // Use simulated measurements which are always valid

    reg_mgr->inputs.b_meas.meas.signal      = reg_mgr->sim_vars.b_meas;
    reg_mgr->inputs.i_meas.meas.signal      = reg_mgr->sim_vars.i_meas;
    reg_mgr->inputs.i_mag_sat.meas.signal   = 0.0F;
    reg_mgr->inputs.i_capa.meas.signal      = reg_mgr->sim_vars.i_capa_meas;
    reg_mgr->inputs.v_meas.meas.signal      = reg_mgr->sim_vars.v_meas;

    reg_mgr->inputs.b_meas.meas.is_valid    = true;
    reg_mgr->inputs.i_meas.meas.is_valid    = true;
    reg_mgr->inputs.i_mag_sat.meas.is_valid = true;
    reg_mgr->inputs.i_capa.meas.is_valid    = true;
    reg_mgr->inputs.v_meas.meas.is_valid    = true;
}


//! Check current or field measurement input signal
//!
//! This function will invent a current or field measurement if it is invalid. It runs the measurement filter on the signal.
//! It saves the unfiltered, filtered or extrapolated measurement in the RST history.
//! It also checks the measurement against the trip limits and the zero and low limits.
//!
//! This is real-time function.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.
//! @param[in]     reg_signal             Pointer to B or I regulation structure.
//! @param[in]     input                  Pointer to measurement input structure.
//! @param[in]     is_regulated           True if reg_signal is being regulated
//! @retval        Returns unfiltered measurement.

static cc_float regMgrMeasBICheckRT(struct REG_mgr        * const reg_mgr,
                                    struct REG_mgr_signal * const reg_signal,
                                    struct REG_mgr_input  * const input,
                                    bool                    const is_regulated)
{
    cc_float meas_unfiltered;

    if(input->meas.is_valid == false)
    {
        // Measured signal is invalid

        if(is_regulated == true && reg_signal->op_rst_pars.active->reg_err_rate == REG_ERR_RATE_MEASUREMENT)
        {
            // Signal is being regulated and reg error rate is measurement, so the delayed ref is available to estimate the measured signal

            meas_unfiltered = reg_signal->meas.signal[REG_MEAS_UNFILTERED] = reg_signal->ref_delayed + reg_signal->reg_err.err;
        }
        else if(input->fault.flag == false)
        {
            // Signal is not being regulated and does not (yet) have a fault so extrapolate previous signal value using the estimated rate of change

            meas_unfiltered = reg_signal->meas.signal[REG_MEAS_UNFILTERED] += reg_signal->meas.rate * reg_mgr->iter_period_fp32;
        }
        else
        {
            // Signal is marked as faulty (invalid for more then REG_MEAS_MAX_INVALID_MEAS iterations) so hold the previous value

            meas_unfiltered = reg_signal->meas.signal[REG_MEAS_UNFILTERED];
        }
    }
    else
    {
        // Measured signal is valid so it can be used

        meas_unfiltered = reg_signal->meas.signal[REG_MEAS_UNFILTERED] = input->unfiltered = input->meas.signal;
    }

    // Track signal invalidity and set failed and fault flags if too many consecutive samples are invalid and limits are defined for the signal

    regMeasSignalInvalidCounterRT(&input->fault, input->meas.is_valid, reg_signal->lim_meas.flags.enabled, (reg_signal->iteration_index == 0));

    // Filter the measurement and prepare to estimate the measurement rate

    regMeasFilterRT(&reg_signal->meas);

    // Check measurement limits

    regLimMeasRT(&reg_signal->lim_meas, meas_unfiltered);

    // On regulation iterations, update RST history index and store new measurement

    if(reg_signal->iteration_index == 0)
    {
        regRstIncHistoryIndexRT(&reg_signal->rst_vars);

        reg_signal->meas.reg = reg_signal->rst_vars.meas[reg_signal->rst_vars.history_index] = reg_signal->meas.signal[reg_signal->rst_pars->reg_meas_select];

        regMeasRateRT(&reg_signal->meas, reg_signal->inv_reg_period);
    }

    return meas_unfiltered;
}


//! Check voltage measurement input signal
//!
//! This function will:
//!
//! * invent the voltage measurement if it is invalid
//! * check the voltage measurement against the trip limits if actuation is FIRING_REF or LIMITS_V_ERR_FAULT is > 0
//! * calculate and check the voltage error
//!
//! This is real-time function.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.

static inline void regMgrMeasVCheckRT(struct REG_mgr * const reg_mgr)
{
    bool const v_meas_is_valid = reg_mgr->inputs.v_meas.meas.is_valid;
    bool const v_meas_required = (regMgrParValue(reg_mgr, VS_ACTUATION) == REG_FIRING_REF || reg_mgr->v.reg_err.fault.threshold > 0.0F);

    // Estimate unfiltered voltage measurement if input is invalid using simulated voltage measurement and the v_reg error.

    cc_float const v_meas_unfiltered = reg_mgr->v.unfiltered = v_meas_is_valid == true
                                                             ? reg_mgr->inputs.v_meas.meas.signal
                                                             : reg_mgr->sim_vars.v_meas + reg_mgr->v.reg_err.err;

    // Check the voltage measurement validity and allow faults only if the voltage regulation is in use

    regMeasSignalInvalidCounterRT(&reg_mgr->inputs.v_meas.fault, v_meas_is_valid, v_meas_required, true);

     // Check voltage measurement against the trip limit if voltage measurement faults are allowed

     if(v_meas_required == true)
     {
         regLimMeasRT(&reg_mgr->v.lim_meas, v_meas_unfiltered);
     }
     else
     {
         reg_mgr->v.lim_meas.flags.trip = false;
     }

    // Calculate the voltage error and check against limits when converter is running
    // Error = actual - expected = v_meas - v_meas_sim

    regErrCheckLimitsRT(&reg_mgr->v.reg_err,
                         v_meas_unfiltered,                  // Actual
                         reg_mgr->sim_vars.v_meas,           // Expected
                         reg_mgr->reg_mode != REG_NONE,      // Check the faults and warning limits
                         false);                             // Do not calculate RMS_ERR
}



//! Check magnet saturation current measurement input signal
//!
//! This function will incorporate the i_mag_sat if available. It will hold the last value if it becomes invalid.
//!
//! This is real-time function.
//!
//! @param[in,out]    reg_mgr                Pointer to regulation manager structure.
//! @param[in]        i_meas_unfiltered      Filtered current measurement for the local circuit

static inline void regMgrMeasImagSatCheckRT(struct REG_mgr * const reg_mgr, cc_float const i_meas_unfiltered)
{
    // Hold last valid i_mag_sat input value in case it becomes invalid in future.
    // Libreg has no warning because i_mag_sat will be rarely used anyway and the application will need to manage
    // failures of i_mag_sat.

    if(reg_mgr->inputs.i_mag_sat.meas.is_valid == true)
    {
        reg_mgr->load_sat_vars.last_valid_i_mag_sat_input = reg_mgr->inputs.i_mag_sat.meas.signal;
    }

    // Calculate i_mag_sat by mixing i_mag_sat input with i_meas_unfiltered (using LOAD_I_SAT_GAIN)

    reg_mgr->load_sat_vars.i_mag_sat = reg_mgr->load_sat_vars.last_valid_i_mag_sat_input +
                                       regMgrParValue(reg_mgr, LOAD_I_SAT_GAIN) * i_meas_unfiltered;

    // Calculate the saturation factor using the operational or test load parameters

    struct REG_load_pars * const load_pars = reg_mgr->reg_rst_source == REG_OPERATIONAL_RST_PARS
                                           ? &reg_mgr->load_pars
                                           : &reg_mgr->load_pars_test;

    reg_mgr->load_sat_vars.sat_factor = regLoadSatFactorRT(load_pars, reg_mgr->load_sat_vars.i_mag_sat);
}


//! Check output filter capacitor current measurement input signal
//!
//! This function will check if the i_capa input is invalid for too long when required and will set a fault in this case.
//! The function holds the last valid value if the input becomes invalid.
//!
//! This is real-time function.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.

static inline void regMgrMeasIcapaCheckRT(struct REG_mgr * const reg_mgr)
{
    // Set the i_capa measurement if the input is valid - hold previous value otherwise

    if(reg_mgr->inputs.i_capa.meas.is_valid == true)
    {
        reg_mgr->inputs.i_capa.unfiltered = reg_mgr->inputs.i_capa.meas.signal;
    }

    // Only allow an i_capa fault if the signal is actually being used

    bool const allow_fault = (regMgrParValue(reg_mgr, VS_ACTUATION)          == REG_FIRING_REF   &&
                              regMgrParValue(reg_mgr, VFILTER_I_CAPA_SOURCE) == REG_MEASUREMENT);

    regMeasSignalInvalidCounterRT(&reg_mgr->inputs.i_capa.fault, reg_mgr->inputs.i_capa.meas.is_valid, allow_fault, true);
}



uint32_t regMgrMeasSetRT(struct REG_mgr    * const reg_mgr,
                         enum REG_rst_source const reg_rst_source,
                         struct CC_ns_time   const iter_time,
                         bool                const invert_limits,
                         bool                const circuit_open)
{
    // Store parameters for this iteration

    reg_mgr->reg_rst_source = reg_rst_source;
    reg_mgr->invert_limits  = invert_limits;

    // If simulated measurements are enabled then set the measurements from the simulation

    if(reg_mgr->sim_meas == CC_ENABLED)
    {
        regMgrMeasSimulatedRT(reg_mgr);
    }

    // Check for new RST parameters and set or increment iteration counters for field and current regulation

    if(reg_mgr->b.lim_meas.flags.enabled == true)
    {
        regMgrRegBIsignalPrepareRT(reg_mgr, &reg_mgr->b, iter_time);
    }

    if(reg_mgr->i.lim_meas.flags.enabled == true)
    {
        regMgrRegBIsignalPrepareRT(reg_mgr, &reg_mgr->i, iter_time);
    }

    // If regulating CURRENT or FIELD then calculate the delayed reference

    uint32_t            iteration_index;
    enum REG_mode const reg_mode = reg_mgr->reg_mode;

    if(reg_mode == REG_CURRENT || reg_mode == REG_FIELD)
    {
        struct REG_mgr_signal * const reg_signal = reg_mgr->reg_signal;

        // Update the delayed reference every iteration if reg_err_rate is MEASUREMENT or on regulation periods

        if(   reg_signal->rst_pars->reg_err_rate == REG_ERR_RATE_MEASUREMENT
           || reg_signal->iteration_index == 0)
        {
            reg_signal->ref_delayed = regRstDelayedRefRT(reg_signal->rst_pars,
                                                        &reg_signal->rst_vars,
                                                         reg_signal->iteration_index);
        }

        // Prepare to return the iteration counter for the active regulation mode and set the ref_advance and track_delay

        iteration_index      = reg_signal->iteration_index;
        reg_mgr->ref_advance = reg_signal->rst_pars->ref_advance;
        reg_mgr->track_delay = reg_signal->rst_pars->track_delay;
    }
    else // REG_NONE or REG_VOLTAGE
    {
        iteration_index      = 0;
        reg_mgr->ref_advance = reg_mgr->v.reg_pars.ref_advance;
        reg_mgr->track_delay = reg_mgr->ref_advance;
    }

    // Process current measurement if it is active

    if(reg_mgr->i.lim_meas.flags.enabled == true)
    {
        // Check the measured current validity and replace it if invalid - this requires reg_signal->ref_delayed

        cc_float i_meas_unfiltered = regMgrMeasBICheckRT(reg_mgr, &reg_mgr->i, &reg_mgr->inputs.i_meas, (reg_mode == REG_CURRENT));

        // Check current RMS limits

        regLimRmsRT(&reg_mgr->lim_i_rms,      i_meas_unfiltered);
        regLimRmsRT(&reg_mgr->lim_i_rms_load, i_meas_unfiltered);

        // Check i_mag_sat input - mix with i_meas_unfiltered if valid

        regMgrMeasImagSatCheckRT(reg_mgr, i_meas_unfiltered);

        // Calculate voltage reference limits for the measured current (V limits can depend on measured current)

        regLimVrefCalcRT(&reg_mgr->v.lim_ref, &reg_mgr->v.lim_v_ref, reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED], circuit_open);
    }
    else
    {
        // Calculate voltage reference limits assuming current is zero

        regLimVrefCalcRT(&reg_mgr->v.lim_ref, &reg_mgr->v.lim_v_ref, 0.0F, circuit_open);

        // Reset current measurement fault flags

        regLimMeasRT(&reg_mgr->i.lim_meas, 0.0F);

        reg_mgr->inputs.i_meas.fault.flag = false;
    }

    // Check field measurement if it is active

    if(reg_mgr->b.lim_meas.flags.enabled == true)
    {
        // Check the measured field validity and replace it if invalid - this requires reg_signal->ref_delayed

        regMgrMeasBICheckRT(reg_mgr, &reg_mgr->b, &reg_mgr->inputs.b_meas, (reg_mode == REG_FIELD));
    }
    else
    {
        // Reset field measurement fault flags

        regLimMeasRT(&reg_mgr->b.lim_meas, 0.0F);

        reg_mgr->inputs.b_meas.fault.flag = false;
    }

    // Check voltage measurement - estimate the voltage if measurement is invalid and check trip limits if actuation is FIRING_REF

    regMgrMeasVCheckRT(reg_mgr);

    // Check i_capa input

    regMgrMeasIcapaCheckRT(reg_mgr);

    // Return the iteration counter for the selected regulation mode

    return iteration_index;
}



void regMgrMeasSetVffRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal v_ff)
{
    // The fault flag is computed but will not actually trip the converter

    regMeasSignalInvalidCounterRT(&reg_mgr->inputs.v_ff.fault, v_ff.is_valid, true, true);

    if(reg_mgr->inputs.v_ff.fault.flag == false)
    {
        // Feed forward signal is okay so save it in v_ff input measurement signal

        reg_mgr->inputs.v_ff.meas = v_ff;
    }
    else
    {
        // Feed forward signal is not available so decay v_ff exponentially to avoid a sudden jump

        reg_mgr->inputs.v_ff.meas.signal *= 0.9F;
    }

    // Filter the V_FF signal if the filter is enabled

    reg_mgr->v.v_ff_fltr.signal = reg_mgr->v.v_ff_fltr.pars.is_vs_undersampled == false
                                ? regSimVsRT(&reg_mgr->v.v_ff_fltr.pars, &reg_mgr->v.v_ff_fltr.vars, reg_mgr->inputs.v_ff.meas.signal)
                                : reg_mgr->inputs.v_ff.meas.signal;
}

// EOF
