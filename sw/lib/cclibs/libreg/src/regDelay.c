//! @file  regDelay.c
//! @brief Converter Control Regulation library signal delay functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


// NON-RT   regDelayInitDelay

void regDelayInitDelay(struct REG_delay_pars * const pars, cc_float delay_iters)
{
    cc_float delay_int;

    // Clip delay

    if(delay_iters < 0.0F)
    {
        delay_iters = 0.0F;
    }
    else if(delay_iters > (REG_DELAY_BUF_INDEX_MASK - 1.0F))
    {
        delay_iters = REG_DELAY_BUF_INDEX_MASK - 1.0F;
    }

    // Calculate integer and fractional parts of the delay in iterations

    pars->delay_frac_iters = modff(delay_iters, &delay_int);
    pars->delay_int_iters  = (int32_t)delay_int;
}


// NON-RT   regDelayInitVars

void regDelayInitVars(struct REG_delay_vars * const vars, cc_float const initial_signal)
{
    uint32_t i;

    for(i = 0 ; i <= REG_DELAY_BUF_INDEX_MASK ; i++)
    {
        vars->buf[i] = initial_signal;
    }
}



cc_float regDelaySignalRT(struct REG_delay_pars const * const pars,
                          struct REG_delay_vars       * const vars,
                          cc_float                      const signal,
                          bool                          const is_under_sampled)
{
    vars->buf[++vars->buf_index & REG_DELAY_BUF_INDEX_MASK] = signal;

    cc_float const s0 = vars->buf[(vars->buf_index - pars->delay_int_iters    ) & REG_DELAY_BUF_INDEX_MASK];

    // When not undersampled, use linear interpolation

    if(is_under_sampled == false)
    {
        cc_float const s1 = vars->buf[(vars->buf_index - pars->delay_int_iters - 1) & REG_DELAY_BUF_INDEX_MASK];

        return s0 + pars->delay_frac_iters * (s1 - s0);
    }

    // When under-sampled, jump to final value at the start of each period

    return s0;
}

// EOF
