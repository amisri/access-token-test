//! @file  regLoad.c
//! @brief Converter Control Regulation library Load-related functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


// NON-RT   regLoadInit

void regLoadInit(struct REG_load_pars * const load,
                 cc_float                     ohms_ser,
                 cc_float               const ohms_par,
                 cc_float                     ohms_mag,
                 cc_float                     henrys,
                 cc_float               const gauss_per_amp)
{
    // Calculate load related parameters

    if(ohms_par > 1.0E-10F && henrys > 1.0E-10F)
    {
        // Clip ohms_ser to avoid Inf

        if(ohms_mag < 1.0E-10F && ohms_ser < 1.0E-10F)
        {
            ohms_ser = 1.0E-10F;
            ohms_mag = 0.0F;
        }

        // Magnet included in circuit

        load->ohms1 = 1.0F + ohms_ser / ohms_par;
        load->ohms2 = 1.0F + ohms_mag / ohms_par;
        load->ohms  = ohms_mag + ohms_ser / load->ohms1;    // Resistance at load pole frequency
        load->tc    = henrys / load->ohms;                  // Time constant of load pole

        load->gain0 = 1.0F / (ohms_par * load->ohms1);
        load->gain2 = 1.0F / (ohms_ser + ohms_mag / load->ohms2);    // DC gain
        load->gain1 = load->gain2 - load->gain0;
        load->gain3 = 1.0F / load->ohms2;

        // Parallel resistance is significant if ohms_par < 1E4.ohms_ser or ohms_par < 1E4.ohms_mag

        load->significant_ohms_par = load->ohms1 > 1.0001F || load->ohms2 > 1.0001F;
    }
    else
    {
        // Clip ohms_ser to avoid Inf

        if(ohms_ser < 1.0E-10F)
        {
            ohms_ser = 1.0E-10F;
        }

        // Rp or Henrys is effectively zero (i.e. no magnet in the circuit)

        henrys       = 0.0F;

        load->ohms1  = 1.0E30F;
        load->ohms2  = 0.0F;
        load->ohms   = ohms_ser;

        load->gain0  = 1.0F / ohms_ser;
        load->gain1  = 0.0F;
        load->gain2  = load->gain0;
        load->gain3  = 0.0F;

        load->tc     = 1.0E-20F;

        load->significant_ohms_par = false;
    }

    // Save the load parameters

    load->ohms_ser      = ohms_ser;
    load->ohms_par      = ohms_par;
    load->ohms_mag      = ohms_mag;
    load->henrys        = henrys;
    load->gauss_per_amp = gauss_per_amp > 0.0F ? gauss_per_amp : 1.0F;

    // Initialize measured load

    load->meas_ohms   = load->ohms;
    load->meas_henrys = henrys;

    // Clip inv_henrys to avoid infinity

    if(henrys > 1.0E-20F)
    {
        load->inv_henrys = 1.0F / henrys;
    }
    else
    {
        load->inv_henrys = 1.0E+20F;
    }
}


// NON-RT   regLoadInitSat

void regLoadInitSat(struct REG_load_pars * const load,
                    cc_float               const henrys_sat,
                    cc_float               const i_sat_start,
                    cc_float               const i_sat_end,
                    cc_float                     sat_smoothing)
{
    if(henrys_sat > 0.0F && load->henrys > henrys_sat && i_sat_start > 0.0F && i_sat_end > i_sat_start)
    {
        // Clip saturation smoothing factor to 0.0001 - 0.5

        if(sat_smoothing <= 0.0F)
        {
            sat_smoothing = 0.0001F;
        }
        else if(sat_smoothing > 0.5F)
        {
            sat_smoothing = 0.5F;
        }

        // Save input parameters

        load->sat.henrys    = henrys_sat;
        load->sat.i_start   = i_sat_start;
        load->sat.i_end     = i_sat_end;
        load->sat.smoothing = sat_smoothing;

        // Calculate parameters defining the first four segments of the normalized saturation model:
        //    0.linear  1.parabola  2.linear  3.parabola
        // There is a fifth linear segment, but this requires no additional parameters.

        cc_float const delta_I_sat = i_sat_end - i_sat_start;
        cc_float const f_sat       = 1.0F - henrys_sat / load->henrys;

        load->sat.f[1] = f_sat * sat_smoothing;
        load->sat.f[2] = f_sat * (1.0F - sat_smoothing);
        load->sat.f[3] = f_sat;

        load->sat.i[1] = i_sat_start + delta_I_sat * sat_smoothing;
        load->sat.i[2] = i_sat_end   - delta_I_sat * sat_smoothing;

        load->sat.df_dI_linear = f_sat / delta_I_sat;

        cc_float const delta_I_parabola = 2.0F * load->sat.f[1] / load->sat.df_dI_linear;

        load->sat.d2f_dI2_parabola = load->sat.df_dI_linear / delta_I_parabola;

        load->sat.i[0] = load->sat.i[1] - delta_I_parabola;
        load->sat.i[3] = load->sat.i[2] + delta_I_parabola;

        load->sat.integrated_f[1] = load->sat.d2f_dI2_parabola * delta_I_parabola * delta_I_parabola * delta_I_parabola / 6.0F;
        load->sat.integrated_f[2] = load->sat.integrated_f[1] + f_sat * (load->sat.i[2] - load->sat.i[1]) / 2.0F;
        load->sat.integrated_f[3] = f_sat * (load->sat.i[3] - load->sat.i[0]) / 2.0F;
    }
    else
    {
        // Disable saturation

        load->sat.i_end = 0.0F;
     }
}



cc_float regLoadSatFactorRT(struct REG_load_pars const * const load, cc_float const i_mag_sat)
{
    // Return immediately if saturation model is not enabled

    if(load->sat.i_end <= 0.0F)
    {
        return 1.0F;
    }

    // Calculate normalized saturation factor (starts at zero and goes up)
    // out of five segments: linear-parabolic-linear-parabolic-linear.

    cc_float i0;        // current relative to end of the segment 0 (linear)
    cc_float i1;        // current relative to end of the segment 1 (parabola)
    cc_float i3;        // current relative to end of the segment 3 (parabola)
    cc_float f;         // normalized saturation factor

    cc_float const abs_i_mag_sat = fabsf(i_mag_sat);

    if((i0 = abs_i_mag_sat - load->sat.i[0]) <= 0.0F)
    {
        // Segment 0: linear

        f = 0.0F;
    }
    else if((i1 = abs_i_mag_sat - load->sat.i[1]) <= 0.0F)
    {
        // Segment 1: parabola

        f = 0.5F * load->sat.d2f_dI2_parabola * i0 * i0;
    }
    else if(abs_i_mag_sat <= load->sat.i[2])
    {
        // Segment 2: linear

        f = load->sat.f[1] + load->sat.df_dI_linear * i1;
    }
    else if((i3 = abs_i_mag_sat - load->sat.i[3]) <= 0.0F)
    {
        // Segment 3: parabola

        f = load->sat.f[3] - 0.5F * load->sat.d2f_dI2_parabola * i3 * i3;
    }
    else
    {
        // Segment 4: linear

        f = load->sat.f[3];
    }

    // Return saturation factor (starts at 1.0 and goes down)

    return 1.0F - f;
}



cc_float regLoadCurrentToFieldRT(struct REG_load_pars const * const load, cc_float const i_meas)
{
    // Return immediately if saturation model is not enabled

    if(load->sat.i_end <= 0.0F)
    {
        return load->gauss_per_amp * i_meas;
    }

    // Calculate integrated normalized saturation factor (starts at zero and goes up)
    // out of five segments: linear-parabolic-linear-parabolic-linear.

    cc_float i0;                // current relative to end of the segment 0 (linear)
    cc_float i1;                // current relative to end of the segment 1 (parabola)
    cc_float i2;                // current relative to end of the segment 2 (linear)
    cc_float i3;                // current relative to end of the segment 3 (parabola)
    cc_float integrated_f_dI;   // normalized saturation factor

    cc_float const abs_i_mag_sat = fabsf(i_meas);

    if((i0 = abs_i_mag_sat - load->sat.i[0]) <= 0.0F)
    {
        // Segment 0: linear

        integrated_f_dI = 0.0F;
    }
    else if((i1 = abs_i_mag_sat - load->sat.i[1]) <= 0.0F)
    {
        // Segment 1: parabola

        integrated_f_dI = load->sat.d2f_dI2_parabola * i0 * i0 * i0 / 6.0F;
    }
    else if((i2 = abs_i_mag_sat - load->sat.i[2]) < 0.0F)
    {
        // Segment 2: linear

        integrated_f_dI = load->sat.integrated_f[1] + i1 * (load->sat.f[1] + 0.5F * load->sat.df_dI_linear * i1);
    }
    else if((i3 = abs_i_mag_sat - load->sat.i[3]) <= 0.0F)
    {
        // Segment 3: parabola

        integrated_f_dI = load->sat.integrated_f[2] + load->sat.f[3] * i2 - load->sat.integrated_f[1] - load->sat.d2f_dI2_parabola * i3 * i3 * i3 / 6.0F;
    }
    else
    {
        // Segment 4: linear

        integrated_f_dI = load->sat.integrated_f[3] + load->sat.f[3] * i3;
    }

    // Return magnet field

    cc_float const abs_i = abs_i_mag_sat - integrated_f_dI;

    return load->gauss_per_amp * (i_meas < 0.0F ? -abs_i : abs_i);
}

// EOF
