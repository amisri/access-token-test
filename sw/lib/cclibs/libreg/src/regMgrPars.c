//! @file  regMgrPars.c
//! @brief Converter Control Regulation library parameter  management functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

//! Filter time for REG_ERR fault/warning for field and current regulation

#define REG_ERR_FILTER_TIME_ITERS   3

// Macros

#define regMgrStaticAssertHierarchy(before,after)    CC_STATIC_ASSERT(REG_PAR_GROUP_##before*2 == REG_PAR_GROUP_##after, before##_out_of_order)

// Check constants that must be a power of two.
// REG_NUM_LOADS is specified in WriteRegParsHeader() in libreg/parameters/pars.awk which generates regPars.h.

CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REG_NUM_LOADS),REG_NUM_LOADS_is_not_a_power_of_2);
CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REG_SIM_NUM_DEN_LEN),REG_SIM_NUM_DEN_LEN_is_not_a_power_of_2);
CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REG_NUM_RST_COEFFS), REG_NUM_RST_COEFFS_is_not_a_power_of_2);

// Protect against the size of enum not being 32-bit

CC_STATIC_ASSERT(sizeof(enum CC_enabled_disabled)==4,sizeof_enum_must_be_4_bytes);


// NON-RT   regMgrParsCheck

void regMgrParsCheck(struct REG_mgr * const reg_mgr)
{
    struct REG_pars * const reg_pars = reg_mgr->pars;

    // Get local copy of load_select according to the reg_mode:
    //   Only use the application's parameter value when reg_mode is NONE
    //   Otherwise use the previous LOAD_SELECT value

    uint32_t const load_select = reg_mgr->reg_mode == REG_NONE
                               ? regMgrParAppValue(reg_pars, LOAD_SELECT) & (REG_NUM_LOADS - 1)
                               : regMgrParValue(reg_mgr, LOAD_SELECT);

    // Take local copy of load_test_select and protect against crazy values

    uint32_t const load_test_select = regMgrParAppValue(reg_pars, LOAD_TEST_SELECT) & (REG_NUM_LOADS - 1);

    // Lock mutex if required, to protect against running at the same time as regMgrParsProcess()

    if(reg_mgr->mutex)
    {
        reg_mgr->mutex_lock(reg_mgr->mutex);
    }

    uint32_t par_groups_mask  = 0;
    uint32_t par_idx;

    for(par_idx = 0 ; par_idx < REG_NUM_PARS ; par_idx++)
    {
        struct REG_pars_meta const * const par_meta = &reg_pars->meta[par_idx];

        uint8_t  const * value_src = (uint8_t const *)par_meta->app_value;
        uint32_t const   flags     = par_meta->flags;

        // Skip parameters that are not relevant

        if(   (reg_mgr->reg_mode     == REG_NONE   || (flags & REG_PAR_FLAG_MODE_NONE_ONLY) == 0)
           && (reg_mgr->b.regulation == CC_ENABLED || (flags & REG_PAR_FLAG_FIELD_REG     ) == 0)
           && (reg_mgr->i.regulation == CC_ENABLED || (flags & REG_PAR_FLAG_CURRENT_REG   ) == 0)
           && (reg_mgr->v.regulation == CC_ENABLED || (flags & REG_PAR_FLAG_VOLTAGE_REG   ) == 0))
        {
            uint8_t      * value_dest    = (uint8_t *)par_meta->lib_value;
            size_t   const size_in_bytes = par_meta->size_in_bytes;
            uint32_t const groups        = par_meta->groups;

            // If parameter is an array based on load select then point to scalar value addressed by load_select

            if((flags & REG_PAR_FLAG_LOAD_SELECT) != 0)
            {
                value_src += load_select * size_in_bytes;
            }

            // If parameter value has changed

            if(memcmp(value_dest, value_src, size_in_bytes) != 0)
            {
                // Save the changed value and set groups mask for this parameter

                memcpy(value_dest, value_src, size_in_bytes);

                par_groups_mask |= groups;
            }

            // If parameter is an array based on load select and it is flagged as being a test parameter,
            // then copy scalar value addressed by load_test_select if it has changed.

            if((flags & (REG_PAR_FLAG_LOAD_SELECT | REG_PAR_FLAG_TEST_PAR)) == (REG_PAR_FLAG_LOAD_SELECT | REG_PAR_FLAG_TEST_PAR))
            {
                value_src   = (uint8_t const *)par_meta->app_value + load_test_select * size_in_bytes;
                value_dest += size_in_bytes;

                // If parameter value has changed

                if(memcmp(value_dest, value_src, size_in_bytes) != 0)
                {
                    // Save the changed value and set flags for this parameter

                    memcpy(value_dest, value_src, size_in_bytes);

                    par_groups_mask |= groups;
                }
            }
        }
    }

    if(par_groups_mask != 0)
    {
        reg_mgr->par_groups_mask |= (par_groups_mask & reg_mgr->par_enabled_groups_mask);
    }

    // Save the load select and load_test_select values - these are protected against crazy values

    regMgrParValue(reg_mgr,LOAD_SELECT)      = load_select;
    regMgrParValue(reg_mgr,LOAD_TEST_SELECT) = load_test_select;

    // Unlock mutex if in use

    if(reg_mgr->mutex)
    {
        reg_mgr->mutex_unlock(reg_mgr->mutex);
    }

    // Process any changed parameters

    regMgrParsProcess(reg_mgr);
}


// NON-RT   regMgrParsProcess

void regMgrParsProcess(struct REG_mgr * const reg_mgr)
{
    // Return immediately if no changed parameters are pending processing

    if(reg_mgr->par_groups_mask == 0)
    {
        return;
    }

    // Lock mutex if required, to protect against running at the same time as regMgrParsCheck()

    if(reg_mgr->mutex)
    {
        reg_mgr->mutex_lock(reg_mgr->mutex);
    }

    // Check every parameter group bit mask flag in hierarchical order

    // Use CC_STATIC_ASSERT to check that hierarchical order is not violated. The order is defined in pars.csv.

    // REG_PAR_GROUP_ITER_PERIOD

    CC_STATIC_ASSERT(REG_PAR_GROUP_ITER_PERIOD == 1, REG_PAR_GROUP_ITER_PERIOD_is_not_the_first_parameter_group);

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_ITER_PERIOD) != 0)
    {
        // Initialize the iteration period

        regMgrInitIterPeriod(reg_mgr, regMgrParValue(reg_mgr, ITER_PERIOD_NS));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_ITER_PERIOD;
    }

    regMgrStaticAssertHierarchy(ITER_PERIOD,MEAS_SIM_DELAYS);

    // REG_PAR_GROUP_MEAS_SIM_DELAYS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_MEAS_SIM_DELAYS) != 0)
    {
        // Set actuation delay to include firing delay when actuation is FIRING

        cc_float act_delay_iters = regMgrParValue(reg_mgr,VS_ACT_DELAY_ITERS);

        if(regMgrParValue(reg_mgr,VS_ACTUATION) == REG_FIRING_REF)
        {
            act_delay_iters += regMgrParValue(reg_mgr,VS_FIRING_DELAY) * reg_mgr->inv_iter_period;
        }

        // Initialize delays for field, current and voltage measurements - subtract 1 because one iteration is automatic

        regDelayInitDelay(&reg_mgr->sim_pars.b_delay, act_delay_iters + regMgrParValue(reg_mgr,MEAS_B_DELAY_ITERS) - 1.0F);
        regDelayInitDelay(&reg_mgr->sim_pars.i_delay, act_delay_iters + regMgrParValue(reg_mgr,MEAS_I_DELAY_ITERS) - 1.0F);
        regDelayInitDelay(&reg_mgr->sim_pars.v_delay, act_delay_iters + regMgrParValue(reg_mgr,MEAS_V_DELAY_ITERS) - 1.0F);

        // Set VS_ACT_DELAY in seconds so that it can be used by the simulated load signals in liblog as the time offset

        reg_mgr->vs_act_delay = act_delay_iters * reg_mgr->iter_period_fp32;

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_MEAS_SIM_DELAYS;
    }

    regMgrStaticAssertHierarchy(MEAS_SIM_DELAYS, VS_SIM);

    // REG_PAR_VS_SIM

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_VS_SIM) != 0)
    {
        // Initialize Simulator 1 voltage source parameters - if VS_ACTUATION is FIRING_REF then Simulator 2 will also run and its
        // signals will be used, however the Simulator 1 signals are available to be logged and compared against Simulator 2.
        // Simulator 1 parameters should always be set, since they are used to estimate the voltage source response delay,
        // which is part of the pure delay used by the RST synthesis algorithms.

        regSimVsInit(          &reg_mgr->sim_pars.vs,
                                reg_mgr->iter_period_fp32,
                                regMgrParValue(reg_mgr,VS_SIM_BANDWIDTH),
                                regMgrParValue(reg_mgr,VS_SIM_Z),
                                regMgrParValue(reg_mgr,VS_SIM_TAU_ZERO),
                                regMgrParPointer(reg_mgr,VS_SIM_NUM),
                                regMgrParPointer(reg_mgr,VS_SIM_DEN));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_VS_SIM;
    }

    regMgrStaticAssertHierarchy(VS_SIM, V_LIMITS_MEAS);

    // REG_PAR_GROUP_V_LIMITS_MEAS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_V_LIMITS_MEAS) != 0)
    {
        regLimMeasInit(          &reg_mgr->v.lim_meas,
                                 &reg_mgr->invert_limits,
                                  regMgrParValue(reg_mgr,LIMITS_V_POS),
                                  regMgrParValue(reg_mgr,LIMITS_V_NEG),
                                  0.0F,                                           // low_lim is always zero for V_MEAS
                                  0.0F);                                          // zero_lim is always zero for V_MEAS

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_V_LIMITS_MEAS;
    }

    regMgrStaticAssertHierarchy(V_LIMITS_MEAS, V_LIMITS_REF);

    // REG_PAR_GROUP_V_LIMITS_REF

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_V_LIMITS_REF) != 0)
    {
        regLimRefInit(           &reg_mgr->v.lim_ref,
                                 &reg_mgr->invert_limits,
                                  regMgrParValue(reg_mgr,LIMITS_V_POS),
                                  0.0F,                                           // standby limit is always zero for V_REF
                                  regMgrParValue(reg_mgr,LIMITS_V_NEG),
                                  regMgrParValue(reg_mgr,LIMITS_V_RATE),
                                  0.0F);                                          // closeloop is always zero for V_REF

        regLimVrefInit(          &reg_mgr->v.lim_ref,
                                 &reg_mgr->v.lim_v_ref,
                                  regMgrParPointer(reg_mgr,LIMITS_I_QUADRANTS41),
                                  regMgrParPointer(reg_mgr,LIMITS_V_QUADRANTS41),
                                  regMgrParValue(reg_mgr,LIMITS_P_POS),
                                  regMgrParValue(reg_mgr,LIMITS_P_NEG));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_V_LIMITS_REF;
    }

    regMgrStaticAssertHierarchy(V_LIMITS_REF, V_LIMITS_RATE_RMS);

    // GROUP_V_LIMITS_RATE_RMS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_V_LIMITS_RATE_RMS) != 0)
    {
        // The voltage error warning and fault thresholds apply to reference and regulation errors

        regLimRmsInit(         &reg_mgr->lim_v_rate_rms,
                                0.0F,
                                regMgrParValue(reg_mgr,LIMITS_V_RATE_RMS_FAULT),
                                regMgrParValue(reg_mgr,LIMITS_V_RATE_RMS_TC),
                                reg_mgr->iter_period_fp32);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_V_LIMITS_RATE_RMS;
    }

    regMgrStaticAssertHierarchy(V_LIMITS_RATE_RMS, I_LIMITS_MEAS);

    // REG_PAR_GROUP_I_LIMITS_MEAS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_I_LIMITS_MEAS) != 0)
    {
        regLimMeasInit(        &reg_mgr->i.lim_meas,
                               &reg_mgr->invert_limits,
                                regMgrParValue(reg_mgr,LIMITS_I_POS),
                                regMgrParValue(reg_mgr,LIMITS_I_NEG),
                                regMgrParValue(reg_mgr,LIMITS_I_LOW),
                                regMgrParValue(reg_mgr,LIMITS_I_ZERO));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_I_LIMITS_MEAS;
    }

    regMgrStaticAssertHierarchy(I_LIMITS_MEAS, I_LIMITS_REF);

    // REG_PAR_GROUP_I_LIMITS_REF

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_I_LIMITS_REF) != 0)
    {
        regLimRefInit (        &reg_mgr->i.lim_ref,
                               &reg_mgr->invert_limits,
                                regMgrParValue(reg_mgr,LIMITS_I_POS),
                                regMgrParValue(reg_mgr,LIMITS_I_STANDBY),
                                regMgrParValue(reg_mgr,LIMITS_I_NEG),
                                regMgrParValue(reg_mgr,LIMITS_I_RATE),
                                regMgrParValue(reg_mgr,LIMITS_I_CLOSELOOP));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_I_LIMITS_REF;
    }

    regMgrStaticAssertHierarchy(I_LIMITS_REF, I_LIMITS_ERR);

    // REG_PAR_GROUP_I_LIMITS_ERR

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_I_LIMITS_ERR) != 0)
    {
        regErrInitLimits(      &reg_mgr->i.reg_err,
                                regMgrParValue(reg_mgr,LIMITS_I_ERR_WARNING),
                                regMgrParValue(reg_mgr,LIMITS_I_ERR_FAULT),
                                REG_ERR_FILTER_TIME_ITERS);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_I_LIMITS_ERR;
    }

    regMgrStaticAssertHierarchy(I_LIMITS_ERR, B_LIMITS_MEAS);

    // REG_PAR_GROUP_B_LIMITS_MEAS (reg mode NONE only)

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_B_LIMITS_MEAS) != 0)
    {
        regLimMeasInit(        &reg_mgr->b.lim_meas,
                               &reg_mgr->invert_limits,
                                regMgrParValue(reg_mgr,LIMITS_B_POS),
                                regMgrParValue(reg_mgr,LIMITS_B_NEG),
                                regMgrParValue(reg_mgr,LIMITS_B_LOW),
                                regMgrParValue(reg_mgr,LIMITS_B_ZERO));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_B_LIMITS_MEAS;
    }

    regMgrStaticAssertHierarchy(B_LIMITS_MEAS, B_LIMITS_REF);

    // REG_PAR_GROUP_B_LIMITS_REF

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_B_LIMITS_REF) != 0)
    {
        regLimRefInit (        &reg_mgr->b.lim_ref,
                               &reg_mgr->invert_limits,
                                regMgrParValue(reg_mgr,LIMITS_B_POS),
                                regMgrParValue(reg_mgr,LIMITS_B_STANDBY),
                                regMgrParValue(reg_mgr,LIMITS_B_NEG),
                                regMgrParValue(reg_mgr,LIMITS_B_RATE),
                                regMgrParValue(reg_mgr,LIMITS_B_CLOSELOOP));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_B_LIMITS_REF;
    }

    regMgrStaticAssertHierarchy(B_LIMITS_REF, B_LIMITS_ERR);

    // REG_PAR_GROUP_B_LIMITS_ERR

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_B_LIMITS_ERR) != 0)
    {
        // The field error warning and fault thresholds apply to reference and regulation errors

        regErrInitLimits(      &reg_mgr->b.reg_err,
                                regMgrParValue(reg_mgr,LIMITS_B_ERR_WARNING),
                                regMgrParValue(reg_mgr,LIMITS_B_ERR_FAULT),
                                REG_ERR_FILTER_TIME_ITERS);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_B_LIMITS_ERR;
    }

    regMgrStaticAssertHierarchy(B_LIMITS_ERR, DECO);

    // REG_PAR_GROUP_DECO

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_DECO) != 0)
    {
        regDecoPars(           &reg_mgr->deco,
                                regMgrParValue(reg_mgr,DECO_PHASE),
                                regMgrParValue(reg_mgr,DECO_INDEX),
                                regMgrParValue(reg_mgr,LOAD_SELECT),
                                regMgrParValue(reg_mgr,BREG_PERIOD_ITERS),
                                regMgrParValue(reg_mgr,IREG_PERIOD_ITERS),
                                reg_mgr->b.lim_meas.flags.enabled,
                                reg_mgr->i.lim_meas.flags.enabled);

        if(reg_mgr->deco.new_phase > 0)
        {
            reg_mgr->b.log_time_offset = -reg_mgr->b.reg_period;
            reg_mgr->i.log_time_offset = -reg_mgr->i.reg_period;
        }
        else
        {
            reg_mgr->b.log_time_offset = 0.0F;
            reg_mgr->i.log_time_offset = 0.0F;
        }

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_DECO;
    }

    regMgrStaticAssertHierarchy(DECO, I_LIMITS_RMS);

    // REG_PAR_GROUP_I_LIMITS_RMS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_I_LIMITS_RMS) != 0)
    {
        regLimRmsInit(         &reg_mgr->lim_i_rms,
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_WARNING),
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_FAULT),
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_TC),
                                reg_mgr->iter_period_fp32);

        regLimRmsInit(         &reg_mgr->lim_i_rms_load,
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_LOAD_WARNING),
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_LOAD_FAULT),
                                regMgrParValue(reg_mgr,LIMITS_I_RMS_LOAD_TC),
                                reg_mgr->iter_period_fp32);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_I_LIMITS_RMS;
    }

    regMgrStaticAssertHierarchy(I_LIMITS_RMS, I_MEAS_FILTER);

    // REG_PAR_GROUP_I_MEAS_FILTER

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_I_MEAS_FILTER) != 0)
    {
        regMeasFilterInit(     &reg_mgr->i.meas,
                                regMgrParPointer(reg_mgr,MEAS_I_FIR_LENGTHS),
                                regMgrParValue(reg_mgr,IREG_PERIOD_ITERS),
                                regMgrParValue(reg_mgr,LIMITS_I_POS),
                                regMgrParValue(reg_mgr,LIMITS_I_NEG),
                                regMgrParValue(reg_mgr,MEAS_I_DELAY_ITERS));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_I_MEAS_FILTER;
    }

    regMgrStaticAssertHierarchy(I_MEAS_FILTER, B_MEAS_FILTER);

    // REG_PAR_GROUP_B_MEAS_FILTER

    if(  (reg_mgr->par_groups_mask & REG_PAR_GROUP_B_MEAS_FILTER) != 0)
    {
        regMeasFilterInit(     &reg_mgr->b.meas,
                                regMgrParPointer(reg_mgr,MEAS_B_FIR_LENGTHS),
                                regMgrParValue(reg_mgr,BREG_PERIOD_ITERS),
                                regMgrParValue(reg_mgr,LIMITS_B_POS),
                                regMgrParValue(reg_mgr,LIMITS_B_NEG),
                                regMgrParValue(reg_mgr,MEAS_B_DELAY_ITERS));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_B_MEAS_FILTER;
    }

    regMgrStaticAssertHierarchy(B_MEAS_FILTER, LOAD);

    // REG_PAR_GROUP_LOAD

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_LOAD) != 0)
    {
        regLoadInit(           &reg_mgr->load_pars,
                                regMgrParValue(reg_mgr,LOAD_OHMS_SER),
                                regMgrParValue(reg_mgr,LOAD_OHMS_PAR),
                                regMgrParValue(reg_mgr,LOAD_OHMS_MAG),
                                regMgrParValue(reg_mgr,LOAD_HENRYS),
                                regMgrParValue(reg_mgr,LOAD_GAUSS_PER_AMP));

        regLoadInitSat(        &reg_mgr->load_pars,
                                regMgrParValue(reg_mgr,LOAD_HENRYS_SAT),
                                regMgrParValue(reg_mgr,LOAD_I_SAT_START),
                                regMgrParValue(reg_mgr,LOAD_I_SAT_END),
                                regMgrParValue(reg_mgr,LOAD_SAT_SMOOTHING));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_LOAD;
    }

    regMgrStaticAssertHierarchy(LOAD, LOAD_SIM);

    // REG_PAR_GROUP_LOAD_SIM

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_LOAD_SIM) != 0)
    {
        regSimLoadInit(        &reg_mgr->sim_pars.load,
                               &reg_mgr->load_pars,
                                regMgrParValue(reg_mgr,LOAD_SIM_TC_ERROR),
                                reg_mgr->iter_period_fp32);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_LOAD_SIM;
    }

    regMgrStaticAssertHierarchy(LOAD_SIM, FIRING_LUT);

    // REG_PAR_GROUP_FIRING_LUT

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_FIRING_LUT) != 0)
    {
        regVregFiringLutInit(  &reg_mgr->v.reg_pars,
                                regMgrParValue(reg_mgr,VS_ACTUATION),
                                regMgrParPointer(reg_mgr,VS_FIRING_LUT));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_FIRING_LUT;
    }

    regMgrStaticAssertHierarchy(FIRING_LUT, VREG);

    // REG_PAR_GROUP_VREG

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_VREG) != 0)
    {
        if(regMgrParValue(reg_mgr,VS_ACTUATION) == REG_FIRING_REF)
        {
            regVregInit(       &reg_mgr->v.reg_pars,
                                regMgrParValue(reg_mgr,VREG_EXT_K_INT),
                                regMgrParValue(reg_mgr,VFILTER_EXT_K_U),
                                regMgrParValue(reg_mgr,VFILTER_EXT_K_D),
                                reg_mgr->load_pars.gain2);                  // gain2 = 1 / load_ohms_dc
        }

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_VREG;
    }

    regMgrStaticAssertHierarchy(VREG, VREF_ADV);

    // REG_PAR_GROUP_VREF_ADV

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_VREF_ADV) != 0)
    {
        reg_mgr->v.reg_pars.ref_advance = reg_mgr->iter_period_fp32 * (regMgrParValue(reg_mgr,VS_ACT_DELAY_ITERS) + reg_mgr->sim_pars.vs.rsp_delay_iters)
                                        + (regMgrParValue(reg_mgr, VS_ACTUATION) == REG_FIRING_REF
                                        ? regMgrParValue(reg_mgr, VS_FIRING_DELAY)
                                        : 0.0F);

        reg_mgr->v.reg_pars.ref_advance_ns = (int32_t)(reg_mgr->v.reg_pars.ref_advance * CCTIME_NS_PER_S + 0.5F);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_VREF_ADV;
    }

    regMgrStaticAssertHierarchy(VREF_ADV, V_LIMITS_ERR);

    // REG_PAR_GROUP_V_LIMITS_ERR

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_V_LIMITS_ERR) != 0)
    {
        // The filter time is set to 3 times the voltage source response time - this is measure of the bandwidth of the
        // voltage regulator, and should give an idea of how long perturbations take to be controlled. It is rounded up.

        uint32_t const filter_time_iters = (uint32_t)(3.0F * (reg_mgr->sim_pars.vs.rsp_delay_iters + 1.0F));

        regErrInitLimits(        &reg_mgr->v.reg_err,
                                  regMgrParValue(reg_mgr,LIMITS_V_ERR_WARNING),
                                  regMgrParValue(reg_mgr,LIMITS_V_ERR_FAULT),
                                  filter_time_iters);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_V_LIMITS_ERR;
    }

    regMgrStaticAssertHierarchy(V_LIMITS_ERR, VREG_LIMITS);

    // REG_PAR_GROUP_VREG_LIMITS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_VREG_LIMITS) != 0)
    {
        regVregLimitsInit(       &reg_mgr->v.reg_pars,
                                  regMgrParValue(reg_mgr,VS_ACTUATION),
                                  regMgrParValue(reg_mgr,VS_FIRING_MODE),
                                  regMgrParValue(reg_mgr,VS_NUM_VSOURCES),
                                  regMgrParValue(reg_mgr,VS_GAIN),
                                  reg_mgr->v.lim_ref.pos,
                                  reg_mgr->v.lim_ref.neg);

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_VREG_LIMITS;
    }

    regMgrStaticAssertHierarchy(VREG_LIMITS, IREG);

    // REG_PAR_GROUP_IREG

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_IREG) != 0 && reg_mgr->i.op_rst_pars.use_next != 1)
    {
        reg_mgr->i.op_rst_pars.use_next = 0;

        regMgrInitRst(          reg_mgr,
                                REG_CURRENT,
                                REG_OPERATIONAL_RST_PARS,
                                regMgrParValue(reg_mgr,IREG_PERIOD_ITERS),
                                regMgrParValue(reg_mgr,IREG_EXTERNAL_ALG),
                                regMgrParValue(reg_mgr,IREG_INT_MEAS_SELECT),
                                regMgrParValue(reg_mgr,IREG_INT_PURE_DELAY_PERIODS),
                                regMgrParValue(reg_mgr,IREG_INT_AUXPOLE1_HZ),
                                regMgrParValue(reg_mgr,IREG_INT_AUXPOLES2_HZ),
                                regMgrParValue(reg_mgr,IREG_INT_AUXPOLES2_Z),
                                regMgrParValue(reg_mgr,IREG_INT_AUXPOLE4_HZ),
                                regMgrParValue(reg_mgr,IREG_INT_AUXPOLE5_HZ),
                                regMgrParValue(reg_mgr,IREG_EXT_MEAS_SELECT),
                                regMgrParValue(reg_mgr,IREG_EXT_TRACK_DELAY_PERIODS),
                                regMgrParPointer(reg_mgr,IREG_EXT_OP_R),
                                regMgrParPointer(reg_mgr,IREG_EXT_OP_S),
                                regMgrParPointer(reg_mgr,IREG_EXT_OP_T));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_IREG;
    }

    regMgrStaticAssertHierarchy(IREG, BREG);

    // REG_PAR_GROUP_BREG

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_BREG) != 0 && reg_mgr->b.op_rst_pars.use_next != 1)
    {
        reg_mgr->b.op_rst_pars.use_next = 0;

        regMgrInitRst(          reg_mgr,
                                REG_FIELD,
                                REG_OPERATIONAL_RST_PARS,
                                regMgrParValue(reg_mgr,BREG_PERIOD_ITERS),
                                regMgrParValue(reg_mgr,BREG_EXTERNAL_ALG),
                                regMgrParValue(reg_mgr,BREG_INT_MEAS_SELECT),
                                regMgrParValue(reg_mgr,BREG_INT_PURE_DELAY_PERIODS),
                                regMgrParValue(reg_mgr,BREG_INT_AUXPOLE1_HZ),
                                regMgrParValue(reg_mgr,BREG_INT_AUXPOLES2_HZ),
                                regMgrParValue(reg_mgr,BREG_INT_AUXPOLES2_Z),
                                regMgrParValue(reg_mgr,BREG_INT_AUXPOLE4_HZ),
                                regMgrParValue(reg_mgr,BREG_INT_AUXPOLE5_HZ),
                                regMgrParValue(reg_mgr,BREG_EXT_MEAS_SELECT),
                                regMgrParValue(reg_mgr,BREG_EXT_TRACK_DELAY_PERIODS),
                                regMgrParPointer(reg_mgr,BREG_EXT_OP_R),
                                regMgrParPointer(reg_mgr,BREG_EXT_OP_S),
                                regMgrParPointer(reg_mgr,BREG_EXT_OP_T));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_BREG;
    }

    regMgrStaticAssertHierarchy(BREG, LOAD_TEST);

    // REG_PAR_GROUP_LOAD_TEST

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_LOAD_TEST) != 0)
    {
        regLoadInit(           &reg_mgr->load_pars_test,
                                regMgrParValueTest(reg_mgr,LOAD_OHMS_SER),
                                regMgrParValueTest(reg_mgr,LOAD_OHMS_PAR),
                                regMgrParValueTest(reg_mgr,LOAD_OHMS_MAG),
                                regMgrParValueTest(reg_mgr,LOAD_HENRYS),
                                regMgrParValueTest(reg_mgr,LOAD_GAUSS_PER_AMP));

        regLoadInitSat(        &reg_mgr->load_pars_test,
                                regMgrParValueTest(reg_mgr,LOAD_HENRYS_SAT),
                                regMgrParValueTest(reg_mgr,LOAD_I_SAT_START),
                                regMgrParValueTest(reg_mgr,LOAD_I_SAT_END),
                                regMgrParValueTest(reg_mgr,LOAD_SAT_SMOOTHING));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_LOAD_TEST;
    }

    regMgrStaticAssertHierarchy(LOAD_TEST, IREG_TEST);

    // REG_PAR_GROUP_IREG_TEST

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_IREG_TEST) != 0 && reg_mgr->i.test_rst_pars.use_next != 1)
    {
        reg_mgr->i.test_rst_pars.use_next = 0;

        regMgrInitRst(          reg_mgr,
                                REG_CURRENT,
                                REG_TEST_RST_PARS,
                                regMgrParValue(reg_mgr,IREG_PERIOD_ITERS),
                                regMgrParValueTest(reg_mgr,IREG_EXTERNAL_ALG),
                                regMgrParValueTest(reg_mgr,IREG_INT_MEAS_SELECT),
                                regMgrParValueTest(reg_mgr,IREG_INT_PURE_DELAY_PERIODS),
                                regMgrParValueTest(reg_mgr,IREG_INT_AUXPOLE1_HZ),
                                regMgrParValueTest(reg_mgr,IREG_INT_AUXPOLES2_HZ),
                                regMgrParValueTest(reg_mgr,IREG_INT_AUXPOLES2_Z),
                                regMgrParValueTest(reg_mgr,IREG_INT_AUXPOLE4_HZ),
                                regMgrParValueTest(reg_mgr,IREG_INT_AUXPOLE5_HZ),
                                regMgrParValueTest(reg_mgr,IREG_EXT_MEAS_SELECT),
                                regMgrParValueTest(reg_mgr,IREG_EXT_TRACK_DELAY_PERIODS),
                                regMgrParPointer(reg_mgr,IREG_EXT_TEST_R),
                                regMgrParPointer(reg_mgr,IREG_EXT_TEST_S),
                                regMgrParPointer(reg_mgr,IREG_EXT_TEST_T));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_IREG_TEST;
    }

    regMgrStaticAssertHierarchy(IREG_TEST, BREG_TEST);

    // REG_PAR_GROUP_BREG_TEST

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_BREG_TEST) != 0 && reg_mgr->b.test_rst_pars.use_next != 1)
    {
        reg_mgr->b.test_rst_pars.use_next = 0;

        regMgrInitRst(          reg_mgr,
                                REG_FIELD,
                                REG_TEST_RST_PARS,
                                regMgrParValue(reg_mgr,BREG_PERIOD_ITERS),
                                regMgrParValueTest(reg_mgr,BREG_EXTERNAL_ALG),
                                regMgrParValueTest(reg_mgr,BREG_INT_MEAS_SELECT),
                                regMgrParValueTest(reg_mgr,BREG_INT_PURE_DELAY_PERIODS),
                                regMgrParValueTest(reg_mgr,BREG_INT_AUXPOLE1_HZ),
                                regMgrParValueTest(reg_mgr,BREG_INT_AUXPOLES2_HZ),
                                regMgrParValueTest(reg_mgr,BREG_INT_AUXPOLES2_Z),
                                regMgrParValueTest(reg_mgr,BREG_INT_AUXPOLE4_HZ),
                                regMgrParValueTest(reg_mgr,BREG_INT_AUXPOLE5_HZ),
                                regMgrParValueTest(reg_mgr,BREG_EXT_MEAS_SELECT),
                                regMgrParValueTest(reg_mgr,BREG_EXT_TRACK_DELAY_PERIODS),
                                regMgrParPointer(reg_mgr,BREG_EXT_TEST_R),
                                regMgrParPointer(reg_mgr,BREG_EXT_TEST_S),
                                regMgrParPointer(reg_mgr,BREG_EXT_TEST_T));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_BREG_TEST;
    }

    regMgrStaticAssertHierarchy(BREG_TEST, VFEEDFWD);

    // REG_PAR_GROUP_VFEEDFWD

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_VFEEDFWD) != 0)
    {
            regSimVsInit(        &reg_mgr->v.v_ff_fltr.pars,
                                  reg_mgr->iter_period_fp32,
                                  0.0F,                                // sim_bandwidth - setting zero forces use of num/den
                                  0.0F,                                // sim_z
                                  0.0F,                                // sim_tau_zero
                                  regMgrParPointer(reg_mgr,VFEEDFWD_NUM),
                                  regMgrParPointer(reg_mgr,VFEEDFWD_DEN));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_VFEEDFWD;
    }

    regMgrStaticAssertHierarchy(VFEEDFWD, HARMONICS);

    // REG_PAR_GROUP_HARMONICS

    if((reg_mgr->par_groups_mask & REG_PAR_GROUP_HARMONICS) != 0)
    {
        // Initialise the harmonics generator

        regHarmonicsPars(       &reg_mgr->v.harmonics,
                                regMgrParValue(reg_mgr,HARMONICS_AC_PERIOD_ITERS),
                                reg_mgr->iter_period_fp32,
                                regMgrParValue(reg_mgr,LIMITS_V_POS),
                                regMgrParPointer(reg_mgr,HARMONICS_GAIN),
                                regMgrParPointer(reg_mgr,HARMONICS_PHASE));

        reg_mgr->par_groups_mask &= ~REG_PAR_GROUP_HARMONICS;
    }

    // Unlock mutex if in use

    if(reg_mgr->mutex)
    {
        reg_mgr->mutex_unlock(reg_mgr->mutex);
    }
}

// EOF
