//! @file  regMgrSim.c
//! @brief Converter Control Regulation library simulation management functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// Real-time functions

void regMgrSimVsAndLoadRT(struct REG_mgr * const reg_mgr, bool const circuit_is_open)
{
    struct REG_mgr_sim_vars       * const sim_vars = &reg_mgr->sim_vars;
    struct REG_mgr_sim_pars const * const sim_pars = &reg_mgr->sim_pars;

    // Simulator 1 - always run voltage source and load simulations

    if(circuit_is_open)
    {
        // Circuit is open, no current can flow so reset the vs voltage and load simulation integrator

        sim_vars->vs.circuit_voltage = 0.0F;
        sim_vars->load.integrator    = 0.0;
    }
    else
    {
        cc_float v_ref = reg_mgr->v.ref;

        if(   reg_mgr->i.lim_ref.flags.unipolar
           && (   (   reg_mgr->invert_limits == true
                   && reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED] >= 0.0F
                   && v_ref > 0.0F)
               || (   reg_mgr->invert_limits == false
                   && reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED] <= 0.0F
                   && v_ref < 0.0F)))
        {
            // Simulating unipolar converter where the current has reached zero - the voltage collapses

            v_ref = 0.0F;
        }

        sim_vars->vs.circuit_voltage = regSimVsRT(&sim_pars->vs, &sim_vars->vs, v_ref);
    }

    regSimLoadRT(&sim_pars->load, &sim_vars->load, sim_pars->vs.is_vs_undersampled, sim_vars->vs.circuit_voltage, reg_mgr->load_sat_vars.i_mag_sat);

    if(regMgrParValue(reg_mgr, VS_ACTUATION) == REG_VOLTAGE_REF)
    {
        // VOLTAGE_REF Actuation - Use the simulator 1 signals

        sim_vars->signals.circuit_voltage = sim_vars->vs.circuit_voltage;
        sim_vars->signals.circuit_current = sim_vars->load.circuit_current;
        sim_vars->signals.magnet_field    = regLoadCurrentToFieldRT(&sim_pars->load.pars, sim_vars->load.magnet_current);
    }
    else
    {
        // FIRING_REF Actuation - Simulator 2

        if(circuit_is_open)
        {
            // Circuit is open, no current can flow so reset the simulated state vector and simulation signals

            memset(&sim_vars->vs_and_load, 0, sizeof(sim_vars->vs_and_load));
        }
        else
        {
            // Simulate voltage and load, including a Kalman filter

            regSim2RT(&sim_pars->vs_and_load,
                      &sim_vars->vs_and_load,
                       reg_mgr->v.reg_vars.f_ref_limited,
                       reg_mgr->v.unfiltered,
                       reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED],
                       reg_mgr->v.reg_vars.i_capa);
        }

        // Use the simulator 2 signals

        sim_vars->signals.circuit_voltage = sim_vars->vs_and_load.circuit_voltage;
        sim_vars->signals.circuit_current = sim_vars->vs_and_load.circuit_current;
        sim_vars->signals.magnet_field    = regLoadCurrentToFieldRT(&sim_pars->load.pars, sim_vars->vs_and_load.magnet_current);
    }
}



void regMgrSimMeasurementsRT(struct REG_mgr * const reg_mgr, struct REG_mgr_sim_vars * const sim_vars)
{
    struct REG_mgr_sim_pars const * const sim_pars = &reg_mgr->sim_pars;

    // Use delays to estimate the measurement of the magnet's field and the circuit's current and voltage

    sim_vars->b_meas = regDelaySignalRT(&sim_pars->b_delay,
                                        &sim_vars->b_delay,
                                         sim_vars->signals.magnet_field,
                                         sim_pars->vs.is_vs_undersampled && sim_pars->load.is_load_undersampled);

    sim_vars->i_meas = regDelaySignalRT(&sim_pars->i_delay,
                                        &sim_vars->i_delay,
                                         sim_vars->signals.circuit_current,
                                         sim_pars->vs.is_vs_undersampled && sim_pars->load.is_load_undersampled);

    sim_vars->v_meas = regDelaySignalRT(&sim_pars->v_delay,
                                        &sim_vars->v_delay,
                                         sim_vars->signals.circuit_voltage,
                                         sim_pars->vs.is_vs_undersampled);

    // Use delay to estimate i_capa measurement when actuation is FIRING_REF

    if(regMgrParValue(reg_mgr, VS_ACTUATION) == REG_FIRING_REF)
    {
        // Use the same delay for i_capa_meas as for v_meas and assume it is always well sampled

        sim_vars->i_capa_meas = regDelaySignalRT(&sim_pars->v_delay,
                                                 &sim_vars->i_capa_delay,
                                                  sim_vars->vs_and_load.capa_current,
                                                  false);
    }
    else
    {
        sim_vars->i_capa_meas = 0.0F;
    }
}



void regMgrSimMeasSetRT(struct REG_mgr * const reg_mgr, bool const sim_meas)
{
    // When the measurement simulation mode changes, reset the simulation variables

    if(reg_mgr->sim_meas != sim_meas)
    {
        memset(&reg_mgr->sim_vars.vs,          0, sizeof(reg_mgr->sim_vars.vs));
        memset(&reg_mgr->sim_vars.load,        0, sizeof(reg_mgr->sim_vars.load));
        memset(&reg_mgr->sim_vars.vs_and_load, 0, sizeof(reg_mgr->sim_vars.vs_and_load));

        regMeasRateInit(&reg_mgr->b.meas, 0.0F);
        regMeasRateInit(&reg_mgr->i.meas, 0.0F);

        reg_mgr->sim_meas = sim_meas;
    }
}

// EOF
