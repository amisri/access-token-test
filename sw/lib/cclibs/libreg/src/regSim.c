//! @file  regSim.c
//! @brief Converter Control Regulation library voltage source and load simulation functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"
#include "libreg/regVreg.h"

// Constants

// #define CCPRINTF

#define REG_SIM_NUM_DEN_LEN_MASK      (REG_SIM_NUM_DEN_LEN-1)


// NON-RT    regSimVsInit

void regSimVsInit(struct REG_sim_vs_pars * const pars,
                  cc_float                 const iter_period,
                  cc_float                 const bandwidth,
                  cc_float                       z,
                  cc_float                 const tau_zero,
                  cc_float                 const num[REG_SIM_NUM_DEN_LEN],
                  cc_float                 const den[REG_SIM_NUM_DEN_LEN])
{
    // If bandwidth is positive, use Tustin to calculate z-transform for voltage source using second order model

    if(bandwidth > 0.0F)
    {
        // Clip z to [0.01.. 1.0]

        if(z < 0.01F)
        {
            z = 0.01F;
        }
        else if(z > 1.0F)
        {
            z = 1.0F;
        }

        // Calculate the natural frequency in Hertz from the bandwidth and damping

        cc_float const natural_freq_hz = regNaturalFrequencyHz(bandwidth, z);

        // Calculate the delay for a steady ramp (see doc/pdf/model/FirstSecondOrder.pdf page 36)

        pars->rsp_delay_iters = 2.0F * z / (M_TWO_PI * natural_freq_hz * iter_period);

        // If voltage source model is too under-sampled then do not attempt Tustin algorithm

        if(pars->rsp_delay_iters < 0.25)
        {
            pars->is_vs_undersampled = true;
            pars->gain = 1.0F;

            pars->den[0] = pars->num[0] = 1.0F;
            pars->den[1] = pars->den[2] = pars->den[3] = pars->num[1] = pars->num[2] = pars->num[3] = 0.0F;

        }
        else
        {
            cc_float w;
            cc_float b;

            pars->is_vs_undersampled = false;

            // Tustin will match z-transform and s-transform at frequency f_pw

            if(z < 0.7F)      // If lightly damped, there is a resonance peak: f_pw = frequency of peak
            {
                cc_float const f_pw = natural_freq_hz * sqrtf(1.0F - 2.0F * z * z);

                w  = M_PI * iter_period * f_pw;
                b  = tan(w) / w;
            }
            else              // else heavily damped, there is no resonance peak: f_pw = 0 (minimizes approximation error)
            {
                w  = 0.0F;
                b  = 1.0F;
            }

            // Calculate intermediate variables

            cc_float const d  = 2.0F * tau_zero / (iter_period * b);
            cc_float const y  = M_PI * iter_period * b * natural_freq_hz;
            cc_float const de = 1.0F / (y * y + 2.0F * z * y + 1.0F);

            // Numerator (b0, b1, b2, b3) coefficients

            pars->num[0] = (y * y * (1.0F + d)) * de;
            pars->num[1] = (y * y * 2.0F) * de;
            pars->num[2] = (y * y * (1.0F - d)) * de;
            pars->num[3] = 0.0F;

            // Denominator (a0, a1, a2, a3) coefficient

            pars->den[0] = 1.0F;
            pars->den[1] = (y * y * 2.0F - 2.0F) * de;
            pars->den[2] = (y * y - 2.0F * z * y + 1.0F) * de;
            pars->den[3] = 0.0F;

            // Set the gain to 1

            pars->gain = 1.0F;
        }
    }
    else
    {
        // Use voltage source model provided in num and den arrays

        memcpy(pars->num, num, sizeof(pars->num));
        memcpy(pars->den, den, sizeof(pars->den));

        // Calculate gain of voltage source model and delay for a steady ramp
        // Steady ramp delay = ( Sum(i*num[i])*Sum(den[i)) – Sum(num[i])*Sum(i*den[i]) ) / ( Sum(num[i])* Sum(den[i]) )

        cc_float sum_num       = 0.0F;
        cc_float sum_den       = 0.0F;
        cc_float sum_idx_x_num = 0.0F;
        cc_float sum_idx_x_den = 0.0F;
        uint32_t i;

        for(i = 0 ; i < REG_SIM_NUM_DEN_LEN ; i++)
        {
            sum_num += pars->num[i];
            sum_den += pars->den[i];

            sum_idx_x_num += (cc_float)i * pars->num[i];
            sum_idx_x_den += (cc_float)i * pars->den[i];
        }

        // Protect gain and rsp_delay_iters against divide by zero

        if(sum_den == 0.0F || sum_num == 0.0F)
        {
            pars->gain = 1.0F;
            pars->rsp_delay_iters = 0.0F;
        }
        else
        {
           pars->gain = sum_num / sum_den;
           pars->rsp_delay_iters = (sum_idx_x_num * sum_den - sum_num * sum_idx_x_den) / (sum_den * sum_num);
        }

        // Mark TF as under-sampled if both sum_den and sum_num are zero or den[0] is zero - this will deactivate the filter

        pars->is_vs_undersampled = (sum_den == 0.0F && sum_num == 0.0F) || pars->den[0] == 0.0F;

        ccprintf("sum_den=%g  sum_num=%g  gain=%g  rsp_delay_iters=%.15E  is_vs_undersampled=%u\n",
                  sum_den,sum_num,pars->gain,pars->rsp_delay_iters,pars->is_vs_undersampled);
    }
}


// NON-RT    regSimVsInitHistory

cc_float regSimVsInitHistory(struct REG_sim_vs_pars const * const pars,
                             struct REG_sim_vs_vars       * const vars,
                             cc_float                       const init_rsp)
{
    cc_float const init_act = init_rsp / pars->gain;
    uint32_t       idx;

    // Initialize history arrays for actuation and response

    for(idx = 0 ; idx < REG_SIM_NUM_DEN_LEN ; idx++)
    {
        vars->act[idx] = init_act;
        vars->rsp[idx] = init_rsp;
    }

    return init_act;
}


// NON-RT    regSimLoadInit

void regSimLoadInit(struct REG_sim_load_pars   * const sim_load_pars,
                    struct REG_load_pars const * const load_pars,
                    cc_float                     const sim_load_tc_error,
                    cc_float                     const sim_period)
{
    // If Tc error is zero, simply copy load parameters into sim load parameters structure.

    if(sim_load_tc_error == 0.0F)
    {
        sim_load_pars->pars = *load_pars;
    }

    // else initialize simulated load with distorted load parameters to have required Tc error

    else
    {
        cc_float const sim_load_tc_factor = sim_load_tc_error / (sim_load_tc_error + 2.0F);

        regLoadInit(&sim_load_pars->pars,
                    load_pars->ohms_ser * (1.0F - sim_load_tc_factor),
                    load_pars->ohms_par * (1.0F - sim_load_tc_factor),
                    load_pars->ohms_mag * (1.0F - sim_load_tc_factor),
                    load_pars->henrys   * (1.0F + sim_load_tc_factor),
                    load_pars->gauss_per_amp);

        regLoadInitSat(&sim_load_pars->pars,
                       load_pars->sat.henrys * (1.0F + sim_load_tc_factor),
                       load_pars->sat.i_start,
                       load_pars->sat.i_end,
                       load_pars->sat.smoothing);
    }

    sim_load_pars->period               = sim_period;
    sim_load_pars->tc_error             = sim_load_tc_error;
    sim_load_pars->period_tc_ratio      = sim_period / sim_load_pars->pars.tc;
    sim_load_pars->is_load_undersampled = (sim_load_pars->period_tc_ratio > 3.0F);
}


// Real-Time Functions

cc_float regSimVsRT(struct REG_sim_vs_pars const * const pars,
                    struct REG_sim_vs_vars       * const vars,
                    cc_float                       const act)
{
    // Return immediately if the filter is under sampled

    if(pars->is_vs_undersampled == true)
    {
        return act * pars->gain;
    }

    // Save new act in the circular buffer

    uint32_t const buf_index0 = ++vars->buf_index & REG_SIM_NUM_DEN_LEN_MASK;

    vars->act[buf_index0] = act;

    // Calculate the new filter response

    uint32_t i;
    uint32_t buf_index = buf_index0;
    cc_float rsp = pars->num[0] * act;

    for(i = 1 ; i < REG_SIM_NUM_DEN_LEN ; i++)
    {
        buf_index = (buf_index - 1) & REG_SIM_NUM_DEN_LEN_MASK;

        rsp += pars->num[i] * vars->act[buf_index] - pars->den[i] * vars->rsp[buf_index];
    }

    rsp /= pars->den[0];

    // Save filter response in the circular buffer and return the value

    vars->rsp[buf_index0] = rsp;

    return rsp;
}



void regSimLoadRT(struct REG_sim_load_pars const * const pars,
                  struct REG_sim_load_vars       * const vars,
                  bool                             const is_vs_undersampled,
                  cc_float                         const v_circuit,
                  cc_float                         const i_mag_sat)
{
    // When load is not under-sampled the inductive transients are modeled with an integrator

    if(pars->is_load_undersampled == false)
    {
        cc_float const int_gain        = pars->period_tc_ratio / regLoadSatFactorRT(&pars->pars,i_mag_sat);
        cc_float const prev_integrator = vars->integrator;
        cc_float       increment;

        // If voltage source simulation is not under-sampled use first-order interpolation of voltage

        if(is_vs_undersampled == false)
        {
            increment = int_gain * (pars->pars.gain1 * 0.5F * (v_circuit + vars->circuit_voltage) - prev_integrator);
        }
        else // else when voltage source simulation is under-sampled use initial voltage for complete sample
        {
            increment = int_gain * (pars->pars.gain1 * vars->circuit_voltage - prev_integrator);
        }

        vars->integrator     += increment;
        vars->circuit_current = vars->integrator + pars->pars.gain0 * v_circuit;
        vars->magnet_current  = vars->integrator * pars->pars.ohms1;
    }
    else // else when load is under-sampled the inductive transients are ignored and ohms law is used
    {
        vars->circuit_current = v_circuit * pars->pars.gain2;
        vars->magnet_current  = vars->circuit_current * pars->pars.gain3;
    }

    // Remember load voltage for next iteration

    vars->circuit_voltage = v_circuit;
}

// EOF
