//! @file  regMeas.c
//! @brief Converter Control Regulation library measurement-related functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


//! Classical two-stage box-car FIR filter used by regMeasFilterRT() and regMeasFilterInit().
//!
//! @param[in,out] filter    Measurement filter parameters and values
//!
//! @returns       Value of reg_meas_filter::fir_accumulator adjusted by reg_meas_filter::integer_to_float factor

static cc_float regMeasFirFilterRT(struct REG_meas_filter * const filter);


// NON-RT   regMeasFilterInitBuffer

void regMeasFilterInitBuffer(struct REG_meas_filter * const filter,
                             int32_t                * const buf,
                             uint32_t                 const buf_len)
{
    filter->fir_buf[0] = buf;
    filter->buf_len    = buf_len;
}


// NON-RT   regMeasFilterInit

void regMeasFilterInit(struct REG_meas_filter * const filter,
                       uint32_t                 const fir_length[2],
                       uint32_t                       extrapolation_len_iters,
                       cc_float                 const pos,
                       cc_float                 const neg,
                       cc_float                 const meas_delay_iters)
{
    filter->state = REG_MEAS_FIR_INIT_STARTED;

    // Disable filter stages that are not used, or both stages if the buffer has not been specified

    if(fir_length[0] == 1 || filter->fir_buf[0] == NULL)
    {
        filter->fir_length[0] = 0;
    }
    else
    {
        filter->fir_length[0] = fir_length[0];
    }

    if(fir_length[1] == 1 || filter->fir_buf[0] == NULL)
    {
        filter->fir_length[1] = 0;
    }
    else
    {
        filter->fir_length[1] = fir_length[1];
    }

    // Sort filter stages to have the longest first

    if(filter->fir_length[0] < filter->fir_length[1])
    {
        uint32_t temp_fir_length = filter->fir_length[0];

        filter->fir_length[0] = filter->fir_length[1];
        filter->fir_length[1] = temp_fir_length;
    }

    // Clip filter lengths if they exceed the buffer space

    uint32_t buf_len = filter->buf_len;

    if(extrapolation_len_iters == 0)
    {
        extrapolation_len_iters = 1;
    }
    else if(extrapolation_len_iters > buf_len)
    {
        extrapolation_len_iters = buf_len;
    }

    buf_len -= extrapolation_len_iters;

    if(filter->fir_length[0] > buf_len)
    {
        filter->fir_length[0] = buf_len;
    }

    buf_len -= filter->fir_length[0];

    if(filter->fir_length[1] > buf_len)
    {
        filter->fir_length[1] = buf_len;
    }

    // Set the pointers to the second stage FIR buffer and extrapolation buffer

    filter->fir_buf[1] = filter->fir_buf[0] + filter->fir_length[0];

    cc_float * extrapolation_buf = (cc_float*)(filter->fir_buf[1] + filter->fir_length[1]);

    filter->extrapolation_buf       = extrapolation_buf;
    filter->extrapolation_len_iters = extrapolation_len_iters;

    // Set up FIR stages

    cc_float    filter_delay_iters;

    if(filter->fir_length[0] != 0 && pos > 0.0F)
    {
        // At least one FIR stage is active and the pos limit is active

        uint32_t total_fir_len = filter->fir_length[0] + filter->fir_length[1];

        // The FIR filters use integers for efficiency. We need to map floating point values to integer values,
        // optimizing resolution but allowing a reasonable over-range. This is 10% beyond the greater of
        // pos and -neg.

        filter->max_meas_value  = 1.1F * (pos > -neg ? pos : -neg);

        // Set filter delay - the delay of an FIR filter is O/2 where O is the order, and O = fir_length - 1

        filter_delay_iters = 0.5F * (cc_float)(total_fir_len - (filter->fir_length[1] > 0 ? 2 : 1));

        // Calculate float to integer scalings for FIR filter stages

        filter->float_to_integer = INT32_MAX / (filter->fir_length[0] * filter->max_meas_value);
        filter->integer_to_float = 1.0F / filter->float_to_integer;

        if(filter->fir_length[1] == 0)
        {
            filter->integer_to_float /= (cc_float)filter->fir_length[0];
        }
        else
        {
            filter->integer_to_float /= (cc_float)filter->fir_length[1];
        }
    }
    else
    {
        // Both FIR stages are disabled

        filter->signal[REG_MEAS_FILTERED] = filter->signal[REG_MEAS_UNFILTERED];
        filter_delay_iters = 0.0F;
    }

    // Set measurement delays

    filter->delay_iters[REG_MEAS_UNFILTERED]   = meas_delay_iters;
    filter->delay_iters[REG_MEAS_FILTERED]     = meas_delay_iters + filter_delay_iters;
    filter->delay_iters[REG_MEAS_EXTRAPOLATED] = 0.0F;

    // Calculate extrapolation factor so that it cancels the filtered measurement delay

    filter->extrapolation_factor = filter->delay_iters[REG_MEAS_FILTERED] / (cc_float)extrapolation_len_iters;

    // Reset the FIR and extrapolation buffers and FIR accumulators

    memset(filter->fir_buf[0], 0, filter->buf_len * sizeof(int32_t));

    // Signal that the initialization has finished

    filter->state = REG_MEAS_FIR_INIT_FINISHED;
}


// NON-RT   regMeasRateInit

void regMeasRateInit(struct REG_meas_filter * const filter, cc_float const initial_meas)
{
    uint32_t index;

    // Initialize rate measurement history buffer with initial measurement

    for(index = 0 ; index <= REG_MEAS_RATE_BUF_MASK ; index++)
    {
        filter->history_buf[index] = initial_meas;
    }

    filter->rate = 0.0F;
}



static cc_float regMeasFirFilterRT(struct REG_meas_filter * const filter)
{
    int32_t     input_integer;
    uint32_t    fir_index;
    cc_float    input_meas = filter->signal[REG_MEAS_UNFILTERED];

    // Clip unfiltered input measurement value to avoid crazy roll-overs in the integer stage

    if(input_meas > filter->max_meas_value)
    {
        input_meas = filter->max_meas_value;
    }
    else if(input_meas < -filter->max_meas_value)
    {
        input_meas = -filter->max_meas_value;
    }

    // Filter stage 1

    input_integer = (int32_t)(filter->float_to_integer * input_meas);
    fir_index     = ++filter->fir_index[0] % filter->fir_length[0];

    filter->fir_accumulator[0] += (input_integer - filter->fir_buf[0][fir_index]);

    filter->fir_buf[0][fir_index] = input_integer;
    filter->fir_index[0]          = fir_index;

    // Return immediately if second filter stage is not in use

    if(filter->fir_length[1] == 0)
    {
        return filter->integer_to_float * (cc_float)filter->fir_accumulator[0];
    }

    // Filter stage 2

    input_integer = filter->fir_accumulator[0] / (int32_t)filter->fir_length[0];
    fir_index     = ++filter->fir_index[1] % filter->fir_length[1];

    filter->fir_accumulator[1] += (input_integer - filter->fir_buf[1][fir_index]);

    filter->fir_buf[1][fir_index] = input_integer;
    filter->fir_index[1]          = fir_index;

    // Convert filter output back to floating point

    return filter->integer_to_float * (cc_float)filter->fir_accumulator[1];
}



void regMeasFilterRT(struct REG_meas_filter * const filter)
{
    if(filter->state == REG_MEAS_FIR_RUNNING)
    {
        // Filter is running

        if(filter->fir_length[0] > 0)
        {
            // At least one FIR stage is active

            filter->signal[REG_MEAS_FILTERED] = regMeasFirFilterRT(filter);
        }
        else
        {
            // Neither FIR stage is active

            filter->signal[REG_MEAS_FILTERED] = filter->signal[REG_MEAS_UNFILTERED];
        }

        // Extrapolate filtered signal using linear extrapolation through two points

        uint32_t extrapolation_index = ++filter->extrapolation_index % filter->extrapolation_len_iters;

        filter->signal[REG_MEAS_EXTRAPOLATED] = filter->signal[REG_MEAS_FILTERED] + filter->extrapolation_factor *
                                               (filter->signal[REG_MEAS_FILTERED] - filter->extrapolation_buf[extrapolation_index]);

        filter->extrapolation_buf[extrapolation_index] = filter->signal[REG_MEAS_FILTERED];
        filter->extrapolation_index = extrapolation_index;
    }
    else
    {
        // Filter is initializing or initialized

        filter->signal[REG_MEAS_EXTRAPOLATED] = filter->signal[REG_MEAS_FILTERED] = filter->signal[REG_MEAS_UNFILTERED];

        if(filter->state == REG_MEAS_FIR_INIT_FINISHED)
        {
            // Initialization has finished - reset FIR accumulators and current elements in the FIR and extrapolation buffers
            // This is because under a non-real-time OS (e.g. ccrt on Linux), the initialization could interrupt the call
            // to

            filter->extrapolation_buf[filter->extrapolation_index]  = 0;

            filter->fir_buf[0][filter->fir_index[0]] = 0;
            filter->fir_buf[1][filter->fir_index[1]] = 0;

            filter->fir_accumulator[0] = 0;
            filter->fir_accumulator[1] = 0;

            filter->state = REG_MEAS_FIR_RUNNING;
        }
    }
}



void regMeasRateRT(struct REG_meas_filter * const filter, cc_float const inv_period)
{
    // This is called at the regulation period for the signal (current or field)

    cc_float * const history_buf = filter->history_buf;     // Local pointer to history buffer for efficiency

    uint32_t const idx = filter->history_index = (filter->history_index + 1) & REG_MEAS_RATE_BUF_MASK;

    history_buf[idx] = filter->signal[REG_MEAS_FILTERED];

    // Estimate rate using linear regression through last four samples - see header file for derivation

    filter->rate = (2.0F / 20.0F) * inv_period * (3.0F * (history_buf[ idx ]                               -
                                                          history_buf[(idx - 3) & REG_MEAS_RATE_BUF_MASK]) +
                                                         (history_buf[(idx - 1) & REG_MEAS_RATE_BUF_MASK]  -
                                                          history_buf[(idx - 2) & REG_MEAS_RATE_BUF_MASK]));
}



void regMeasSignalInvalidCounterRT(struct REG_meas_fault * const meas_fault, bool const meas_is_valid, bool const allow_fault, bool const check_invalid_seq)
{
    if(allow_fault == false)
    {
        // Reporting a fault is not allowed, so reset fault flag

        meas_fault->flag = false;
    }
    else if(check_invalid_seq == true)
    {
        // Reporting a fault is allowed and the invalid sequence length can be checked on this iteration

        if(meas_fault->flag == true)
        {
            // Fault flag is currently set

            if(meas_is_valid == true)
            {
                // Signal is valid so check if the number of consecutive valid measurements is sufficient to remove the fault

                if(++meas_fault->seq_counter >= REG_MEAS_MAX_INVALID_MEAS)
                {
                    meas_fault->flag = false;
                }
            }
            else
            {
                // Signal is still invalid so reset sequence counter

                meas_fault->seq_counter = 0;
                meas_fault->invalid_counter++;
            }
        }
        else
        {
            // Fault flag is not currently set

            if(meas_is_valid == false)
            {
                meas_fault->invalid_counter++;

                // Signal is invalid so check if the number of consecutive invalid measurements has hit the limit

                if(++meas_fault->seq_counter >= REG_MEAS_MAX_INVALID_MEAS)
                {
                    meas_fault->flag = true;
                }
            }
            else
            {
                // Signal is valid so reset sequence counter

                meas_fault->seq_counter = 0;
            }
        }
    }
}

// EOF
