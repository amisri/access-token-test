//! @file  regDeco.c
//! @brief Converter Control Regulation library symmetric decoupling functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// NON-RT   regDecoInit

void regDecoInit(struct REG_deco                 * const deco,
                 struct REG_deco_shared volatile * const shared,
                 cc_float                        * const k,
                 cc_float                        * const d)
{
    // Save the pointers in the deco structure

    deco->shared = shared;

    deco->k = k;
    deco->d = d;
}


// NON-RT   regDecoPars

void regDecoPars(struct REG_deco * const deco,
                 uint32_t                phase,
                 uint32_t                index,
                 uint32_t          const load_select,
                 uint32_t          const b_period_iters,
                 uint32_t          const i_period_iters,
                 bool              const b_reg_enabled,
                 bool              const i_reg_enabled)
{
    // Disable decoupling if shared data was defined, load_select is not the nominal load (0) or the phase or index is invalid

    if(deco->shared != NULL)
    {
        if(   load_select != 0
           || phase == 0
           || (b_reg_enabled == true && phase >= b_period_iters)
           || (i_reg_enabled == true && phase >= i_period_iters)
           || index >= REG_NUM_DECO)
        {
            // Setting phase to zero deactivates the decoupling algorithm

            phase = 0;
        }

        // Force the index to be in the valid range

        if(index >= REG_NUM_DECO)
        {
            index = 0;
        }

        // Save phase and index in the deco structure. The phase is saved initially in new_phase.
        // This will be transfered to deco->phase at the start of the next regulation period in regMgrRegRT().
        // This is essential to ensure that the pre- and post-decoupling regulation processing are
        // always executed in the right order, otherwise Inf are produced.

        deco->new_phase = phase;
        deco->index     = index;

        // Save the state and index in the shared structure

        deco->shared->index = index;
        deco->shared->state = phase > 0;
    }
}



cc_float regDecoRT(struct REG_deco * deco)
{
    cc_float v_ref_deco;

    if(deco->phase == 0)
    {
        // Decoupling is disabled so return the saved v_ref without compensation

        v_ref_deco = deco->v_ref;
    }
    else
    {
        // Decoupling is enabled so calculate the one row of the decoupling matrix for this FGC

        cc_float * d      = deco->d;
        cc_float * k      = deco->k;
        cc_float volatile * v_ref  = deco->shared->v_ref;
        cc_float volatile * i_meas = deco->shared->i_meas;
        uint32_t   i;

        v_ref_deco = 0.0F;

        for(i = 0 ; i < REG_NUM_DECO ; i++)
        {
            v_ref_deco += *(k++) * *(i_meas++) + *(d++) * *(v_ref++);
        }
    }

    return v_ref_deco;
}

// EOF
