//! @file  regRst.c
//! @brief Converter Control Regulation library RST regulation algorithm functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"

// Constants

#define FLOAT_THRESHOLD            1E-7F                        //!< Lower bound for s[0]. To allow for floating point rounding errors.
#define REG_AVE_V_REF_LEN          4                            //!< Number of iterations over which to average V_REF
#define REG_MM_STEPS               20                           //!< Number of steps to cover modulus margin scan

// Macros

#define MINIMUM(A,B)               (A<B?A:B)
#define REG_MM_FREQ(index)         (0.1F + (9.9F / (REG_MM_STEPS*REG_MM_STEPS*REG_MM_STEPS)) * (cc_float)(index*index*index))

// Typedef for complex numbers

typedef struct complex
{
    cc_double   real;
    cc_double   imag;
} complex;


// NON-RT    regJurysTest

static bool regJurysTest(cc_float coeffs[REG_NUM_RST_COEFFS])
{
    // Skip trailing zero coefficients - note that the first coefficient cannot be zero because previous check

    int32_t n = 1;

    while(n < REG_NUM_RST_COEFFS && coeffs[n] != 0.0F)
    {
        n++;
    }

    n--;

    // Convert coefficients to double in b[] and sum even and odd coefficients separately

    int32_t     i;
    cc_double   b[REG_NUM_RST_COEFFS];
    cc_double   sum_even;
    cc_double   sum_odd;
    cc_double   sum_abs;

    for(i = 0, sum_abs = sum_odd = sum_even = 0.0F ; i <= n ; i++)
    {
        b[i] = coeffs[i];

        sum_abs += fabs(b[i]);

        if((i & 1) == 0)
        {
            sum_even += b[i];
        }
        else
        {
            sum_odd += b[i];
        }
    }

    // Stability check 1 : Sum(even coeffs) >= Sum(odd coeffs)
    // Stability check 2 : Sum(coeffs) > 0 - allow for floating point rounding errors

    if(   sum_even < sum_odd
       || ((sum_even + sum_odd) / sum_abs) < -FLOAT_THRESHOLD)
    {
        return false;
    }

    // Stability check 3 : Jury's Stability Test for unstable roots

    cc_double a[REG_NUM_RST_COEFFS];

    while(n > 2)
    {
        for(i = 0 ; i <= n ; i++)
        {
            a[i] = b[i];
        }

        cc_double const d = a[n] / a[0];

        for(i = 0 ; i < n ; i++)
        {
            b[i] = a[i] - d * a[n - i];
        }

        // First element of every row of Jury's array > 0 for stability

        if(b[0] <= 0.0F)
        {
            return false;
        }

        n--;
    }

    // Return true because coeffs are stable

    return true;
}


// NON-RT    regRstTest

static enum REG_status regRstTest(struct REG_rst_pars * const pars)
{
    // RST check 1 : r[0] != 0 to prevent divide by zero

    if(fabsf(pars->rst.r[0]) < FLOAT_THRESHOLD)
    {
        return REG_FAULT_R0_IS_ZERO;
    }

    // RST check 2 : s[0] > 0 for stability

    if(pars->rst.s[0] < FLOAT_THRESHOLD)
    {
        return REG_FAULT_S0_NOT_POSITIVE;
    }

    // RST check 3 : t[0] > 0 to prevent divide by zero

    if(pars->rst.t[0] < FLOAT_THRESHOLD)
    {
        return REG_FAULT_T0_NOT_POSITIVE;
    }

    // Check that S is stable

    if(regJurysTest(pars->rst.s) == false)
    {
        return REG_FAULT_S_HAS_UNSTABLE_ROOT;
    }

    // Check that T is stable

    if(regJurysTest(pars->rst.t) == false)
    {
        return REG_FAULT_T_HAS_UNSTABLE_ROOT;
    }

    return REG_OK;
}


// NON-RT    regAbsComplexRatio

static cc_float regAbsComplexRatio(cc_float const * const num,
                                   cc_float const * const den,
                                   cc_float         const k)
{
    int32_t     idx;
    cc_double   cosine;
    cc_double   sine;
    cc_double   w;
    complex     num_exp = { 0.0, 0.0 };
    complex     den_exp = { 0.0, 0.0 };

    for(idx = 0 ; idx < REG_NUM_RST_COEFFS; idx++)
    {
        w      = M_TWO_PI * (cc_double)(idx + 1) * k;
        cosine = cos(w);
        sine   = sin(w);

        num_exp.real += num[idx] * cosine;
        num_exp.imag -= num[idx] * sine;

        den_exp.real += den[idx] * cosine;
        den_exp.imag -= den[idx] * sine;
    }

    return sqrt(num_exp.real * num_exp.real + num_exp.imag * num_exp.imag) /
           sqrt(den_exp.real * den_exp.real + den_exp.imag * den_exp.imag);
}


// NON-RT    regModulusMargin

static void regModulusMargin(struct REG_rst_pars * const pars)
{
    // For algorithm 1 (true dead-beat 1 period), the modulus margin comes at the Nyquist

    if(pars->alg_index == 1)
    {
        pars->modulus_margin    = regAbsComplexRatio(pars->asbr, pars->as, 0.5F);
        pars->modulus_margin_hz = 0.5F / pars->reg_period;

        return;
    }

    // For algorithms 2-5, scan for minimum abs_S_p_y (this is the modulus margin)

    // Limit scan frequency range from 0.1 x min_auxpole_hz to 10 x min_auxpole_hz.
    // Frequency steps size follows a cubic because abs_S_p_y changes more quickly at lower
    // frequencies. Start the scan in the middle of the range and stop scan at the minimum
    // abs_S_p_y or the Nyquist.

    // Start in the middle of the linearized range

    int32_t frequency_index = REG_MM_STEPS / 2;

    // 1 = regulation frequency, 0.5 = Nyquist

    cc_float frequency_fraction = pars->min_auxpole_hz * pars->reg_period * REG_MM_FREQ(frequency_index);
    cc_float frequency_fraction_for_min_abs_S_p_y = frequency_fraction;

    // If this frequency is over the Nyquist, set the modulus margin to -1 to signal the issue.

    if(frequency_fraction > 0.5F)
    {
        pars->modulus_margin = -1.0F;
        return;
    }

    // Evaluate abs_S_p_y on cons

    pars->modulus_margin = regAbsComplexRatio(pars->asbr, pars->as, frequency_fraction);
    frequency_index--;
    frequency_fraction = pars->min_auxpole_hz * pars->reg_period * REG_MM_FREQ(frequency_index);

    cc_float abs_S_p_y = regAbsComplexRatio(pars->asbr, pars->as, frequency_fraction); // Current value of the sensitivity function
    int32_t  frequency_index_step;                                                     // +/-1

    if(abs_S_p_y < pars->modulus_margin)
    {
        frequency_index_step = -1;
    }
    else
    {
        abs_S_p_y            = pars->modulus_margin;
        frequency_fraction   = frequency_fraction_for_min_abs_S_p_y;
        frequency_index_step = 1;
        frequency_index++;
    }

    frequency_index += frequency_index_step;

    do
    {
        pars->modulus_margin = abs_S_p_y;
        frequency_fraction_for_min_abs_S_p_y = frequency_fraction;
        frequency_fraction = pars->min_auxpole_hz * pars->reg_period * REG_MM_FREQ(frequency_index);
        abs_S_p_y = regAbsComplexRatio(pars->asbr, pars->as, frequency_fraction);
        frequency_index += frequency_index_step;

    } while(   frequency_index >= 0
            && frequency_index <= REG_MM_STEPS
            && frequency_fraction < 0.5F
            && abs_S_p_y < pars->modulus_margin);

    pars->modulus_margin_hz = frequency_fraction_for_min_abs_S_p_y / pars->reg_period;
}


// NON-RT    regVectorMultiply

static cc_float regVectorMultiply(cc_float const * const p,
                                  cc_float const * const m,
                                  int32_t          const p_order,
                                  int32_t                m_idx)
{
    int32_t   p_idx;
    cc_double product = 0.0;

    for(p_idx = 0 ; m_idx >= 0 && p_idx <= p_order; m_idx--, p_idx++)
    {
        product += (cc_double)p[p_idx] * (cc_double)m[m_idx];
    }

    return product;
}


// NON-RT    regRstInitDeadbeat
//
// This function prepares coefficients for the RST regulation algorithm. It chooses the algorithm to use
// based on pars->pure_delay_iters. It is divided into 5 ranges, three result in true dead-beat
// controllers and two in pseudo-dead-beat controllers. To work well the bandwidth of the voltage source and
// FIR filter notches must be at least ten times the frequency of the auxiliary poles because they
// not included in the load model.
//
// It can be used with slow inductive circuits as well as fast or even resistive loads. For fast loads,
// auxpole1_hz is set to 1.0E+5 and z to 0.8. Normally auxpole1_hz should equal auxpoles2_hz and z should be 0.5.
//
// The pars->pure_delay_periods parameter informs the algorithm of the pure delay around the loop. This can be
// a dynamic behavior (i.e. of the voltage source or filters) crudely modeled as a pure delay for low
// frequencies, or it can be real pure delay due to computation or communications delays.
//
// Support for pure delays above 40% of the period is restricted to loads without a significant parallel resistor
// (i.e. one pole and no zero).
//
// The calculation of the RST coefficients requires double precision floating point, even though the
// coefficients themselves are stored as single precision.

static enum REG_status regRstInitDeadbeat(struct REG_rst_pars        * const pars,
                                     struct REG_load_pars const * const load,
                                     cc_float                     const auxpole1_hz,
                                     cc_float                     const auxpoles2_hz,
                                     cc_float                     const auxpoles2_z,
                                     cc_float                           auxpole4_hz,
                                     cc_float                           auxpole5_hz)
{
    // Calculate a2 = 1 - exp(t1) using Maclaurin series if t1 is small

    cc_double const t1 = -pars->reg_period / load->tc;
    cc_double const a1 = -exp(t1);

    cc_double a2;    // a2 = 1 + a1 = 1 - exp(t1) - use Maclaurin expansion for small t1

    if(a1 > -0.99)              // if t1 > 0.01
    {
        a2 = 1.0 + a1;                  // it is okay to use 1 - exp(t1)
    }
    else                        // else use Maclaurin series for exp(t1) = 1 + t1 + t1^2/2! + ...
    {
        a2 = -(t1 * (1.0 + 0.5 * t1));  // This is more precise for small t1
    }

    cc_double b0_b1 = load->gain1 * a2;

    // Identify minimum frequency of an auxiliary pole

    pars->min_auxpole_hz = MINIMUM(auxpole1_hz, auxpoles2_hz);

    // If auxpole_4 or auxpole_5 are not specified, then default to using auxpole_1

    auxpole4_hz = auxpole4_hz > 0.0F ? auxpole4_hz : auxpole1_hz;
    auxpole5_hz = auxpole5_hz > 0.0F ? auxpole5_hz : auxpole1_hz;

    // Apply gain to b0 and b1 if regulating field

    if(pars->reg_mode == REG_FIELD)
    {
        // If the circuit has a significant (i.e. low) parallel resistor, or if
        // the magnet has large eddy currents or inter-turn capacitance
        // then the field regulator may be unstable. But libreg will
        // allow the user to try it out.

        b0_b1 *= load->gauss_per_amp;
    }

    // Select algorithm based on the pure delay in regulation periods

    if(pars->pure_delay_periods < 0.401F)
    {
        pars->alg_index = 1;
    }
    else if(load->significant_ohms_par == true)
    {
        // If pure delay >= 0.401 and parallel resistance is significant, then no solution is possible

        return REG_FAULT_OHMS_PAR_TOO_SMALL;
    }
    else if(pars->pure_delay_periods < 1.0F)
    {
        pars->alg_index = 2;
    }
    else if(pars->pure_delay_periods < 1.401F)
    {
        pars->alg_index = 3;
    }
    else if (pars->pure_delay_periods < 2.00F)
    {
        pars->alg_index = 4;
    }
    else if (pars->pure_delay_periods < 2.401F)
    {
        pars->alg_index = 5;
    }
    else
    {
        // pure delay >= 2.401 so no RST regulator can be synthesized internally

        return REG_FAULT_PURE_DELAY_TOO_LARGE;
    }

    // Calculate intermediate values

    cc_double const c1 = -exp(-pars->reg_period * M_TWO_PI * auxpole1_hz);
    cc_double       q1 = -exp(-pars->reg_period * M_TWO_PI * auxpoles2_hz * auxpoles2_z);
    cc_double const d1 = 2.0 * q1 * cos(pars->reg_period * M_TWO_PI * auxpoles2_hz * sqrt(1.0 - auxpoles2_z * auxpoles2_z));
    cc_double const d2 = q1 * q1;

    cc_double c2;
    cc_double c3;
    cc_double q2;

    cc_double b0 = 0.0;
    cc_double b1 = 0.0;

    int32_t s_idx = 0;
    int32_t r_idx = 0;

    // Calculate RST coefficients

    switch(pars->alg_index)
    {
    case 1:                 // Algorithm 1 : Pure delay fraction 0 .. 0.401 : dead-beat (1)

        b0 = (load->gain0      + load->gain1 * a2 * (1.0 - pars->pure_delay_periods));
        b1 = (load->gain0 * a1 + load->gain1 * a2 * pars->pure_delay_periods);

        if(pars->reg_mode == REG_FIELD)
        {
            b0 *= load->gauss_per_amp;
            b1 *= load->gauss_per_amp;
        }

        pars->rst.r[0] = c1 + d1 - a1 + 2.0;
        pars->rst.r[1] = c1*d1 + d2 + 2.0*a1 - 1.0;
        pars->rst.r[2] = c1*d2 - a1;

        pars->rst.s[0] = b0;
        pars->rst.s[1] = b1 - 2.0*b0;
        pars->rst.s[2] = b0 - 2.0*b1;
        pars->rst.s[3] = b1;

        pars->rst.t[0] = 1.0F;
        pars->rst.t[1] = c1 + d1;
        pars->rst.t[2] = c1*d1 + d2;
        pars->rst.t[3] = c1*d2;

        pars->dead_beat = 1;
        r_idx = 4;
        s_idx = 5;
        break;

    case 2:                 // Algorithm 2 : Pure delay fraction 0.401 .. ~0.9 : pseudo dead-beat (1 + pure delay)

        b0 = b0_b1 * (1.0 - pars->pure_delay_periods);
        b1 = b0_b1 * pars->pure_delay_periods;

        q2 = (b0*b0*b1 + 2*b0*b1*b1 + b1*b1*b1)/(b0*b0*b0 + 2*b0*b0*b1 + b0*b1*b1)
           - (b1*(b1 - b0*c1)*(d2*b0*b0 - d1*b0*b1 + b1*b1))/(b0*b0_b1*b0_b1*(b1 - a1*b0));

        // Use algorithm 2 if controller will be stable

        if(q2 < 1.0)
        {
            pars->rst.s[0] = 1.0F;
            pars->rst.s[1] = q2 - 2;
            pars->rst.s[2] = 1 - 2 * q2;
            pars->rst.s[3] = q2;

            pars->rst.r[0] = (3*a1 + c1 + d1 + 2*a1*c1 + 2*a1*d1 + a1*d2 - c1*d2 + a1*c1*d1 + 2)/(b0_b1*(a1 + 1)*(a1 + 1))
                           + (b1*(c1 + 1)*(d1 + d2 + 1))/(b0_b1*b0_b1*(a1 + 1)) + (a1*(a1 - c1)*(a1*a1 - d1*a1 + d2))/((a1 + 1)*(a1 + 1)*(b1 - a1*b0));

            pars->rst.r[1] = (d2 + c1*d1 + 2*c1*d2 + 2*a1*a1*c1 + 2*a1*a1*d1 + a1*a1*d2 + 3*a1*a1 + a1*a1*c1*d1 - 1)/(b0_b1*(a1 + 1)*(a1 + 1))
                           - (2*a1*(a1 - c1)*(a1*a1 - d1*a1 + d2))/((a1 + 1)*(a1 + 1)*(b1 - a1*b0)) + (b1*(a1 - 1)*(c1 + 1)*(d1 + d2 + 1))/(b0_b1*b0_b1*(a1 + 1));

            pars->rst.r[2] = (a1*(a1 - c1*d2)*b0*b0 + a1*(2*a1 + d2 + c1*d1 - 1)*b0*b1 - a1*(c1 - a1 + d1 + 2)*b1*b1)/(b0_b1*b0_b1*(b1 - a1*b0));

            pars->rst.t[0] = 1.0 / b0_b1;
            pars->rst.t[1] = (c1 + d1) / b0_b1;
            pars->rst.t[2] = (c1*d1 + d2) / b0_b1;
            pars->rst.t[3] = c1*d2 / b0_b1;

            pars->dead_beat = 0;
            r_idx = 4;
            s_idx = 5;
            break;
        }

        // S will be unstable so fall through to use Algorithm 3...

        pars->alg_index = 3;

        /* no break */

    case 3:                 // Algorithm 3 : Pure delay fraction ~0.9 .. 1.401 : dead-beat (2)

        b0 = b0_b1 * (2.0 - pars->pure_delay_periods);
        b1 = b0_b1 * (pars->pure_delay_periods - 1.0);

        c2 = -exp(-pars->reg_period * M_TWO_PI * auxpole4_hz);
        q1 = 2.0 - a1 + c1 + c2 + d1;

        pars->rst.r[0] = q1*(2.0 - a1) + d2 + c1*c2 + d1*(c1 + c2) + 2.0*a1 - 1.0;
        pars->rst.r[1] = q1*(2.0*a1 - 1.0)  + c1*c2*d1 + d2*(c1 + c2) - a1;
        pars->rst.r[2] = c1*c2*d2 - a1*q1;

        pars->rst.s[0] = b0;
        pars->rst.s[1] = b0*(q1 - 2.0) + b1;
        pars->rst.s[2] = b1*(q1 - 2.0) - b0*(2.0*q1 - 1.0);
        pars->rst.s[3] = b0*q1         - b1*(2.0*q1 - 1.0);
        pars->rst.s[4] = b1*q1;

        pars->rst.t[0] = 1.0F;
        pars->rst.t[1] = c1 + c2 + d1;
        pars->rst.t[2] = c1*c2 + d1*(c1 + c2) + d2;
        pars->rst.t[3] = c1*c2*d1 + d2*(c1 + c2);
        pars->rst.t[4] = c1*c2*d2;

        pars->dead_beat = 2;
        pars->min_auxpole_hz = MINIMUM(pars->min_auxpole_hz, auxpole4_hz);
        r_idx = 5;
        s_idx = 7;
        break;

    case 4:                 // Algorithm 4 : Pure delay fraction 1.401 .. ~1.9 : pseudo dead-beat (2 + pure delay)

        b0 = b0_b1 * (2.0 - pars->pure_delay_periods);
        b1 = b0_b1 * (pars->pure_delay_periods - 1.0);

        c2 = -exp(-pars->reg_period * M_TWO_PI * auxpole4_hz);

        q2 = (b1*(c1 + c2 + d1 - c1*d2 - c2*d2 - c1*c2*d1 - 2*c1*c2*d2 + 2) + a1*b1*(2*c1 + 2*c2 + 2*d1 + d2 + c1*c2 + c1*d1 + c2*d1 - c1*c2*d2 + 3))/(b0_b1*(a1 + 1)*(a1 + 1))
           + (b1*b1*(c1 + 1)*(c2 + 1)*(d1 + d2 + 1))/(b0_b1*b0_b1*(a1 + 1))
           + (b1*(a1 - c1)*(a1 - c2)*(a1*a1 - d1*a1 + d2))/((a1 + 1)*(a1 + 1)*(b1 - a1*b0));

        // Use algorithm 4 if controller will be stable

        if(q2 < 1.0)
        {
            q1 = 2.0 - a1 + c1 + c2 + d1;

            pars->rst.r[0] = (4*a1 + 2*c1 + 2*c2 + 2*d1 + d2 + 3*a1*c1 + 3*a1*c2 + 3*a1*d1 + 2*a1*d2 + c1*c2 + c1*d1 + c2*d1
                           +  2*a1*c1*c2 + 2*a1*c1*d1 + a1*c1*d2 + 2*a1*c2*d1 + a1*c2*d2 - c1*c2*d2 + a1*c1*c2*d1 + 3)/(b0_b1*(a1 + 1)*(a1 + 1))
                           +  (b1*(c1 + 1)*(c2 + 1)*(d1 + d2 + 1))/(b0_b1*b0_b1*(a1 + 1))
                           -  (a1*(a1 - c1)*(a1 - c2)*(a1*a1 - d1*a1 + d2))/((a1 + 1)*(a1 + 1)*(b1 - a1*b0));

            pars->rst.r[1] = (c1*d2 - c2 - d1 - c1 + c2*d2 + 3*a1*a1*c1 + 3*a1*a1*c2 + 3*a1*a1*d1 + 2*a1*a1*d2 + 4*a1*a1 + c1*c2*d1 + 2*c1*c2*d2
                           +  2*a1*a1*c1*c2 + 2*a1*a1*c1*d1 + a1*a1*c1*d2 + 2*a1*a1*c2*d1 + a1*a1*c2*d2 + a1*a1*c1*c2*d1 - 2)/(b0_b1*(a1 + 1)*(a1 + 1))
                           +  (2*a1*(a1 - c1)*(a1 - c2)*(a1*a1 - d1*a1 + d2))/((a1 + 1)*(a1 + 1)*(b1 - a1*b0))
                           +  (b1*(a1 - 1)*(c1 + 1)*(c2 + 1)*(d1 + d2 + 1))/(b0_b1*b0_b1*(a1 + 1));

            pars->rst.r[2] = (a1*(2*a1 + a1*c1 + a1*c2 + a1*d1 - a1*a1 - c1*c2*d2)*b0*b0
                           +  a1*(4*a1 - c1 - c2 - d1 + 2*a1*c1 + 2*a1*c2 + 2*a1*d1 + c1*d2 + c2*d2 - 2*a1*a1 + c1*c2*d1 - 2)*b0*b1
                           -  a1*(2*c1 - 2*a1 + 2*c2 + 2*d1 + d2 - a1*c1 - a1*c2 - a1*d1 + c1*c2 + c1*d1 + c2*d1 + a1*a1 + 3)*b1*b1)/(b0_b1*b0_b1*(b1 - a1*b0));

            pars->rst.s[0] = 1.0F;
            pars->rst.s[1] = q1 - 2.0;
            pars->rst.s[2] = q2 - 2.0*q1 + 1.0;
            pars->rst.s[3] = q1 - 2.0*q2;
            pars->rst.s[4] = q2;

            pars->rst.t[0] = 1.0 / b0_b1;
            pars->rst.t[1] = (c1 + c2 + d1) / b0_b1;
            pars->rst.t[2] = (d2 + c1*c2 + c1*d1 + c2*d1) / b0_b1;
            pars->rst.t[3] = (c1*d2 + c2*d2 + c1*c2*d1) / b0_b1;
            pars->rst.t[4] = c1*c2*d2 / b0_b1;

            pars->dead_beat = 0;
            pars->min_auxpole_hz = MINIMUM(pars->min_auxpole_hz, auxpole4_hz);
            r_idx = 5;
            s_idx = 7;
            break;
        }

        // S will be unstable so fall through to use Algorithm 5...

        pars->alg_index = 5;

        /* no break */

    case 5:                 // Algorithm 5 : Pure delay fraction ~0.9 .. 2.401 : dead-beat (3)

        b0 = b0_b1 * (3.0 - pars->pure_delay_periods);
        b1 = b0_b1 * (pars->pure_delay_periods - 2.0);

        c2 = -exp(-pars->reg_period * M_TWO_PI * auxpole4_hz);
        c3 = -exp(-pars->reg_period * M_TWO_PI * auxpole5_hz);
        q1 = 2.0 - a1 + c1 + c2 + c3 + d1;
        q2 = (2.0 - a1)*q1 + 2.0*a1 - 1 + d2 + c1*c2 + c1*c3 + c2*c3 + c1*d1 + c2*d1 + c3*d1;

        pars->rst.r[0] = -a1 +(2*a1 - 1)*q1 + (2 - a1)*q2 + c1*d2 + c2*d2 + c3*d2 + c1*c2*c3 + c1*c2*d1 + c1*c3*d1 + c2*c3*d1;
        pars->rst.r[1] = (2*a1 - 1)*q2 - a1*q1 + c1*c2*d2 + c1*c3*d2 + c2*c3*d2 + c1*c2*c3*d1;
        pars->rst.r[2] = -a1*q2 + c1*c2*c3*d2;

        pars->rst.s[0] = b0;
        pars->rst.s[1] = b0*(q1 - 2.0)  + b1;
        pars->rst.s[2] = b1*(q1 - 2.0)  - b0*(2.0*q1 - q2 - 1.0);
        pars->rst.s[3] = b0*(q1 -2.0*q2)- b1*(2.0*q1 - q2 - 1.0);
        pars->rst.s[4] = b0*q2 + b1*(q1 - 2.0*q2);
        pars->rst.s[5] = b1*q2;

        pars->rst.t[0] = 1.0F;
        pars->rst.t[1] = c1 + c2 + c3 + d1;
        pars->rst.t[2] = d2 + c1*c2 + c1*c3 + c2*c3 + c1*d1 + c2*d1 + c3*d1;
        pars->rst.t[3] = c1*d2 + c2*d2 + c3*d2 + c1*c2*c3 + c1*c2*d1 + c1*c3*d1 + c2*c3*d1;
        pars->rst.t[4] = c1*c2*d2 + c1*c3*d2 + c2*c3*d2 + c1*c2*c3*d1;
        pars->rst.t[5] = c1*c2*c3*d2;

        pars->dead_beat = 3;
        pars->min_auxpole_hz = MINIMUM(pars->min_auxpole_hz, auxpole4_hz);
        pars->min_auxpole_hz = MINIMUM(pars->min_auxpole_hz, auxpole5_hz);
        r_idx = 6;
        s_idx = 9;
        break;
    }

    // Save plant coefficients

    pars->b[0] = b0;
    pars->b[1] = b1;

    pars->a[0] = 1.0F;
    pars->a[1] = a1;

    // Calculate A.S and A.S + B.R to allow Modulus Margin to be calculated later

    uint32_t idx;

    for(idx = REG_NUM_RST_COEFFS-1 ; idx > s_idx ; idx --)
    {
        pars->as  [idx] = 0.0F;
        pars->asbr[idx] = 0.0F;
    }

    for(idx = s_idx ; s_idx >= 0 || r_idx >= 0 ; s_idx--, r_idx--, idx--)
    {
        pars->as  [idx] = regVectorMultiply(pars->a, pars->rst.s, 1, s_idx);
        pars->asbr[idx] = regVectorMultiply(pars->b, pars->rst.r, 1, r_idx) + pars->as[idx];
    }

    return REG_OK;
}


// NON-RT    regRstInitPI
//
// This function prepares coefficients for the RST regulation algorithm to implement a proportional-integral
// controller. It can be used with fast slightly inductive circuits.

static void regRstInitPI(struct REG_rst_pars        * const pars,
                         struct REG_load_pars const * const load,
                         cc_float                     const auxpole1_hz)
{
    cc_float const a1 = -expf(-pars->reg_period * (load->ohms_ser + load->ohms_mag) * load->inv_henrys);
    cc_float       b1 = (1.0F + a1) / (load->ohms_ser + load->ohms_mag);
    cc_float const c1 = -expf(-pars->reg_period * M_TWO_PI * auxpole1_hz);

    pars->alg_index = 10;

    if(pars->reg_mode == REG_FIELD)
    {
        b1 *= load->gauss_per_amp;
    }

    pars->rst.r[0] = 1.0F + c1;
    pars->rst.r[1] = a1 * pars->rst.r[0];

    pars->rst.s[0] =  b1;
    pars->rst.s[1] = -b1;

    pars->rst.t[0] = pars->rst.r[0];
    pars->rst.t[1] = pars->rst.r[1];
}


// NON-RT    regRstInitI
//
// This function prepares coefficients for the RST regulation algorithm to implement an integrator only.
// This can be used with resistive circuits.

static void regRstInitI(struct REG_rst_pars        * const pars,
                        struct REG_load_pars const * const load,
                        cc_float                     const auxpole1_hz)
{
    cc_float const c1 = -expf(-M_TWO_PI * pars->reg_period * auxpole1_hz);
    cc_float       b1 = 1.0F / load->ohms_ser;

    pars->alg_index = 20;

    if(pars->reg_mode == REG_FIELD)
    {
        b1 *= load->gauss_per_amp;
    }

    pars->rst.r[0] = 1.0F + c1;

    pars->rst.s[0] =  b1;
    pars->rst.s[1] = -b1;

    pars->rst.t[0] = 1.0F + c1;
}


// NON-RT    regRstInitOpenLoop
//
// Calculate coefficients for open loop reference calculation based on forward Euler method

static void regRstInitOpenLoop(struct REG_rst_pars        * const pars,
                               struct REG_load_pars const * const load)
{
    cc_double const s1 = -load->henrys / (pars->reg_period * load->ohms_par);
    cc_double const t1 = -load->henrys / pars->reg_period * (1.0 + load->ohms_ser / load->ohms_par);
    cc_double const s0 =  load->ohms_mag / load->ohms_par +  1.0 - s1;
    cc_double const t0 =  load->ohms_mag + load->ohms_ser * (1.0 + load->ohms_mag/load->ohms_par) - t1;

    pars->openloop_forward.ref[0] =  t0/s0;
    pars->openloop_forward.ref[1] =  t1/s0;
    pars->openloop_forward.act[1] = -s1/s0;

    pars->openloop_reverse.ref[1] = -t1/t0;
    pars->openloop_reverse.act[0] =  s0/t0;
    pars->openloop_reverse.act[1] =  s1/t0;

   // If the regulation mode is CURRENT, we can use the reference current value as provided.
   // If it is FIELD, the current is calculated as I = B/G where G is the field-to-current
   // ratio for the magnet. This disregards magnet saturation because open loop is only needed
   // close to zero current when the magnet shouldn't be saturated.

    if(pars->reg_mode == REG_FIELD)
    {
        pars->openloop_forward.ref[0] /= load->gauss_per_amp;
        pars->openloop_forward.ref[1] /= load->gauss_per_amp;
        pars->openloop_reverse.act[0] *= load->gauss_per_amp;
        pars->openloop_reverse.act[1] *= load->gauss_per_amp;
    }
}


// NON-RT    regRstInit

enum REG_status regRstInit(struct REG_rst_pars        * const pars,
                           enum REG_mode                const reg_mode,
                           uint32_t                     const reg_period_iters,
                           cc_float                     const reg_period,
                           struct REG_load_pars const * const load,
                           enum CC_enabled_disabled     const external_alg,
                           cc_float                     const int_pure_delay_periods,
                           cc_float                     const int_auxpole1_hz,
                           cc_float                     const int_auxpoles2_hz,
                           cc_float                     const int_auxpoles2_z,
                           cc_float                     const int_auxpole4_hz,
                           cc_float                     const int_auxpole5_hz,
                           cc_float                     const ext_track_delay_periods,
                           struct REG_rst       const * const ext_rst)
{
    // Clear pars structure

    memset(pars, 0, sizeof(*pars));

    // Set default parameters

    pars->status               = REG_OK;
    pars->reg_mode             = reg_mode;
    pars->inv_reg_period_iters = 1.0F / (cc_float)reg_period_iters;
    pars->reg_period           = reg_period;
    pars->inv_reg_period       = 1.0F / reg_period;
    pars->pure_delay_periods   = 0.0F;
    pars->track_delay_periods  = 1.0F;
    pars->modulus_margin       = 0.0F;

    // Reset R, S, T, A, B and ASBR coefficients

    uint32_t i;

    for(i=0 ; i < REG_NUM_RST_COEFFS ; i++)
    {
        pars->rst.r[i] = pars->rst.s[i] = pars->rst.t[i] = pars->a[i] = pars->b[i] = pars->asbr[i] = 0.0F;
    }

    if(external_alg == CC_DISABLED)
    {
        // Use an internal synthesis algorithm to calculate RST coefficients - the algorithm depends on AUXPOLES2_HZ and LOAD_HENRYS

        if(int_auxpoles2_hz > 0.0F)
        {
            // AUXPOLES2_HZ > 0 -> Algorithms 1-5: Deadbeat regulator for slow inductive load, or resistive load
            //                                     (set AUXPOLES1_HZ to 1E5 and AUXPOLES2_Z to 0.8)

            pars->pure_delay_periods = int_pure_delay_periods;

            pars->status = regRstInitDeadbeat(pars, load, int_auxpole1_hz, int_auxpoles2_hz, int_auxpoles2_z, int_auxpole4_hz, int_auxpole5_hz);
        }
        else if(load->henrys >= 1.0E-10F)
        {
            // AUXPOLES2_HZ <= 0 and HENRYS > 0 -> Algorithm 10: PI regulator for a fast or poorly known inductive load
            //                                     (it has poor tracking but is very stable)

            regRstInitPI(pars, load, int_auxpole1_hz);
        }
        else
        {
            // AUXPOLES2_HZ <= 0 and HENRYS <= 0 -> Algorithm 20: I regulator for resistive load

            regRstInitI(pars, load, int_auxpole1_hz);
        }
    }
    else
    {
        // Use externally calculated RST coefficients

        pars->rst = *ext_rst;
    }

    // Determine the highest order of the RST polynomials

    i = REG_NUM_RST_COEFFS;

    while(--i > 0 && pars->rst.r[i] == 0.0F && pars->rst.s[i] == 0.0F && pars->rst.t[i] == 0.0F);

    pars->rst_order = i;

    //Check that the R, S and T polynomials are valid using Jury's test on S and T to check for stability

    if(pars->status == REG_OK)
    {
        pars->status = regRstTest(pars);
    }

    if(pars->status == REG_OK)
    {
        // Calculate floating point math correction for T coefficients to ensure that Sum(T) == Sum(R)

        cc_double t0_correction = 0.0F;

        for(i = 0 ; i <= pars->rst_order ; i++)
        {
            t0_correction += (cc_double)pars->rst.r[i] - (cc_double)pars->rst.t[i];
        }

        // Note that s[0] and t[0] must be positive because of the checks done in regJurysTest()

        pars->t0_correction    = t0_correction;
        pars->inv_corrected_t0 = 1.0F / (t0_correction + (cc_double)pars->rst.t[0]);
        pars->inv_s0           = 1.0F /  pars->rst.s[0];

        // Set track_delay to the ext_track_delay_periods supplied if external RST coefficients are used,
        // or the internal non-dead-beat I or PI algorithms.

        if(pars->alg_index == 0 || pars->alg_index >= 10)
        {
            // External RST or internal I or PI controller - set track delay to externally supplied track delay

            pars->track_delay_periods = ext_track_delay_periods;
        }
        else
        {
            // Internal dead-beat algorithms - the track delay will be fixed and depends on whether the
            // algorithm is true dead-beat (alg_idx 1, 3, 5) or pseudo-dead-beat (alg_idx 2, 4)

            if(pars->dead_beat > 0)
            {
                pars->track_delay_periods = (cc_float)pars->dead_beat;
            }
            else
            {
                pars->track_delay_periods = 1.0F + int_pure_delay_periods;
            }

            // With internal dead-beat algorithms, we can calculate the Modulus Margin

            regModulusMargin(pars);
        }

        // If defined then check the modulus margin is not too low

        if(pars->modulus_margin > 0.0F && pars->modulus_margin < REG_MM_WARNING_THRESHOLD)
        {
            pars->status = REG_WARNING_LOW_MODULUS_MARGIN;
        }

        // Calculate coefficients for open loop difference equation.

        regRstInitOpenLoop(pars, load);
    }
    else
    {
        // RST coefficients are invalid and cannot be used

        pars->inv_s0           = 0.0F;
        pars->t0_correction    = 0.0F;
        pars->inv_corrected_t0 = 0.0F;
        pars->rst_order        = 0;
    }

    // Return the status

    return pars->status;
}


// Real-Time Functions

void regRstInitRefRT(struct REG_rst_pars const * const pars,
                     struct REG_rst_vars       * const vars,
                     cc_float                    const rate)
{
    // Return immediately if parameters are invalid

    if(pars->status >= REG_FAULT)
    {
        return;
    }

    // Use RST coefficients to calculate new actuation from reference

    cc_float const ref_offset = rate * pars->track_delay_periods * pars->reg_period;
    uint32_t       var_idx    = vars->history_index;

    vars->ref_limited[var_idx] = vars->ref_closed[var_idx] = vars->meas[var_idx] + ref_offset;

    cc_double meas = (cc_double)pars->t0_correction * (cc_double)vars->ref_closed[var_idx]
                   + (cc_double)pars->rst.t[0]      * (cc_double)vars->ref_closed[var_idx]
                   - (cc_double)pars->rst.s[0]      * (cc_double)vars->act       [var_idx];

    uint32_t const rst_order = pars->rst_order;
    uint32_t       par_idx;

    for(par_idx = 1 ; par_idx <= rst_order ; par_idx++)
    {
        var_idx = (var_idx - 1) & REG_RST_HISTORY_MASK;

        vars->ref_limited[var_idx] = vars->ref_closed[var_idx] = vars->meas[var_idx] + ref_offset;

        meas += (cc_double)pars->rst.t[par_idx] * (cc_double)vars->ref_closed[var_idx]
             -  (cc_double)pars->rst.s[par_idx] * (cc_double)vars->act       [var_idx]
             -  (cc_double)pars->rst.r[par_idx] * (cc_double)vars->meas      [var_idx];
    }

    // Set meas[0] to balance the RST - this should minimise the glitch in the voltage

    var_idx = vars->history_index;

    vars->meas[var_idx] = meas / pars->rst.r[0];

    // Set the open-loop reference to the same as the closed-loop reference - which is based on the measurement

    vars->ref_open[var_idx] = vars->ref_closed[var_idx];
}



cc_float regRstCalcActRT(struct REG_rst_pars const * const pars,
                         struct REG_rst_vars       * const vars,
                         cc_float                    const ref,
                         bool                        const openloop)
{
    cc_double act;
    uint32_t  var_idx;

    // Return zero immediately if parameters are invalid

    if(pars->status >= REG_FAULT)
    {
        return 0.0F;
    }

    // Calculate actuation based on open-loop flag from regRstCalcRefRT() on previous iteration

    if(openloop == true)
    {
        // Store the reference in open-loop history

        vars->ref_open[vars->history_index] = ref;

        // Use open-loop coefficients to calculate new open-loop actuation from reference

        var_idx = (vars->history_index - 1) & REG_RST_HISTORY_MASK;

        // Calculate open loop actuation

        act = (cc_double)pars->openloop_forward.ref[0] * (cc_double)ref
            + (cc_double)pars->openloop_forward.ref[1] * (cc_double)vars->ref_open[var_idx]
            + (cc_double)pars->openloop_forward.act[1] * (cc_double)vars->act[var_idx];
    }
    else
    {
        uint32_t rst_order = pars->rst_order;

        var_idx = vars->history_index;

        // Store the reference in RST history

        vars->ref_closed[var_idx] = ref;

        // Use RST coefficients to calculate new actuation from reference

        act = (cc_double)pars->rst.t[0]      * (cc_double)ref
            - (cc_double)pars->rst.r[0]      * (cc_double)vars->meas[var_idx]
            + (cc_double)pars->t0_correction * (cc_double)ref;

        uint32_t par_idx;

        for(par_idx = 1 ; par_idx <= rst_order ; par_idx++)
        {
            var_idx = (var_idx - 1) & REG_RST_HISTORY_MASK;

            act += (cc_double)pars->rst.t[par_idx] * (cc_double)vars->ref_closed[var_idx]
                -  (cc_double)pars->rst.r[par_idx] * (cc_double)vars->meas      [var_idx]
                -  (cc_double)pars->rst.s[par_idx] * (cc_double)vars->act       [var_idx];
        }

        act *= (cc_double)pars->inv_s0;
    }

    return act;
}



uint32_t regRstCalcRefRT(struct REG_rst_pars const * const pars,
                         struct REG_rst_vars       * const vars,
                         cc_float                    const act,
                         bool                        const is_limited,
                         bool                        const openloop)
{
    uint32_t const var_idx0 = vars->history_index;

    // Return immediately if parameters are invalid

    if(pars->status >= REG_FAULT)
    {
        return var_idx0;
    }

    // If we are limited, we need to calculate both the closed-loop and open-loop reference values for the
    // history. If we are not limited, we only need to calculate the one we are not.

    uint32_t const var_idx1 = (var_idx0 - 1) & REG_RST_HISTORY_MASK;

    if(is_limited == true || openloop == false)
    {
        // Use open-loop coefficients to calculate new open-loop reference from actuation

        vars->ref_open[var_idx0] = (cc_double)pars->openloop_reverse.act[0] * (cc_double)act
                                 + (cc_double)pars->openloop_reverse.act[1] * (cc_double)vars->act     [var_idx1]
                                 + (cc_double)pars->openloop_reverse.ref[1] * (cc_double)vars->ref_open[var_idx1];
    }

    if(is_limited == true || openloop == true)
    {
        // Use RST coefficients to back-calculate reference from actuation

        uint32_t const rst_order = pars->rst_order;
        uint32_t       var_idx   = var_idx0;
        cc_double      ref       = pars->rst.s[0] * (cc_double)act + pars->rst.r[0] * (cc_double)vars->meas[var_idx];
        uint32_t       par_idx;

        for(par_idx = 1 ; par_idx <= rst_order ; par_idx++)
        {
            var_idx = (var_idx - 1) & REG_RST_HISTORY_MASK;

            ref += (cc_double)pars->rst.s[par_idx] * (cc_double)vars->act       [var_idx]
                +  (cc_double)pars->rst.r[par_idx] * (cc_double)vars->meas      [var_idx]
                -  (cc_double)pars->rst.t[par_idx] * (cc_double)vars->ref_closed[var_idx];
        }

        ref *= pars->inv_corrected_t0;

        // Save closed loop ref in history

        vars->ref_closed[var_idx0] = ref;
    }

    // Save act in history

    vars->act[var_idx0] = act;

    // Calculate ref_rate

    if(openloop == true)
    {
        vars->ref_rate = (vars->ref_open[var_idx0] - vars->ref_open[var_idx1]) * pars->inv_reg_period;
    }
    else
    {
        vars->ref_rate = (vars->ref_closed[var_idx0] - vars->ref_closed[var_idx1]) * pars->inv_reg_period;
    }

    // Return the history index

    return var_idx0;
}



cc_float regRstTrackDelayRT(struct REG_rst_vars const * const vars)
{
    cc_float const delta_ref                = regRstDeltaRefRT(vars);
    uint32_t const var_idx                  = vars->history_index;
    cc_float       meas_track_delay_periods = 0.0F;

    // Measure track delay if reference is changing

    if(fabsf(delta_ref) > 1.0E-4F)
    {
        meas_track_delay_periods = 1.0F + ((vars->ref_closed[(var_idx - 1) & REG_RST_HISTORY_MASK]) - vars->meas[var_idx]) / delta_ref;

        // Clip to sane range to handle when delta_ref is small

        if(meas_track_delay_periods < 0.5F)
        {
            meas_track_delay_periods = 0.5F;
        }
        else if(meas_track_delay_periods > 9.0F)
        {
            meas_track_delay_periods = 9.0F;
        }
    }

    return meas_track_delay_periods;
}



cc_float regRstDelayedRefRT(struct REG_rst_pars const * const pars,
                            struct REG_rst_vars const * const vars,
                            uint32_t                    const iteration_index)
{
    // iteration_index will run from 0 to reg_signal->reg_period_iters - 1. When it is zero, it will run the regulation,
    // however, the RST history has not yet been updated at this point in the real-time processing,
    // so the iter_time_offset_periods is set to 1.0.

    cc_float const iter_time_offset_periods = iteration_index == 0
                                            ? 1.0F
                                            : (cc_float)iteration_index * pars->inv_reg_period_iters;

    // Convert ref_delay_periods adjust by the iteration time to integer and fractional parts

    cc_float float_delay_int;
    cc_float delay_frac = modff(pars->ref_delay_periods - iter_time_offset_periods, &float_delay_int);
    int32_t  delay_int  = (int32_t)float_delay_int;

    // Calculate delayed ref_adv using interpolation (or extrapolation) at the measurement period.
    // ref_delay_periods is clipped in regMgrInitRst() to the range [0.1 .. REG_RST_HISTORY_MASK]
    // so no further limits checking is made here.

    uint32_t const idx0 = (vars->history_index - delay_int) & REG_RST_HISTORY_MASK;
    uint32_t const idx1 = (idx0 - 1) & REG_RST_HISTORY_MASK;

    cc_float const ref1 = vars->ref_limited[idx0];
    cc_float const ref2 = vars->ref_limited[idx1];

    return ref1 + delay_frac * (ref2 - ref1);
}



cc_float regRstAverageVrefRT(struct REG_rst_vars const * const vars)
{
    uint32_t i;
    cc_float sum_vref = 0.0F;

    for(i = 0 ; i < REG_NUM_RST_COEFFS ; i++)
    {
        sum_vref += vars->act[i];
    }

    return sum_vref * (1.0F / REG_NUM_RST_COEFFS);
}

// EOF
