//! @file  regSim2.c
//! @brief Converter Control Regulation library voltage source and load simulation functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libreg.h"


// NON-RT   regSim2Init

void regSim2Init(struct REG_sim2_pars * const pars,
                 cc_float                     k[REG_SIM2_K_LEN],            // [4x3]
                 cc_float                     h[REG_SIM2_H_LEN],            // [3x4]
                 cc_float                     ma[REG_SIM2_MA_LEN],          // [4x4]
                 cc_float                     mb[REG_SIM2_MB_LEN])          // [4x1]
{
    // Save pointers to the simulation 2 (voltage source and load) matrices

    pars->k  = k;
    pars->h  = h;
    pars->ma = ma;
    pars->mb = mb;
}


// Real-Time Functions

static inline double regSimRowTimesColumn4RT(float const * const row, double const * const column)
{
    return row[0] * column[0] + row[1] * column[1] + row[2] * column[2] + row[3] * column[3];
}


static inline double regSimRowTimesColumn3RT(float const * const row, double const * const column)
{
    return row[0] * column[0] + row[1] * column[1] + row[2] * column[2];
}



void regSim2RT(struct REG_sim2_pars const * const pars,
               struct REG_sim2_vars       * const vars,
               cc_float                     const f_ref_limited,
               cc_float                     const v_circuit,
               cc_float                     const i_circuit,
               cc_float                     const i_capa)
{
    double   x[4];            // VS and load model corrected state vector [0]:I_filter [1]:V_circuit [2]:I_magnet [3]:V_C1
    double   e[3];            // Error vector:z - sim_h uncorrected_x
    cc_float s[3];            // Simulated measurements [0]:V_circuit [1]:I_circuit [2]:I_capa
    uint32_t i;

    // Simulator 2 for FIRING_REF only

    cc_float const * row;             // Row of H, K or MA matrices (H, K and MA are all row-major matrices)
    cc_float const   z[3] = { v_circuit, i_circuit, i_capa };    // Set measurement vector z_k

    // 1. e = z - H.uncorrected_x                       [3x1] = [3x1] - [3x4][4x1]

    for(i = 0, row = pars->h ; i < 3 ; i++, row += 4)
    {
        e[i] = z[i] + regSimRowTimesColumn4RT(row, vars->uncorrected_x);
    }

    // 2. x = uncorrected_x + K.e                       [4x1] = [4x1] + [4x3][3x1]

    for(i = 0, row = pars->k ; i < 4 ; i++, row += 3)
    {
        x[i] = vars->uncorrected_x[i] + regSimRowTimesColumn3RT(row, e);
    }

    // 3. s = H.x                                       [3x1] = [3x4][4x1]

    for(i = 0, row = pars->h ; i < 3 ; i++, row += 4)
    {
        s[i] = regSimRowTimesColumn4RT(row, x);
    }

    // 4. uncorrected_x = MA.x + MB.f_ref_limited       [4x1] = [4x4][4x1] + [4x1][1x1]

    for(i = 0, row = pars->ma ; i < 4 ; i++, row += 4)
    {
        vars->uncorrected_x[i] = regSimRowTimesColumn4RT(row, x) + pars->mb[i] * f_ref_limited;
    }

    // Save simulated signals

    vars->circuit_voltage = s[0];
    vars->circuit_current = s[1];
    vars->capa_current    = s[2];
    vars->magnet_current  = x[2];
}

// EOF
