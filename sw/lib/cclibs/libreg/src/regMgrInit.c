//! @file  regMgrInit.c
//! @brief Converter Control Regulation library management initialization functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


//#define CCPRINTF

// Include header files

#include "libreg.h"
#include "libreg/regParsInit.h"


// NON-RT   regMgrInitSignal

static void regMgrInitSignal(struct REG_mgr * const reg_mgr, struct REG_mgr_signal * const reg_signal, uint32_t const groups_mask)
{
    reg_mgr->par_enabled_groups_mask |= groups_mask;

    // Initialize the next and active pointers for the operation RST parameters

    reg_signal->op_rst_pars.next       = &reg_signal->op_rst_pars.pars[0];
    reg_signal->op_rst_pars.active     = &reg_signal->op_rst_pars.pars[1];

    // Initialize the next and active pointers for the test RST parameters

    reg_signal->test_rst_pars.next     = &reg_signal->test_rst_pars.pars[0];
    reg_signal->test_rst_pars.active   = &reg_signal->test_rst_pars.pars[1];

    // Initialize the RST parameter statuses to report a fault

    reg_signal->op_rst_pars.active->status   = REG_FAULT_R0_IS_ZERO;
    reg_signal->test_rst_pars.active->status = REG_FAULT_R0_IS_ZERO;

    // Copy active to last so the initial error will be visible to the users

    reg_signal->last_op_rst_pars   = *reg_signal->op_rst_pars.active;
    reg_signal->last_test_rst_pars = *reg_signal->test_rst_pars.active;

    // Set regulation periods to default value to avoid a divide by zero later

    reg_signal->reg_period_iters = 1;
    reg_signal->reg_period       = reg_mgr->iter_period_fp32;
    reg_signal->inv_reg_period   = reg_mgr->inv_iter_period;
    reg_signal->rst_pars         = reg_mgr->b.op_rst_pars.active;
}


// NON-RT   regMgrInit

int32_t regMgrInit(struct REG_mgr                  * const reg_mgr,
                   struct REG_pars                 * const reg_pars,
                   struct REG_deco_shared volatile * const deco_shared,
                   void                            * const mutex,
                   REG_mutex_callback              * const mutex_lock,
                   REG_mutex_callback              * const mutex_unlock,
                   cc_float                        * const v_ac_buf,
                   uint32_t                          const v_ac_buf_len,
                   enum CC_enabled_disabled          const field_regulation,
                   enum CC_enabled_disabled          const current_regulation,
                   enum CC_enabled_disabled          const voltage_regulation)
{
    memset(reg_mgr, 0, sizeof(*reg_mgr));

    reg_mgr->pars            = reg_pars;
    reg_mgr->mutex           = mutex;
    reg_mgr->mutex_lock      = mutex_lock;
    reg_mgr->mutex_unlock    = mutex_unlock;

    reg_mgr->b.regulation    = field_regulation;
    reg_mgr->i.regulation    = current_regulation;
    reg_mgr->v.regulation    = voltage_regulation;

    reg_mgr->reg_mode        = REG_NONE;
    reg_mgr->reg_rst_source  = REG_OPERATIONAL_RST_PARS;
    reg_mgr->reg_signal      = &reg_mgr->i;
    reg_mgr->lim_ref         = &reg_mgr->v.lim_ref;
    reg_mgr->ref             = &reg_mgr->v.ref_reg;

    // Initialize libreg parameter structures reg_mgr

    regMgrParsInit(reg_mgr, reg_pars);

    // Initialise the iteration period to a default value of 1 ms if not set

    if(regMgrParAppValue(reg_pars, ITER_PERIOD_NS) == 0)
    {
        regMgrParAppValue(reg_pars, ITER_PERIOD_NS) = 1000000;
    }

    regMgrParValue(reg_mgr, ITER_PERIOD_NS) = regMgrParAppValue(reg_pars, ITER_PERIOD_NS);

    regMgrInitIterPeriod(reg_mgr, regMgrParValue(reg_mgr, ITER_PERIOD_NS));

    // Initialize the decoupling structure

    regDecoInit(&reg_mgr->deco,
                deco_shared,
                regMgrParPointer(reg_mgr,DECO_K),
                regMgrParPointer(reg_mgr,DECO_D));

    // Initialize harmonics generator

    regHarmonicsInit(&reg_mgr->v.harmonics, v_ac_buf, v_ac_buf_len);

    // Set up the regulation structures for field, current and voltage regulation, as required

    if(field_regulation)
    {
        regMgrInitSignal(reg_mgr, &reg_mgr->b, REG_PAR_FIELD_GROUPS_MASK);
    }

    if(current_regulation)
    {
        regMgrInitSignal(reg_mgr, &reg_mgr->i, REG_PAR_CURRENT_GROUPS_MASK);
    }

    if(voltage_regulation)
    {
        reg_mgr->par_enabled_groups_mask |= REG_PAR_VOLTAGE_GROUPS_MASK;

        regSim2Init(&reg_mgr->sim_pars.vs_and_load,
                    regMgrParPointer(reg_mgr,VS_SIM2_K),
                    regMgrParPointer(reg_mgr,VS_SIM2_H),
                    regMgrParPointer(reg_mgr,VS_SIM2_MA),
                    regMgrParPointer(reg_mgr,VS_SIM2_MB));
    }

    reg_mgr->par_enabled_groups_mask |= REG_PAR_OTHER_GROUPS_MASK;

    // Initialize parameter groups mask to treat every active parameter group

    reg_mgr->par_groups_mask = reg_mgr->par_enabled_groups_mask;

    // Check if mutex is defined that mutex_locked and mutex_unlocked are not NULL

    if(mutex != NULL && (mutex_lock == NULL || mutex_unlock == NULL))
    {
        return -1;
    }

    return 0;
}


// NON-RT   regMgrInitIterPeriod

void regMgrInitIterPeriod(struct REG_mgr * const reg_mgr,
                          uint32_t         const iter_period_ns)
{
    reg_mgr->iter_period      = (cc_double)iter_period_ns * 1.0E-9;
    reg_mgr->iter_period_fp32 = (cc_float)reg_mgr->iter_period;
    reg_mgr->inv_iter_period  = 1.0F / reg_mgr->iter_period_fp32;

    ccprintf("\n  iter_period_ns=%d  iter_period_fp32=%.7f\n", iter_period_ns, reg_mgr->iter_period_fp32);

    // If reg mode is VOLTAGE then update the reg_mgr reg_period variables

    if(reg_mgr->reg_mode == REG_VOLTAGE)
    {
        reg_mgr->reg_period_ns = iter_period_ns;
        reg_mgr->reg_period = reg_mgr->iter_period_fp32;
    }
}


// NON-RT   regMgrInitRegPeriodPointers

void regMgrInitRegPeriodPointers(struct REG_mgr * const reg_mgr,
                                 int32_t        * const b_reg_period_ns_ptr,
                                 int32_t        * const i_reg_period_ns_ptr)
{
    // Save optional pointers (can be NULL) to location to have the regulation periods written in nanoseconds

    reg_mgr->b.reg_period_ns_ptr = b_reg_period_ns_ptr;
    reg_mgr->i.reg_period_ns_ptr = i_reg_period_ns_ptr;

    ccprintf("\n  b.reg_period_ns_ptr=%p  i.reg_period_ns_ptr=%p\n"
            , reg_mgr->b.reg_period_ns_ptr
            , reg_mgr->i.reg_period_ns_ptr
            );
}


// NON-RT   regMgrInitRst

void regMgrInitRst(struct REG_mgr         * const reg_mgr,
                   enum REG_mode            const reg_mode,
                   enum REG_rst_source      const reg_rst_source,
                   uint32_t                 const reg_period_iters,
                   enum CC_enabled_disabled const external_alg,
                   enum REG_meas_select     const int_meas_select,
                   cc_float                       int_pure_delay_periods,
                   cc_float                 const int_auxpole1_hz,
                   cc_float                 const int_auxpoles2_hz,
                   cc_float                 const int_auxpoles2_z,
                   cc_float                 const int_auxpole4_hz,
                   cc_float                 const int_auxpole5_hz,
                   enum REG_meas_select     const ext_meas_select,
                   cc_float                 const ext_track_delay_periods,
                   cc_float                 const ext_r[REG_NUM_RST_COEFFS],
                   cc_float                 const ext_s[REG_NUM_RST_COEFFS],
                   cc_float                 const ext_t[REG_NUM_RST_COEFFS])
{
    struct REG_mgr_signal * const reg_signal = (reg_mode == REG_FIELD ? &reg_mgr->b : &reg_mgr->i);

    // Protect against a zero reg_period_iters by forcing to 1 - this can happen during startup for an application
    // before settings are recovered from storage. Note: the regulation period will never exceed 100ms.

    reg_signal->reg_period_iters = reg_period_iters > 0 ? reg_period_iters : 1;
    reg_signal->reg_period_ns    = regMgrParValue(reg_mgr, ITER_PERIOD_NS) * reg_signal->reg_period_iters;
    reg_signal->reg_period       = (1.0F / CCTIME_NS_PER_S) * (cc_float)reg_signal->reg_period_ns;
    reg_signal->inv_reg_period   = 1.0F / reg_signal->reg_period;

    ccprintf("\n  reg_mode=%u  reg_rst_source=%u  reg_period_iters=%u"
             "\n  sig->reg_period_iters=%u  reg_period_ns=%u  reg_period=%.4E  inv_reg_period=%.4E\n"
            , reg_mode
            , reg_rst_source
            , reg_period_iters
            , reg_signal->reg_period_iters
            , reg_signal->reg_period_ns
            , reg_signal->reg_period
            , reg_signal->inv_reg_period
            );

    // Set pointer to the reg_mgr_rst_pars structure for FIELD/CURRENT regulation with OPERATIONAL/TEST parameters

    // Note that the Test RST parameters are forced to use the same period as the operational parameters,
    // so there is only one period for a signal (current or field).

    struct REG_mgr_rst_pars * mgr_rst_pars;
    struct REG_load_pars    * load_pars;
    struct REG_rst_pars     * last_rst_pars;

    if(reg_rst_source == REG_OPERATIONAL_RST_PARS)
    {
        mgr_rst_pars  = &reg_signal->op_rst_pars;
        last_rst_pars = &reg_signal->last_op_rst_pars;
        load_pars     = &reg_mgr->load_pars;
    }
    else // REG_TEST_PARS
    {
        mgr_rst_pars  = &reg_signal->test_rst_pars;
        last_rst_pars = &reg_signal->last_test_rst_pars;
        load_pars     = &reg_mgr->load_pars_test;
    }

    // Set pointer to the next RST pars structure to be filled, if the preparation of the
    // parameters is successful. The parameters will be created in last_rst_pars, which is
    // visible after a successful or failed attempt to initialize new parameters. This can
    // give the users information about what went wrong.

    struct REG_rst_pars * const next_rst_pars = mgr_rst_pars->next;

    // Prepare to initialise the RST arrays

    struct REG_rst ext_rst = { { 0.0F } };

    if(external_alg == CC_ENABLED)
    {
        // Prepare structure with external RST coefficients

        memcpy(ext_rst.r, ext_r, REG_NUM_RST_COEFFS * sizeof(cc_float));  // On Mac, sizeof(ext_r) is sizeof(float *)
        memcpy(ext_rst.s, ext_s, REG_NUM_RST_COEFFS * sizeof(cc_float));  // and not size of the array of floats.
        memcpy(ext_rst.t, ext_t, REG_NUM_RST_COEFFS * sizeof(cc_float));  // So we force the calculation of the size.
    }
    else if(int_pure_delay_periods <= 0.0F)
    {
        // Internal pure_delay_periods is not positive so calculate it

        int_pure_delay_periods = (regMgrParValue(reg_mgr, VS_ACT_DELAY_ITERS) + (cc_float)reg_mgr->deco.new_phase + reg_mgr->sim_pars.vs.rsp_delay_iters +
                                  reg_signal->meas.delay_iters[int_meas_select]) / (cc_float)reg_signal->reg_period_iters;

        if(regMgrParValue(reg_mgr, VS_ACTUATION) == REG_FIRING_REF)
        {
            // Firing actuation is active so add the firing delay to the pure delay

            int_pure_delay_periods += regMgrParValue(reg_mgr, VS_FIRING_DELAY) / reg_signal->reg_period;
        }
    }

    // Note: regRstInit erases the contents of last_rst_pars so all other fields
    // that need to be set in last_rst_pars must be set after this call

    if(regRstInit(last_rst_pars,
                  reg_mode,
                  reg_signal->reg_period_iters,
                  reg_signal->reg_period,
                  load_pars,
                  external_alg,
                  int_pure_delay_periods,
                  int_auxpole1_hz,
                  int_auxpoles2_hz,
                  int_auxpoles2_z,
                  int_auxpole4_hz,
                  int_auxpole5_hz,
                  ext_track_delay_periods,
                  &ext_rst) >= REG_FAULT)
    {
        // RST parameters are invalid so return with details in last_rst_pars

        return;
    }

    // RST synthesized successfully

    // For operational RSTs, if the reg_period_ns pointer is defined, then write the reg period in nanoseconds to the address.
    // This is provided to support setting the liblog log period values for current and field regulation logs.

    if(reg_rst_source == REG_OPERATIONAL_RST_PARS && reg_signal->reg_period_ns_ptr != NULL)
    {
        ccprintf("\n  reg_period_ns_ptr=%p  reg_period_ns_ptr=%u -> %u\n"
                , reg_signal->reg_period_ns_ptr
                , *reg_signal->reg_period_ns_ptr
                , reg_signal->reg_period_ns
                );

        *reg_signal->reg_period_ns_ptr = reg_signal->reg_period_ns;
    }

    // Set measurement selector

    if(external_alg == CC_ENABLED)
    {
        // External RST algorithm

        last_rst_pars->reg_meas_select = ext_meas_select;
    }
    else
    {
        // Internal RST algorithm

        last_rst_pars->reg_meas_select = int_meas_select;
    }

    // Protect against a track delay of less than 0.1, since this is meaningless for any real controller.

    cc_float const track_delay_periods = last_rst_pars->track_delay_periods > 0.1F
                                       ? last_rst_pars->track_delay_periods
                                       : 0.1F;

    // Calculate track_delay and ref_advance in seconds

    last_rst_pars->track_delay = last_rst_pars->track_delay_periods * last_rst_pars->reg_period;
    last_rst_pars->ref_advance = last_rst_pars->track_delay - reg_signal->meas.delay_iters[last_rst_pars->reg_meas_select] * reg_mgr->iter_period_fp32;

    // Convert track_delay and ref_advance from seconds to the nearest nanosecond

    last_rst_pars->track_delay_ns = (int32_t)(last_rst_pars->track_delay * CCTIME_NS_PER_S + 0.5F);
    last_rst_pars->ref_advance_ns = (int32_t)(last_rst_pars->ref_advance * CCTIME_NS_PER_S + 0.5F);

    // Set the regulation error calculation rate to either the iteration rate or the regulation rate
    //
    // It only makes sense to use the iteration rate if the regulated signal is expected to change linearly during the
    // regulation period. In this case the history of the reference can be linearly interpolated to estimate the delayed
    // reference and this can be used to calculate the regulation error. This can be useful for slow superconducting
    // circuits, where the regulation period can be very long (100ms). In case there is an issue with the voltage source,
    // this gives time for a substantial error to develop in the circuit current before the next regulation iteration.
    // The delayed reference allows the regulation error to be estimated and checked at the faster iteration rate,
    // allowing a beam dump request to be sent sooner.
    //
    // It is better to use the slower regulation rate if the regulated signal will change in a non-linear way during the
    // regulation period. This will be true if there is a significant parallel resistance in the load, or the time constant
    // of the load is less than the regulation period. In this case, the error must be calculated using the regulated
    // measurement, which can be unfiltered, filtered or extrapolated.

    // When the regulation error calculation rate is the iteration rate, the error is calculated using the unfiltered
    // measurement. However, the track delay is the delay between the advanced reference and the regulated measurement.
    // So ref_delay_periods must take this into account and it equals the track_delay plus the delay between the unfiltered
    // measurement and the regulated measurement. However, if the regulated signals is filtered, then ref_delay_periods
    // will be less than the track_delay and can even be less than one period (or even less than zero).
    // If it is less then 1, then the calculation can only be done at the regulation rate using the regulated signal
    // and by using extrapolation rather than interpolation. This inevitably introduces errors in the calculated
    // regulated error, but hopefully these errors are not too significant.

    last_rst_pars->ref_delay_periods = track_delay_periods +
                                       (reg_signal->meas.delay_iters[REG_MEAS_UNFILTERED] -
                                        reg_signal->meas.delay_iters[last_rst_pars->reg_meas_select]) /
                                       (cc_float)reg_signal->reg_period_iters;

    last_rst_pars->reg_err_rate =    last_rst_pars->ref_delay_periods < 1.0F           // ref_delay_periods is less than 1 period, or
                                  || load_pars->tc < last_rst_pars->reg_period         // time constant of load pole is shorter than the regulation period, or
                                  || load_pars->significant_ohms_par == true           // parallel load resistance is significant
                                     ? REG_ERR_RATE_REGULATION
                                     : REG_ERR_RATE_MEASUREMENT;

    if(last_rst_pars->reg_err_rate == REG_ERR_RATE_REGULATION)
    {
        // Regulation rate: regulation error will use the regulation signal

        last_rst_pars->reg_err_meas_select = last_rst_pars->reg_meas_select;

        //  ref_delay equals the track_delay

        last_rst_pars->ref_delay_periods = track_delay_periods;
    }
    else
    {
        // Measurement rate: regulation error will use the unfiltered measurement

        last_rst_pars->reg_err_meas_select = REG_MEAS_UNFILTERED;
    }

    // Clip the ref_delay_periods to avoid over runing the RST reference history buffer

    if(last_rst_pars->ref_delay_periods > (cc_float)REG_RST_HISTORY_MASK)
    {
        last_rst_pars->ref_delay_periods = (cc_float)REG_RST_HISTORY_MASK;
    }

    // Copy the newly initialized RST parameter structure the next RST structure

    *next_rst_pars = *last_rst_pars;

    // Signal to real-time regMgrSignalPrepareRT() to switch to use next RST pars after a delay of at least 1s.
    // The use_next down counter is checked in regMgrRegBIsignalPrepareRT() at the iteration rate.

    CC_MEMORY_BARRIER;

    mgr_rst_pars->use_next = 2 + CCTIME_NS_PER_S / regMgrParValue(reg_mgr, ITER_PERIOD_NS);
}

// EOF
