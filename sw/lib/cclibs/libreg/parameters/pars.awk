# libreg/parameters/pars.awk
#
# Converter Control Regulation library parameter header file generator
#
# All libreg parameters are defined in libreg/parameteres/pars.csv and this
# script translates the csv file into a header file that declares and defines
# structures that describe all the parameters, and a second header file that
# defines a function to initialize the structure.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libreg.
#
# libreg is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Notes
#
# All libreg parameters are addressed by pointers, so initialization of values
# cannot be done by using the linker. Instead, this script creates the static
# function regMgrParsInit() in the header file libreg_pars_init.h.  This initializes
# the reg_mgr->pars and reg_mgr->pars_values structures. This is called by regMgrInit().
#
# There is a restriction in the current implementation which means that parameters
# that are arrays can only be initialized with the same value in all elements.

BEGIN {

    # Read pars.csv from stdin

    ReadCSV()

    # Generate header files

    WriteRegParsHeader("inc/libreg/regPars.h")

    WriteRegParsInitHeader("inc/libreg/regParsInit.h")
}



function ReadCSV(  i)
{
    # Set field separater to comma to read csv file

    FS = ","

    # Identify the leading columns in the csv file

    par_group_column      = 1
    par_name_column       = 2
    par_type_column       = 3
    par_length_column     = 4
    flags_column          = 5
    flag_test_column      = 5
    flag_mode_none_column = 6

    # Read heading line from stdin

    getline

    n_columns      = NF
    n_pars         = 0
    n_groups       = 0
    n_groups_masks = 0

    # Prepare to calculate max length of par_variable

    max_par_variable_len = 0
    max_par_type_len     = 0

    # Create FLAG_LOAD_SELECT as the first flag

    n_flags = 1
    flag[0] = "FLAG_LOAD_SELECT"

    # Create the rest of the flags from the column headings

    for(i=flags_column ; i <= n_columns && index($i,"FLAG_") == 1; i++)
    {
        flag[n_flags++] = $i
    }

    if(n_flags > 32)
    {
        Error("", 0, "Number of flags (" n_flags ") exceeds limit (32)")
    }

    # Create all the groups from the column headings

    groups_column = i

    for(; i <= n_columns ; i++)
    {
        group[n_groups++] = "GROUP_" $i
    }

    if(n_groups > 32)
    {
        Error("", 0, "Number of groups (" n_groups ") exceeds limit (32)")
    }

    # Read parameter definitions from stdin

    line_num = 1

    while(getline > 0)
    {
        line_num++

        # Skip blank lines or comment (#) lines

        if($par_group_column == "" || $par_group_column == "#") continue

        # If PARS column == "GROUPS" then read in the groups mask

        if($par_group_column == "GROUPS")
        {
            groups_mask_name[n_groups_masks] = $par_name_column
            groups_mask[n_groups_masks++]    = ReadGroupsMask()
            continue
        }

        # Stop if non-blank lines do not have the correct number of colums

        if($flags_column == "")
        {
            Error("stdin", line_num, "Missing data")
        }

        # Detect if parameter is an array based on LOAD SELECT

        par_comment[n_pars] = ""

        if($par_length_column == "REG_NUM_LOADS")
        {
            # Check for illegal combination of FLAG_TEST_PAR and FLAG_MODE_NONE_ONLY

            if($flag_test_column == "YES" && $flag_mode_none_column == "YES")
            {
                Error("stdin", line_num, "Parameter cannot be TEST_PAR and MODE_NONE_ONLY")
            }

            par_array_length[n_pars] = "[REG_NUM_LOADS]"

            # Set REG_LOAD_SELECT flag to inform regMgrParsCheck() that this parameter is based on LOAD SELECT

            par_flags[n_pars] = "REG_PAR_" flag[0]

            # For test load select parameters, libreg will keep two values, one for LOAD SELECT and one for LOAD TEST_SELECT

            if($flag_test_column == "YES")
            {
                $par_length_column  = "2"
                par_comment[n_pars] = "// [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT"
            }
            else
            {
                $par_length_column = "1"
                par_comment[n_pars] = "// [0] for LOAD_SELECT"
            }
        }
        else
        {
            par_flags[n_pars] = "0"

            if($par_length_column != 1)
            {
                par_length_multiplier[n_pars] = "*" $par_length_column
                par_array_length[n_pars]      = "[" $par_length_column "]"
            }
        }

        # Save contents

        par_variable[n_pars] = tolower($par_group_column) "_" tolower($par_name_column)
        par_type    [n_pars] = $par_type_column
        par_length  [n_pars] = $par_length_column

        par_variable_len = length(par_variable[n_pars])
        par_type_len     = length(par_type[n_pars])

        if(par_variable_len > max_par_variable_len)
        {
            max_par_variable_len = par_variable_len
        }

        if(par_type_len > max_par_type_len)
        {
            max_par_type_len = par_type_len
        }

        # Interpret flag specifiers (YES or NO)
        # Note: flag 0 (FLAG_LOAD_SELECT) is created internally by pars.awk so flags from pars.csv start from index 1

        for(i=1; i < n_flags ; i++)
        {
            f = $(flags_column+i-1)

            if(f == "YES")
            {
                par_flags[n_pars] = par_flags[n_pars] "|REG_PAR_" flag[i]
            }
            else if(f != "NO")
            {
                Error("stdin", line_num, "Column " (flags_column + i - 1)" (" f ") must be YES or NO")
            }
        }

        # Interpret group specifiers (YES or NO)

        par_groups[n_pars] = "0"

        for(i=0; i < n_groups ; i++)
        {
            f = $(groups_column+i)

            if(f == "YES")
            {
                par_groups[n_pars] = par_groups[n_pars] "|REG_PAR_" group[i]
            }
            else if(f != "NO")
            {
                Error("stdin", line_num, "Column " (groups_column + i)  " (" f ") must be YES or NO")
            }
        }

        n_pars++
    }
}



function ReadGroupsMask(   i, bit, mask, groups_mask)
{
    groups_mask = 0

    mask = 1

    for(i = groups_column; i <= n_columns ; i++)
    {
        if($i == "YES")
        {
            groups_mask += mask
        }

        mask *= 2
    }

    return groups_mask
}



function WriteRegParsHeader(of,  i)
{
    WriteGnuLicense("libreg", "Regulation", "Parameter header file", of)

    print "#pragma once\n"                                                                                          > of

    print "#define REG_NUM_PARS                              ", n_pars                                              > of
    print "#define REG_NUM_LOADS                              4"                                                    > of
    print "#define regMgrParAppPointer(REG_PARS,PAR_NAME)     (&(REG_PARS)->values.REG_PAR_ ## PAR_NAME)"           > of
    print "#define regMgrParAppValue(REG_PARS,PAR_NAME)       ((REG_PARS)->values.REG_PAR_ ## PAR_NAME)"            > of
    print "#define regMgrParPointer(REG_MGR,PAR_NAME)         (&(REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[0])"     > of
    print "#define regMgrParValue(REG_MGR,PAR_NAME)           (REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[0]"        > of
    print "#define regMgrParValueTest(REG_MGR,PAR_NAME)       (REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[1]"        > of

    print "\n// Parameter flags\n"                                                                                  > of

    for(i=0 ; i < n_flags ; i++)
    {
        printf "#define REG_PAR_%-35s(1u<<%d)\n", flag[i], i                                                        > of
    }

    print "\n// Parameter groups\n"                                                                                 > of

    for(i=0 ; i < n_groups ; i++)
    {
        printf "#define REG_PAR_%-35s(1u<<%d)\n", group[i], i                                                       > of
    }

    print "\n// Parameter groups masks\n"                                                                           > of

    other_groups_mask = 2^n_groups - 1

    printf "#define REG_PAR_%-35s0x%08X\n", "ALL_GROUPS_MASK", other_groups_mask                                    > of

    for(i=0 ; i < n_groups_masks ; i++)
    {
        printf "#define REG_PAR_%-35s0x%08X\n", groups_mask_name[i] "_GROUPS_MASK", groups_mask[i]                  > of

        other_groups_mask = other_groups_mask - groups_mask[i]
    }

    printf "#define REG_PAR_%-35s0x%08X\n", "OTHER_GROUPS_MASK", other_groups_mask                                  > of

    print "\n// Parameter variables\n"                                                                              > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "#define REG_PAR_%-*s%s\n",
                max_par_variable_len + 10,  # Add 9 to align with groups
                toupper(par_variable[i]) "         ", par_variable[i]                                               > of
    }

    print "\n// Application parameters structure - can be in slow memory\n"                                         > of

    print "struct REG_pars"                                                                                         > of
    print "{"                                                                                                       > of
    print "    struct"                                                                                              > of
    print "    {"                                                                                                   > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "           %-*s %s%s;\n",  max_par_type_len, par_type[i], par_variable[i], par_array_length[i]      > of
    }

    print "    } values;\n"                                                                                         > of

    print "    struct REG_pars_meta"                                                                                > of
    print "    {"                                                                                                   > of
    print "        void                * app_value;"                                                                > of
    print "        void                * lib_value;"                                                                > of
    print "        uint32_t              size_in_bytes;"                                                            > of
    print "        uint32_t              flags;"                                                                    > of
    print "        uint32_t              groups;"                                                                   > of
    print "    } meta[REG_NUM_PARS];"                                                                               > of
    print "};\n"                                                                                                    > of

    print "// Library parameters structure - included in reg_mgr in fast memory\n"                                  > of

    print "struct REG_par_values"                                                                                   > of
    print "{"                                                                                                       > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "    %-*s %-*s[%s];",
                max_par_type_len, par_type[i], max_par_variable_len, par_variable[i], par_length[i]                 > of

        if(par_comment[i] != "")
        {
            printf "    %s", par_comment[i]                                                                         > of
        }

        printf "\n"                                                                                                 > of
    }

    print "};\n"                                                                                                    > of

    print "// EOF"                                                                                                  > of

    close(of)
}



function WriteRegParsInitHeader(of,  i)
{
    WriteGnuLicense("libreg", "Regulation", "Parameter initialization header file", of)

    print "static void regMgrParsInit(struct REG_mgr * const reg_mgr, struct REG_pars * const reg_pars)"            > of
    print "{"                                                                                                       > of

    print "// Initialize parameter meta data"                                                                       > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "\n    // %3u. %s\n\n", i+1, par_variable[i]                                                         > of
        printf "    reg_pars->meta[%3u].app_value     = &reg_pars->values.%s;\n",   i, par_variable[i]              > of
        printf "    reg_pars->meta[%3u].lib_value     = &reg_mgr->par_values.%s;\n",i, par_variable[i]              > of
        printf "    reg_pars->meta[%3u].size_in_bytes = sizeof(%s)%s;\n",i, par_type  [i], par_length_multiplier[i] > of
        printf "    reg_pars->meta[%3u].flags         = %s;\n",          i, par_flags [i]                           > of
        printf "    reg_pars->meta[%3u].groups        = %s;\n",          i, par_groups[i]                           > of
    }

    print "}\n\n// EOF"                                                                                             > of

    close(of)
}

# EOF

