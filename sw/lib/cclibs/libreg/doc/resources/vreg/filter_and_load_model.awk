# Test Vreg model coefficient calculator

BEGIN {

    T  = 1.0E-4
    Lf = 0.0006
    C1 = 0.040
    C2 = 0.010
    Rf = 0.007
    Rload = 0.115
    Lload = 0.8

    a = T/(2*Lf);
    b = T/(2*C2);
    c = T/(2*C1);
    d = T/(2*Lload);
    e = 1/Rf;
    f = Rload;

    printf "a=%.9E\n",a
    printf "b=%.9E\n",b
    printf "c=%.9E\n",c
    printf "d=%.9E\n",d
    printf "e=%.9E\n",e
    printf "f=%.9E\n",f

    full()
    calc_aa_bb()
    calc_ma()

    print "\nCOMPACT :"
    printf "1/N=%.9E  delta=% .2E\n",1/Fc,1/Fc - 1/F

    for(i=0;i<5;i++)
    {
        printf "\naa[%u]   ",i

        for(j=0;j<4;j++)
        {
            printf "% .15E  ",aa[i][j]
        }
    }

    print

    for(i=0;i<5;i++)
    {
        printf "\nab[%u]   ",i

        for(j=0;j<4;j++)
        {
            printf "% .15E  ",ab[i][j]
        }
    }

    print

    for(i=0;i<5;i++)
    {
        printf "\nma[%u]   ",i

        for(j=0;j<4;j++)
        {
            printf "% .15E  ",ma[i][j]
        }
    }

    print

    for(i=0;i<5;i++)
    {
        for(j=0;j<4;j++)
        {
            printf "\nma[%u][%u]=% .15E\nma[%u][%u]=% .15E   delta=% .2E\n",
                    i,j,mac[i][j],i,j,ma[i][j],mac[i][j]-ma[i][j]
        }
    }

    BW1 = 80
    z1  = 0.9

    BW2 = 40
    z2  = 0.9

    pi = 3.14159265358979323846264338327950288

    wn1=2*pi*BW1/sqrt(1-2*z1*z1 + sqrt(4*z1*z1*z1*z1 - 4*z1*z1 +2));
    wn2=2*pi*BW2/sqrt(1-2*z2*z2 + sqrt(4*z2*z2*z2*z2 - 4*z2*z2 +2));
 
    K_i = (wn2*wn2) * ((2*z1/wn1) - (2*z2/wn2)*(wn2/wn1)*(wn2/wn1))
  
    tf = (1/K_i) - 2*z2/wn2
   
    t = (2*z2/wn2) + ((wn2/wn1)^2 - 1)/K_i
    
    K_p = t*K_i
     
    K_f = tf*K_i

    printf "K_i=%.10g  K_p=%.10g  K_f=%.10g\n", K_i, K_p, K_f
}

function full()
{
    ma[0][0] = b*d - a*b + b*e + c*e + d*f - a*b*c*e - a*b*d*f + b*c*d*e + b*d*e*f + c*d*e*f - a*b*c*d*e*f + 1
    ma[0][1] = -2*a*(c*e + 1)*(d*f + 1)
    ma[0][2] = -2*a*b*e*(d*f + 1)
    ma[0][3] =  2*a*b*(c*e + 1)

    ma[1][0] = 2*b*(c*e + 1)*(d*f + 1)
    ma[1][1] = c*e - b*d - b*e - a*b + d*f - a*b*c*e - a*b*d*f - b*c*d*e - b*d*e*f + c*d*e*f - a*b*c*d*e*f + 1
    ma[1][2] = 2*b*e*(d*f + 1)
    ma[1][3] = -2*b*(c*e + 1)

    ma[2][0] = 2*b*c*e*(d*f + 1)
    ma[2][1] = 2*c*e*(d*f + 1)
    ma[2][2] = a*b + b*d + b*e - c*e + d*f - a*b*c*e + a*b*d*f - b*c*d*e + b*d*e*f - c*d*e*f - a*b*c*d*e*f + 1
    ma[2][3] = -2*b*c*e

    ma[3][0] = 2*b*d*(c*e + 1)
    ma[3][1] = 2*d*(c*e + 1)
    ma[3][2] = 2*b*d*e
    ma[3][3] = a*b - b*d + b*e + c*e - d*f + a*b*c*e - a*b*d*f - b*c*d*e - b*d*e*f - c*d*e*f - a*b*c*d*e*f + 1

    ma[4][0] = 2*a*(b*d + b*e + c*e + d*f + b*c*d*e + b*d*e*f + c*d*e*f + 1)
    ma[4][1] = 2*a*b*(c*e + 1)*(d*f + 1)
    ma[4][2] = 2*a*b*c*e*(d*f + 1)
    ma[4][3] = 2*a*b*d*(c*e + 1)

    F = a*b + b*d + b*e + c*e + d*f + a*b*c*e + a*b*d*f + b*c*d*e + b*d*e*f + c*d*e*f + a*b*c*d*e*f + 1
}

function calc_aa_bb()
{
    cep1     = (c * e + 1.0);
    cem1     = (c * e - 1.0);
    bcep1    = b * cep1;
    bcem1    = b * cem1;
    ace      = a * c * e;
    bace     = b * ace;

    aa[0][0] =  cep1 - bace - b * (a - e);
    aa[0][1] = -2.0 * a * cep1;
    aa[0][2] = -2.0 * a * b * e;
    aa[0][3] = -b * aa[0][1];

    aa[1][0] =  2.0 * bcep1;
    aa[1][1] =  cep1 - bace - b * (a + e);
    aa[1][2] =  2.0 * b * e;
    aa[1][3] = -aa[1][0];

    aa[2][0] =  2.0 * b * c * e;
    aa[2][1] =  2.0 * c * e;
    aa[2][2] = -cem1 - bace + b * (a + e);
    aa[2][3] = -aa[2][0];

    aa[3][0] =  0.0;
    aa[3][1] =  0.0;
    aa[3][2] =  0.0;
    aa[3][3] = -aa[1][1] + 2.0 * cep1;

    aa[4][0] =  2.0 * a * (cep1 + b * e);
    aa[4][1] =  2.0 * a * bcep1;
    aa[4][2] =  2.0 * bace;
    aa[4][3] =  0.0;

    ab[0][0] =  f * aa[0][0] + bcep1;
    ab[0][1] =  f * aa[0][1];
    ab[0][2] =  f * aa[0][2];
    ab[0][3] =  0.0;

    ab[1][0] =  f * aa[1][0];
    ab[1][1] =  f * aa[1][1] - bcep1;
    ab[1][2] =  f * aa[1][2];
    ab[1][3] =  0.0;

    ab[2][0] =  f * aa[2][0];
    ab[2][1] =  f * aa[2][1];
    ab[2][2] =  f * aa[2][2] - bcem1;
    ab[2][3] =  0.0;

    ab[3][0] =  2.0 * bcep1;
    ab[3][1] =  2.0 * cep1;
    ab[3][2] =  2.0 * b * e;
    ab[3][3] = -f * aa[3][3] - bcep1;

    ab[4][0] =  f * aa[4][0] + 2.0 * a * bcep1;
    ab[4][1] =  f * aa[4][1];
    ab[4][2] =  f * aa[4][2];
    ab[4][3] =      aa[4][1];
}

function calc_ma()
{
    Fc = aa[3][3] - d*ab[3][3]

    mac[0][0] = aa[0][0] + d * ab[0][0];
    mac[0][1] = aa[0][1] + d * ab[0][1];
    mac[0][2] = aa[0][2] + d * ab[0][2];
    mac[0][3] = aa[0][3];
    mac[1][0] = aa[1][0] + d * ab[1][0];
    mac[1][1] = aa[1][1] + d * ab[1][1];
    mac[1][2] = aa[1][2] + d * ab[1][2];
    mac[1][3] = aa[1][3];
    mac[2][0] = aa[2][0] + d * ab[2][0];
    mac[2][1] = aa[2][1] + d * ab[2][1];
    mac[2][2] = aa[2][2] + d * ab[2][2];
    mac[2][3] = aa[2][3];
    mac[3][0] =            d * ab[3][0];
    mac[3][1] =            d * ab[3][1];
    mac[3][2] =            d * ab[3][2];
    mac[3][3] = aa[3][3] + d * ab[3][3];
    mac[4][0] = aa[4][0] + d * ab[4][0];
    mac[4][1] = aa[4][1] + d * ab[4][1];
    mac[4][2] = aa[4][2] + d * ab[4][2];
    mac[4][3] =            d * ab[4][3];
}

# EOF
