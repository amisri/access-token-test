#include <stdio.h>

#define OFFSET 1000

int main(int argc, char **argv)
{

    printf("     ceil(% f) = % f\n", 1.6,     ceil( 1.6));
    printf("     ceil(% f) = % f\n", 1.4,     ceil( 1.4));
    printf("     ceil(% f) = % f\n", 0.6,     ceil( 0.6));
    printf("     ceil(% f) = % f\n", 0.4,     ceil( 0.4));
    printf("     ceil(% f) = % f\n",-0.4,     ceil(-0.4));
    printf("     ceil(% f) = % f\n",-0.6,     ceil(-0.6));
    printf("     ceil(% f) = % f\n",-1.4,     ceil(-1.4));
    printf("     ceil(% f) = % f\n",-1.6,     ceil(-1.6));
    printf("    trunc(% f) = % f\n", 1.6,    trunc( 1.6));
    printf("    trunc(% f) = % f\n", 1.4,    trunc( 1.4));
    printf("    trunc(% f) = % f\n", 0.6,    trunc( 0.6));
    printf("    trunc(% f) = % f\n", 0.4,    trunc( 0.4));
    printf("    trunc(% f) = % f\n",-0.4,    trunc(-0.4));
    printf("    trunc(% f) = % f\n",-0.6,    trunc(-0.6));
    printf("    trunc(% f) = % f\n",-1.4,    trunc(-1.4));
    printf("    trunc(% f) = % f\n",-1.6,    trunc(-1.6));
    printf("    floor(% f) = % f\n", 1.6,    floor( 1.6));
    printf("    floor(% f) = % f\n", 1.4,    floor( 1.4));
    printf("    floor(% f) = % f\n", 0.6,    floor( 0.6));
    printf("    floor(% f) = % f\n", 0.4,    floor( 0.4));
    printf("    floor(% f) = % f\n",-0.4,    floor(-0.4));
    printf("    floor(% f) = % f\n",-0.6,    floor(-0.6));
    printf("    floor(% f) = % f\n",-1.4,    floor(-1.4));
    printf("    floor(% f) = % f\n",-1.6,    floor(-1.6));
    printf("nearbyint(% f) = % f\n", 1.6,nearbyint( 1.6));
    printf("nearbyint(% f) = % f\n", 1.4,nearbyint( 1.4));
    printf("nearbyint(% f) = % f\n", 0.6,nearbyint( 0.6));
    printf("nearbyint(% f) = % f\n", 0.4,nearbyint( 0.4));
    printf("nearbyint(% f) = % f\n",-0.4,nearbyint(-0.4));
    printf("nearbyint(% f) = % f\n",-0.6,nearbyint(-0.6));
    printf("nearbyint(% f) = % f\n",-1.4,nearbyint(-1.4));
    printf("nearbyint(% f) = % f\n",-1.6,nearbyint(-1.6));


    printf("     ceil(% f) = % f\n", 1.6,     ceil( 1.6+0.499));
    printf("     ceil(% f) = % f\n", 1.4,     ceil( 1.4+0.499));
    printf("     ceil(% f) = % f\n", 0.6,     ceil( 0.6+0.499));
    printf("     ceil(% f) = % f\n", 0.4,     ceil( 0.4+0.499));
    printf("     ceil(% f) = % f\n",-0.4,     ceil(-0.4+0.499));
    printf("     ceil(% f) = % f\n",-0.6,     ceil(-0.6+0.499));
    printf("     ceil(% f) = % f\n",-1.4,     ceil(-1.4+0.499));
    printf("     ceil(% f) = % f\n",-1.6,     ceil(-1.6+0.499));
    printf("    trunc(% f) = % f\n", 1.6,    trunc( 1.6+0.499));
    printf("    trunc(% f) = % f\n", 1.4,    trunc( 1.4+0.499));
    printf("    trunc(% f) = % f\n", 0.6,    trunc( 0.6+0.499));
    printf("    trunc(% f) = % f\n", 0.4,    trunc( 0.4+0.499));
    printf("    trunc(% f) = % f\n",-0.4,    trunc(-0.4+0.499));
    printf("    trunc(% f) = % f\n",-0.6,    trunc(-0.6+0.499));
    printf("    trunc(% f) = % f\n",-1.4,    trunc(-1.4+0.499));
    printf("    trunc(% f) = % f\n",-1.6,    trunc(-1.6+0.499));
    printf("    floor(% f) = % f\n", 1.6,    floor( 1.6+0.499));
    printf("    floor(% f) = % f\n", 1.4,    floor( 1.4+0.499));
    printf("    floor(% f) = % f\n", 0.6,    floor( 0.6+0.499));
    printf("    floor(% f) = % f\n", 0.4,    floor( 0.4+0.499));
    printf("    floor(% f) = % f\n",-0.4,    floor(-0.4+0.499));
    printf("    floor(% f) = % f\n",-0.6,    floor(-0.6+0.499));
    printf("    floor(% f) = % f\n",-1.4,    floor(-1.4+0.499));
    printf("    floor(% f) = % f\n",-1.6,    floor(-1.6+0.499));
    printf("nearbyint(% f) = % f\n", 1.6,nearbyint( 1.6+0.499));
    printf("nearbyint(% f) = % f\n", 1.4,nearbyint( 1.4+0.499));
    printf("nearbyint(% f) = % f\n", 0.6,nearbyint( 0.6+0.499));
    printf("nearbyint(% f) = % f\n", 0.4,nearbyint( 0.4+0.499));
    printf("nearbyint(% f) = % f\n",-0.4,nearbyint(-0.4+0.499));
    printf("nearbyint(% f) = % f\n",-0.6,nearbyint(-0.6+0.499));
    printf("nearbyint(% f) = % f\n",-1.4,nearbyint(-1.4+0.499));
    printf("nearbyint(% f) = % f\n",-1.6,nearbyint(-1.6+0.499));


    printf("    (int)(% f) = % d\n", 1.6,    (int)( 1.6+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n", 1.4,    (int)( 1.4+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n", 0.6,    (int)( 0.6+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n", 0.4,    (int)( 0.4+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n",-0.4,    (int)(-0.4+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n",-0.6,    (int)(-0.6+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n",-1.4,    (int)(-1.4+OFFSET+0.499) - OFFSET);
    printf("    (int)(% f) = % d\n",-1.6,    (int)(-1.6+OFFSET+0.499) - OFFSET);
}
