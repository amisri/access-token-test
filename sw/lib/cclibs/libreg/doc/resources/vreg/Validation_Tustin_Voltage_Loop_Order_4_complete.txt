% 
RfL = 1e-3;         % R_filter_L
Lf = 0.01;          % L_filter
RfC = 0.05;          % R_filter_C
C1 = 2e-3;          % C1
C2 = 0.5e-3;        % C2
Rload = 0.8001;     % R_ser + R_mag
Lload = 0.975;      % L_load
T=100e-6;


% syms Lf Lload Rf Rload C1 C2 RfL
A=[-RfL/Lf -1/Lf 0 0;1/C2 -1/(C2*RfC) -1/C2 1/(C2*RfC); 0 1/Lload -Rload/Lload 0;0 1/(C1*RfC) 0 -1/(C1*RfC)];
B=[1/Lf;0;0;0];

% sysc=ss(A,B,[1 0 0 0],0)

a = T/(2*Lf);
b = T/(2*C2);
c = T/(2*C1);
d = T/(2*Lload);
e = 1/RfC;
f = Rload;
g = RfL;


% syms a b c d e f g
% 
% AT_2 = [-a*g -a 0 0;b -b*e -b b*e;0 d -d*f 0;0 c*e 0 -c*e];
% 
% BT_2 = [a;0;0;0]
% 
% Am = eye(4)-AT_2;
% 
% Ap = eye(4)+AT_2;
% 
% Am_inv = simplify(inv(Am))

F = (a*b + b*d + b*e + a*g + c*e + d*f + a*b*c*e + a*b*d*f + a*b*d*g + b*c*d*e + a*b*e*g + a*c*e*g + b*d*e*f + a*d*f*g + c*d*e*f + a*b*c*d*e*f + a*b*c*d*e*g + a*b*d*e*f*g + a*c*d*e*f*g + 1);

% Am_prime = Am_inv*F 
% 
% 
% Ad_prime1 = simplify(Ap*Am_prime)



% Ad_prime is a 4x4 Matrix written as 16x1 Vector
% 4x4 Matrix [A11,
%               A12,
%               A13,
%               A14;
% 			 [A21,
% 			  A22,
% 			  ...]

Ad_prime = [                                 - (a*g - 1)*(b*d + b*e + c*e + d*f + b*c*d*e + b*d*e*f + c*d*e*f + 1) - a*b*(c*e + 1)*(d*f + 1),
                                                                                                                    -2*a*(c*e + 1)*(d*f + 1),
                                                                                                                             2*a*b*(c*e + 1),
                                                                                                                          -2*a*b*e*(d*f + 1);
                                                                                                                     2*b*(c*e + 1)*(d*f + 1),
                   b*c*e^2*(a*g + 1)*(d*f + 1) - b*d*(a*g + 1)*(c*e + 1) - (b*e - 1)*(a*g + 1)*(c*e + 1)*(d*f + 1) - a*b*(c*e + 1)*(d*f + 1),
                                                                                                                    -2*b*(a*g + 1)*(c*e + 1),
                                                                                                                   2*b*e*(a*g + 1)*(d*f + 1);
                                                                                                                             2*b*d*(c*e + 1),
                                                                                                                     2*d*(a*g + 1)*(c*e + 1),
                                             - (d*f - 1)*(a*b + b*e + a*g + c*e + a*b*c*e + a*b*e*g + a*c*e*g + 1) - b*d*(a*g + 1)*(c*e + 1),
                                                                                                                           2*b*d*e*(a*g + 1);
                                                                                                                           2*b*c*e*(d*f + 1),
                                                                                                                   2*c*e*(a*g + 1)*(d*f + 1),
                                                                                                                          -2*b*c*e*(a*g + 1),
   b*c*e^2*(a*g + 1)*(d*f + 1) - (c*e - 1)*(a*b + b*d + b*e + a*g + d*f + a*b*d*f + a*b*d*g + a*b*e*g + b*d*e*f + a*d*f*g + a*b*d*e*f*g + 1)];

Ad_prime=vec2mat(Ad_prime,4); % Ignore this


% simplify(Ad_prime1-Ad_prime)

Ad=Ad_prime/F


% Bd_prime1= simplify(inv(AT_2)*F*(Ad-eye(4))*BT_2)

% Column vector 
Bd_prime= [2*a*(b*d + b*e + c*e + d*f + b*c*d*e + b*d*e*f + c*d*e*f + 1);
                                               2*a*b*(c*e + 1)*(d*f + 1);
                                                       2*a*b*d*(c*e + 1);
                                                     2*a*b*c*e*(d*f + 1)];


Bd=Bd_prime/F