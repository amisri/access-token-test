//! @file  libreg.h
//! @brief Converter Control Regulation library header file
//!
//! The converter control regulation library provides support for:
//!
//! * Field, Current and voltage limits
//! * RST-based regulation of field or current with a voltage source
//! * PI-based regulation of voltage and output filter damping
//! * Regulation error calculation
//! * Voltage source and load simulation
//! * Magnet load definition and simulation with magnet saturation compensation
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Include all libreg header files

#include "cclibs.h"
#include "libcctime.h"
#include "libreg/regVars.h"
#include "libreg/regDelay.h"
#include "libreg/regErr.h"
#include "libreg/regLim.h"
#include "libreg/regLoad.h"
#include "libreg/regMeas.h"
#include "libreg/regRst.h"
#include "libreg/regDeco.h"
#include "libreg/regHarmonics.h"
#include "libreg/regVreg.h"
#include "libreg/regSim.h"
#include "libreg/regSim2.h"
#include "libreg/regPars.h"
#include "libreg/regMgr.h"
#include "libreg/regMgrInit.h"
#include "libreg/regMgrPars.h"
#include "libreg/regMgrMeas.h"
#include "libreg/regMgrSim.h"
#include "libreg/regMgrMode.h"
#include "libreg/regMgrReg.h"

// EOF
