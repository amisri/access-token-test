//! @file  regMeas.h
//! @brief Converter Control Regulation library measurement-related functions
//!
//! Provides a basic two-stage cascaded box-car filter with extrapolation to
//! compensate for the delay.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REG_MEAS_RATE_BUF_MASK      3                              //!< Rate will use linear regression through 4 points

#define REG_MEAS_MAX_INVALID_MEAS   4                              //!< Maximum number of consecutive invalid/valid samples to set/reset fault flag

enum REG_meas_fir_state                                            //!  FIR Filter state
{
    REG_MEAS_FIR_INIT_STARTED,
    REG_MEAS_FIR_INIT_FINISHED,
    REG_MEAS_FIR_RUNNING,
};

// Measurement structures

//! Measurement filter parameters and variables

struct REG_meas_filter
{
    enum REG_meas_fir_state state;                                 //!< FIR filters state machine
    int32_t                 buf_len;                               //!< Total length of buffer in elements

    uint32_t                extrapolation_len_iters;               //!< Extrapolation length (normally regulation period)
    uint32_t                extrapolation_index;                   //!< Index to oldest sample in extrapolation buffer

    uint32_t                fir_length[2];                         //!< FIR filter length for two cascaded stages
    uint32_t                fir_index[2];                          //!< Index to oldest sample in FIR buffers
    int32_t                 fir_accumulator[2];                    //!< FIR filter accumulator for two cascaded stages

    int32_t              * fir_buf[2];                             //!< Pointers to buffers for two cascaded FIR stages
    cc_float             * extrapolation_buf;                      //!< Pointer to buffer for extrapolation stage

    cc_float                max_meas_value;                        //!< Maximum value that can be filtered
    cc_float                float_to_integer;                      //!< Factor to convert unfiltered measurement to integer
    cc_float                integer_to_float;                      //!< Factor to converter integer to filtered measurement
    cc_float                extrapolation_factor;                  //!< Extrapolation factor

    uint32_t                history_index;                         //!< Index of most recent sample in history buffer
    cc_float                history_buf[REG_MEAS_RATE_BUF_MASK+1]; //!< History buffer. See also #REG_MEAS_RATE_BUF_MASK
    cc_float                rate;                                  //!< Estimated rate using linear regression through 4 samples at reg period

    cc_float                delay_iters[REG_MEAS_NUM_SELECTS];     //!< Delay for each signal in iterations. See also #REG_MEAS_NUM_SELECTS
    cc_float                signal     [REG_MEAS_NUM_SELECTS];     //!< Array of measurement with different filtering. See also #REG_MEAS_NUM_SELECTS
    cc_float                reg;                                   //!< Measurement used for regulation (selected by reg_select)
};

//! Measurement fault counter structure

struct REG_meas_fault
{
    uint32_t              invalid_counter;                       //!< Counter for number of iterations with invalid measurement
    uint32_t              seq_counter;                           //!< Counter for a sequence of consecutive invalid/valid input measurements
    bool                  flag;                                  //!< True if measurement is in fault (it was invalid for too many consecutive iterations)
};

#ifdef __cplusplus
extern "C" {
#endif

//! Pass memory allocated for the measurement filter buffer into the filter data
//! structure. The buffer is used for both FIR filter stages and the extrapolation
//! history, so it must be long enough to cover all three requirements:
//! reg_meas_filter::fir_length[0] + reg_meas_filter::fir_length[1] + reg_meas_filter::extrapolation_len_iters
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[in,out] filter                     Pointer to measurement filter structure. Buffer is allocated to reg_meas_filter::fir_buf
//! @param[in]     buf                        Pointer to buffer
//! @param[in]     buf_len                    Length of buffer in elements

void regMeasFilterInitBuffer(struct REG_meas_filter * filter,
                             int32_t                * buf,
                             uint32_t                 buf_len);


//! Initialize the FIR measurement filter.
//!
//! The measurement filter buffer must be allocated (using regMeasFilterInitBuffer())
//! before calling this function.
//!
//! Scale factors for the FIR filters are calculated from the positive/negative
//! limits for the measurement being filtered. This is necessary because the
//! filter must work with integers to avoid rounding errors.
//!
//! The filter history buffers are initialized using reg_meas_filter::signal[#REG_MEAS_UNFILTERED],
//! then the filter will be restarted. This can be done in the background while
//! the filter is being called in real-time, but the filter will be bypassed
//! during the initialization process, so expect a perturbation and potential
//! instability.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[in,out] filter                     Pointer to measurement filter structure
//! @param[in]     fir_length                 Two-dimensional array containing the lengths of the FIR filter stages
//! @param[in]     extrapolation_len_iters    Extrapolation buffer length (number of iterations). Normally equal to the regulation period.
//!                                           This can be set to zero if the extrapolated signal is not required. In
//!                                           this case it will be set to the filtered signal.
//! @param[in]     pos                        Positive limit
//! @param[in]     neg                        Negative limit
//! @param[in]     meas_delay_iters           Delay in "unfiltered" measurement, i.e. the hardware filtering delay

void regMeasFilterInit(struct REG_meas_filter * filter,
                       const uint32_t           fir_length[2],
                       uint32_t                 extrapolation_len_iters,
                       cc_float                 pos,
                       cc_float                 neg,
                       cc_float                 meas_delay_iters);



//! Initialize measurement rate history buffer.
//!
//! This function is used by cctest to avoid a glitch in the measurement rate calculation at the start of
//! a simulation. It is not needed for an operational application.
//!
//! @param[in,out] filter                     Pointer to measurement filter structure
//! @param[in]     initial_meas               Initial measurement

void regMeasRateInit(struct REG_meas_filter * filter, cc_float initial_meas);


//! Filter the measurement with a two-stage cascaded box car filter and extrapolate
//! to estimate the measurement without the measurement and FIR filtering delays.
//! If the filter is not running then the output is simply the unfiltered input.
//!
//! This is a Real-Time function.
//!
//! @param[in,out] filter                     Pointer to measurement filter structure

void regMeasFilterRT(struct REG_meas_filter * filter);


//! Calculate the estimated measurement rate by least-squares regression across
//! the last four saved values. The function should be called at the regulation
//! rate. It also calculates the mean of the last four values.
//!
//! This is a Real-Time function.
//!
//! <h3> Linear Least Squares Regression </h3>
//!
//!      y = m.x + b
//!      b = ( sum(x).sum(xy) - sum(x^2).sum(y) ) / ( sum(x)^2 - n.sum(x^2) )
//!      m = ( sum(x).sum(y)  -       n.sum(xy) ) / ( sum(x)^2 - n.sum(x^2) )
//!
//! If points are symmetrical around x = 0:
//!
//!      sum(x) = 0
//!
//! So:
//!
//!      b = sum(y) / n
//!      m = sum(xy) / sum(x^2)
//!
//! For four points, n = 4, and to be symmetrical around x = 0:
//!
//!      t = -i.period
//!      x = 2.( 1.5 - i ) = 2.( 1.5 + t / period )

//!      i =  3,  2,  1,  0
//!      y = y3, y2, y1, y0
//!      x = -3, -1, +1, +3
//!
//! So:
//!
//!      sum(xy)  = 3.(y0 - y3) + y1 - y2
//!      sum(x^2) = 20

//!      rate  = dy/dt = dy/dx . dx/dt
//!      dy/dx = m
//!      dx/dt = 2 / period
//!
//! Therefore:
//!
//!      rate = 2.m / period = ( 2 / period ) . ( 3.(y0 - y3) + y1 - y2 ) / 20
//!
//! @param[in,out] filter                     Pointer to measurement filter structure
//! @param[in]     inv_period                 1/period

void regMeasRateRT(struct REG_meas_filter * filter, cc_float inv_period);


//! Monitors the signal validity to generate the signal fault flag.
//!
//! If allow_faults is true, then this function applies a time filter to the signal's validity flag
//! on iterations when check_invalid_seq is true.  This allows the filter to run at the regulation
//! rate for each signal. If the signal is invalid for REG_MEAS_MAX_INVALID_MEAS regulation
//! periods, then the fault flag will be set. If the signal is then valid for REG_MEAS_MAX_INVALID_MEAS
//! regulation periods, the fault flag will be reset.
//!
//! The function also increments an invalidity counter on every iteration when the sig_is_valid is false.
//!
//! This is a Real-Time function.
//!
//! @param[in,out] meas_fault                 Pointer to measurement fault structure
//! @param[in]     meas_is_valid              True if the measured signal is valid on this iteration
//! @param[in]     allow_fault                True if a fault should be reported if sequence of invalid measurements exceeds limit
//! @param[in]     check_invalid_seq          True if the invalidity sequence should be checked on this iteration

void regMeasSignalInvalidCounterRT(struct REG_meas_fault * meas_fault, bool meas_is_valid, bool allow_fault, bool check_invalid_seq);

#ifdef __cplusplus
}
#endif

// EOF
