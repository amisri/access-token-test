//! @file  regRst.h
//! @brief Converter Control Regulation library RST regulation algorithm functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libreg/regMeas.h"
#include "libreg/regLoad.h"

// Constants

#define REG_RST_HISTORY_MASK       (REG_NUM_RST_COEFFS-1)       //!< History buffer index mask (must be 2^N - 1)
#define REG_MM_WARNING_THRESHOLD   0.4                          //!< Modulus Margin warning threshold

// Regulation RST algorithm structures

//!  RST polynomial arrays and track delay

struct REG_rst
{
    cc_float                    r[REG_NUM_RST_COEFFS];          //!< R polynomial coefficients (measurement). See also #REG_NUM_RST_COEFFS.
    cc_float                    s[REG_NUM_RST_COEFFS];          //!< S polynomial coefficients (actuation). See also #REG_NUM_RST_COEFFS.
    cc_float                    t[REG_NUM_RST_COEFFS];          //!< T polynomial coefficients (reference). See also #REG_NUM_RST_COEFFS.
};

//! Open-loop coefficients. The difference equation for open loop regulation is:
//!
//!   V(t) = act[1]_forward.V(t-1) + ref[0]_forward.I(t) + ref[1]_forward.I(t-1)
//!
//! in the forward direction (to calculate the actuation), and:
//!
//!   I(t) = act[0]_reverse.V(t) + ref[0]_reverse.I(t) + ref[1]_reverse.I(t-1)
//!
//! in the reverse direction (to back-calculate the reference).
//!
//! These coefficients are calculated in regRstInit()

struct REG_openloop
{
    cc_float                    ref[2];                         //!< Difference equation coefficients for I(t) and I(t-1) terms.
    cc_float                    act[2];                         //!< Difference equation coefficients for V(t) term (used only in the reverse
};                                                              //!< direction) and V(t-1) terms (used only in the forward direction).

//!  RST algorithm parameters

struct REG_rst_pars
{
    enum REG_mode               reg_mode;                       //!< Regulation mode (#REG_CURRENT | #REG_FIELD)
    cc_float                    reg_period;                     //!< Regulation period (s)
    cc_float                    inv_reg_period;                 //!< 1.0 / reg_period
    cc_float                    inv_reg_period_iters;           //!< 1.0 / reg_period_iters
    cc_float                    min_auxpole_hz;                 //!< Minimum of RST auxpole*_hz parameters. Used to limit the scan frequency range.

    struct REG_openloop         openloop_forward;               //!< Coefficients for open-loop difference equation in forward direction.
    struct REG_openloop         openloop_reverse;               //!< Coefficients for open-loop difference equation in reverse direction.
    struct REG_rst              rst;                            //!< RST polynomials
    uint32_t                    rst_order;                      //!< Highest order of RST polynomials
    cc_float                    inv_s0;                         //!< 1/ S[0]
    cc_float                    t0_correction;                  //!< Correction to t[0] for rounding errors
    cc_float                    inv_corrected_t0;               //!< 1 / T[0] + t0_correction

    enum REG_status             status;                         //!< Regulation parameters status
    enum REG_meas_select        reg_meas_select;                //!< Measurement to use for regulation
    uint32_t                    alg_index;                      //!< Algorithm index (1-5). Based on pure delay
    uint32_t                    dead_beat;                      //!< 0 = not dead-beat, 1-3 = dead-beat (1-3)
    int32_t                     ref_advance_ns;                 //!< Reference advance time (ns)
    cc_float                    ref_advance;                    //!< Reference advance time (s)
    int32_t                     track_delay_ns;                 //!< Track delay (ns)
    cc_float                    track_delay;                    //!< Track delay (s)
    cc_float                    track_delay_periods;            //!< Track delay in regulation periods
    cc_float                    pure_delay_periods;             //!< Pure delay in regulation periods
    cc_float                    ref_delay_periods;              //!< Reference delay in regulation periods used for regulation error calculation
    enum REG_err_rate           reg_err_rate;                   //!< Regulation error calculation rate
    enum REG_meas_select        reg_err_meas_select;            //!< Measurement to use for regulation error calculation

    cc_float                    modulus_margin;                 //!< Modulus margin. Equal to the minimum value of the sensitivity function (abs_S_p_y)
    cc_float                    modulus_margin_hz;              //!< Frequency for modulus margin.
    cc_float                    a   [REG_NUM_RST_COEFFS];       //!< Plant numerator A. See also #REG_NUM_RST_COEFFS.
    cc_float                    b   [REG_NUM_RST_COEFFS];       //!< Plant denominator B. See also #REG_NUM_RST_COEFFS.
    cc_float                    as  [REG_NUM_RST_COEFFS];       //!< A.S. See also #REG_NUM_RST_COEFFS.
    cc_float                    asbr[REG_NUM_RST_COEFFS];       //!< A.S + B.R. See also #REG_NUM_RST_COEFFS.
};

//! RST algorithm variables

struct REG_rst_vars
{
    uint32_t                    history_index;                  //!< Index to latest entry in the history
    cc_float                    ref_rate;                       //!< Reference rate from previous iteration

    cc_float                    ref_limited[REG_RST_HISTORY_MASK+1]; //!< Limited reference history. See also #REG_RST_HISTORY_MASK.
    cc_float                    ref_open   [REG_RST_HISTORY_MASK+1]; //!< Open loop calculated reference history (only 2 values are used). See also #REG_RST_HISTORY_MASK.
    cc_float                    ref_closed [REG_RST_HISTORY_MASK+1]; //!< RST calculated reference history. See also #REG_RST_HISTORY_MASK.
    cc_float                    meas       [REG_RST_HISTORY_MASK+1]; //!< RST measurement history. See also #REG_RST_HISTORY_MASK.
    cc_float                    act        [REG_RST_HISTORY_MASK+1]; //!< RST actuation history. See also #REG_RST_HISTORY_MASK.
};

// RST macro "functions"

#define regRstIncHistoryIndexRT(rst_vars_p)  (rst_vars_p)->history_index = ((rst_vars_p)->history_index + 1) & REG_RST_HISTORY_MASK
#define regRstPrevRefRT(rst_vars_p)          (rst_vars_p)->ref_closed[(rst_vars_p)->history_index]
#define regRstDeltaRefRT(rst_vars_p)         (regRstPrevRefRT(rst_vars_p) - (rst_vars_p)->ref_closed[((rst_vars_p)->history_index - 1) & REG_RST_HISTORY_MASK])
#define regRstPrevOpenRefRT(rst_vars_p)      (rst_vars_p)->ref_open[(rst_vars_p)->history_index]
#define regRstDeltaOpenRefRT(rst_vars_p)     (regRstPrevOpenRefRT(rst_vars_p) - (rst_vars_p)->ref_open[((rst_vars_p)->history_index - 1) & REG_RST_HISTORY_MASK])
#define regRstPrevActRT(rst_vars_p)          (rst_vars_p)->act[(rst_vars_p)->history_index]

// RST regulation functions

#ifdef __cplusplus
extern "C" {
#endif

//! Prepare coefficients for the RST regulation algorithm, to allow a pure loop delay to be accommodated.
//! This function also prepares coefficients for open loop regulation. See reg_openloop for details.
//!
//! <h3>Notes</h3>
//!
//! The algorithms can calculate the RST coefficients from the auxpole*, pure delay and load parameters.
//! This only works well if the voltage source bandwidth and FIR notch are much faster (~10 times)
//! than the closed loop bandwidth. Three controllers are selectable: I, PI, Deadbeat.
//! For the Deadbeat, the regulator may be a pseudo-deadbeat (fixed but non-integer track delay) according to the pure delay.
//!
//! If the voltage source bandwidth is less than a factor 10 above the closed loop bandwidth, then the
//! algorithms will not produce good results and the RST coefficients need to be calculated manually,
//! e.g. using Matlab.
//!
//! <h3>Reference</h3>
//!
//! This function is based on the paper <a href="https://edms.cern.ch/document/686163/1">CERN EDMS 686163</a>
//! by Hugues Thiesen with extensions from Martin Veenstra and Michele Martino.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out] pars                      RST parameters object to update with the new coefficients.
//! @param[in]  reg_mode                  Regulation mode (voltage, current or field).
//! @param[in]  reg_period_iters          Regulation period. Specified as an integer number of iteration periods,
//!                                       as regulation can only run on iteration boundaries.
//! @param[in]  reg_period                Regulation period in seconds.
//! @param[in]  load                      Load parameters struct. Used by the internal algorithms to calculate RST coefficients.
//! @param[in]  external_alg              CC_ENABLED if external algorithm has calculated the RST coefficients in ext_rst.
//! @param[in]  int_pure_delay_periods    Pure delay in the regulation loop, specified as number of regulation periods,
//!                                       for use by the internal synthesis algorithms if external_alg is CC_DISABLED.
//!                                       This approximates the voltage source actuation delay, voltage source response and
//!                                       measurement delay as a simple pure delay. The choice of internal RST synthesis algorithm will
//!                                       depend on this pure delay when a Deadbeat (or pseudo-deadbeat) regulator is being synthesized.
//! @param[in]  int_auxpole1_hz           Frequency of (real) auxiliary pole 1. Used to calculate RST coefficients if external_alg is CC_DISABLED.
//! @param[in]  int_auxpoles2_hz          Frequency of (conjugate) auxiliary poles 2 and 3. Used to calculate RST coefficients if external_alg is CC_DISABLED.
//! @param[in]  int_auxpoles2_z           Damping of (conjugate) auxiliary poles 2 and 3. Used to calculate RST coefficients if external_alg is CC_DISABLED.
//! @param[in]  int_auxpole4_hz           Frequency of (real) auxiliary pole 4. Used to calculate RST coefficients if external_alg is CC_DISABLED.
//! @param[in]  int_auxpole5_hz           Frequency of (real) auxiliary pole 5. Used to calculate RST coefficients if external_alg is CC_DISABLED.
//! @param[in]  ext_track_delay_periods   Anticipated delay between the setting of the field/current reference and
//!                                       the moment when the measurement should equal the reference, when using the
//!                                       external RST coefficients. Specified in regulation periods.
//! @param[in]  ext_rst                   Externally calculated RST parameters. These are used if external_alg is CC_ENABLED.
//!
//! @retval     REG_OK on success
//! @retval     REG_WARNING if reg_rst_pars::modulus_margin < #REG_MM_WARNING_THRESHOLD
//! @retval     REG_FAULT if s[0] is too small (1E-10) or is unstable (has poles outside the unit circle)

enum REG_status regRstInit(struct REG_rst_pars        * pars,
                           enum REG_mode                reg_mode,
                           uint32_t                     reg_period_iters,
                           cc_float                     reg_period,
                           struct REG_load_pars const * load,
                           enum CC_enabled_disabled     external_alg,
                           cc_float                     int_pure_delay_periods,
                           cc_float                     int_auxpole1_hz,
                           cc_float                     int_auxpoles2_hz,
                           cc_float                     int_auxpoles2_z,
                           cc_float                     int_auxpole4_hz,
                           cc_float                     int_auxpole5_hz,
                           cc_float                     ext_track_delay_periods,
                           struct REG_rst       const * ext_rst);


//! Complete the initialization of the RST history in vars.
//!
//! The actuation (act) and measurement (meas) histories must be kept up to date. This function will initalise
//! the reference (ref) history based on the measurement history and the supplied rate of change. It will
//! modify meas[0] to balance the RST history so as to reduce the perturbation following the change of regulation
//! mode.
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars    Pointer to RST parameters structure
//! @param[in,out] vars    Pointer to history of actuation, measurement and reference values.
//! @param[in]     rate    Estimated measurement rate

void regRstInitRefRT(struct REG_rst_pars const * pars,
                     struct REG_rst_vars       * vars,
                     cc_float                    rate);


//! Use the supplied RST or open loop coefficients to calculate the actuation based on the supplied reference value
//! and the measurement. If openloop is true, then the open loop regulation algorithm is used. Otherwise,
//! we are in closed loop mode and the RST algorithm is used.
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars           RST coefficients and parameters
//! @param[in,out] vars           History of actuation, measurement and reference values. Updated with new values by this function.
//! @param[in]     ref            Latest reference value
//! @param[in]     openloop       Set to true if the function should return the actuation calculated by the open loop algorithm.
//!                               This is required for 1- and 2-quadrant converters while the measurement is less than the
//!                               minimum current for closed loop regulation.
//!
//! @return        New actuation value

cc_float regRstCalcActRT(struct REG_rst_pars const * pars,
                         struct REG_rst_vars       * vars,
                         cc_float                    ref,
                         bool                        openloop);


//! Use the supplied RST and open loop coefficients to back-calculate the reference based on the supplied actuation
//! value and the measurement in *vars. This function is always called after regRstCalcActRT(). It back-calculates the RST and
//! open loop references and saves them in the respective histories. If is_limited is true, both RST and open
//! loop references are calculated, otherwise we calculate the one we are not, based on the value of openloop.
//! The function returns the reference for the active mode (open or closed loop).
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars           RST coefficients and parameters
//! @param[in,out] vars           History of actuation, measurement and reference values. Updated with new values by this function.
//! @param[in]     act            Latest actuation value
//! @param[in]     is_limited     Set to true if act has been limited. In this case, both closed-loop and open-loop reference
//!                               values will be calculated for the history. If set to false, we only calculate one or the other
//!                               (determined by openloop).
//! @param[in]     openloop       Set to true if the function should return the reference back-calculated by the open loop algorithm.
//!                               This is required for 1- and 2-quadrant converters while the measurement is less than I_CLOSELOOP.
//!
//! @return        RST history index

uint32_t regRstCalcRefRT(struct REG_rst_pars const * pars,
                         struct REG_rst_vars       * vars,
                         cc_float                    act,
                         bool                        is_limited,
                         bool                        openloop);


//! Measure the tracking delay if the reference is changing.
//! This function must be called after calling regRstCalcRefRT().
//!
//! This is a Real-Time function.
//!
//! @param[in]     vars            History of regulation actuation, measurement and reference values.
//!
//! @return        Measured track delay in regulation period (clipped between 0.5 and 3.5)

cc_float regRstTrackDelayRT(struct REG_rst_vars const * vars);


//! Calculate the delayed limited reference.
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars               RST parameters. The amount of delay is specified by REG_rst_pars::ref_delay_periods and
//!                                   REG_rst_pars::inv_reg_period_iters.
//! @param[in]     vars               History of actuation, measurement and reference values.
//! @param[in]     iteration_index    Regulation iteration index
//! @return        Delayed limited reference value.

cc_float regRstDelayedRefRT(struct REG_rst_pars const * pars,
                            struct REG_rst_vars const * vars,
                            uint32_t                    iteration_index);


//! Calculate the average RST actuation (V_REF) over the past #REG_AVE_V_REF_LEN iterations.
//!
//! This is a Real-Time function.
//!
//! @param[in]     vars    History of actuation values.
//!
//! @return        Average actuation value

cc_float regRstAverageVrefRT(struct REG_rst_vars const * vars);

#ifdef __cplusplus
}
#endif

// EOF
