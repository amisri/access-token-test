//! @file  regLoad.h
//! @brief Converter Control Regulation library Load-related functions
//!
//! <h4>Load Model</h4>
//!
//! \image html LoadModel.png
//!
//! The load model used by libreg is based on a magnet with inductance L and
//! resistance R_m (zero for superconducting magnets). This is associated
//! with a parallel damping resistor R_p and a series resistor R_s,
//! which corresponds to the cables or bus bars leading to the magnet. In many
//! cases, multiple magnets are connected in series but for low frequencies their
//! individual impedances can be combined.
//!
//! The gain response of this model is first order:
//!
//! \image html  GainResponse.png
//!
//! <h4>Saturation Model</h4>
//!
//! Non-superconducting magnets may be significantly affected by saturation of their iron yokes,
//! resulting in a reduction in their differential inductance as the current increases. This
//! can be by as much as 60%, which can destabilize the current regulation if it is
//! not compensated. Libreg supports compensation of saturation using a five-segment
//! model:
//!
//! \image html  MagnetSaturationModel.png
//!
//! The saturation is summarized by four parameters: L_sat, I_sat_start, I_sat_end
//! and sat_smoothing. Despite being rather crude, this model works very well
//! and hides the non-linearity of the load from the RST regulation algorithm.
//!
//! Note that magnet saturation is not a problem when regulating the magnetic field. In
//! this case it is a second-order effect and can be ignored.

//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REG_LOAD_NUM_SAT_MODEL_SEGS                 4           //!< Number of parameters for saturation model: 0.linear 1.parabola 2.linear 3.parabola

//! Load structure

struct REG_load_pars
{
    cc_float                    ohms_ser;                       //!< Load series resistance
    cc_float                    ohms_par;                       //!< Load parallel resistance
    cc_float                    ohms_mag;                       //!< Load magnet resistance
    cc_float                    henrys;                         //!< Load inductance
    cc_float                    inv_henrys;                     //!< 1.0 / henrys. Clipped to 1.0E+20 to avoid infinities.
    cc_float                    gauss_per_amp;                  //!< Field-to-current ratio for the magnet
    cc_float                    ohms;                           //!< Resistance corresponding to load pole
    cc_float                    tc;                             //!< Time constant for load pole
    cc_float                    ohms1;                          //!< 1 + ohms_ser/ohms_par
    cc_float                    ohms2;                          //!< 1 + ohms_mag/ohms_par
    cc_float                    gain0;                          //!< Load gain 0
    cc_float                    gain1;                          //!< Load gain 1
    cc_float                    gain2;                          //!< Load gain 2 (steady state gain)
    cc_float                    gain3;                          //!< Load gain 3 (magnet current / circuit current)
    cc_float                    meas_ohms;                      //!< Measured resistance without magnet saturation compensation
    cc_float                    meas_henrys;                    //!< Measured inductance without magnet saturation compensation
    cc_float                    meas_henrys_sat;                //!< Measured inductance with magnet saturation compensation
    bool                        significant_ohms_par;           //!< True if ohms_par < 1E4.ohms_ser || ohms_par < 1E4.ohms_mag

    //! Saturation structure

    struct
    {
        cc_float                henrys;                         //!< Inductance for i >> i_sat_end
        cc_float                i_start;                        //!< Current measurement at start of saturation (linear)
        cc_float                i_end;                          //!< Current measurement at end of saturation (linear)
        cc_float                smoothing;                      //!< Saturation smoothing factor (0.0001 - 0.5)
        cc_float                df_dI_linear;                   //!< df/dI during linear segment
        cc_float                d2f_dI2_parabola;               //!< d2f/dI2 during parabolic segment

        cc_float                i[REG_LOAD_NUM_SAT_MODEL_SEGS];            //!< Currents at end of saturation segments (LPLP)
        cc_float                f[REG_LOAD_NUM_SAT_MODEL_SEGS];            //!< Normalized sat_factor at end of saturation segments (LPLP)
        cc_float                integrated_f[REG_LOAD_NUM_SAT_MODEL_SEGS]; //!< Integral of f(I).dI up to the end of the segment (LPLP)
    } sat;
};

#ifdef __cplusplus
extern "C" {
#endif

// Load functions

//! Initialize the load structure. The saturation model is disabled by default.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  load           Load structure to initialize
//! @param[in]   ohms_ser       Load series resistance
//! @param[in]   ohms_par       Load parallel resistance. Setting to zero means there is no magnet in the circuit.
//! @param[in]   ohms_mag       Load magnet resistance
//! @param[in]   henrys         Load inductance
//! @param[in]   gauss_per_amp  Field-to-current ratio for the magnet

void regLoadInit(struct REG_load_pars * const load,
                 cc_float                     ohms_ser,
                 cc_float                     ohms_par,
                 cc_float                     ohms_mag,
                 cc_float                     henrys,
                 cc_float                     gauss_per_amp);


//! Process the magnet saturation parameters and calculate the parabolic-linear-parabolic
//! differential inductance model parameters.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt.
//!
//! @param[in,out]  load            Load structure to store saturation parameters in.
//! @param[in]      henrys_sat      Differential inductance when magnet is fully saturated.
//! @param[in]      i_sat_start     Start current for linear drop in in differential inductance.
//! @param[in]      i_sat_end       Start current for linear drop in in differential inductance.
//! @param[in]      sat_smoothing   Smoothing factor: proportion of inductance reduction to use parabola (0-0.5)

void regLoadInitSat(struct REG_load_pars * const load,
                    cc_float                     henrys_sat,
                    cc_float                     i_sat_start,
                    cc_float                     i_sat_end,
                    cc_float                     sat_smoothing);


//! Calculate the saturation factor for the load for the given measured current.
//!
//! The differential inductance L(i_mag_sat) = L_nominal x sat_factor(i_mag_sat)
//!
//! This is a Real-Time function.
//!
//! @param[in]  load       Load parameters and saturation model
//! @param[in]  i_mag_sat  Current driving magnet saturation
//!
//! @return     Saturation factor

cc_float regLoadSatFactorRT(const struct REG_load_pars * load, cc_float i_mag_sat);


//! Estimate the field based on the current. The field follows a linear-parabola-linear-parabola-linear
//! relationship to current due to magnet saturation.
//!
//! This is a Real-Time function.
//!
//! @param[in]  load    Load parameters and saturation model
//! @param[in]  i_meas  Measured current value
//!
//! @return  Estimated field value

cc_float regLoadCurrentToFieldRT(const struct REG_load_pars * load, cc_float i_meas);

#ifdef __cplusplus
}
#endif

// Static inline functions

//! This function helps to linearize the effects of magnet saturation when regulating
//! current. This is not required if regulating field.
//!
//! The function subtracts voltage from the regulator actuation using the load saturation model.
//!
//! This is a Real-Time function.
//!
//! @param[in]  load       Load parameters and saturation model
//! @param[in]  i_meas     Measured circuit current
//! @param[in]  sat_factor Saturation factor
//! @param[in]  v_ref      Voltage reference
//!
//! @return     Voltage reference after compensation for magnet saturation

static inline cc_float regLoadVrefSatRT(struct REG_load_pars * const load, const cc_float i_meas, const cc_float sat_factor, const cc_float v_ref)
{
    return sat_factor * v_ref + (1.0 - sat_factor) * i_meas * load->ohms;
}


//! This function is the inverse of regLoadVrefSatRT().
//!
//! This is a Real-Time function.
//!
//! @param[in]  load       Load parameters and saturation model
//! @param[in]  i_meas     Measured circuit current
//! @param[in]  sat_factor Saturation factor
//! @param[in]  v_ref_sat  Voltage reference after compensation for magnet saturation
//!
//! @return     Uncompensated voltage reference

static inline cc_float regLoadInverseVrefSatRT(const struct REG_load_pars * const load, const cc_float i_meas, const cc_float sat_factor, const cc_float v_ref_sat)
{
    return (v_ref_sat - (1.0 - sat_factor) * i_meas * load->ohms) / sat_factor;
}

// EOF
