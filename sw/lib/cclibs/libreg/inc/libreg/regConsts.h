//! @file  regConsts.h
//! @brief Converter Control Regulation library public constants header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REG_NUM_RST_COEFFS         16                   //!< Number of elements in RST coefficient arrays (Max RST order + 1) (must be a power of two)
#define REG_FIRING_LUT_LEN         101                  //!< Number of elements in firing linearisation look-up table (LUT)
#define REG_FRESCO_INDEX_LEN       2                    //!< Number of elements for FRESCO_INDEX parameters
#define REG_SIM_NUM_DEN_LEN        4                    //!< Number of voltage source simulation (and v_ff filter) coefficients (must be a power of two)
#define REG_SIM2_K_LEN             12                   //!< Number of elements in row-major K matrix [4x3]
#define REG_SIM2_H_LEN             12                   //!< Number of elements in row-major H matrix [3x4]
#define REG_SIM2_MA_LEN            16                   //!< Number of elements in row-major MA matrix [4x4]
#define REG_SIM2_MB_LEN            4                    //!< Number of elements in MB matrix [4x1]

//! Voltage source actuation

enum REG_actuation
{
    REG_VOLTAGE_REF = 1,                                //!< Actuation is a voltage reference - start at 1 for legacy reasons
    REG_FIRING_REF     ,                                //!< Actuation is a firing reference
    REG_NUM_ACTUATIONS = 2                              //!< Number of actuation modes
};

//! Regulation status

enum REG_status
{
    REG_OK                                   ,          //!< Reg status: Okay
    REG_WARNING_LOW_MODULUS_MARGIN           ,          //!< Reg status: Warning:Modulus margin less than 0.4
    REG_FAULT                                ,          //!< Reg status: Fault:Start of fault status constants
    REG_FAULT_OHMS_PAR_TOO_SMALL = REG_FAULT ,          //!< Reg status: Fault:Parallel resistance is too small
    REG_FAULT_PURE_DELAY_TOO_LARGE           ,          //!< Reg status: Fault:Pure delay is too large (max is 2.4 periods)
    REG_FAULT_R0_IS_ZERO                     ,          //!< Reg status: Fault:R[0] is zero
    REG_FAULT_S0_NOT_POSITIVE                ,          //!< Reg status: Fault:S[0] is zero (or negative)
    REG_FAULT_T0_NOT_POSITIVE                ,          //!< Reg status: Fault:T[0] is zero (or negative)
    REG_FAULT_S_HAS_UNSTABLE_ROOT            ,          //!< Reg status: Fault:Unstable root in S
    REG_FAULT_T_HAS_UNSTABLE_ROOT            ,          //!< Reg status: Fault:Unstable root in T
    REG_STATUS_LEN                                      //!< Number of reg status constants
};

//! Converter regulation mode

enum REG_mode
{
    REG_VOLTAGE    ,                                     //!< Open loop (voltage reference)
    REG_CURRENT    ,                                     //!< Closed loop on current
    REG_FIELD      ,                                     //!< Regulation of field using voltage
    REG_NONE       ,                                     //!< No regulation mode set
    REG_NUM_MODES                                        //!< Number of different reg modes
};

//! Thyristor converter firing mode

enum REG_firing_mode
{
    REG_CASSEL           ,                               //!< Cassel firing mode
    REG_MIXED            ,                               //!< Mixed firing mode
    REG_RECONSTITUE      ,                               //!< Reconstitué firing mode
    REG_ASAD             ,                               //!< ASAD firing mode
    REG_NUM_FIRING_MODES                                 //!< Number of different firing modes
};

//! Regulation parameters source (operational or test)

enum REG_rst_source
{
    REG_OPERATIONAL_RST_PARS ,                          //!< Use operational RST parameters
    REG_TEST_RST_PARS                                   //!< Use test RST parameters
};

//! Signal source

enum REG_sig_source
{
    REG_MEASUREMENT ,                                   //!< Use measured signal
    REG_SIMULATION                                      //!< Use simulated signal
};

//! Regulation error rate control

enum REG_err_rate
{
    REG_ERR_RATE_REGULATION ,                           //!< Calculate regulation error at the regulation rate
    REG_ERR_RATE_MEASUREMENT                            //!< Calculate regulation error at the measurement rate (i.e. the iteration rate)
};

//! Array index to select measurements to use with current or field regulation

enum REG_meas_select
{
    REG_MEAS_UNFILTERED   ,                             //!< Signal unfiltered by the DSP
    REG_MEAS_FILTERED     ,                             //!< Signal after FIR filtering by DSP
    REG_MEAS_EXTRAPOLATED ,                             //!< Signal after FIR filtering and extrapolation by DSP
    REG_MEAS_NUM_SELECTS                                //!< More intuitive name in some parts of the code
};

// EOF
