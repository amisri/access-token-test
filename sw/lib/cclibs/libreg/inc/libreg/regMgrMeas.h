//! @file  regMgrMeas.h
//! @brief Converter Control Regulation library high-level management measurement functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Static inline functions for the application to set the measurements in the real-time thread

static inline void regMgrMeasSetBmeasRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.b_meas.meas = meas;
}

static inline void regMgrMeasSetImeasRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.i_meas.meas = meas;
}

static inline void regMgrMeasSetImagSatRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.i_mag_sat.meas = meas;
}

static inline void regMgrMeasSetIcapaRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.i_capa.meas = meas;
}

static inline void regMgrMeasSetVmeasRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.v_meas.meas = meas;
}

static inline void regMgrMeasSetVacRT(struct REG_mgr * const reg_mgr, struct CC_meas_signal meas)
{
    reg_mgr->inputs.v_ac.meas = meas;
}

#ifdef __cplusplus
extern "C" {
#endif

//! Start of real-time processing on each iteration.
//! Receive new voltage, current and field measurements, then apply limits and filters.
//!
//! First, check reg_mgr.b and reg_mgr.i and swap RST parameter pointers for field and current if
//! reg_mgr_rst_pars.use_next_pars flag is set. Select simulated or real measurements based on
//! input parameter use_sim_meas. For field and current regulation, update the iteration
//! counter, resetting it to zero if the counter has reached the end of the regulation period
//! (= reg_rst_pars.reg_period_iters).
//!
//! Next, check the voltage, current and field measurements. If the voltage measurement is invalid,
//! use the voltage source model instead (reg_err.delayed_ref).
//!
//! If current is being regulated, use the delayed reference (reg_mgr.ref_delayed) adjusted by
//! the regulation error (reg_err.err). Otherwise, extrapolate the previous value using the current
//! rate of change (reg_meas_rate.estimate). Field measurements are treated in the same way.
//!
//! Next, check the current measurement limits with a call to regLimMeasRT(). If field is being
//! regulated, field measurement limits are also checked. If actuation is #REG_VOLTAGE_REF, the
//! voltage regulation and reference limits are checked with regErrCheckLimitsRT().
//! Note that voltage limits can depend on the current, so voltage reference limits are calculated
//! with respect to reg_meas_filter.signal[#REG_MEAS_FILTERED] by regLimVrefCalcRT().
//!
//! Finally, the field and current measurements are filtered with regMeasFilterRT() and the measurement
//! rates are estimated with regMeasRateRT().
//!
//! This is a Real-Time function.
//!
//! @param[in,out] reg_mgr                   Pointer to the regulation manager structure.
//! @param[in]     reg_rst_source            #REG_OPERATIONAL_RST_PARS or #REG_TEST_RST_PARS
//! @param[in]     iter_time                 Iteration time (Unix time + ns time)
//! @param[in]     invert_limits             True if limits should be inverted (due to polarity switch)
//! @param[in]     circuit_open              True if the polarity switch is moving
//!
//! @return        Iteration number (0 indicates that the reference should be calculated on this iteration)

uint32_t regMgrMeasSetRT(struct REG_mgr    * reg_mgr,
                         enum REG_rst_source reg_rst_source,
                         struct CC_ns_time   iter_time,
                         bool                invert_limits,
                         bool                circuit_open);

//! Set feed forward voltage signal. The signal is passed through a delay and a gain before being used.
//!
//! This is a Real-Time function.
//!
//! @param[in,out] reg_mgr                   Pointer to the regulation manager structure.
//! @param[in]     v_ff                      Measurement signal containing the feed forward voltage.

void regMgrMeasSetVffRT(struct REG_mgr * reg_mgr, struct CC_meas_signal v_ff);

#ifdef __cplusplus
}
#endif

// EOF
