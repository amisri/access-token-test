//! @file  inc/libreg/regParsInit.h
//!
//! @brief Converter Control Regulation library : Parameter initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

static void regMgrParsInit(struct REG_mgr * const reg_mgr, struct REG_pars * const reg_pars)
{
// Initialize parameter meta data

    //   1. load_select

    reg_pars->meta[  0].app_value     = &reg_pars->values.load_select;
    reg_pars->meta[  0].lib_value     = &reg_mgr->par_values.load_select;
    reg_pars->meta[  0].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[  0].flags         = 0;
    reg_pars->meta[  0].groups        = 0;

    //   2. load_test_select

    reg_pars->meta[  1].app_value     = &reg_pars->values.load_test_select;
    reg_pars->meta[  1].lib_value     = &reg_mgr->par_values.load_test_select;
    reg_pars->meta[  1].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[  1].flags         = 0;
    reg_pars->meta[  1].groups        = 0;

    //   3. load_sim_tc_error

    reg_pars->meta[  2].app_value     = &reg_pars->values.load_sim_tc_error;
    reg_pars->meta[  2].lib_value     = &reg_mgr->par_values.load_sim_tc_error;
    reg_pars->meta[  2].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  2].flags         = 0;
    reg_pars->meta[  2].groups        = 0|REG_PAR_GROUP_LOAD_SIM;

    //   4. load_ohms_ser

    reg_pars->meta[  3].app_value     = &reg_pars->values.load_ohms_ser;
    reg_pars->meta[  3].lib_value     = &reg_mgr->par_values.load_ohms_ser;
    reg_pars->meta[  3].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  3].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  3].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_FIRING_LUT|REG_PAR_GROUP_VREG|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_LOAD_TEST|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //   5. load_ohms_par

    reg_pars->meta[  4].app_value     = &reg_pars->values.load_ohms_par;
    reg_pars->meta[  4].lib_value     = &reg_mgr->par_values.load_ohms_par;
    reg_pars->meta[  4].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  4].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  4].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_FIRING_LUT|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_LOAD_TEST|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //   6. load_ohms_mag

    reg_pars->meta[  5].app_value     = &reg_pars->values.load_ohms_mag;
    reg_pars->meta[  5].lib_value     = &reg_mgr->par_values.load_ohms_mag;
    reg_pars->meta[  5].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  5].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  5].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_FIRING_LUT|REG_PAR_GROUP_VREG|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_LOAD_TEST|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //   7. load_henrys

    reg_pars->meta[  6].app_value     = &reg_pars->values.load_henrys;
    reg_pars->meta[  6].lib_value     = &reg_mgr->par_values.load_henrys;
    reg_pars->meta[  6].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  6].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  6].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_VREG|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_LOAD_TEST|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //   8. load_henrys_sat

    reg_pars->meta[  7].app_value     = &reg_pars->values.load_henrys_sat;
    reg_pars->meta[  7].lib_value     = &reg_mgr->par_values.load_henrys_sat;
    reg_pars->meta[  7].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  7].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  7].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_LOAD_TEST;

    //   9. load_i_sat_gain

    reg_pars->meta[  8].app_value     = &reg_pars->values.load_i_sat_gain;
    reg_pars->meta[  8].lib_value     = &reg_mgr->par_values.load_i_sat_gain;
    reg_pars->meta[  8].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  8].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  8].groups        = 0;

    //  10. load_i_sat_start

    reg_pars->meta[  9].app_value     = &reg_pars->values.load_i_sat_start;
    reg_pars->meta[  9].lib_value     = &reg_mgr->par_values.load_i_sat_start;
    reg_pars->meta[  9].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[  9].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[  9].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_LOAD_TEST;

    //  11. load_i_sat_end

    reg_pars->meta[ 10].app_value     = &reg_pars->values.load_i_sat_end;
    reg_pars->meta[ 10].lib_value     = &reg_mgr->par_values.load_i_sat_end;
    reg_pars->meta[ 10].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 10].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[ 10].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_LOAD_TEST;

    //  12. load_sat_smoothing

    reg_pars->meta[ 11].app_value     = &reg_pars->values.load_sat_smoothing;
    reg_pars->meta[ 11].lib_value     = &reg_mgr->par_values.load_sat_smoothing;
    reg_pars->meta[ 11].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 11].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[ 11].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_LOAD_TEST;

    //  13. load_gauss_per_amp

    reg_pars->meta[ 12].app_value     = &reg_pars->values.load_gauss_per_amp;
    reg_pars->meta[ 12].lib_value     = &reg_mgr->par_values.load_gauss_per_amp;
    reg_pars->meta[ 12].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 12].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR;
    reg_pars->meta[ 12].groups        = 0|REG_PAR_GROUP_LOAD|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_BREG|REG_PAR_GROUP_LOAD_TEST|REG_PAR_GROUP_BREG_TEST;

    //  14. breg_period_iters

    reg_pars->meta[ 13].app_value     = &reg_pars->values.breg_period_iters;
    reg_pars->meta[ 13].lib_value     = &reg_mgr->par_values.breg_period_iters;
    reg_pars->meta[ 13].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[ 13].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 13].groups        = 0|REG_PAR_GROUP_DECO|REG_PAR_GROUP_B_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  15. breg_external_alg

    reg_pars->meta[ 14].app_value     = &reg_pars->values.breg_external_alg;
    reg_pars->meta[ 14].lib_value     = &reg_mgr->par_values.breg_external_alg;
    reg_pars->meta[ 14].size_in_bytes = sizeof(enum CC_enabled_disabled);
    reg_pars->meta[ 14].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 14].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  16. breg_int_meas_select

    reg_pars->meta[ 15].app_value     = &reg_pars->values.breg_int_meas_select;
    reg_pars->meta[ 15].lib_value     = &reg_mgr->par_values.breg_int_meas_select;
    reg_pars->meta[ 15].size_in_bytes = sizeof(enum REG_meas_select);
    reg_pars->meta[ 15].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 15].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  17. breg_int_pure_delay_periods

    reg_pars->meta[ 16].app_value     = &reg_pars->values.breg_int_pure_delay_periods;
    reg_pars->meta[ 16].lib_value     = &reg_mgr->par_values.breg_int_pure_delay_periods;
    reg_pars->meta[ 16].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 16].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 16].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  18. breg_int_auxpole1_hz

    reg_pars->meta[ 17].app_value     = &reg_pars->values.breg_int_auxpole1_hz;
    reg_pars->meta[ 17].lib_value     = &reg_mgr->par_values.breg_int_auxpole1_hz;
    reg_pars->meta[ 17].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 17].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 17].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  19. breg_int_auxpoles2_hz

    reg_pars->meta[ 18].app_value     = &reg_pars->values.breg_int_auxpoles2_hz;
    reg_pars->meta[ 18].lib_value     = &reg_mgr->par_values.breg_int_auxpoles2_hz;
    reg_pars->meta[ 18].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 18].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 18].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  20. breg_int_auxpoles2_z

    reg_pars->meta[ 19].app_value     = &reg_pars->values.breg_int_auxpoles2_z;
    reg_pars->meta[ 19].lib_value     = &reg_mgr->par_values.breg_int_auxpoles2_z;
    reg_pars->meta[ 19].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 19].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 19].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  21. breg_int_auxpole4_hz

    reg_pars->meta[ 20].app_value     = &reg_pars->values.breg_int_auxpole4_hz;
    reg_pars->meta[ 20].lib_value     = &reg_mgr->par_values.breg_int_auxpole4_hz;
    reg_pars->meta[ 20].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 20].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 20].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  22. breg_int_auxpole5_hz

    reg_pars->meta[ 21].app_value     = &reg_pars->values.breg_int_auxpole5_hz;
    reg_pars->meta[ 21].lib_value     = &reg_mgr->par_values.breg_int_auxpole5_hz;
    reg_pars->meta[ 21].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 21].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 21].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  23. breg_ext_fresco_index

    reg_pars->meta[ 22].app_value     = &reg_pars->values.breg_ext_fresco_index;
    reg_pars->meta[ 22].lib_value     = &reg_mgr->par_values.breg_ext_fresco_index;
    reg_pars->meta[ 22].size_in_bytes = sizeof(uint32_t)*REG_FRESCO_INDEX_LEN;
    reg_pars->meta[ 22].flags         = 0|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 22].groups        = 0;

    //  24. breg_ext_meas_select

    reg_pars->meta[ 23].app_value     = &reg_pars->values.breg_ext_meas_select;
    reg_pars->meta[ 23].lib_value     = &reg_mgr->par_values.breg_ext_meas_select;
    reg_pars->meta[ 23].size_in_bytes = sizeof(enum REG_meas_select);
    reg_pars->meta[ 23].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 23].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  25. breg_ext_track_delay_periods

    reg_pars->meta[ 24].app_value     = &reg_pars->values.breg_ext_track_delay_periods;
    reg_pars->meta[ 24].lib_value     = &reg_mgr->par_values.breg_ext_track_delay_periods;
    reg_pars->meta[ 24].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 24].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 24].groups        = 0|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    //  26. breg_ext_op_r

    reg_pars->meta[ 25].app_value     = &reg_pars->values.breg_ext_op_r;
    reg_pars->meta[ 25].lib_value     = &reg_mgr->par_values.breg_ext_op_r;
    reg_pars->meta[ 25].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 25].flags         = 0|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 25].groups        = 0|REG_PAR_GROUP_BREG;

    //  27. breg_ext_op_s

    reg_pars->meta[ 26].app_value     = &reg_pars->values.breg_ext_op_s;
    reg_pars->meta[ 26].lib_value     = &reg_mgr->par_values.breg_ext_op_s;
    reg_pars->meta[ 26].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 26].flags         = 0|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 26].groups        = 0|REG_PAR_GROUP_BREG;

    //  28. breg_ext_op_t

    reg_pars->meta[ 27].app_value     = &reg_pars->values.breg_ext_op_t;
    reg_pars->meta[ 27].lib_value     = &reg_mgr->par_values.breg_ext_op_t;
    reg_pars->meta[ 27].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 27].flags         = 0|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 27].groups        = 0|REG_PAR_GROUP_BREG;

    //  29. breg_ext_test_r

    reg_pars->meta[ 28].app_value     = &reg_pars->values.breg_ext_test_r;
    reg_pars->meta[ 28].lib_value     = &reg_mgr->par_values.breg_ext_test_r;
    reg_pars->meta[ 28].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 28].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 28].groups        = 0|REG_PAR_GROUP_BREG_TEST;

    //  30. breg_ext_test_s

    reg_pars->meta[ 29].app_value     = &reg_pars->values.breg_ext_test_s;
    reg_pars->meta[ 29].lib_value     = &reg_mgr->par_values.breg_ext_test_s;
    reg_pars->meta[ 29].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 29].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 29].groups        = 0|REG_PAR_GROUP_BREG_TEST;

    //  31. breg_ext_test_t

    reg_pars->meta[ 30].app_value     = &reg_pars->values.breg_ext_test_t;
    reg_pars->meta[ 30].lib_value     = &reg_mgr->par_values.breg_ext_test_t;
    reg_pars->meta[ 30].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 30].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 30].groups        = 0|REG_PAR_GROUP_BREG_TEST;

    //  32. ireg_period_iters

    reg_pars->meta[ 31].app_value     = &reg_pars->values.ireg_period_iters;
    reg_pars->meta[ 31].lib_value     = &reg_mgr->par_values.ireg_period_iters;
    reg_pars->meta[ 31].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[ 31].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 31].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_DECO|REG_PAR_GROUP_I_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  33. ireg_external_alg

    reg_pars->meta[ 32].app_value     = &reg_pars->values.ireg_external_alg;
    reg_pars->meta[ 32].lib_value     = &reg_mgr->par_values.ireg_external_alg;
    reg_pars->meta[ 32].size_in_bytes = sizeof(enum CC_enabled_disabled);
    reg_pars->meta[ 32].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 32].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  34. ireg_int_meas_select

    reg_pars->meta[ 33].app_value     = &reg_pars->values.ireg_int_meas_select;
    reg_pars->meta[ 33].lib_value     = &reg_mgr->par_values.ireg_int_meas_select;
    reg_pars->meta[ 33].size_in_bytes = sizeof(enum REG_meas_select);
    reg_pars->meta[ 33].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 33].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  35. ireg_int_pure_delay_periods

    reg_pars->meta[ 34].app_value     = &reg_pars->values.ireg_int_pure_delay_periods;
    reg_pars->meta[ 34].lib_value     = &reg_mgr->par_values.ireg_int_pure_delay_periods;
    reg_pars->meta[ 34].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 34].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 34].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  36. ireg_int_auxpole1_hz

    reg_pars->meta[ 35].app_value     = &reg_pars->values.ireg_int_auxpole1_hz;
    reg_pars->meta[ 35].lib_value     = &reg_mgr->par_values.ireg_int_auxpole1_hz;
    reg_pars->meta[ 35].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 35].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 35].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  37. ireg_int_auxpoles2_hz

    reg_pars->meta[ 36].app_value     = &reg_pars->values.ireg_int_auxpoles2_hz;
    reg_pars->meta[ 36].lib_value     = &reg_mgr->par_values.ireg_int_auxpoles2_hz;
    reg_pars->meta[ 36].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 36].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 36].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  38. ireg_int_auxpoles2_z

    reg_pars->meta[ 37].app_value     = &reg_pars->values.ireg_int_auxpoles2_z;
    reg_pars->meta[ 37].lib_value     = &reg_mgr->par_values.ireg_int_auxpoles2_z;
    reg_pars->meta[ 37].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 37].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 37].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  39. ireg_int_auxpole4_hz

    reg_pars->meta[ 38].app_value     = &reg_pars->values.ireg_int_auxpole4_hz;
    reg_pars->meta[ 38].lib_value     = &reg_mgr->par_values.ireg_int_auxpole4_hz;
    reg_pars->meta[ 38].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 38].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 38].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  40. ireg_int_auxpole5_hz

    reg_pars->meta[ 39].app_value     = &reg_pars->values.ireg_int_auxpole5_hz;
    reg_pars->meta[ 39].lib_value     = &reg_mgr->par_values.ireg_int_auxpole5_hz;
    reg_pars->meta[ 39].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 39].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 39].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  41. ireg_ext_fresco_index

    reg_pars->meta[ 40].app_value     = &reg_pars->values.ireg_ext_fresco_index;
    reg_pars->meta[ 40].lib_value     = &reg_mgr->par_values.ireg_ext_fresco_index;
    reg_pars->meta[ 40].size_in_bytes = sizeof(uint32_t)*REG_FRESCO_INDEX_LEN;
    reg_pars->meta[ 40].flags         = 0|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 40].groups        = 0;

    //  42. ireg_ext_meas_select

    reg_pars->meta[ 41].app_value     = &reg_pars->values.ireg_ext_meas_select;
    reg_pars->meta[ 41].lib_value     = &reg_mgr->par_values.ireg_ext_meas_select;
    reg_pars->meta[ 41].size_in_bytes = sizeof(enum REG_meas_select);
    reg_pars->meta[ 41].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 41].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  43. ireg_ext_track_delay_periods

    reg_pars->meta[ 42].app_value     = &reg_pars->values.ireg_ext_track_delay_periods;
    reg_pars->meta[ 42].lib_value     = &reg_mgr->par_values.ireg_ext_track_delay_periods;
    reg_pars->meta[ 42].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 42].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 42].groups        = 0|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    //  44. ireg_ext_op_r

    reg_pars->meta[ 43].app_value     = &reg_pars->values.ireg_ext_op_r;
    reg_pars->meta[ 43].lib_value     = &reg_mgr->par_values.ireg_ext_op_r;
    reg_pars->meta[ 43].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 43].flags         = 0|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 43].groups        = 0|REG_PAR_GROUP_IREG;

    //  45. ireg_ext_op_s

    reg_pars->meta[ 44].app_value     = &reg_pars->values.ireg_ext_op_s;
    reg_pars->meta[ 44].lib_value     = &reg_mgr->par_values.ireg_ext_op_s;
    reg_pars->meta[ 44].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 44].flags         = 0|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 44].groups        = 0|REG_PAR_GROUP_IREG;

    //  46. ireg_ext_op_t

    reg_pars->meta[ 45].app_value     = &reg_pars->values.ireg_ext_op_t;
    reg_pars->meta[ 45].lib_value     = &reg_mgr->par_values.ireg_ext_op_t;
    reg_pars->meta[ 45].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 45].flags         = 0|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 45].groups        = 0|REG_PAR_GROUP_IREG;

    //  47. ireg_ext_test_r

    reg_pars->meta[ 46].app_value     = &reg_pars->values.ireg_ext_test_r;
    reg_pars->meta[ 46].lib_value     = &reg_mgr->par_values.ireg_ext_test_r;
    reg_pars->meta[ 46].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 46].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 46].groups        = 0|REG_PAR_GROUP_IREG_TEST;

    //  48. ireg_ext_test_s

    reg_pars->meta[ 47].app_value     = &reg_pars->values.ireg_ext_test_s;
    reg_pars->meta[ 47].lib_value     = &reg_mgr->par_values.ireg_ext_test_s;
    reg_pars->meta[ 47].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 47].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 47].groups        = 0|REG_PAR_GROUP_IREG_TEST;

    //  49. ireg_ext_test_t

    reg_pars->meta[ 48].app_value     = &reg_pars->values.ireg_ext_test_t;
    reg_pars->meta[ 48].lib_value     = &reg_mgr->par_values.ireg_ext_test_t;
    reg_pars->meta[ 48].size_in_bytes = sizeof(cc_float)*REG_NUM_RST_COEFFS;
    reg_pars->meta[ 48].flags         = 0|REG_PAR_FLAG_TEST_PAR|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[ 48].groups        = 0|REG_PAR_GROUP_IREG_TEST;

    //  50. vreg_ext_fresco_index

    reg_pars->meta[ 49].app_value     = &reg_pars->values.vreg_ext_fresco_index;
    reg_pars->meta[ 49].lib_value     = &reg_mgr->par_values.vreg_ext_fresco_index;
    reg_pars->meta[ 49].size_in_bytes = sizeof(uint32_t)*REG_FRESCO_INDEX_LEN;
    reg_pars->meta[ 49].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 49].groups        = 0;

    //  51. vreg_ext_k_int

    reg_pars->meta[ 50].app_value     = &reg_pars->values.vreg_ext_k_int;
    reg_pars->meta[ 50].lib_value     = &reg_mgr->par_values.vreg_ext_k_int;
    reg_pars->meta[ 50].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 50].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 50].groups        = 0|REG_PAR_GROUP_VREG;

    //  52. vreg_ext_k_p

    reg_pars->meta[ 51].app_value     = &reg_pars->values.vreg_ext_k_p;
    reg_pars->meta[ 51].lib_value     = &reg_mgr->par_values.vreg_ext_k_p;
    reg_pars->meta[ 51].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 51].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 51].groups        = 0;

    //  53. vreg_ext_k_ff

    reg_pars->meta[ 52].app_value     = &reg_pars->values.vreg_ext_k_ff;
    reg_pars->meta[ 52].lib_value     = &reg_mgr->par_values.vreg_ext_k_ff;
    reg_pars->meta[ 52].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 52].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 52].groups        = 0;

    //  54. vfilter_v_meas_source

    reg_pars->meta[ 53].app_value     = &reg_pars->values.vfilter_v_meas_source;
    reg_pars->meta[ 53].lib_value     = &reg_mgr->par_values.vfilter_v_meas_source;
    reg_pars->meta[ 53].size_in_bytes = sizeof(enum REG_sig_source);
    reg_pars->meta[ 53].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 53].groups        = 0;

    //  55. vfilter_i_capa_source

    reg_pars->meta[ 54].app_value     = &reg_pars->values.vfilter_i_capa_source;
    reg_pars->meta[ 54].lib_value     = &reg_mgr->par_values.vfilter_i_capa_source;
    reg_pars->meta[ 54].size_in_bytes = sizeof(enum REG_sig_source);
    reg_pars->meta[ 54].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 54].groups        = 0;

    //  56. vfilter_i_capa_filter

    reg_pars->meta[ 55].app_value     = &reg_pars->values.vfilter_i_capa_filter;
    reg_pars->meta[ 55].lib_value     = &reg_mgr->par_values.vfilter_i_capa_filter;
    reg_pars->meta[ 55].size_in_bytes = sizeof(cc_float)*2;
    reg_pars->meta[ 55].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 55].groups        = 0;

    //  57. vfilter_ext_k_u

    reg_pars->meta[ 56].app_value     = &reg_pars->values.vfilter_ext_k_u;
    reg_pars->meta[ 56].lib_value     = &reg_mgr->par_values.vfilter_ext_k_u;
    reg_pars->meta[ 56].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 56].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 56].groups        = 0|REG_PAR_GROUP_VREG;

    //  58. vfilter_ext_k_i

    reg_pars->meta[ 57].app_value     = &reg_pars->values.vfilter_ext_k_i;
    reg_pars->meta[ 57].lib_value     = &reg_mgr->par_values.vfilter_ext_k_i;
    reg_pars->meta[ 57].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 57].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 57].groups        = 0;

    //  59. vfilter_ext_k_d

    reg_pars->meta[ 58].app_value     = &reg_pars->values.vfilter_ext_k_d;
    reg_pars->meta[ 58].lib_value     = &reg_mgr->par_values.vfilter_ext_k_d;
    reg_pars->meta[ 58].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 58].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[ 58].groups        = 0|REG_PAR_GROUP_VREG;

    //  60. vfeedfwd_num

    reg_pars->meta[ 59].app_value     = &reg_pars->values.vfeedfwd_num;
    reg_pars->meta[ 59].lib_value     = &reg_mgr->par_values.vfeedfwd_num;
    reg_pars->meta[ 59].size_in_bytes = sizeof(cc_float)*REG_SIM_NUM_DEN_LEN;
    reg_pars->meta[ 59].flags         = 0;
    reg_pars->meta[ 59].groups        = 0|REG_PAR_GROUP_VFEEDFWD;

    //  61. vfeedfwd_den

    reg_pars->meta[ 60].app_value     = &reg_pars->values.vfeedfwd_den;
    reg_pars->meta[ 60].lib_value     = &reg_mgr->par_values.vfeedfwd_den;
    reg_pars->meta[ 60].size_in_bytes = sizeof(cc_float)*REG_SIM_NUM_DEN_LEN;
    reg_pars->meta[ 60].flags         = 0;
    reg_pars->meta[ 60].groups        = 0|REG_PAR_GROUP_VFEEDFWD;

    //  62. harmonics_ac_period_iters

    reg_pars->meta[ 61].app_value     = &reg_pars->values.harmonics_ac_period_iters;
    reg_pars->meta[ 61].lib_value     = &reg_mgr->par_values.harmonics_ac_period_iters;
    reg_pars->meta[ 61].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[ 61].flags         = 0;
    reg_pars->meta[ 61].groups        = 0|REG_PAR_GROUP_HARMONICS;

    //  63. harmonics_gain

    reg_pars->meta[ 62].app_value     = &reg_pars->values.harmonics_gain;
    reg_pars->meta[ 62].lib_value     = &reg_mgr->par_values.harmonics_gain;
    reg_pars->meta[ 62].size_in_bytes = sizeof(cc_float)*REG_NUM_HARMONICS;
    reg_pars->meta[ 62].flags         = 0;
    reg_pars->meta[ 62].groups        = 0|REG_PAR_GROUP_HARMONICS;

    //  64. harmonics_phase

    reg_pars->meta[ 63].app_value     = &reg_pars->values.harmonics_phase;
    reg_pars->meta[ 63].lib_value     = &reg_mgr->par_values.harmonics_phase;
    reg_pars->meta[ 63].size_in_bytes = sizeof(cc_float)*REG_NUM_HARMONICS;
    reg_pars->meta[ 63].flags         = 0;
    reg_pars->meta[ 63].groups        = 0|REG_PAR_GROUP_HARMONICS;

    //  65. deco_phase

    reg_pars->meta[ 64].app_value     = &reg_pars->values.deco_phase;
    reg_pars->meta[ 64].lib_value     = &reg_mgr->par_values.deco_phase;
    reg_pars->meta[ 64].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[ 64].flags         = 0;
    reg_pars->meta[ 64].groups        = 0|REG_PAR_GROUP_DECO|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  66. deco_index

    reg_pars->meta[ 65].app_value     = &reg_pars->values.deco_index;
    reg_pars->meta[ 65].lib_value     = &reg_mgr->par_values.deco_index;
    reg_pars->meta[ 65].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[ 65].flags         = 0;
    reg_pars->meta[ 65].groups        = 0|REG_PAR_GROUP_DECO|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  67. deco_k

    reg_pars->meta[ 66].app_value     = &reg_pars->values.deco_k;
    reg_pars->meta[ 66].lib_value     = &reg_mgr->par_values.deco_k;
    reg_pars->meta[ 66].size_in_bytes = sizeof(cc_float)*REG_NUM_DECO;
    reg_pars->meta[ 66].flags         = 0;
    reg_pars->meta[ 66].groups        = 0;

    //  68. deco_d

    reg_pars->meta[ 67].app_value     = &reg_pars->values.deco_d;
    reg_pars->meta[ 67].lib_value     = &reg_mgr->par_values.deco_d;
    reg_pars->meta[ 67].size_in_bytes = sizeof(cc_float)*REG_NUM_DECO;
    reg_pars->meta[ 67].flags         = 0;
    reg_pars->meta[ 67].groups        = 0;

    //  69. limits_b_pos

    reg_pars->meta[ 68].app_value     = &reg_pars->values.limits_b_pos;
    reg_pars->meta[ 68].lib_value     = &reg_mgr->par_values.limits_b_pos;
    reg_pars->meta[ 68].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 68].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 68].groups        = 0|REG_PAR_GROUP_B_LIMITS_MEAS|REG_PAR_GROUP_B_LIMITS_REF|REG_PAR_GROUP_DECO|REG_PAR_GROUP_B_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  70. limits_b_standby

    reg_pars->meta[ 69].app_value     = &reg_pars->values.limits_b_standby;
    reg_pars->meta[ 69].lib_value     = &reg_mgr->par_values.limits_b_standby;
    reg_pars->meta[ 69].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 69].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 69].groups        = 0|REG_PAR_GROUP_B_LIMITS_REF;

    //  71. limits_b_neg

    reg_pars->meta[ 70].app_value     = &reg_pars->values.limits_b_neg;
    reg_pars->meta[ 70].lib_value     = &reg_mgr->par_values.limits_b_neg;
    reg_pars->meta[ 70].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 70].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 70].groups        = 0|REG_PAR_GROUP_B_LIMITS_MEAS|REG_PAR_GROUP_B_LIMITS_REF|REG_PAR_GROUP_DECO|REG_PAR_GROUP_B_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  72. limits_b_rate

    reg_pars->meta[ 71].app_value     = &reg_pars->values.limits_b_rate;
    reg_pars->meta[ 71].lib_value     = &reg_mgr->par_values.limits_b_rate;
    reg_pars->meta[ 71].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 71].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 71].groups        = 0|REG_PAR_GROUP_B_LIMITS_REF;

    //  73. limits_b_closeloop

    reg_pars->meta[ 72].app_value     = &reg_pars->values.limits_b_closeloop;
    reg_pars->meta[ 72].lib_value     = &reg_mgr->par_values.limits_b_closeloop;
    reg_pars->meta[ 72].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 72].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 72].groups        = 0|REG_PAR_GROUP_B_LIMITS_REF;

    //  74. limits_b_low

    reg_pars->meta[ 73].app_value     = &reg_pars->values.limits_b_low;
    reg_pars->meta[ 73].lib_value     = &reg_mgr->par_values.limits_b_low;
    reg_pars->meta[ 73].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 73].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 73].groups        = 0|REG_PAR_GROUP_B_LIMITS_MEAS;

    //  75. limits_b_zero

    reg_pars->meta[ 74].app_value     = &reg_pars->values.limits_b_zero;
    reg_pars->meta[ 74].lib_value     = &reg_mgr->par_values.limits_b_zero;
    reg_pars->meta[ 74].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 74].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 74].groups        = 0|REG_PAR_GROUP_B_LIMITS_MEAS;

    //  76. limits_b_err_warning

    reg_pars->meta[ 75].app_value     = &reg_pars->values.limits_b_err_warning;
    reg_pars->meta[ 75].lib_value     = &reg_mgr->par_values.limits_b_err_warning;
    reg_pars->meta[ 75].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 75].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 75].groups        = 0|REG_PAR_GROUP_B_LIMITS_ERR;

    //  77. limits_b_err_fault

    reg_pars->meta[ 76].app_value     = &reg_pars->values.limits_b_err_fault;
    reg_pars->meta[ 76].lib_value     = &reg_mgr->par_values.limits_b_err_fault;
    reg_pars->meta[ 76].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 76].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[ 76].groups        = 0|REG_PAR_GROUP_B_LIMITS_ERR;

    //  78. limits_i_pos

    reg_pars->meta[ 77].app_value     = &reg_pars->values.limits_i_pos;
    reg_pars->meta[ 77].lib_value     = &reg_mgr->par_values.limits_i_pos;
    reg_pars->meta[ 77].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 77].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[ 77].groups        = 0|REG_PAR_GROUP_I_LIMITS_MEAS|REG_PAR_GROUP_I_LIMITS_REF|REG_PAR_GROUP_DECO|REG_PAR_GROUP_I_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  79. limits_i_standby

    reg_pars->meta[ 78].app_value     = &reg_pars->values.limits_i_standby;
    reg_pars->meta[ 78].lib_value     = &reg_mgr->par_values.limits_i_standby;
    reg_pars->meta[ 78].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 78].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[ 78].groups        = 0|REG_PAR_GROUP_I_LIMITS_REF;

    //  80. limits_i_neg

    reg_pars->meta[ 79].app_value     = &reg_pars->values.limits_i_neg;
    reg_pars->meta[ 79].lib_value     = &reg_mgr->par_values.limits_i_neg;
    reg_pars->meta[ 79].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 79].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[ 79].groups        = 0|REG_PAR_GROUP_I_LIMITS_MEAS|REG_PAR_GROUP_I_LIMITS_REF|REG_PAR_GROUP_DECO|REG_PAR_GROUP_I_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    //  81. limits_i_rate

    reg_pars->meta[ 80].app_value     = &reg_pars->values.limits_i_rate;
    reg_pars->meta[ 80].lib_value     = &reg_mgr->par_values.limits_i_rate;
    reg_pars->meta[ 80].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 80].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 80].groups        = 0|REG_PAR_GROUP_I_LIMITS_REF;

    //  82. limits_i_closeloop

    reg_pars->meta[ 81].app_value     = &reg_pars->values.limits_i_closeloop;
    reg_pars->meta[ 81].lib_value     = &reg_mgr->par_values.limits_i_closeloop;
    reg_pars->meta[ 81].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 81].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 81].groups        = 0|REG_PAR_GROUP_I_LIMITS_REF;

    //  83. limits_i_low

    reg_pars->meta[ 82].app_value     = &reg_pars->values.limits_i_low;
    reg_pars->meta[ 82].lib_value     = &reg_mgr->par_values.limits_i_low;
    reg_pars->meta[ 82].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 82].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 82].groups        = 0|REG_PAR_GROUP_I_LIMITS_MEAS;

    //  84. limits_i_zero

    reg_pars->meta[ 83].app_value     = &reg_pars->values.limits_i_zero;
    reg_pars->meta[ 83].lib_value     = &reg_mgr->par_values.limits_i_zero;
    reg_pars->meta[ 83].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 83].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 83].groups        = 0|REG_PAR_GROUP_I_LIMITS_MEAS;

    //  85. limits_i_err_warning

    reg_pars->meta[ 84].app_value     = &reg_pars->values.limits_i_err_warning;
    reg_pars->meta[ 84].lib_value     = &reg_mgr->par_values.limits_i_err_warning;
    reg_pars->meta[ 84].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 84].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 84].groups        = 0|REG_PAR_GROUP_I_LIMITS_ERR;

    //  86. limits_i_err_fault

    reg_pars->meta[ 85].app_value     = &reg_pars->values.limits_i_err_fault;
    reg_pars->meta[ 85].lib_value     = &reg_mgr->par_values.limits_i_err_fault;
    reg_pars->meta[ 85].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 85].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 85].groups        = 0|REG_PAR_GROUP_I_LIMITS_ERR;

    //  87. limits_i_rms_load_tc

    reg_pars->meta[ 86].app_value     = &reg_pars->values.limits_i_rms_load_tc;
    reg_pars->meta[ 86].lib_value     = &reg_mgr->par_values.limits_i_rms_load_tc;
    reg_pars->meta[ 86].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 86].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 86].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  88. limits_i_rms_load_warning

    reg_pars->meta[ 87].app_value     = &reg_pars->values.limits_i_rms_load_warning;
    reg_pars->meta[ 87].lib_value     = &reg_mgr->par_values.limits_i_rms_load_warning;
    reg_pars->meta[ 87].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 87].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 87].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  89. limits_i_rms_load_fault

    reg_pars->meta[ 88].app_value     = &reg_pars->values.limits_i_rms_load_fault;
    reg_pars->meta[ 88].lib_value     = &reg_mgr->par_values.limits_i_rms_load_fault;
    reg_pars->meta[ 88].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 88].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 88].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  90. limits_i_rms_tc

    reg_pars->meta[ 89].app_value     = &reg_pars->values.limits_i_rms_tc;
    reg_pars->meta[ 89].lib_value     = &reg_mgr->par_values.limits_i_rms_tc;
    reg_pars->meta[ 89].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 89].flags         = 0;
    reg_pars->meta[ 89].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  91. limits_i_rms_warning

    reg_pars->meta[ 90].app_value     = &reg_pars->values.limits_i_rms_warning;
    reg_pars->meta[ 90].lib_value     = &reg_mgr->par_values.limits_i_rms_warning;
    reg_pars->meta[ 90].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 90].flags         = 0;
    reg_pars->meta[ 90].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  92. limits_i_rms_fault

    reg_pars->meta[ 91].app_value     = &reg_pars->values.limits_i_rms_fault;
    reg_pars->meta[ 91].lib_value     = &reg_mgr->par_values.limits_i_rms_fault;
    reg_pars->meta[ 91].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 91].flags         = 0;
    reg_pars->meta[ 91].groups        = 0|REG_PAR_GROUP_I_LIMITS_RMS;

    //  93. limits_v_pos

    reg_pars->meta[ 92].app_value     = &reg_pars->values.limits_v_pos;
    reg_pars->meta[ 92].lib_value     = &reg_mgr->par_values.limits_v_pos;
    reg_pars->meta[ 92].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 92].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[ 92].groups        = 0|REG_PAR_GROUP_V_LIMITS_MEAS|REG_PAR_GROUP_V_LIMITS_REF|REG_PAR_GROUP_VREG_LIMITS|REG_PAR_GROUP_HARMONICS;

    //  94. limits_v_neg

    reg_pars->meta[ 93].app_value     = &reg_pars->values.limits_v_neg;
    reg_pars->meta[ 93].lib_value     = &reg_mgr->par_values.limits_v_neg;
    reg_pars->meta[ 93].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 93].flags         = REG_PAR_FLAG_LOAD_SELECT|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[ 93].groups        = 0|REG_PAR_GROUP_V_LIMITS_MEAS|REG_PAR_GROUP_V_LIMITS_REF|REG_PAR_GROUP_VREG_LIMITS;

    //  95. limits_v_rate

    reg_pars->meta[ 94].app_value     = &reg_pars->values.limits_v_rate;
    reg_pars->meta[ 94].lib_value     = &reg_mgr->par_values.limits_v_rate;
    reg_pars->meta[ 94].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 94].flags         = 0;
    reg_pars->meta[ 94].groups        = 0|REG_PAR_GROUP_V_LIMITS_REF;

    //  96. limits_v_rate_rms_tc

    reg_pars->meta[ 95].app_value     = &reg_pars->values.limits_v_rate_rms_tc;
    reg_pars->meta[ 95].lib_value     = &reg_mgr->par_values.limits_v_rate_rms_tc;
    reg_pars->meta[ 95].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 95].flags         = 0;
    reg_pars->meta[ 95].groups        = 0|REG_PAR_GROUP_V_LIMITS_RATE_RMS;

    //  97. limits_v_rate_rms_fault

    reg_pars->meta[ 96].app_value     = &reg_pars->values.limits_v_rate_rms_fault;
    reg_pars->meta[ 96].lib_value     = &reg_mgr->par_values.limits_v_rate_rms_fault;
    reg_pars->meta[ 96].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 96].flags         = 0;
    reg_pars->meta[ 96].groups        = 0|REG_PAR_GROUP_V_LIMITS_RATE_RMS;

    //  98. limits_v_err_warning

    reg_pars->meta[ 97].app_value     = &reg_pars->values.limits_v_err_warning;
    reg_pars->meta[ 97].lib_value     = &reg_mgr->par_values.limits_v_err_warning;
    reg_pars->meta[ 97].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 97].flags         = 0;
    reg_pars->meta[ 97].groups        = 0|REG_PAR_GROUP_V_LIMITS_ERR;

    //  99. limits_v_err_fault

    reg_pars->meta[ 98].app_value     = &reg_pars->values.limits_v_err_fault;
    reg_pars->meta[ 98].lib_value     = &reg_mgr->par_values.limits_v_err_fault;
    reg_pars->meta[ 98].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 98].flags         = 0;
    reg_pars->meta[ 98].groups        = 0|REG_PAR_GROUP_V_LIMITS_ERR;

    // 100. limits_p_pos

    reg_pars->meta[ 99].app_value     = &reg_pars->values.limits_p_pos;
    reg_pars->meta[ 99].lib_value     = &reg_mgr->par_values.limits_p_pos;
    reg_pars->meta[ 99].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[ 99].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[ 99].groups        = 0|REG_PAR_GROUP_V_LIMITS_REF;

    // 101. limits_p_neg

    reg_pars->meta[100].app_value     = &reg_pars->values.limits_p_neg;
    reg_pars->meta[100].lib_value     = &reg_mgr->par_values.limits_p_neg;
    reg_pars->meta[100].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[100].flags         = REG_PAR_FLAG_LOAD_SELECT;
    reg_pars->meta[100].groups        = 0|REG_PAR_GROUP_V_LIMITS_REF;

    // 102. limits_i_quadrants41

    reg_pars->meta[101].app_value     = &reg_pars->values.limits_i_quadrants41;
    reg_pars->meta[101].lib_value     = &reg_mgr->par_values.limits_i_quadrants41;
    reg_pars->meta[101].size_in_bytes = sizeof(cc_float)*2;
    reg_pars->meta[101].flags         = 0;
    reg_pars->meta[101].groups        = 0|REG_PAR_GROUP_V_LIMITS_REF;

    // 103. limits_v_quadrants41

    reg_pars->meta[102].app_value     = &reg_pars->values.limits_v_quadrants41;
    reg_pars->meta[102].lib_value     = &reg_mgr->par_values.limits_v_quadrants41;
    reg_pars->meta[102].size_in_bytes = sizeof(cc_float)*2;
    reg_pars->meta[102].flags         = 0;
    reg_pars->meta[102].groups        = 0|REG_PAR_GROUP_V_LIMITS_REF;

    // 104. meas_b_delay_iters

    reg_pars->meta[103].app_value     = &reg_pars->values.meas_b_delay_iters;
    reg_pars->meta[103].lib_value     = &reg_mgr->par_values.meas_b_delay_iters;
    reg_pars->meta[103].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[103].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[103].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_B_MEAS_FILTER|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    // 105. meas_i_delay_iters

    reg_pars->meta[104].app_value     = &reg_pars->values.meas_i_delay_iters;
    reg_pars->meta[104].lib_value     = &reg_mgr->par_values.meas_i_delay_iters;
    reg_pars->meta[104].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[104].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[104].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_I_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    // 106. meas_v_delay_iters

    reg_pars->meta[105].app_value     = &reg_pars->values.meas_v_delay_iters;
    reg_pars->meta[105].lib_value     = &reg_mgr->par_values.meas_v_delay_iters;
    reg_pars->meta[105].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[105].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[105].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_V_LIMITS_ERR;

    // 107. meas_b_fir_lengths

    reg_pars->meta[106].app_value     = &reg_pars->values.meas_b_fir_lengths;
    reg_pars->meta[106].lib_value     = &reg_mgr->par_values.meas_b_fir_lengths;
    reg_pars->meta[106].size_in_bytes = sizeof(uint32_t)*2;
    reg_pars->meta[106].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_FIELD_REG;
    reg_pars->meta[106].groups        = 0|REG_PAR_GROUP_B_MEAS_FILTER|REG_PAR_GROUP_BREG|REG_PAR_GROUP_BREG_TEST;

    // 108. meas_i_fir_lengths

    reg_pars->meta[107].app_value     = &reg_pars->values.meas_i_fir_lengths;
    reg_pars->meta[107].lib_value     = &reg_mgr->par_values.meas_i_fir_lengths;
    reg_pars->meta[107].size_in_bytes = sizeof(uint32_t)*2;
    reg_pars->meta[107].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_CURRENT_REG;
    reg_pars->meta[107].groups        = 0|REG_PAR_GROUP_I_MEAS_FILTER|REG_PAR_GROUP_IREG|REG_PAR_GROUP_IREG_TEST;

    // 109. vs_actuation

    reg_pars->meta[108].app_value     = &reg_pars->values.vs_actuation;
    reg_pars->meta[108].lib_value     = &reg_mgr->par_values.vs_actuation;
    reg_pars->meta[108].size_in_bytes = sizeof(enum REG_actuation);
    reg_pars->meta[108].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[108].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_FIRING_LUT|REG_PAR_GROUP_VREG|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_V_LIMITS_ERR|REG_PAR_GROUP_VREG_LIMITS|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 110. vs_act_delay_iters

    reg_pars->meta[109].app_value     = &reg_pars->values.vs_act_delay_iters;
    reg_pars->meta[109].lib_value     = &reg_mgr->par_values.vs_act_delay_iters;
    reg_pars->meta[109].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[109].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[109].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 111. vs_num_vsources

    reg_pars->meta[110].app_value     = &reg_pars->values.vs_num_vsources;
    reg_pars->meta[110].lib_value     = &reg_mgr->par_values.vs_num_vsources;
    reg_pars->meta[110].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[110].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[110].groups        = 0|REG_PAR_GROUP_VREG_LIMITS|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 112. vs_gain

    reg_pars->meta[111].app_value     = &reg_pars->values.vs_gain;
    reg_pars->meta[111].lib_value     = &reg_mgr->par_values.vs_gain;
    reg_pars->meta[111].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[111].flags         = 0;
    reg_pars->meta[111].groups        = 0|REG_PAR_GROUP_VREG_LIMITS;

    // 113. vs_offset

    reg_pars->meta[112].app_value     = &reg_pars->values.vs_offset;
    reg_pars->meta[112].lib_value     = &reg_mgr->par_values.vs_offset;
    reg_pars->meta[112].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[112].flags         = 0;
    reg_pars->meta[112].groups        = 0;

    // 114. vs_firing_mode

    reg_pars->meta[113].app_value     = &reg_pars->values.vs_firing_mode;
    reg_pars->meta[113].lib_value     = &reg_mgr->par_values.vs_firing_mode;
    reg_pars->meta[113].size_in_bytes = sizeof(enum REG_firing_mode);
    reg_pars->meta[113].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[113].groups        = 0|REG_PAR_GROUP_VREG_LIMITS;

    // 115. vs_firing_delay

    reg_pars->meta[114].app_value     = &reg_pars->values.vs_firing_delay;
    reg_pars->meta[114].lib_value     = &reg_mgr->par_values.vs_firing_delay;
    reg_pars->meta[114].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[114].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[114].groups        = 0|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 116. vs_firing_lut

    reg_pars->meta[115].app_value     = &reg_pars->values.vs_firing_lut;
    reg_pars->meta[115].lib_value     = &reg_mgr->par_values.vs_firing_lut;
    reg_pars->meta[115].size_in_bytes = sizeof(cc_float)*REG_FIRING_LUT_LEN;
    reg_pars->meta[115].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[115].groups        = 0|REG_PAR_GROUP_FIRING_LUT|REG_PAR_GROUP_VREG_LIMITS;

    // 117. vs_sim_bandwidth

    reg_pars->meta[116].app_value     = &reg_pars->values.vs_sim_bandwidth;
    reg_pars->meta[116].lib_value     = &reg_mgr->par_values.vs_sim_bandwidth;
    reg_pars->meta[116].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[116].flags         = 0;
    reg_pars->meta[116].groups        = 0|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 118. vs_sim_z

    reg_pars->meta[117].app_value     = &reg_pars->values.vs_sim_z;
    reg_pars->meta[117].lib_value     = &reg_mgr->par_values.vs_sim_z;
    reg_pars->meta[117].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[117].flags         = 0;
    reg_pars->meta[117].groups        = 0|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 119. vs_sim_tau_zero

    reg_pars->meta[118].app_value     = &reg_pars->values.vs_sim_tau_zero;
    reg_pars->meta[118].lib_value     = &reg_mgr->par_values.vs_sim_tau_zero;
    reg_pars->meta[118].size_in_bytes = sizeof(cc_float);
    reg_pars->meta[118].flags         = 0;
    reg_pars->meta[118].groups        = 0|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 120. vs_sim_num

    reg_pars->meta[119].app_value     = &reg_pars->values.vs_sim_num;
    reg_pars->meta[119].lib_value     = &reg_mgr->par_values.vs_sim_num;
    reg_pars->meta[119].size_in_bytes = sizeof(cc_float)*REG_SIM_NUM_DEN_LEN;
    reg_pars->meta[119].flags         = 0;
    reg_pars->meta[119].groups        = 0|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 121. vs_sim_den

    reg_pars->meta[120].app_value     = &reg_pars->values.vs_sim_den;
    reg_pars->meta[120].lib_value     = &reg_mgr->par_values.vs_sim_den;
    reg_pars->meta[120].size_in_bytes = sizeof(cc_float)*REG_SIM_NUM_DEN_LEN;
    reg_pars->meta[120].flags         = 0;
    reg_pars->meta[120].groups        = 0|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_IREG|REG_PAR_GROUP_BREG|REG_PAR_GROUP_IREG_TEST|REG_PAR_GROUP_BREG_TEST;

    // 122. vs_sim2_k

    reg_pars->meta[121].app_value     = &reg_pars->values.vs_sim2_k;
    reg_pars->meta[121].lib_value     = &reg_mgr->par_values.vs_sim2_k;
    reg_pars->meta[121].size_in_bytes = sizeof(cc_float)*REG_SIM2_K_LEN;
    reg_pars->meta[121].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[121].groups        = 0;

    // 123. vs_sim2_h

    reg_pars->meta[122].app_value     = &reg_pars->values.vs_sim2_h;
    reg_pars->meta[122].lib_value     = &reg_mgr->par_values.vs_sim2_h;
    reg_pars->meta[122].size_in_bytes = sizeof(cc_float)*REG_SIM2_H_LEN;
    reg_pars->meta[122].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[122].groups        = 0;

    // 124. vs_sim2_ma

    reg_pars->meta[123].app_value     = &reg_pars->values.vs_sim2_ma;
    reg_pars->meta[123].lib_value     = &reg_mgr->par_values.vs_sim2_ma;
    reg_pars->meta[123].size_in_bytes = sizeof(cc_float)*REG_SIM2_MA_LEN;
    reg_pars->meta[123].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[123].groups        = 0;

    // 125. vs_sim2_mb

    reg_pars->meta[124].app_value     = &reg_pars->values.vs_sim2_mb;
    reg_pars->meta[124].lib_value     = &reg_mgr->par_values.vs_sim2_mb;
    reg_pars->meta[124].size_in_bytes = sizeof(cc_float)*REG_SIM2_MB_LEN;
    reg_pars->meta[124].flags         = 0|REG_PAR_FLAG_VOLTAGE_REG;
    reg_pars->meta[124].groups        = 0;

    // 126. iter_period_ns

    reg_pars->meta[125].app_value     = &reg_pars->values.iter_period_ns;
    reg_pars->meta[125].lib_value     = &reg_mgr->par_values.iter_period_ns;
    reg_pars->meta[125].size_in_bytes = sizeof(uint32_t);
    reg_pars->meta[125].flags         = 0|REG_PAR_FLAG_MODE_NONE_ONLY;
    reg_pars->meta[125].groups        = 0|REG_PAR_GROUP_ITER_PERIOD|REG_PAR_GROUP_MEAS_SIM_DELAYS|REG_PAR_GROUP_VS_SIM|REG_PAR_GROUP_V_LIMITS_RATE_RMS|REG_PAR_GROUP_I_LIMITS_RMS|REG_PAR_GROUP_LOAD_SIM|REG_PAR_GROUP_VREF_ADV|REG_PAR_GROUP_VFEEDFWD|REG_PAR_GROUP_HARMONICS;
}

// EOF
