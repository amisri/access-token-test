//! @file regDecoStructs.h
//! @brief Converter Control Regulation library symmetric decoupling structures
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//  Constants

#define REG_NUM_DECO            4                               //!< Number of FGCs that can perform symmetric decoupling

//! Decoupling data shared between controllers

struct REG_deco_shared
{
    uint32_t                    state;                          //!< Decoupling algorithm state  (0=Disabled 1=Enabled)
    uint32_t                    index;                          //!< Controller decoupling index (0-3)
    cc_float                    v_ref [REG_NUM_DECO];           //!< Voltage references          (V_REF_FF)
    cc_float                    i_meas[REG_NUM_DECO];           //!< Current measurements        (I_MEAS_REG)
};

// EOF
