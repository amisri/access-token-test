//! @file  regMgrSim.h
//! @brief Converter Control Regulation library high-level management simulation functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//! Simulate the voltage source and load
//!
//! This always calls the Simulator 1 function, even though its signals are only used if the actuation is VOLTAGE_REF.
//! Simulator 2 is only called if the actuation is FIRING_REF.
//!
//! @param[in,out] reg_mgr              Pointer to regulation manager structure.
//! @param[in]     circuit_is_open      True if circuit is open (e.g. due to a polarity switch).

void regMgrSimVsAndLoadRT(struct REG_mgr * reg_mgr, bool circuit_is_open);


//! Simulate the measurement of the voltage, current and field
//!
//! Uses regDelaySignalRT() to simulate the measurement of the voltage, current and field and i_capa. The simulation of the VS and load
//! does not take into account the VS actuation delay, so this must be added to the measurement delay when simulating the measurements.
//!
//! The simulated voltage measurement is used as the delayed reference for the voltage error calculation.
//!
//! @param[in,out] reg_mgr              Pointer to regulation manager structure.
//! @param[in,out] sim_vars             Pointer to simulation variable structures.

void regMgrSimMeasurementsRT(struct REG_mgr * reg_mgr, struct REG_mgr_sim_vars * sim_vars);


// Set or reset the measurement simulation control
//
// This function allow control of the measurement simulation. If the simulation mode is disabled,
// the load simulation is reset.
//
// @param[in,out] reg_mgr              Pointer to regulation manager structure.
// @param[in]     sim_meas             True to simulate the measurements, false to use the real measurements.

void regMgrSimMeasSetRT(struct REG_mgr * reg_mgr, bool sim_meas);

#ifdef __cplusplus
}
#endif

// EOF
