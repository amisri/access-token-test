//! @file  regVreg.h
//! @brief Converter Control Regulation library voltage regulation and damping algorithm functions for thyristor converters
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// Voltage Regulation and Damping algorithm parameter structures

struct REG_v_pars
{
    cc_float                  * firing_lut;                     //!< Firing linearisation look-up table
    bool                        asad_1q_without_lut;            //!< Firing reference scaling needed for ASAD 1 quadrant converter without LUT
    bool                        lut_enabled;                    //!< Firing look-up table is defined
    bool                        linearisation_enabled;          //!< Firing linearisation is enabled: asad_1q_without_lut || lut_enabled
    cc_float                    k_aw;                           //!< Anti-wind-up gain for integrator
    cc_float                    k_w;                            //!< Filter damping gain for D_REF
    cc_float                    f_neg;                          //!< Negative F_REF limit
    cc_float                    f_pos;                          //!< Positive F_REF limit
    cc_float                    lut_factor;                     //!< LUT index factor ((REG_FIRING_LUT_LEN - 1) / (f_pos - f_neg))
    cc_float                    inv_num_vsources;               //!< 1/VS_NUM_VSOURCES, used to share V_REF between multiple voltage sources
    cc_float                    vs_gain;                        //!< Voltage source gain (Vout/Vdac)
    cc_float                    inv_vs_gain;                    //!< Inverse voltage source gain
    cc_float                    ref_advance;                    //!< Reference advance for voltage regulation in seconds
    int32_t                     ref_advance_ns;                 //!< Reference advance for voltage regulation in nanoseconds
};

// Voltage Regulation and Damping algorithm variable structures

struct REG_v_vars
{
    cc_float                    v_meas_reg;                     //!< v_meas or v_meas_sim, according to v_meas_source
    cc_float                    v_integrator;                   //!< Voltage regulator integrator
    cc_float                    v_reg_err;                      //!< Voltage regulator error
    cc_float                    d_ref;                          //!< Damping regulator reference
    cc_float                    d_meas;                         //!< Damping regulator measurement
    cc_float                    f_ref_reg;                      //!< Firing reference
    cc_float                    f_ref_limited;                  //!< Limited firing reference
    cc_float                    f_ref;                          //!< Linearized firing reference
    cc_float                    i_capa;                         //!< Measured partial i_capa (via LPF if measured i_capa is full current)
    cc_float                    i_capa_reg;                     //!< i_capa or i_capa_sim, according to i_capa_source
};

// Voltage regulation functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the voltage regulation and filter damping parameters.
//!
//! This is a background function.
//!
//! @param[out] pars                  Pointer to voltage regulation parameters structure.
//! @param[in]  k_int                 Voltage regulation integrator gain.
//! @param[in]  k_u                   Damping feedback v_meas gain.
//! @param[in]  k_d                   Damping feedback i_meas gain.
//! @param[in]  inv_load_ohms_dc      1 / Load_resistance for DC (gain2).

void regVregInit(struct REG_v_pars * pars,
                 cc_float            k_int,
                 cc_float            k_u,
                 cc_float            k_d,
                 cc_float            inv_load_ohms_dc);


//! Initialize the firing linearisation look-up table (LUT)
//!
//! Linearisation is enabled if actuation is FIRING_REF and any values in the LUT are non-zero.
//! If this is called then regVregLimitsInit is always called afterwards.
//!
//! This is a background function.
//!
//! @param[out] pars                  Pointer to voltage regulation parameters structure.
//! @param[in]  actuation             Actuation.
//! @param[in]  firing_lut            Pointer to the firing look-up table.

void regVregFiringLutInit(struct REG_v_pars * pars,
                          enum REG_actuation  actuation,
                          cc_float          * firing_lut);


//! Initialize the voltage regulation limits and VS gain parameters.
//!
//! If regVregFiringLutInit is called then this is always called afterwards.
//!
//! @param[out] pars                  Pointer to voltage regulation parameters structure.
//! @param[in]  actuation             Actuation.
//! @param[in]  firing_mode           Number of voltage sources in series.
//! @param[in]  num_vsources          Number of voltage sources in series.
//! @param[in]  vs_gain               Voltage source analog gain (volts per DAC volt).
//! @param[in]  v_pos                 Positive voltage reference limit.
//! @param[in]  v_neg                 Negative voltage reference limit.

 void regVregLimitsInit(struct REG_v_pars  * pars,
                        enum REG_actuation   actuation,
                        enum REG_firing_mode firing_mode,
                        uint32_t             num_vsources,
                        cc_float             vs_gain,
                        cc_float             v_pos,
                        cc_float             v_neg);


 //! Linearization function for ASAD thyristor converter firing
 //!
 //! This is a real-time function. It uses linear interpolation of a look-up table with
 //! non-equally spaced points. The points are spaced with a separation given by a cosine
 //! to match the standard cosine non-linear behaviour. This results in a LUT with
 //! uniform steps.
 //!
 //! @param[out] pars                  Pointer to voltage regulation parameters structure.
 //! @param[in]  f_ref_limited         Limited firing reference.

 //! @returns    firing reference after linearization

cc_float regVregLinearizeRT(struct REG_v_pars * pars,
                            cc_float            f_ref_limited);

#ifdef __cplusplus
}
#endif

// EOF
