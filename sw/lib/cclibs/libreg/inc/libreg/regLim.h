//! @file  regLim.h
//! @brief Converter Control Regulation library limit functions for field/current/voltage reference and field/current measurement
//!
//! The limits support includes three types of limits relevant to voltage source controls:
//!
//! * Field/Current measurement limits (trip)
//! * Field/Current reference limits (clip)
//! * Voltage reference limits (clip)
//!
//! Voltage reference limits for some 4-quadrant converters need to protect against
//! excessive power losses in the output stage when ramping down the current. This
//! can be done by defining an exclusion zone for positive voltages in quadrants 4
//! and 1. The software will rotate the zone by 180 degrees to
//! define the exclusion zone for negative voltages in quadrants 3 and 2. For example:
//!
//! \image html  4QuadrantLimits.png
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REG_LIM_CLIP                0.001F          //!< Clip limit shift factor
#define REG_LIM_TRIP                0.01F           //!< Trip limit shift factor
#define REG_LIM_OPENLOOP_HYSTERESIS 0.9F            //!< Hysteresis factor from openloop as a proportion of closeloop threshold
#define REG_LIM_MEAS_HYSTERESIS     2.0F            //!< Zero and Low limits hysteresis factor
#define REG_LIM_RMS_HYSTERESIS      0.9F            //!< RMS warning limit hysteresis factor
#define REG_LIM_FP32_MARGIN         2.0E-07F        //!< Margin on the relative precision of 32-bit floats

// Limits structures

//! Measurement limits

struct REG_lim_meas
{
    bool          * invert_limits;                  //!< Flag to invert limits before use.
    cc_float        pos_trip;                       //!< Positive measurement trip limit
    cc_float        neg_trip;                       //!< Negative measurement trip limit
    cc_float        low;                            //!< Low measurement threshold
    cc_float        zero;                           //!< Zero measurement threshold
    cc_float        low_hysteresis;                 //!< Low measurement threshold with hysteresis
    cc_float        zero_hysteresis;                //!< Zero measurement threshold with hysteresis

    struct
    {
        bool        enabled;                        //!< Set if measurement limits are enabled (true if pos_lim > 0.0 || neg_lim < 0.0)
        bool        trip;                           //!< Set if measurement exceeds reg_lim_meas::pos_trip or reg_lim_meas::neg_trip limits
        bool        low;                            //!< Set if absolute measurement is below reg_lim_meas::low
        bool        zero;                           //!< Set if absolute measurement is below reg_lim_meas::zero
    } flags;                                        //!< Measurement limit flags
};

//! Generic reference limits - used for voltage, current and field references

struct REG_lim_ref
{
    bool          * invert_limits;                  //!< Pointer to flag to invert limits before use.
    cc_float        pos;                            //!< User's positive limit
    cc_float        standby;                        //!< User's standby limit
    cc_float        neg;                            //!< User's negative limit
    cc_float        rate;                           //!< User's rate limit

    cc_float        max_clip;                       //!< Maximum reference clip limit from reg_lim_ref::max_clip_user or Q41 limit
    cc_float        min_clip;                       //!< Minimum reference clip limit from reg_lim_ref::min_clip_user or Q41 limit
    cc_float        rate_clip;                      //!< Absolute reference rate clip limit

    cc_float        closeloop;                      //!< Closeloop threshold (0 for bipolar reference)
    cc_float        openloop;                       //!< Openloop threshold (90% of closeloop - this hysteresis is to avoid toggling)

    struct
    {
        bool        unipolar;                       //!< Unipolar flag
        bool        clipped;                        //!< Set if reference has been clipped to range
                                                    //!< [reg_lim_ref::min_clip,reg_lim_ref::max_clip]
        bool        rate_clipped;                   //!< Set if reference rate has been clipped to range
                                                    //!< [- reg_lim_ref::rate_clip,reg_lim_ref::rate_clip]
    } flags;                                        //!< Reference limit flags
};

//! Specific voltage reference limits - voltage limit depends upon the current

struct REG_lim_v_ref
{
    cc_float        max_clip;                       //!< Maximum reference clip limit from user or power limits
    cc_float        min_clip;                       //!< Minimum reference clip limit from user or power limits

    cc_float        max_clip_user;                  //!< Maximum reference clip limit from user
    cc_float        min_clip_user;                  //!< Minimum reference clip limit from user

    cc_float        max_clip_power;                 //!< Maximum voltage clip limit due to power limits
    cc_float        min_clip_power;                 //!< Minimum voltage clip limit due to power limits

    cc_float        i_quadrants41_max;              //!< Quadrants 41 max current. At least a 1A spread is needed to
                                                    //!< activate the Q41 limiter. Disable by setting to -1.0E10.
    cc_float        v0;                             //!< Voltage limit for zero measured current
    cc_float        dvdi;                           //!< Voltage limit slope with measured current

    cc_float        p_pos;                          //!< Positive power limit
    cc_float        p_neg;                          //!< Negative power limit
};

//! RMS limits

struct REG_lim_rms
{
    cc_float        input2_filter;                  //!< Filtered square of the input value
    cc_float        filter_factor;                  //!< First-order filter factor for square of input value
    cc_float        rms;                            //!< Root of input2_filter
    cc_float        fault;                          //!< RMS fault threshold
    cc_float        warning;                        //!< RMS warning threshold
    cc_float        warning_hysteresis;             //!< RMS warning threshold reduced by hysteresis factor

    struct
    {
        bool        fault;                          //!< True if RMS fault limit exceeded
        bool        warning;                        //!< True if RMS warning limit exceeded
    } flags;                                        //!< RMS limits flags
};

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize measurement limits structure.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  lim_meas         Measurement limits object to initialize.
//! @param[in]   invert_limits    Pointer to flag that indicates if limits should be inverted due to the polarity switch.
//! @param[in]   pos_lim          Positive measurement trip limit. Will be scaled by (1+#REG_LIM_TRIP).
//! @param[in]   neg_lim          Negative measurement trip limit. Will be scaled by (1+#REG_LIM_TRIP).
//! @param[in]   low_lim          Low measurement threshold.
//! @param[in]   zero_lim         Zero measurement threshold.

void regLimMeasInit(struct REG_lim_meas * lim_meas,
                    bool                * invert_limits,
                    cc_float              pos_lim,
                    cc_float              neg_lim,
                    cc_float              low_lim,
                    cc_float              zero_lim);


//! Initialize RMS limits structure
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  lim_rms      RMS limits object to initialize.
//! @param[in]   rms_warning  RMS warning threshold.
//! @param[in]   rms_fault    RMS fault threshold.
//! @param[in]   rms_tc       First order filter time constant.
//! @param[in]   iter_period  Iteration period.

void regLimRmsInit(struct REG_lim_rms * lim_rms,
                   cc_float             rms_warning,
                   cc_float             rms_fault,
                   cc_float             rms_tc,
                   cc_float             iter_period);


//! Initialize generic field/current/voltage reference limits.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  lim_ref      Reference limits object to initialize
//! @param[in]   invert_limits    Pointer to flag that indicates if limits should be inverted due to the polarity switch.
//! @param[in]   pos_lim      Maximum reference clip limit. Will be scaled by (1+#REG_LIM_CLIP).
//! @param[in]   standby_lim  Standby reference limit.
//! @param[in]   neg_lim      Minimum reference clip limit. Will be scaled by (1+#REG_LIM_CLIP).
//! @param[in]   rate_lim     Absolute reference rate clip limit. Will be scaled by (1+#REG_LIM_CLIP).
//! @param[in]   closeloop    Closeloop threshold. Must be positive (zero in the case of 4-quadrant converters).

void regLimRefInit(struct REG_lim_ref * lim_ref,
                   bool               * invert_limits,
                   cc_float             pos_lim,
                   cc_float             standby_lim,
                   cc_float             neg_lim,
                   cc_float             rate_lim,
                   cc_float             closeloop);


//! Initialize voltage reference limits. Voltage reference limits use the same
//! structure as field/current limits but have different behavior.
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  lim_ref        Generic reference limits object to initialize
//! @param[out]  lim_v_ref      Specific voltage reference limits object to initialize
//! @param[in]   i_quadrants41  Define exclusion zone in quadrants 4 and 1 (I dimension).
//! @param[in]   v_quadrants41  Define exclusion zone in quadrants 4 and 1 (V dimension).
//! @param[in]   p_pos          Maximum positive power.
//! @param[in]   p_neg          Maximum negative power.

void regLimVrefInit(struct REG_lim_ref   * lim_ref,
                    struct REG_lim_v_ref * lim_v_ref,
                    const cc_float         i_quadrants41[2],
                    const cc_float         v_quadrants41[2],
                          cc_float         p_pos,
                          cc_float         p_neg);


//! Check the measurement against the trip levels and the absolute measurement against
//! the low and zero limits with hysteresis to avoid toggling. The flags in reg_lim_meas::flags
//! are set according to the result of the checks. This function calls regLimVrefCalcRT().
//!
//! This is a Real-Time function.
//!
//! @param[in,out]  lim_meas  Measurement limits object to check against
//! @param[in]      meas      Measurement value

void regLimMeasRT(struct REG_lim_meas * lim_meas, cc_float meas);


//! Check RMS value against limits.
//!
//! This is a Real-Time function.
//!
//! @param[in,out]  lim_rms  Pointer to RMS limits structure
//! @param[in]      input    Input value

void regLimRmsRT(struct REG_lim_rms * lim_rms, cc_float input);


//! Use the measured current and previous voltage reference to calculate the voltage limits
//! for the next regulation period, to respect the power limits.
//!
//! This is a Real-Time function.
//!
//! @param[in,out]  lim_ref              Generic reference limits object to initialize
//! @param[in]      lim_v_ref            Specific voltage reference limits object
//! @param[in]      i_meas               Measured current

void regLimVrefPowerRT(struct REG_lim_ref   * lim_ref,
                       struct REG_lim_v_ref * lim_v_ref,
                       cc_float               i_meas);


//! Reset the RMS limits structure
//!
//! @param[out]  lim_rms      RMS limits object to initialize.

void regLimRmsResetRT(struct REG_lim_rms * lim_rms);


//! Use the measured current to work out the voltage limits based on the operating
//! zone for the voltage source. The user defines the exclusion zone for positive
//! voltages (quadrants 4-1) in regLimVrefInit(). This function rotates this
//! zone to calculate the exclusion zone for negative voltages (quadrants 3-2).
//!
//! This is a Real-Time function.
//!
//! @param[in,out]  lim_ref         Generic reference limits object to initialize
//! @param[in]      lim_v_ref       Specific voltage reference limits object
//! @param[in]      i_meas          Measured current
//! @param[in]      circuit_open    True if the circuit is open and the clip limits show be zero

void regLimVrefCalcRT(      struct REG_lim_ref   * lim_ref,
                      const struct REG_lim_v_ref * lim_v_ref,
                            cc_float               i_meas,
                            bool                   circuit_open);


//! Apply clip and rate limits to the field, current or voltage reference. The appropriate
//! flags in reg_lim_ref::flags are set if clip or rate limits are active.
//!
//! This is a Real-Time function.
//!
//! @param[in,out]  lim_ref      Reference limits object to update
//! @param[in]      period       Iteration period, used to compute rate limit
//! @param[in]      ref          Reference value to check
//! @param[in]      prev_ref     Previous reference, used to calculate change of reference value
//! @param[in]      limit_rate   If true then apply rate of change limit, otherwise just the clip limits

cc_float regLimRefRT(struct REG_lim_ref * lim_ref,
                     cc_float             period,
                     cc_float             ref,
                     cc_float             prev_ref,
                     bool                 limit_rate);

#ifdef __cplusplus
}
#endif

// EOF
