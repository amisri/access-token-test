//! @file  regSim.h
//! @brief Converter Control Regulation library voltage source and load simulator functions
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "regLoad.h"

// Simulator structures

//! Voltage source and load simulators
//!
//! Libreg supports the control of either a voltage source (VS ACTUATION is VOLTAGE_REF) or a
//! firing card of a thyristor converter (VS ACTUATION is FIRING_REF). It has two different
//! voltage and load simulators, known as Simulator 1 and Simualtor 2.
//!
//! Simulator 1
//!
//! This simulator always runs, independent of the VS ACTUATION, however, its simulated
//! signals are only used when VS ACTUATION is VOLTAGE_REF. With this simulator, the
//! voltage source and load models are independent. The voltage source model is a
//! third order digital filter that can be defined by the numerator and denominator coefficients, or
//! using the in-built Tustin algorithm that will derive a second order model from the bandwidth
//! and damping parameters.
//!
//! The load model uses an integrator and tries to handle both well sampled and undersampled loads.
//!
//! Simulator 2
//!
//! This simulator only runs when VS ACTUATION is FIRING_REF.

//! Simulator 1 - Voltage source simulation parameters and variables stuctures

struct REG_sim_vs_pars
{

    cc_float                    num[REG_SIM_NUM_DEN_LEN];       //!< Numerator coefficients b0, b1, b2, etc.
    cc_float                    den[REG_SIM_NUM_DEN_LEN];       //!< Denominator coefficients a0, a2, a2, etc.
    cc_float                    rsp_delay_iters;                //!< Voltage source response delay for steady actuation ramp.
    cc_float                    gain;                           //!< gain = sum(den) / sum(num).
    bool                        is_vs_undersampled;             //!< Simulated voltage source is under-sampled flag.
};

struct REG_sim_vs_vars
{
    cc_float                    circuit_voltage;                //!< Circuit voltage       (without VS ACT_DELAY_ITERS).
    cc_float                    act[REG_SIM_NUM_DEN_LEN];       //!< Actuation history circular buffer.
    cc_float                    rsp[REG_SIM_NUM_DEN_LEN];       //!< Voltage source response history circular buffer (excluding VS ACT_DELAY_ITERS).
    uint32_t                    buf_index;                      //!< Free running index to most recent sample in the circular buffer
};

//! Simulator 1 - Load simulation parameters and variables structures

struct REG_sim_load_pars
{
    cc_float                    period;                         //!< Simulation period
    cc_float                    tc_error;                       //!< Simulated load time constant error
    cc_float                    period_tc_ratio;                //!< Simulation period / load time constant
    bool                        is_load_undersampled;           //!< Simulated load is under-sampled flag
    struct REG_load_pars        pars;                           //!< Simulated load parameters
    cc_float                    inv_load_ohms;                  //!< 1/Rload
    cc_float                    inv_filter_ohms;                //!< 1/Rfilter
};

struct REG_sim_load_vars
{
    cc_float                    circuit_voltage;                //!< Previous circuit voltage (for iterpolation)
    cc_float                    circuit_current;                //!< Circuit current       (without VS ACT_DELAY_ITERS).
    cc_float                    magnet_current;                 //!< Magnet current        (without VS ACT_DELAY_ITERS).
    cc_double                   integrator;                     //!< Integrator for simulated load current model
};


#ifdef __cplusplus
extern "C" {
#endif

// Simulator 1 functions

//! Initialize voltage source model.
//! This function sets or clears reg_sim_vs_pars.is_vs_undersampled flag.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in,out] pars                 Pointer to voltage source simulation parameters.
//! @param[in]     iter_period          Simulation iteration period in seconds.
//! @param[in]     bandwidth            Second order model bandwidth (-3 dB). Set to zero to use num,den.
//! @param[in]     z                    Second order model damping.
//! @param[in]     tau_zero             Second order zero time constant. Set to 0 if not required.
//! @param[in]     num                  Third order model numerator coefficients (used if bandwidth is zero).
//! @param[in]     den                  Third order model denominator coefficients (used if bandwidth is zero).

void regSimVsInit(struct REG_sim_vs_pars * pars,
                  cc_float                 iter_period,
                  cc_float                 bandwidth,
                  cc_float                 z,
                  cc_float                 tau_zero,
                  cc_float           const num[REG_SIM_NUM_DEN_LEN],
                  cc_float           const den[REG_SIM_NUM_DEN_LEN]);


//! Initialize the voltage source simulation history to be in steady-state with the given initial response.
//!
//! <strong>Note:</strong> the model's gain must be calculated (using regSimVsInit()) before calling this function.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in]     pars                 Pointer to voltage source simulation parameters.
//! @param[out]    vars                 Pointer to voltage source simulation variables.
//! @param[in]     init_rsp             Initial voltage source model response.
//! @return        Steady-state actuation that will produce the supplied response.

cc_float regSimVsInitHistory(struct REG_sim_vs_pars const * pars,
                             struct REG_sim_vs_vars       * vars,
                             cc_float                       init_rsp);


//! Initialize the load simulation parameters structure.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    sim_load_pars        Load simulation parameters object to update
//! @param[in]     load_pars            Load parameters
//! @param[in]     sim_load_tc_error    Simulation load time constant error. If the Tc
//!                                     error is zero, the simulation will simply use load_pars.
//!                                     Otherwise it will initialize the simulated load using
//!                                     distorted load_pars so that the simulated load time
//!                                     constant will mismatch the load_pars time constants by
//!                                     the required factor (i.e. 0.1 = 10% error in Tc).
//! @param[in]     sim_period           Simulation period

void regSimLoadInit(struct REG_sim_load_pars   * sim_load_pars,
                    struct REG_load_pars const * load_pars,
                    cc_float                     sim_load_tc_error,
                    cc_float                     sim_period);


//! Simulator 1 - Voltage source response to the voltage reference
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars                 Pointer to voltage source simulation parameters.
//! @param[in,out] vars                 Pointer to voltage source simulation variables.
//! @param[in]     act                  Actuation (voltage or current reference).
//! @return        Load voltage or current (according to VS ACTUATION)

cc_float regSimVsRT(struct REG_sim_vs_pars const * pars,
                    struct REG_sim_vs_vars       * vars,
                    cc_float                       act);


//! Simulator 1 - Load response to simulated circuit voltage
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars                 Load simulation parameters.
//! @param[in,out] vars                 Load simulation variables.
//! @param[in]     is_vs_undersampled   Voltage Source undersampled flag. If false, use first-order interpolation
//!                                     of voltage. If true, voltage source is undersampled: use final voltage for complete sample.
//! @param[in]     v_circuit            Circuit voltage
//! @param[in]     i_mag_sat            Magnet current causing magnet saturation.

void regSimLoadRT(struct REG_sim_load_pars const * pars,
                  struct REG_sim_load_vars       * vars,
                  bool                             is_vs_undersampled,
                  cc_float                         v_circuit,
                  cc_float                         i_mag_sat);


#ifdef __cplusplus
}
#endif

// EOF
