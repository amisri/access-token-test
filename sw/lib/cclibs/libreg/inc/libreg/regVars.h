//! @file  inc/libreg/regVars.h
//!
//! @brief Converter Control Regulation library : Variables header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Use regMgrVarValue() and regMgrVarPointer() with a pointer to the reg_mgr structure.

#define regMgrVarValue(REG_MGR, VAR_NAME)    ((REG_MGR)->REG_VAR_ ## VAR_NAME)
#define regMgrVarPointer(REG_MGR, VAR_NAME)  (&(REG_MGR)->REG_VAR_ ## VAR_NAME)

#define REG_VAR_ITER_PERIOD                    iter_period_fp32                         // cc_float            Iteration (measurement) period as a float in seconds
#define REG_VAR_REF_ADVANCE_NS                 ref_advance_ns                           // int32_t             Currently active reference advance in nanoseconds
#define REG_VAR_REF_ADVANCE                    ref_advance                              // cc_float            Currently active reference advance in seconds
#define REG_VAR_REF_RATE                       ref_rate                                 // cc_float            Rate of change for active reference (field current or voltage)
#define REG_VAR_REF_ACTUATION                  ref_actuation                            // cc_float            Actuation (current
#define REG_VAR_REG_PERIOD                     reg_period                               // cc_float            Regulation period (field or current if active) in seconds
#define REG_VAR_REG_MODE                       reg_mode                                 // enum REG_mode       Regulation mode
#define REG_VAR_REG_RST_SOURCE                 reg_rst_source                           // enum REG_rst_source RST parameters source (operation or test)
#define REG_VAR_REG_ABS_ERR                    abs_reg_err                              // cc_float            Actual absolute regulation error (field
#define REG_VAR_REG_MAX_ABS_ERR                max_abs_reg_err                          // cc_float            Maximum absolute regulation error (field
#define REG_VAR_REG_MILLI_REG_ERR              milli_reg_err                            // int16_t             Regulation error times 1000 clipped to signed 16-bit range
#define REG_VAR_REG_MILLI_RMS_ERR              milli_rms_err                            // int16_t             RMS regulation error times 1000 clipped to signed 16-bit range
#define REG_VAR_BREG_ITER_INDEX                b.iteration_index                        // uint32_t            Field regulation iteration index (0=RST algorithm will start to run)
#define REG_VAR_BREG_PERIOD_ITERS              b.reg_period_iters                       // uint32_t            Field regulation period in iterations
#define REG_VAR_BREG_PERIOD_NS                 b.reg_period_ns                          // int32_t             Field regulation period in nanoseconds
#define REG_VAR_BREG_PERIOD                    b.reg_period                             // cc_float            Field regulation period in seconds
#define REG_VAR_BREG_FREQUENCY                 b.inv_reg_period                         // cc_float            Field regulation frequency in Hz
#define REG_VAR_BREG_LOG_TIME_OFFSET           b.log_time_offset                        // cc_float            Field regulation log time offset for signals calculated after the decoupling
#define REG_VAR_B_REF_ADV                      b.ref_adv                                // cc_float            Field reference advanced by ref_advance
#define REG_VAR_B_REF_LIMITED                  b.ref_limited                            // cc_float            Advanced field reference after limits
#define REG_VAR_B_REF_CLOSED                   b.ref_closed                             // cc_float            Back-calculated closed-loop field reference saved in history
#define REG_VAR_B_REF_OPEN                     b.ref_open                               // cc_float            Back-calculated open-loop field reference saved in history
#define REG_VAR_B_REF_DELAYED                  b.ref_delayed                            // cc_float            B REF_LIMITED delayed for regulation error calculation
#define REG_VAR_B_REF_RATE                     b.rst_vars.ref_rate                      // cc_float            Rate of change of field reference
#define REG_VAR_B_MEAS_REG                     b.meas.reg                               // cc_float            Field measurement used for regulation
#define REG_VAR_B_ERR                          b.reg_err.err                            // cc_float            Field regulation error
#define REG_VAR_B_RMS_ERR                      b.reg_err.rms_err                        // cc_float            RMS field regulation error
#define REG_VAR_B_MAX_ABS_ERR                  b.reg_err.max_abs_err                    // cc_float            Maximum absolute field regulation error
#define REG_VAR_B_TRACK_DELAY_PERIODS          b.track_delay_periods                    // cc_float            Measured field regulation track delay in regulation periods
#define REG_VAR_BREG_OP_STATUS                 b.last_op_rst_pars.status                // enum REG_status     Status of last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_REF_ADVANCE            b.last_op_rst_pars.ref_advance           // cc_float            Reference advance from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_MEAS_SELECT            b.last_op_rst_pars.reg_meas_select       // cc_float            Regulation measurement from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_PURE_DELAY_PERIODS     b.last_op_rst_pars.pure_delay_periods    // cc_float            Pure delay from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_TRACK_DELAY_PERIODS    b.last_op_rst_pars.track_delay_periods   // cc_float            Track delay from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_REF_DELAY_PERIODS      b.last_op_rst_pars.ref_delay_periods     // cc_float            Reference delay from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_MOD_MARGIN             b.last_op_rst_pars.modulus_margin        // cc_float            Modulus margin from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_MOD_MARGIN_HZ          b.last_op_rst_pars.modulus_margin_hz     // cc_float            Modulus margin frequency from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_RST_ORDER              b.last_op_rst_pars.rst_order             // uint32_t            Order of RST from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_R                      b.last_op_rst_pars.rst.r                 // cc_float *          R polynomial from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_S                      b.last_op_rst_pars.rst.s                 // cc_float *          S polynomial from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_T                      b.last_op_rst_pars.rst.t                 // cc_float *          T polynomial from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_A                      b.last_op_rst_pars.a                     // cc_float *          Plant numerator A from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_B                      b.last_op_rst_pars.b                     // cc_float *          Plant denominator B from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_AS                     b.last_op_rst_pars.as                    // cc_float *          A.S. from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_ASBR                   b.last_op_rst_pars.asbr                  // cc_float *          A.S. + B.R. from last attempt to update the operational field regulation RST coefficients
#define REG_VAR_BREG_OP_ALG_INDEX              b.last_op_rst_pars.alg_index             // uint32_t            Index (1-5) of the algorithm used during last attempt to update the operational field regulation RST coefficients (based on pure delay).
#define REG_VAR_BREG_TEST_STATUS               b.last_test_rst_pars.status              // enum REG_status     Status of last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_REF_ADVANCE          b.last_test_rst_pars.ref_advance         // cc_float            Reference advance from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_MEAS_SELECT          b.last_test_rst_pars.reg_meas_select     // cc_float            Regulation measurement from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_SMOOTH_ACTUATION     b.last_test_rst_pars.smooth_actuation    // cc_float            Smooth actuation control from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_PURE_DELAY_PERIODS   b.last_test_rst_pars.pure_delay_periods  // cc_float            Pure delay from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_TRACK_DELAY_PERIODS  b.last_test_rst_pars.track_delay_periods // cc_float            Track delay from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_REF_DELAY_PERIODS    b.last_test_rst_pars.ref_delay_periods   // cc_float            Reference delay from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_MOD_MARGIN           b.last_test_rst_pars.modulus_margin      // cc_float            Modulus margin from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_MOD_MARGIN_HZ        b.last_test_rst_pars.modulus_margin_hz   // cc_float            Modulus margin frequency from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_RST_ORDER            b.last_test_rst_pars.rst_order           // uint32_t            Order of RST from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_R                    b.last_test_rst_pars.rst.r               // cc_float *          R polynomial from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_S                    b.last_test_rst_pars.rst.s               // cc_float *          S polynomial from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_T                    b.last_test_rst_pars.rst.t               // cc_float *          T polynomial from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_A                    b.last_test_rst_pars.a                   // cc_float *          Plant numerator A from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_B                    b.last_test_rst_pars.b                   // cc_float *          Plant denominator B from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_AS                   b.last_test_rst_pars.as                  // cc_float *          A.S. from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_ASBR                 b.last_test_rst_pars.asbr                // cc_float *          A.S. + B.R. from last attempt to update the test field regulation RST coefficients
#define REG_VAR_BREG_TEST_ALG_INDEX            b.last_test_rst_pars.alg_index           // uint32_t            Index (1-5) of the algorithm used during last attempt to update the test field regulation RST coefficients (based on pure delay).
#define REG_VAR_IREG_ITER_INDEX                i.iteration_index                        // uint32_t            Current regulation iteration index (0=RST algorithm will start to run)
#define REG_VAR_IREG_PERIOD_ITERS              i.reg_period_iters                       // uint32_t            Current regulation period in iterations
#define REG_VAR_IREG_PERIOD_NS                 i.reg_period_ns                          // int32_t             Current regulation period in nanoseconds
#define REG_VAR_IREG_PERIOD                    i.reg_period                             // cc_float            Current regulation period in seconds
#define REG_VAR_IREG_FREQUENCY                 i.inv_reg_period                         // cc_float            Current regulation frequency in Hz
#define REG_VAR_IREG_LOG_TIME_OFFSET           i.log_time_offset                        // cc_float            Current regulation log time offset for signals calculated after the decoupling
#define REG_VAR_I_REF_ADV                      i.ref_adv                                // cc_float            Current reference advanced by ref_advance
#define REG_VAR_I_REF_LIMITED                  i.ref_limited                            // cc_float            Advanced current reference after limits
#define REG_VAR_I_REF_CLOSED                   i.ref_closed                             // cc_float            Back-calculated closed-loop current reference saved in history
#define REG_VAR_I_REF_OPEN                     i.ref_open                               // cc_float            Back-calculated open-loop current reference saved in history
#define REG_VAR_I_REF_DELAYED                  i.ref_delayed                            // cc_float            I REF_LIMITED delayed for regulation error calculation
#define REG_VAR_I_REF_RATE                     i.rst_vars.ref_rate                      // cc_float            Rate of change of current reference
#define REG_VAR_I_MEAS_REG                     i.meas.reg                               // cc_float            Current measurement used for regulation
#define REG_VAR_I_ERR                          i.reg_err.err                            // cc_float            Current regulation error
#define REG_VAR_I_MAX_ABS_ERR                  i.reg_err.max_abs_err                    // cc_float            Maximum absolute current regulation error
#define REG_VAR_I_RMS_ERR                      i.reg_err.rms_err                        // cc_float            RMS current regulation error
#define REG_VAR_I_TRACK_DELAY_PERIODS          i.track_delay_periods                    // cc_float            Measured current regulation track delay in regulation periods
#define REG_VAR_I_RMS                          lim_i_rms.rms                            // cc_float            Root filtered squared current measurement
#define REG_VAR_I_RMS_LOAD                     lim_i_rms_load.rms                       // cc_float            Root filtered squared load current measurement
#define REG_VAR_IREG_OP_STATUS                 i.last_op_rst_pars.status                // enum REG_status     Status of last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_REF_ADVANCE            i.last_op_rst_pars.ref_advance           // cc_float            Reference advance from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_MEAS_SELECT            i.last_op_rst_pars.reg_meas_select       // cc_float            Regulation measurement from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_PURE_DELAY_PERIODS     i.last_op_rst_pars.pure_delay_periods    // cc_float            Pure delay from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_TRACK_DELAY_PERIODS    i.last_op_rst_pars.track_delay_periods   // cc_float            Track delay from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_REF_DELAY_PERIODS      i.last_op_rst_pars.ref_delay_periods     // cc_float            Reference delay from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_MOD_MARGIN             i.last_op_rst_pars.modulus_margin        // cc_float            Modulus margin from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_MOD_MARGIN_HZ          i.last_op_rst_pars.modulus_margin_hz     // cc_float            Modulus margin frequency from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_RST_ORDER              i.last_op_rst_pars.rst_order             // uint32_t            Order of RST from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_R                      i.last_op_rst_pars.rst.r                 // cc_float *          R polynomial from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_S                      i.last_op_rst_pars.rst.s                 // cc_float *          S polynomial from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_T                      i.last_op_rst_pars.rst.t                 // cc_float *          T polynomial from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_A                      i.last_op_rst_pars.a                     // cc_float *          Plant numerator A from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_B                      i.last_op_rst_pars.b                     // cc_float *          Plant denominator B from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_AS                     i.last_op_rst_pars.as                    // cc_float *          A.S. from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_ASBR                   i.last_op_rst_pars.asbr                  // cc_float *          A.S. + B.R. from last attempt to update the operational current regulation RST coefficients
#define REG_VAR_IREG_OP_ALG_INDEX              i.last_op_rst_pars.alg_index             // uint32_t            Index (1-5) of the algorithm used during last attempt to update the operational current regulation RST coefficients (based on pure delay).
#define REG_VAR_IREG_TEST_STATUS               i.last_test_rst_pars.status              // enum REG_status     Status of last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_REF_ADVANCE          i.last_test_rst_pars.ref_advance         // cc_float            Reference advance from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_MEAS_SELECT          i.last_test_rst_pars.reg_meas_select     // cc_float            Regulation measurement from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_SMOOTH_ACTUATION     i.last_test_rst_pars.smooth_actuation    // cc_float            Smooth actuation control from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_PURE_DELAY_PERIODS   i.last_test_rst_pars.pure_delay_periods  // cc_float            Pure delay from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_TRACK_DELAY_PERIODS  i.last_test_rst_pars.track_delay_periods // cc_float            Track delay from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_REF_DELAY_PERIODS    i.last_test_rst_pars.ref_delay_periods   // cc_float            Reference delay from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_MOD_MARGIN           i.last_test_rst_pars.modulus_margin      // cc_float            Modulus margin from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_MOD_MARGIN_HZ        i.last_test_rst_pars.modulus_margin_hz   // cc_float            Modulus margin frequency from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_RST_ORDER            i.last_test_rst_pars.rst_order           // uint32_t            Order of RST from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_R                    i.last_test_rst_pars.rst.r               // cc_float *          R polynomial from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_S                    i.last_test_rst_pars.rst.s               // cc_float *          S polynomial from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_T                    i.last_test_rst_pars.rst.t               // cc_float *          T polynomial from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_A                    i.last_test_rst_pars.a                   // cc_float *          Plant numerator A from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_B                    i.last_test_rst_pars.b                   // cc_float *          Plant denominator B from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_AS                   i.last_test_rst_pars.as                  // cc_float *          A.S. from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_ASBR                 i.last_test_rst_pars.asbr                // cc_float *          A.S. + B.R. from last attempt to update the test current regulation RST coefficients
#define REG_VAR_IREG_TEST_ALG_INDEX            i.last_test_rst_pars.alg_index           // uint32_t            Index (1-5) of the algorithm used during last attempt to update the test current regulation RST coefficients (based on pure delay).
#define REG_VAR_V_REF_REG                      v.ref_reg                                // cc_float            Voltage reference from regulator or function generator
#define REG_VAR_V_REF_SAT                      v.ref_sat                                // cc_float            Voltage reference with magnet saturation compensation
#define REG_VAR_V_REF_FF                       v.ref_ff                                 // cc_float            Voltage reference after saturation and feed forward
#define REG_VAR_V_FF                           inputs.v_ff.meas.signal                  // cc_float            Feed forward voltage reference
#define REG_VAR_V_FF_FLTR                      v.v_ff_fltr.signal                       // cc_float            Filtered feed forward voltage reference
#define REG_VAR_V_REF_DECO                     v.ref_deco                               // cc_float            Voltage reference after saturation
#define REG_VAR_V_REF                          v.ref                                    // cc_float            Voltage reference after saturation
#define REG_VAR_V_HARMONICS                    v.harmonics.ref                          // cc_float            Feed forward voltage reference from harmonics generator
#define REG_VAR_V_REF_VS                       v.ref_vs                                 // cc_float            Voltage reference after saturation
#define REG_VAR_V_REF_RATE                     v.ref_rate                               // cc_float            Rate of V REF
#define REG_VAR_V_ERR                          v.reg_err.err                            // cc_float            Voltage error (v_meas-v_meas_sim)
#define REG_VAR_V_MAX_ABS_ERR                  v.reg_err.max_abs_err                    // cc_float            Maximum absolute voltage regulation error
#define REG_VAR_V_RATE_RMS                     lim_v_rate_rms.rms                       // cc_float            Root filtered squared voltage rate
#define REG_VAR_V_AC_HZ_VALID                  inputs.v_ac.meas.is_valid                // bool                V_AC frequency measurement is valid
#define REG_VAR_DECO_PHASE                     deco.new_phase                           // uint32_t            Decoupling phase
#define REG_VAR_VREG_V_INTEGRATOR              v.reg_vars.v_integrator                  // cc_float            Voltage regulator integrator
#define REG_VAR_VREG_V_REG_ERR                 v.reg_vars.v_reg_err                     // cc_float            Voltage regulator error
#define REG_VAR_VFILTER_V_MEAS_REG             v.reg_vars.v_meas_reg                    // cc_float            Measured or simulated voltage for voltage source filter damping
#define REG_VAR_VFILTER_D_REF                  v.reg_vars.d_ref                         // cc_float            Voltage source filter damping feedback reference
#define REG_VAR_VFILTER_D_MEAS                 v.reg_vars.d_meas                        // cc_float            Voltage source filter damping feedback measurement
#define REG_VAR_VFILTER_I_CAPA_REG             v.reg_vars.i_capa_reg                    // cc_float            Partial i_capa measurement or simulation for voltage source filter damping
#define REG_VAR_VFILTER_I_CAPA                 v.reg_vars.i_capa                        // cc_float            Voltage source filter damping feedback partial i_capa measurement
#define REG_VAR_FIRING_F_REF_REG               v.reg_vars.f_ref_reg                     // cc_float            Firing reference
#define REG_VAR_FIRING_F_REF_LIMITED           v.reg_vars.f_ref_limited                 // cc_float            Limited firing reference
#define REG_VAR_FIRING_F_REF                   v.reg_vars.f_ref                         // cc_float            Linearized firing reference
#define REG_VAR_VREG_OP_VS_GAIN                v.reg_pars.vs_gain                       // cc_float            Voltage source gain (Vout/Vdac)
#define REG_VAR_VREG_OP_INV_VS_GAIN            v.reg_pars.inv_vs_gain                   // cc_float            Inverse voltage source gain
#define REG_VAR_VREG_OP_K_AW                   v.reg_pars.k_aw                          // cc_float            Integrator anti-wind-up gain
#define REG_VAR_VREG_OP_K_W                    v.reg_pars.k_w                           // cc_float            Voltage source filter input gain
#define REG_VAR_VREG_OP_F_POS                  v.reg_pars.f_pos                         // cc_float            Positive firing limit
#define REG_VAR_SIM_V_CIRCUIT                  sim_vars.vs.circuit_voltage              // cc_float            Simulated voltage across the circuit without delay
#define REG_VAR_SIM_I_CIRCUIT                  sim_vars.load.circuit_current            // cc_float            Simulated current in the circuit without delay
#define REG_VAR_SIM_I_MAGNET                   sim_vars.load.magnet_current             // cc_float            Simulated current in the magnet without delay
#define REG_VAR_SIM2_V_CIRCUIT                 sim_vars.vs_and_load.circuit_voltage     // cc_float            Simulated voltage across the circuit without delay
#define REG_VAR_SIM2_I_CIRCUIT                 sim_vars.vs_and_load.circuit_current     // cc_float            Simulated current in the circuit without delay
#define REG_VAR_SIM2_I_MAGNET                  sim_vars.vs_and_load.magnet_current      // cc_float            Simulated current in the magnet without delay
#define REG_VAR_SIM2_I_CAPA                    sim_vars.vs_and_load.capa_current        // cc_float            Simulated current in filter damped capacitor branch (partial i_capa)
#define REG_VAR_SIM_B_MEAS                     sim_vars.b_meas                          // cc_float            Simulated measurement of field in the magnet
#define REG_VAR_SIM_I_MEAS                     sim_vars.i_meas                          // cc_float            Simulated measurement of the circuit current
#define REG_VAR_SIM_V_MEAS                     sim_vars.v_meas                          // cc_float            Simulated measurement of the voltage across the circuit
#define REG_VAR_SIM_I_CAPA_MEAS                sim_vars.i_capa_meas                     // cc_float            Simulated measurement of the capa current
#define REG_VAR_FLAG_B_UNIPOLAR                b.lim_ref.flags.unipolar                 // bool                True if converter (without polarity switch) can only generate a unipolar field
#define REG_VAR_FLAG_I_UNIPOLAR                i.lim_ref.flags.unipolar                 // bool                True if converter (without polarity switch) can only generate a unipolar current
#define REG_VAR_FLAG_V_UNIPOLAR                v.lim_ref.flags.unipolar                 // bool                True if converter (without polarity switch) can only generate a unipolar voltage
#define REG_VAR_FLAG_SIM_MEAS                  sim_meas                                 // bool                True if libreg is simulating the measurements
#define REG_VAR_FLAG_OPENLOOP                  openloop                                 // bool                True if openloop regulation is active
#define REG_VAR_FLAG_REF_CLIPPED               ref_clipped_flag                         // bool                True if reference is currently being clipped
#define REG_VAR_FLAG_REF_RATE_CLIPPED          ref_rate_clipped_flag                    // bool                True if the rate of change of the reference is being clipped
#define REG_VAR_FLAG_INVERT_LIMITS             invert_limits                            // bool                True if reference limits are currently being inverted because of the polarity switch
#define REG_VAR_FLAG_B_START_REG_ITER          b.start_reg_iter_flag                    // bool                True if iteration_index is zero – regulation will start on this iteration
#define REG_VAR_FLAG_B_END_REG_ITER            b.end_reg_iter_flag                      // bool                True if iteration_index equals the decoupling phase – regulation will end on this iteration
#define REG_VAR_FLAG_B_MEAS_VALID              inputs.b_meas.meas.is_valid              // bool                True if field measurement is valid
#define REG_VAR_FLAG_B_MEAS_FAULT              inputs.b_meas.fault.flag                 // bool                True if field measurement fault is active
#define REG_VAR_FLAG_B_MEAS_LIM_ENABLED        b.lim_meas.flags.enabled                 // bool                True if field measurement limits are enabled
#define REG_VAR_FLAG_B_MEAS_TRIP               b.lim_meas.flags.trip                    // bool                True if field measurement exceeds trip limit
#define REG_VAR_FLAG_B_MEAS_LOW                b.lim_meas.flags.low                     // bool                True if field measurement is below low limit
#define REG_VAR_FLAG_B_MEAS_ZERO               b.lim_meas.flags.zero                    // bool                True if field measurement is below zero limit
#define REG_VAR_FLAG_B_REF_CLIPPED             b.lim_ref.flags.clipped                  // bool                True if field reference is being clipped
#define REG_VAR_FLAG_B_REF_RATE_CLIPPED        b.lim_ref.flags.rate_clipped             // bool                True if rate of change of the field reference is being limited
#define REG_VAR_FLAG_B_ERR_WARNING             b.reg_err.warning.flag                   // bool                True if field regulation error exceeds warning limit
#define REG_VAR_FLAG_B_ERR_FAULT               b.reg_err.fault.flag                     // bool                True if field regulation error exceeds fault limit
#define REG_VAR_FLAG_I_START_REG_ITER          i.start_reg_iter_flag                    // bool                True if iteration_index is zero – regulation will start on this iteration
#define REG_VAR_FLAG_I_END_REG_ITER            i.end_reg_iter_flag                      // bool                True if iteration_index equals the decoupling phase – regulation will end on this iteration
#define REG_VAR_FLAG_I_MEAS_VALID              inputs.i_meas.meas.is_valid              // bool                True if current measurement is valid
#define REG_VAR_FLAG_I_MEAS_FAULT              inputs.i_meas.fault.flag                 // bool                True if current measurement fault is active
#define REG_VAR_FLAG_I_MEAS_LIM_ENABLED        i.lim_meas.flags.enabled                 // bool                True if current measurement limits are enabled
#define REG_VAR_FLAG_I_MEAS_TRIP               i.lim_meas.flags.trip                    // bool                True if current measurement exceeds trip limit
#define REG_VAR_FLAG_I_MEAS_LOW                i.lim_meas.flags.low                     // bool                True if current measurement is below low limit
#define REG_VAR_FLAG_I_MEAS_ZERO               i.lim_meas.flags.zero                    // bool                True if current measurement is below zero limit
#define REG_VAR_FLAG_I_REF_CLIPPED             i.lim_ref.flags.clipped                  // bool                True if current reference is being clipped
#define REG_VAR_FLAG_I_REF_RATE_CLIPPED        i.lim_ref.flags.rate_clipped             // bool                True if rate of change of the current reference is being limited
#define REG_VAR_FLAG_I_ERR_WARNING             i.reg_err.warning.flag                   // bool                True if current regulation error exceeds warning limit
#define REG_VAR_FLAG_I_ERR_FAULT               i.reg_err.fault.flag                     // bool                True if current regulation error exceeds fault limit
#define REG_VAR_FLAG_I_MAG_SAT_VALID           inputs.i_mag_sat.meas.is_valid           // bool                True if I_mag_sat measurement is valid
#define REG_VAR_FLAG_I_MAG_SAT_FAULT           inputs.i_mag_sat.fault.flag              // bool                True if I_mag_sat measurement fault is active
#define REG_VAR_FLAG_I_CAPA_VALID              inputs.i_capa.meas.is_valid              // bool                True if I_capa measurement is valid
#define REG_VAR_FLAG_I_CAPA_FAULT              inputs.i_capa.fault.flag                 // bool                True if I_capa measurement fault is active when required for regulation
#define REG_VAR_FLAG_V_FF_VALID                inputs.v_ff.meas.is_valid                // bool                True if voltage measurement invalid for too many samples
#define REG_VAR_FLAG_V_FF_FAULT                inputs.v_ff.fault.flag                   // bool                True if voltage measurement failed when needed for regulation
#define REG_VAR_FLAG_V_MEAS_VALID              inputs.v_meas.meas.is_valid              // bool                True if voltage measurement invalid for too many samples
#define REG_VAR_FLAG_V_MEAS_FAULT              inputs.v_meas.fault.flag                 // bool                True if voltage measurement failed when needed for regulation
#define REG_VAR_FLAG_V_MEAS_TRIP               v.lim_meas.flags.trip                    // bool                True if current measurement exceeds trip limit
#define REG_VAR_FLAG_V_REF_CLIPPED             v.lim_ref.flags.clipped                  // bool                True if voltage reference is being clipped
#define REG_VAR_FLAG_V_REF_RATE_CLIPPED        v.lim_ref.flags.rate_clipped             // bool                True if rate of change of the voltage reference is being limited
#define REG_VAR_FLAG_V_ERR_WARNING             v.reg_err.warning.flag                   // bool                True if voltage regulation error exceeds warning limit
#define REG_VAR_FLAG_V_ERR_FAULT               v.reg_err.fault.flag                     // bool                True if voltage regulation error exceeds fault limit
#define REG_VAR_FLAG_I_RMS_WARNING             lim_i_rms.flags.warning                  // bool                True if RMS current is above the converter warning limit
#define REG_VAR_FLAG_I_RMS_FAULT               lim_i_rms.flags.fault                    // bool                True if RMS current is above the converter fault limit
#define REG_VAR_FLAG_I_RMS_LOAD_WARNING        lim_i_rms_load.flags.warning             // bool                True if RMS current is above the load warning limit
#define REG_VAR_FLAG_I_RMS_LOAD_FAULT          lim_i_rms_load.flags.fault               // bool                True if RMS current is above the load fault limit
#define REG_VAR_FLAG_V_RATE_RMS_FAULT          lim_v_rate_rms.flags.fault               // bool                True if RMS voltage reference rate is above the fault limit
#define REG_VAR_FLAG_LINEARISATION             v.reg_pars.linearisation_enabled         // bool                True if firing linearisation is enabled
#define REG_VAR_FLAG_ASAD_1Q_WITHOUT_LUT       v.reg_pars.asad_1q_without_lut           // bool                True if firing mode is ASAD for a 1-quadrant converter and without a firing look-up table defined
#define REG_VAR_LIMITS_B_POS                   b.lim_ref.pos                            // cc_float            Operational positive field reference limit
#define REG_VAR_LIMITS_B_POS_CLIP              b.lim_ref.max_clip                       // cc_float            Operational positive field reference limit
#define REG_VAR_LIMITS_B_STANDBY               b.lim_ref.standby                        // cc_float            Operational standby field reference limit
#define REG_VAR_LIMITS_B_NEG                   b.lim_ref.neg                            // cc_float            Operational negative field reference limit
#define REG_VAR_LIMITS_B_NEG_CLIP              b.lim_ref.min_clip                       // cc_float            Operational negative field reference limit
#define REG_VAR_LIMITS_B_RATE                  b.lim_ref.rate                           // cc_float            Operational rate of change of field reference limit
#define REG_VAR_LIMITS_B_CLOSELOOP             b.lim_ref.closeloop                      // cc_float            Operational field closeloop limit
#define REG_VAR_LIMITS_B_LOW                   b.lim_meas.low                           // cc_float            Operational field measurement low limit
#define REG_VAR_LIMITS_B_ZERO                  b.lim_meas.zero                          // cc_float            Operational field measurement zero limit
#define REG_VAR_LIMITS_B_ERR_WARNING           b.reg_err.warning.threshold              // cc_float            Operational field regulation and reference error warning limit
#define REG_VAR_LIMITS_B_ERR_FAULT             b.reg_err.fault.threshold                // cc_float            Operational field regulation and reference error fault limit
#define REG_VAR_LIMITS_I_POS                   i.lim_ref.pos                            // cc_float            Operational positive current reference limit
#define REG_VAR_LIMITS_I_POS_CLIP              i.lim_ref.max_clip                       // cc_float            Operational positive current reference limit
#define REG_VAR_LIMITS_I_STANDBY               i.lim_ref.standby                        // cc_float            Operational standby current reference limit
#define REG_VAR_LIMITS_I_NEG                   i.lim_ref.neg                            // cc_float            Operational negative current reference limit
#define REG_VAR_LIMITS_I_NEG_CLIP              i.lim_ref.min_clip                       // cc_float            Operational negative current reference limit
#define REG_VAR_LIMITS_I_RATE                  i.lim_ref.rate                           // cc_float            Operational rate of change of current reference limit
#define REG_VAR_LIMITS_I_CLOSELOOP             i.lim_ref.closeloop                      // cc_float            Operational current closeloop limit
#define REG_VAR_LIMITS_I_LOW                   i.lim_meas.low                           // cc_float            Operational current measurement low limit
#define REG_VAR_LIMITS_I_ZERO                  i.lim_meas.zero                          // cc_float            Operational current measurement zero limit
#define REG_VAR_LIMITS_I_ERR_WARNING           i.reg_err.warning.threshold              // cc_float            Operational current regulation and reference error warning limit
#define REG_VAR_LIMITS_I_ERR_FAULT             i.reg_err.fault.threshold                // cc_float            Operational current regulation and reference error fault limit
#define REG_VAR_LIMITS_V_POS                   v.lim_ref.pos                            // cc_float            Operational positive voltage reference limit
#define REG_VAR_LIMITS_V_POS_CLIP              v.lim_v_ref.max_clip_user                // cc_float            Operational positive voltage reference limit
#define REG_VAR_LIMITS_V_NEG                   v.lim_ref.neg                            // cc_float            Operational negative voltage reference limit
#define REG_VAR_LIMITS_V_NEG_CLIP              v.lim_v_ref.min_clip_user                // cc_float            Operational negative voltage reference limit
#define REG_VAR_LIMITS_V_RATE                  v.lim_ref.rate                           // cc_float            Operational rate of change of voltage reference limit
#define REG_VAR_LIMITS_V_ERR_WARNING           v.reg_err.warning.threshold              // cc_float            Operational voltage regulation and reference error warning limit
#define REG_VAR_LIMITS_V_ERR_FAULT             v.reg_err.fault.threshold                // cc_float            Operational voltage regulation and reference error fault limit
#define REG_VAR_LIMITS_I_RMS_WARNING           lim_i_rms.warning                        // cc_float            Operational RMS current warning limit
#define REG_VAR_LIMITS_I_RMS_FAULT             lim_i_rms.fault                          // cc_float            Operational RMS current fault limit
#define REG_VAR_LIMITS_I_RMS_LOAD_WARNING      lim_i_rms_load.warning                   // cc_float            Operational RMS load current warning limit
#define REG_VAR_LIMITS_I_RMS_LOAD_FAULT        lim_i_rms_load.fault                     // cc_float            Operational RMS load current fault limit
#define REG_VAR_LIMITS_V_RATE_RMS_FAULT        lim_v_rate_rms.fault                     // cc_float            Operational RMS voltage rate fault limit
#define REG_VAR_LIMITS_P_POS                   v.lim_v_ref.p_pos                        // cc_float            Operational positive power limit
#define REG_VAR_LIMITS_P_NEG                   v.lim_v_ref.p_neg                        // cc_float            Operational negative power limit
#define REG_VAR_MEAS_B_UNFILTERED              b.meas.signal[REG_MEAS_UNFILTERED]       // cc_float            Unfiltered field measurement
#define REG_VAR_MEAS_B_FILTERED                b.meas.signal[REG_MEAS_FILTERED]         // cc_float            Filtered field measurement
#define REG_VAR_MEAS_B_EXTRAPOLATED            b.meas.signal[REG_MEAS_EXTRAPOLATED]     // cc_float            Extrapolated field measurement
#define REG_VAR_MEAS_B_RATE                    b.meas.rate                              // cc_float            Estimate rate of change of field measurement
#define REG_VAR_MEAS_B_INVALID_COUNTER         inputs.b_meas.fault.invalid_counter      // uint32_t            Counter for invalid field measurements
#define REG_VAR_MEAS_I_UNFILTERED              i.meas.signal[REG_MEAS_UNFILTERED]       // cc_float            Unfiltered current measurement
#define REG_VAR_MEAS_I_FILTERED                i.meas.signal[REG_MEAS_FILTERED]         // cc_float            Filtered current measurement
#define REG_VAR_MEAS_I_EXTRAPOLATED            i.meas.signal[REG_MEAS_EXTRAPOLATED]     // cc_float            Extrapolated current measurement
#define REG_VAR_MEAS_I_RATE                    i.meas.rate                              // cc_float            Estimate rate of change of current measurement
#define REG_VAR_MEAS_I_INVALID_COUNTER         inputs.i_meas.fault.invalid_counter      // uint32_t            Counter for invalid current measurements
#define REG_VAR_MEAS_I_MAG_SAT_UNFILTERED      inputs.i_mag_sat.unfiltered              // cc_float            Unfiltered measured current for magnet saturation compensation
#define REG_VAR_MEAS_I_MAG_SAT_INVALID_COUNTER inputs.i_mag_sat.fault.invalid_counter   // uint32_t            Counter of invalid i_mag_sat measurements
#define REG_VAR_MEAS_I_CAPA_UNFILTERED         inputs.i_capa.unfiltered                 // cc_float            Unfiltered i_capa measurements
#define REG_VAR_MEAS_I_CAPA_INVALID_COUNTER    inputs.i_capa.fault.invalid_counter      // uint32_t            Counter of invalid i_capa measurements
#define REG_VAR_MEAS_V_UNFILTERED              v.unfiltered                             // cc_float            Unfiltered voltage measurement
#define REG_VAR_MEAS_V_INVALID_COUNTER         inputs.v_meas.fault.invalid_counter      // uint32_t            Counter for invalid voltage measurements
#define REG_VAR_MEAS_POWER                     power                                    // cc_float            Power calculated from current reference or measurement x voltage reference
#define REG_VAR_MEAS_V_AC_HZ                   v.harmonics.meas_v_ac_hz                 // cc_float            Unfiltered V_AC frequency measurement
#define REG_VAR_MEAS_V_AC_HZ_FLTR              v.harmonics.meas_v_ac_hz_fltr            // cc_float            Filtered V_AC frequency measurement
#define REG_VAR_LOAD_OHMS                      load_pars.ohms                           // cc_float            Resistance for the load pole
#define REG_VAR_LOAD_MEAS_OHMS                 load_pars.meas_ohms                      // cc_float            Measured load resistance without magnet saturation compensation
#define REG_VAR_LOAD_MEAS_HENRYS               load_pars.meas_henrys                    // cc_float            Measured load inductance without magnet saturation compensation
#define REG_VAR_LOAD_MEAS_HENRYS_SAT           load_pars.meas_henrys_sat                // cc_float            Measured load inductance without saturation compensation
#define REG_VAR_LOAD_I_MAG_SAT                 load_sat_vars.i_mag_sat                  // cc_float            Current used for magnet saturation
#define REG_VAR_LOAD_SAT_FACTOR                load_sat_vars.sat_factor                 // cc_float            Magnet saturation factor
#define REG_VAR_VS_ACT_DELAY                   vs_act_delay                             // cc_float            Voltage source actuation delay in seconds

// EOF
