//! @file  regSim2.h
//! @brief Converter Control Regulation library Simulator 2 structures and functions
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "regLoad.h"

// Simulator 2 structures

//! Voltage source and load simulation
//!
//! Libreg supports the control of either a voltage source (VS ACTUATION is VOLTAGE_REF) or a
//! firing card of a thyristor converter (VS ACTUATION is FIRING_REF). It has two different
//! voltage and load simulators, known as Simulator 1 and Simualtor 2.
//!
//! Simulator 1
//!
//! This simulator always runs, independent of the VS ACTUATION, however, its simulated
//! signals are only used when VS ACTUATION is VOLTAGE_REF. The signals can be logged and
//! compared with Simulator 2 signals.
//!
//! Simulator 2
//!
//! This simulator only runs when VS ACTUATION is FIRING_REF. The voltage source and load models are
//! combined into one matrix-based algorithm that must be configured using Fresco. This algorithm
//! includes a Kalman filter and it produces a simulated I_capa signal, in addition to the usual
//! V_circuit, I_circuit and I_magnet signals.

//! Simulator 2 - Voltage source and load simulation parameters and variables structures (FIRING_REF only)

struct REG_sim2_pars
{
    cc_float                  * k;                              //!< VS SIM2_K row-major Kalman filter matrix [4x3]
    cc_float                  * h;                              //!< VS SIM2_H row-major observation matrix [3x4]
    cc_float                  * ma;                             //!< VS SIM2_MA row-major matrix [4x4]
    cc_float                  * mb;                             //!< VS SIM2_MB matrix [4x1]
};

struct REG_sim2_vars
{
    cc_float                    circuit_voltage;                //!< Circuit voltage       (without VS ACT_DELAY_ITERS).
    cc_float                    circuit_current;                //!< Circuit current       (without VS ACT_DELAY_ITERS).
    cc_float                    magnet_current;                 //!< Magnet current        (without VS ACT_DELAY_ITERS).
    cc_float                    capa_current;                   //!< Filter i_capa current (without VS ACT_DELAY_ITERS).
    cc_double                   uncorrected_x[4];               //!< Filter and load model uncorrected state vector
};


#ifdef __cplusplus
extern "C" {
#endif

// Simulator 2 functions

//! Initialize the Simulator 2 parameters structure.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    sim2_pars    Voltage source and Load simulation parameters object to update
//! @param[in]     k            Kalman filter row-major matrix [4x3]
//! @param[in]     h            Observation row-major matrix [3x4]
//! @param[in]     ma           Model MA row-major matrix [4x4]
//! @param[in]     mb           Model MB matrix [4x1]

void regSim2Init(struct REG_sim2_pars * sim2_pars,
                 cc_float               k[REG_SIM2_K_LEN],
                 cc_float               h[REG_SIM2_H_LEN],
                 cc_float               ma[REG_SIM2_MA_LEN],
                 cc_float               mb[REG_SIM2_MB_LEN]);


//! Simulator 2 - Model voltage source and load (used when VS ACTUATION is FIRING_REF).
//!
//! This is a Real-Time function.
//!
//! @param[in]     pars                 Voltage souce and Load simulation parameters.
//! @param[in,out] vars                 Voltage souce and Load simulation variables.
//! @param[in]     f_ref_limited        Limited firing reference
//! @param[in]     v_circuit            Measured circuit voltage
//! @param[in]     i_circuit            Measured circuit current
//! @param[in]     i_capa               Measured filter capactor current

void regSim2RT(struct REG_sim2_pars const * pars,
               struct REG_sim2_vars       * vars,
               cc_float                     f_ref_limited,
               cc_float                     v_circuit,
               cc_float                     i_circuit,
               cc_float                     i_capa);

#ifdef __cplusplus
}
#endif

// EOF
