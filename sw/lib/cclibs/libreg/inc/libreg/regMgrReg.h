//! @file  regMgrReg.h
//! @brief Converter Control Regulation library high-level management regulation functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// Prepare signal (B or I) for regulation
//
// This function will check if new RST parameter have been initialized and will make them active.
//
// It manages the iteration counter. When reg_mode is NONE, it will initialize the counter based
// on the absolute iteration time (UTC) to allow multiple controllers to synchronize their regulation
// periods. Otherwise it increments and wraps the iteration counter.
//
// @param[in,out] reg_mgr              Pointer to regulation manager structure.
// @param[in]     reg_signal           Pointer to regulated signal structure (b or i)
// @param[in]     iter_time            Iteration timestamp with ns resolution (unixtime + ns_time)

void regMgrRegBIsignalPrepareRT(struct REG_mgr          * reg_mgr,
                                struct REG_mgr_signal   * reg_signal,
                                struct CC_ns_time         iter_time);


//! Regulate voltage, current or field for one regulation period.
//!
//! If the regulation mode is #REG_VOLTAGE (the reference is a voltage reference), the function applies
//! limits and updates the RST actuation history for field and current, to prepare for a smooth switch
//! to another regulation mode.
//!
//! If the regulation mode is #REG_CURRENT or #REG_FIELD, the function checks whether the measurement is
//! above the threshold for closed loop regulation. This determines whether to use the RST or open loop
//! algorithm for regulation. In both cases, the function applies the current reference clip and rate limits
//! with regLimRefRT() and keeps the RST and open loop actuation histories up-to-date for both regulation modes
//! to allow for mode switching.
//!
//! Otherwise, the regulation is #REG_CURRENT or #REG_FIELD and the actuation is #REG_VOLTAGE_REF. In this
//! case, we execute the following steps for each regulation period:
//!
//! * Calculate the voltage reference using the RST algorithm, regRstCalcActRT().
//! * [If we are regulating current] Calculate the magnet saturation compensation with regLoadVrefSatRT().
//! * Apply voltage reference clip and rate limits with regLimRefRT().
//! * If the voltage reference has been clipped:
//! ** [If we are regulating current] Back-calculate the new voltage reference before the saturation
//!    compensation, using regLoadInverseVrefSatRT().
//! ** Mark the current reference as rate-limited (reg_lim_ref::rate).
//! * Back-calculate new current reference to keep RST and open loop histories balanced, using regRstCalcRefRT().
//!
//! Finally, in all cases, this function monitors the regulation error using the delayed reference and the
//! filtered measurement with regErrCheckLimitsRT().
//!
//! This is a Real-Time function.
//!
//! @param[in,out] reg_mgr             Pointer to regulation manager structure.
//! @param[in]     ref                 Pointer to the reference
//! @param[in]     ref_ilc             Iterative Learning Controller (ILC) reference
//! @param[in]     enable_reg_error    If true, then the regulation error fault and warning limits will be checked and max_abs_err will be calculated.
//! @param[in]     force_openloop      If true, then the regulation (if active) will be openloop
//! @param[in]     generate_harmonics  If true and if V_AC signal is available then run the V_AC harmonics generator
//! @param[in]     ilc_active          If true then the regulation error will be REF_ADV-MEAS, otherwise REF_DELAYED-MEAS
//!
//! @retval        false               Regulation did not complete on this iteration
//! @retval        true                Regulation did complete on this iteration

bool regMgrRegRT(struct REG_mgr * reg_mgr,
                 cc_float       * ref,
                 cc_float         ref_ilc,
                 bool             enable_reg_error,
                 bool             force_openloop,
                 bool             generate_harmonics,
                 bool             ilc_active);

#ifdef __cplusplus
}
#endif

// EOF
