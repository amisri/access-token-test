//! @file  inc/libreg/regPars.h
//!
//! @brief Converter Control Regulation library : Parameter header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#define REG_NUM_PARS                               126
#define REG_NUM_LOADS                              4
#define regMgrParAppPointer(REG_PARS,PAR_NAME)     (&(REG_PARS)->values.REG_PAR_ ## PAR_NAME)
#define regMgrParAppValue(REG_PARS,PAR_NAME)       ((REG_PARS)->values.REG_PAR_ ## PAR_NAME)
#define regMgrParPointer(REG_MGR,PAR_NAME)         (&(REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[0])
#define regMgrParValue(REG_MGR,PAR_NAME)           (REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[0]
#define regMgrParValueTest(REG_MGR,PAR_NAME)       (REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[1]

// Parameter flags

#define REG_PAR_FLAG_LOAD_SELECT                   (1u<<0)
#define REG_PAR_FLAG_TEST_PAR                      (1u<<1)
#define REG_PAR_FLAG_MODE_NONE_ONLY                (1u<<2)
#define REG_PAR_FLAG_FIELD_REG                     (1u<<3)
#define REG_PAR_FLAG_CURRENT_REG                   (1u<<4)
#define REG_PAR_FLAG_VOLTAGE_REG                   (1u<<5)

// Parameter groups

#define REG_PAR_GROUP_ITER_PERIOD                  (1u<<0)
#define REG_PAR_GROUP_MEAS_SIM_DELAYS              (1u<<1)
#define REG_PAR_GROUP_VS_SIM                       (1u<<2)
#define REG_PAR_GROUP_V_LIMITS_MEAS                (1u<<3)
#define REG_PAR_GROUP_V_LIMITS_REF                 (1u<<4)
#define REG_PAR_GROUP_V_LIMITS_RATE_RMS            (1u<<5)
#define REG_PAR_GROUP_I_LIMITS_MEAS                (1u<<6)
#define REG_PAR_GROUP_I_LIMITS_REF                 (1u<<7)
#define REG_PAR_GROUP_I_LIMITS_ERR                 (1u<<8)
#define REG_PAR_GROUP_B_LIMITS_MEAS                (1u<<9)
#define REG_PAR_GROUP_B_LIMITS_REF                 (1u<<10)
#define REG_PAR_GROUP_B_LIMITS_ERR                 (1u<<11)
#define REG_PAR_GROUP_DECO                         (1u<<12)
#define REG_PAR_GROUP_I_LIMITS_RMS                 (1u<<13)
#define REG_PAR_GROUP_I_MEAS_FILTER                (1u<<14)
#define REG_PAR_GROUP_B_MEAS_FILTER                (1u<<15)
#define REG_PAR_GROUP_LOAD                         (1u<<16)
#define REG_PAR_GROUP_LOAD_SIM                     (1u<<17)
#define REG_PAR_GROUP_FIRING_LUT                   (1u<<18)
#define REG_PAR_GROUP_VREG                         (1u<<19)
#define REG_PAR_GROUP_VREF_ADV                     (1u<<20)
#define REG_PAR_GROUP_V_LIMITS_ERR                 (1u<<21)
#define REG_PAR_GROUP_VREG_LIMITS                  (1u<<22)
#define REG_PAR_GROUP_IREG                         (1u<<23)
#define REG_PAR_GROUP_BREG                         (1u<<24)
#define REG_PAR_GROUP_LOAD_TEST                    (1u<<25)
#define REG_PAR_GROUP_IREG_TEST                    (1u<<26)
#define REG_PAR_GROUP_BREG_TEST                    (1u<<27)
#define REG_PAR_GROUP_VFEEDFWD                     (1u<<28)
#define REG_PAR_GROUP_HARMONICS                    (1u<<29)

// Parameter groups masks

#define REG_PAR_ALL_GROUPS_MASK                    0x3FFFFFFF
#define REG_PAR_FIELD_GROUPS_MASK                  0x09008E00
#define REG_PAR_CURRENT_GROUPS_MASK                0x04800100
#define REG_PAR_VOLTAGE_GROUPS_MASK                0x003C0000
#define REG_PAR_OTHER_GROUPS_MASK                  0x324370FF

// Parameter variables

#define REG_PAR_LOAD_SELECT                           load_select
#define REG_PAR_LOAD_TEST_SELECT                      load_test_select
#define REG_PAR_LOAD_SIM_TC_ERROR                     load_sim_tc_error
#define REG_PAR_LOAD_OHMS_SER                         load_ohms_ser
#define REG_PAR_LOAD_OHMS_PAR                         load_ohms_par
#define REG_PAR_LOAD_OHMS_MAG                         load_ohms_mag
#define REG_PAR_LOAD_HENRYS                           load_henrys
#define REG_PAR_LOAD_HENRYS_SAT                       load_henrys_sat
#define REG_PAR_LOAD_I_SAT_GAIN                       load_i_sat_gain
#define REG_PAR_LOAD_I_SAT_START                      load_i_sat_start
#define REG_PAR_LOAD_I_SAT_END                        load_i_sat_end
#define REG_PAR_LOAD_SAT_SMOOTHING                    load_sat_smoothing
#define REG_PAR_LOAD_GAUSS_PER_AMP                    load_gauss_per_amp
#define REG_PAR_BREG_PERIOD_ITERS                     breg_period_iters
#define REG_PAR_BREG_EXTERNAL_ALG                     breg_external_alg
#define REG_PAR_BREG_INT_MEAS_SELECT                  breg_int_meas_select
#define REG_PAR_BREG_INT_PURE_DELAY_PERIODS           breg_int_pure_delay_periods
#define REG_PAR_BREG_INT_AUXPOLE1_HZ                  breg_int_auxpole1_hz
#define REG_PAR_BREG_INT_AUXPOLES2_HZ                 breg_int_auxpoles2_hz
#define REG_PAR_BREG_INT_AUXPOLES2_Z                  breg_int_auxpoles2_z
#define REG_PAR_BREG_INT_AUXPOLE4_HZ                  breg_int_auxpole4_hz
#define REG_PAR_BREG_INT_AUXPOLE5_HZ                  breg_int_auxpole5_hz
#define REG_PAR_BREG_EXT_FRESCO_INDEX                 breg_ext_fresco_index
#define REG_PAR_BREG_EXT_MEAS_SELECT                  breg_ext_meas_select
#define REG_PAR_BREG_EXT_TRACK_DELAY_PERIODS          breg_ext_track_delay_periods
#define REG_PAR_BREG_EXT_OP_R                         breg_ext_op_r
#define REG_PAR_BREG_EXT_OP_S                         breg_ext_op_s
#define REG_PAR_BREG_EXT_OP_T                         breg_ext_op_t
#define REG_PAR_BREG_EXT_TEST_R                       breg_ext_test_r
#define REG_PAR_BREG_EXT_TEST_S                       breg_ext_test_s
#define REG_PAR_BREG_EXT_TEST_T                       breg_ext_test_t
#define REG_PAR_IREG_PERIOD_ITERS                     ireg_period_iters
#define REG_PAR_IREG_EXTERNAL_ALG                     ireg_external_alg
#define REG_PAR_IREG_INT_MEAS_SELECT                  ireg_int_meas_select
#define REG_PAR_IREG_INT_PURE_DELAY_PERIODS           ireg_int_pure_delay_periods
#define REG_PAR_IREG_INT_AUXPOLE1_HZ                  ireg_int_auxpole1_hz
#define REG_PAR_IREG_INT_AUXPOLES2_HZ                 ireg_int_auxpoles2_hz
#define REG_PAR_IREG_INT_AUXPOLES2_Z                  ireg_int_auxpoles2_z
#define REG_PAR_IREG_INT_AUXPOLE4_HZ                  ireg_int_auxpole4_hz
#define REG_PAR_IREG_INT_AUXPOLE5_HZ                  ireg_int_auxpole5_hz
#define REG_PAR_IREG_EXT_FRESCO_INDEX                 ireg_ext_fresco_index
#define REG_PAR_IREG_EXT_MEAS_SELECT                  ireg_ext_meas_select
#define REG_PAR_IREG_EXT_TRACK_DELAY_PERIODS          ireg_ext_track_delay_periods
#define REG_PAR_IREG_EXT_OP_R                         ireg_ext_op_r
#define REG_PAR_IREG_EXT_OP_S                         ireg_ext_op_s
#define REG_PAR_IREG_EXT_OP_T                         ireg_ext_op_t
#define REG_PAR_IREG_EXT_TEST_R                       ireg_ext_test_r
#define REG_PAR_IREG_EXT_TEST_S                       ireg_ext_test_s
#define REG_PAR_IREG_EXT_TEST_T                       ireg_ext_test_t
#define REG_PAR_VREG_EXT_FRESCO_INDEX                 vreg_ext_fresco_index
#define REG_PAR_VREG_EXT_K_INT                        vreg_ext_k_int
#define REG_PAR_VREG_EXT_K_P                          vreg_ext_k_p
#define REG_PAR_VREG_EXT_K_FF                         vreg_ext_k_ff
#define REG_PAR_VFILTER_V_MEAS_SOURCE                 vfilter_v_meas_source
#define REG_PAR_VFILTER_I_CAPA_SOURCE                 vfilter_i_capa_source
#define REG_PAR_VFILTER_I_CAPA_FILTER                 vfilter_i_capa_filter
#define REG_PAR_VFILTER_EXT_K_U                       vfilter_ext_k_u
#define REG_PAR_VFILTER_EXT_K_I                       vfilter_ext_k_i
#define REG_PAR_VFILTER_EXT_K_D                       vfilter_ext_k_d
#define REG_PAR_VFEEDFWD_NUM                          vfeedfwd_num
#define REG_PAR_VFEEDFWD_DEN                          vfeedfwd_den
#define REG_PAR_HARMONICS_AC_PERIOD_ITERS             harmonics_ac_period_iters
#define REG_PAR_HARMONICS_GAIN                        harmonics_gain
#define REG_PAR_HARMONICS_PHASE                       harmonics_phase
#define REG_PAR_DECO_PHASE                            deco_phase
#define REG_PAR_DECO_INDEX                            deco_index
#define REG_PAR_DECO_K                                deco_k
#define REG_PAR_DECO_D                                deco_d
#define REG_PAR_LIMITS_B_POS                          limits_b_pos
#define REG_PAR_LIMITS_B_STANDBY                      limits_b_standby
#define REG_PAR_LIMITS_B_NEG                          limits_b_neg
#define REG_PAR_LIMITS_B_RATE                         limits_b_rate
#define REG_PAR_LIMITS_B_CLOSELOOP                    limits_b_closeloop
#define REG_PAR_LIMITS_B_LOW                          limits_b_low
#define REG_PAR_LIMITS_B_ZERO                         limits_b_zero
#define REG_PAR_LIMITS_B_ERR_WARNING                  limits_b_err_warning
#define REG_PAR_LIMITS_B_ERR_FAULT                    limits_b_err_fault
#define REG_PAR_LIMITS_I_POS                          limits_i_pos
#define REG_PAR_LIMITS_I_STANDBY                      limits_i_standby
#define REG_PAR_LIMITS_I_NEG                          limits_i_neg
#define REG_PAR_LIMITS_I_RATE                         limits_i_rate
#define REG_PAR_LIMITS_I_CLOSELOOP                    limits_i_closeloop
#define REG_PAR_LIMITS_I_LOW                          limits_i_low
#define REG_PAR_LIMITS_I_ZERO                         limits_i_zero
#define REG_PAR_LIMITS_I_ERR_WARNING                  limits_i_err_warning
#define REG_PAR_LIMITS_I_ERR_FAULT                    limits_i_err_fault
#define REG_PAR_LIMITS_I_RMS_LOAD_TC                  limits_i_rms_load_tc
#define REG_PAR_LIMITS_I_RMS_LOAD_WARNING             limits_i_rms_load_warning
#define REG_PAR_LIMITS_I_RMS_LOAD_FAULT               limits_i_rms_load_fault
#define REG_PAR_LIMITS_I_RMS_TC                       limits_i_rms_tc
#define REG_PAR_LIMITS_I_RMS_WARNING                  limits_i_rms_warning
#define REG_PAR_LIMITS_I_RMS_FAULT                    limits_i_rms_fault
#define REG_PAR_LIMITS_V_POS                          limits_v_pos
#define REG_PAR_LIMITS_V_NEG                          limits_v_neg
#define REG_PAR_LIMITS_V_RATE                         limits_v_rate
#define REG_PAR_LIMITS_V_RATE_RMS_TC                  limits_v_rate_rms_tc
#define REG_PAR_LIMITS_V_RATE_RMS_FAULT               limits_v_rate_rms_fault
#define REG_PAR_LIMITS_V_ERR_WARNING                  limits_v_err_warning
#define REG_PAR_LIMITS_V_ERR_FAULT                    limits_v_err_fault
#define REG_PAR_LIMITS_P_POS                          limits_p_pos
#define REG_PAR_LIMITS_P_NEG                          limits_p_neg
#define REG_PAR_LIMITS_I_QUADRANTS41                  limits_i_quadrants41
#define REG_PAR_LIMITS_V_QUADRANTS41                  limits_v_quadrants41
#define REG_PAR_MEAS_B_DELAY_ITERS                    meas_b_delay_iters
#define REG_PAR_MEAS_I_DELAY_ITERS                    meas_i_delay_iters
#define REG_PAR_MEAS_V_DELAY_ITERS                    meas_v_delay_iters
#define REG_PAR_MEAS_B_FIR_LENGTHS                    meas_b_fir_lengths
#define REG_PAR_MEAS_I_FIR_LENGTHS                    meas_i_fir_lengths
#define REG_PAR_VS_ACTUATION                          vs_actuation
#define REG_PAR_VS_ACT_DELAY_ITERS                    vs_act_delay_iters
#define REG_PAR_VS_NUM_VSOURCES                       vs_num_vsources
#define REG_PAR_VS_GAIN                               vs_gain
#define REG_PAR_VS_OFFSET                             vs_offset
#define REG_PAR_VS_FIRING_MODE                        vs_firing_mode
#define REG_PAR_VS_FIRING_DELAY                       vs_firing_delay
#define REG_PAR_VS_FIRING_LUT                         vs_firing_lut
#define REG_PAR_VS_SIM_BANDWIDTH                      vs_sim_bandwidth
#define REG_PAR_VS_SIM_Z                              vs_sim_z
#define REG_PAR_VS_SIM_TAU_ZERO                       vs_sim_tau_zero
#define REG_PAR_VS_SIM_NUM                            vs_sim_num
#define REG_PAR_VS_SIM_DEN                            vs_sim_den
#define REG_PAR_VS_SIM2_K                             vs_sim2_k
#define REG_PAR_VS_SIM2_H                             vs_sim2_h
#define REG_PAR_VS_SIM2_MA                            vs_sim2_ma
#define REG_PAR_VS_SIM2_MB                            vs_sim2_mb
#define REG_PAR_ITER_PERIOD_NS                        iter_period_ns

// Application parameters structure - can be in slow memory

struct REG_pars
{
    struct
    {
           uint32_t                 load_select;
           uint32_t                 load_test_select;
           cc_float                 load_sim_tc_error;
           cc_float                 load_ohms_ser[REG_NUM_LOADS];
           cc_float                 load_ohms_par[REG_NUM_LOADS];
           cc_float                 load_ohms_mag[REG_NUM_LOADS];
           cc_float                 load_henrys[REG_NUM_LOADS];
           cc_float                 load_henrys_sat[REG_NUM_LOADS];
           cc_float                 load_i_sat_gain[REG_NUM_LOADS];
           cc_float                 load_i_sat_start[REG_NUM_LOADS];
           cc_float                 load_i_sat_end[REG_NUM_LOADS];
           cc_float                 load_sat_smoothing[REG_NUM_LOADS];
           cc_float                 load_gauss_per_amp[REG_NUM_LOADS];
           uint32_t                 breg_period_iters[REG_NUM_LOADS];
           enum CC_enabled_disabled breg_external_alg[REG_NUM_LOADS];
           enum REG_meas_select     breg_int_meas_select[REG_NUM_LOADS];
           cc_float                 breg_int_pure_delay_periods[REG_NUM_LOADS];
           cc_float                 breg_int_auxpole1_hz[REG_NUM_LOADS];
           cc_float                 breg_int_auxpoles2_hz[REG_NUM_LOADS];
           cc_float                 breg_int_auxpoles2_z[REG_NUM_LOADS];
           cc_float                 breg_int_auxpole4_hz[REG_NUM_LOADS];
           cc_float                 breg_int_auxpole5_hz[REG_NUM_LOADS];
           uint32_t                 breg_ext_fresco_index[REG_FRESCO_INDEX_LEN];
           enum REG_meas_select     breg_ext_meas_select[REG_NUM_LOADS];
           cc_float                 breg_ext_track_delay_periods[REG_NUM_LOADS];
           cc_float                 breg_ext_op_r[REG_NUM_RST_COEFFS];
           cc_float                 breg_ext_op_s[REG_NUM_RST_COEFFS];
           cc_float                 breg_ext_op_t[REG_NUM_RST_COEFFS];
           cc_float                 breg_ext_test_r[REG_NUM_RST_COEFFS];
           cc_float                 breg_ext_test_s[REG_NUM_RST_COEFFS];
           cc_float                 breg_ext_test_t[REG_NUM_RST_COEFFS];
           uint32_t                 ireg_period_iters[REG_NUM_LOADS];
           enum CC_enabled_disabled ireg_external_alg[REG_NUM_LOADS];
           enum REG_meas_select     ireg_int_meas_select[REG_NUM_LOADS];
           cc_float                 ireg_int_pure_delay_periods[REG_NUM_LOADS];
           cc_float                 ireg_int_auxpole1_hz[REG_NUM_LOADS];
           cc_float                 ireg_int_auxpoles2_hz[REG_NUM_LOADS];
           cc_float                 ireg_int_auxpoles2_z[REG_NUM_LOADS];
           cc_float                 ireg_int_auxpole4_hz[REG_NUM_LOADS];
           cc_float                 ireg_int_auxpole5_hz[REG_NUM_LOADS];
           uint32_t                 ireg_ext_fresco_index[REG_FRESCO_INDEX_LEN];
           enum REG_meas_select     ireg_ext_meas_select[REG_NUM_LOADS];
           cc_float                 ireg_ext_track_delay_periods[REG_NUM_LOADS];
           cc_float                 ireg_ext_op_r[REG_NUM_RST_COEFFS];
           cc_float                 ireg_ext_op_s[REG_NUM_RST_COEFFS];
           cc_float                 ireg_ext_op_t[REG_NUM_RST_COEFFS];
           cc_float                 ireg_ext_test_r[REG_NUM_RST_COEFFS];
           cc_float                 ireg_ext_test_s[REG_NUM_RST_COEFFS];
           cc_float                 ireg_ext_test_t[REG_NUM_RST_COEFFS];
           uint32_t                 vreg_ext_fresco_index[REG_FRESCO_INDEX_LEN];
           cc_float                 vreg_ext_k_int;
           cc_float                 vreg_ext_k_p;
           cc_float                 vreg_ext_k_ff;
           enum REG_sig_source      vfilter_v_meas_source;
           enum REG_sig_source      vfilter_i_capa_source;
           cc_float                 vfilter_i_capa_filter[2];
           cc_float                 vfilter_ext_k_u;
           cc_float                 vfilter_ext_k_i;
           cc_float                 vfilter_ext_k_d;
           cc_float                 vfeedfwd_num[REG_SIM_NUM_DEN_LEN];
           cc_float                 vfeedfwd_den[REG_SIM_NUM_DEN_LEN];
           uint32_t                 harmonics_ac_period_iters;
           cc_float                 harmonics_gain[REG_NUM_HARMONICS];
           cc_float                 harmonics_phase[REG_NUM_HARMONICS];
           uint32_t                 deco_phase;
           uint32_t                 deco_index;
           cc_float                 deco_k[REG_NUM_DECO];
           cc_float                 deco_d[REG_NUM_DECO];
           cc_float                 limits_b_pos[REG_NUM_LOADS];
           cc_float                 limits_b_standby[REG_NUM_LOADS];
           cc_float                 limits_b_neg[REG_NUM_LOADS];
           cc_float                 limits_b_rate[REG_NUM_LOADS];
           cc_float                 limits_b_closeloop[REG_NUM_LOADS];
           cc_float                 limits_b_low[REG_NUM_LOADS];
           cc_float                 limits_b_zero[REG_NUM_LOADS];
           cc_float                 limits_b_err_warning[REG_NUM_LOADS];
           cc_float                 limits_b_err_fault[REG_NUM_LOADS];
           cc_float                 limits_i_pos[REG_NUM_LOADS];
           cc_float                 limits_i_standby[REG_NUM_LOADS];
           cc_float                 limits_i_neg[REG_NUM_LOADS];
           cc_float                 limits_i_rate[REG_NUM_LOADS];
           cc_float                 limits_i_closeloop[REG_NUM_LOADS];
           cc_float                 limits_i_low[REG_NUM_LOADS];
           cc_float                 limits_i_zero[REG_NUM_LOADS];
           cc_float                 limits_i_err_warning[REG_NUM_LOADS];
           cc_float                 limits_i_err_fault[REG_NUM_LOADS];
           cc_float                 limits_i_rms_load_tc[REG_NUM_LOADS];
           cc_float                 limits_i_rms_load_warning[REG_NUM_LOADS];
           cc_float                 limits_i_rms_load_fault[REG_NUM_LOADS];
           cc_float                 limits_i_rms_tc;
           cc_float                 limits_i_rms_warning;
           cc_float                 limits_i_rms_fault;
           cc_float                 limits_v_pos[REG_NUM_LOADS];
           cc_float                 limits_v_neg[REG_NUM_LOADS];
           cc_float                 limits_v_rate;
           cc_float                 limits_v_rate_rms_tc;
           cc_float                 limits_v_rate_rms_fault;
           cc_float                 limits_v_err_warning;
           cc_float                 limits_v_err_fault;
           cc_float                 limits_p_pos[REG_NUM_LOADS];
           cc_float                 limits_p_neg[REG_NUM_LOADS];
           cc_float                 limits_i_quadrants41[2];
           cc_float                 limits_v_quadrants41[2];
           cc_float                 meas_b_delay_iters;
           cc_float                 meas_i_delay_iters;
           cc_float                 meas_v_delay_iters;
           uint32_t                 meas_b_fir_lengths[2];
           uint32_t                 meas_i_fir_lengths[2];
           enum REG_actuation       vs_actuation;
           cc_float                 vs_act_delay_iters;
           uint32_t                 vs_num_vsources;
           cc_float                 vs_gain;
           cc_float                 vs_offset;
           enum REG_firing_mode     vs_firing_mode;
           cc_float                 vs_firing_delay;
           cc_float                 vs_firing_lut[REG_FIRING_LUT_LEN];
           cc_float                 vs_sim_bandwidth;
           cc_float                 vs_sim_z;
           cc_float                 vs_sim_tau_zero;
           cc_float                 vs_sim_num[REG_SIM_NUM_DEN_LEN];
           cc_float                 vs_sim_den[REG_SIM_NUM_DEN_LEN];
           cc_float                 vs_sim2_k[REG_SIM2_K_LEN];
           cc_float                 vs_sim2_h[REG_SIM2_H_LEN];
           cc_float                 vs_sim2_ma[REG_SIM2_MA_LEN];
           cc_float                 vs_sim2_mb[REG_SIM2_MB_LEN];
           uint32_t                 iter_period_ns;
    } values;

    struct REG_pars_meta
    {
        void                * app_value;
        void                * lib_value;
        uint32_t              size_in_bytes;
        uint32_t              flags;
        uint32_t              groups;
    } meta[REG_NUM_PARS];
};

// Library parameters structure - included in reg_mgr in fast memory

struct REG_par_values
{
    uint32_t                 load_select                 [1];
    uint32_t                 load_test_select            [1];
    cc_float                 load_sim_tc_error           [1];
    cc_float                 load_ohms_ser               [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_ohms_par               [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_ohms_mag               [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_henrys                 [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_henrys_sat             [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_i_sat_gain             [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_i_sat_start            [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_i_sat_end              [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_sat_smoothing          [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 load_gauss_per_amp          [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    uint32_t                 breg_period_iters           [1];    // [0] for LOAD_SELECT
    enum CC_enabled_disabled breg_external_alg           [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    enum REG_meas_select     breg_int_meas_select        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_pure_delay_periods [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_auxpole1_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_auxpoles2_hz       [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_auxpoles2_z        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_auxpole4_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_int_auxpole5_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    uint32_t                 breg_ext_fresco_index       [REG_FRESCO_INDEX_LEN];
    enum REG_meas_select     breg_ext_meas_select        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_ext_track_delay_periods[2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 breg_ext_op_r               [REG_NUM_RST_COEFFS];
    cc_float                 breg_ext_op_s               [REG_NUM_RST_COEFFS];
    cc_float                 breg_ext_op_t               [REG_NUM_RST_COEFFS];
    cc_float                 breg_ext_test_r             [REG_NUM_RST_COEFFS];
    cc_float                 breg_ext_test_s             [REG_NUM_RST_COEFFS];
    cc_float                 breg_ext_test_t             [REG_NUM_RST_COEFFS];
    uint32_t                 ireg_period_iters           [1];    // [0] for LOAD_SELECT
    enum CC_enabled_disabled ireg_external_alg           [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    enum REG_meas_select     ireg_int_meas_select        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_pure_delay_periods [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_auxpole1_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_auxpoles2_hz       [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_auxpoles2_z        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_auxpole4_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_int_auxpole5_hz        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    uint32_t                 ireg_ext_fresco_index       [REG_FRESCO_INDEX_LEN];
    enum REG_meas_select     ireg_ext_meas_select        [2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_ext_track_delay_periods[2];    // [0] for LOAD_SELECT and [1] for LOAD_TEST_SELECT
    cc_float                 ireg_ext_op_r               [REG_NUM_RST_COEFFS];
    cc_float                 ireg_ext_op_s               [REG_NUM_RST_COEFFS];
    cc_float                 ireg_ext_op_t               [REG_NUM_RST_COEFFS];
    cc_float                 ireg_ext_test_r             [REG_NUM_RST_COEFFS];
    cc_float                 ireg_ext_test_s             [REG_NUM_RST_COEFFS];
    cc_float                 ireg_ext_test_t             [REG_NUM_RST_COEFFS];
    uint32_t                 vreg_ext_fresco_index       [REG_FRESCO_INDEX_LEN];
    cc_float                 vreg_ext_k_int              [1];
    cc_float                 vreg_ext_k_p                [1];
    cc_float                 vreg_ext_k_ff               [1];
    enum REG_sig_source      vfilter_v_meas_source       [1];
    enum REG_sig_source      vfilter_i_capa_source       [1];
    cc_float                 vfilter_i_capa_filter       [2];
    cc_float                 vfilter_ext_k_u             [1];
    cc_float                 vfilter_ext_k_i             [1];
    cc_float                 vfilter_ext_k_d             [1];
    cc_float                 vfeedfwd_num                [REG_SIM_NUM_DEN_LEN];
    cc_float                 vfeedfwd_den                [REG_SIM_NUM_DEN_LEN];
    uint32_t                 harmonics_ac_period_iters   [1];
    cc_float                 harmonics_gain              [REG_NUM_HARMONICS];
    cc_float                 harmonics_phase             [REG_NUM_HARMONICS];
    uint32_t                 deco_phase                  [1];
    uint32_t                 deco_index                  [1];
    cc_float                 deco_k                      [REG_NUM_DECO];
    cc_float                 deco_d                      [REG_NUM_DECO];
    cc_float                 limits_b_pos                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_standby            [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_neg                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_rate               [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_closeloop          [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_low                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_zero               [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_err_warning        [1];    // [0] for LOAD_SELECT
    cc_float                 limits_b_err_fault          [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_pos                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_standby            [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_neg                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_rate               [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_closeloop          [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_low                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_zero               [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_err_warning        [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_err_fault          [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_rms_load_tc        [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_rms_load_warning   [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_rms_load_fault     [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_rms_tc             [1];
    cc_float                 limits_i_rms_warning        [1];
    cc_float                 limits_i_rms_fault          [1];
    cc_float                 limits_v_pos                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_v_neg                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_v_rate               [1];
    cc_float                 limits_v_rate_rms_tc        [1];
    cc_float                 limits_v_rate_rms_fault     [1];
    cc_float                 limits_v_err_warning        [1];
    cc_float                 limits_v_err_fault          [1];
    cc_float                 limits_p_pos                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_p_neg                [1];    // [0] for LOAD_SELECT
    cc_float                 limits_i_quadrants41        [2];
    cc_float                 limits_v_quadrants41        [2];
    cc_float                 meas_b_delay_iters          [1];
    cc_float                 meas_i_delay_iters          [1];
    cc_float                 meas_v_delay_iters          [1];
    uint32_t                 meas_b_fir_lengths          [2];
    uint32_t                 meas_i_fir_lengths          [2];
    enum REG_actuation       vs_actuation                [1];
    cc_float                 vs_act_delay_iters          [1];
    uint32_t                 vs_num_vsources             [1];
    cc_float                 vs_gain                     [1];
    cc_float                 vs_offset                   [1];
    enum REG_firing_mode     vs_firing_mode              [1];
    cc_float                 vs_firing_delay             [1];
    cc_float                 vs_firing_lut               [REG_FIRING_LUT_LEN];
    cc_float                 vs_sim_bandwidth            [1];
    cc_float                 vs_sim_z                    [1];
    cc_float                 vs_sim_tau_zero             [1];
    cc_float                 vs_sim_num                  [REG_SIM_NUM_DEN_LEN];
    cc_float                 vs_sim_den                  [REG_SIM_NUM_DEN_LEN];
    cc_float                 vs_sim2_k                   [REG_SIM2_K_LEN];
    cc_float                 vs_sim2_h                   [REG_SIM2_H_LEN];
    cc_float                 vs_sim2_ma                  [REG_SIM2_MA_LEN];
    cc_float                 vs_sim2_mb                  [REG_SIM2_MB_LEN];
    uint32_t                 iter_period_ns              [1];
};

// EOF
