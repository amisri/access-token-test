//! @file  regMgrInit.h
//! @brief Converter Control Regulation library high-level management initialization functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the global regulation manager structure.
//!
//! The application is responsible for instantiating reg_mgr and reg_pars. If the device has a limited amount of
//! fast memory, then reg_mgr should be in the fast memory, while reg_pars can be in slow memory.
//! The function will set up internal pointers within the reg_mgr and reg_pars structures.
//! It will set the iteration period from the supplied parameter - this may not be changed within reinitializing.
//! The field_regulation, current_regulation and voltage_regulation parameters are used to enable or disable the option
//! to regulate field, current or voltage. Disabling a never used regulation mode reduces processing overhead.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.
//! @param[in]     reg_pars               Pointer to regulation parameters structure.
//! @param[in]     deco_shared            Pointer to regulation shared decoupling data structure (NULL if not required).
//! @param[in]     mutex                  Pointer to mutex to protect regMgrParsCheck() and regMgrParsProcess() (NULL is not required).
//! @param[in]     mutex_lock             Pointer to mutex lock function.
//! @param[in]     mutex_unlock           Pointer to mutex unlock function.
//! @param[in]     v_ac_buf               AC voltage measurement circular buffer (NULL if harmonics generation not required).
//! @param[in]     v_ac_buf_len           Length in elements of the v_ac_buf array.
//! @param[in]     field_regulation       Field   regulation control (ENABLED/DISABLED).
//! @param[in]     current_regulation     Current regulation control (ENABLED/DISABLED).
//! @param[in]     voltage_regulation     Voltage regulation control (ENABLED/DISABLED).
//!
//! @retval         0                     Success
//! @retval         -1                    mutex is defined but either mutex_lock or mutex_unlock are NULL

int32_t regMgrInit(struct REG_mgr                  * reg_mgr,
                   struct REG_pars                 * reg_pars,
                   struct REG_deco_shared volatile * deco_shared,
                   void                            * mutex,
                   REG_mutex_callback              * mutex_lock,
                   REG_mutex_callback              * mutex_unlock,
                   cc_float                        * v_ac_buf,
                   uint32_t                          v_ac_buf_len,
                   enum CC_enabled_disabled          field_regulation,
                   enum CC_enabled_disabled          current_regulation,
                   enum CC_enabled_disabled          voltage_regulation);


//! Set the iteration period
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.
//! @param[in]     iter_period_ns         Iteration period in nanoseconds

void regMgrInitIterPeriod(struct REG_mgr * reg_mgr,
                          uint32_t         iter_period_ns);


//! Set the optional pointers for field and current regulation periods in nanoseconds
//!
//! Liblog needs the period in nanoseconds for every log. For current and field regulation logs, the period
//! should match the regulation period, which can change. This function allows pointers to be set to point to
//! the variables to receive the field and/or current regulation periods in nanoseconds. The pointer can
//! be NULL if it is not required.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    reg_mgr                Pointer to regulation manager structure.
//! @param[in]     b_reg_period_ns_ptr    Optional pointer to variable to receive the field regulation period in nanoseconds.
//! @param[in]     i_reg_period_ns_ptr    Optional pointer to variable to receive the current regulation period in nanoseconds.

void regMgrInitRegPeriodPointers(struct REG_mgr * reg_mgr,
                                 int32_t        * b_reg_period_ns_ptr,
                                 int32_t        * i_reg_period_ns_ptr);


//! Initialize the RST coefficients
//!
//! This function will use libreg to initialize the RST controller coefficients. The result
//! is double buffered to ensure that the real-time processing always has a consistent
//! set of coefficients. If the initialization is a success,
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[out]    reg_mgr                  Pointer to regulation manager structure.
//! @param[in]     reg_mode                 Regulation mode to initialize (field or current).
//! @param[in]     reg_rst_source           Indicate if operational or test parameters are to be initialized.
//! @param[in]     reg_period_iters         Regulation period in iterations.
//! @param[in]     external_alg             CC_ENABLED if RST coefficients have been computed externally (in ext_r/s/t)
//! @param[in]     int_meas_select          Regulation measurement when using the internal RST synthesis algorithms.
//! @param[in]     int_pure_delay_periods   Pure delay in regulation periods (zero to calculate automatically) when using the internal RST synthesis algorithms.
//! @param[in]     int_auxpole1_hz          Frequency of first real pole when using the internal RST synthesis algorithms.
//! @param[in]     int_auxpoles2_hz         Frequency of conjugate poles when using the internal RST synthesis algorithms.
//! @param[in]     int_auxpoles2_z          Damping of conjugate poles when using the internal RST synthesis algorithms.
//! @param[in]     int_auxpole4_hz          Frequency of fourth real pole when using the internal RST synthesis algorithms.
//! @param[in]     int_auxpole5_hz          Frequency of fifth real pole when using the internal RST synthesis algorithms.
//! @param[in]     ext_meas_select          Regulation measurement when using the external RST synthesis algorithms.
//! @param[in]     ext_track_delay_periods  Track delay in regulation periods when using the external RST synthesis algorithms.
//! @param[in]     ext_r                    Externally calculated R coefficients.
//! @param[in]     ext_s                    Externally calculated S coefficients.
//! @param[in]     ext_t                    Externally calculated T coefficients.

void regMgrInitRst(struct REG_mgr         * reg_mgr,
                   enum REG_mode            reg_mode,
                   enum REG_rst_source      reg_rst_source,
                   uint32_t                 reg_period_iters,
                   enum CC_enabled_disabled external_alg,
                   enum REG_meas_select     int_meas_select,
                   cc_float                 int_pure_delay_periods,
                   cc_float                 int_auxpole1_hz,
                   cc_float                 int_auxpoles2_hz,
                   cc_float                 int_auxpoles2_z,
                   cc_float                 int_auxpole4_hz,
                   cc_float                 int_auxpole5_hz,
                   enum REG_meas_select     ext_meas_select,
                   cc_float                 ext_track_delay_periods,
                   cc_float                 const ext_r[REG_NUM_RST_COEFFS],
                   cc_float                 const ext_s[REG_NUM_RST_COEFFS],
                   cc_float                 const ext_t[REG_NUM_RST_COEFFS]);

#ifdef __cplusplus
}
#endif

// EOF
