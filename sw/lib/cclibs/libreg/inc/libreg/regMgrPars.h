//! @file  regMgrPars.h
//! @brief Converter Control Regulation library high-level management parameters functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//! Check libreg parameters for changes and set the parameter processing group bit mask accordingly.
//!
//! This function should be called by the background thread of the application whenever any libreg parameters might have changed.
//! It scans all the parameters for changes, keeps a copy of any parameters that change, and sets the parameter
//! group mask to signal to regMgrParsProcess(), which initialization functions must be called in order to treat the changed
//! parameters. It calls regMgrParsProcess() once, but this may not be able to treat all the
//! changes immediately. It is important to poll regMgrParsProcess() in the background in order
//! to process the remaining changed parameters when conditions allow (for example, when the regulation mode is NONE).
//!
//! If regMgrParsCheck and regMgrParsProcess are called from different threads, then a mutex and lock/unlock functions must be
//! declared to regMgrInit() so that the functions can protect themselves.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in,out]   reg_mgr    Pointer to regulation manager structure.

void regMgrParsCheck(struct REG_mgr * reg_mgr);


//! Process changed parameters detected by regMgrParsCheck.
//!
//! This should be called periodically by the background thread of the application to check if regMgrParsCheck has
//! identified any parameter groups with parameters that have changed value. This function is called once by regMgrParsCheck()
//! if it detects changed parameters, but it should be called periodically in case it cannot treat all the changed parameters
//! immediately.
//!
//! The function calls the appropriate initialization functions in the correct order to treat the change parameters.
//! The function return immediately if there is nothing to do. It can take some time if many initialization functions
//! must be called.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in,out]   reg_mgr    Pointer to regulation manager structure.

void regMgrParsProcess(struct REG_mgr *reg_mgr);

#ifdef __cplusplus
}
#endif

// EOF
