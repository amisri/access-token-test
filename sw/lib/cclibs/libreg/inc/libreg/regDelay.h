//! @file  regDelay.h
//! @brief Converter Control Regulation library signal delay functions
//!
//! These functions use a circular buffer and linear interpolation to provide a programmable
//! delay line for signals. It is used by the regulation error calculation functions and
//! can be used to simulate measurement filter delays etc.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Size of circular buffer. Specified as a mask, so value must be of the form (2^n)-1.

#define REG_DELAY_BUF_INDEX_MASK    255

//! Signal delay structure

struct REG_delay_pars
{
    int32_t                     delay_int_iters;                   //!< Integer delays in iteration periods
    cc_float                    delay_frac_iters;                  //!< Fractional delays in iteration periods
};

struct REG_delay_vars
{
    int32_t                     buf_index;                         //!< Index into circular buffer
    cc_float                    buf[REG_DELAY_BUF_INDEX_MASK+1];   //!< Circular buffer for signal. See also #REG_DELAY_BUF_INDEX_MASK
};

// Signal delay functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize reg_delay parameters structure delay
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]    pars                  Pointer to delay parameters structure
//! @param[in]     delay_iters           Delay in iterations.
//!                                      Value is clipped to the range (0,#REG_DELAY_BUF_INDEX_MASK-1.0)

void regDelayInitDelay(struct REG_delay_pars * const pars, cc_float delay_iters);


//! Initialize reg_delay variables structure buffer
//!
//! This is a non-Real-Time function: do not call from the real-time thread or interrupt
//!
//! @param[out]  vars            Pointers to delay variables structure.
//! @param[in]   initial_signal  Value to assign to all elements of REG_delay_vars::buf

void regDelayInitVars(struct REG_delay_vars * const vars, const cc_float initial_signal);


//! Delay a signal
//!
//! This is a Real-Time function.
//!
//! @param[in]      pars              Pointers to delay parameters structure.
//! @param[in,out]  vars              Pointers to delay variables structure.
//! @param[in]      signal            Value to assign to next slot in reg_delay_vars::buf
//! @param[in]      is_under_sampled  True if delayed signal jumps to final value immediately between iterations.
//!                                   False if delayed signal changes (roughly) linearly between iterations.
//!
//! @return  Signal value after delay

cc_float regDelaySignalRT(const struct REG_delay_pars * const pars,
                                struct REG_delay_vars * const vars,
                          const cc_float                      signal,
                          const bool                          is_under_sampled);

#ifdef __cplusplus
}
#endif

// EOF
