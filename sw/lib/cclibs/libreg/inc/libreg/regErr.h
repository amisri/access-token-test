//! @file regErr.h
//! @brief Converter Control Regulation library regulation error functions
//!
//! Functions for all types of regulation (current, field, voltage). These
//! functions maintain a history of the reference so that the measurement can be
//! compared against the reference, taking into account the tracking delay.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Regulation error limit structure

struct REG_err_limit
{
    cc_float                    threshold;                      //!< Limit threshold
    uint32_t                    counter;                        //!< Iteration counter for threshold passed (in either direction)
    bool                        flag;                           //!< Limit exceeded flag
};

//! Regulation error structure

struct REG_err
{
    cc_float                    err;                            //!< Error = actual - expected
    cc_float                    abs_err;                        //!< Absolute error
    cc_float                    max_abs_err;                    //!< Max absolute error when enabled
    cc_float                    err2_filter;                    //!< Error squared filter
    cc_float                    rms_err;                        //!< Root mean squared error
    uint32_t                    filter_time_iters;              //!< Filter time in iterations
    struct REG_err_limit        warning;                        //!< Warning limit structure
    struct REG_err_limit        fault;                          //!< Fault limit structure
};

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the warning and fault limits of the reg_err structure.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[out]   err                  Pointer to regulation error structure.
//! @param[in]    warning_threshold    New warning threshold. Set to zero to disable the limit.
//! @param[in]    fault_threshold      New fault threshold. Set to zero to disable the limit.
//! @param[in]    filter_time_iters    Time in iterations that limit must be passed to set or reset the limit flag.

void regErrInitLimits(struct REG_err * err,
                      cc_float         warning_threshold,
                      cc_float         fault_threshold,
                      uint32_t         filter_time_iters);


//! Reset the reg_err structure variables to zero.
//!
//! This is a Real-Time function.
//!
//! @param[out]   err                  Pointer to regulation error structure.

void regErrResetLimitsVarsRT(struct REG_err * err);


//! Calculate the regulation error and check the fault and warning limits (if supplied).
//!
//! Note: error = actual - expected
//!
//! This is a Real-Time function.
//!
//! @param[in,out] err            Pointer to regulation error structure.
//! @param[in]     actual         Actual (measured) value.
//! @param[in]     expected       Expected (delayed reference) value.
//! @param[in]     enabled        Set to true to enable error calculation and limits checks
//! @param[in]     reg_iter_flag  Set to true on iterations that include the regulation algorithm

void regErrCheckLimitsRT(struct REG_err * err,
                         cc_float         actual,
                         cc_float         expected,
                         bool             enabled,
                         bool             reg_iter_flag);

#ifdef __cplusplus
}
#endif

// EOF
