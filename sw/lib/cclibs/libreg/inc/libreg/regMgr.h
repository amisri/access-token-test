//! @file  regMgr.h
//! @brief Converter Control Regulation library high-level management structures.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// typedefs

typedef int REG_mutex_callback(void * const data);          //!< Typedef for lock/unlock mutex functions (compatible with pthread_mutex_lock and pthread_mutex_unlock)

// Global power regulation manager structures

//! Signal input structure

struct REG_mgr_input
{
    struct CC_meas_signal           meas;                   //!< Input measurement and measurement status
    struct REG_meas_fault           fault;                  //!< Measurement invalid counters and fault flag
    cc_float                        unfiltered;             //!< Last valid unfiltered measurement
};

struct REG_mgr_inputs
{
    struct REG_mgr_input            b_meas;                 //!< Field measurement input
    struct REG_mgr_input            i_meas;                 //!< Current measurement input
    struct REG_mgr_input            i_mag_sat;              //!< Current measurement for the magnet saturation input
    struct REG_mgr_input            i_capa;                 //!< Converter filter capacitor current measurement input
    struct REG_mgr_input            v_meas;                 //!< Voltage measurement input
    struct REG_mgr_input            v_ac;                   //!< AC voltage measurement input
    struct REG_mgr_input            v_ff;                   //!< Feed forward voltage input
};

//! RST parameters structure

struct REG_mgr_rst_pars
{
    struct REG_rst_pars           * active;                 //!< Pointer to active parameters in pars[]
    struct REG_rst_pars           * next;                   //!< Pointer to next parameters in pars[]
    struct REG_rst_pars             pars[2];                //!< Structures for active and next RST parameter
    volatile uint32_t               use_next;               //!< Down counter to delay activation of the next set of RST parameters
};

//! V feed forward signal filter structure

struct REG_v_ff_fltr
{
    struct REG_sim_vs_pars          pars;                   //!< V feed forward filter parameters
    struct REG_sim_vs_vars          vars;                   //!< V feed forward filter variables
    cc_float                        signal;                 //!< Filtered V feed forward signal
};

//! Field or Current regulation signal structure

struct REG_mgr_signal
{
    enum   CC_enabled_disabled      regulation;             //!< Option to regulate this signal is enabled or disabled
    uint32_t                        iteration_index;        //!< Iteration index (within each regulation period)
    uint32_t                        reg_period_iters;       //!< Regulation period (in iterations) for Operational and Test parameters
    int32_t                       * reg_period_ns_ptr;      //!< Optional pointer to place to write the reg_period in ns (useful for liblog log_mgr.period.ns)
    int32_t                         reg_period_ns;          //!< Regulation period (ns) for Operational and Test parameters
    cc_float                        reg_period;             //!< Regulation period (s) for Operational and Test parameters
    cc_float                        inv_reg_period;         //!< Regulation frequency (Hz) for Operational and Test parameters
    struct REG_meas_filter          meas;                   //!< Unfiltered and filtered measurement (real or sim)
    struct REG_lim_meas             lim_meas;               //!< Measurement limits
    struct REG_lim_ref              lim_ref;                //!< Reference limits
    struct REG_rst_pars           * rst_pars;               //!< Active RST parameters (Active Operational or Test)
    struct REG_rst_vars             rst_vars;               //!< RST regulation variables
    struct REG_mgr_rst_pars         op_rst_pars;            //!< Operational regulation RST parameters
    struct REG_rst_pars             last_op_rst_pars;       //!< Last initialized operational RST parameters for debugging
    struct REG_mgr_rst_pars         test_rst_pars;          //!< Test regulation RST parameters
    struct REG_rst_pars             last_test_rst_pars;     //!< Last initialized test RST parameters for debugging
    struct REG_err                  reg_err;                //!< Regulation error = ref_delayed - meas
    cc_float                        ref_adv;                //!< Reference advanced by rst_pars->ref_advance
    cc_float                        ref_limited;            //!< ref_adv after reference limits
    cc_float                        ref_closed;             //!< Reference from RST back-calculation
    cc_float                        ref_open;               //!< Reference from open loop back-calculation
    cc_float                        ref_delayed;            //!< ref_limited delayed by rst_pars->ref_delay_periods
    cc_float                        track_delay_periods;    //!< Measured track_delay in regulation periods
    cc_float                        log_time_offset;        //!< Time offset for logged signals that are computed after decoupling (when enabled)
};

//! Voltage regulation structure

struct REG_mgr_voltage
{
    enum   CC_enabled_disabled      regulation;             //!< Option to regulate this signal is enabled or disabled
    cc_float                        unfiltered;             //!< Unfiltered voltage measurement (real or sim)
    struct REG_lim_meas             lim_meas;               //!< Voltage measurement limits
    struct REG_lim_ref              lim_ref;                //!< Generic voltage reference limits
    struct REG_lim_v_ref            lim_v_ref;              //!< Specific voltage reference limits (quadrants41 voltage limits)
    struct REG_v_pars               reg_pars;               //!< Voltage regulation and filter damping parameters
    struct REG_v_vars               reg_vars;               //!< Voltage regulation and filter damping variables
    struct REG_err                  reg_err;                //!< Voltage regulation error = v_meas - v_meas_sim
    struct REG_harmonics            harmonics;              //!< AC voltage harmonics injection
    struct REG_v_ff_fltr            v_ff_fltr;              //!< V feed forward filter parameters and variables
    cc_float                        ref_reg;                //!< Voltage reference from regulator or function generator
    cc_float                        ref_sat;                //!< Voltage reference after saturation compensation
    cc_float                        ref_ff;                 //!< Voltage reference after saturation and feed forward
    cc_float                        ref_deco;               //!< Voltage reference after saturation, feed forward and decoupling
    cc_float                        ref;                    //!< Voltage reference after saturation, feed forward, decoupling and limits
    cc_float                        ref_vs;                 //!< Voltage reference after saturation, feed forward, decoupling, limits and sharing between voltage sources
    cc_float                        ref_rate;               //!< Rate of change of voltage ref
};

//! Load saturation variables structure

struct REG_mgr_sat_vars
{
    cc_float                        last_valid_i_mag_sat_input; //!< Last valid i_mag_sat_input
    cc_float                        i_mag_sat;              //!< Combined i_mag_sat
    cc_float                        sat_factor;             //!< Saturation factor for i_mag_sat
};

//! Simulated signals - taken from simulator 1 (VOLTAGE_REF) or simulator 2 (FIRING_REF)

struct REG_sim_signals
{
    cc_float                    circuit_voltage;                //!< Circuit voltage       (without VS ACT_DELAY_ITERS).
    cc_float                    circuit_current;                //!< Circuit current       (without VS ACT_DELAY_ITERS).
    cc_float                    magnet_field;                   //!< Magnet field          (without VS ACT_DELAY_ITERS).
};

//! Simulation parameters structure

struct REG_mgr_sim_pars
{
    struct REG_sim_vs_pars          vs;                     //!< Simulator 1: Voltage source
    struct REG_sim_load_pars        load;                   //!< Simulator 1: Load
    struct REG_sim2_pars            vs_and_load;            //!< Simulator 2: voltage source and load (for FIRING_REF)
    struct REG_delay_pars           b_delay;                //!< Simulated field measurement delay parameters
    struct REG_delay_pars           i_delay;                //!< Simulated current measurement delay parameters
    struct REG_delay_pars           v_delay;                //!< Simulated voltage and i_capa measurement delay parameters
};

//! Simulation variables structure

struct REG_mgr_sim_vars
{
    struct REG_sim_vs_vars          vs;                     //!< Simulator 1: Voltage source
    struct REG_sim_load_vars        load;                   //!< Simulator 1: Load
    struct REG_sim2_vars            vs_and_load;            //!< Simulator 2: Voltage source and load (for FIRING_REF)
    struct REG_sim_signals          signals;                //!< Simulator 1 or 2 signals (selected according to FIRING_REF)
    struct REG_delay_vars           b_delay;                //!< Simulated field measurement delay variables
    struct REG_delay_vars           i_delay;                //!< Simulated current measurement delay variables
    struct REG_delay_vars           v_delay;                //!< Simulated voltage measurement delay variables
    struct REG_delay_vars           i_capa_delay;           //!< Simulated output filter capacitor current measurement delay variables (uses pars from v_delay)
    cc_float                        b_meas;                 //!< Simulated field measurement
    cc_float                        i_meas;                 //!< Simulated current measurement
    cc_float                        v_meas;                 //!< Simulated voltage measurement
    cc_float                        i_capa_meas;            //!< Simulated output filter capacitor current measurement
};

//! Global regulation manager structure.

struct REG_mgr
{
    cc_double                       iter_period;            //!< Iteration (measurement) period in seconds
    cc_float                        iter_period_fp32;       //!< Iteration (measurement) period in seconds in single-precision float
    cc_float                        inv_iter_period;        //!< 1 / Iteration period

    // Mutex callbacks to protect regMgrParsCheck() and regMgrParsProcess(), which must not be called simultaneously

    void                          * mutex;                  //!< Pointer to mutex structure to pass to mutex_lock and mutex_unlock (NULL is not required)
    REG_mutex_callback            * mutex_lock;             //!< Callback to mutex lock function, if mutex is not NULL
    REG_mutex_callback            * mutex_unlock;           //!< Callback to mutex unlock function, if mutex is not NULL

    // Libreg initialization parameter structures

    struct REG_pars               * pars;                   //!< Libreg parameter structures in application memory
    struct REG_par_values           par_values;             //!< Private copy of all libreg parameter values
    uint32_t                        par_enabled_groups_mask;//!< Mask of parameter groups that are enabled by the application when calling regMgrInit()
    uint32_t                        par_groups_mask;        //!< Bit mask to indicate which parameter groups should be set by regMgrPars()

    // Regulation reference and measurement variables and parameters

    enum   REG_mode                 reg_mode;               //!< Regulation mode: this is the requested regulation state.
    enum   REG_rst_source           reg_rst_source;         //!< RST parameter source. Can be #REG_OPERATIONAL_RST_PARS or #REG_TEST_RST_PARS.
    struct REG_mgr_signal         * reg_signal;             //!< Pointer to currently regulated signal structure. Can be reg_mgr.b or reg_mgr.i.
    struct REG_lim_ref            * lim_ref;                //!< Pointer to the currently active reference limit (b, i or v)
    cc_float                      * ref;                    //!< Pointer to the currently used reference (b, i or v)
    cc_float                        ref_actuation;          //!< Actuation (reg_mgr.v.ref_vs or reg_mgr->v.reg_vars.f_ref)
    cc_float                        vs_act_delay;           //!< VS actuation delay (s)
    int32_t                         reg_period_ns;          //!< Regulation period (ns)
    cc_float                        reg_period;             //!< Regulation period (s)
    int32_t                         ref_advance_ns;         //!< Time to advance reference function (ns)
    cc_float                        ref_advance;            //!< Time to advance reference function (s)
    int32_t                         track_delay_ns;         //!< RST track delay (ns)
    cc_float                        track_delay;            //!< RST track delay (s)
    cc_float                        ref_rate;               //!< Ref rate after a call to regMgrModeSetRT()
    cc_float                        power;                  //!< Current reference or measurement x voltage ref
    cc_float                        max_abs_reg_err;        //!< Absolute regulation error for the actual reg_mode
    cc_float                        abs_reg_err;            //!< Absolute regulation error for the actual reg_mode
    int16_t                         milli_reg_err;          //!< Regulation error * 1000 for the actual reg_mode clipped to signed 16-bit range
    int16_t                         milli_rms_err;          //!< RMS regulation error * 1000 for the actual reg_mode clipped to signed 16-bit range

    // Regulation related flags

    bool                            sim_meas;               //!< True when measurement should be simulated
    bool                            openloop;               //!< True when regulation of field or current is open loop
    bool                            invert_limits;          //!< Limits must be inverted because of the polarity switch
    bool                            ref_clipped_flag;       //!< Reference is being clipped flag
    bool                            ref_rate_clipped_flag;  //!< Reference rate of change is being clipped flag

    // Measurement inputs

    struct REG_mgr_inputs           inputs;                 //!< Measurement inputs

    // Field, current and voltage regulation structures

    struct REG_mgr_signal           b;                      //!< Field regulation parameters and variables
    struct REG_mgr_signal           i;                      //!< Current regulation parameters and variables
    struct REG_mgr_voltage          v;                      //!< Voltage regulation parameters and variables

    // Decoupling structure

    struct REG_deco                 deco;                   //!< Symmetric decoupling structure

    // RMS limits

    struct REG_lim_rms              lim_i_rms;              //!< Converter RMS current limits
    struct REG_lim_rms              lim_i_rms_load;         //!< Load RMS current limits
    struct REG_lim_rms              lim_v_rate_rms;         //!< RMS voltage rate limit

    // Load parameters structures and variables

    struct REG_load_pars            load_pars;              //!< Circuit load model for regulation for LOAD SELECT
    struct REG_load_pars            load_pars_test;         //!< Circuit load model for regulation for LOAD TEST_SELECT
    struct REG_mgr_sat_vars         load_sat_vars;          //!< Load magnet saturation variables

    // Simulation parameters and variables

    struct REG_mgr_sim_pars         sim_pars;               //!< Simulation parameters
    struct REG_mgr_sim_vars         sim_vars;               //!< Simulation variables

    // Debug structure

    union CC_debug                  debug;                  //!< Debug data union
};



//! Calculate the natural frequency in Hertz for a second-order system.
//!
//! @param[in]  bandwidth    Second order system bandwidth.
//! @param[in]  z            Second order system damping.
//!
//! @return     Natural frequency in Hz of the system.

static inline cc_double regNaturalFrequencyHz(cc_float const bandwidth, cc_float const z)
{
    cc_double const z2 = z * z;

    // sqrt cannot fail since 4.0 * z2 * (z2 - 1) is never less than -2

    return bandwidth / sqrt(1.0 - 2.0 * z2 + sqrt(4.0 * z2 * (z2 - 1.0) + 2.0));
}

// EOF
