//! @file  regMgrMode.h
//! @brief Converter Control Regulation library high-level management mode functions.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//! Set the regulation mode.
//!
//! This function does nothing if the requested mode matches the actual mode, or if the
//! requested mode (FIELD or CURRENT) is not enabled (based on LIMITS POS and NEG).
//!
//! Otherwise, the regulation mode is changed and all the relevant regulation variables are adjusted.
//!
//! The reference is adjusted and is available in *reg_mgr->ref.
//!
//! This is a Real-Time function. Note that reading and resetting the flags is not an atomic
//! operation, so it is assumed that this function will be called from one thread only.
//!
//! @param[in,out] reg_mgr              Pointer to regulation manager structure.
//! @param[in]     reg_mode             Regulation mode to set (#REG_NONE, #REG_VOLTAGE, #REG_CURRENT or #REG_FIELD).
//! @param[in]     use_average_v_ref    If reg_mode is set to #REG_VOLTAGE then this flag controls how the voltage reference
//!                                     is initialized. If true, v_ref will be set to the average v_ref in the RST history.
//!                                     If false, it will be set to the most recent v_ref.
//! @returns       new_reg_mode         Actual regulation mode after the function finishes (maybe unchanged)

enum REG_mode regMgrModeSetRT(struct REG_mgr * reg_mgr, enum REG_mode reg_mode, bool use_average_v_ref);

#ifdef __cplusplus
}
#endif

// EOF
