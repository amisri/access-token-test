//! @file regDeco.h
//! @brief Converter Control Regulation library symmetric decoupling functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//  Include structure declarations

#include "libreg/regDecoStructs.h"

//! Decoupling structure

struct REG_deco
{
    uint32_t                          phase;           //!< Iteration to end the regulation - zero if decoupling is disabled
    uint32_t                          new_phase;       //!< New value for phase - it is transfered to phase at the start of the regulation period
    uint32_t                          index;           //!< Controller decoupling index (0-3)
    cc_float                          v_ref;           //!< Voltage reference
    struct REG_deco_shared volatile * shared;          //!< Decoupling data shared with the application
    cc_float                        * k;               //!< Pointer to current decoupling coefficients
    cc_float                        * d;               //!< Pointer to voltage decoupling coefficients
};

// Static inline functions

//! Register decoupling data to share with the partner decoupling controllers
//!
//! @param[in,out]   deco              Pointer to decoupling structure.
//! @param[in]       v_ref             Voltage reference to share.
//! @param[in]       i_meas            Current reference to share.

static inline void regDecoSaveDataRT(struct REG_deco * const deco, cc_float const v_ref, cc_float const i_meas)
{
    // Save v_ref always as it will be returned by regDecoRt() if phase is zero

    deco->v_ref = v_ref;

    // If decoupling algorithm is active

    if(deco->phase > 0)
    {
        // Save the voltage reference and current measurement in the shared structure

        struct REG_deco_shared volatile * const shared = deco->shared;
        uint32_t                          const index  = deco->index;

        shared->v_ref [index] = v_ref;
        shared->i_meas[index] = i_meas;
    }
}

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the decoupling structure
//!
//! The application is responsible for instantiating the shared decoupling data structure
//! and letting libreg know the address of the structure.
//! The decoupling data received from the partner controllers should be stored in the shared data
//! structure and the data to exchange is also contained in it. regDecoSetMyDataRT() will save the
//! elements corresponding to the FGC itself and these must not be overwritten by the application.
//!
//! @param[in,out]   deco              Pointer to decoupling structure.
//! @param[in]       shared            Pointer to shared data structure to exchange decoupling data with the application (can be NULL if Deco not required).
//! @param[in]       k                 Pointer to current decoupling coefficients.
//! @param[in]       d                 Pointer to voltage decoupling coefficients.

void regDecoInit(struct REG_deco                 * deco,
                 struct REG_deco_shared volatile * shared,
                 cc_float                        * k,
                 cc_float                        * d);


//! Set the decoupling parameters
//!
//! Decoupling is enabled by setting the phase to be non-zero. This will only possible if
//! load_select is zero, the index is 0-3 and the phase less than i_period_iters.
//!
//! @param[in,out]   deco              Pointer to decoupling structure.
//! @param[in]       phase             Phase parameter: 0=Off, 1-(i_period_iters-1)=On.
//! @param[in]       index             Controller decoupling index: 0-3.
//! @param[in]       load_select       Load select index.
//! @param[in]       b_period_iters    Field regulation period.
//! @param[in]       i_period_iters    Current regulation period.
//! @param[in]       b_reg_enabled     True if field regulation is possible (LIMITS B_POS or B_NEG is non-zero)
//! @param[in]       i_reg_enabled     True if current regulation is possible (LIMITS I_POS or I_NEG is non-zero)

void regDecoPars(struct REG_deco * deco,
                 uint32_t          phase,
                 uint32_t          index,
                 uint32_t          load_select,
                 uint32_t          b_period_iters,
                 uint32_t          i_period_iters,
                 bool              b_reg_enabled,
                 bool              i_reg_enabled);

//! Calculate the voltage reference taking into account the decoupling
//!
//! This is a Real-Time function.
//!
//! @param[in,out] deco           Pointer to decoupling structure.
//!
//! @returns       V_REF_DECO     The voltage reference to send to the voltage source (after limits)

cc_float regDecoRT(struct REG_deco * deco);

#ifdef __cplusplus
}
#endif

// EOF
