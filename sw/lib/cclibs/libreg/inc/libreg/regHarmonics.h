//! @file regHarmonics.h
//! @brief Converter Control Regulation library AC harmonics functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Constants

#define REG_NUM_HARMONICS           3               //!< Number of harmonics of the AC that can be compensated
#define REG_V_AC_HZ_FLTR_PERIOD     500             //!< Number of iterations for meas_v_ac_hz filtering

//! Harmonics structure

struct REG_harmonics
{
    cc_float   * v_ac_buf;                          //!< Pointer to circular buffer for V_AC (provided by the application)
    uint32_t     buf_len;                           //!< Length of the circular buffer in elements
    uint32_t     buf_index;                         //!< Index of the most recent value in the circular buffer
    uint32_t     ac_period_iters;                   //!< V_AC period in iterations (must be <= buf_len to enable harmonics generation)
    uint32_t     meas_v_ac_hz_counter;              //!< Counter for filtering of meas_v_ac_hz
    bool         enabled;                           //!< True if at least one harmonic gain is non-zero
    cc_float     zero_crossing;                     //!< Fractional index when V_AC cross from below to above zero
    cc_float     gain[REG_NUM_HARMONICS];           //!< Gain for the different harmonics
    cc_float     phase[REG_NUM_HARMONICS];          //!< Phase in iterations for the different harmonics
    cc_float     clip_limit;                        //!< Harmonics reference clip limit
    cc_float     ref;                               //!< Feed forward voltage reference from AC harmonics
    cc_float     v_ac_period_iters_limits[2];       //!< Min/Max limits for V_AC period in iterations (to trigger wrapping)
    cc_float     meas_v_ac_hz_acc;                  //!< Measured frequency of V_AC accumulator
    cc_float     meas_v_ac_hz;                      //!< Unfiltered measured frequency of V_AC
    cc_float     meas_v_ac_hz_fltr;                 //!< Filtered measured frequency of V_AC
    cc_float     iter_period;                       //!< Iteration period - needed for frequency measurement
};


#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the harmonics structure
//!
//! The application is responsible for instantiating the circular buffer to hold the V_AC measurements.
//!
//! @param[in,out]   harmonics         Pointer to harmonics structure.
//! @param[in]       v_ac_buf          Pointer to V_AC circular buffer.
//! @param[in]       buf_len           Length of circular buffer in iterations.

void regHarmonicsInit(struct REG_harmonics * harmonics,
                      cc_float             * v_ac_buf,
                      uint32_t               buf_len);


//! Set the harmonics parameters
//!
//! This is called if any of the harmonics parameters are changed.
//!
//! @param[in,out]   harmonics         Pointer to harmonics structure.
//! @param[in]       ac_period_iters   V_AC period in iterations - clipped to V_AC buffer length
//! @param[in]       iter_period       Iteration period (s)
//! @param[in]       v_pos             Positive voltage limit - used to define harmonics clip limit
//! @param[in]       gain              Pointer to harmonics gain coefficients.
//! @param[in]       phase             Pointer to harmonics phase coefficients (in degrees).

void regHarmonicsPars(struct REG_harmonics * harmonics,
                      uint32_t               ac_period_iters,
                      cc_float               iter_period,
                      cc_float               v_pos,
                      cc_float             * gain,
                      cc_float             * phase);


//! Calculate the feed forward harmonics voltage reference.
//!
//! @param[in,out] harmonics           Pointer to harmonics structure.
//! @param[in]     v_ac                AC voltage measurement for this iteration.
//! @param[in]     generate            True:run the harmonics generator to compute ref_harmonics. False:return zero.

void regHarmonicsRT(struct REG_harmonics * harmonics,
                    cc_float               v_ac,
                    bool                   generate);

#ifdef __cplusplus
}
#endif

// EOF
