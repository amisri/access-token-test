//! @file  ccrt/src/ccCmds.c
//!
//! @brief ccrt command processing functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

// Include ccrt program header files

#include "ccCmds.h"
#include "ccRt.h"
#include "ccParse.h"
#include "ccTest.h"
#include "ccFile.h"
#include "ccLog.h"
#include "ccEvtlog.h"
#include "ccDebug.h"
#include "ccInit.h"
#include "ccRun.h"
#include "ccStatus.h"
#include "ccInput.h"
#include "ccCal.h"

// Local constants

enum CC_cmds_par_action
{
    CC_CMDS_PAR_GET_ALL,
    CC_CMDS_PAR_GET_READ_WRITE,
    CC_CMDS_PAR_GET_READ_ONLY,
    CC_CMDS_PAR_GET_SINGLE_PAR,
    CC_CMDS_PAR_GET_RANGE,
    CC_CMDS_PAR_TEST,
    CC_CMDS_PAR_SET,
    CC_CMDS_PAR_NUM_ACTIONS,
};

//! Check if there's only one char in the string (plus terminating NULL) and if it matches the expected one

static inline bool ccCmdsIsLastChar(char *remaining_line, char const expected)
{
    return remaining_line[0] == expected && remaining_line[1] == '\0';
}


//! Action event

uint32_t ccCmdsActionEvent(uint32_t cmd_idx, char *remaining_line, enum REF_event_type action)
{
    struct CC_ns_time offset;
    struct CC_ns_time iter_time;

    if(ccParsGetIterTimeAndOffset(&iter_time, &offset, &remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Calculate event time

    struct CC_ns_time event_time;

    if(ccRtModeIsBatch() == false)
    {
        // Interactive mode : offset from time now

        event_time = cctimeNsAddRT(iter_time, offset);
    }
    else
    {
        // Batch mode : offset from last wakeup time

        event_time = cctimeNsAddRT(ccrun.wait.last_wakeup_iter_time, offset);
    }

    // Try to arm the action event

    struct CC_us_time event_us_time = cctimeNsToUsRT(event_time);

    ccRunLogEventStart(action);

    bool status = false;

    switch(action)
    {
        case REF_EVENT_PAUSE:           status = refEventPause         (&ref_mgr, &event_us_time); break;
        case REF_EVENT_UNPAUSE:         status = refEventUnpause       (&ref_mgr, &event_us_time); break;
        case REF_EVENT_COAST:           status = refEventCoast         (&ref_mgr, &event_us_time); break;
        case REF_EVENT_RECOVER:         status = refEventRecover       (&ref_mgr, &event_us_time); break;
        case REF_EVENT_START_HARMONICS: status = refEventStartHarmonics(&ref_mgr, &event_us_time); break;
        case REF_EVENT_STOP_HARMONICS:  status = refEventStopHarmonics (&ref_mgr, &event_us_time); break;

        default: ccRtPrintErrorAndExit("ccCmdsActionEvent: unsupported action: %u", action);
    }

    if(status == false)
    {
        if(ccpars_global.hide_errors == CC_DISABLED)
        {
            ccRtPrintError("Failed to set %s event", cmds[cmd_idx].name);
        }
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsPar(uint32_t cmd_idx, char *remaining_line)
{
    enum CC_cmds_par_action action;
    struct CC_pars * par_matched;

    // >CMD_NAME

    if(remaining_line == NULL)
    {
        action = CC_CMDS_PAR_GET_ALL;
    }

    // >CMD_NAME +

    else if(ccCmdsIsLastChar(remaining_line, '+'))
    {
        action = CC_CMDS_PAR_GET_READ_WRITE;
    }

    // >CMD_NAME -

    else if(ccCmdsIsLastChar(remaining_line, '-'))
    {
        action = CC_CMDS_PAR_GET_READ_ONLY;
    }
    else
    {
        // The actions below need a parameter name

        if(ccParseParName(cmd_idx, &remaining_line, &par_matched) == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }

        // >CMD_NAME PAR_NAME

        if(remaining_line == NULL)
        {
            action = CC_CMDS_PAR_GET_SINGLE_PAR;
        }

        // >CMD_NAME PAR_NAME ?

        else if(ccCmdsIsLastChar(remaining_line, '?'))
        {
            if(par_matched->type == PAR_ENUM || par_matched->type == PAR_BITMASK)
            {
                action = CC_CMDS_PAR_GET_RANGE;
            }
            else
            {
                ccRtPrintError("Range can be printed only for enums or bitmasks");
                return EXIT_FAILURE;
            }
        }

        // >CMD_NAME PAR_NAME [=!~%<>] EXPECTED_VALUE

        else if(ccTestIsValidCondition(remaining_line[0]))
        {
            action = CC_CMDS_PAR_TEST;
        }

        // >CMD_NAME PAR_NAME NEW_VALUE

        else if(ccParsIsReadWrite(par_matched))
        {
            action = CC_CMDS_PAR_SET;
        }
        else
        {
            ccRtPrintError("Can't set read-only parameters");
            return EXIT_FAILURE;
        }
    }

    struct CC_cmds const * const cmd       = &cmds[cmd_idx];
    uint32_t               const cyc_sel   = ccfile.cyc_sel;
    uint32_t               const array_idx = ccfile.array_idx;

    switch(action)
    {
        case CC_CMDS_PAR_GET_ALL:

            ccParsGetAll(stderr, cmd, cyc_sel, array_idx, PARS_RW|PARS_RO, 0);
            break;

        case CC_CMDS_PAR_GET_READ_WRITE:

            ccParsGetAll(stderr, cmd, cyc_sel, array_idx, PARS_RW, 0);
            break;

        case CC_CMDS_PAR_GET_READ_ONLY:

            ccParsGetAll(stderr, cmd, cyc_sel, array_idx, PARS_RO, 0);
            break;

        case CC_CMDS_PAR_GET_SINGLE_PAR:

            ccParsGet(stderr, par_matched, cyc_sel, array_idx, par_matched->parent_cmd->name_len, par_matched->name_len);
            break;

        case CC_CMDS_PAR_GET_RANGE:
        {
            // Print current parameter value

            ccParsGet(stderr, par_matched, cyc_sel, array_idx, par_matched->parent_cmd->name_len, par_matched->name_len);

            // Print all possible values

            struct CC_pars_enum const * par_enum = par_matched->ccpars_enum;

            while(par_enum->string != NULL)
            {
                fprintf(stderr, "%s ", par_enum->string);
                par_enum++;
            }

            fputc('\n',stderr);

            break;
        }
        case CC_CMDS_PAR_TEST:

            return ccTestPar(par_matched, remaining_line);

        case CC_CMDS_PAR_SET:

            {
                // Prepare parameter name with (cyc_sel)[array_idx] if required in local string and prepare the event log record

                char evtlog_property[CC_PATH_LEN];

                ccParsFullName(par_matched, evtlog_property, CC_PATH_LEN);

                struct EVTLOG_record * const evtlog_record = evtlogPrepareRecord(&evtlog_data.device, evtlog_property, remaining_line,EVTLOG_STATUS_NORMAL);

                // Run parameter set command

                uint32_t status = ccParsSet(cmd_idx, par_matched, remaining_line);

                // Finish the event log record with the status of the set command

                evtlogFinishRecord(evtlog_record, cctimeNsToUsRT(ccThreadGetIterTime()), status == EXIT_SUCCESS ? "OK" : "ERROR");

                return status;
            }

        default:

            assert(false);
            break;
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsHelp(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    for(cmd_idx = 0 ; cmd_idx < NUM_CMDS ; cmd_idx++)
    {
        fprintf(stderr,"%-*s %s\n", ccfile.max_cmd_name_len, cmds[cmd_idx].name, cmds[cmd_idx].help_message);
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsRead(uint32_t cmd_idx, char *remaining_line)
{
    // Get the first argument (if provided)

    char * filename = ccParseNextArg(&remaining_line);

    // If more than one parameter supplied then return immediately with an error

    if(ccParseNoMoreArgs(&remaining_line))
    {
        return EXIT_FAILURE;
    }

    // Read from the filename (or stdin if filename == NULL)

    return ccFileReadCmds(cmd_idx, filename);
}



uint32_t ccCmdsArm(uint32_t cmd_idx, char *remaining_line)
{
    // If no arguments provided, try to arm cyc_sel 0

    if(remaining_line == NULL)
    {
        ccRtArmRef(ccpars_global.sub_sel, 0, 0, NULL);

        return EXIT_SUCCESS;
     }

    // Check if first argument is a symbol

    char * arg;

    if(*remaining_line >= 'A')
    {
        // Treat arguments as FG_TYPE,PAR_VAL,PAR_VAL,...

        arg = ccParseNextArg(&remaining_line);

        int32_t const enum_idx = ccParsEnumStringToIndex(&ref_pars[REF_PAR_FG_TYPE], arg, false);

        if(enum_idx < 0)
        {
            return EXIT_FAILURE;
        }

        ccpars_ref[ccpars_global.sub_sel][0].fg_type[0] = enum_fg_type[enum_idx].value;

        cc_float par_values[REF_FG_PAR_MAX_ARM_PAR_VALUES];
        uint32_t num_pars = 0;

        while((arg = ccParseNextArg(&remaining_line)) != NULL)
        {
            if(num_pars < REF_FG_PAR_MAX_ARM_PAR_VALUES)
            {
                cc_double double_value;

                if(ccParsDouble(arg, &double_value) == EXIT_FAILURE)
                {
                    return EXIT_FAILURE;
                }

                par_values[num_pars] = double_value;
            }

            num_pars++;
        }

        ccRtArmRef(ccpars_global.sub_sel, 0, num_pars, par_values);
    }
    else
    {
        // else treat arguments as a series of cycle selectors

        while((arg = ccParseNextArg(&remaining_line)) != NULL)
        {
            long cyc_sel;

            if(ccParsLong(arg, &cyc_sel, 0) == EXIT_FAILURE || cyc_sel < 0 || cyc_sel > CC_MAX_CYC_SEL)
            {
                ccRtPrintError("Invalid cycle selector (%s)", arg);
                return EXIT_FAILURE;
            }

            ccRtArmRef(ccpars_global.sub_sel, (uint32_t)cyc_sel, 0, NULL);
        }
    }

    // ARM command reports success, even if the arming failed

    return EXIT_SUCCESS;
}



uint32_t ccCmdsRestore(uint32_t cmd_idx, char *remaining_line)
{
    // If no arguments provided, try to restore cyc_sel 0

    if(remaining_line == NULL)
    {
        refRestore(&ref_mgr, ccpars_global.sub_sel, 0, ccrun.fg_pars_idx[ccpars_global.sub_sel][0]);
    }

    // else treat arguments as a series of cycle selectors

    else
    {
        char * arg;

        while((arg = ccParseNextArg(&remaining_line)) != NULL)
        {
            long cyc_sel;

            if(ccParsLong(arg, &cyc_sel, 0) == EXIT_FAILURE || cyc_sel < 0 || cyc_sel > CC_MAX_CYC_SEL)
            {
                ccRtPrintError("Invalid cycle selector (%s)", arg);
                return EXIT_FAILURE;
            }

            refRestore(&ref_mgr, ccpars_global.sub_sel, (uint32_t)cyc_sel, ccrun.fg_pars_idx[ccpars_global.sub_sel][cyc_sel]);
        }
    }

    // RESTORE command reports success always

    return EXIT_SUCCESS;
}



uint32_t ccCmdsRun(uint32_t cmd_idx, char *remaining_line)
{
    struct CC_ns_time iter_time;
    struct CC_ns_time offset;

    // Allow run time offset as an argument.

    if(ccParsGetIterTimeAndOffset(&iter_time, &offset, &remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Calculate run time

    struct CC_ns_time run_time;

    if(   offset.secs.rel < 0
       || (offset.secs.rel == 0 && offset.ns == 0))
    {
        // No offset or negative offset : set run time to zero which
        // tells refEventRun to set the event 1s in the future

        run_time = cc_zero_ns;
    }
    else if(ccRtModeIsBatch() == false)
    {
        // Interactive mode : offset from time now

        run_time = cctimeNsAddRT(iter_time, offset);
    }
    else
    {
        // Batch mode : offset from last wakeup time

        run_time = cctimeNsAddRT(ccrun.wait.last_wakeup_iter_time, offset);
    }

    // Bail out if the calculated run time is in the past

    struct CC_ns_time delta_time = cc_zero_ns;

    if(run_time.secs.abs > 0)
    {
        delta_time = cctimeNsSubRT(iter_time, run_time);

        if(cctimeNsCmpRelTimesRT(delta_time, cc_zero_ns) > 0)
        {
            char strbuf[22];

            ccRtPrintError("Run time is already %s s in the past", cctimeNsPrintRelTime(delta_time, strbuf));
            return EXIT_FAILURE;
        }
    }

    // Issue Run event

    ccRunLogEventStart(REF_EVENT_RUN);

    refMgrParValue(&ref_mgr, REF_RUN) = cctimeNsToUsRT(run_time);

    if(refEventRun(&ref_mgr) == false)
    {
        if(ccpars_global.hide_errors == CC_DISABLED)
        {
            ccRtPrintError("No armed reference function is ready to run in the current state");
        }

        return EXIT_FAILURE;
    }

    char strbuf[3][22];
    ccRtPrintInVerbose("%s run : event_time_s=%s margin=%s",
                        cctimeNsPrintRelTime(cctimeNsSubRT(iter_time, ccrun.start_time), strbuf[0]),
                        cctimeNsPrintRelTime(cctimeNsSubRT(cctimeUsToNsRT(refMgrParValue(&ref_mgr, REF_RUN)), ccrun.start_time), strbuf[1]),
                        cctimeNsPrintRelTime(delta_time, strbuf[2]));

    ccRtWaitForRefStateMachine();

    return EXIT_SUCCESS;
}



uint32_t ccCmdsAbort(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Set abort flag - it is only significant in ARMED, RUNNING and PAUSED

    ccRunLogEventStart(REF_EVENT_ABORT);

    bool status = refEventAbort(&ref_mgr);

    ccRtWaitForRefStateMachine();

    if(status == false)
    {
        ccRtPrintError("Bad state for abort");
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsPause(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_PAUSE);
}



uint32_t ccCmdsUnpause(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_UNPAUSE);
}



uint32_t ccCmdsCoast(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_COAST);
}



uint32_t ccCmdsRecover(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_RECOVER);
}



uint32_t ccCmdsStartHarmonics(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_START_HARMONICS);
}



uint32_t ccCmdsStopHarmonics(uint32_t cmd_idx, char *remaining_line)
{
    return ccCmdsActionEvent(cmd_idx, remaining_line, REF_EVENT_STOP_HARMONICS);
}



uint32_t ccCmdsReset(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Reset simulated current

    ccsim.sim_vars.load.integrator = 0.0;
    memset(&ccsim.sim_vars.vs_and_load, 0, sizeof(ccsim.sim_vars.vs_and_load));

    // Reset faults

    refMgrResetFaultsRT(&ref_mgr);

    ccRtWaitForRefStateMachine();

    return EXIT_SUCCESS;
}



uint32_t ccCmdsWaitFuncTime(uint32_t cmd_idx, char *remaining_line)
{
    char *arg;
    struct CC_ns_time func_time = { { 100000000 }, 0 };         // Default is waits for end of the function

    if(remaining_line != NULL)
    {
        arg = ccParseNextArg(&remaining_line);

        if(ccParseNoMoreArgs(&remaining_line))
        {
            return EXIT_FAILURE;
        }

        if(ccParsRelTime(arg, &func_time) == EXIT_FAILURE || func_time.secs.rel < -10000 || func_time.secs.rel > 10000)
        {
            ccRtPrintError("Invalid function time: %s", ccParseAbbreviateArg(arg));
            return EXIT_FAILURE;
        }
    }

    ccRtWaitForRefStateMachine();
    ccRtWaitForRefStateMachine();

    // Set func time and wait for ccRun() to wake thread using condition variable

    ccThreadWaitUntil(CMD_WAIT_WAITFUNCTIME, func_time, cc_zero_ns, "WaitFuncTime");

    return EXIT_SUCCESS;
}



uint32_t ccCmdsWait(uint32_t cmd_idx, char *remaining_line)
{
    // Get iter_time_s offset by the parameter with respect to the last wake-up time (+1s is the default)

    struct CC_ns_time wait_time;

    if(ccParsGetSleepOffset(&wait_time, &remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // If more arguments remain, they can specify a parameter test: "par condition value(s)"

    bool test_result = false;
    uint32_t exit_status;

    if(remaining_line != NULL &&
       ((exit_status = ccTestPrepareWait(remaining_line, &test_result)) == EXIT_FAILURE || test_result == true))
    {
        ccrun.wait.test.condition = '\0';

        return exit_status;
    }

    ccThreadWaitUntil(CMD_WAIT_WAIT, wait_func_time_not_set, wait_time, "Wait");

    ccrun.wait.test.condition = '\0';

    return EXIT_SUCCESS;
}



uint32_t ccCmdsSync(uint32_t cmd_idx, char *remaining_line)
{
    // Get iter_time_s offset by the parameter with respect to the last wake-up time (+1s is the default)

    struct CC_ns_time wait_time;

    if(ccParsGetSleepOffset(&wait_time, &remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    if(remaining_line == NULL)
    {
        ccRtPrintError("No command supplied");
        return EXIT_FAILURE;
    }

    ccrun.wait.sync_command = remaining_line;

    ccThreadWaitUntil(CMD_WAIT_SYNC, wait_func_time_not_set, wait_time, "Sync");

    ccRtWaitForRefStateMachine();

    return ccrun.wait.sync_command_exit_status;
}



uint32_t ccCmdsWriteLogs(uint32_t cmd_idx, char *remaining_line)
{
    char * log_dir_name = ccParseNextArg(&remaining_line);

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Trigger log writing thread

    ccLogWakeThread(LOG_READ_GET_SPY, log_dir_name);

    // Wait for logging to complete

    while(ccrun.log.in_progress == true)
    {
        // Sleep for 100 ms

        usleep(100000);
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsEnable(uint32_t cmd_idx, char *remaining_line)
{
    char * arg;

    // Try to enable the logs named in the arguments

    while((arg = ccParseNextArg(&remaining_line)) != NULL)
    {
        if(ccLogEnableDisable(arg, true) == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsDisable(uint32_t cmd_idx, char *remaining_line)
{
    char * arg;

    // Try to disable the logs named in the arguments

    while((arg = ccParseNextArg(&remaining_line)) != NULL)
    {
        if(ccLogEnableDisable(arg, false) == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}



uint32_t ccCmdsSigNames(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    ccLogWriteSigNames(stderr);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsSave(uint32_t cmd_idx, char *remaining_line)
{
    static const char * const default_filename = "ccrt_pars";

    char const * arg = ccParseNextArg(&remaining_line);

    // If no argument supplied then use the default filename

    if(arg == NULL)
    {
        arg = default_filename;
    }
    else if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    FILE * const f = ccFileOpenTextFile(CC_TMP_CFG_FILE);

    // Save all parameters used to arm the function

    fprintf(stderr,"Writing all parameters to '%s'\n", arg);

    fprintf(f,"# CCRT %s %s\n\n",__DATE__, __TIME__);

    for(struct CC_cmds * cmd = cmds ; cmd->name != NULL ; cmd++)
    {
        if(cmd->pars != NULL)
        {
            // Get all parameters that have the CFG flag, but NOT the SUB_SEL flags set

            ccParsGetAllMatchingFlags(f, cmd, 0, CC_NO_INDEX, PARS_CFG|PARS_SUB_SEL, PARS_CFG, ccfile.max_cmd_name_len, ccfile.max_par_name_len);
        }
    }

    fputs("\n# EOF\n",f);

    // Try to rename temporary file to the user's filename

    ccFileCloseAndReplace(f, CC_TMP_CFG_FILE, arg);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsStatus(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Display status information

    ccStatus(false);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsEvtStore(uint32_t cmd_idx, char *remaining_line)
{
    return ccEvtlogStore(remaining_line);
}



uint32_t ccCmdsEvtLog(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    ccEvtlogPrintFgc(stderr);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsEvtDebug(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    ccEvtlogDebug();

    return EXIT_SUCCESS;
}



uint32_t ccCmdsCalAdcs(uint32_t cmd_idx, char *remaining_line)
{
    // No arguments expected

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    return ccCalAdcs();
}



uint32_t ccCmdsCalDccts(uint32_t cmd_idx, char *remaining_line)
{
    // There should be only one argument for this command

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    return ccCalDccts();
}



uint32_t ccCmdsCalDac(uint32_t cmd_idx, char *remaining_line)
{
    // There should be no arguments for this command

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    return ccCalDac();
}



uint32_t ccCmdsFifo(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Write ADC A circular buffer data to stderr

    struct SIG_adc_raw adc_raw;

    while(sigAdcGetCircBuf(&sig_struct.adc.array[0], &adc_raw))
    {
        fprintf(stderr,"%10u.%03u %10d %10d\n",adc_raw.timestamp.secs.abs, adc_raw.timestamp.ns/1000000, adc_raw.meas, adc_raw.peak_to_peak);
    }

    // Return failure to close current file

    return EXIT_FAILURE;
}



uint32_t ccCmdsIlcRst(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Reset the ILC

    refIlcReset(&ref_mgr);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsDebug(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Write debug data to stderr with space delimiter

    ccDebug(stderr, ' ');

    return EXIT_SUCCESS;
}



uint32_t ccCmdsUnfreeze(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Clear and unfreeze PM logs

    logClearFreezablePostmortemLogs(&log_buffers);
    logUnfreezePostmortemLogs(&log_mgr);

    return EXIT_SUCCESS;
}



uint32_t ccCmdsExit(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // If processing commands from stdin then quit immediately

    if(ccInputIsStdin())
    {
        ccCmdsQuit(0, remaining_line);
    }

    // Return CC_EXIT_FILE to close current file

    return CC_EXIT_FILE;
}



uint32_t ccCmdsQuit(uint32_t cmd_idx, char *remaining_line)
{
    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    fputs("Stopping ccrt.\n", stderr);
    exit(EXIT_SUCCESS);
}

// EOF
