//! @file  ccrt/src/ccSim.c
//!
//! @brief ccrt real-time simulation functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// #define CCPRINTF

#include <signal.h>

// Include ccrt program header files

#include "ccThread.h"
#include "ccCmds.h"
#include "ccRt.h"
#include "ccFile.h"
#include "ccParse.h"
#include "ccLog.h"
#include "ccRun.h"
#include "ccSim.h"
#include "ccNoise.h"

enum CC_sim_cal_error
{
    SIM_OFFSET_ERR,
    SIM_POS_GAIN_ERR,
    SIM_NEG_GAIN_ERR,
};

// Static variables

struct CC_sim_tone
{
    cc_float                    accumulator;                    //!< Iteration counter in range +/-half_period_iters
    cc_float                    period_iters;                   //!< Tone period in iterations
    cc_float                    half_period_iters;              //!< Half tone period in iterations
} sim_tone[TONE_NUM_SOURCES];

// Static function definitions

//! This function returns the primary turns for a DCCT.
//!
//! Primary turns can be specified as a value or a pointer to a value. This covers both cases.
//!
//! @return primary turns as a float

static cc_float ccSimDcctPrimaryTurnsRT(uint32_t const dcct_select)
{
    // Calculate primary turns and return 1.0 if the value is zero

    uint32_t * const primary_turns_p = sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_PRIMARY_TURNS_POINTER);

    uint32_t   const primary_turns   =  primary_turns_p != NULL
                                     ? *primary_turns_p
                                     : sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_PRIMARY_TURNS);

    return primary_turns > 1 ? (cc_float)primary_turns : 1.0;
}



//! This function simulates the calibration current based on the value of SIM REF_ERR_PPM parameter.
//!
//! @return  Simulated calibration current

static cc_float ccSimCalibrationCurrentRT(uint32_t const dcct_select)
{

    // Calculate the calibration current using the DCCT nominal gain and the external calibration reference error

    switch(ccpars_sim.dcct_cal)
    {
        case SIG_CAL_POSITIVE:

            // Current corresponding to positive full scale + external calibration reference error

            return SIG_CAL_V_NOMINAL * (1.0 + sigVarValue(&sig_struct, cal_ref, external, CAL_REF_EXT_CAL_ERR) * 1.0E-6) *
                                              sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_NOMINAL_GAIN) /
                                              ccSimDcctPrimaryTurnsRT(dcct_select);

        case SIG_CAL_NEGATIVE:

            // Current corresponding to negative full scale + external calibration reference error

            return -SIG_CAL_V_NOMINAL * (1.0 + sigVarValue(&sig_struct, cal_ref, external, CAL_REF_EXT_CAL_ERR) * 1.0E-6) *
                                               sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_NOMINAL_GAIN) /
                                               ccSimDcctPrimaryTurnsRT(dcct_select);

        case SIG_CAL_OFFSET:

            // Current corresponding to external calibration reference error. This is defined in PPM of the nominal current

            return SIG_CAL_V_NOMINAL * sigVarValue(&sig_struct, cal_ref, external, CAL_REF_EXT_CAL_ERR) * 1.0E-6 *
                                       sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_NOMINAL_GAIN) /
                                       ccSimDcctPrimaryTurnsRT(dcct_select);

        default:
            break;
    }

    return 0.0;
}



// This function simulates the temperature error in ADCs and DCCTs.
//
// The compensation can be linear or parabolic. For second order compensation, the offset from linear is defined
// by dtc at T2. The offset from linear will be zero by definition at T0 and T2.
//
// @param[in]  tc            First order temperature coefficient (ppm/C)
// @param[in]  dtc           Second order temperature coefficient (ppm offset from linear tc at temperature T1)
// @param[in]  temp_c        Actual temperature
//
// @return  Temperature error in ppm

static cc_float ccSimTempErrorPpm(cc_float const tc, cc_float const dtc, cc_float const temp_c)
{
    // If second order compensation is specified

    if(dtc != 0.0)
    {
        // Use second order parabolic temperature compensation function

        return (temp_c - SIG_TEMP_CAL_T0) * (tc + dtc * (SIG_TEMP_CAL_T2 - temp_c) *
               (1.0/((SIG_TEMP_CAL_T1 - SIG_TEMP_CAL_T0) * (SIG_TEMP_CAL_T2 - SIG_TEMP_CAL_T1))));
    }

    // else use linear temperature compensation function

    return (temp_c - SIG_TEMP_CAL_T0) * tc;
}



//! The function calculates offset, positive and negative gain errors for ADCs and DCCTs
//!
//! @param[in] err        Structure with ppm error values and temperature time constants
//! @param[in] err_index  Type of error to calculate
//! @param[in] temp       Actual temperature of the ADC/DCCT
//!
//! @return  Value of the requested error

static cc_float ccSimCalcError(struct CC_sim_error  const * const err,
                               enum CC_sim_cal_error        const err_index,
                               cc_float                     const temp)
{
    return (err->err_ppm[err_index] + ccSimTempErrorPpm(err->tc[err_index], err->dtc[err_index], temp)) * 1.0E-6;
}



//! This function back-calculates v_dac based on raw_dac value passed to it.
//!
//! @param[in] sim_dac  Structure with simulated DAC gains and offset
//! @param[in] raw_dac  DAC raw value
//!
//! @return  v_dac value

static cc_float ccSimDac(struct CC_sim_dac const * const sim_dac, int32_t const raw_dac)
{
    cc_float v_dac;

    if(raw_dac >= 0)
    {
        v_dac = raw_dac / sim_dac->pos_raw_per_volt;
    }
    else
    {
        v_dac = raw_dac / sim_dac->neg_raw_per_volt;
    }

    return v_dac + sim_dac->v_offset;
}



//! The function applies the calibration coefficients of a DCCT to the circuit current in order to obtain
//! a voltage (v_adc) representing this current, which then can be fed to an ADC.
//!
//! @param[in] dcct_select   Libsig transducer index for the DCCT being simulated
//! @param[in] dcct_err      Pointer to simulated DCCT calibration error structure
//! @param[in] i_dcct        Actual circuit current
//! @param[in] dcct_temp     Actual temperature of the DCCT
//!
//! @return  v_adc value

static cc_float ccSimDcctRT(uint32_t                    const dcct_select,
                            struct CC_sim_error const * const dcct_err,
                            cc_float                    const i_dcct,
                            cc_float                    const dcct_temp)
{
    // Calculate offset and gain errors for the specified temperature

    cc_float const offset_err   = ccSimCalcError(dcct_err, SIM_OFFSET_ERR,   dcct_temp);
    cc_float const pos_gain_err = ccSimCalcError(dcct_err, SIM_POS_GAIN_ERR, dcct_temp);
    cc_float const neg_gain_err = ccSimCalcError(dcct_err, SIM_NEG_GAIN_ERR, dcct_temp);

    // Calculate intermediate V_DCCT

    enum CC_enabled_disabled const * ignore_gain_err_during_cal = sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_IGNORE_GAIN_ERR_DURING_CAL);

    cc_float const gain_err_ppm = (ignore_gain_err_during_cal == NULL || *ignore_gain_err_during_cal == CC_DISABLED) ?
                                  sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_GAIN_ERR_PPM) : 0.0;

    cc_float const v_dcct = i_dcct * ccSimDcctPrimaryTurnsRT(dcct_select) * (1.0 + gain_err_ppm * 1.0e-6) /
                            sigVarValueByIdx(&sig_struct, transducer, dcct_select, TRANSDUCER_NOMINAL_GAIN);

    // Calculate the DCCT electronics offset first

    cc_float v_adc  = offset_err * SIG_CAL_V_NOMINAL;

    if(v_dcct >= 0.0)
    {
        v_adc += (1.0 + pos_gain_err) * v_dcct;
    }
    else
    {
        v_adc += (1.0 + neg_gain_err) * v_dcct;
    }

    return v_adc;
}



//! This function applies the calibration coefficients of an ADC to the input voltage (v_adc) in order to obtain
//! a raw value (raw_adc) representing this voltage.
//!
//! @param[in] sim_adc   Simulated ADC structure
//! @param[in] v_adc     v_adc value from the multiplexer
//! @param[in] adc_temp  Actual temperature of the ADC
//!
//! @return  raw_adc value

static int32_t ccSimAdc(struct CC_sim_adc const * const sim_adc,
                        cc_float                        v_adc,
                        cc_float                  const adc_temp)
{
    cc_float const offset_err   = ccSimCalcError(&sim_adc->err, SIM_OFFSET_ERR,   adc_temp);
    cc_float const pos_gain_err = ccSimCalcError(&sim_adc->err, SIM_POS_GAIN_ERR, adc_temp);
    cc_float const neg_gain_err = ccSimCalcError(&sim_adc->err, SIM_NEG_GAIN_ERR, adc_temp);

    cc_double raw_adc = offset_err * (cc_float)sim_adc->nominal_gain;

    if(v_adc >= 0.0)
    {
        // Clip ADC voltage at 10% over the nominal value

        if(v_adc > (SIG_CAL_V_NOMINAL * 1.1))
        {
            v_adc = (SIG_CAL_V_NOMINAL * 1.1);
        }

        // Calculate raw ADC value from the voltage using the positive gain

        raw_adc += (1.0 + pos_gain_err) * (cc_float)sim_adc->nominal_gain * v_adc / SIG_CAL_V_NOMINAL;
    }
    else
    {
        // Clip ADC voltage at 10% over the nominal value

        if(v_adc < -(SIG_CAL_V_NOMINAL * 1.1))
        {
            v_adc = -(SIG_CAL_V_NOMINAL * 1.1);
        }

        // Calculate raw ADC value from the voltage using the negative gain

        raw_adc += (1.0 + neg_gain_err) * (cc_float)sim_adc->nominal_gain * v_adc / SIG_CAL_V_NOMINAL;
    }

    return (int32_t)nearbyint(raw_adc);
}



//! This real-time function quantizes a signal into steps.
//!
//! @param[in] signal            Signal to be quantized
//! @param[in] quantization      Quantization step size
//!
//! @return  Quantized signal

static cc_float ccSimQuantizationRT(cc_float signal, cc_float const quantization)
{
    if(quantization > 0.0)
    {
        signal = quantization * nearbyintf(signal / quantization);
    }

    return signal;
}



//! This real-time function generates a normalized tone with the specified period
//!
//! @param[in] sim_tone              Pointer to tone structure
//! @param[in] tone_period_iters     Tone period in iterations
//!
//! @return    Normalized tone (peak-peak = 1, average = 0)


static cc_float ccSimToneRT(struct CC_sim_tone * const sim_tone, cc_float const tone_period_iters)
{
    // If tone period has changed then recalculate tone parameters

    if(tone_period_iters != sim_tone->period_iters)
    {
        if(tone_period_iters > 1.0 && tone_period_iters <= 10000.0)
        {
            sim_tone->period_iters      =  tone_period_iters;
            sim_tone->half_period_iters =  tone_period_iters * 0.5;
        }
        else
        {
            // Set iter_counter_start to deactivate the tone

            sim_tone->period_iters = 0.0;
        }

        sim_tone->period_iters = tone_period_iters;
    }

    // Use efficient square tone generator to create roughly square wave tone with zero average value

    cc_float tone;

    if(sim_tone->period_iters > 0.0)
    {
        sim_tone->accumulator += 1.0;

        if(sim_tone->accumulator >= sim_tone->half_period_iters)
        {
            sim_tone->accumulator -= sim_tone->period_iters;
        }

        tone = sim_tone->accumulator >= 0.0 ? 0.5 : -0.5;
    }
    else
    {
        tone = 0.0;
    }

    // Return normalized tone

    return tone;
}



//! This real-time function simulates the effect of the polarity switch on tranducer signals. These are all simulated on the load
//! and must be inverted if the polarity switch is inverted and the transducer position is CONVERTER.
//!
//! @param[in] transducer            Pointer to transducer structure
//! @param[in] circuit_signal        Transducer signal simulated on the circuit side of the polarity switch
//!
//! @return    Transducer signal as measured by the transducer

static cc_float ccSimPolswitchOnTransducerRT(struct SIG_transducer * const transducer, cc_float const circuit_signal)
{
    if(polswitchIsNegative(&polswitch_mgr) == true && transducer->position == SIG_TRANSDUCER_POSITION_CONVERTER)
    {
        return -circuit_signal;
    }
    else
    {
        return circuit_signal;
    }
}



//! This real-time function simulates the voltage source and load response when PC ACTUATION is VOLTAGE_REF
//!
//! @param[in]     sim_pars             Pointer simulation parameters
//! @param[in,out] sim_vars             Pointer to simulation variables
//! @param[in]     v_ref                Voltage reference
//! @param[in]     set_vfeedfwd_flag   Set vfeedfwd when true

static void ccSimVoltageSourceRT(struct REG_mgr_sim_pars const * const sim_pars,
                                 struct REG_mgr_sim_vars       * const sim_vars,
                                 cc_float                        const v_ref,
                                 bool                            const set_vfeedfwd_flag)
{
    // Simulate the voltage source and polarity switch

    cc_float v_circuit = regSimVsRT(&sim_pars->vs, &sim_vars->vs, v_ref);

    // When the converter is running

    if(ccSimIsPcOn())
    {
        // Simulate voltage source quantization, noise and tone with VS period (normally linked to switching ripple)

        v_circuit = ccSimQuantizationRT(v_circuit, ccpars_sim.vs_quantization) +
                    ccpars_sim.noise_vs         * ccNoiseRT() +
                    ccpars_sim.tone_pp[TONE_VS] * ccSimToneRT(&sim_tone[TONE_VS], ccpars_sim.vs_tone_period_iters);
    }

    // Add voltage perturbation

    ccpars_sim.vs_perturbation += ccpars_sim.vs_perturbation_rate * reg_mgr.iter_period;

    v_circuit += ccpars_sim.vs_perturbation;

    // Use negative vs_perturbation as the vfeedfwd signal - the validity is controlled by SIM VFEEDFWD_VALID

    if(set_vfeedfwd_flag == true)
    {
        ccpars_sim.vfeedfwd.signal = -ccpars_sim.vs_perturbation;
    }

    // If the circuit is open, no current can flow so reset the simulation integrator

    if(polswitchVarValue(&polswitch_mgr, STATE) == POLSWITCH_STATE_MOVING)
    {
        v_circuit                 = 0.0;
        sim_vars->load.integrator = 0.0;
    }

    // Simulate the load

    regSimLoadRT(&sim_pars->load, &sim_vars->load, sim_pars->vs.is_vs_undersampled, v_circuit, regMgrVarValue(&reg_mgr, LOAD_I_MAG_SAT));

    // Transfer signals to be used to create simulated measurements

    sim_vars->signals.circuit_voltage = v_circuit;
    sim_vars->signals.circuit_current = sim_vars->load.circuit_current;
    sim_vars->signals.magnet_field    = regLoadCurrentToFieldRT(&sim_pars->load.pars, sim_vars->load.magnet_current);
 }



//! This real-time function simulates firing, converter and load response when PC ACTUATION is FIRING_REF
//!
//! @param[in]     sim_pars        Pointer simulation parameters
//! @param[in,out] sim_vars        Pointer to simulation variables
//! @param[in]     f_ref_limited   Limited firing reference

static void ccSimFiringSourceRT(struct REG_mgr_sim_pars const * const sim_pars,
                                struct REG_mgr_sim_vars       * const sim_vars,
                                cc_float                              f_ref_limited)
{
    if(polswitchVarValue(&polswitch_mgr, STATE) == POLSWITCH_STATE_MOVING)
    {
        // Circuit is open, no current can flow so reset the simulated state vector

        f_ref_limited = 0.0;

        memset(&sim_vars->vs_and_load, 0, sizeof(sim_vars->vs_and_load));
    }
    else
    {
        // Circuit is closed, add noise and tone to firing reference

        f_ref_limited += ccpars_sim.noise_vs         * ccNoiseRT() +
                         ccpars_sim.tone_pp[TONE_VS] * ccSimToneRT(&sim_tone[TONE_VS], ccpars_sim.vs_tone_period_iters);

        // Add voltage perturbation

        ccpars_sim.vs_perturbation += ccpars_sim.vs_perturbation_rate * reg_mgr.iter_period;

        f_ref_limited += ccpars_sim.vs_perturbation;
    }

    // Make response non-linear for ASAD using a cosine function

    if(regMgrParAppValue(&reg_pars,VS_FIRING_MODE) == REG_ASAD)
    {
        if(regMgrVarValue(&reg_mgr, FLAG_ASAD_1Q_WITHOUT_LUT) == true)
        {
            // 1-quadrant converter with ASAD firing mode and no LUT defined
            // Map [-f_pos..f_pos] to [0..f_pos]

            f_ref_limited = (f_ref_limited + regMgrVarValue(&reg_mgr, VREG_OP_F_POS)) * 0.5F;
        }
        else
        {
            cc_float const non_linear_f_ref_limited = 5.0F * reg_mgr.v.reg_pars.vs_gain * (1.0F - cosf(fabsf(f_ref_limited * reg_mgr.v.reg_pars.inv_vs_gain) * 0.314159265F));

            f_ref_limited = f_ref_limited >= 0.0F
                          ?  non_linear_f_ref_limited
                          : -non_linear_f_ref_limited;
        }
    }

    // Simulator 2 - Simuate voltage source and load

    regSim2RT(&ccsim.vs_and_load_pars,
              &sim_vars->vs_and_load,
              f_ref_limited,
              regMgrVarValue(&reg_mgr,MEAS_V_UNFILTERED),
              regMgrVarValue(&reg_mgr,MEAS_I_UNFILTERED),
              regMgrVarValue(&reg_mgr,MEAS_I_CAPA_UNFILTERED));

    // Transfer signals to be used to create simulation measurements

    sim_vars->signals.circuit_voltage = sim_vars->vs_and_load.circuit_voltage;
    sim_vars->signals.circuit_current = sim_vars->vs_and_load.circuit_current;
    sim_vars->signals.magnet_field    = regLoadCurrentToFieldRT(&sim_pars->load.pars, sim_vars->vs_and_load.magnet_current);
}



// External function definitions

void ccSimInit(void)
{
    // Initialize simulated ADC nominal gains to the default values for the libsig ADCs (from def/sig.csv)
    // The libsig values can be zeroed and recalculated, but the simulated ADC values will be fixed from now on.

    ccsim.adc[SIG_ADC_ADC_A].nominal_gain = sigVarValue(&sig_struct, adc, adc_a, ADC_NOMINAL_GAIN);
    ccsim.adc[SIG_ADC_ADC_B].nominal_gain = sigVarValue(&sig_struct, adc, adc_b, ADC_NOMINAL_GAIN);
    ccsim.adc[SIG_ADC_ADC_C].nominal_gain = sigVarValue(&sig_struct, adc, adc_c, ADC_NOMINAL_GAIN);
    ccsim.adc[SIG_ADC_ADC_D].nominal_gain = sigVarValue(&sig_struct, adc, adc_d, ADC_NOMINAL_GAIN);

    // Initialize the ADC calibration errors to match the simulated values

    for(uint32_t idx = 0 ; idx < SIG_NUM_ADCS ; idx++)
    {
        memcpy(sig_struct.adc.array[idx].cal.event.f, ccsim.adc[idx].err.err_ppm, SIG_NUM_CALS * sizeof(cc_float));
    }

    // Initialize the DCCT calibration errors to match the simulated values

    memcpy(sig_struct.transducer.named.dcct_a.cal.event.f, ccsim.dcct_err[0].err_ppm, SIG_NUM_CALS * sizeof(cc_float));
    memcpy(sig_struct.transducer.named.dcct_b.cal.event.f, ccsim.dcct_err[1].err_ppm, SIG_NUM_CALS * sizeof(cc_float));

    // Initialize the DAC calibration

    sigDacInit(&dac, ccsim.dac.resolution, ccsim.dac.v_nominal, CC_MAX_DAC_V_ERROR);

    sigDacCal(&dac, ccsim.cal_dac_raw[0],  CAL_DAC_OFFSET);
    sigDacCal(&dac, ccsim.cal_dac_raw[1],  CAL_DAC_VOLTS);
    sigDacCal(&dac, ccsim.cal_dac_raw[2], -CAL_DAC_VOLTS);

    // Get a copy of reg_mgr variables. We are going to run two circuit simulations in parallel -
    // one in libreg and one in ccrt

    ccsim.sim_vars = reg_mgr.sim_vars;
}



void ccSimPcStateRT(void)
{
    // Fault state if polarity switch fault is latched or When ref_state is OFF and a libref fault is active

    if(   polswitchVarValue(&polswitch_mgr,LATCHED_FAULTS) != 0
       || (   refMgrVarValue(&ref_mgr, REF_FAULTS_IN_OFF)  != 0
           && refMgrVarValue(&ref_mgr, REF_STATE) == REF_OFF))
    {
        refMgrParValue(&ref_mgr, PC_STATE) = REF_PC_FAULT;
    }
    else
    {
        // Delay switching on but not switching off

        if(ccpars_mode.pc != refMgrParValue(&ref_mgr,PC_STATE))
        {
            ccsim.pcstate.timer += regMgrVarValue(&reg_mgr, ITER_PERIOD);

            if(ccpars_mode.pc == REF_PC_OFF || ccsim.pcstate.timer > 1.5F)
            {
                refMgrParValue(&ref_mgr, PC_STATE) = ccpars_mode.pc;
                ccsim.pcstate.timer = 0.0;
            }
        }
    }
}



void ccSimCircuitRT(bool const set_vfeedfwd_flag)
{
    struct REG_mgr_sim_pars const * const sim_pars = &reg_mgr.sim_pars;          // Pointer to parameters needed for simulation
    struct REG_mgr_sim_vars       * const sim_vars = &ccsim.sim_vars;            // Pointer to variables needed for simulation

    int32_t raw_dac;

    // If the command thread signals that the DAC is being calibrated then set raw_dac to the
    // value corresponding to the current calibration level

    if(ccsim.cal_dac)
    {
      raw_dac = ccsim.cal_dac_raw[ccsim.cal_dac_level];
    }
    else
    {
      raw_dac = sigDacRT(&dac, refMgrVarValue(&ref_mgr,REF_DAC), NULL);
    }

    // Calculate the voltage sent by the DAC based on raw value using the DAC model

    ccsim.circuit.v_dac = ccSimDac(&ccsim.dac, raw_dac);

    // Convert the DAC voltage to reference value using the gain and offset for the power converter.
    // If the converter is off then for the reference to zero.

    cc_float ref = (ccSimIsPcOn() ? (ccsim.circuit.v_dac * regMgrParValue(&reg_mgr, VS_GAIN) + regMgrParValue(&reg_mgr, VS_OFFSET)) : 0.0);

    // If the polarity switch is present and is negative, then invert the reference

    if(polswitchIsNegative(&polswitch_mgr) == true)
    {
        ref = -ref;
    }

    // Simulate the converter and load according to the actuation mode

    if(regMgrParValue(&reg_mgr, VS_ACTUATION) == REG_VOLTAGE_REF)
    {
        ccSimVoltageSourceRT(sim_pars, sim_vars, ref, set_vfeedfwd_flag);
    }
    else
    {
        ccSimFiringSourceRT(sim_pars, sim_vars, ref);
    }

    // Use delays to estimate the measurement of the magnet's field and the circuit's current and voltage and i_capa

    regMgrSimMeasurementsRT(&reg_mgr, sim_vars);

    // For DCCTs, add the calibration current to enable DCCT calibration.
    // For all transducers except ext_ref, invert if polartiy switch is inverted and the tranducers position is CONVERTER.
    // Add noise and tone to all measured quantities except ext_ref - the tone period is associated with measurement noise (normally 50 Hz)

    ccsim.circuit.i_dcct_a = ccSimPolswitchOnTransducerRT(&sig_struct.transducer.named.dcct_a,
                                                          sim_vars->i_meas +
                                                          ccpars_sim.noise_dcct_a          * ccNoiseRT() +
                                                          ccpars_sim.tone_pp[TONE_DCCT_A]  * ccSimToneRT(&sim_tone[TONE_DCCT_A], ccpars_sim.meas_tone_period_iters) +
                                                          ccSimCalibrationCurrentRT(SIG_TRANSDUCER_DCCT_A));

    ccsim.circuit.i_dcct_b = ccSimPolswitchOnTransducerRT(&sig_struct.transducer.named.dcct_b,
                                                          sim_vars->i_meas +
                                                          ccpars_sim.noise_dcct_b          * ccNoiseRT() +
                                                          ccpars_sim.tone_pp[TONE_DCCT_B]  * ccSimToneRT(&sim_tone[TONE_DCCT_B], ccpars_sim.meas_tone_period_iters) +
                                                          ccSimCalibrationCurrentRT(SIG_TRANSDUCER_DCCT_B));

    ccsim.circuit.v_probe  = ccSimPolswitchOnTransducerRT(&sig_struct.transducer.named.v_probe,
                                                          sim_vars->v_meas +
                                                          ccpars_sim.noise_v_probe         * ccNoiseRT() +
                                                          ccpars_sim.tone_pp[TONE_V_PROBE] * ccSimToneRT(&sim_tone[TONE_V_PROBE], ccpars_sim.meas_tone_period_iters));

    ccsim.circuit.b_probe  = ccSimPolswitchOnTransducerRT(&sig_struct.transducer.named.b_probe,
                                                          sim_vars->b_meas +
                                                          ccpars_sim.noise_b_probe         * ccNoiseRT() +
                                                          ccpars_sim.tone_pp[TONE_B_PROBE] * ccSimToneRT(&sim_tone[TONE_B_PROBE], ccpars_sim.meas_tone_period_iters));

    ccsim.circuit.i_probe  = ccSimPolswitchOnTransducerRT(&sig_struct.transducer.named.i_probe,
                                                          sim_vars->i_capa_meas +
                                                          ccpars_sim.noise_i_probe         * ccNoiseRT() +
                                                          ccpars_sim.tone_pp[TONE_I_PROBE] * ccSimToneRT(&sim_tone[TONE_I_PROBE], ccpars_sim.meas_tone_period_iters));

    // Simulate the transducers - for simple probes (voltage, field and i_capa), no gain error or offset is needed.
    // For DCCTs, a full simulation is made with 2nd order temperature coefficients, head gain error, electronics
    // offset and positive/negative gain with errors.

    ccsim.circuit.v_adc[ADC_SIGNAL_I_DCCT_A ] = ccSimDcctRT(SIG_TRANSDUCER_DCCT_A, &ccsim.dcct_err[DCCT_A], ccsim.circuit.i_dcct_a, sigVarValue(&sig_struct, temp_filter, dcct_a, TEMP_FILTER_TEMP_C));
    ccsim.circuit.v_adc[ADC_SIGNAL_I_DCCT_B ] = ccSimDcctRT(SIG_TRANSDUCER_DCCT_B, &ccsim.dcct_err[DCCT_B], ccsim.circuit.i_dcct_b, sigVarValue(&sig_struct, temp_filter, dcct_b, TEMP_FILTER_TEMP_C));

    ccsim.circuit.v_adc[ADC_SIGNAL_V_MEAS]    = ccsim.circuit.v_probe / sigVarValue(&sig_struct, transducer, v_probe, TRANSDUCER_NOMINAL_GAIN);
    ccsim.circuit.v_adc[ADC_SIGNAL_B_PROBE_A] = ccsim.circuit.b_probe / sigVarValue(&sig_struct, transducer, b_probe, TRANSDUCER_NOMINAL_GAIN);
    ccsim.circuit.v_adc[ADC_SIGNAL_I_CAPA]    = ccsim.circuit.i_probe / sigVarValue(&sig_struct, transducer, i_probe, TRANSDUCER_NOMINAL_GAIN);

    // Simulate the internal calibration references with first order temperature coefficient only

    cc_float const delta_cal_ref_temp = ccpars_sim.int_cal_ref_temp - SIG_TEMP_CAL_REF_T0; // Calibration reference operating temperature relative to CAL_REF_T0

    ccsim.circuit.v_adc[ADC_SIGNAL_CAL_POSITIVE] =  SIG_CAL_V_NOMINAL * (1.0 + 1.0E-6 * (sigVarValue(&sig_struct, cal_ref, internal, CAL_REF_INT_CAL_EVENT)[SIM_POS_GAIN_ERR] +
                                                                   delta_cal_ref_temp * (sigVarValue(&sig_struct, cal_ref, internal, CAL_REF_INT_CAL_TC   )[SIM_POS_GAIN_ERR])));

    ccsim.circuit.v_adc[ADC_SIGNAL_CAL_NEGATIVE] = -SIG_CAL_V_NOMINAL * (1.0 + 1.0E-6 * (sigVarValue(&sig_struct, cal_ref, internal, CAL_REF_INT_CAL_EVENT)[SIM_NEG_GAIN_ERR] +
                                                                   delta_cal_ref_temp * (sigVarValue(&sig_struct, cal_ref, internal, CAL_REF_INT_CAL_TC   )[SIM_NEG_GAIN_ERR])));

    // Simulate v_ac - use 5Hz so that it is easy to test at 1kHz iteration rate

    ccsim.circuit.v_adc[ADC_SIGNAL_V_AC] = sin(cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(ccrun.iter_time, ccrun.start_time)) * ccpars_global.v_ac_hz * (2.0 * M_PI)) * 5.0;

    // v_adc coming from the DAC equals simply v_dac

    ccsim.circuit.v_adc[ADC_SIGNAL_DAC] = ccsim.circuit.v_dac;

    // Simulate the ADCs

    for(uint32_t adc_idx = 0; adc_idx < SIG_NUM_ADCS; adc_idx++)
    {
        // Apply the calibration coefficients to get the raw value (raw_adc) from the measured signal plus ADC noise

        ccsim.circuit.raw_adc[adc_idx] = ccSimAdc(&ccsim.adc[adc_idx],
                                                  ccsim.circuit.v_adc[ccpars_sim.adc_signal[adc_idx]] + ccNoiseRT() * ccpars_sim.noise_adcs,
                                                  sigVarValue(&sig_struct, temp_filter, internal, TEMP_FILTER_TEMP_C));

        // Create floating point version of raw_adc to allow logging

        ccsim.circuit.raw_adc_f[adc_idx] = (cc_float)ccsim.circuit.raw_adc[adc_idx];
    }


    // Allow decoupling tests by setting the v_ref on each external index with v_ref gain doubling
    //    deco.v_ref[0] = v_ref * 1
    //    deco.v_ref[1] = v_ref * 2
    //    deco.v_ref[2] = v_ref * 4
    //    deco.v_ref[3] = v_ref * 8

    if(reg_mgr.deco.phase > 0 && refMgrVarValue(&ref_mgr, REF_ITERATION_INDEX) == 0)
    {
        cc_float       v_ref  = ccsim.deco.v_ref [ccsim.deco.index];
        cc_float const i_meas = ccsim.deco.i_meas[ccsim.deco.index];

        for(uint32_t deco_idx = 0; deco_idx < REG_NUM_DECO; deco_idx++)
        {
            ccsim.deco.v_ref [deco_idx] = v_ref;
            ccsim.deco.i_meas[deco_idx] = i_meas;

            v_ref *= 2.0F;
        }
    }
}



void ccSimSuperCycle(void)
{
    // Wait for warning time for the next start function event

    if(cctimeNsCmpAbsTimesRT(ccrun.iter_time, ccsim.supercycle.warning_time) >= 0)
    {
        ccRunLogEventStart(REF_EVENT_START_FUNC);

        // Declare start function event to libref.
        // This would normally be run in a background level thread, but in ccrt it runs in the
        // real-time thread in order to produce deterministic test results. Otherwise, ccrt
        // test script logs will vary from run to run and cannot be monitored for changes.

        refEventStartFunc(&ref_mgr,
                          &ccsim.supercycle.next_event_us_time,
                           ccsim.supercycle.next.event.sub_sel,
                           ccsim.supercycle.next.event.cyc_sel,
                           ccpars_economy.once == CC_ENABLED);

        // Clear ECONOMY ONCE flag

        ccpars_economy.once = false;

        // Save pre-function sequences so they can be visible

        if(ref_mgr.event_mgr.next_start_func.func != NULL)
        {
            pre_func_ramp = ref_mgr.event_mgr.next_start_func.event->pre_func_ramp;
            pre_func_seq  = ref_mgr.event_mgr.next_start_func.event->pre_func_seq;
        }

        // Activate the next cycle

        ccsim.supercycle.active = ccsim.supercycle.next;

        // Reset cycle_running flag - this is set when time passes the event time
        // It is used for the continuous logs test.

        ccrun.cycle_running = false;

        // Prepare the next cycle and the next start function event warning time

        ccSimPrepareNextCycle();
    }

    // Wait for event time to latch active cycle as the running cycle

    if(   cctimeNsCmpAbsTimesRT(ccrun.iter_time, ccsim.supercycle.active.event_time) >= 0
       && cctimeNsCmpAbsTimesRT(ccsim.supercycle.prev_iter_time, ccsim.supercycle.active.event_time) < 0)
    {
        pthread_mutex_lock(&ccsim.supercycle.event_mutex);

        ccsim.supercycle.running = ccsim.supercycle.active;

        pthread_mutex_unlock(&ccsim.supercycle.event_mutex);

        ccsim.supercycle.prev_iter_time = ccrun.iter_time;
    }
}



void ccSimPrepareNextCycle(void)
{
    // Calculate time of the next event - note: basic period must be less than ~4s

    struct CC_ns_time const basic_period = cctimeNsAddDelayRT(cc_zero_ns, ccpars_global.basic_period_ms * 1000000);
    struct CC_ns_time const cycle_duration = cctimeNsMulRT(basic_period, ccsim.supercycle.next.basic_periods);

    ccsim.supercycle.next_cycle_time = cctimeNsAddRT(ccsim.supercycle.next_cycle_time, cycle_duration);

    char strbuf[2][22];

    ccprintf("next_cycle_time:%u.%09d  basic_period:%s  cycle_duration:%s\n"
            , ccsim.supercycle.next_cycle_time.secs.abs
            , ccsim.supercycle.next_cycle_time.ns
            , cctimeNsPrintRelTime(basic_period,strbuf[0])
            , cctimeNsPrintRelTime(cycle_duration,strbuf[1])
            );

    // Extract the next event from the supercycle array

    if(++ccsim.supercycle.next.idx >= global_pars[GLOBAL_SUPER_CYCLE].num_elements[0])
    {
        ccsim.supercycle.next.idx = 0;
    }

    uint32_t const cycle_selector = ccpars_global.super_cycle[ccsim.supercycle.next.idx];

    // Set sub-device and cycle selectors for the next event

    uint32_t const basic_periods = (cycle_selector % 100);
    uint32_t const sub_sel       = (cycle_selector / 10000);
    uint32_t const cyc_sel       = (cycle_selector / 100) % 100;

    ccsim.supercycle.next.basic_periods = (basic_periods > 0         ? basic_periods : 1);
    ccsim.supercycle.next.event.sub_sel = (sub_sel < CC_NUM_SUB_SELS ? sub_sel       : 0);
    ccsim.supercycle.next.event.cyc_sel = (cyc_sel < CC_NUM_CYC_SELS ? cyc_sel       : 0);
    ccsim.supercycle.next.event_time = ccsim.supercycle.next_cycle_time;
    ccsim.supercycle.next_event_us_time = cctimeNsToUsRT(ccsim.supercycle.next_cycle_time);

    // Set warning time for the next start function event based on the GLOBAL EVENT_ADVANCE_MS parameter

    ccsim.supercycle.warning_time = cctimeNsSubRT(ccsim.supercycle.next.event_time,
                                                  cctimeNsDoubleToRelTimeRT(1.0E-3 * (cc_double)ccpars_global.event_advance_ms));

    ccprintf("next_event_time:%u.%06d  warning_time:%u.%09d  cyc_sel:%u  sub_sel:%u  basic_periods:%u\n"
            , ccsim.supercycle.next_event_us_time.secs.abs
            , ccsim.supercycle.next_event_us_time.us
            , ccsim.supercycle.warning_time.secs.abs
            , ccsim.supercycle.warning_time.ns
            , ccsim.supercycle.next.event.cyc_sel
            , ccsim.supercycle.next.event.sub_sel
            , ccsim.supercycle.next.basic_periods
            );
}

// EOF
