//! @file  ccrt/src/ccEvtlog.c
//!
//! @brief ccrt Event Log functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <time.h>
#include <errno.h>
#include <unistd.h>

#include "ccCmds.h"
#include "ccParse.h"
#include "ccSim.h"
#include "ccRt.h"
#include "ccPars.h"
#include "ccRun.h"
#include "ccThread.h"
#include "ccEvtlog.h"

#include "evtlogStructsInit.h"

// Global event log device structure

struct EVTLOG_data evtlog_data;

// Static functions

static void ccEvtlogInitEvent(char const status, int32_t const time_offset_s)
{
    char value[EVTLOG_VALUE_LEN];

    sprintf(value,"TIME_OFFSET %d", time_offset_s);

    struct CC_us_time time_stamp = { { ccrun.iter_time.secs.abs + time_offset_s }, 0 };

    evtlogStoreRecord(&evtlog_data.device, time_stamp, "INIT_TEST", value, "ACTION", status);
}



// Global functions

void ccEvtlogInit(void)
{
    // Completely initialize the event log

    evtlogStructsInit(&evtlog_data.device, 0);

    // Store some "old" entries - only two normal and two frequent events should be retained in the log

    ccEvtlogInitEvent(EVTLOG_STATUS_NORMAL,   -3000010);       // Should be cancelled
    ccEvtlogInitEvent(EVTLOG_STATUS_NORMAL,   -2999990);
    ccEvtlogInitEvent(EVTLOG_STATUS_NORMAL,   -10);
    ccEvtlogInitEvent(EVTLOG_STATUS_NORMAL,   +10);            // Should be cancelled

    ccEvtlogInitEvent(EVTLOG_STATUS_FREQ,     -3000015);       // Should be cancelled
    ccEvtlogInitEvent(EVTLOG_STATUS_FREQ,     -2999995);
    ccEvtlogInitEvent(EVTLOG_STATUS_FREQ,     -15);
    ccEvtlogInitEvent(EVTLOG_STATUS_FREQ,     +15);            // Should be cancelled

    // Re-initialize the event log to keep the events that lie in the past 30M seconds only (~3 months)

    evtlogStructsInit(&evtlog_data.device, ccrun.iter_time.secs.abs);
}



void *ccEvtlogThread(void *args)
{
    pthread_mutex_lock(&cc_threads.event_log.condition_mutex);

    for(;;)
    {
        // Wait to be woken by the condition variable signal

        cc_threads.event_log.wake_up_flag = false;

        while(cc_threads.event_log.wake_up_flag == false)
        {
            pthread_cond_wait(&cc_threads.event_log.condition_cond, &cc_threads.event_log.condition_mutex);
        }

        // Check if reg_mode has just changed to REG_NONE

        static enum REG_mode prev_reg_mode;

        if(regMgrVarValue(&reg_mgr,REG_MODE) != prev_reg_mode)
        {
            prev_reg_mode = regMgrVarValue(&reg_mgr,REG_MODE);
        }

        // Check if any libreg parameter changes need to be processed

        regMgrParsProcess(&reg_mgr);

        // Call the background function of libsig with start_average2 set to true
        // on 1s boundaries, but only after the real-time processing has ended.
        // Reset both flags afterwards

        if(ccrun.iter_200ms_boundary)
        {
            ccrun.iter_200ms_boundary = false;
            ccrun.iter_1s_boundary    = false;

            // Update log_menus from log_mgr

            logMenuUpdate(&log_mgr, &log_menus);
        }

        // Get active event with mutex protection to ensure that logged values are consistent

        pthread_mutex_lock(&ccrun.cycle.mutex);

        ccpars_event.active_event = ccrun.cycle.active_event;

        pthread_mutex_unlock(&ccrun.cycle.mutex);

        // Scan event log properties

        evtlogStoreProperties(&evtlog_data.device, ccrun.evtlog_time);
    }

    return NULL;
}



void ccEvtlogChangedFunction(uint32_t const device_index, char const * const name, void * const user_data)
{
    struct CC_pars * const par = (struct CC_pars *)user_data;

    if(par != NULL)
    {
        ccRtPrintInVerbose("name='%s': %s %s changed",name, par->parent_cmd->name, par->name);
    }
}



void ccEvtlogLockFunction(uint32_t const device_index, void * const mutex)
{
    pthread_mutex_lock(mutex);
}



void ccEvtlogUnlockFunction(uint32_t const device_index, void * const mutex)
{
    pthread_mutex_unlock(mutex);
}



uint32_t ccEvtlogStore(char *remaining_line)
{
    char * const property = ccParseNextArg(&remaining_line);
    char * const value    = ccParseNextArg(&remaining_line);
    char * const action   = ccParseNextArg(&remaining_line);
    char * const status   = ccParseNextArg(&remaining_line);

    if(action == NULL || ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        ccRtPrintError("Invalid or missing parameters");
        return EXIT_FAILURE;
    }

    // Check that status is just one character

    char status_char = EVTLOG_STATUS_NORMAL;     // Default status is NORMAL event

    if(status != NULL)
    {
        if(status[1] != '\0')
        {
            ccRtPrintError("Invalid event log status character specifier: %s", ccParseAbbreviateArg(status));
            return EXIT_FAILURE;
        }

        status_char = status[0];
    }

    // Store a record in the Frequent or Normal event log

    evtlogStoreRecord(&evtlog_data.device, cctimeNsToUsRT(ccThreadGetIterTime()), property, value, action, status_char);

    return EXIT_SUCCESS;
}



void ccEvtlogPrintCsv(FILE * const f)
{
    uint16_t record_index = 0;
    char     read_buf[EVTLOG_READ_CSV_BUF_LEN];

    while(evtlogReadCsv(&evtlog_data.device, &record_index, read_buf))
    {
        fputs(read_buf, f);

        fputc('\n', f);
    }
}



void ccEvtlogPrintFgc(FILE * const f)
{
    uint16_t record_index = 0;
    char     read_buf[EVTLOG_READ_FGC_BUF_LEN];

    while(evtlogReadFgc(&evtlog_data.device, &record_index, read_buf))
    {
        fputs(read_buf, f);

        fputc('\n', f);
    }
}



void ccEvtlogPrintBin(FILE * const f)
{
    uint16_t record_index = 0;
    char     read_buf[EVTLOG_RECORD_LEN];

    while(evtlogReadBin(&evtlog_data.device, &record_index, read_buf, EVTLOG_NETWORK_BYTE_ORDER))
    {
        fwrite(read_buf, EVTLOG_RECORD_LEN, 1, f);
    }
}



void ccEvtlogDebug(void)
{
    fputc('\n', stderr);
    fprintf(stderr, "EVTLOG_header: property               = 0x%lX\n", (intptr_t)evtlog_def.property               );
    fprintf(stderr, "EVTLOG_header: symlist                = 0x%lX\n", (intptr_t)evtlog_def.symlist                );
    fprintf(stderr, "EVTLOG_header: symbol                 = 0x%lX\n", (intptr_t)evtlog_def.symbol                 );
    fprintf(stderr, "EVTLOG_header: changed_function       = 0x%lX\n", (intptr_t)evtlog_def.changed_function       );
    fprintf(stderr, "EVTLOG_header: lock_function          = 0x%lX\n", (intptr_t)evtlog_def.lock_function          );
    fprintf(stderr, "EVTLOG_header: unlock_function        = 0x%lX\n", (intptr_t)evtlog_def.unlock_function        );
    fprintf(stderr, "EVTLOG_header: lock_data              = 0x%lX\n", (intptr_t)evtlog_def.lock_data              );
    fprintf(stderr, "EVTLOG_header: num_properties         = %hu\n",             evtlog_def.num_properties         );
    fprintf(stderr, "EVTLOG_header: num_records            = %hu\n",             evtlog_def.num_records            );
    fprintf(stderr, "EVTLOG_header: num_freq_event_records = %hu\n",             evtlog_def.num_freq_event_records );
    fprintf(stderr, "EVTLOG_header: num_last_evt           = %hu\n",             evtlog_def.num_last_evt           );
    fprintf(stderr, "EVTLOG_header: freq_evt_iters         = %hu\n",             evtlog_def.freq_evt_iters         );
    fprintf(stderr, "EVTLOG_header: filtering_iters        = %hu\n",             evtlog_def.filtering_iters        );

    fputc('\n', stderr);
    fprintf(stderr, "EVTLOG_device: evtlog_data              = 0x%lX\n", (intptr_t)&evtlog_data                                 );
    fprintf(stderr, "EVTLOG_device: version                  = %u\n",               evtlog_data.device.version                  );
    fprintf(stderr, "EVTLOG_device: magic                    = %X\n",               evtlog_data.device.magic                    );
    fprintf(stderr, "EVTLOG_device: newest_record_index      = %hu\n",              evtlog_data.device.newest_record_index      );
    fprintf(stderr, "EVTLOG_device: newest_freq_record_index = %hu\n",              evtlog_data.device.newest_freq_record_index );
    fprintf(stderr, "EVTLOG_device: prop_data                = 0x%lX\n",  (intptr_t)evtlog_data.device.prop_data                );
    fprintf(stderr, "EVTLOG_device: last_evt                 = 0x%lX\n",  (intptr_t)evtlog_data.device.last_evt                 );
    fprintf(stderr, "EVTLOG_device: record                   = 0x%lX\n",  (intptr_t)evtlog_data.device.record                   );

    fputc('\n', stderr);

    uint32_t i;
    struct EVTLOG_record * evtlog = evtlog_data.device.record;

    for(i = 0 ; i < evtlog_def.num_records ; i++)
    {
        fprintf(stderr, "EVTLOG[%03u] 0x%lX %10u.%06u  0x%08X:0x%08X\n", i, (intptr_t)&evtlog[i],
                ntohl(evtlog[i].time_stamp.secs.abs),
                ntohl(evtlog[i].time_stamp.us),
                ntohl(evtlog[i].time_stamp.secs.abs),
                ntohl(evtlog[i].time_stamp.us));

    }
}

// EOF
