//! @file  ccrt/src/ccLog.c
//!
//! @brief ccrt logging functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "ccThread.h"
#include "ccLog.h"
#include "ccLogSpy.h"
#include "ccLogPmBuf.h"
#include "ccDebug.h"
#include "ccEvtlog.h"

// Constants

#define LOG_OUTPUT_BLK_LEN          374



void ccLogGetOutput(struct LOG_read_control * const read_control)
{
    uint32_t * blk_buf_ptr = local_log_buffer;
    uint32_t   values_returned = 1;

    while(logReadOutputFinished(read_control) == false && values_returned > 0)
    {
        values_returned = logOutput(read_control, log_read.buffers, blk_buf_ptr, LOG_OUTPUT_BLK_LEN);
        blk_buf_ptr    += values_returned;
    }

    ccLogPrintTestTap(values_returned > 0,"logOutput","number of return values","is ZERO\n");
}



void ccLogPrintTestTap(bool         const test_result,
                       char const * const test_title,
                       char const * const test_title_suffix,
                       char const * const fmt, ...)
{
    pthread_mutex_lock(&ccrun.test.mutex);

    ccTestPrintTapHeader(test_result);

    printf("%s %s ", test_title, test_title_suffix);

    if(test_result == false)
    {
        va_list args;
        va_start(args, fmt);

        vprintf(fmt, args);

        va_end(args);
    }

    putchar('\n');

    pthread_mutex_unlock(&ccrun.test.mutex);
}



void ccLogTestReadControl(struct LOG_log const * const log, struct LOG_read_control * const read_control)
{
    uint32_t const header_size     = read_control->header_size;
    uint32_t const data_size       = read_control->data_size;
    uint32_t const max_buffer_size = log_menus.max_buffer_size;

    ccLogPrintTestTap(data_size % LOG_VALUE_SIZE == 0,
                      "Log Read Control -",
                      "data size",
                      "Data size is not a multiple of sample size");

    ccLogPrintTestTap(data_size + header_size == read_control->total_size,
                      "Log Read Control -",
                      "total size",
                      "Data_size (%d) + header_size (%d) != total_size (%d)\n", data_size, header_size, read_control->total_size);

    ccLogPrintTestTap(!(max_buffer_size > 0 && read_control->total_size > max_buffer_size),
                      "Log Read Control -",
                      "max buffer size",
                      "Total size (%d) exceeds max buffer size (%d)", read_control->total_size, max_buffer_size);

    ccLogPrintTestTap(read_control->buf_offset == log->priv->sig_bufs_offset,
                      "Log Read Control -",
                      "log buf offset",
                      "log buf offset (%d) != read control buf offset (%d)", log->priv->sig_bufs_offset, read_control->buf_offset);

    ccLogPrintTestTap(read_control->buf_len == log->priv->sig_bufs_len,
                      "Log Read Control -",
                      "buf length",
                      "Log buf len (%d) != read control buf len (%d)", log->priv->sig_bufs_len, read_control->buf_len);

    ccLogPrintTestTap((read_control->meta & LOG_META_LITTLE_ENDIAN_BIT_MASK) != 0,
                      "Log Read Control -",
                      "meta data - ENDIAN flag",
                      "incorrect");

    ccLogPrintTestTap(!(   (logIsAnalog(&log_mgr, log->index) == false && (read_control->meta & LOG_META_ANALOG_BIT_MASK) != 0)
                        || (logIsAnalog(&log_mgr, log->index) == true  && (read_control->meta & LOG_META_ANALOG_BIT_MASK) == 0)),
                      "Log Read Control -",
                      "meta data - ANALOG flag",
                      "incorrect");

    ccLogPrintTestTap(!(   (logIsContinuous(&log_mgr, log->index) == false && (read_control->meta & LOG_META_CONTINUOUS_BIT_MASK) != 0)
                        || (logIsContinuous(&log_mgr, log->index) == true  && (read_control->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0)),
                      "Log Read Control -",
                      "meta data - CONTINUOUS flag",
                      "incorrect");

    uint32_t const unknown_flags = ~(uint32_t)(LOG_META_LITTLE_ENDIAN_BIT_MASK     |
                                               LOG_META_CYCLIC_BIT_MASK            |
                                               LOG_META_ANALOG_BIT_MASK            |
                                               LOG_META_POSTMORTEM_BIT_MASK        |
                                               LOG_META_CONTINUOUS_BIT_MASK        |
                                               LOG_META_FREEZABLE_BIT_MASK         |
                                               LOG_META_DISABLED_BIT_MASK);

    ccLogPrintTestTap((read_control->meta & unknown_flags) == 0,
                      "Log Read Control -",
                      "log meta",
                      "Unknown flags are present");
}



void ccLogStoreSigNamesAndUnits(uint32_t const log_index,
                                uint32_t const sig_index,
                                char   * const sig_name,
                                char   * const units,
                                bool     const direct_store)
{
    if(direct_store == true)
    {
        // Store name and/or units directly

        logStoreSigNameAndUnits(&log_structs.log[log_index], sig_index, sig_name, units);
    }
    else
    {
        // Store name and/or units via logReadRequest

        logPrepareStoreSigNameAndUnitsRequest(&log_menus, log_index, sig_index, sig_name, units, &log_read.request);

        logReadRequest(&log_read, NULL);
    }
}



static char * ccLogSigNames(FILE * const f, uint32_t const menu_index, enum LOG_read_action action)
{
    struct LOG_read_request * const read_request = &log_read_request[menu_index];

    if(logPrepareReadMenuSigNamesRequest(&log_menus, menu_index, action, read_request))
    {
        // Copy read_request into log_structs. This simulates the transfer of the structure between CPUs on a multi-CPU platform.

        memcpy(&log_read.request, read_request, sizeof(struct LOG_read_request));

        // Process the read request to generate the read control and header

        logReadRequest(&log_read, &log_read_control[menu_index]);

        if(log_read_control[menu_index].status == LOG_READ_CONTROL_SUCCESS)
        {
            return log_read.header.sig_names;
        }
        else
        {
            return "READ_REQUEST failed";
        }
    }

    return "";
}



void ccLogWriteSigNames(FILE * const f)
{
    fputs("SIG_NAMES:\n",f);

    for(enum LOG_menu_index menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {
        fprintf(f,"  %2d.%-12s: %s.\n",
                menu_index,
                log_menus.names[menu_index],
                ccLogSigNames(f, menu_index, LOG_READ_GET_SIG_NAMES));
    }

    fputs("ALL_SIG_NAMES:\n",f);

    for(enum LOG_menu_index menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {
        fprintf(f,"  %2d.%-12s: %s.\n",
                menu_index,
                log_menus.names[menu_index],
                ccLogSigNames(f, menu_index, LOG_READ_GET_ALL_SIG_NAMES));
    }
}



//! Set log directory name and create the directory if it doesn't exist
//!
//! @param[out]  dir_name   Array to be filled with log directory name

void ccLogSetDirName(char dir_name[CC_PATH_LEN])
{
    if(ccrun.log.dir_name != NULL)
    {
        // Copy user supplied directory name and safely null terminate

        strncpy(dir_name, ccrun.log.dir_name, CC_PATH_LEN);

        dir_name[CC_PATH_LEN - 1] = '\0';
    }
    else
    {
        // Otherwise set default directory name

        strcpy(dir_name, "log");
    }

    // Clip directory name len to keep space for cycle selector and log index

    size_t len = strlen(dir_name);

    if(len > (CC_PATH_LEN-10))
    {
        len = (CC_PATH_LEN-10);
    }

    // Add (cycle_selector) as a suffix, if it is in the valid range

    if(ccrun.log.cyc_sel > 0 && ccrun.log.cyc_sel < CC_NUM_CYC_SELS)
    {
        len += sprintf(&dir_name[len],"(%02u)", ccrun.log.cyc_sel);
    }

    // If LOG OVERWRITE is DISABLED then add _log_idx suffix

    if(ccpars_log.overwrite == CC_DISABLED)
    {
        len += sprintf(&dir_name[len],"_%02u", ccfile.log_dir_idx);
    }

    // Create the log directory if it doesn't already exist

    char path[CC_PATH_LEN];

    ccFilePrintPath(path, LOG_PATH,
                    ccfile.converter,
                    ccfile.test_file.dirname,
                    ccfile.test_file.basename_no_ext,
                    dir_name);

    ccFileMakeDir(path);
}



static void ccLogWriteEventLog(void)
{
    if(ccpars_logmenu[LOG_MENU_EVENTS] == CC_ENABLED && ccrun.log.cyc_sel == 0)
    {
        FILE * const f = ccFileOpen(CC_TMP_LOG_FILE, "w");

        // Write PowerSpy Table Header and timestamp

        char dir_name[CC_PATH_LEN];

        ccLogSetDirName(dir_name);

        fprintf(f,"type:table class:eventlog source:ccrt device:%s name:EVENTS\n", dir_name);

        // Write event log to file using comma delimiters

        ccEvtlogPrintCsv(f);

        // Close and rename temporary filename to final filename

        char filename[CC_PATH_LEN];

        ccFilePrintPath(filename, LOG_PATH "/_EVENTS.csv",
                        ccfile.converter,
                        ccfile.test_file.dirname,
                        ccfile.test_file.basename_no_ext,
                        dir_name);

        ccFileCloseAndReplace(f, CC_TMP_LOG_FILE, filename);
    }
}



static void ccLogWriteConfigPars(void)
{
    if(ccpars_logmenu[LOG_MENU_CONFIGURATION] == CC_ENABLED && ccrun.log.cyc_sel == 0)
    {
        FILE * const f = ccFileOpen(CC_TMP_LOG_FILE, "w");

        // Write PowerSpy Table Header and timestamp

        char dir_name[CC_PATH_LEN];

        ccLogSetDirName(dir_name);

        fprintf(f,"type:table source:ccrt device:%s name:CONFIGURATION,,,\n", dir_name);

        // Write debug data with comma delimiters

        uint32_t num_lines = 1;

        for(struct CC_cmds * cmd = cmds ; cmd->name != NULL ; cmd++)
        {
            if(cmd->pars != NULL)
            {
                // Get all parameters that have the CFG flag, but NOT the SUB_SEL flags set
                // Setting cmd_name_width triggers commas as delimiters

                if(num_lines > 0)
                {
                    fputc('\n', f);
                }

                num_lines = ccParsGetAllMatchingFlags(f, cmd, 0, CC_NO_INDEX, PARS_CFG|PARS_SUB_SEL, PARS_CFG, 0, 0);
            }
        }

        // Close and rename temporary filename to final filename

        char filename[CC_PATH_LEN];

        ccFilePrintPath(filename, LOG_PATH "/_CONFIGURATION.csv",
                        ccfile.converter,
                        ccfile.test_file.dirname,
                        ccfile.test_file.basename_no_ext,
                        dir_name);

        ccFileCloseAndReplace(f, CC_TMP_LOG_FILE, filename);
    }
}



static void ccLogWriteDebugData(void)
{
    if(ccpars_logmenu[LOG_MENU_DEBUG_DATA] == CC_ENABLED && ccrun.log.cyc_sel == 0)
    {
        FILE * const f = ccFileOpen(CC_TMP_LOG_FILE, "w");

        // Write PowerSpy Table Header and timestamp

        char dir_name[CC_PATH_LEN];

        ccLogSetDirName(dir_name);

        fprintf(f,"type:table source:ccrt device:%s name:DEBUG_DATA,,,\n", dir_name);

        // Write debug data with comma delimiters

        ccDebug(f, ',');

        // Close and rename temporary filename to final filename

        char filename[CC_PATH_LEN];

        ccFilePrintPath(filename, LOG_PATH "/_DEBUG_DATA.csv",
                        ccfile.converter,
                        ccfile.test_file.dirname,
                        ccfile.test_file.basename_no_ext,
                        dir_name);

        ccFileCloseAndReplace(f, CC_TMP_LOG_FILE, filename);
    }
}



void ccLogWakeThread(enum LOG_read_action const action, char * const log_dir_name)
{
    // Wake up ccLogSigsWriteThread()

    if(pthread_mutex_trylock(&cc_threads.log_writing.condition_mutex) == 0)
    {
        cc_threads.log_writing.wake_up_flag = true;

        ccrun.log.action = action;
        ccrun.log.dir_name    = log_dir_name;
        ccrun.log.cyc_sel     = ccpars_log.prev_cycle == CC_ENABLED ? logPrevCycSel(&log_mgr) : ccpars_log.cyc_sel;

        pthread_cond_signal(&cc_threads.log_writing.condition_cond);

        pthread_mutex_unlock(&cc_threads.log_writing.condition_mutex);

        ccrun.log.in_progress = true;
    }
    else
    {
        cc_threads.log_writing.missed_counter++;
    }
}



uint32_t ccLogEnableDisable(char * const arg, bool const enable)
{
    for(uint32_t log_index = 0 ; log_index < LOG_NUM_LOGS ; log_index++)
    {
        if(strcasecmp(arg, log_buffers.priv[log_index].name) == 0)
        {
            if(enable)
            {
                logEnable(&log_mgr, log_index);
            }
            else
            {
                logDisable(&log_mgr, log_index);
            }

            return EXIT_SUCCESS;
        }
    }

    // Unknown

    ccRtPrintError("Unknown log name: %s", ccParseAbbreviateArg(arg));

    return EXIT_FAILURE;
}



void *ccLogSigsWriteThread(void *args)
{
    int  oldcancelstate;

    pthread_mutex_lock(&cc_threads.log_writing.condition_mutex);

    for(;;)
    {
        ccrun.log.in_progress = false;

        // Wait to be woken by the condition variable signal

        cc_threads.log_writing.wake_up_flag = false;

        while(cc_threads.log_writing.wake_up_flag == false)
        {
            pthread_cond_wait(&cc_threads.log_writing.condition_cond, &cc_threads.log_writing.condition_mutex);
        }

        // Disable thread cancel state to ensure that log writing will finish before

        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldcancelstate);

        // Set time origin

        ccpars_log.read_timing.time_origin = ccpars_log.time_origin;

        // Generate log according to the type (Postmortem or Spy)

        if(ccrun.log.action == LOG_READ_GET_PM_BUF)
        {
            ccLogWritePmBuf();
        }
        else
        {
            // If LOG OVERWRITE is DISABLED then increment log directory index

            if(ccpars_log.overwrite == CC_DISABLED)
            {
                ccfile.log_dir_idx++;
            }

            // Log type is not POSTMORTEM - write logs to Spy files and create config pars and debug data logs if required

            ccLogWriteSpy       ();
            ccLogWriteConfigPars();
            ccLogWriteDebugData ();
            ccLogWriteEventLog  ();
        }

        // Enable thread cancel state and unlock mutex

        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldcancelstate);
    }

    return NULL;
}

// EOF
