//! @file  ccrt/src/ccNoise.c
//!
//! @brief ccrt real-time Gaussian noise generation functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "cclibs.h"
#include "ccNoise.h"

// Constants

#define CC_NOISE_LEN    100003   // Should be a prime number for best results (~100K samples for 10s of noise at 10ksps)

// Static normalized noise buffer - noise is generated in pairs so one extra element is needed

static cc_float normalized_noise_buf[CC_NOISE_LEN + 1];

// Creating Gaussian noise using the polar form of the Box-Muller method is not a deterministic process.
// For this reason, a buffer of normalized noise (standard deviation = 1) is created in advance,
// and then a real-time function can simply return one sample of normalized noise at a time.
// This can be scaled by the calling application to the required level by multiplying by the
// desired standard deviation.
//
// By making the buffer length a prime number, if multiple signals require noise at the same time,
// then each will eventually get every sample from the normalized noise buffer before looping back to the start.


void ccNoiseInit(void)
{
    uint32_t idx = 0;

    // Fill noise buffer with normalized Gaussian noise

    while(idx < CC_NOISE_LEN)
    {
        cc_float v1;
        cc_float v2;
        cc_float r;

        // Try generating random vectors until the amplitude lies inside the unit circle

        do
        {
            v1 = 2.0 / (cc_float)RAND_MAX * (cc_float)rand() - 1.0;
            v2 = 2.0 / (cc_float)RAND_MAX * (cc_float)rand() - 1.0;
            r  = v1 * v1 + v2 * v2;
         }
         while(r >= 1.0);

         // Generate two Gaussian noise values from the vector

        cc_float f = sqrtf(-2.0 * log(r) / r);

        normalized_noise_buf[idx++] = v1 * f;
        normalized_noise_buf[idx++] = v2 * f;
    }
}



cc_float ccNoiseRT(void)
{
    static uint32_t  noise_buf_idx;     // Static buffer index is shared for all noise requests

    // This is not strictly thread safe, but if threads collide, nothing bad will happen

    noise_buf_idx = (noise_buf_idx + 1) % CC_NOISE_LEN;

    return normalized_noise_buf[noise_buf_idx];
}

// EOF
