//! @file  ccrt/src/ccStatus.c
//!
//! @brief ccrt status reporting functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>
#include <float.h>
#include <time.h>
#include <errno.h>

// Include libterm header file to get terminal control sequences

#include "libterm.h"

// Include ccrt program header files

#include "ccCmds.h"
#include "ccRt.h"
#include "ccParse.h"
#include "ccFile.h"
#include "ccInput.h"
#include "ccRun.h"
#include "ccSim.h"

// Constants for show_mask

#define CC_POLSWITCH_STATUS     0x01
#define CC_DIRECT_STATUS        0x02

// Static variables

static uint32_t show_mask;



static inline void ccStatusPrintRowLabel(char const * const label)
{
    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "%-10s" TERM_NORMAL ": ", label);
}



static void ccStatusEnumPointer(struct CC_pars_enum const * const enum_p,
                                char                const * const fmt,
                                char                const * const format)
{
    if(enum_p->flags & CC_ENUM_RED)
    {
        fprintf(stderr,TERM_CSI TERM_FG_RED "%s" TERM_SGR, format);
    }
    else if(enum_p->flags & CC_ENUM_YELLOW)
    {
        fprintf(stderr,TERM_CSI TERM_FG_YELLOW "%s" TERM_SGR, format);
    }
    else if(enum_p->flags & CC_ENUM_GREEN)
    {
        fprintf(stderr,TERM_CSI TERM_FG_GREEN "%s" TERM_SGR, format);
    }
    else if(enum_p->flags & CC_ENUM_CYAN)
    {
        fprintf(stderr,TERM_CSI TERM_FG_CYAN "%s" TERM_SGR, format);
    }
    else if(format[0] != '\0')
    {
        fprintf(stderr,TERM_CSI "%s" TERM_SGR, format);
    }

    fprintf(stderr,fmt, enum_p->string);

    fprintf(stderr,TERM_NORMAL);
}



static void ccStatusEnumValue(struct CC_pars const * const par, char const * const fmt)
{
    int32_t                     const enum_idx = ccParsEnumValueToIndex(par->ccpars_enum, *par->value_p.u);
    struct CC_pars_enum const * const enum_p   = par->ccpars_enum + enum_idx;

    if(enum_idx < 0)
    {
        fprintf(stderr,fmt, "?");
    }
    else
    {
        ccStatusEnumPointer(enum_p, fmt, "");
    }
}



static void ccStatusBoolValue(bool const value)
{
    if(value == false)
    {
        fputs(TERM_CSI TERM_FG_YELLOW TERM_SGR "FALSE" TERM_NORMAL, stderr);
    }
    else
    {
        fputs(TERM_CSI TERM_FG_CYAN TERM_SGR "TRUE " TERM_NORMAL, stderr);
    }
}



// TODO Find better name for this

static void ccStatusMeasOrRef(char const * const label, enum REG_mode const reg_mode, cc_float const meas)
{
    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "%s" TERM_NORMAL ":", label);

    if(reg_mgr.reg_mode == reg_mode)
    {
        if(reg_mgr.openloop)
        {
            fprintf(stderr,TERM_CSI TERM_FG_CYAN TERM_SGR);
        }
        else
        {
            fprintf(stderr,TERM_CSI TERM_FG_GREEN TERM_SGR);
        }
    }

    fprintf(stderr,"%10.3f  " TERM_NORMAL, meas);
}



static void ccStatusFlags(char           const * const label,
                          struct CC_cmds const * const cmd,
                          char           const * const active_fg,
                          char           const * const active_bg)
{
    struct CC_pars const * par = cmd->pars;

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "%s" TERM_NORMAL ":", label);

    while(par->name != NULL)
    {
        if(ccParsIsStat(par))
        {
            if((ccParsIsType(par, PAR_BOOL)    && *par->value_p.b == true) ||
               (ccParsIsType(par, PAR_BITMASK) && *par->value_p.u != 0))
            {
                fprintf(stderr,TERM_CSI "%s%s" TERM_SGR, active_fg, active_bg);
            }

            // Print first letter of the parameter name and cancel formatting

            fprintf(stderr,"%c%s", par->name[0], TERM_NORMAL);
        }

        par++;
    }

    fputs("  ",stderr);
}



static void ccStatusBitMask(char           const * const label,
                            struct CC_pars const *       par,
                            char           const * const active_fg,
                            char           const * const active_bg)
{
    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "%s" TERM_NORMAL ":", label);

    struct CC_pars_enum const * pars_enum = par->ccpars_enum;
    uint32_t const value = *par->value_p.u;

    while(pars_enum->string != NULL)
    {
        if((value & (1 << pars_enum->value)) != 0)
        {
            fprintf(stderr,TERM_CSI "%s%s" TERM_SGR, active_fg, active_bg);
        }

        // Print first letter of the enum string and cancel formatting

        fprintf(stderr,"%c%s", pars_enum->string[0], TERM_NORMAL);

        pars_enum++;
    }
}



static uint32_t ccStatusTiming(void)
{
    ccStatusPrintRowLabel("TIMING");

    // Print current iteration time with millisecond resolution (3)

    ccParsPrintAbsTime(stderr, ccThreadGetIterTime(), 3);

    // Supercycle selectors

    struct CC_sim_sc_event running;

    ccSimGetRunningCycle(&running);

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "  SUPERCYCLE" TERM_NORMAL ":%02u-%u/%02u-%02u",
            running.idx,
            running.event.sub_sel,
            running.event.cyc_sel,
            running.basic_periods);

    fputc('\n',stderr);

    return 1;
}



static uint32_t ccStatusEvent(void)
{
    ccStatusPrintRowLabel("EVENT");

    // Event date and time

    pthread_mutex_lock(&ccrun.cycle.mutex);

    struct REF_event const active_event = ccrun.cycle.active_event;

    pthread_mutex_unlock(&ccrun.cycle.mutex);

    // Print evebnt time with millisecond resolution (3)

    ccParsPrintAbsTime(stderr, active_event.abs_time, 3);

    // Event cycle selectors

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "  CYCLE" TERM_NORMAL ":");

    if(active_event.abs_time.secs.abs == 0)
    {
        fputs("-/--/--",stderr);
    }
    else
    {
        // SUB_SEL/CYC_SEL/REF_CYC_SEL

        fprintf(stderr,"%u/", active_event.sub_sel);

        // Use colour to highlight CYC_SEL and REF_CYC_SEL when test_cyc_sel is being used

        if(active_event.cyc_sel > 0 && active_event.cyc_sel == refMgrParValue(&ref_mgr,MODE_TEST_CYC_SEL))
        {
            fprintf(stderr,TERM_CSI TERM_FG_YELLOW TERM_SGR "%02u/", active_event.cyc_sel);

            if(active_event.cyc_sel != active_event.ref_cyc_sel)
            {
                fprintf(stderr,TERM_CSI TERM_FG_GREEN TERM_SGR);
            }

            fprintf(stderr,"%02u" TERM_NORMAL, active_event.ref_cyc_sel);
        }
        else
        {
            fprintf(stderr,"%02u/%02u",active_event.cyc_sel, active_event.ref_cyc_sel);
        }
    }

    fputc('\n',stderr);

    return 1;
}



static uint32_t ccStatusState(void)
{
    ccStatusPrintRowLabel("STATE");

    // REF mode and state

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "REF" TERM_NORMAL ":");
    ccStatusEnumValue(&mode_pars[MODE_REF], "%10s");
    fputc('/',stderr);
    ccStatusEnumValue(&state_pars[STATE_REF], "%-15s");

    // PC state

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "PC" TERM_NORMAL ":");
    ccStatusEnumValue(&mode_pars[MODE_PC], "%3s");
    fputc('/',stderr);
    ccStatusEnumValue(&state_pars[STATE_PC], "%-5s");

    // Actuation

    fprintf(stderr," " TERM_CSI TERM_BOLD TERM_SGR "ACT" TERM_NORMAL ":");
    ccStatusEnumValue(&vs_pars[VS_ACTUATION], "%7s");

    // Global sub-device selector (clip to valid range)

    uint32_t const sub_sel = (ccpars_global.sub_sel < CC_NUM_SUB_SELS ? ccpars_global.sub_sel : 0);

    fprintf(stderr, "  " TERM_CSI TERM_BOLD TERM_SGR "SUB" TERM_NORMAL ":%u  ", sub_sel);

    fputc('\n',stderr);

    return 1;      // Number of lines produced
}



static uint32_t ccStatusMeas(void)
{
    ccStatusPrintRowLabel("MEAS");

    // Field measurement

    ccStatusMeasOrRef("B", REG_FIELD,   regMgrVarValue(&reg_mgr,MEAS_B_EXTRAPOLATED));
    ccStatusMeasOrRef("I", REG_CURRENT, regMgrVarValue(&reg_mgr,MEAS_I_EXTRAPOLATED));
    ccStatusMeasOrRef("V", REG_VOLTAGE, regMgrVarValue(&reg_mgr,MEAS_V_UNFILTERED));

    // B and I LOW and ZERO flags

    ccStatusFlags("LOW_ZERO", &cmds[CMD_MEAS], TERM_FG_WHITE, TERM_BG_BLUE);

    fputc('\n',stderr);

    return 1;      // Number of lines produced
}



static uint32_t ccStatusFunc(void)
{
    ccStatusPrintRowLabel("FUNC");

    fprintf(stderr, TERM_CSI TERM_BOLD TERM_SGR "TYPE" TERM_NORMAL ":");
    ccStatusEnumPointer(&enum_fg_type[refMgrVarValue(&ref_mgr, REF_FG_TYPE)], "%-6s", "");

    fprintf(stderr, "  " TERM_CSI TERM_BOLD TERM_SGR "STATUS" TERM_NORMAL ":");
    ccStatusEnumPointer(&enum_fg_status[refMgrVarValue(&ref_mgr, REF_FG_STATUS)], "%-11s", "");

    fprintf(stderr, "  " TERM_CSI TERM_BOLD TERM_SGR "TIME" TERM_NORMAL ":%9.3f", refMgrVarValue(&ref_mgr, REF_FG_TIME));

    fprintf(stderr, "  " TERM_CSI TERM_BOLD TERM_SGR "REF_FG" TERM_NORMAL ":%10.3f", refMgrVarValue(&ref_mgr, REF_FG));

    fputc('\n', stderr);

    return 1;
}



static uint32_t ccStatusDirect(void)
{
    // Return immediately if polswitch is not active

    if((show_mask & CC_DIRECT_STATUS) == 0)
    {
        return 0;
    }

    ccStatusPrintRowLabel("DIRECT");

    // Line 1

    ccStatusMeasOrRef("B", REG_FIELD,   refMgrParValue(&ref_mgr, DIRECT_B_REF));
    ccStatusMeasOrRef("I", REG_CURRENT, refMgrParValue(&ref_mgr, DIRECT_I_REF));
    ccStatusMeasOrRef("V", REG_VOLTAGE, refMgrParValue(&ref_mgr, DIRECT_V_REF));

    fputc('\n', stderr);

    return 1;      // Number of lines produced
}


static uint32_t ccStatusPolSwitch(void)
{
    // Return immediately if polswitch is not active

    if((show_mask & CC_POLSWITCH_STATUS) == 0)
    {
        return 0;
    }

    // Line 1

    ccStatusPrintRowLabel("POLSWITCH");

    fprintf(stderr, TERM_CSI TERM_BOLD TERM_SGR "REQ_STATE" TERM_NORMAL ":");
    ccStatusEnumValue(&polswitch_pars[POLSWITCH_REQ_STATE], "%-8s");

    fprintf(stderr,"  " TERM_CSI TERM_BOLD TERM_SGR "EXP_STATE" TERM_NORMAL ":");
    ccStatusEnumValue(&polswitch_pars[POLSWITCH_EXP_STATE], "%-8s");

    fprintf(stderr,"  " TERM_CSI TERM_BOLD TERM_SGR "STATE" TERM_NORMAL ":");
    ccStatusEnumValue(&polswitch_pars[POLSWITCH_STATE], "%-8s  ");

    ccStatusBitMask("FAULTS", &polswitch_pars[POLSWITCH_FAULTS], TERM_FG_WHITE, TERM_BG_RED);

    fputc('\n',stderr);

    // Line 2

    ccStatusPrintRowLabel("POLSWITCH");

    fprintf(stderr, TERM_CSI TERM_BOLD TERM_SGR "MODE_USER" TERM_NORMAL ":");
    ccStatusEnumValue(&polswitch_pars[POLSWITCH_MODE_USER], "%-8s");

    fprintf(stderr," " TERM_CSI TERM_BOLD TERM_SGR " MODE_AUTO" TERM_NORMAL ":");
    ccStatusEnumValue(&polswitch_pars[POLSWITCH_MODE_AUTO], "%-8s");

    fprintf(stderr,"  " TERM_CSI TERM_BOLD TERM_SGR "STARTING" TERM_NORMAL ":");
    ccStatusBoolValue(polswitchIsStarting(&polswitch_mgr));

    fputc('\n',stderr);

    // Line 3

    ccStatusPrintRowLabel("POLSWITCH");

    fprintf(stderr,     TERM_CSI TERM_BOLD TERM_SGR "SIMULATED" TERM_NORMAL ":");
    ccStatusBoolValue(polswitchIsSimulated(&polswitch_mgr));

    fprintf(stderr,"     " TERM_CSI TERM_BOLD TERM_SGR "AUTOMATIC" TERM_NORMAL ":");
    ccStatusBoolValue(polswitchIsAutomatic(&polswitch_mgr));

    fprintf(stderr,"     " TERM_CSI TERM_BOLD TERM_SGR "LOCKED" TERM_NORMAL ":");
    ccStatusBoolValue(polswitchIsLocked(&polswitch_mgr));

    fputc('\n',stderr);

    return 3;      // Number of lines produced
}



static uint32_t ccStatusStatus(void)
{
    ccStatusPrintRowLabel("STATUS");

    ccStatusFlags("FAULTS",   &cmds[CMD_FAULTS],   TERM_FG_WHITE, TERM_BG_RED);
    ccStatusFlags("WARNINGS", &cmds[CMD_WARNINGS], TERM_FG_BLACK, TERM_BG_YELLOW);

    fprintf(stderr,TERM_CSI TERM_BOLD TERM_SGR "RUNNING_LOGS_MASK" TERM_NORMAL ":0x%08X", log_mgr.running_logs_mask);

    fputc('\n',stderr);

    return 1;      // Number of lines produced
}



void ccStatus(bool const overwrite)
{
    uint32_t        new_show_mask = 0;
    static uint32_t num_lines;

    // Only wait for the state machine if the period of execution is < 11ms, otherwise the
    // display of status data gets messed up

    if(reg_mgr.reg_period < 0.011)
    {
        // Regulation period is less than 11ms so wait for state machine to run (at the regulation rate)

        ccRtWaitForRefStateMachine();
    }

    // Prepare flags in show mask for conditional status lines

    if(polswitchVarValue(&polswitch_mgr,TIMEOUT) != 0)
    {
        new_show_mask |= CC_POLSWITCH_STATUS;
    }

    if(ref_mgr.ref_state == REF_DIRECT)
    {
        new_show_mask |= CC_DIRECT_STATUS;
    }

    fputs(TERM_HIDE_CURSOR,stderr);

    // If overwrite is true then move up to overwrite previous status
    // The number of lines to move up depends on whether the polarity switch status line was included

    if(overwrite && new_show_mask == show_mask)
    {
        fprintf(stderr,TERM_CSI "%u" TERM_UP "\r", num_lines + 1);
    }
    else
    {
        fputc('\n',stderr);
    }

    show_mask = new_show_mask;

    // Generate and count the number of status lines displayed

    num_lines = 0;

    num_lines += ccStatusTiming();
    num_lines += ccStatusEvent();
    num_lines += ccStatusState();
    num_lines += ccStatusStatus();
    num_lines += ccStatusFunc();
    num_lines += ccStatusMeas();
    num_lines += ccStatusDirect();
    num_lines += ccStatusPolSwitch();

    // If interactive then scroll the window to anticipate the newline when user presses enter and write the prompt

    if(ccInputIsStdin())
    {
        fputs("\n" TERM_CSI "2" TERM_UP TERM_SHOW_CURSOR "\n\r" CC_PROMPT, stderr);
    }
}

// EOF
