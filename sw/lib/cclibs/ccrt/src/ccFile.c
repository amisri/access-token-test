//! @file  ccrt/src/ccFile.c
//!
//! @brief ccrt file system related functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>

// Include ccrt program header files

#include "ccCmds.h"
#include "ccFile.h"
#include "ccParse.h"
#include "ccLog.h"
#include "ccRun.h"
#include "ccStatus.h"
#include "ccInput.h"


static DIR * ccFileOpenDir(char const * const path)
{
    DIR * const dir = opendir(path);

    if(dir == NULL)
    {
        ccRtPrintErrorAndExit("Failed to open directory %s : %s (%d)", path, strerror(errno), errno);
    }

    return dir;
}



static void ccFileCloseDir(DIR * const dir, char const * const path)
{
    if(closedir(dir) != 0)
    {
        ccRtPrintErrorAndExit("Closedir on '%s' failed : %s (%d)", path, strerror(errno), errno);
    }
}



void ccFileClock(void)
{
    // Initialize the timer parameters

    regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) = CC_ITER_PERIOD_NS;
    timer.event_log_period_iters = CC_EVENT_LOG_PERIOD_ITERS;

    // Try to open the CLOCK file

    FILE *f = fopen("CLOCK", "r");

    if(f != NULL)
    {
        uint32_t iter_period_ns;
        uint32_t event_log_period_iters;
        char     unexpected_char;
        if(fscanf(f, "ITER_PERIOD_NS %u EVENT_LOG_PERIOD_ITERS %u %c", &iter_period_ns, &event_log_period_iters, &unexpected_char) == 2 &&
           iter_period_ns >= 5000 && iter_period_ns <= 100000000 &&
           event_log_period_iters >= 1 && event_log_period_iters <= 1000)
        {
            regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) = iter_period_ns;
            timer.event_log_period_iters = event_log_period_iters;
        }

        ccFileClose(f);
    }

    // Try to rewrite the CLOCK file

    f = ccFileOpenTextFile("CLOCK");

    fprintf(f, "ITER_PERIOD_NS %u\nEVENT_LOG_PERIOD_ITERS %u\n",
            regMgrParAppValue(&reg_pars, ITER_PERIOD_NS),
            timer.event_log_period_iters);

    ccFileClose(f);
}



void ccFileMakeDir(char const * const path)
{
    struct stat     path_stat;
    char            path_tokens [CC_PATH_LEN];
    char            current_path[CC_PATH_LEN] = {'\0'};  // Zero it for strcat
    char           *token;

    // Make a copy of the path because strtok would destroy it

    strncpy(path_tokens, path, CC_PATH_LEN);
    path_tokens[CC_PATH_LEN - 1] = '\0';

    // Get the first sub-dir from the path

    token = strtok(path_tokens, "/");

    // For each sub-directory in the path

    while(token != NULL)
    {
        // Build full path in top-down manner

        strcat(current_path, token);
        strcat(current_path, "/");

        // Get status of path
        // Using stat before calling mkdir is a TOCTOU problem, but it should not be visible in case of ccrt

        if(stat(current_path, &path_stat) == -1)
        {
            // Path doesn't exist so try to create it

            ccRtPrintInVerbose("Creating '%s'", current_path);

            if(mkdir(current_path, S_IXUSR|S_IRUSR|S_IXGRP|S_IWUSR|S_IRGRP|S_IXOTH|S_IROTH))
            {
                ccRtPrintErrorAndExit("Failed to create new directory '%s': %s (%d)", current_path, strerror(errno), errno);
            }
        }
        else
        {
            // Report error if the path is not a directory

            if(!S_ISDIR(path_stat.st_mode))
            {
                ccRtPrintErrorAndExit("Path '%s' is not a valid directory", current_path);
            }
        }

        // Get the next sub-dir from the path

        token = strtok(NULL, "/");
    }
}



void ccFileCreateDirs(void)
{
    // Create top-level directories

    ccFileMakeDir("config");
    ccFileMakeDir("scripts");
    ccFileMakeDir("tests");

    // Create a sub-directory in ref for each sub_sel index

    for(uint32_t sub_sel = 0; sub_sel < CC_NUM_SUB_SELS; sub_sel++)
    {
        char path[CC_PATH_LEN];

        ccFilePrintPath(path, "ref/%u", sub_sel);
        ccFileMakeDir(path);
    }
}



void ccFilePrintPath(char * const path, char const * fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    if(vsnprintf(path, CC_PATH_LEN, fmt, args) >= CC_PATH_LEN)
    {
        ccRtPrintErrorAndExit("Path too long: %s", path);
    }

    va_end(args);
}



void ccFileGetCwd(char * const cwd, size_t const size)
{
    if(getcwd(cwd, size) == NULL)
    {
        ccRtPrintErrorAndExit("Getcwd failed: %s (%d)", strerror(errno), errno);
    }
}



void ccFileClose(FILE * const file)
{
    if(fclose(file) != 0)
    {
        ccRtPrintErrorAndExit("Failed to close file: %s (%d)", strerror(errno), errno);
    }
}



FILE * ccFileOpen(char const * const filename, char const * const mode)
{
    // Open temporary file for writing

    FILE *f = fopen(filename, mode);

    if(f == NULL)
    {
         ccRtPrintErrorAndExit("Opening file '%s' : %s (%d)", filename, strerror(errno), errno);
    }

    return f;
}



FILE * ccFileOpenTextFile(char const * const filename)
{
    return ccFileOpen(filename, "w") ;
}



FILE * ccFileOpenBinaryFile(char const * const filename)
{
    return ccFileOpen(filename, "wb");
}



void ccFileRemove(char const * const filename)
{
    if(remove(filename) != 0)
    {
        ccRtPrintErrorAndExit("Failed to remove file %s: %s (%d)", filename, strerror(errno), errno);
    }
}



void ccFileCloseAndReplace(FILE       * const temp_file,
                           char const * const temp_filename,
                           char const * const permanent_filename)
{
    if(temp_file != NULL && ccFileExists(temp_filename))
    {
        if(fclose(temp_file) != 0)
        {
            ccRtPrintErrorAndExit("Closing file '%s' : %s (%d)", temp_filename, strerror(errno), errno);
        }

        // Rename temporary file to replace permanent file

        if(rename(temp_filename, permanent_filename) != 0)
        {
            ccRtPrintErrorAndExit("Failed to rename '%s' to '%s' : %s (%d)", temp_filename, permanent_filename, strerror(errno), errno);
        }

        ccRtPrintInVerbose("Renamed '%s' to '%s'", temp_filename, permanent_filename);
    }
}



uint32_t ccFileReadCmds(uint32_t cmd_idx, char *filename)
{
    FILE *f;

    // If filename is not supplied the return SUCCESS immediately

    if(filename == NULL)
    {
        return EXIT_SUCCESS;
    }

    // If cmd_idx is 0, it is being called from main() so use stdin if no filename supplied

    if(cmd_idx == 0 && *filename == '\0')
    {
        f = stdin;
        ccStatus(false);
    }
    else
    {
        // Call from ccCmdsRead() so if no filename supplied, return SUCCESS immediately

        if(*filename == '\0')
        {
            return EXIT_SUCCESS;
        }

        // Nested READ(cyc_sel) commands are not supported

        if(ccfile.read_cyc_sel != CC_NO_INDEX && ccfile.cyc_sel != CC_NO_INDEX)
        {
             ccRtPrintError("cyc_sel already active from previous READ command");
             return EXIT_FAILURE;
        }

        // Stack new file information

        ccRtPrintInVerbose("Reading file %s: %d", filename, ccrun.iteration_counter);

        if(ccInputPush(filename) == EXIT_FAILURE)
        {
            ccRtPrintError("Input file nesting limit (%u) reached", CC_INPUT_NESTED_FILES_LIMIT);
            return EXIT_FAILURE;
        }

        // Try to open the file given in the argument

        f = fopen(filename, "r");

        if(f == NULL)
        {
            ccInputPop();
            ccRtPrintError("Opening file '%s' for reading: %s (%d)", filename, strerror(errno), errno);
            return EXIT_FAILURE;
        }

        // If ccCmdsRead() was called from ccParseLine()

        if(cmd_idx == CMD_READ)
        {
            // Save cyc_sel in read_cyc_sel. It will be used as the default if no cycle sel is provided when getting/setting a parameter

            ccfile.read_cyc_sel = ccfile.cyc_sel;
        }
    }

    // Process all lines from the new file or from stdin

    uint32_t exit_status = EXIT_SUCCESS;
    char     line[CC_MAX_FILE_LINE_LEN];

    while(fgets(line, CC_MAX_FILE_LINE_LEN, f) != NULL)
    {
        size_t const line_length = strlen(line);
        bool         is_line_empty;

        // Check if line was too long (2 chars are needed for newline and terminating nul)

        if(line_length >= (CC_MAX_FILE_LINE_LEN - 1) && line[CC_MAX_FILE_LINE_LEN - 2] != '\n')
        {
            int input_ch;

            ccRtPrintError("Line exceeds maximum length (%u)", CC_MAX_FILE_LINE_LEN - 2);
            exit_status = EXIT_FAILURE;

            // Purge the rest of the line

            while((input_ch = fgetc(f)) != '\n' && input_ch != EOF);
        }
        else
        {
            // Remove the newline character and parse the input line

            if(line[line_length - 1] == '\n')
            {
                line[line_length - 1] = '\0';
            }

            exit_status = ccParseLine(line, &is_line_empty);
        }

        // Print prompt when using stdin

        if(ccInputIsStdin())
        {
            ccStatus(is_line_empty);
        }
        else // else when reading from file, break out if error or EXIT_FILE reported
        {
            if(exit_status != EXIT_SUCCESS)
            {
                break;
            }

            ccInputIncrementLineNumber();
        }
    }

    // IF EXIT command has been run then set exit_status according to whether we are reading a nested file

    if(exit_status == CC_EXIT_FILE)
    {
        exit_status = ccInputIsSecondaryFile() ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    // If we are reading a file then close it

    if(ccInputIsFile())
    {
        ccFileClose(f);
        ccInputPop();
        ccfile.read_cyc_sel = CC_NO_INDEX;
    }

    return exit_status;
}



void ccFileReadAll(char const * const path)
{
    struct dirent * files;
    struct stat     dir_file_stat;
    char            filename[CC_PATH_LEN];

    DIR * const dp = ccFileOpenDir(path);

    // Read contents of directory - this is not re-entrant but its only used during start-up so this is fine

    ccRtPrintInVerbose("Reading all files from %s", path);

    while((files = readdir(dp)) != NULL)
    {
        // Get status of each file

        ccFilePrintPath(filename, "%s/%s", path, files->d_name);

        if(stat(filename, &dir_file_stat) == -1)
        {
            ccRtPrintError("Failed to stat '%s' : %s (%d)", filename, strerror(errno), errno);

            ccFileCloseDir(dp, path);

            exit(EXIT_FAILURE);
        }

        // If regular file then read the contents

        if(S_ISREG(dir_file_stat.st_mode))
        {
            if(ccFileReadCmds(0, filename) == EXIT_FAILURE)
            {
                ccRtPrintError("Failed to read '%s'", filename);

                ccFileCloseDir(dp, path);

                exit(EXIT_FAILURE);
            }
        }
    }

    ccFileCloseDir(dp, path);
}



uint32_t ccFileConfigPar(struct CC_pars const * const par)
{
    // This function is called only for configuration parameters

    CC_ASSERT(ccParsIsConfig(par));

    char filename[CC_PATH_LEN];

    // Create file name according to whether parameter is sub_sel

    if(ccParsIsSubSel(par))
    {
        ccFilePrintPath(filename, "ref/%u/%s_%s", ccpars_global.sub_sel, par->parent_cmd->name, par->name);
    }
    else
    {
        ccFilePrintPath(filename, "config/%s_%s", par->parent_cmd->name, par->name);
    }

    FILE *f = ccFileOpenTextFile(CC_TMP_CFG_FILE);

    // Save parameter values to the file

    ccParsGet(f, par, CC_ALL_CYC_SELS, CC_NO_INDEX, ccfile.max_cmd_name_len, ccfile.max_par_name_len);

    // Try to replace old file by the new temporary file

    ccFileCloseAndReplace(f, CC_TMP_CFG_FILE, filename);

    return EXIT_SUCCESS;
}



uint32_t ccFileRefArmedPars(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    char filename[CC_PATH_LEN];

    // Create filename to include sub_sel and cyc_sel

    ccFilePrintPath(filename, "ref/%u/REF_%02u", sub_sel, cyc_sel);

    FILE *f = ccFileOpenTextFile(CC_TMP_REF_FILE);

    // Save all parameters used to arm the function

    ccParsGetRefArmedPars(f, sub_sel, cyc_sel);

    // Try to replace ref/{sub_sel}/REF_{cyc_sel} by the new temporary file

    ccFileCloseAndReplace(f, CC_TMP_REF_FILE, filename);

    return EXIT_SUCCESS;
}

// EOF
