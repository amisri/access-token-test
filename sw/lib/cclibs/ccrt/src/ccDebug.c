//! @file  ccrt/src/ccDebug.c
//!
//! @brief ccrt Debug functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <stdarg.h>

#include "ccCmds.h"

//! Enum for MODE REG_ERR_RATE parameter value decoding

static struct CC_pars_enum enum_reg_err_rate[]
= {
    { REG_ERR_RATE_REGULATION , "REGULATION"  },
    { REG_ERR_RATE_MEASUREMENT, "MEASUREMENT" },
    { 0                       , NULL          },
};



static void ccDebugPrint(FILE       * const f,
                         char         const delimiter,
                         char const * const prefix,
                         char const * const label,
                         char const * const format, ...)
{
    // Print prefix and label

    if(delimiter == ',')
    {
        // PowerSpy TABLE format requires leading comma because first column is for the timestamp, which can be omitted

        fprintf(f, ",%s,%s,", prefix, label);
    }
    else
    {
        uint32_t const indent = PARS_INDENT - strlen(prefix);

        fprintf(f, "%s %-*s", prefix, indent, label);
    }

    // Print value(s) using format

    va_list     argv;
    va_start(argv, format);
    vfprintf(f, format, argv);
    va_end(argv);

    fputc('\n', f);
}



static void ccDebugPrintLoad(FILE * const f, char const delimiter, char const * const prefix, struct REG_load_pars * const load_pars)
{
    ccDebugPrint(f, delimiter, prefix, "ohms_ser"     , PARS_FLOAT_FORMAT, load_pars->ohms_ser      );
    ccDebugPrint(f, delimiter, prefix, "ohms_par"     , PARS_FLOAT_FORMAT, load_pars->ohms_par      );
    ccDebugPrint(f, delimiter, prefix, "ohms_mag"     , PARS_FLOAT_FORMAT, load_pars->ohms_mag      );
    ccDebugPrint(f, delimiter, prefix, "henrys"       , PARS_FLOAT_FORMAT, load_pars->henrys        );
    ccDebugPrint(f, delimiter, prefix, "inv_henrys"   , PARS_FLOAT_FORMAT, load_pars->inv_henrys    );
    ccDebugPrint(f, delimiter, prefix, "gauss_per_amp", PARS_FLOAT_FORMAT, load_pars->gauss_per_amp );
    ccDebugPrint(f, delimiter, prefix, "ohms"         , PARS_FLOAT_FORMAT, load_pars->ohms          );
    ccDebugPrint(f, delimiter, prefix, "tc"           , PARS_FLOAT_FORMAT, load_pars->tc            );
    ccDebugPrint(f, delimiter, prefix, "ohms1"        , PARS_FLOAT_FORMAT, load_pars->ohms1         );
    ccDebugPrint(f, delimiter, prefix, "ohms2"        , PARS_FLOAT_FORMAT, load_pars->ohms2         );
    ccDebugPrint(f, delimiter, prefix, "gain0"        , PARS_FLOAT_FORMAT, load_pars->gain0         );
    ccDebugPrint(f, delimiter, prefix, "gain1"        , PARS_FLOAT_FORMAT, load_pars->gain1         );
    ccDebugPrint(f, delimiter, prefix, "gain2"        , PARS_FLOAT_FORMAT, load_pars->gain2         );
    ccDebugPrint(f, delimiter, prefix, "gain3"        , PARS_FLOAT_FORMAT, load_pars->gain3         );
    ccDebugPrint(f, delimiter, prefix, "meas_ohms"    , PARS_FLOAT_FORMAT, load_pars->meas_ohms     );
    ccDebugPrint(f, delimiter, prefix, "meas_henrys"  , PARS_FLOAT_FORMAT, load_pars->meas_henrys   );

    if(load_pars->sat.i_end > 0.0)
    {
        fputc('\n', f);
        ccDebugPrint(f, delimiter, prefix, "sat.henrys"          , PARS_FLOAT_FORMAT, load_pars->sat.henrys           );
        ccDebugPrint(f, delimiter, prefix, "sat.i_start"         , PARS_FLOAT_FORMAT, load_pars->sat.i_start          );
        ccDebugPrint(f, delimiter, prefix, "sat.i_end"           , PARS_FLOAT_FORMAT, load_pars->sat.i_end            );
        ccDebugPrint(f, delimiter, prefix, "sat.smoothing"       , PARS_FLOAT_FORMAT, load_pars->sat.smoothing        );
        ccDebugPrint(f, delimiter, prefix, "sat.df_dI_linear"    , PARS_FLOAT_FORMAT, load_pars->sat.df_dI_linear     );
        ccDebugPrint(f, delimiter, prefix, "sat.d2f_dI2_parabola", PARS_FLOAT_FORMAT, load_pars->sat.d2f_dI2_parabola );

        for(uint32_t i = 0 ; i < REG_LOAD_NUM_SAT_MODEL_SEGS ; i++)
        {
            char label[20];

            snprintf(label,20,"i[%u] f[%u] int_f[%u]", i, i, i);

            ccDebugPrint(f, delimiter, "LOAD SAT", label, PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT,
                            load_pars->sat.i[i],
                            load_pars->sat.f[i],
                            load_pars->sat.integrated_f[i]);
        }
    }

    fputc('\n', f);
}

static void ccDebugMeasFault(FILE * const f, char const delimiter, char const * const prefix, struct REG_meas_fault * const meas_fault)
{
    ccDebugPrint(f, delimiter, prefix, "invalid_counter" , PARS_INT_FORMAT, meas_fault->invalid_counter);
    ccDebugPrint(f, delimiter, prefix, "seq_counter"     , PARS_INT_FORMAT, meas_fault->seq_counter    );
    ccDebugPrint(f, delimiter, prefix, "flag"            , PARS_INT_FORMAT, meas_fault->flag           );
    fputc('\n', f);
}



static void ccDebugMeasLimits(FILE * const f, char const delimiter, char const * const prefix, struct REG_lim_meas * const lim_meas)
{
    ccDebugPrint(f, delimiter, prefix, "invert_limits"   , PARS_INT_FORMAT  ,*lim_meas->invert_limits  );
    ccDebugPrint(f, delimiter, prefix, "pos_trip"        , PARS_FLOAT_FORMAT, lim_meas->pos_trip       );
    ccDebugPrint(f, delimiter, prefix, "neg_trip"        , PARS_FLOAT_FORMAT, lim_meas->neg_trip       );
    ccDebugPrint(f, delimiter, prefix, "low"             , PARS_FLOAT_FORMAT, lim_meas->low            );
    ccDebugPrint(f, delimiter, prefix, "zero"            , PARS_FLOAT_FORMAT, lim_meas->zero           );
    ccDebugPrint(f, delimiter, prefix, "low_hysteresis"  , PARS_FLOAT_FORMAT, lim_meas->low_hysteresis );
    ccDebugPrint(f, delimiter, prefix, "zero_hysteresis" , PARS_FLOAT_FORMAT, lim_meas->zero_hysteresis);
    ccDebugPrint(f, delimiter, prefix, "flags.enabled"   , PARS_INT_FORMAT  , lim_meas->flags.enabled  );
    ccDebugPrint(f, delimiter, prefix, "flags.trip"      , PARS_INT_FORMAT  , lim_meas->flags.trip     );
    ccDebugPrint(f, delimiter, prefix, "flags.low"       , PARS_INT_FORMAT  , lim_meas->flags.low      );
    ccDebugPrint(f, delimiter, prefix, "flags.zero"      , PARS_INT_FORMAT  , lim_meas->flags.zero     );
    fputc('\n', f);
}



static void ccDebugRefLimits(FILE * const f, char const delimiter, char const * const prefix, struct REG_lim_ref * const lim_ref)
{
    ccDebugPrint(f, delimiter, prefix, "invert_limits" , PARS_INT_FORMAT  ,*lim_ref->invert_limits );
    ccDebugPrint(f, delimiter, prefix, "pos"           , PARS_FLOAT_FORMAT, lim_ref->pos           );
    ccDebugPrint(f, delimiter, prefix, "standby"       , PARS_FLOAT_FORMAT, lim_ref->standby       );
    ccDebugPrint(f, delimiter, prefix, "neg"           , PARS_FLOAT_FORMAT, lim_ref->neg           );
    ccDebugPrint(f, delimiter, prefix, "rate"          , PARS_FLOAT_FORMAT, lim_ref->rate          );
    ccDebugPrint(f, delimiter, prefix, "max_clip"      , PARS_FLOAT_FORMAT, lim_ref->max_clip      );
    ccDebugPrint(f, delimiter, prefix, "min_clip"      , PARS_FLOAT_FORMAT, lim_ref->min_clip      );
    ccDebugPrint(f, delimiter, prefix, "rate_clip"     , PARS_FLOAT_FORMAT, lim_ref->rate_clip     );
    ccDebugPrint(f, delimiter, prefix, "closeloop"     , PARS_FLOAT_FORMAT, lim_ref->closeloop     );
    ccDebugPrint(f, delimiter, prefix, "openloop"      , PARS_FLOAT_FORMAT, lim_ref->openloop      );
    ccDebugPrint(f, delimiter, prefix, "flags.unipolar", PARS_INT_FORMAT  , lim_ref->flags.unipolar);
    fputc('\n', f);
}



static void ccDebugVRefLimits(FILE * const f, char const delimiter, char const * const prefix, struct REG_lim_v_ref * const lim_v_ref)
{
    ccDebugPrint(f, delimiter, prefix, "max_clip"         , PARS_FLOAT_FORMAT, lim_v_ref->max_clip          );
    ccDebugPrint(f, delimiter, prefix, "min_clip"         , PARS_FLOAT_FORMAT, lim_v_ref->min_clip          );
    ccDebugPrint(f, delimiter, prefix, "max_clip_user"    , PARS_FLOAT_FORMAT, lim_v_ref->max_clip_user     );
    ccDebugPrint(f, delimiter, prefix, "min_clip_user"    , PARS_FLOAT_FORMAT, lim_v_ref->min_clip_user     );
    ccDebugPrint(f, delimiter, prefix, "max_clip_power"   , PARS_FLOAT_FORMAT, lim_v_ref->max_clip_power    );
    ccDebugPrint(f, delimiter, prefix, "min_clip_power"   , PARS_FLOAT_FORMAT, lim_v_ref->min_clip_power    );
    ccDebugPrint(f, delimiter, prefix, "i_quadrants41_max", PARS_FLOAT_FORMAT, lim_v_ref->i_quadrants41_max );
    ccDebugPrint(f, delimiter, prefix, "v0"               , PARS_FLOAT_FORMAT, lim_v_ref->v0                );
    ccDebugPrint(f, delimiter, prefix, "dvdi"             , PARS_FLOAT_FORMAT, lim_v_ref->dvdi              );
    ccDebugPrint(f, delimiter, prefix, "p_pos"            , PARS_FLOAT_FORMAT, lim_v_ref->p_pos             );
    ccDebugPrint(f, delimiter, prefix, "p_neg"            , PARS_FLOAT_FORMAT, lim_v_ref->p_neg             );
    fputc('\n', f);
}



static void ccDebugRmsLimits(FILE * const f, char const delimiter, char const * const prefix, struct REG_lim_rms * const lim_rms)
{
    ccDebugPrint(f, delimiter, prefix, "filter_factor"     , PARS_FLOAT_FORMAT, lim_rms->filter_factor      );
    ccDebugPrint(f, delimiter, prefix, "fault"             , PARS_FLOAT_FORMAT, lim_rms->fault              );
    ccDebugPrint(f, delimiter, prefix, "warning"           , PARS_FLOAT_FORMAT, lim_rms->warning            );
    ccDebugPrint(f, delimiter, prefix, "warning_hysteresis", PARS_FLOAT_FORMAT, lim_rms->warning_hysteresis );
    ccDebugPrint(f, delimiter, prefix, "input2_filter"     , PARS_FLOAT_FORMAT, lim_rms->input2_filter      );
    ccDebugPrint(f, delimiter, prefix, "rms"               , PARS_FLOAT_FORMAT, lim_rms->rms                );
    fputc('\n', f);
}



static void ccDebugErrLimits(FILE * const f, char const delimiter, char const * const prefix, struct REG_err * const err)
{
    ccDebugPrint(f, delimiter, prefix, "warning.threshold" , PARS_FLOAT_FORMAT, err->warning.threshold );
    ccDebugPrint(f, delimiter, prefix, "fault.threshold"   , PARS_FLOAT_FORMAT, err->fault.threshold   );
    ccDebugPrint(f, delimiter, prefix, "filter_time_iters" , PARS_INT_FORMAT,   err->filter_time_iters );
    fputc('\n', f);
}



static void ccDebugSimPC(FILE * const f, char const delimiter)
{
    for(uint32_t i = 0 ; i < REG_SIM_NUM_DEN_LEN; i++)
    {
        char label[16];

        snprintf(label,16,"num[%u] den[%u]", i, i);

        ccDebugPrint(f, delimiter, "SIMVS", label, PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT,
                        reg_mgr.sim_pars.vs.num[i],
                        reg_mgr.sim_pars.vs.den[i]);
    }
    ccDebugPrint(f, delimiter, "SIMVS", "rsp_delay_iters"   , PARS_FLOAT_FORMAT, reg_mgr.sim_pars.vs.rsp_delay_iters   );
    ccDebugPrint(f, delimiter, "SIMVS", "gain"              , PARS_FLOAT_FORMAT, reg_mgr.sim_pars.vs.gain              );
    ccDebugPrint(f, delimiter, "SIMVS", "is_vs_undersampled", PARS_INT_FORMAT,   reg_mgr.sim_pars.vs.is_vs_undersampled);
    fputc('\n', f);
}



static void ccDebugPrintMeas(FILE * const f, char const delimiter, char const * const prefix, struct REG_meas_filter * const meas_filter)
{
    ccDebugPrint(f, delimiter, prefix, "fir_length[0]"          , PARS_INT_FORMAT  , meas_filter->fir_length[0]          );
    ccDebugPrint(f, delimiter, prefix, "fir_length[1]"          , PARS_INT_FORMAT  , meas_filter->fir_length[1]          );
    ccDebugPrint(f, delimiter, prefix, "extrapolation_len_iters", PARS_INT_FORMAT  , meas_filter->extrapolation_len_iters);
    ccDebugPrint(f, delimiter, prefix, "float_to_integer"       , PARS_FLOAT_FORMAT, meas_filter->float_to_integer       );
    ccDebugPrint(f, delimiter, prefix, "integer_to_float"       , PARS_FLOAT_FORMAT, meas_filter->integer_to_float       );
    ccDebugPrint(f, delimiter, prefix, "extrapolation_factor"   , PARS_FLOAT_FORMAT, meas_filter->extrapolation_factor   );
    ccDebugPrint(f, delimiter, prefix, "delay_iters[0]"         , PARS_FLOAT_FORMAT, meas_filter->delay_iters[0]         );
    ccDebugPrint(f, delimiter, prefix, "delay_iters[1]"         , PARS_FLOAT_FORMAT, meas_filter->delay_iters[1]         );
    fputc('\n', f);
}



static void ccDebugPrintDelay(FILE * const f, char const delimiter, char const * const prefix, struct REG_delay_pars * const delay_pars)
{
    ccDebugPrint(f, delimiter, prefix, "delay_int_iters" , PARS_INT_FORMAT  , delay_pars->delay_int_iters );
    ccDebugPrint(f, delimiter, prefix, "delay_frac_iters", PARS_FLOAT_FORMAT, delay_pars->delay_frac_iters);
    fputc('\n', f);
}



static void ccDebugPrintReg(FILE * const f, char const delimiter, char const * const prefix, struct REG_rst_pars * const rst_pars)
{
    ccDebugPrint(f, delimiter, prefix, "status"              , PARS_STRING_FORMAT, ccParsEnumValueToString(enum_reg_status,      rst_pars->status));
    ccDebugPrint(f, delimiter, prefix, "meas_select"         , PARS_STRING_FORMAT, ccParsEnumValueToString(enum_reg_meas_select, rst_pars->reg_meas_select));
    ccDebugPrint(f, delimiter, prefix, "alg_index"           , PARS_INT_FORMAT   , rst_pars->alg_index              );
    ccDebugPrint(f, delimiter, prefix, "dead_beat"           , PARS_INT_FORMAT   , rst_pars->dead_beat              );
    ccDebugPrint(f, delimiter, prefix, "modulus_margin"      , PARS_FLOAT_FORMAT , rst_pars->modulus_margin         );
    ccDebugPrint(f, delimiter, prefix, "modulus_margin_hz"   , PARS_FLOAT_FORMAT , rst_pars->modulus_margin_hz      );

    ccDebugPrint(f, delimiter, prefix, "reg_period"          , PARS_FLOAT_FORMAT , rst_pars->reg_period             );
    ccDebugPrint(f, delimiter, prefix, "inv_reg_period"      , PARS_FLOAT_FORMAT , rst_pars->inv_reg_period         );
    ccDebugPrint(f, delimiter, prefix, "inv_reg_period_iters", PARS_FLOAT_FORMAT , rst_pars->inv_reg_period_iters   );
    ccDebugPrint(f, delimiter, prefix, "pure_delay_periods"  , PARS_FLOAT_FORMAT , rst_pars->pure_delay_periods     );
    ccDebugPrint(f, delimiter, prefix, "track_delay_periods" , PARS_FLOAT_FORMAT , rst_pars->track_delay_periods    );
    ccDebugPrint(f, delimiter, prefix, "track_delay"         , PARS_FLOAT_FORMAT , rst_pars->track_delay            );
    ccDebugPrint(f, delimiter, prefix, "ref_advance"         , PARS_FLOAT_FORMAT , rst_pars->ref_advance            );
    ccDebugPrint(f, delimiter, prefix, "ref_delay_periods"   , PARS_FLOAT_FORMAT , rst_pars->ref_delay_periods      );

    ccDebugPrint(f, delimiter, prefix, "reg_err_rate"        , PARS_STRING_FORMAT, ccParsEnumValueToString(enum_reg_err_rate, rst_pars->reg_err_rate));
    ccDebugPrint(f, delimiter, prefix, "reg_err_meas_select" , PARS_STRING_FORMAT, ccParsEnumValueToString(enum_reg_meas_select, rst_pars->reg_err_meas_select));

    ccDebugPrint(f, delimiter, prefix, "openloop_fwd_ref[0]" , PARS_FLOAT_FORMAT , rst_pars->openloop_forward.ref[0]);
    ccDebugPrint(f, delimiter, prefix, "openloop_fwd_ref[1]" , PARS_FLOAT_FORMAT , rst_pars->openloop_forward.ref[1]);
    ccDebugPrint(f, delimiter, prefix, "openloop_fwd_act[1]" , PARS_FLOAT_FORMAT , rst_pars->openloop_forward.act[1]);

    ccDebugPrint(f, delimiter, prefix, "openloop_rev_ref[1]" , PARS_FLOAT_FORMAT , rst_pars->openloop_reverse.ref[1]);
    ccDebugPrint(f, delimiter, prefix, "openloop_rev_act[0]" , PARS_FLOAT_FORMAT , rst_pars->openloop_reverse.act[0]);
    ccDebugPrint(f, delimiter, prefix, "openloop_rev_act[1]" , PARS_FLOAT_FORMAT , rst_pars->openloop_reverse.act[1]);

    ccDebugPrint(f, delimiter, prefix, "rst_order"           , PARS_INT_FORMAT   , rst_pars->rst_order              );
    ccDebugPrint(f, delimiter, prefix, "t0_correction"       , PARS_FLOAT_FORMAT , rst_pars->t0_correction          );

    for(uint32_t i = 0 ; i < REG_NUM_RST_COEFFS ; i++)
    {
        ccDebugPrint(f, delimiter, prefix, "R S T A B AS+BR" ,
                     PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT " "
                     PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT " " PARS_FLOAT_FORMAT ,
                        rst_pars->rst.r[i],
                        rst_pars->rst.s[i],
                        rst_pars->rst.t[i],
                        rst_pars->a[i],
                        rst_pars->b[i],
                        rst_pars->asbr[i]);
    }

    fputc('\n',f);
}



static void ccDebugPrintVreg(FILE * const f, char const delimiter, char const * const prefix)
{
    ccDebugPrint(f, delimiter, prefix, "f_neg"           , PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.f_neg           );
    ccDebugPrint(f, delimiter, prefix, "f_pos"           , PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.f_pos           );
    ccDebugPrint(f, delimiter, prefix, "vs_gain"         , PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.vs_gain         );
    ccDebugPrint(f, delimiter, prefix, "inv_num_vsources", PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.inv_num_vsources);
    ccDebugPrint(f, delimiter, prefix, "inv_vs_gain"     , PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.inv_vs_gain     );
    ccDebugPrint(f, delimiter, prefix, "ref_advance"     , PARS_FLOAT_FORMAT  , reg_mgr.v.reg_pars.ref_advance     );

    fputc('\n',f);
}


void ccDebug(FILE *f, char const delimiter)
{
     // Report load select and test_load select variables

    ccDebugPrint    (f, delimiter, "LOAD", "select" , PARS_INT_FORMAT, reg_mgr.par_values.load_select[0]);
    ccDebugPrintLoad(f, delimiter, "LOAD", &reg_mgr.load_pars);

    if(refMgrParValue(&ref_mgr,MODE_TEST_CYC_SEL) > 0)
    {
        ccDebugPrint    (f, delimiter, "TEST_LOAD", "select" , PARS_INT_FORMAT, reg_mgr.par_values.load_test_select[0]);
        ccDebugPrintLoad(f, delimiter, "TEST_LOAD", &reg_mgr.load_pars_test);
    }

    ccDebugPrint(f, delimiter, "SIMLOAD", "period_tc_ratio"     , PARS_FLOAT_FORMAT, reg_mgr.sim_pars.load.period_tc_ratio);
    ccDebugPrint(f, delimiter, "SIMLOAD", "is_load_undersampled", PARS_INT_FORMAT  , reg_mgr.sim_pars.load.is_load_undersampled);

    if(reg_mgr.sim_pars.load.tc_error != 0.0)
    {
        ccDebugPrint    (f, delimiter, "SIMLOAD", "tc_error", PARS_FLOAT_FORMAT, reg_mgr.sim_pars.load.tc_error);
        ccDebugPrintLoad(f, delimiter, "SIMLOAD", &reg_mgr.sim_pars.load.pars);
    }
    else
    {
        fputc('\n',f);
    }

    // B/I/V measurement faults

    ccDebugMeasFault(f, delimiter, "B_MEAS_FAULT", &reg_mgr.inputs.b_meas.fault);
    ccDebugMeasFault(f, delimiter, "I_MEAS_FAULT", &reg_mgr.inputs.i_meas.fault);
    ccDebugMeasFault(f, delimiter, "V_MEAS_FAULT", &reg_mgr.inputs.v_meas.fault);

    // B/I/V measurement limits

    ccDebugMeasLimits(f, delimiter, "B_MEAS_LIM", &reg_mgr.b.lim_meas);
    ccDebugMeasLimits(f, delimiter, "I_MEAS_LIM", &reg_mgr.i.lim_meas);
    ccDebugMeasLimits(f, delimiter, "V_MEAS_LIM", &reg_mgr.v.lim_meas);

    // B/I/V reference limits

    ccDebugRefLimits (f, delimiter, "B_REF_LIM", &reg_mgr.b.lim_ref);
    ccDebugRefLimits (f, delimiter, "I_REF_LIM", &reg_mgr.i.lim_ref);
    ccDebugRefLimits (f, delimiter, "V_REF_LIM", &reg_mgr.v.lim_ref);
    ccDebugVRefLimits(f, delimiter, "V_REF_LIM", &reg_mgr.v.lim_v_ref);

    // I/V RMS limits

    ccDebugRmsLimits(f, delimiter, "I_RMS"      , &reg_mgr.lim_i_rms);
    ccDebugRmsLimits(f, delimiter, "I_RMS_LOAD" , &reg_mgr.lim_i_rms_load);
    ccDebugRmsLimits(f, delimiter, "V_RATE_RMS" , &reg_mgr.lim_v_rate_rms);

    // Report internally calculated power converter variables

    ccDebugSimPC(f, delimiter);

    // Report filter and load simulation matrices when actuation if FIRING_REF

    if(regMgrParValue(&reg_mgr, VS_ACTUATION) == REG_FIRING_REF)
    {
        ccDebugErrLimits   (f, delimiter, "VERR", &reg_mgr.v.reg_err);
        ccDebugPrintVreg   (f, delimiter, "VREG");
    }

    // Voltage measurement

    ccDebugPrintDelay(f, delimiter, "DELAY_V", &reg_mgr.sim_pars.v_delay);

    // Field measurement and regulation

    if(reg_mgr.b.lim_meas.flags.enabled == true)
    {
        ccDebugPrintMeas (f, delimiter, "MEAS_B",  &reg_mgr.b.meas);
        ccDebugPrintDelay(f, delimiter, "DELAY_B", &reg_mgr.sim_pars.b_delay);
        ccDebugErrLimits (f, delimiter, "BERR",    &reg_mgr.b.reg_err);
        ccDebugPrintReg  (f, delimiter, "BREG",    &reg_mgr.b.last_op_rst_pars);

        if(refMgrParValue(&ref_mgr,MODE_TEST_CYC_SEL) > 0)
        {
            ccDebugPrintReg(f, delimiter, "BREG_TEST", &reg_mgr.b.last_test_rst_pars);
        }
    }

    // Current measurement and regulation

    if(reg_mgr.i.lim_meas.flags.enabled == true)
    {
        ccDebugPrintMeas (f, delimiter, "MEAS_I",  &reg_mgr.i.meas);
        ccDebugPrintDelay(f, delimiter, "DELAY_I", &reg_mgr.sim_pars.i_delay);
        ccDebugErrLimits (f, delimiter, "IERR",    &reg_mgr.i.reg_err);
        ccDebugPrintReg  (f, delimiter, "IREG",    &reg_mgr.i.last_op_rst_pars);

        if(refMgrParValue(&ref_mgr,MODE_TEST_CYC_SEL) > 0)
        {
            ccDebugPrintReg(f, delimiter, "IREG_TEST", &reg_mgr.i.last_test_rst_pars);
        }
    }
}

// EOF
