//! @file  ccrt/src/ccRt.c
//!
//! @brief ccrt main function
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <unistd.h>
#include <libgen.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <sys/utsname.h>
#include <sys/mman.h>
#include <ctype.h>

// Define GLOBALS to define all global variables in ccRt.c

#define GLOBALS

// Include libterm header file to get terminal control sequences

#include "libterm.h"

// Include ccrt program header files

#include "ccCmds.h"
#include "ccThread.h"
#include "ccRt.h"
#include "ccFile.h"
#include "ccInit.h"
#include "ccRun.h"
#include "ccSim.h"
#include "ccTest.h"
#include "ccNoise.h"
#include "ccLog.h"
#include "ccEvtlog.h"
#include "ccInput.h"

// Include cclibs generated header file

#include "sigStructsInit.h"

// Static variables

static char     cwd_buf[CC_PATH_LEN];                   // Current working directory on start-up, restored on exit

// Static function declarations

static void     ccRtChdir           (char const * path);
static void     ccRtAtExit          (void);
static void     ccRtInitTime        (void);
static void    *ccRtReadCmdsThread  (void *arg);

//! Prints the usage messages and exits the program with failure code

static void     ccRtExitWithUsageMessage(void);

//! Parses the command line arguments and initializes some of the ccrt global variables.
//!
//! @param[in] argc argc value passed to main
//! @param[in] argv argv array passed to main

static void     ccRtParseArguments(int argc, char **argv);

//! In batch mode the function gets various information from the test name and saves them in ccfile.test_file
//! structure for future use. In interactive mode all fields in ccfile.test.file are left empty.
//!
//! @param[in] test_path Path to the test, relative to 'tests' directory and without .cct extension

static void ccRtProcessTestFilename(char const * test_path);



static void ccRtChdir(char const * const path)
{
    if(chdir(path) != EXIT_SUCCESS)
    {
        ccRtPrintErrorAndExit("Changing directory to '%s' : %s (%d)", path, strerror(errno), errno);
    }
}



static void ccRtRegisterAtExit(void)
{
    if(atexit(ccRtAtExit) != 0)
    {
        ccRtPrintErrorAndExit("atexit failed");
    }
}



static void ccRtPrintWelcomeMessage(void)
{
    fprintf(stderr,"\nccrt: " TERM_CSI TERM_BOLD TERM_SGR "%s %s" TERM_NORMAL, __DATE__, __TIME__);

    if(ccRtModeIsBatch())
    {
        fprintf(stderr, " - Rate: %ux - Test: " TERM_CSI TERM_BOLD TERM_SGR "%s %s/%s" TERM_NORMAL "\n\n",
                timer.acceleration,
                ccfile.converter,
                ccfile.test_file.dirname,
                ccfile.test_file.basename);
    }
    else
    {
        fprintf(stderr," - Converter type: "         TERM_CSI TERM_BOLD TERM_SGR "%s"      TERM_NORMAL
                       "\n\niter_period: "           TERM_CSI TERM_BOLD TERM_SGR "%.4f ms" TERM_NORMAL
                       " - event_log_period_iters: " TERM_CSI TERM_BOLD TERM_SGR "%u\n"    TERM_NORMAL,
                ccfile.converter,
                1.0E-6 * (cc_float)regMgrParAppValue(&reg_pars, ITER_PERIOD_NS),
                timer.event_log_period_iters);
    }
}



int main(int argc, char **argv)
{
    // Process command line arguments

    ccRtParseArguments(argc, argv);

    // Save current working directory so that it can be restored at exit

    if(getcwd(cwd_buf, CC_PATH_LEN) == NULL)
    {
        ccRtPrintErrorAndExit("Getcwd failed: %s (%d)", strerror(errno), errno);
    }

    ccRtRegisterAtExit();

    // Try to set path to ccrt/converters/{converter}

    ccRtChdir(dirname(argv[0]));
    ccRtChdir("../../converters");
    ccRtChdir(ccfile.converter);

    // Read or create the CLOCK file for this converter type

    ccFileClock();

    ccRtPrintWelcomeMessage();

    // Detect if running a Linux kernel with RT PREEMPT patch

    ccThreadDetectRtPreempt();

    // Check the necessary sub-directories exist, or try to make them if they don't

    ccFileCreateDirs();

    // Initialize ccrt parameters, libreg, libref and libsig

    ccNoiseInit();

    ccInitPars();

    ccInitLibreg();

    polswitchMgrInit(&polswitch_mgr,
                     1.0E-9 * (cc_float)(timer.event_log_period_iters * regMgrParAppValue(&reg_pars, ITER_PERIOD_NS)),
                     &reg_mgr.i.lim_meas.flags.zero,
                     &reg_mgr.i.lim_meas.flags.low);

    ccInitLibref();

    // Now that libref parameters have been initialized, we can initialize the signal structures

    sigStructsInit(&sig_struct, &polswitch_mgr);

    sig_struct.cal_ref.named.internal.event.s.temp_c = ccpars_sim.int_cal_ref_temp;

    // Initialize liblog - this must be after ccInitLibref, otherwise some pointers will not have been set

    ccInitLogging();

    // Initialize simulation variables

    ccSimInit();

    // Initialize simulation time-related variables

    ccRtInitTime();

    // Initialize event log - after time has been set by ccRtInitTime()

    ccEvtlogInit();

    // Treat all libreg parameters - this must be after ccInitLogging()

    regMgrParsCheck(&reg_mgr);

    // Create the real-time processing thread and the timer thread - these must be running during the initialization phase

    ccThreadStart("RT processing", &cc_threads.real_time, ccRunRealTimeThread, -1);
    ccThreadStart("Timer",         &cc_threads.timer,     ccThreadTimerThread,  0);

    // Wait for ref state machine to run

    ccRtPrintInVerbose("Waiting for REF state machine to start");
    ccRtWaitForRefStateMachine();

    if(ccRtModeIsBatch() == false)
    {
        // Running iteratively from stdin in real-time
        // Read all config files in config directory unless -n (no_config) option specified

        if(ccrt_flags.no_config == false)
        {
            ccFileReadAll("config");

            // Process libreg configuration parameters

            regMgrParsCheck(&reg_mgr);

            // Read all config files in ref/{sub_sel} directories

            for(ccpars_global.sub_sel = 0; ccpars_global.sub_sel < CC_NUM_SUB_SELS; ccpars_global.sub_sel++)
            {
                char path[CC_PATH_LEN];

                ccFilePrintPath(path, "ref/%u", ccpars_global.sub_sel);
                ccFileReadAll(path);

                // Try arming the functions for this sub_sel for cyc_sel > 0

                for(uint32_t cyc_sel = 1; cyc_sel < CC_NUM_CYC_SELS ; cyc_sel++)
                {
                    ccRtArmRef(ccpars_global.sub_sel, cyc_sel, 0, NULL);
                }
            }

            ccpars_global.sub_sel = 0;
        }

        // Initialize MODE REG_MODE from MODE REG_MODE_CYC

        refMgrParValue(&ref_mgr,MODE_REG_MODE) = refMgrParValue(&ref_mgr,MODE_REG_MODE_CYC);
    }

    // Initialize the time of the first cycle in the supercycle to be 2 seconds after the start.

    ccSimPrepareNextCycle();

    // Create other threads

    ccThreadStart("Log writing",   &cc_threads.log_writing, ccLogSigsWriteThread,  -4);
    ccThreadStart("Event logging", &cc_threads.event_log,   ccEvtlogThread,        -2);

    // Signal that initialization is complete - this will enable the event logging

    ccrt_flags.initialized = true;

    // Create thread to read commands from stdin or the script named in test_file

    ccThreadStart("Command reading", &cc_threads.read_cmds, ccRtReadCmdsThread, -3);

    // Wait for threads to exit

    exit(ccThreadWait());
}



static void ccRtInitTime(void)
{
    // Initialize iteration time to start on a second boundary

    ccrun.iter_time.secs.abs = time(NULL);
    ccrun.iter_time.ns = 0;

    // When running in batch mode, round down to a 3s boundary to ensure consistent test results.
    // See regMgrRegBIsignalPrepareRT() for details of libreg's handling of time for regulation periods.

    if(ccRtModeIsBatch())
    {
        ccrun.iter_time.secs.abs = (ccrun.iter_time.secs.abs / 3) * 3;
    }

    // Set the start time and the last wake-up time to the same value as the actual iter_time

    ccrun.start_time                 = ccrun.iter_time;
    ccrun.wait.last_wakeup_iter_time = ccrun.iter_time;

    // Set the supercycle wake-up time to 2s in the future, to give time to initialize it later

    ccsim.supercycle.next_cycle_time.secs.abs = ccrun.iter_time.secs.abs + 2;
    ccsim.supercycle.next.idx = global_pars[GLOBAL_SUPER_CYCLE].max_num_elements;

    ccSimPrepareNextCycle();
}



static void *ccRtReadCmdsThread(void *arg)
{
    // Pass null to read from stdin

    char test_file[CC_PATH_LEN] = "\0";

    if(ccRtModeIsBatch())
    {
        ccFilePrintPath(test_file, "tests/%s/%s", ccfile.test_file.dirname, ccfile.test_file.basename);
    }

    return (void*)(intptr_t)ccCmdsRead(0, test_file);
}



static void ccRtAtExit(void)
{
    // Restore current working directory from when the program started

    ccRtChdir(cwd_buf);

    // Report number of missed iterations for each thread

    ccThreadPrintMissed(&cc_threads.timer);
    ccThreadPrintMissed(&cc_threads.real_time);
    ccThreadPrintMissed(&cc_threads.event_log);
    ccThreadPrintMissed(&cc_threads.log_writing);

    // Report test counter if tests have been run

    if(ccrun.test.counter != 0)
    {
        printf("1..%u\n",ccrun.test.counter);

        // Report to stderr the number of failed tests

        if(ccrun.test.not_ok_counter > 0)
        {
            fprintf(stderr,TERM_CSI TERM_BG_RED TERM_FG_WHITE TERM_BOLD TERM_SGR "%-20s: %u " TERM_NORMAL "\n",
                    " Failed ccrt tests", ccrun.test.not_ok_counter);
        }
    }
}



void ccRtWaitForRefStateMachine(void)
{
    if(pthread_self() != cc_threads.real_time.thread)
    {
        struct timespec sleep_time;
        uint32_t        init_ref_state_counter = ref_mgr.ref_state_counter;

        sleep_time.tv_sec = 0;
        sleep_time.tv_nsec = regMgrParValue(&reg_mgr, ITER_PERIOD_NS) / timer.acceleration;

        while(init_ref_state_counter == ref_mgr.ref_state_counter)
        {
            nanosleep(&sleep_time, NULL);
        }
    }
}



void ccRtArmRef(uint32_t sub_sel, uint32_t cyc_sel, uint32_t const num_pars, cc_float const * const par_values)
{
    // Lock mutex before arming to avoid a concurrent call to refEventStartFunc() from the Super Cycle thread

    if(ccRtStateIsInitialised() && refMgrParValue(&ref_mgr,MODE_USE_ARM_NOW) == CC_ENABLED)
    {
        ccRunLogEventStart(REF_EVENT_USE_ARM_NOW);
    }

    // Adjust sub_sel/cyc_sel according to MODE REF_SUB_SEL and MODE REF_CYC_SEL

    sub_sel = refMgrSubSel(&ref_mgr, sub_sel);
    cyc_sel = refMgrCycSel(&ref_mgr, cyc_sel);

    // Try to arm the reference

    ccpars_global.fg_errno = refArm(&ref_mgr, sub_sel, cyc_sel, ccpars_global.test_arm, num_pars, par_values, ccrun.fg_pars_idx[sub_sel][cyc_sel]);

    // Check error response

    if(ccpars_global.fg_errno != FG_OK && ccpars_global.hide_errors == CC_DISABLED)
    {
        struct FG_error * const fg_error = &ref_mgr.fg_error[cyc_sel + sub_sel * ref_mgr.num_cyc_sels];

        if(ccpars_global.fg_errno == fg_error->fg_errno)
        {
            ccRtPrintError("Failed to arm %s for sub_sel %u cyc_sel %u: %s (%u) %g, %g, %g, %g",
                    ccParsEnumValueToString(enum_fg_type, fg_error->fg_type),
                    sub_sel,
                    cyc_sel,
                    ccParsEnumValueToString(enum_fgerror_errno,fg_error->fg_errno),
                    fg_error->index,
                    fg_error->data[0],
                    fg_error->data[1],
                    fg_error->data[2],
                    fg_error->data[3]);
        }
        else
        {
            ccRtPrintError(ccParsEnumValueToString(enum_fgerror_errno,ccpars_global.fg_errno));
        }

        // If arming fails during start-up of ccrt, then re-arm with NONE, which always succeeds

        if(ccRtStateIsInitialised() == false)
        {
            ccpars_ref[sub_sel][cyc_sel].fg_type[0] = FG_NONE;

            refArm(&ref_mgr, sub_sel, cyc_sel, CC_DISABLED, 0, NULL, ccrun.fg_pars_idx[sub_sel][cyc_sel]);
        }
    }

    // On successful arming in interactive mode, overwrite the parameters in ref/{sub_sel}/REF_{cyc_sel} file

    if(ccpars_global.fg_errno == FG_OK && ccRtModeIsBatch() == false)
    {
        ccFileRefArmedPars(sub_sel, cyc_sel);
    }
}



static void ccRtExitWithUsageMessage(void)
{
    fprintf(stderr, "Usage: ccrt [-hvans] [converter_name [test_directory/test_file [time_acceleration (1-100)]]]\n"
                     "  -h   Display this message\n"
                     "  -v   Verbose mode - reports debug information to stderr\n"
                     "  -n   No config mode - suppress initialization of configuration parameters\n");

    exit(EXIT_FAILURE);
}



static void ccRtParseArguments(int argc, char **argv)
{
    int      c;
    uint32_t index;
    uint32_t non_opt_args;

    errno = 0;

    // Check the command line arguments for options

    while((c = getopt(argc, argv, "hvans")) != -1)
    {
        switch(c)
        {
            case 'v':

                ccrt_flags.verbose = true;
                break;

            case 'n':

                ccrt_flags.no_config = true;
                break;

            case 'h':
            default:

                ccRtExitWithUsageMessage();
        }
    }

    // Get number of non-option arguments

    non_opt_args = argc - optind;

    // Start from the first non-option argument

    index = optind;

    // Set the default time acceleration to 1 (real-time)

    timer.acceleration = 1;

    if(non_opt_args > 3)
    {
        ccRtExitWithUsageMessage();
    }
    else if(non_opt_args == 0)
    {
        strncpy(ccfile.converter, CC_DEFAULT_CONVERTER_NAME, CC_PATH_LEN - 1);
    }
    else
    {
        strncpy(ccfile.converter, argv[index++], CC_PATH_LEN - 1);

        if(non_opt_args > 1)
        {
            ccrt_flags.batch = true;

            ccRtProcessTestFilename(argv[index++]);

            if(non_opt_args > 2)
            {
                long  value;
                char *remaining_arg;

                value = strtol(argv[index++], &remaining_arg, 0);

                if(*remaining_arg != '\0' || errno != 0 || value < 1 || value > 1000)
                {
                    ccRtExitWithUsageMessage();
                }

                timer.acceleration = value;
            }
            else
            {
                timer.acceleration = CC_BATCH_ACCELERATION;
            }
        }
    }

    // If running interactively

    if(ccRtModeIsBatch() == false)
    {
        ccRtProcessTestFilename("interactive/stdin");
    }

    ccRtPrintInVerbose("ccrt: Running in verbose mode.");
}



static void ccRtProcessTestFilename(char const * test_path)
{
    if(ccRtModeIsBatch())
    {
        // Skip any leading '/'

        while(*test_path == '/')
        {
            test_path++;
        }

        if(*test_path == '\0')
        {
            ccRtExitWithUsageMessage();
        }

        // Save test file directory

        char path_buf[CC_PATH_LEN];      // Test file path buffer

        strncpy(path_buf, test_path, CC_PATH_LEN - 1);

        char * const dirname_p = dirname(path_buf);

        strncpy(ccfile.test_file.dirname, dirname_p, CC_PATH_LEN - 1);

        // Save test file basename with extension

        strncpy(path_buf, test_path, CC_PATH_LEN - 1);

        char * const basename_p = basename(path_buf);

        strncpy(ccfile.test_file.basename, basename_p, CC_PATH_LEN - 1);

        // Check that extension is .cct and save basename without the extension

        strcpy(ccfile.test_file.basename_no_ext, ccfile.test_file.basename);

        size_t basename_len = strlen(ccfile.test_file.basename_no_ext);

         if(ccfile.test_file.dirname[0] == '.' || basename_len < 5 || strcmp(&ccfile.test_file.basename_no_ext[basename_len - 4], ".cct") != 0)
         {
             ccRtExitWithUsageMessage();
         }

         ccfile.test_file.basename_no_ext[basename_len - 4] = '\0';
    }
    else
    {
        // Interactive mode

        ccFilePrintPath(ccfile.test_file.basename,       "stdin");
        ccFilePrintPath(ccfile.test_file.basename_no_ext,"stdin");
        ccFilePrintPath(ccfile.test_file.dirname,        "interactive");
    }
}



//! General message printing function
//!
//! Message format:
//! * '[label - ] message\n' when reading from stdin
//! * '[label at ] file_name:line_number - message\n' when reading from a file
//!
//! File name and line number are deduced by the function. A newline is added at the
//! end of the message.
//!
//! @param[in]  label   Optional label, displayed at the beginning, or NULL
//! @param[in]  format  Format string similar to printf format
//! @param[in]  args    Pointer to list of arguments corresponding to the format string

static void ccRtPrintMessage(char    const * const label,
                             char    const * const format,
                             va_list       * const args)
{
    // Print message header - label (optional) + file info (when reading from a file)

    if(ccInputIsStdin())
    {
        if(label != NULL)
        {
            fprintf(stderr, "%s - ", label);
        }
    }
    else
    {
        uint32_t const input_index = ccInputGetCurrentIndex();

        if(label != NULL)
        {
            fprintf(stderr, "%s at %s:%3u - ", label, ccInputGetPath(input_index), ccInputGetLineNumber(input_index));
        }
        else
        {
            fprintf(stderr, "%s:%3u - ", ccInputGetPath(input_index), ccInputGetLineNumber(input_index));
        }
    }

    // Print message to stderr

    vfprintf(stderr, format, *args);

    // Append newline

    fputc('\n', stderr);
}



void ccRtPrintError(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    ccRtPrintMessage(TERM_CSI TERM_FG_BLACK TERM_BG_YELLOW TERM_SGR " ERROR " TERM_NORMAL, format, &args);
    va_end(args);
}



void ccRtPrintErrorAndExit(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    ccRtPrintMessage(TERM_CSI TERM_FG_BLACK TERM_BG_YELLOW TERM_SGR " ERROR " TERM_NORMAL, format, &args);
    va_end(args);

    exit(EXIT_FAILURE);
}



void ccRtPrintWarning(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    ccRtPrintMessage("WARNING", format, &args);
    va_end(args);
}



void ccRtPrintInVerbose(char const * const format, ...)
{
    if(ccRtModeIsVerbose())
    {
        va_list args;

        va_start(args, format);
        ccRtPrintMessage(NULL, format, &args);
        va_end(args);
    }
}

// EOF
