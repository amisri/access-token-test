//! @file  ccrt/src/ccLogPmBuf.c
//!
//! @brief ccrt PM_BUF logging functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//#define LOG_READ_CONTROL_PRINT

#include <ctype.h>
#include "ccPars.h"
#include "ccLog.h"
#include "ccEvtlog.h"

// Include generated pm_buf header file and check the structure size is valid

#define PM_BUF_NO_PMD_FUNCTIONS

#include "pm_buf_0.h"

CC_STATIC_ASSERT(sizeof(struct PM_buf_0) == PM_BUF_0_SIZE, sizeof_PM_buf_0_not_PM_BUF_0_SIZE);


// Static functions

static bool ccLogPmBufTestNsTimestamp(uint32_t const timestamp_s, uint32_t const timestamp_ns)
{
    // Check that nanoseconds are in valid range

    if(timestamp_ns > 999999999)
    {
        return false;
    }

    // Check that seconds are a sane value (zero or within 30001000s of now - chosen because
    // 30M is the maximum window that libevtlog accepts for old events when reinitialising the log)

    if(   timestamp_s > 0
       && (   timestamp_s > ccrun.iter_time.secs.abs
           || (ccrun.iter_time.secs.abs - timestamp_s) > 30001000))
    {
        return false;
    }

    // Timestamp is ok

    return true;
}



static bool ccLogPmBufTestUsTimestamp(uint32_t const timestamp_s, uint32_t const timestamp_us)
{
    // Check that microseconds are in valid range

    if(timestamp_us > 999999)
    {
        return false;
    }

    // Check that seconds are a sane value (zero or within 30001000s of now - chosen because
    // 30M is the maximum window that libevtlog accepts for old events when reinitialising the log)

    if(   timestamp_s > 0
       && (   timestamp_s > ccrun.iter_time.secs.abs
           || (ccrun.iter_time.secs.abs - timestamp_s) > 30001000))
    {
        return false;
    }

    // Timestamp is ok

    return true;
}



static bool ccLogPmBufTestString(const char * string, uint32_t max_len)
{
    // Check string starts with printable characters

    while(max_len > 0 && isprint(*string))
    {
        max_len--;
        string++;
    }

    if(max_len == 0)
    {
        // String has no terminating nul

        return false;
    }

    // Return true if the string is nul terminated, false otherwise

    return *string == '\0';
}



static bool ccLogPmBufTestSignal(float * const signal, uint32_t const sig_len, float const min, float const max)
{
    for(uint32_t i = 0 ; i < sig_len ; i++)
    {
        if(signal[i] < min || signal[i] > max)
        {
            return false;
        }
    }

    return true;
}



static void ccLogPmBufTestEvtlog(struct PM_buf_0  * const pm_buf)
{
    uint32_t i;

    // Test evtlog timestamps in pm_buf and compare with the converted pm_buf data and check strings are valid in the converted data

    uint32_t bad_us_timestamps = 0;
    uint32_t bad_ns_timestamps = 0;
    uint32_t bad_strings = 0;

    for(i = 0 ; i < PM_BUF_0_EVTLOG_LEN ; i++)
    {
        if(ccLogPmBufTestUsTimestamp(ntohl(pm_buf->evtlog[i].timestamp_s), ntohl(pm_buf->evtlog[i].timestamp_us)) == false)
        {
            bad_us_timestamps++;
        }

        if(pm_buf_0_converted.evtlog_timestamps_ns[i] != (ntohl(pm_buf->evtlog[i].timestamp_s ) * 1000000000LL
                                                        + ntohl(pm_buf->evtlog[i].timestamp_us) * 1000LL))
        {
            bad_ns_timestamps++;
        }

        if(   ccLogPmBufTestString(pm_buf_0_converted.evtlog_property_ptr[i], PM_BUF_0_EVTLOG_PROPERTY_LEN+1) == false
           || ccLogPmBufTestString(pm_buf_0_converted.evtlog_symbol_ptr[i],   PM_BUF_0_EVTLOG_SYMBOL_LEN+1  ) == false
           || ccLogPmBufTestString(pm_buf_0_converted.evtlog_action_ptr[i],   PM_BUF_0_EVTLOG_ACTION_LEN+1  ) == false
           || ccLogPmBufTestString(pm_buf_0_converted.evtlog_status_ptr[i],   30) == false)
        {
            bad_strings++;
        }
    }

    // Report results with TAP protocol

    ccLogPrintTestTap(bad_us_timestamps == 0, "pm_buf -", "evtlog us timestamps", "Bad us timestamps: %u", bad_us_timestamps);
    ccLogPrintTestTap(bad_ns_timestamps == 0, "pm_buf -", "evtlog ns timestamps", "Bad ns timestamps: %u", bad_ns_timestamps);
    ccLogPrintTestTap(bad_strings == 0,       "pm_buf -", "evtlog strings",       "Bad strings: %u",       bad_strings);
}



static long long ccLogTimestampToNs(uint32_t const timestamp_s, uint32_t const timestamp_ns)
{
    return (long long)timestamp_s * 1000000000LL + (long long)timestamp_ns;
}



static void ccLogPmBufTestLiblog(struct PM_buf_0 * const pm_buf)
{
    // Test liblog buffer headers (first sample time and period) and signals

    long long const iab_period_ns              = ccLogTimestampToNs(pm_buf->iab.header.period_s, pm_buf->iab.header.period_ns);
    long long const iloop_period_ns            = ccLogTimestampToNs(pm_buf->iloop.header.period_s, pm_buf->iloop.header.period_ns);
    long long const iab_first_sample_time_ns   = pm_buf_0_converted.iab_timestamps_ns[0];
    long long const iloop_first_sample_time_ns = pm_buf_0_converted.iloop_timestamps_ns[0];

    bool const periods =    iab_period_ns   == ccLogTimestampToNs(log_mgr.period[LOG_ACQ].secs.abs, log_mgr.period[LOG_ACQ].ns)       // ACQ is known as the IAB log
                         && iloop_period_ns == ccLogTimestampToNs(log_mgr.period[LOG_I_REG].secs.abs, log_mgr.period[LOG_I_REG].ns);  // I_REG is known as the ILOOP log

    bool const first_sample_time =    ccLogPmBufTestNsTimestamp(pm_buf->iab.header.first_sample_time_s,   pm_buf->iab.header.first_sample_time_ns  ) == true
                                   && ccLogPmBufTestNsTimestamp(pm_buf->iloop.header.first_sample_time_s, pm_buf->iloop.header.first_sample_time_ns) == true
                                   && iab_first_sample_time_ns   == ccLogTimestampToNs(pm_buf->iab.header.first_sample_time_s, pm_buf->iab.header.first_sample_time_ns)
                                   && iloop_first_sample_time_ns == ccLogTimestampToNs(pm_buf->iloop.header.first_sample_time_s, pm_buf->iloop.header.first_sample_time_ns);

    bool const timestamp =    (iab_first_sample_time_ns   + iab_period_ns   * (PM_BUF_0_IAB_LEN  -1)) == pm_buf_0_converted.iab_timestamps_ns[PM_BUF_0_IAB_LEN-1]
                           && (iloop_first_sample_time_ns + iloop_period_ns * (PM_BUF_0_ILOOP_LEN-1)) == pm_buf_0_converted.iloop_timestamps_ns[PM_BUF_0_ILOOP_LEN-1];

    bool const signals =    ccLogPmBufTestSignal(pm_buf->iab.signals.i_a,      PM_BUF_0_IAB_LEN   ,  0.0F,  0.0F) == true
                         && ccLogPmBufTestSignal(pm_buf->iab.signals.i_a,      PM_BUF_0_IAB_LEN   ,  0.0F,  0.0F) == true
                         && ccLogPmBufTestSignal(pm_buf->iloop.signals.i_meas, PM_BUF_0_ILOOP_LEN , -1.0F,  1.1F) == true
                         && ccLogPmBufTestSignal(pm_buf->iloop.signals.i_ref , PM_BUF_0_ILOOP_LEN , -1.0F,  1.1F) == true
                         && ccLogPmBufTestSignal(pm_buf->iloop.signals.v_ref , PM_BUF_0_ILOOP_LEN ,-10.0F, 10.0F) == true
                         && ccLogPmBufTestSignal(pm_buf->iloop.signals.v_meas, PM_BUF_0_ILOOP_LEN ,-10.0F, 10.0F) == true;

    // Report results with TAP protocol

    ccLogPrintTestTap(first_sample_time, "pm_buf -", "liblog first sample time", "invalid");
    ccLogPrintTestTap(periods,           "pm_buf -", "liblog periods",           "invalid");
    ccLogPrintTestTap(timestamp,         "pm_buf -", "liblog timestamp",         "invalid");
    ccLogPrintTestTap(signals,           "pm_buf -", "liblog signal",            "invalid");
}



static void ccLogTestPmBuf(char * const file_name)
{
    // Open the PM_BUF binary file to check the size

    struct PM_buf_0 pm_buf;

    FILE *f = ccFileOpen(file_name, "rb");

    fseek(f, 0L, SEEK_END);

    size_t file_size = ftell(f);

    bool file_size_ok = (file_size == PM_BUF_0_SIZE);

    ccLogPrintTestTap(file_size_ok, "pm_buf -", "file size", "incorrect");

    if(file_size_ok == false)
    {
        fclose(f);
        return;
    }

    // Read the file into the buffer and check the size read

    rewind(f);

    file_size = fread(&pm_buf, 1, PM_BUF_0_SIZE, f);

    fclose(f);

    file_size_ok = (file_size == PM_BUF_0_SIZE);

    ccLogPrintTestTap(file_size_ok, "pm_buf -", "file read", "incorrect length");

    if(file_size_ok == false)
    {
        return;
    }

    // Check the data version (always in network byte order)

    ccLogPrintTestTap(ntohl(pm_buf.version) == PM_BUF_0_VERSION, "pm_buf -", "version", "mismatch");

    // Reset the converted data arrays

    memset(&pm_buf_0_converted, 0, sizeof(pm_buf_0_converted));

    // Convert event log data

    pm_buf_0_ConvertEvtlog(&pm_buf);

    // Convert liblog data

    pm_buf_0_ConvertLiblog(&pm_buf);

    // Test event log

    ccLogPmBufTestEvtlog(&pm_buf);

    // Test liblog data

    ccLogPmBufTestLiblog(&pm_buf);

    return;
}



static enum LOG_read_control_status ccLogWritePmBufLog(FILE * const f, uint32_t const menu_index)
{
    struct LOG_read_request * const read_request = &log_read_request[menu_index];
    struct LOG_read_control * const read_control = &log_read_control[menu_index];
    struct LOG_log    const * const log          = &log_read.log[log_menus.log_index[menu_index]];

    ccpars_log.read_timing.time_origin = ccpars_log.time_origin;

    logPrepareReadPmBufRequest(&log_menus, menu_index, &ccpars_log.read_timing, read_request);

    // Copy read_request into log_structs. This simulates the transfer of the structure between CPUs on a multi-CPU platform.

    memcpy(&log_read.request, read_request, sizeof(struct LOG_read_request));

    // Process the read request to generate the read control and header

    logReadRequest(&log_read, read_control);

    logReadControlPrint(read_control);

    if(read_control->status == LOG_READ_CONTROL_SUCCESS)
    {
        // Read request was successful so extract signal data to local linear log buffer

        ccLogGetOutput(read_control);

        if(ccpars_log.internal_tests)
        {
            // LOG INTERNAL_TESTS is ENABLED so run tests on read_control

            ccLogTestReadControl(log, read_control);
        }

        // Write header and data to pm_buf file

        size_t written = fwrite(&log_read.header, sizeof(uint8_t), read_control->header_size, f);

        if(written != read_control->header_size)
        {
            ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to header size (%d)", written, read_control->header_size);
        }

        written = fwrite(local_log_buffer, sizeof(uint8_t), read_control->data_size, f);

        if(written != read_control->data_size)
        {
            ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to data size (%d)", written, read_control->data_size);
        }
    }

    return read_control->status;
}



void ccLogWritePmBuf(void)
{
//    char const * const temp_filename = CC_TMP_PM_BUF_FILE;

    FILE *f = ccFileOpenBinaryFile(CC_TMP_PM_BUF_FILE);

    // Write PM_BUF version long word

    uint32_t pm_buf_version = htonl(LOG_PM_BUF_VERSION);

    fwrite(&pm_buf_version, sizeof(pm_buf_version), 1, f);

    // Write the event log in binary

    ccEvtlogPrintBin(f);

    // Write liblog PM_BUF logs into temporary file

    for(uint32_t menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {
        if(logMenuIsInPmBuf(&log_menus, menu_index))
        {
            ccRtPrintInVerbose("Dumping PM_BUF log for log menu %s", log_menus.names[menu_index]);

            enum LOG_read_control_status status = ccLogWritePmBufLog(f, menu_index);

            ccLogPrintTestTap(status == LOG_READ_CONTROL_SUCCESS,
                              "Write PM_BUF",
                              log_menus.names[menu_index],
                              ccParsEnumValueToString(enum_read_control_status, status));
        }
    }

    // Prepare directory name and create directory if it doesn't exist

    char dir_name[CC_PATH_LEN];

    ccLogSetDirName(dir_name);

    // Prepare filename and move temporary file to this name

    char filename[CC_PATH_LEN];

    ccFilePrintPath(filename, LOG_PATH "/pm_buf.bin",
                    ccfile.converter,
                    ccfile.test_file.dirname,
                    ccfile.test_file.basename_no_ext,
                    dir_name);

    ccFileCloseAndReplace(f, CC_TMP_PM_BUF_FILE, filename);

    // Test saved PM_BUF data

    if(ccpars_log.internal_tests)
    {
        ccLogTestPmBuf(filename);
    }
}

// EOF
