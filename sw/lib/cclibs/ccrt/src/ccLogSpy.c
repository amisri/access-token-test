//! @file  ccrt/src/ccLogSpy.c
//!
//! @brief ccrt Spy logging functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//#define LOG_READ_CONTROL_PRINT

#include "ccPars.h"
#include "ccLog.h"

static void ccLogTestSpy(struct LOG_read_control const * const read_control)
{
    uint32_t const num_signals           = read_control->num_signals;
    uint32_t const predicted_header_size = sizeof(struct SPY_v3_buf_header) + num_signals * sizeof(struct SPY_v3_sig_header);

    ccLogPrintTestTap(predicted_header_size == read_control->header_size,
                      "Spy header size",
                      "",
                      "Predicted header size (%d) != actual header size (%d)", predicted_header_size, read_control->header_size);
}



static void ccLogWriteSpyCsv(FILE                        * const f,
                             struct LOG_spy_header const * const header,
                             char                  const * const buf_name,
                             char                  const * const device_name)
{
    struct SPY_v3_buf_header const * const buf_header          = &header->buf_header;
    uint32_t                         const num_samples         = buf_header->num_samples;
    uint32_t                         const num_signals         = buf_header->num_signals;
    bool                             const log_is_digital      = (buf_header->meta & SPY_BUF_META_ANA_SIGNALS) == 0;

    // Derive epoch from the earlier of time_origin and timestamp (which is always the first sample time for ccrt)

    uint32_t const epoch = buf_header->time_origin_s < buf_header->timestamp_s
                         ? buf_header->time_origin_s
                         : buf_header->timestamp_s;

    // Print CSV header line

    struct CC_ns_time const first_sample_time = { { buf_header->timestamp_s - epoch }, buf_header->timestamp_ns };
    struct CC_ns_time const period            = { { buf_header->period_s }, buf_header->period_ns };

    fprintf(f, "source:ccrt device:%s", device_name);
    fprintf(f, " epoch:%u", epoch);
    fprintf(f, " timeOrigin:%u.%09d", buf_header->time_origin_s - epoch, buf_header->time_origin_ns);
    fprintf(f, " firstSampleTime:%u.%09d", first_sample_time.secs.abs, first_sample_time.ns);
    fprintf(f, " period:%d.%09d", period.secs.rel, period.ns);

    // Write buffer name and add a suffice for cyc_sel or log_dir_idx (if >1)

    fprintf(f, " name:%s", buf_name);

    if(ccpars_log.cyc_sel > 0)
    {
        fprintf(f, "(%u)", ccpars_log.cyc_sel);
    }
    else if(ccfile.log_dir_idx > 1)
    {
        fprintf(f, "[%u]", ccfile.log_dir_idx);
    }

    if(log_is_digital)
    {
        fputs(" type:digital",f);
    }
    else
    {
        fputs(" type:analog",f);
    }

    struct SPY_v3_sig_header const * const sig_header = header->sig_headers;

    for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
    {
        // Print signal name

        fprintf(f, ",%s",  sig_header[signal_index].name);

        // Print meta data tags and time offset (if non-zero)

        if((sig_header[signal_index].meta & SPY_SIG_META_STEPS) != 0)
        {
            fputs(" STEP",f);
        }

        if(sig_header[signal_index].time_offset_ns != 0)
        {
            struct CC_ns_time const time_offset = cctimeNsAddOffsetRT(cc_zero_ns, sig_header[signal_index].time_offset_ns);

            fprintf(f, " %+d.%09u", time_offset.secs.rel, time_offset.ns);
        }
    }

    fputc('\n',f);

    // Print CSV signal records

    for(int32_t sample_index = 0; sample_index < num_samples; sample_index++)
    {
        // Print time stamp

        struct CC_ns_time sample_time = cctimeNsAddRT(first_sample_time, cctimeNsMulRT(period, sample_index));

        fprintf(f, "%d.%09d", sample_time.secs.rel, sample_time.ns);

        // Print signal values

        if(log_is_digital)
        {
            uint32_t const dig_mask = local_log_buffer[sample_index];

            for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
            {
                fputc(',', f);
                fputc( dig_mask & (1 << sig_header[signal_index].dig_bit_index) ? '1' : '0', f);
            }
        }
        else
        {
            for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
            {
                cc_float const * const float_buf = (cc_float *)&local_log_buffer[signal_index * num_samples + sample_index];

                fprintf(f, ",%.8g", *float_buf);
            }
        }

        fputc('\n',f);
    }
}



static void ccLogWriteSpyBin(FILE                          * const f,
                             struct LOG_spy_header   const * const header,
                             struct LOG_read_control const * const read_control)
{
    size_t written = fwrite(header, sizeof(uint8_t), read_control->header_size, f);

    if(written != read_control->header_size)
    {
        ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to header size (%d)", written, read_control->header_size);
    }

    written = fwrite(local_log_buffer, sizeof(uint8_t), read_control->data_size, f);

    if(written != read_control->data_size)
    {
        ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to data size (%d)", written, read_control->data_size);
    }
}



//! Write spy file in binary or CSV format
//!
//! @param[in]  format       Pointer to "bin" or "csv"
//! @param[in]  dir_name     Pointer to directory name where the Spy file should be created
//! @param[in]  menu_index   Log menu index

static void ccLogWriteSpyFile(char const * const format, char const * const dir_name, uint32_t const menu_index)
{
    // Create new file with binary or ASCII type

    FILE * const f = ccFileOpen(CC_TMP_LOG_FILE, *format == 'b' ? "wb" : "w");

    ccRtPrintInVerbose("Dumping SPY %s log for %s", format, log_menus.names[menu_index]);

    if(*format == 'b')
    {
        ccLogWriteSpyBin(f,&log_read.header.spy, &log_read_control[menu_index]);
    }
    else
    {
        ccLogWriteSpyCsv(f, &log_read.header.spy, log_menus.names[menu_index], dir_name);
    }

    char filename[CC_PATH_LEN];

    ccFilePrintPath(filename, LOG_PATH "/%s.%s",
                    ccfile.converter,
                    ccfile.test_file.dirname,
                    ccfile.test_file.basename_no_ext,
                    dir_name,
                    log_menus.names[menu_index],
                    format);

    ccFileCloseAndReplace(f, CC_TMP_LOG_FILE, filename);
}



static void ccLogSpySetParsFromHeader(struct LOG_spy_header * const header)
{
    ccpars_logspy.version      = header->buf_header.version;
    ccpars_logspy.meta         = header->buf_header.meta;
    ccpars_logspy.num_signals  = header->buf_header.num_signals;

    uint32_t sig_idx;

    for(sig_idx = 0 ; sig_idx < ccpars_logspy.num_signals ; sig_idx++)
    {
        ccpars_logspy.sig_meta      [sig_idx] = header->sig_headers[sig_idx].meta;
        ccpars_logspy.dig_bit_index [sig_idx] = header->sig_headers[sig_idx].dig_bit_index;
        ccpars_logspy.time_offset_ns[sig_idx] = header->sig_headers[sig_idx].time_offset_ns;
        ccpars_logspy.name          [sig_idx] = header->sig_headers[sig_idx].name;
    }
}



static void ccLogRenameDimSignals(uint32_t const menu_index)
{
    for(uint32_t sig_index = 0 ; sig_index < 4 ; sig_index++)
    {
        char new_signal_name[SPY_SIG_NAME_LEN];

        sprintf(new_signal_name, "%s_ANA_%u", log_menus.names[menu_index], (sig_index+1));

        logChangeSpySignalName(&log_read.header.spy, sig_index, new_signal_name);
    }
}


//! Creates a Spy log file for each enabled log menu in CSV or binary formats

void ccLogWriteSpy(void)
{
    bool  dir_name_set = false;
    bool  write_file = ccpars_log.csv_spy == CC_ENABLED || ccpars_log.binary_spy == CC_ENABLED;
    char  dir_name[CC_PATH_LEN];

    for(enum LOG_menu_index menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {
        struct LOG_read_request * const read_request = &log_read_request[menu_index];
        struct LOG_read_control * const read_control = &log_read_control[menu_index];

        if(ccpars_logmenu[menu_index] == CC_ENABLED)
        {
            // Log menu is ENABLED - prepare and run a read request for the log menu

            ccpars_log.read_timing.time_origin = ccpars_log.time_origin;

            // Set previous first sample time from previous read_control for this menu index, if previous acquisition was for SPY

            if(read_request->action == LOG_READ_GET_SPY)
            {
                ccpars_log.read_timing.prev_first_sample_time = read_control->first_sample_time;
            }
            else
            {
                ccpars_log.read_timing.prev_first_sample_time.secs.abs = 0;
            }

            // Prepare the read_request structure

            logPrepareReadMenuRequest(&log_menus,
                                      menu_index,
                                      LOG_READ_GET_SPY,
                                      ccrun.log.cyc_sel,
                                      &ccpars_log.read_timing,
                                      ccpars_log.sub_sampling_period,
                                      read_request);

            // Copy read_request into log_structs. This simulates the transfer of the structure between CPUs on a multi-CPU platform.

            memcpy(&log_read.request, read_request, sizeof(struct LOG_read_request));

            // Process the read request to generate the read control and header

            logReadRequest(&log_read, read_control);

            logReadControlPrint(read_control);

            // Check the read_control status

            if(ccpars_log.internal_tests == CC_DISABLED)
            {
                // LOG INTERNAL_TESTS is DISABLED so just check and report the overall read_control status

                ccLogPrintTestTap(read_control->status == LOG_READ_CONTROL_SUCCESS,
                                  "Write Spy Buffer",
                                  log_menus.names[menu_index],
                                  "(%s)", ccParsEnumValueToString(enum_read_control_status, read_control->status));
            }

            if(read_control->status == LOG_READ_CONTROL_SUCCESS)
            {
                // Read request successful

                ccLogGetOutput(read_control);

                // Set LOGSPY parameters from SPY header

                ccLogSpySetParsFromHeader(&log_read.header.spy);

                // Test read_control

                if(ccpars_log.internal_tests)
                {
                    ccLogTestReadControl(&log_read.log[log_menus.log_index[menu_index]], read_control);

                    ccLogTestSpy(read_control);
                }

                if(write_file)
                {
                    // Rename DIM signals

                    if(menu_index >= LOG_MENU_CODIM)
                    {
                       ccLogRenameDimSignals(menu_index);
                    }

                    // CSV and/or binary file requested by the user

                    if(dir_name_set == false)
                    {
                        // On first pass, prepare the directory name

                        dir_name_set = true;

                        ccLogSetDirName(dir_name);
                    }

                    // Write CSV and/or binary spy files as required

                    if(ccpars_log.csv_spy == CC_ENABLED)
                    {
                        ccLogWriteSpyFile("csv", dir_name, menu_index);
                    }

                    if(ccpars_log.binary_spy == CC_ENABLED)
                    {
                        ccLogWriteSpyFile("bin", dir_name, menu_index);
                    }
                }
            }
            else
            {
                // Read request failed - reset LOGSPY parameters

                memset(&ccpars_logspy, 0 , sizeof(ccpars_logspy));
            }
        }
        else
        {
            // Menu index is DISABLED - reset the read_request and read_control structures for this menu

            memset(read_request, 0, sizeof(struct LOG_read_request));
            memset(read_control, 0, sizeof(struct LOG_read_control));
        }
    }
}

// EOF
