//! @file  ccrt/src/ccRun.c
//!
//! @brief ccrt real-time processing functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "ccThread.h"
#include "ccCmds.h"
#include "ccRt.h"
#include "ccLog.h"
#include "ccRun.h"
#include "ccSim.h"
#include "ccParse.h"

// Static function declarations

static void ccRunLogSignals        (bool const iter_1s_boundary, bool const iter_200ms_boundary);
static void ccRunWakeCommandThread (void);
static void ccRunRealTimeDeltaRef  (void);
static void ccRunPrepareMeas       (void);

//! Updates STATE COUNTER property on each REF state change. If the state changes then
//! value corresponding to the REF_state enum entry for the new state gets incremented.

static void ccRunCountRefStateTransitions(void)
{
    static enum REF_state prev_ref_state;
    enum REF_state        ref_state = refMgrVarValue(&ref_mgr, REF_STATE);

    // If REF state has changed

    if(prev_ref_state != ref_state)
    {
        // Increase the counter for the new state

        ccpars_state.counter[ref_state]++;
    }

    prev_ref_state = ref_state;
}



static void ccRunPolswitch(void)
{
    // Run polarity switch manager

    polswitchMgr(&polswitch_mgr,
                 ccpars_polswitch.simulate,
                 ccpars_polswitch.positive,
                 ccpars_polswitch.negative,
                 ccpars_polswitch.fault,
                 ccpars_polswitch.locked);

    // Log polarity switch signals

    ccrun.log.polswitch.mode_user      = 5.0F + (cc_float)polswitchParValue(&polswitch_mgr,MODE_USER)       / 3.0F;
    ccrun.log.polswitch.mode_auto      = 4.0F + (cc_float)polswitchParValue(&polswitch_mgr,MODE_AUTO)       / 3.0F;
    ccrun.log.polswitch.req_state      = 3.0F + (cc_float)polswitchVarValue(&polswitch_mgr,REQUESTED_STATE) / 3.0F;
    ccrun.log.polswitch.exp_state      = 2.0F + (cc_float)polswitchVarValue(&polswitch_mgr,EXPECTED_STATE)  / 3.0F;
    ccrun.log.polswitch.state          = 0.0F + (cc_float)polswitchVarValue(&polswitch_mgr,STATE)           / 3.0F;
    ccrun.log.polswitch.time_remaining =      - (cc_float)polswitchVarValue(&polswitch_mgr, TIME_REMAINING);

    struct CC_ns_time const sample_time = cctimeUsToNsRT(ccrun.evtlog_time);

    logStoreContinuousRT(&log_structs.log[LOG_POLSWITCH],   sample_time);
    logStoreContinuousRT(&log_structs.log[LOG_POLSWIFLAGS], sample_time);
}


void *ccRunRealTimeThread(void *args)
{
    pthread_mutex_lock(&cc_threads.real_time.condition_mutex);

    for(;;)
    {
        // Wait to be woken by the condition variable signal

        cc_threads.real_time.wake_up_flag = false;

        while(cc_threads.real_time.wake_up_flag == false)
        {
            pthread_cond_wait(&cc_threads.real_time.condition_cond, &cc_threads.real_time.condition_mutex);
        }

        ccrun.iteration_counter++;

        // Adjust iteration time stamp

        bool              iter_1s_boundary = false;
        struct CC_ns_time iter_time;

        // Increment iter_time without using cctimeNsAddRT() so that iter_1s_boundary can be set on 1s boundaries

        iter_time.secs.abs = ccrun.iter_time.secs.abs;
        iter_time.ns = ccrun.iter_time.ns + regMgrParValue(&reg_mgr, ITER_PERIOD_NS);

        if(iter_time.ns >= 1000000000)
        {
            iter_time.secs.abs++;
            iter_time.ns    -= 1000000000;
            iter_1s_boundary = true;
        }

        uint32_t  const iter_ms_time            = iter_time.ns / 1000000;
        uint32_t  const iter_ns_time_mod_10T    = iter_time.ns % (10 * regMgrParValue(&reg_mgr, ITER_PERIOD_NS));
        uint32_t  const iter_ns_time_mod_200Mns = iter_time.ns % 200000000;

        // Create time boundary flags

        bool const set_vfeedfwd_flag   = (iter_ns_time_mod_10T    < ccrun.iter_ns_time_mod_10T);
        bool const iter_200ms_boundary = (iter_ns_time_mod_200Mns < ccrun.iter_ns_time_mod_200Mns);

        // Set current iteration time with protection using a mutex

        pthread_mutex_lock(&ccrun.iter_time_mutex);

        ccrun.iter_time               = iter_time;
        ccrun.iter_us_time.secs.abs   = iter_time.secs.abs;
        ccrun.iter_us_time.us         = iter_time.ns / 1000;
        ccrun.iter_ns_time_mod_10T    = iter_ns_time_mod_10T;
        ccrun.iter_ns_time_mod_200Mns = iter_ns_time_mod_200Mns;
        ccrun.iter_ms_time_mod_20     = iter_ms_time % 20;

        pthread_mutex_unlock(&ccrun.iter_time_mutex);

        // Update the voltage reference feed forward signal every tenth iteration period

        if(set_vfeedfwd_flag == true)
        {
            // Declare the V_FF set by ccSimCircuitRT() on the the previous millisecond

            refMgrSetVfeedfwdRT(&ref_mgr, ccpars_sim.vfeedfwd, iter_time);
        }

        // Run the supercycle simulation

        ccSimSuperCycle();

        // Run the circuit simulation

        ccSimCircuitRT(set_vfeedfwd_flag);

        // Prepare the ADC channels

        ccRunPrepareMeas();

        // Call sigMgrRT() with start_average1 set to true on 200ms boundaries.

        sigMgrRT(&sig_struct.mgr, iter_200ms_boundary, iter_time);

        // Calculate real time delta reference (the direct external reference is directly set by ccrt parameter DIRECT EXT_REF)

        ccRunRealTimeDeltaRef();

        // Set measurements in libreg

        regMgrMeasSetBmeasRT   (&reg_mgr, sigVarValue(&sig_struct, transducer, b_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetImeasRT   (&reg_mgr, sigVarValue(&sig_struct, select    , i_meas  , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetImagSatRT (&reg_mgr, ccpars_sim.i_mag_sat);
        regMgrMeasSetIcapaRT   (&reg_mgr, sigVarValue(&sig_struct, transducer, i_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetVmeasRT   (&reg_mgr, sigVarValue(&sig_struct, transducer, v_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetVacRT     (&reg_mgr, sigVarValue(&sig_struct, transducer, v_ac    , TRANSDUCER_MEAS_SIGNAL));

        // Run CCLIBS real-time activity for this iteration

        refRtRegulationRT(&ref_mgr, iter_time);

        refRtStateRT(&ref_mgr);

        // Run the ILC state machine - this is normally run in the background for to
        // make tests consistent, in ccrt it must be run from the real-time thread.

        refIlcState(&ref_mgr);

        // Count state transitions

        ccRunCountRefStateTransitions();

        // If the active event was updated then save the event

        if(refMgrVarValue(&ref_mgr, FLAG_FG_INTERNAL_EVT))
        {
            pthread_mutex_lock(&ccrun.cycle.mutex);

            ccrun.cycle.active_event = refMgrVarValue(&ref_mgr, EVENT_ACTIVE);

            pthread_mutex_unlock(&ccrun.cycle.mutex);
        }

        // Test latching of the unfiltered measurement used for regulation at 0.1s and 0.2s after the start of each function

        refMgrLatchUnfltrMeasRT(&ref_mgr, &ccpars_cycstatus[ref_mgr.fg.event.cyc_sel].latched_unfltr_meas1, 0, ref_mgr.fg.time_fp32 >= 0.1F);

        // For the second latch, use 2 instead of 1 to check that indexes > 1 are treated as 1

        refMgrLatchUnfltrMeasRT(&ref_mgr, &ccpars_cycstatus[ref_mgr.fg.event.cyc_sel].latched_unfltr_meas2, 2, ref_mgr.fg.time_fp32 >= 0.2F);

        // Store all relevant signals in logs

        ccRunLogSignals(iter_1s_boundary, iter_200ms_boundary);

        // Simulate PC state

        ccSimPcStateRT();

        // Use a simple first order filter to smooth changes in the raw i_mag_sat setting with ~1s time constant

        ccpars_sim.i_mag_sat.signal += (ccpars_sim.raw_i_mag_sat - ccpars_sim.i_mag_sat.signal) * reg_mgr.iter_period;

        // Check if command thread needs to be woken

        ccRunWakeCommandThread();

        // Wake up the event log thread at the specified rate

        if(++timer.iter_counter >= timer.event_log_period_iters)
        {
            timer.iter_counter = 0;

            // Wake the event log thread only once ccrt initialization is complete

            if(ccRtStateIsInitialised())
            {
                // Get the time of the current iteration for the event log thread

                ccrun.evtlog_time = ccrun.iter_us_time;

                // Manage polarity switch - this would really be done in a background thread, but it must be executed in
                // the real-time thread so that the test script results are consistent from run to run.

                ccRunPolswitch();

                // Try to start the event log thread

                ccThreadTryWake(&cc_threads.event_log);
            }
        }

        if(iter_200ms_boundary)
        {
            // Register "measurements" of simulated temperatures

            sigTempMeas(&sig_struct.temp_filter.named.internal, ccpars_sim.internal_temp);
            sigTempMeas(&sig_struct.temp_filter.named.dcct_a  , ccpars_sim.dcct_a_temp  );
            sigTempMeas(&sig_struct.temp_filter.named.dcct_b  , ccpars_sim.dcct_b_temp  );

            // Background processing in libsig to calculate calibration factors for the following 200 ms

            sigMgr(&sig_struct.mgr, iter_1s_boundary);

            // Prepare to log the TEMP signals

            ccrun.iter_200ms_timestamp = iter_time;
            ccrun.log.temp_meas_ready = true;

            // Pass time boundary flags to the event log thread after completing the real-time processing.
            // On RT Linux, this wouldn't need to be at the end of the real-time processing, but on normal Linux,
            // the event log thread can preempt the real-time thread.
            // The flags are reset by the event log thread.
            // Note: iter_1s_boundary must be set first.

            ccrun.iter_1s_boundary    |= iter_1s_boundary;
            ccrun.iter_200ms_boundary |= iter_200ms_boundary;
        }
    }

    return NULL;
}



static void ccRunTestLogStateMachine(struct LOG_log const * const log)
{
    if(logIsContinuous(&log_mgr, log->index))
    {
        if((log_mgr.freeze_mask & log->bit_mask) != 0)
        {
            CC_ASSERT(log->cd.continuous.state == LOG_STOPPING || log->cd.continuous.state == LOG_FROZEN);
        }
        else
        {
            CC_ASSERT(log->cd.continuous.state == LOG_STARTING || log->cd.continuous.state == LOG_RUNNING);
        }
    }
}



static void ccRunLogInterpolateAcqDisc(struct CC_run_acq_disc * const acq_disc, cc_float value)
{
    cc_float const step               = (value - acq_disc->last_value) / ACQ_DISC_INTERPOLATION_STEPS;
    cc_float       interpolated_value = acq_disc->last_value;

    uint32_t    i;

    for(i = 0 ; i < ACQ_DISC_INTERPOLATION_STEPS ; i++)
    {
        acq_disc->interpolated[i] = interpolated_value;
        interpolated_value += step;
    }

    acq_disc->last_value = value;
}



static void ccRunLogDiscontinuousSignals(void)
{
    struct LOG_log * const acq_disc0_log = &log_structs.log[LOG_ACQ_DISC0];     // Non-cycling discontinuous log
    struct LOG_log * const acq_disc_log  = &log_structs.log[LOG_ACQ_DISC];      // Cycling discontinuous log

    // Run discontinuous log on each cycle from 100ms for 1000 samples

    ccRunLogInterpolateAcqDisc(&ccrun.acq_disc[0], sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_UNFILTERED));
    ccRunLogInterpolateAcqDisc(&ccrun.acq_disc[1], sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_MEAS_UNFILTERED));

    static int32_t adc_disc_samples;

    // At set time within each cycle

    if(ccrun.acq_disc_counter == 0 && logArePostmortemLogsFrozen(&log_mgr) == false)
    {
        // Start new time series for non-cyclic discontinuous log

        logStoreStartDiscontinuousRT(acq_disc0_log, ccpars_event.active_event.abs_time, ccrun.iter_time, 0);

        // Start new time series for the cyclic discontinuous log if the cycle selector is non-zero

        uint32_t const cyc_sel = ccpars_event.active_event.cyc_sel;

        if(cyc_sel > 0)
        {
            adc_disc_samples = acq_disc_log->priv->num_postmortem_samples;

            // Start logging in the discontinuous log with the event time as the time origin

            logStoreStartDiscontinuousRT(acq_disc_log, ccpars_event.active_event.abs_time, ccrun.iter_time,  cyc_sel);
        }
    }
    else
    {
        // While samples remain to be logged

        if(adc_disc_samples > 0)
        {
            // Store all interpolated samples for this iteration in the non-cyclic and cyclic discontinuous logs

            logStoreDiscontinuousRT(acq_disc0_log, ACQ_DISC_INTERPOLATION_STEPS);
            logStoreDiscontinuousRT(acq_disc_log,  ACQ_DISC_INTERPOLATION_STEPS);

            adc_disc_samples -= ACQ_DISC_INTERPOLATION_STEPS;

            // If all samples now logged then complete the logging for this cycle

            if(adc_disc_samples <= 0)
            {
                CC_ASSERT(adc_disc_samples == 0);

                int32_t const offset = regMgrParValue(&reg_mgr, ITER_PERIOD_NS) / ACQ_DISC_INTERPOLATION_STEPS;

                struct CC_ns_time const last_sample_time_stamp = cctimeNsAddOffsetRT(ccrun.iter_time, -offset);

                logStoreEndDiscontinuousRT(acq_disc0_log, last_sample_time_stamp);
                logStoreEndDiscontinuousRT(acq_disc_log,  last_sample_time_stamp);
            }
        }
    }

    ccrun.acq_disc_counter++;
}



static void ccRunLogContinuousSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary)
{
    // log_debug_sig can be assigned to anything of interest to assist in debugging

    log_debug_sig = (cc_float)ref_mgr.fsm.off.init_rst_timer;

    // Save these signals as floats for logging purposes
    // Scale function type to better match other debugging signals
    // Negate the values for improved readability when WAIT_CMDS signal is visible

    ccrun.log.debug.reg_mode        =  (cc_float)regMgrVarValue(&reg_mgr,REG_MODE )          / 3.0 - 6.0;                      // 0 ... 3  ->  -6.0  ... -5.0
    ccrun.log.debug.ref_state       =  (cc_float)refMgrVarValue(&ref_mgr,REF_STATE)          * 0.1;                            // 0 ... 14 ->   0.0  ...  1.4
    ccrun.log.debug.cyc_sel         =  (cc_float)refMgrVarValue(&ref_mgr,EVENT_CYC_SEL)      * 0.1;                            // 0 ... 32 ->   0.0  ...  3.2
    ccrun.log.debug.fg_type         = -(cc_float)refMgrVarValue(&ref_mgr,REF_FG_TYPE)        * 0.1;                            // 0 ... 13 ->   0.0  ... -1.3
    ccrun.log.debug.fg_status       = -(cc_float)refMgrVarValue(&ref_mgr,REF_FG_STATUS)      / 4.0 - 2.0;                      // 0 ... 3  ->  -2.0  ... -2.75
    ccrun.log.debug.ramp_mgr_status = -(cc_float)refMgrVarValue(&ref_mgr,REF_RAMP_MGR)       / 3.0 - 3.0;                      // 0 ... 2  ->  -3.0  ... -3.66
    ccrun.log.debug.cycle_status    = -(cc_float)refMgrVarValue(&ref_mgr,REF_CYCLING_STATUS) / 3.0 - 4.0;                      // 0 ... 2  ->  -4.0  ... -4.66
    ccrun.log.debug.dcct_user_sel   = -(cc_float)sigVarValue(&sig_struct, select, i_meas, SELECT_SELECTOR)        / 3.0 - 3.0; // 0 ... 2  ->  -3.0  ... -3.66
    ccrun.log.debug.dcct_act_sel    = -(cc_float)sigVarValue(&sig_struct, select, i_meas, SELECT_ACTUAL_SELECTOR) / 3.0 - 4.0; // 0 ... 3  ->  -4.0  ... -5.00
    ccrun.log.debug.iter_time_ns    =  (cc_float)ccrun.iter_time.ns * 1.0E-9;
    ccrun.log.debug.iter_index_i    =  (cc_float)regMgrVarValue(&reg_mgr,IREG_ITER_INDEX);
    ccrun.log.debug.iter_index_b    =  (cc_float)regMgrVarValue(&reg_mgr,BREG_ITER_INDEX);

    // Use mutex to log and reset the event type atomically

    pthread_mutex_lock(&ccrun.log.event_type_mutex);

    ccrun.log.debug.event_type = (cc_float)ccrun.log.event_type;
    ccrun.log.event_type       = REF_EVENT_NONE;

    pthread_mutex_unlock(&ccrun.log.event_type_mutex);

    struct LOG_log * const log = log_structs.log;

    // If LOG DEBUG is active, then overwrite some digital signals with an identifiable pattern

#if LOGSTORE_DEBUG > 1
    if(iter_1s_boundary)
    {
        regMgrVarValue(&reg_mgr, FLAG_V_REG_ERR_FAULT)    = true;
        sigVarValue(&sig_struct, select, i_meas, SELECT_WARNINGS) |= SIG_SELECT_DIFF_WARN_BIT_MASK;
    }
    else
    {
        regMgrVarValue(&reg_mgr, FLAG_V_REG_ERR_FAULT)    = false;
        sigVarValue(&sig_struct, select, i_meas, SELECT_WARNINGS) &= ~SIG_SELECT_DIFF_WARN_BIT_MASK;
    }
    if(iter_200ms_boundary)
    {
        sigVarValue(&sig_struct, select, i_meas, SELECT_FAULTS) |= SIG_SELECT_DIFF_FLT_BIT_MASK;
    }
    else
    {
        sigVarValue(&sig_struct, select, i_meas, SELECT_FAULTS) &= ~SIG_SELECT_DIFF_FLT_BIT_MASK;
    }
    if(ccrun.iteration_counter & 32)
    {
        regMgrVarValue(&reg_mgr, FLAG_I_REF_ERR_FAULT)    = true;
        regMgrVarValue(&reg_mgr, FLAG_I_REG_ERR_WARNING)  = true;
    }
    else
    {
        regMgrVarValue(&reg_mgr, FLAG_I_REF_ERR_FAULT)    = false;
        regMgrVarValue(&reg_mgr, FLAG_I_REG_ERR_WARNING)  = false;
    }
#endif

    // Store the following logs on every iteration

    logStoreContinuousRT(&log[LOG_ACQ],        ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_DEBUG],      ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_B_MEAS],     ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_I_MEAS],     ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_V_REF],      ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_V_REG],      ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_FAULTS],     ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_WARNINGS],   ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_FLAGS],      ccrun.iter_time);
    logStoreContinuousRT(&log[LOG_LOGRUNNING], ccrun.iter_time);

    ccRunTestLogStateMachine(&log[LOG_ACQ]    );
    ccRunTestLogStateMachine(&log[LOG_FLAGS]  );

    // Store ACQ_FLTR every 10th iteration

    if((ccrun.iteration_counter % 10) == 0)
    {
        logStoreContinuousRT(&log[LOG_ACQ_FLTR], ccrun.iter_time);
        ccRunTestLogStateMachine(&log[LOG_ACQ_FLTR]);
    }

    // Store field regulation signals in log at field regulation rate

    if(regMgrVarValue(&reg_mgr, BREG_ITER_INDEX) == 0)
    {
        // Log field regulation

        logStoreContinuousRT(&log[LOG_B_REG], ccrun.iter_time);

        if(reg_mgr.reg_mode == REG_FIELD)
        {
            log_mgr.period[LOG_ILC_FLAGS] = log_mgr.period[LOG_B_REG];

            logStoreContinuousRT(&log[LOG_ILC_FLAGS], ccrun.iter_time);
        }

        ccRunTestLogStateMachine(&log[LOG_B_REG]);
    }

    // Store current regulation signals in log at current regulation rate

    if(regMgrVarValue(&reg_mgr, IREG_ITER_INDEX) == 0)
    {
        // Log current regulation

        logStoreContinuousRT(&log[LOG_I_REG], ccrun.iter_time);

        if(reg_mgr.reg_mode == REG_CURRENT)
        {
            log_mgr.period[LOG_ILC_FLAGS] = log_mgr.period[LOG_I_REG];

            logStoreContinuousRT(&log[LOG_ILC_FLAGS], ccrun.iter_time);
        }

        ccRunTestLogStateMachine(&log[LOG_I_REG]);
    }

    // Store ILC cyclic RMS signals when requested by refIlcStateToCycling

    struct CC_ns_time const ilc_log_time_stamp = refMgrVarValue(&ref_mgr, ILC_LOG_TIME_STAMP);

    if(ilc_log_time_stamp.secs.abs != 0)
    {
        logStoreContinuousRT(&log[LOG_ILC_CYC], ilc_log_time_stamp);

        refMgrVarValue(&ref_mgr, ILC_LOG_TIME_STAMP) = cc_zero_ns;
    }

    // Store temperatures when the measurements get ready in the event log thread (~200ms intervals)
    // The time stamp should match the 200ms time stamp.

    if(ccrun.log.temp_meas_ready)
    {
        logStoreContinuousRT(&log[LOG_TEMP], ccrun.iter_200ms_timestamp);

        ccRunTestLogStateMachine(&log[LOG_TEMP]);

        ccrun.log.temp_meas_ready = false;

        // Store the limits in the limits log at 5Hz

        logStoreContinuousRT(&log[LOG_LIMITS], ccrun.iter_200ms_timestamp);

        ccRunTestLogStateMachine(&log[LOG_LIMITS]);

        // Store the V_AC frequency measurement at 5Hz

        logStoreContinuousRT(&log[LOG_V_AC_HZ], ccrun.iter_200ms_timestamp);

        ccRunTestLogStateMachine(&log[LOG_V_AC_HZ]);
    }
}



static void ccRunLogSimSignal(uint32_t   const index,
                              uint32_t   const time_offset_ms,
                              cc_float * const dim_ana)
{
    if(ccrun.iter_ms_time_mod_20 == time_offset_ms && (ccrun.dim.chan_recorded_mask & (1 << index)) == 0)
    {
        // On first iteration of the specified millisecond, sample the us time as the DIM signal

        *dim_ana = (cc_float)ccrun.iter_time.ns * 1.0E-9;

        // Mark that the sample has been taken for this 20ms period

        ccrun.dim.chan_recorded_mask |= (1 << index);
    }
}



static inline void ccRunLogShiftCodimSignals(void)
{
    // Shift all DIM channels up by one unit

    ccrun.dim.ana_a += 1.0;
    ccrun.dim.ana_b += 1.0;
    ccrun.dim.ana_c += 1.0;
    ccrun.dim.ana_d += 1.0;
}


static void ccRunLogDimSignals(void)
{
    // Sample signal (the us_time within the second) at different times for each analog DIM channel

    ccRunLogSimSignal(0, DIM_ANA_A_TIME_OFFSET_MS, &ccrun.dim.ana_a);
    ccRunLogSimSignal(1, DIM_ANA_B_TIME_OFFSET_MS, &ccrun.dim.ana_b);
    ccRunLogSimSignal(2, DIM_ANA_C_TIME_OFFSET_MS, &ccrun.dim.ana_c);
    ccRunLogSimSignal(3, DIM_ANA_D_TIME_OFFSET_MS, &ccrun.dim.ana_d);

    if(ccrun.iter_ms_time_mod_20 == 0 && ccrun.dim.chan_recorded_mask == 0)
    {
        // Set bit 7 to indicate that the 20 ms has started

        ccrun.dim.chan_recorded_mask = 0x80;

        // At start of each 20ms period

        if(ccrun.dim.dc_recording)
        {
            // Discontinuous DIM logging is active

            if(ccrun.dim.dc_counter >= ccrun.dim.dc_num_samples)
            {
                // Discontinuous DIM logs are full for this cycle so end recording

                logStoreEndDiscontinuousRT(&log_structs.log[LOG_DCDIM ], ccrun.dim.dim_sample_time);
                logStoreEndDiscontinuousRT(&log_structs.log[LOG_DCDIM2], ccrun.dim.dim_sample_time);
                logStoreEndDiscontinuousRT(&log_structs.log[LOG_DCDIM3], ccrun.dim.dim_sample_time);
                logStoreEndDiscontinuousRT(&log_structs.log[LOG_DCDIM4], ccrun.dim.dim_sample_time);

                ccrun.dim.dc_recording = false;
            }
        }
        else
        {
            // Discontinuous DIM logging is not active

            if(--ccrun.dim.dc_counter == 0)
            {
                logStoreStartDiscontinuousRT(&log_structs.log[LOG_DCDIM ], ccrun.iter_time, ccrun.iter_time, 0);
                logStoreStartDiscontinuousRT(&log_structs.log[LOG_DCDIM2], ccrun.iter_time, ccrun.iter_time, 0);
                logStoreStartDiscontinuousRT(&log_structs.log[LOG_DCDIM3], ccrun.iter_time, ccrun.iter_time, 0);
                logStoreStartDiscontinuousRT(&log_structs.log[LOG_DCDIM4], ccrun.iter_time, ccrun.iter_time, 0);

                ccrun.dim.dc_recording = true;
            }
        }

        // Latch timestamp to be used with the DIM samples

        ccrun.dim.dim_sample_time = ccrun.iter_time;
    }

    if(ccrun.iter_ms_time_mod_20 == 19 && ccrun.dim.chan_recorded_mask != 0)
    {
        // On millisecond 19 out of 20, log the DIM samples in the continuous DIM logs

        logStoreContinuousRT(&log_structs.log[LOG_CODIM ], ccrun.dim.dim_sample_time);
        ccRunLogShiftCodimSignals();
        logStoreContinuousRT(&log_structs.log[LOG_CODIM2], ccrun.dim.dim_sample_time);
        ccRunLogShiftCodimSignals();
        logStoreContinuousRT(&log_structs.log[LOG_CODIM3], ccrun.dim.dim_sample_time);

        if(ccrun.dim.dc_recording)
        {
            // Discontinuous DIM log recording is active so log samples in discontinuous DIM logs

            ccRunLogShiftCodimSignals();
            logStoreDiscontinuousRT(&log_structs.log[LOG_DCDIM ], 1);
            ccRunLogShiftCodimSignals();
            logStoreDiscontinuousRT(&log_structs.log[LOG_DCDIM2], 1);
            ccRunLogShiftCodimSignals();
            logStoreDiscontinuousRT(&log_structs.log[LOG_DCDIM3], 1);
            ccRunLogShiftCodimSignals();
            logStoreDiscontinuousRT(&log_structs.log[LOG_DCDIM4], 1);

            ccrun.dim.dc_counter++;
        }

        // Clear the channels recorded mask in preparation for the next 20ms period

        ccrun.dim.chan_recorded_mask = 0;
    }
}



static void ccRunLogSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary)
{
    // Freeze logs when converter stops if postmortem generation is enabled

    static bool pc_was_on;

    bool pc_is_on = ccSimIsPcOn();

    if(ccpars_log.pm_buf == CC_ENABLED && pc_is_on == false && pc_was_on == true)
    {
        logFreezePostmortemLogs(&log_mgr);
    }

    pc_was_on = pc_is_on;

    // Check if new cycle has started (iter time is >= start_cycle_time) - order of conditions matters!

    if(ccrun.cycle_running == false              &&
       ccsim.supercycle.active.event.cyc_sel > 0 &&
       cctimeNsCmpAbsTimesRT(ccrun.iter_time, ccsim.supercycle.active.event_time) >= 0)
    {
        ccrun.cycle_running    = true;
        ccrun.acq_disc_counter = -100;  // iterations till start of discontinuous logging

        // Inform liblog about new cycle

        logStoreStartContinuousRT(log_read.log, ccrun.iter_time, ccsim.supercycle.active.event.cyc_sel);
    }

    // Log continuous and discontinuous logs

    ccRunLogContinuousSignals(iter_1s_boundary, iter_200ms_boundary);
    ccRunLogDiscontinuousSignals();
    ccRunLogDimSignals();

    // Write PMD file if needed

    if(logPostmortemLogsFroze(&log_mgr))
    {
        ccLogWakeThread(LOG_READ_GET_PM_BUF, NULL);
    }
}



static void ccRunWakeCommandThread(void)
{
    pthread_mutex_lock(&ccrun.wait.condition_mutex);

    bool test_result;

    // If wait function time is set, and ...

    if(   (    ccrun.wait.func_time.secs.rel != wait_func_time_not_set.secs.rel

        // function has ended or func_time has arrived, or ...

           && (   refMgrVarValue(&ref_mgr, REF_FG_STATUS) == FG_POST_FUNC
               || cctimeNsCmpRelTimesRT(ref_mgr.fg.time, ccrun.wait.func_time) >= 0))

        // wait time is set, and...

       || (   cctimeNsCmpRelTimesRT(ccrun.wait.wait_time, cc_zero_ns) != 0

        // wait time has arrived, or ...

           && (   cctimeNsCmpAbsTimesRT(ccrun.iter_time, ccrun.wait.wait_time) >= 0

        // a parameter test is set and the test returns an error or is true

               || (   (ccrun.wait.test.condition != '\0')
                   && (   ccpars_test_func[ccrun.wait.test.par->type](&ccrun.wait.test, &test_result) == EXIT_FAILURE
                       || test_result == true)))))
    {
        // Save the iteration time and counter on which the command thread was woken up
        // The iter time mutex doesn't have to be locked, because we are in the same thread
        // that sets the value of iter_time

        ccrun.wait.last_wakeup_iter_time = ccrun.iter_time;
        ccrun.wait.last_wakeup_iteration_counter = ccrun.iteration_counter;

        // Cancel the wake requests

        ccrun.wait.func_time = wait_func_time_not_set;
        ccrun.wait.wait_time = cc_zero_ns;
        ccrun.wait.cmd       = CMD_WAIT_NONE;

        // If a sync command was supplied then process the command and return the exit status

        if(ccrun.wait.sync_command != NULL)
        {
            ccrun.wait.sync_command_exit_status = ccParseLine(ccrun.wait.sync_command, NULL);

            ccrun.wait.sync_command = NULL;

            // Check libreg parameters for changes and process them immediately to ensure reproducibility

            regMgrParsCheck(&reg_mgr);
        }

        // Wake up command thread using condition signal and by canceling the wait active flag

        ccrun.wait.active = false;

        pthread_cond_signal(&ccrun.wait.condition_cond);
    }

    // Report waiting command in DEBUG WAIT_CMD signal

    ccrun.wait.log_value = (cc_float)ccrun.wait.cmd;

    pthread_mutex_unlock(&ccrun.wait.condition_mutex);
}



static void ccRunRealTimeDeltaRef(void)
{
    // Adjust the sine wave argument (radians) to remain within the range from -PI to +PI

    ccrun.real_time.w += 2.0 * M_PI * ccpars_rtref.frequency * reg_mgr.iter_period_fp32;

    if(ccrun.real_time.w > M_PI)
    {
        ccrun.real_time.w -= (2.0 * M_PI);
    }

    // Create real-time reference from a sine wave sampled at specified sampling period

    if(++ccrun.real_time.iter_counter >= ccpars_rtref.sampling_iters)
    {
        ccrun.real_time.iter_counter = 0;

        refMgrParValue(&ref_mgr,RT_REF) = sin(ccrun.real_time.w) * ccpars_rtref.amplitude + ccpars_rtref.offset;
    }

}



static void ccRunPrepareMeas(void)
{
    static const uint32_t transducer_index[] =
    {
        SIG_NOT_IN_USE,            // ADC_SIGNAL_CAL_ZERO
        SIG_NOT_IN_USE,            // ADC_SIGNAL_CAL_POS
        SIG_NOT_IN_USE,            // ADC_SIGNAL_CAL_NEG
        SIG_NOT_IN_USE,            // ADC_SIGNAL_DAC
        SIG_TRANSDUCER_DCCT_A,     // ADC_SIGNAL_I_DCCT_A
        SIG_TRANSDUCER_DCCT_B,     // ADC_SIGNAL_I_DCCT_B
        SIG_TRANSDUCER_V_PROBE,    // ADC_SIGNAL_V_MEAS
        SIG_TRANSDUCER_B_PROBE,    // ADC_SIGNAL_B_PROBE_A
        SIG_TRANSDUCER_I_PROBE,    // ADC_SIGNAL_I_CAPA
        SIG_TRANSDUCER_V_AC,       // ADC_SIGNAL_V_AC
        SIG_NOT_IN_USE,            // ADC_SIGNAL_NONE
    };

    // Evaluate the boolean early to ensure that the behaviour is the same for all ADC channels

    bool const is_meas_invalid = (rand() < (long)(RAND_MAX * ccpars_sim.invalid_probability));

    // Process all ADCs

    for(uint32_t adc_idx = 0; adc_idx < SIG_NUM_ADCS; adc_idx++)
    {
        // Set simulated raw_adc value on the ADC

        sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_RAW) = ccsim.circuit.raw_adc[adc_idx];

        // Pair transducers with ADCs based on SIM ADC_SOURCE parameter

        sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_TRANSDUCER_INDEX) = transducer_index[ccpars_sim.adc_signal[adc_idx]];

        // Invalidate the measurement by setting the flag when 'invalid measurement event' occurs

        sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_FAULT_FLAG) = is_meas_invalid;
    }
}

// EOF
