//! @file  ccrt/src/ccPars.c
//!
//! @brief ccrt parameter processing functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>
#include <float.h>
#include <errno.h>
#include <pthread.h>
#include <limits.h>

// Include ccrt program header files

#include "ccCmds.h"
#include "ccRt.h"
#include "ccParse.h"
#include "ccFile.h"
#include "ccRun.h"

// Stuctures

struct CC_range
{
    uint32_t from;
    uint32_t to;
};



static inline uint32_t ccParsSubSel(struct CC_pars const * const par)
{
    if(ccParsIsSubSel(par) == true && ccpars_global.sub_sel < CC_NUM_SUB_SELS)
    {
        return ccpars_global.sub_sel;
    }

    return 0;
}



static inline uint8_t * ccParsGetCycSelOffset(struct CC_pars const * const par, uint32_t const cyc_sel)
{
    return par->value_p.c + ccParsSubSel(par) * par->sub_sel_step + cyc_sel * par->cyc_sel_step;
}



static inline uintptr_t * ccParsNumElements(struct CC_pars const * const par, uint32_t const cyc_sel)
{
    return par->num_elements + ccParsSubSel(par) * CC_NUM_CYC_SELS + cyc_sel;
}



static uint32_t ccParsGetCycSelRange(struct CC_pars const * const par,
                                     struct CC_range      * const cyc_sel_range,
                                     uint32_t                     cyc_sel)
{
    // If reading from file and a read_cyc_sel is provided, replace unspecified cyc_sels with read_cyc_sel

    if(ccParsIsCycSel(par) && cyc_sel == CC_NO_INDEX && ccfile.read_cyc_sel != CC_NO_INDEX)
    {
        cyc_sel = ccfile.read_cyc_sel;
    }

    // Process cyc_sel to work out [from:to] range

    if(ccParsIsCycSel(par))
    {
        if(cyc_sel == CC_ALL_CYC_SELS)
        {
            cyc_sel_range->from = 0;
            cyc_sel_range->to   = CC_MAX_CYC_SEL;
        }
        else if(cyc_sel == CC_NO_INDEX)
        {
            cyc_sel_range->from = cyc_sel_range->to = 0;
        }
        else
        {
            cyc_sel_range->from = cyc_sel_range->to = cyc_sel;
        }
    }
    else
    {
        if(cyc_sel == CC_NO_INDEX || cyc_sel == CC_ALL_CYC_SELS || cyc_sel == 0)
        {
            cyc_sel_range->from = cyc_sel_range->to = 0;
        }
        else // return immediately if non-cyc_sel parameters when non-zero cyc_sel supplied
        {
            ccRtPrintError("Invalid cycle selector %s: %d", par->full_name, ccfile.cyc_sel);
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}



static uint32_t ccParsGetArrayIndex(struct CC_pars const * const par, uint32_t * const array_idx)
{
    // Prepare array index

    *array_idx = (ccfile.array_idx == CC_NO_INDEX) ? 0 : ccfile.array_idx;

    if(*array_idx >= par->max_num_elements)
    {
        ccRtPrintError("Array index out of bounds %s: %d > %d", par->full_name, *array_idx, par->max_num_elements);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



static bool ccParsGetCycSelInc(struct CC_pars const * const par)
{
    // When the parameter is scalar then iterate through cycle selectors, otherwise iterate through array elements

    return (   ccParsIsCycSel(par)
            && ccParsIsScalar(par)
            && (ccfile.cyc_sel != CC_NO_INDEX)
            && (ccfile.cyc_sel != CC_ALL_CYC_SELS) );
}



static uint32_t ccParsGetMaxPars(struct CC_pars const * const par,
                                 uint32_t               const cyc_sel_from,
                                 uint32_t               const array_idx)
{
    uint32_t max_pars;

    if(   ccParsIsCycSel(par)
       && ccParsIsScalar(par)
       && (ccfile.cyc_sel != CC_NO_INDEX)
       && (ccfile.cyc_sel != CC_ALL_CYC_SELS) )
    {
        max_pars = CC_NUM_CYC_SELS - cyc_sel_from;
    }
    else
    {
        max_pars = par->max_num_elements - array_idx;

    }

    return max_pars;
}



void ccParsFullName(struct CC_pars * const par, char * const buf, size_t const buflen)
{
    int num_characters = snprintf(buf, buflen, "%s", par->full_name);

    if(   ccfile.cyc_sel >  0
       && ccfile.cyc_sel != CC_NO_INDEX
       && ccfile.cyc_sel != CC_ALL_CYC_SELS)
    {
        num_characters += snprintf(buf + num_characters, buflen - num_characters, "(%u)", ccfile.cyc_sel);
    }

    if(ccfile.array_idx != CC_NO_INDEX)
    {
        snprintf(buf + num_characters, buflen - num_characters,  "[%u]", ccfile.array_idx);
    }
}



uint32_t ccParsLong(char const * const arg, long * const long_value, int const base)
{
    char * remaining_arg;

    errno = 0;

    long const value = strtol(arg, &remaining_arg, base);

    if(remaining_arg == arg || (errno != 0 && value == 0))
    {
        ccRtPrintError("String to long conversion error - ill-formed long argument (%s)", arg);
        return EXIT_FAILURE;
    }
    else if(errno == ERANGE)
    {
        if(value == LONG_MAX)
        {
            ccRtPrintError("String to long conversion would cause overflow");
        }
        else
        {
            ccRtPrintError("String to long conversion would cause underflow");
        }

        return EXIT_FAILURE;
    }

    *long_value = value;

    return EXIT_SUCCESS;
}



uint32_t ccParsAbsTime(char const * const arg, struct CC_ns_time * const time_value)
{
    char * remaining_arg;

    errno = 0;

    unsigned long const value = strtoul(arg, &remaining_arg, 10);

    if(remaining_arg == arg || (errno != 0 && value == 0))
    {
        ccRtPrintError("String to abstime integer part conversion error (%s)", arg);
        return EXIT_FAILURE;
    }
    else if(errno == ERANGE)
    {
        ccRtPrintError("String to abstime conversion would cause overflow");

        return EXIT_FAILURE;
    }

    int32_t time_ns = 0;

    if(*remaining_arg == '.')
    {
        int c;
        int32_t ns = 100000000;

        while(isdigit(c = *(++remaining_arg)) && (ns > 0 || c == '0'))
        {
            time_ns += (c - '0') * ns;
            ns /= 10;
        }
    }

    if(*remaining_arg != '\0' && *remaining_arg != ' ')
    {
        ccRtPrintError("String to abstime fractional part conversion error (%s)", arg);
        return EXIT_FAILURE;
    }

    time_value->secs.abs = (uint32_t)value;
    time_value->ns = time_ns;

    return EXIT_SUCCESS;
}



uint32_t ccParsRelTime(char const * const arg, struct CC_ns_time * const time_value)
{
    char * remaining_arg;

    errno = 0;

    long const value = strtol(arg, &remaining_arg, 10);

    if(remaining_arg == arg || (errno != 0 && value == 0))
    {
        ccRtPrintError("String to reltime integer part conversion error (%s)", arg);
        return EXIT_FAILURE;
    }
    else if(errno == ERANGE)
    {
        ccRtPrintError("String to reltime conversion would cause overflow");

        return EXIT_FAILURE;
    }

    int32_t time_ns = 0;

    if(*remaining_arg == '.')
    {
        int c;
        int32_t ns = 100000000;

        while(isdigit(c = *(++remaining_arg)) && (ns > 0 || c == '0'))
        {
            time_ns += (c - '0') * ns;
            ns /= 10;
        }
    }

    if(*remaining_arg != '\0' && *remaining_arg != ' ')
    {
        ccRtPrintError("String to reltime fractional part conversion error (%s)", arg);
        return EXIT_FAILURE;
    }

    time_value->secs.rel = (int32_t)value;
    time_value->ns = value >= 0
                   ? time_ns
                   : CCTIME_NS_PER_S - time_ns;

    return EXIT_SUCCESS;
}



static uint32_t ccParsDoubleInternal(char const * const arg, double * const double_value, bool const try)
{
    char * remaining_arg;

    // Try to convert argument to double value

    errno = 0;

    cc_double const value = strtod(arg, &remaining_arg);

    if(*remaining_arg != '\0' && *remaining_arg != ' ')
    {
        if(try == false)
        {
            ccRtPrintError("String to double conversion error - ill-formed double argument ( %s )", arg);
        }

        return EXIT_FAILURE;
    }
    else if(errno == ERANGE)
    {
        if(value == HUGE_VAL || value == -HUGE_VAL)
        {
            ccRtPrintError("String to double conversion would cause %s overflow", (value == HUGE_VAL ? "positive" : "negative"));
        }
        else
        {
            ccRtPrintError("String to double conversion would cause underflow");
        }

        return EXIT_FAILURE;
    }
    else if(value >= FLT_MAX || value <= -FLT_MAX)
    {
        ccRtPrintError("Double value won't fit in a float");
        return EXIT_FAILURE;
    }

    *double_value = value;

    return EXIT_SUCCESS;
}



uint32_t ccParsTryDouble(char const * const arg, double * const double_value)
{
    return ccParsDoubleInternal(arg, double_value, true);
}



uint32_t ccParsDouble(char const * const arg, double * const double_value)
{
    return ccParsDoubleInternal(arg, double_value, false);
}



uint32_t ccParsSet(uint32_t const cmd_idx, struct CC_pars * const par, char *remaining_line)
// This function will try to interpret arguments remaining on the input line as values belonging to
// a command parameter. It supports different interpretations of multiple arguments, e.g.:
//
// REF FUNCTION() SINE                   Sets REF FUNCTION(1-16)
// REF FUNCTION(5) PLEP TABLE PPPL       Sets REF FUNCTION(5) (6) and (7)
// TABLE REF(5) 1 2 3 4                  Sets TABLE REF(5)
//
// If a cycle selector is supplied and the parameter is scalar then multiple values will apply to
// consecutive cycle selectors.

{
    // In verbose mode print out all set operations

    ccRtPrintInVerbose("%s %s", par->full_name, remaining_line);

    uint32_t array_idx;

    if(ccParsGetArrayIndex(par, &array_idx) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    struct CC_range cyc_sel_range;

    if(ccParsGetCycSelRange(par, &cyc_sel_range, ccfile.cyc_sel) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    uint32_t       max_pars     = ccParsGetMaxPars  (par, cyc_sel_range.from, array_idx);
    bool     const cyc_sel_inc  = ccParsGetCycSelInc(par);
    uint32_t const cyc_sel_from = cyc_sel_range.from;

    // Reset errno because strtod does not set it to zero on success

    errno = 0;

    // Try to parse the arguments to set the parameter values

    uint32_t   num_pars = 0;
    char     * arg;

    while((arg = ccParseNextArg(&remaining_line)) != NULL)
    {
        union CC_value_p   value_p;
        char             * remaining_arg;


        // Check for null parameter (-) to allow an array to be empty

        if(num_pars == 0 && strcmp(arg, "-") == 0)
        {
            break;
        }

        // Check for too many parameters

        if(num_pars >= max_pars)
        {
            ccRtPrintError("Too many values for %s (%u max)", par->full_name, max_pars);
            return EXIT_FAILURE;
        }

        // Convert the parameter from ASCII according to the type of parameter

        switch(par->type)
        {
            // Unimplemented for set

        case PAR_USHORT:
        case PAR_POINTER:

            ccRtPrintError("Set not implemented for this type of parameter: %s", par->full_name);
            return EXIT_FAILURE;

            break;

        case PAR_BOOL:
        {
            bool const bool_value = strcmp(arg, "TRUE") == 0;

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c            = ccParsGetCycSelOffset(par, cyc_sel);
                value_p.b[array_idx] = bool_value;
            }

            break;
        }

        case PAR_SIGNED:
        {
            int32_t const int_value = strtol(arg, &remaining_arg, 0);

            if(*remaining_arg != '\0' || errno != 0)
            {
                ccRtPrintError("Invalid signed integer for %s[%d]: '%s'", par->full_name, array_idx, arg);
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c            = ccParsGetCycSelOffset(par, cyc_sel);
                value_p.i[array_idx] = int_value;
            }

            break;
        }

        case PAR_UNSIGNED:
        case PAR_HEX:
        {
            uint32_t const uint_value = strtoul(arg, &remaining_arg, 0);

            if(*remaining_arg != '\0' || errno != 0)
            {
                ccRtPrintError("Invalid integer for %s[%u]: '%s'", par->full_name, array_idx, arg);
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c            = ccParsGetCycSelOffset(par, cyc_sel);
                value_p.u[array_idx] = uint_value;
            }

            break;
        }

        case PAR_FLOAT:
        case PAR_DOUBLE:
        {
            double double_value;

            if(ccParsDouble(arg, &double_value) == EXIT_FAILURE)
            {
                ccRtPrintError("Invalid float for %s[%u]: '%s'", par->full_name, array_idx, arg);
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c = ccParsGetCycSelOffset(par, cyc_sel);

                if(par->type == PAR_FLOAT)
                {
                    value_p.f[array_idx] = (float)double_value;
                }
                else
                {
                    value_p.d[array_idx] = double_value;
                }
            }

            break;
        }

        case PAR_ABS_TIME:
        {
            struct CC_ns_time time_value;

            if(arg[0] == 'T' && arg[1] == '\0')
            {
                time_value = ccrun.iter_time;
            }
            else if(ccParsAbsTime(arg, &time_value) == EXIT_FAILURE)
            {
                ccRtPrintError("Invalid abs_time for %s[%u]: '%s'", par->full_name, array_idx, arg);
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c = ccParsGetCycSelOffset(par, cyc_sel);

                value_p.n[array_idx] = time_value;
            }

            break;
        }

        case PAR_POINT:
        {
            cc_float time_value;
            cc_float ref_value;
            char     trailing_char;

            if(sscanf(arg, "%f|%f%c", &time_value, &ref_value, &trailing_char) != 2)
            {
                ccRtPrintError("Invalid point for %s[%u]: '%s'", par->full_name, array_idx, arg);
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c = ccParsGetCycSelOffset(par, cyc_sel);

                value_p.p[array_idx].time = (float)time_value;
                value_p.p[array_idx].ref  = (float)ref_value;
            }

            break;
        }

        case PAR_STRING:
        {
            size_t const arg_len = strlen(arg);

            // Limit string lengths because they are used in path names

            if(arg_len > 32)
            {
                ccRtPrintError("Invalid string length (%u) for %s[%u]: '%s' (32 max)",
                            arg_len, par->full_name, array_idx, ccParseAbbreviateArg(arg));
                return EXIT_FAILURE;
            }

            // Free and reallocate space for string argument
            // On first call, the pointer is NULL which is ignored by free()

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c = ccParsGetCycSelOffset(par, cyc_sel);
                free(value_p.s[array_idx]);
                value_p.s[array_idx] = strcpy(malloc(arg_len+1),arg);
            }

            break;
        }

        case PAR_ENUM:
        {
            int32_t const enum_idx = ccParsEnumStringToIndex(par, arg, false);

            if(enum_idx < 0)
            {
                return EXIT_FAILURE;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c            = ccParsGetCycSelOffset(par, cyc_sel);
                value_p.u[array_idx] = par->ccpars_enum[enum_idx].value;
            }

            break;
        }

        case PAR_BITMASK:
        {
            uint32_t uint_value;

            if(arg[0] == '0' || par->max_num_elements > 1)
            {
                // Numeric value or parameter is an array - only numeric entry allowed

                uint_value = strtoul(arg, &remaining_arg, 0);

                if(*remaining_arg != '\0' || errno != 0)
                {
                    ccRtPrintError("Invalid integer for %s[%u]: '%s'", par->full_name, array_idx, arg);
                    return EXIT_FAILURE;
                }
            }
            else
            {
                // Scalar value and non-numeric value provided

                int32_t const enum_idx = ccParsEnumStringToIndex(par, arg, false);

                if(enum_idx == -1)
                {
                    return EXIT_FAILURE;
                }

                // Set value_p to avoid compiler warning about the use of value_p.u[0] uninitialized.
                //
                // The value is actually only used on a second iteration with num_pars > 0, when it would
                // have been initialized in the for loop below.


                value_p.c  = ccParsGetCycSelOffset(par, cyc_sel_range.from);

                uint_value = (1 << par->ccpars_enum[enum_idx].value) | (num_pars > 0 ? value_p.u[0] : 0);

                array_idx = 0;
                max_pars  = 32;
            }

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                value_p.c            = ccParsGetCycSelOffset(par, cyc_sel);
                value_p.u[array_idx] = uint_value;
            }

            break;
        }

        default:

            // This should never happen

            assert(false);
            break;
        }

        if(cyc_sel_inc == false)
        {
            array_idx++;

            // Update num_elements if the array length is growing with each new argument

            for(uint32_t cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
            {
                uintptr_t * const num_elements_prt = ccParsNumElements(par, cyc_sel);

                if(array_idx > *num_elements_prt)
                {
                    *num_elements_prt = array_idx;
                }
            }
        }
        else
        {
            cyc_sel_range.from++;
            cyc_sel_range.to++;
        }

        num_pars++;
    }

    // Set cyc_sel range according to the number of parameters

    uint32_t cyc_sel_to = cyc_sel_from + (cyc_sel_inc == true ? num_pars - 1 : 0);

    // If parameter length is NOT fixed and not scalar

    if(ccParsIsFixedLength(par) == false && ccParsIsScalar(par) == false)
    {
        for(uint32_t cyc_sel = cyc_sel_from ; cyc_sel <= cyc_sel_to ; cyc_sel++)
        {
            uintptr_t * const num_elements_prt = ccParsNumElements(par, cyc_sel);

            // If array has shrunk then zero the trailing values and update num_elements

            if(   array_idx < *num_elements_prt
               && (   ccfile.array_idx == CC_NO_INDEX
                   || array_idx > (ccfile.array_idx + 1)))
            {
                union CC_value_p value_p;

                value_p.c = ccParsGetCycSelOffset(par, cyc_sel);

                memset(&value_p.c[ccParsGetTypeSize(par->type) * array_idx],
                       0,
                       ccParsGetTypeSize(par->type) * (*num_elements_prt - array_idx));

                *num_elements_prt = array_idx;
            }
        }
    }

    // Save libref FG parameter index for transactional parameters

    if((par->flags & PARS_TRANS) != 0)
    {
        for(uint32_t fg_par_idx = 0 ; fg_par_idx < REF_NUM_FG_PARS ; fg_par_idx++)
        {
            if(par == ccrun.fg_pars_link[fg_par_idx])
            {
                for(uint32_t cyc_sel = cyc_sel_from ; cyc_sel <= cyc_sel_to ; cyc_sel++)
                {
                    refMgrFgParValueSubCyc(&ref_mgr, ccpars_global.sub_sel, cyc_sel, TRANSACTION_LAST_FG_PAR_INDEX) = fg_par_idx;
                }
            }
        }
    }

    // Wait for state machine to process a parameter change before continuing

    ccRtWaitForRefStateMachine();

    // If parameter is used by libreg then inform libreg that a parameter has been changed

    if(cmd_idx > 0 && ccParsIsLibreg(par) && ccRtStateIsInitialised())
    {
        regMgrParsCheck(&reg_mgr);
    }

    // If parameter is a LOGMPX then cache log signal pointers

    if(ccParsIsLogMpx(par))
    {
        logCacheSelectors(&log_structs);
    }

    // If running interactively and a configuration parameter is changed then store it in a file

    if(ccRtStateIsInitialised()    &&
       ccRtModeIsBatch() == false  &&
       ccParsIsConfig(par))
    {
        return ccFileConfigPar(par);
    }

    return EXIT_SUCCESS;
}



int32_t ccParsEnumValueToIndex(struct CC_pars_enum const * const par_enum, uint32_t value)
{
    int32_t idx;

    // Search enum for a matching value

    for(idx = 0 ; par_enum[idx].string != NULL && par_enum[idx].value != value; idx++);

    // Return -1 if no match found

    return par_enum[idx].string != NULL ? idx : CC_UNKNOWN_ENUM;
}



const char * ccParsEnumValueToString(struct CC_pars_enum const * par_enum, uint32_t const value)
{
    // Search enum for a matching value

    while(par_enum->string != NULL && par_enum->value != value)
    {
        par_enum++;
    }

    // Return "invalid" if no match found

    return par_enum->string != NULL ? par_enum->string : "invalid";
}



int32_t ccParsEnumStringToIndex(struct CC_pars const * const par, char * const string, bool const test_flag)
{
    int32_t      index         = 0;
    int32_t      matched_index = CC_UNKNOWN_ENUM;
    size_t const string_length = strlen(string);

    struct CC_pars_enum const * const par_enum = par->ccpars_enum;

    while(par_enum[index].string != NULL)
    {
        // If argument matches start or all of an enum string

        if(strncasecmp(par_enum[index].string, string, string_length) == 0)
        {
            // If match is exact then break out of search

            if(strlen(par_enum[index].string) == string_length)
            {
                matched_index = index;
                break;
            }

            // If first match, remember enum

            if(matched_index == CC_UNKNOWN_ENUM)
            {
                matched_index = index;
            }
            else // else second match so report error
            {
                matched_index = CC_AMBIGUOUS_ENUM;
            }
        }

        index++;
    }

    if(matched_index == CC_AMBIGUOUS_ENUM)
    {
        ccRtPrintError("Ambiguous enum for %s: '%s'", par->full_name, string);
        return -1;
    }

    if(matched_index == CC_UNKNOWN_ENUM)
    {
        ccRtPrintError("Unknown enum for %s: '%s'", par->full_name, ccParseAbbreviateArg(string));
        return -1;
    }

    if(test_flag == false && (par->ccpars_enum[matched_index].flags & CC_ENUM_READ_ONLY) != 0)
    {
        ccRtPrintError("Read-only enum for %s: '%s'", par->full_name, string);
        return -1;
    }

    return matched_index;
}



void ccParsPrintAbsTime(FILE * const f, struct CC_ns_time const time, uint32_t const ns_resolution)
{
    if(time.secs.abs > 0)
    {
        struct tm tm;

        time_t const unix_time = (time_t)time.secs.abs;
        uint32_t const ns = ns_resolution == 3
                          ? (uint32_t)time.ns / 1000000
                          : (uint32_t)time.ns;

        localtime_r(&unix_time, &tm);

        fprintf(f, PARS_TIMEVAL_FORMAT,
                1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday,
                tm.tm_hour, tm.tm_min, tm.tm_sec,
                ns_resolution, ns);
    }
    else
    {
        fputs("Not set                ", f);
    }
}



void ccParsPrintBitmask(FILE * const file, uint32_t const bitmask, struct CC_pars_enum const * par_enum)
{
    // For a bit mask par_enum, the value contains the bit index, not the bit mask

    while(par_enum->string != NULL)
    {
        if((bitmask & (1 << par_enum->value)) != 0)
        {
            fprintf(file, "  %s", par_enum->string);
        }

        par_enum++;
    }
}



void ccParsPrintElement(FILE                 * const f,
                        struct CC_pars const * const par,
                        uint32_t                     cyc_sel,
                        uint32_t                     array_idx)
{
    if(array_idx < *ccParsNumElements(par, cyc_sel))
    {
        uint8_t * byte_p;
        char strbuf[22];

        refMgrVarPointerFunc(&ref_mgr, par->value_p.c, ccParsSubSel(par), cyc_sel, par->sub_sel_step, par->cyc_sel_step, &byte_p);

        union CC_value_p const value_p = { .c = byte_p + array_idx * par->array_step };

        switch(par->type)
        {
        case PAR_SIGNED:

            fprintf(f, PARS_INT_FORMAT, *value_p.i);
            break;

        case PAR_UNSIGNED:

            fprintf(f, PARS_UINT_FORMAT, *value_p.u);
            break;

        case PAR_UINTPTR:

            fprintf(f, PARS_LONGUINT_FORMAT, *value_p.r);
            break;

        case PAR_USHORT:

            fprintf(f, PARS_USHORT_FORMAT, *value_p.h);
            break;

        case PAR_HEX:

            fprintf(f, PARS_HEX_FORMAT, *value_p.u);
            break;

        case PAR_FLOAT:

            fprintf(f, (par->flags & PARS_RO) != 0 ? PARS_RO_FLOAT_FORMAT : PARS_FLOAT_FORMAT, *value_p.f);
            break;

        case PAR_DOUBLE:

            fprintf(f, PARS_DOUBLE_FORMAT, *value_p.d);
            break;

        case PAR_STRING:

            fprintf(f, PARS_STRING_FORMAT, *value_p.s);
            break;

        case PAR_ENUM:

            fprintf(f, PARS_STRING_FORMAT, ccParsEnumValueToString(par->ccpars_enum, *value_p.u));
            break;

        case PAR_BOOL:

            fprintf(f, PARS_STRING_FORMAT, *value_p.b == false ? "FALSE" : "TRUE");
            break;

        case PAR_REL_TIME:

            fprintf(f, PARS_STRING_FORMAT, cctimeNsPrintRelTime(*value_p.n, strbuf));
            break;

        case PAR_ABS_TIME:

            fputs("  ",f);

            ccParsPrintAbsTime(f, *value_p.n, 9);   // Print ABS_TIME with nanosecond resolution (9)
            break;

        case PAR_US_TIME:

            fputs("  ",f);

            fprintf(f, PARS_US_TIME_FORMAT, value_p.us->secs.abs, value_p.us->us);
            break;

        case PAR_BITMASK:

            if((par->flags & PARS_RW) != 0 && *value_p.u == 0)
            {
                // Bit mask parameter is settable and value is zero to print 0x0 to avoid an empty value

                fputs("  0x0", f);
            }
            else
            {
                ccParsPrintBitmask(f, *value_p.u, par->ccpars_enum);
            }

            break;

        case PAR_POINT:

            fprintf(f, PARS_POINT_FORMAT, value_p.p->time, value_p.p->ref);

            break;

        case PAR_POINTER:

            if(*value_p.t != NULL)
            {
                fprintf(f, PARS_POINTER_FORMAT, *value_p.t);
            }
            break;

        case PAR_FGIDX:

            if(*value_p.x < REF_NUM_FG_PARS)
            {
                fprintf(f, PARS_STRING_FORMAT, ccrun.fg_pars_link[*value_p.x]->name);
            }
            break;

        default:

            // This should never happen

            assert(false);
            break;
        }
    }
}



void ccParsGet(FILE                 * const f,
               struct CC_pars const * const par,
               uint32_t                     cyc_sel,
               uint32_t               const array_idx,
               uint32_t               const cmd_name_width,
               uint32_t                     par_name_width)
{
    struct CC_range cyc_sel_range;

    // Add space for (cyc_sel) after the parameter name

    par_name_width += 5;

    // Determine cycle selector range from cyc_sel

    if(ccParsGetCycSelRange(par, &cyc_sel_range, cyc_sel) == EXIT_SUCCESS)
    {
        // Print values for each cycle selector within the range

        for(cyc_sel = cyc_sel_range.from ; cyc_sel <= cyc_sel_range.to ; cyc_sel++)
        {
            uint32_t const num_elements = *ccParsNumElements(par, cyc_sel);

            if(num_elements > 0)
            {
                uint32_t num_characters = 0;

                // Print parameter name

                if(cmd_name_width > 0)
                {
                    num_characters = fprintf(f, "%-*s %s", cmd_name_width, par->parent_cmd->name, par->name) - cmd_name_width;
                }
                else
                {
                    num_characters = fprintf(f, ",%s,%s,", par->parent_cmd->name, par->name);

                }

                // Print cycle selector if non zero

                if(cyc_sel > 0)
                {
                    num_characters += fprintf(f, "(%u)", cyc_sel);
                }

                uint32_t idx_from;
                uint32_t idx_to;

                // If array index provided, then just display this one element

                if(array_idx != CC_NO_INDEX)
                {
                    idx_from        = array_idx;
                    idx_to          = array_idx;
                    num_characters += fprintf(f, "[%u]", array_idx);
                }
                else // else display all elements
                {
                    idx_from = 0;
                    idx_to   = num_elements - 1;
                }

                // Pad with spaces to align values column

                while(++num_characters <= par_name_width)
                {
                    fputc(' ', f);
                }

                // Print the values for the specified array range

                for(uint32_t idx_local = idx_from ; idx_local <= idx_to ; idx_local++)
                {
                    ccParsPrintElement(f, par, cyc_sel, idx_local);
                }

                fputc('\n', f);
            }
        }
    }
}



 uint32_t ccParsGetAllMatchingFlags(FILE                 * const f,
                                    struct CC_cmds const * const cmd,
                                    uint32_t               const cyc_sel,
                                    uint32_t               const array_idx,
                                    uint32_t               const flags_mask,
                                    uint32_t               const flags_required,
                                    uint32_t               const cmd_name_width,
                                    uint32_t               const par_name_width)
{
    struct CC_pars const * par;
    uint32_t               num_lines = 0;

    for(par = cmd->pars ; par->name != NULL ; par++)
    {
        if((flags_required == 0 && (par->flags & flags_mask) != 0) ||
           (flags_required != 0 && (par->flags & flags_mask) == flags_required))
        {
            ccParsGet(f, par, cyc_sel, array_idx, cmd_name_width, par_name_width);

            num_lines++;
        }
    }

    return num_lines;
}



uint32_t ccParsGetAll(FILE                 * const f,
                      struct CC_cmds const * const cmd,
                      uint32_t                     cyc_sel,
                      uint32_t               const array_idx,
                      uint32_t               const flags_mask,
                      uint32_t               const flags_required)
{
    uint32_t num_lines = 0;

    // If ALL_CYCLES specified by ()

    if(cyc_sel == CC_ALL_CYC_SELS)
    {
        for(cyc_sel = 0 ; cyc_sel < CC_NUM_CYC_SELS ; cyc_sel++)
        {
            num_lines += ccParsGetAllMatchingFlags(f, cmd, cyc_sel, array_idx, flags_mask, flags_required, ccfile.max_cmd_name_len, ccfile.max_par_name_len);
        }
    }
    else // Use supplied cyc_sel
    {
        num_lines = ccParsGetAllMatchingFlags(f, cmd, cyc_sel, array_idx, flags_mask, flags_required, cmd->name_len, cmd->max_par_name_len);
    }

    return num_lines;
}



void ccParsGetRefArmedPars(FILE *ref_file, uint32_t sub_sel, uint32_t cyc_sel)
{
    enum REF_fg_par_idx * fg_par_idx = &ccrun.fg_pars_idx[sub_sel][cyc_sel][0];

    while(*fg_par_idx != REF_FG_PAR_NULL)
    {
        ccParsGet(ref_file, ccrun.fg_pars_link[*(fg_par_idx++)], cyc_sel, CC_NO_INDEX, ccfile.max_cmd_name_len, ccfile.max_par_name_len);
    }
}



uint32_t ccParsSetDirectRef(double direct_ref)
{
    struct CC_pars    *par;
    enum REG_mode      idx      = 0;
    enum REG_mode      reg_mode = regMgrVarValue(&reg_mgr, REG_MODE);

    // Find the direct parameter index based on reg mode

    switch(reg_mode)
    {
        case REG_FIELD:
        case REG_CURRENT:
        case REG_VOLTAGE:

            idx = reg_mode;
            break;

        default:

            return EXIT_SUCCESS;
    }

    // Get the direct reference parameter corresponding to the actual reg mode

    par = &direct_pars[idx];

    // Set the reference value

    *par->value_p.f = (cc_float)direct_ref;

    // Save the reference in a file

    return ccFileConfigPar(par);
}



uint32_t ccParsGetIterTimeAndOffset(struct CC_ns_time * const iter_time,
                                    struct CC_ns_time * const time_offset,
                                    char              **      remaining_line)
{
    *iter_time = ccThreadGetIterTime();
    *time_offset = cc_zero_ns;

    if(*remaining_line == NULL)
    {
        return EXIT_SUCCESS;
    }

    char * arg = ccParseNextArg(remaining_line);

    if(ccParsRelTime(arg, time_offset) == EXIT_FAILURE)
    {
        ccRtPrintError("Invalid offset time: %s", ccParseAbbreviateArg(arg));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



uint32_t ccParsGetSleepOffset(struct CC_ns_time * const offset_time, char ** remaining_line)
{
    struct CC_ns_time iter_time;
    struct CC_ns_time time_offset;
    char strbuf[22];

    if(ccParsGetIterTimeAndOffset(&iter_time, &time_offset, remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    if(time_offset.secs.rel < 0 || time_offset.secs.rel > 10000)
    {
        ccRtPrintError("Invalid sleep time: %s", cctimeNsPrintRelTime(time_offset, strbuf));
        return EXIT_FAILURE;
    }

    // In interactive mode or if offset is zero, sleep is always relative to the time now.

    if(ccRtModeIsBatch() == false || cctimeNsCmpRelTimesRT(time_offset, cc_zero_ns) == 0)
    {
        ccrun.wait.last_wakeup_iter_time = iter_time;
    }

    // Compute offset time since the last wake-up time

    *offset_time = cctimeNsAddRT(ccrun.wait.last_wakeup_iter_time, time_offset);

    // Bail out if the calculated offset time is less than the actual iteration time

    struct CC_ns_time const delta_time = cctimeNsSubRT(iter_time, *offset_time);

    if(cctimeNsCmpRelTimesRT(delta_time, cc_zero_ns) > 0)
    {
        ccRtPrintError("Event time is already %s s in the past", cctimeNsPrintRelTime(delta_time, strbuf));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// EOF
