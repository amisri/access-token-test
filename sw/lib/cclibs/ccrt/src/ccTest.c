//! @file  ccrt/src/ccTest.c
//!
//! @brief ccrt parameter testing functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>
#include <float.h>
#include <errno.h>

// Include ccrt program header files

#include "ccCmds.h"
#include "ccRt.h"
#include "ccParse.h"
#include "ccFile.h"
#include "ccInput.h"
#include "ccRun.h"
#include "ccTest.h"

// Test Anything Protocol messages should go to stdout
// Additional information can go to stderr
//
// ok {test_number} { - optional message}
// not ok {test_number} { - optional message}
//
// test_number must be incremented, starting from 1.
//
// Before exit, the program must write a final line giving the range of test numbers completed:
//
// 1..{total_number_of_tests_executed}




static bool ccCompareFloat(cc_float const arg_value, char const condition, cc_float const par_value)
{
    // Prepare tolerance: '%' -> GLOBAL FLOAT_TOLERANCE, '~' -> GLOBAL INC_FLOAT_TOLERANCE

    cc_float const tolerance = (condition == '%' ? ccpars_global.inc_float_tolerance : ccpars_global.float_tolerance);

    // Do not scale the tolerance for values which are close to 0. Compare them with constant tolerance instead.

    if(fabs(arg_value) < tolerance)
    {
        return (fabs(par_value - arg_value) <= tolerance);
    }

    return (fabs(par_value - arg_value) <= (tolerance * fabs(arg_value)));
}



uint32_t ccTestBool(struct CC_test const * const test, bool * const result)
{
    // Check that only one parameter element is to be tested

    if(test->num_elements > 1)
    {
        ccRtPrintError("Only scalar booleans can be tested");
        return EXIT_FAILURE;
    }

    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Identify next argument, which must be the last on the line

    struct CC_pars const * const par = test->par;

    char * arg;
    char * remaining_line = &args_string[0];

    if((arg = ccParseNextArg(&remaining_line)) == NULL)
    {
        ccRtPrintError("Missing argument");
        return EXIT_FAILURE;
    }

    if(ccParseNoMoreArgs(&remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Convert boolean argument

    bool arg_value;

    if(strcasecmp(arg, "TRUE") == 0)
    {
        arg_value = true;
    }
    else if(strcasecmp(arg, "FALSE") == 0)
    {
        arg_value = false;
    }
    else
    {
        ccRtPrintError("Invalid boolean to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));
        return EXIT_FAILURE;
    }

    // Test parameter value and argument value using the specified condition

    switch(test->condition)
    {
        case '=':

            *result = (*test->value_p.b == arg_value);
            break;

        case '!':

            *result = (*test->value_p.b != arg_value);
            break;

        default:

            ccRtPrintError("Invalid condition for boolean parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestBitmask(struct CC_test const * const test, bool * const result)
{
    // Check that only one parameter element is to be checked

    if(test->num_elements > 1)
    {
        ccRtPrintError("Only scalar bitmask can be tested");
        return EXIT_FAILURE;
    }

    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each bit mask argument in term to produce the complete bit mask

    struct CC_pars const * const par = test->par;

    char     * arg;
    char     * remaining_line = &args_string[0];
    uint32_t   arg_bitmask    = 0;

    while((arg = ccParseNextArg(&remaining_line)) != NULL)
    {
        // A bit mask enum value is the set bit's index, not the bit mask - only one bit can be set

        int32_t enum_index = ccParsEnumStringToIndex(par, arg, true);

        if(enum_index == -1)
        {
            return EXIT_FAILURE;
        }

        arg_bitmask |= (1 << par->ccpars_enum[enum_index].value);
    }

    // Apply condition to the argument bit mask and the parameter bit mask

    uint32_t arg_and_par_mask = arg_bitmask & *test->value_p.u;

    switch(test->condition)
    {
        case '=':

            *result = (arg_and_par_mask == arg_bitmask);
            break;

        case '!':

            *result = (arg_and_par_mask == 0);
            break;

        default:

            ccRtPrintError("Invalid condition for bitmask parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestEnum(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;

    char            * arg;
    char            * remaining_line = &args_string[0];
    uint32_t          num_elements   = test->num_elements;
    union CC_value_p  value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in enum assertion");
            return EXIT_FAILURE;
        }

        // Convert enum argument to get the enum index

        int32_t  enum_index = ccParsEnumStringToIndex(par, arg, true);

        if(enum_index == -1)
        {
            return EXIT_FAILURE;
        }

        uint32_t enum_value = par->ccpars_enum[enum_index].value;

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '=':

                *result = (*value_p.u == enum_value);
                break;

            case '!':

                *result = (*value_p.u != enum_value);
                break;

            default:

                ccRtPrintError("Invalid condition for ENUM parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestInteger(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;
    char                 *       arg;
    char                 *       remaining_line = &args_string[0];
    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in integer assertion");
            return EXIT_FAILURE;
        }

        // Convert argument to signed integer

        char *remaining_arg;

        errno = 0;

        int32_t arg_value = (int32_t)strtol(arg, &remaining_arg, 0);

        if(*remaining_arg != '\0' || errno != 0)
        {
            ccRtPrintError("Invalid integer to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));
            return EXIT_FAILURE;
        }

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '=':

                *result = (*value_p.i == arg_value);
                break;

            case '!':

                *result = (*value_p.i != arg_value);
                break;

            case '<':

                *result = (*value_p.i < arg_value);
                break;

            case '>':

                *result = (*value_p.i > arg_value);
                break;

            default:

                ccRtPrintError("Invalid condition for integer parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestShort(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;
    char                 *       arg;
    char                 *       remaining_line = &args_string[0];
    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in short integer assertion");
            return EXIT_FAILURE;
        }

        // Convert argument to signed integer

        char *remaining_arg;

        errno = 0;

        int32_t arg_value = (int32_t)strtol(arg, &remaining_arg, 0);

        if(*remaining_arg != '\0' || errno != 0)
        {
            ccRtPrintError("Invalid integer to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));
            return EXIT_FAILURE;
        }

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '=':

                *result = ((int32_t)*value_p.h == arg_value);
                break;

            case '!':

                *result = ((int32_t)*value_p.h != arg_value);
                break;

            case '<':

                *result = ((int32_t)*value_p.h < arg_value);
                break;

            case '>':

                *result = ((int32_t)*value_p.h > arg_value);
                break;

            default:

                ccRtPrintError("Invalid condition for integer parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestFloat(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;
    char                 *       arg;
    char                 *       remaining_line = &args_string[0];
    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in float assertion");
            return EXIT_FAILURE;
        }

        // Convert argument to floating point value

        cc_double arg_value;

        if(ccParsDouble(arg, &arg_value) == EXIT_FAILURE)
        {
            ccRtPrintError("Invalid float to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));
            return EXIT_FAILURE;
        }

        cc_double par_value = par->type == PAR_FLOAT ? *value_p.f : *value_p.d;

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '~':   // Within 0.01% of value
            case '%':   // WIthin    1% of value

                *result = ccCompareFloat(arg_value, test->condition, par_value);
                break;

            case '=':

                *result = (par_value == arg_value);
                break;

            case '!':

                *result = (par_value != arg_value);
                break;

            case '<':

                *result = (par_value < arg_value);
                break;

            case '>':

                *result = (par_value > arg_value);
                break;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestRelTime(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;
    char                 *       arg;
    char                 *       remaining_line = &args_string[0];
    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in point assertion");
            return EXIT_FAILURE;
        }

        // Convert argument to point point value

        struct CC_ns_time arg_value;

        if(ccParsRelTime(arg, &arg_value) != EXIT_SUCCESS)
        {
            ccRtPrintError("Invalid point to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));

            return EXIT_FAILURE;
        }

        // Compare parameter against test value

        int32_t const cmp = cctimeNsCmpRelTimesRT(*value_p.n, arg_value);

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '=':

                *result = (cmp == 0);
                break;

            case '!':

                *result = (cmp != 0);
                break;

            case '<':

                *result = (cmp < 0);
                break;

            case '>':

                *result = (cmp > 0);
                break;

            default:

                ccRtPrintError("Invalid condition for point parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestPoint(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    struct CC_pars const * const par = test->par;
    char                 *       arg;
    char                 *       remaining_line = &args_string[0];
    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in point assertion");
            return EXIT_FAILURE;
        }

        // Convert argument to point point value

        struct FG_point arg_value;
        char            trailing_char;

        if(sscanf(arg, "%f|%f%c", &arg_value.time, &arg_value.ref, &trailing_char) != 2)
        {
            ccRtPrintError("Invalid point to compare with %s %s : '%s'", par->parent_cmd->name, par->name, ccParseAbbreviateArg(arg));

            return EXIT_FAILURE;
        }

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '~':   // Within 0.01% of value

                *result = ccCompareFloat(arg_value.time, '~', value_p.p->time) &&
                          ccCompareFloat(arg_value.ref , '~', value_p.p->ref);
                break;

            case '=':

                *result = (value_p.p->time == arg_value.time && value_p.p->ref == arg_value.ref);
                break;

            case '!':

                *result = (value_p.p->time != arg_value.time && value_p.p->ref != arg_value.ref);
                break;

            default:

                ccRtPrintError("Invalid condition for point parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestString(struct CC_test const * const test, bool * const result)
{
    // Take a copy of the arguments string, to avoid corrupting original string

    char args_string[CC_MAX_TEST_PARS_LEN+1];

    strcpy(args_string, test->args_string);

    // Process each argument in term

    uint32_t                     num_elements   = test->num_elements;
    union CC_value_p             value_p        = test->value_p;
    struct CC_pars const * const par            = test->par;
    char                 *       remaining_line = &args_string[0];
    char                 *       empty_string   = "";
    char                 *       arg;
    char           const *       value;

    *result = true;

    while((arg = ccParseNextArg(&remaining_line)) != NULL && *result)
    {
        // Check that we haven't run out of array elements to test

        if(num_elements-- == 0)
        {
            ccRtPrintError("Too many argument in string assertion");
            return EXIT_FAILURE;
        }

        // Support PAR_STRING and PAR_FGIDX

        if(par->type == PAR_FGIDX)
        {
            value = *value_p.x == REF_FG_PAR_NULL ? empty_string : ccrun.fg_pars_link[*value_p.x]->name;
        }
        else
        {
            // par->type == PAR_STRING

            value = *value_p.s;
        }

        // Test parameter value and argument value using the specified condition

        switch(test->condition)
        {
            case '=':

                *result = !strcmp(value, arg);
                break;

            case '!':

                *result = strcmp(value, arg);
                break;

            default:

                ccRtPrintError("Invalid condition for string parameter %s %s : '%c'", par->parent_cmd->name, par->name, test->condition);
                return EXIT_FAILURE;
        }

        value_p.c += par->array_step;
    }

    return EXIT_SUCCESS;
}



uint32_t ccTestPrepare(struct CC_test * const test, struct CC_pars const * const par, char * remaining_line)
{
    static char * empty_string = "";

    // Check that the next argument contains a single character (the condition, this was already checked)

    char * arg = ccParseNextArg(&remaining_line);

    if(arg[1] != '\0')
    {
        ccRtPrintError("Invalid condition: %s", ccParseAbbreviateArg(arg));
        return EXIT_FAILURE;
    }

    test->condition = *arg;

    // If no other arguments follow the condition then stop

//    if(remaining_line == NULL)
//    {
//        ccRtPrintError("Missing argument");
//        return EXIT_FAILURE;
//    }

    // Check length of remaining arguments is not too long to store in test structure

    if(remaining_line == NULL)
    {
        test->args_string = empty_string;
    }
    else
    {
        uint32_t args_string_len = (uint32_t)strlen(remaining_line);

        if(args_string_len > CC_MAX_TEST_PARS_LEN)
        {
            ccRtPrintError("Parameter string length (%u) exceeds limit (%u)", args_string_len, CC_MAX_TEST_PARS_LEN);
            return EXIT_FAILURE;
        }

        test->args_string = remaining_line;
    }


    // Prepare and check sub_sel index

    uint32_t sub_sel = (ccParsIsSubSel(par) ? ccpars_global.sub_sel : 0);

    if(sub_sel >= CC_NUM_SUB_SELS)
    {
        ccRtPrintError("GLOBAL SUB_SEL is out of bounds for %s %s: %d >= %d", par->parent_cmd->name, par->name, sub_sel, CC_NUM_SUB_SELS);
        return EXIT_FAILURE;
    }

    // Prepare and check cyc_sel

    uint32_t cyc_sel = ccfile.cyc_sel;

    if(cyc_sel == CC_NO_INDEX)
    {
        cyc_sel = 0;
    }

    if(cyc_sel == CC_ALL_CYC_SELS || (ccParsIsCycSel(par) == false && cyc_sel != 0))
    {
        ccRtPrintError("Invalid cycle selector for %s %s: %d", par->parent_cmd->name, par->name, cyc_sel);
        return EXIT_FAILURE;
    }

    // Prepare and check array index

    uint32_t num_elements = par->num_elements[cyc_sel];

    // If an array index is defined then limit array range to this one index

    if(ccfile.array_idx != CC_NO_INDEX)
    {
        test->idx_from = test->idx_to = ccfile.array_idx;
    }
    else // else display all elements
    {
        test->idx_from = 0;
        test->idx_to   = num_elements - 1;
    }

    // Check if from index is beyond the number of elements in the array at the moment

    if(test->idx_from >= num_elements)
    {
        ccRtPrintError("Array index out of bounds for %s %s: %d >= %d", par->parent_cmd->name, par->name, test->idx_from, num_elements);
        return EXIT_FAILURE;
    }

    test->num_elements = test->idx_to - test->idx_from + 1;

    // Get pointer to parameter value for (cyc_sel)[array_idx]

    uint8_t * byte_p = NULL;

    refMgrVarPointerFunc(&ref_mgr, par->value_p.c, sub_sel, cyc_sel, par->sub_sel_step, par->cyc_sel_step, (uint8_t **)&byte_p);

    test->value_p.c = byte_p + test->idx_from * par->array_step;

    // Check that a test function is defined for this type parameter

    if(ccpars_test_func[par->type] == NULL)
    {
        ccRtPrintError("Type of %s %s cannot be tested", par->parent_cmd->name, par->name);
        return EXIT_FAILURE;
    }

    test->cyc_sel = cyc_sel;
    test->par     = par;

    return EXIT_SUCCESS;
}



void ccTestPrintTapHeader(bool const test_result)
{
    ccrun.test.counter++;

    // If test failed then add "not " prefix

    if(test_result)
    {
        printf("ok %3u | ", ccrun.test.counter);
    }
    else
    {
        printf("not ok %3u | ", ccrun.test.counter);
        ccrun.test.not_ok_counter++;
    }
}



uint32_t ccTestPar(struct CC_pars const * const par, char * remaining_line)
{
    // Prepare test structure

    struct CC_test test;

    if(ccTestPrepare(&test, par, remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Run test function

    bool result;

    uint32_t const exit_status = ccpars_test_func[par->type](&test, &result);

    // If no errors reported then report test result using Test Anything Protocol to stdout

    if(exit_status == EXIT_SUCCESS)
    {
        pthread_mutex_lock(&ccrun.test.mutex);

        ccTestPrintTapHeader(result);

        // Report assertion

        printf("%s %s", par->parent_cmd->name, par->name);

        if(test.cyc_sel > 0)
        {
            printf("(%u)", test.cyc_sel);
        }

        // Print the values for the specified array range

        if(ccfile.array_idx != CC_NO_INDEX)
        {
            printf("[%u]", ccfile.array_idx);
        }

        printf(" %c", test.condition);

        char * arg;
        remaining_line = test.args_string;

        while((arg = ccParseNextArg(&remaining_line)) != NULL)
        {
            printf(" %s", arg);
        }

        if(result == false)
        {
            fputs(" (", stdout);

            for(uint32_t idx_local = test.idx_from ; idx_local <= test.idx_to ; idx_local++)
            {
                ccParsPrintElement(stdout, par, test.cyc_sel, idx_local);
            }

            printf("  ) %u ", ccrun.iteration_counter);

            if(ccInputIsStdin() == false)
            {
                uint32_t const num_input_files = ccInputGetNumberOfFiles();

                for(uint32_t i = 0; i < num_input_files; i++)
                {
                    time_t const timep = ccrun.iter_time.secs.abs;
                    struct tm    tm_result;

                    localtime_r(&timep, &tm_result);

                    printf("_from_ %s:%u at %02u.%09d", ccInputGetPath(i), ccInputGetLineNumber(i), tm_result.tm_sec, ccrun.iter_time.ns);
                }
            }
        }

        putchar('\n');

        pthread_mutex_unlock(&ccrun.test.mutex);
    }

    return exit_status;
}



uint32_t ccTestPrepareWait(char *remaining_line, bool * const test_result)
{
    bool    multiple_match;
    char  * par_group_name = remaining_line;
    int32_t par_group_idx  = ccParseCmdName(&remaining_line, &multiple_match);

    if(multiple_match)
    {
        ccRtPrintError("Ambiguous parameter name '%s'", ccParseAbbreviateArg(par_group_name));
        return EXIT_FAILURE;
    }

    if(par_group_idx < 0 || ccCmdsIsParameter(par_group_idx) == false)
    {
        ccRtPrintError("Unknown parameter '%s'", ccParseAbbreviateArg(par_group_name));
        return EXIT_FAILURE;
    }

    if(remaining_line == NULL)
    {
        ccRtPrintError("Incomplete parameter name", par_group_name);
        return EXIT_FAILURE;
    }

    struct CC_pars * par_matched;

    if(ccParseParName(par_group_idx, &remaining_line, &par_matched) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    if(remaining_line == NULL)
    {
        ccRtPrintError("Missing condition");
        return EXIT_FAILURE;
    }


    if(ccTestIsValidCondition(remaining_line[0]) == false)
    {
        ccRtPrintError("Invalid condition");
        return EXIT_FAILURE;
    }

    if(ccTestPrepare(&ccrun.wait.test, par_matched, remaining_line) == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }

    // Check the test function - this validates the condition and arguments and if the result is true
    // then there is no need to wait

    return ccpars_test_func[par_matched->type](&ccrun.wait.test, test_result);
}

// EOF
