//! @file  ccrt/src/ccCal.c
//!
//! @brief ccrt calibration functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// Include ccrt program header files

#include "ccCmds.h"
#include "ccThread.h"

// Static variables

static char const * const cal_string[] =
{
    "offset",
    "positive gain error",
    "negative gain error",
};



uint32_t ccCalAdcs(void)
{
    uint32_t exit_status = EXIT_SUCCESS;

    // Do not allow for DAC calibration if the converter is running

    if(ccSimIsPcOn())
    {
        ccRtPrintError("ADC calibration request rejected because the PC is on");
        return EXIT_FAILURE;
    }

    // Save ADC sources

    enum CC_sim_adc_signal  prev_adc_signal[SIG_NUM_ADCS];

    memcpy(&prev_adc_signal, &ccpars_sim.adc_signal, sizeof(ccpars_sim.adc_signal));

    // Calibrate offset, positive gain and negative gain errors for all ADCs

    enum CC_sim_adc_signal adc_signal;
    enum SIG_cal_level     cal_level;

    for(cal_level = SIG_CAL_OFFSET, adc_signal = ADC_SIGNAL_CAL_OFFSET; cal_level < SIG_NUM_CALS && exit_status == EXIT_SUCCESS; cal_level++, adc_signal++)
    {
        fprintf(stderr, "Calibrating %s for all ADCs\n", cal_string[cal_level]);

        // Set the multiplexer to calibration voltage corresponding to the calibration level

        uint32_t adc_idx;

        for(adc_idx = 0; adc_idx < SIG_NUM_ADCS; adc_idx++)
        {
            // Notify libsig that the ADC will be used for calibration

            sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_CALIBRATING_FLAG) = true;

            ccpars_sim.adc_signal[adc_idx] = adc_signal;
        }

        // Wait for the calibration buffer to fill - add half a second to allow for more than two extra 200ms averaging periods

        ccThreadwaitFor(CMD_WAIT_CAL_ADCS, (cc_double)ccpars_cal.num_samples * reg_mgr.iter_period + 0.5, "CalAdcs");

        // Calibrate current level

        for(adc_idx = 0; adc_idx < SIG_NUM_ADCS; adc_idx++)
        {
            enum SIG_cal_level returned_cal_level = sigAdcCal(&sig_struct.mgr, &sig_struct.adc.array[adc_idx]);

            if(returned_cal_level == SIG_CAL_FAULT)
            {
                ccRtPrintError("Calibration failed for ADC %c", 'A' + adc_idx);
                exit_status = EXIT_FAILURE;
            }
            else if(returned_cal_level != cal_level)
            {
                ccRtPrintError("Calibration level mismatch for ADC %c: exp %u got %u", 'A' + adc_idx, cal_level, returned_cal_level);
                exit_status = EXIT_FAILURE;
            }

            // Notify libsig that the ADC is no longer being used for calibration

            sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_CALIBRATING_FLAG) = false;
        }
    }

    // Restore ADC sources

    memcpy(&ccpars_sim.adc_signal, &prev_adc_signal, sizeof(ccpars_sim.adc_signal));

    return exit_status;
}



uint32_t ccCalDccts(void)
{
    uint32_t exit_status = EXIT_SUCCESS;

    // Do not allow for DCCT calibration if the converter is running

    if(ccSimIsPcOn())
    {
        ccRtPrintError("DCCT calibration request rejected because the PC is on");

        return EXIT_FAILURE;
    }

    // Calibration of DCCT will always use ADC_A and ADC_B, so notify libsig

    sigVarValue(&sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = true;
    sigVarValue(&sig_struct, adc, adc_b, ADC_CALIBRATING_FLAG) = true;

    // Save ADC sources and then select DCCT_A and DCCT_B

    enum CC_sim_adc_signal  prev_adc_signal[SIG_NUM_ADCS];

    memcpy(&prev_adc_signal, &ccpars_sim.adc_signal, sizeof(ccpars_sim.adc_signal));

    ccpars_sim.adc_signal[SIG_ADC_ADC_A] = ADC_SIGNAL_I_DCCT_A;
    ccpars_sim.adc_signal[SIG_ADC_ADC_B] = ADC_SIGNAL_I_DCCT_B;

    // Set the external calibration current to each calibration level in turn

    enum SIG_cal_level cal_level;

    for(cal_level = SIG_CAL_OFFSET; cal_level < SIG_NUM_CALS && exit_status == EXIT_SUCCESS; cal_level++)
    {
        fprintf(stderr, "Calibrating %s for all DCCTs\n", cal_string[cal_level]);

        // Inform the real-time thread about the calibration level, so that it can inject proper calibration current into the DCCTs

        ccpars_sim.dcct_cal = cal_level;

        // Wait for the calibration buffer to fill - add half a second to allow for more than two extra 200ms averaging periods

        ccThreadwaitFor(CMD_WAIT_CAL_DCCTS, (cc_double)ccpars_cal.num_samples * reg_mgr.iter_period + 0.5, "CalDccts");

        // Calibrate current level

        uint32_t dcct_idx;

        for(dcct_idx = 0 ; dcct_idx < DCCT_N_DCCTS ; dcct_idx++)
        {
            enum SIG_cal_level returned_cal_level = sigTransducerCal(&sig_struct.mgr, &sig_struct.transducer.array[dcct_idx]);

            if(returned_cal_level == SIG_CAL_FAULT)
            {
                ccRtPrintError("Calibration failed for DCCT %c", 'A' + dcct_idx);
                exit_status = EXIT_FAILURE;
            }
            else if(returned_cal_level != cal_level)
            {
                ccRtPrintError("Calibration level mismatch for DCCT %c: exp %u got %u", 'A' + dcct_idx, cal_level, returned_cal_level);
                exit_status = EXIT_FAILURE;
            }
        }
    }

    ccpars_sim.dcct_cal = SIG_CAL_NONE;

    // Restore ADC sources

    memcpy(&ccpars_sim.adc_signal, &prev_adc_signal, sizeof(ccpars_sim.adc_signal));

    // Notify libsig that the ADCs are back to normal

    sigVarValue(&sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = false;
    sigVarValue(&sig_struct, adc, adc_b, ADC_CALIBRATING_FLAG) = false;

    return exit_status;
}



uint32_t ccCalDac(void)
{
    // Do not allow for DAC calibration if the converter is running

    if(ccSimIsPcOn())
    {
        ccRtPrintError("Cannot calibrate the DAC - PC is on");
        return EXIT_FAILURE;
    }

    // Notify libsig that ADC_A will be used for the calibration of the DAC

    sigVarValue(&sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = true;

    // Inform the real-time thread that DAC calibration is in progress
    // This variable is set only by the command thread, so it should be safe
    // to access it in the RT thread without a mutex

    ccsim.cal_dac = true;

    // Save the ADC A source and link it to the DAC

    enum CC_sim_adc_signal prev_adc_a_source = ccpars_sim.adc_signal[SIG_ADC_ADC_A];

    ccpars_sim.adc_signal[SIG_ADC_ADC_A] = ADC_SIGNAL_DAC;

    // Calibrate the DAC - offset, positive gain and negative gain - in that order

    uint32_t exit_status = EXIT_SUCCESS;

    for(ccsim.cal_dac_level = SIG_CAL_OFFSET; ccsim.cal_dac_level < SIG_NUM_CALS && exit_status == EXIT_SUCCESS; ccsim.cal_dac_level++)
    {
        fprintf(stderr, "Calibrating DAC %s\n", cal_string[ccsim.cal_dac_level]);

        // Wait for the calibration buffer to fill - allow for more than two 200ms averaging periods

        ccThreadwaitFor(CMD_WAIT_CAL_DAC, 0.5, "CalDac");

        // Calibrate the DAC using the actual average measured voltage from ADC A

        enum SIG_cal_level returned_cal_level = sigDacCal(&dac, ccsim.cal_dac_raw[ccsim.cal_dac_level],
                                                          sigVarValue(&sig_struct, adc, adc_a, ADC_MEAS_AVE1_MEAN));

        if(returned_cal_level == SIG_CAL_FAULT)
        {
            ccRtPrintError("DAC calibration failed");
            exit_status = EXIT_FAILURE;
        }
        else if(returned_cal_level != ccsim.cal_dac_level)
        {
            ccRtPrintError("DAC calibration level mismatch: exp %u got %u", ccsim.cal_dac_level, returned_cal_level);
            exit_status = EXIT_FAILURE;
        }
    }

    // Restore the source of ADC A

    ccpars_sim.adc_signal[SIG_ADC_ADC_A] = prev_adc_a_source;

    // Inform the real-time thread that DAC calibration has finished

    ccsim.cal_dac = false;

    // Notify libsig that ADC_A is no longer used for calibration

    sigVarValue(&sig_struct, adc, adc_a, ADC_CALIBRATING_FLAG) = false;

    return exit_status;
}

// EOF
