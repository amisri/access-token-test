//! @file  ccrt/src/ccThread.c
//!
//! @brief ccrt thread functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/utsname.h>
#include <sys/mman.h>

#include "ccThread.h"
#include "ccCmds.h"
#include "ccFile.h"
#include "ccRun.h"
#include "ccDebug.h"

// Static variables

static bool     rt_linux;                             // True if Linux has PREMPT RT extensions

// Constants

#define CC_MAX_PRIORITY                     49



void ccThreadDetectRtPreempt(void)
{
    struct utsname u;
    FILE          *f;
    bool           criteria_1;
    bool           criteria_2 = false;

    uname(&u);

    criteria_1 = (strcasestr(u.version, "PREEMPT RT") != NULL);

    if((f = fopen("/sys/kernel/realtime","r")) != NULL)
    {
        int flag;

        criteria_2 = (fscanf(f, "%d", &flag) == 1 && flag == 1);

        ccFileClose(f);
    }

    rt_linux = (criteria_1 && criteria_2);

    if(rt_linux)
    {
        fputs("Running Linux with RT PREMPT patch\n", stderr);
    }
}



void ccThreadStart(char *label, struct CC_thread *thread, void *(*function)(void *), int32_t priority)
{
    thread->label = label;

    // Configure thread attributes

    if(pthread_attr_init(&thread->thread_attr) == -1)
    {
        ccRtPrintErrorAndExit("Failed to configure thread %s attributes", label);
    }

    if(rt_linux == false)
    {
        pthread_attr_setinheritsched(&thread->thread_attr, PTHREAD_INHERIT_SCHED);
    }
    else
    {
        if(mlockall(MCL_FUTURE|MCL_CURRENT))
        {
            ccRtPrintErrorAndExit("Failed to lock memory, please make sure you have root privileges");
        }

        if(pthread_attr_setinheritsched(&thread->thread_attr, PTHREAD_EXPLICIT_SCHED))
        {
            ccRtPrintErrorAndExit("Failed to set explicit sched, please make sure you have root privileges");
        }

        if(pthread_attr_setschedpolicy(&thread->thread_attr, SCHED_FIFO))
        {
            ccRtPrintErrorAndExit("Failed to set sched policy , please make sure you have root privileges");
        }
    }

    thread->thread_param.sched_priority = CC_MAX_PRIORITY + priority;

    pthread_attr_setschedparam(&thread->thread_attr, &thread->thread_param);

    // Start the thread

    pthread_create(&thread->thread, &thread->thread_attr, function, NULL);

    // Report start of thread

    ccRtPrintInVerbose("%s thread started with priority %d", label, thread->thread_param.sched_priority);
}



void ccThreadTryWake(struct CC_thread *cc_thread)
{
    if(pthread_mutex_trylock(&cc_thread->condition_mutex) == 0)
    {
        cc_thread->wake_up_flag = true;

        pthread_cond_signal(&cc_thread->condition_cond);

        pthread_mutex_unlock(&cc_thread->condition_mutex);
    }
    else
    {
        cc_thread->missed_counter++;
    }
}



void ccThreadwaitFor(enum CC_cmds_wait_commands const wait_cmd, cc_double const timeout, char const * const label)
{
    ccThreadWaitUntil(wait_cmd, wait_func_time_not_set, cctimeNsAddRT(ccThreadGetIterTime(), cctimeNsDoubleToRelTimeRT(timeout)), label);
}



void ccThreadWaitUntil(enum CC_cmds_wait_commands const wait_cmd,
                       struct CC_ns_time          const func_time,
                       struct CC_ns_time          const wait_time,
                       char               const * const label)
{
    char strbuf[2][22];

    ccRtPrintInVerbose("%s %s command started : func_time=%s  wait_time=%u.%09d"
                      ,  cctimeNsPrintRelTime(cctimeNsSubRT(ccThreadGetIterTime(), ccrun.start_time), strbuf[0])
                      ,  label
                      ,  cctimeNsPrintRelTime(func_time, strbuf[1])
                      ,  wait_time.secs.abs
                      ,  wait_time.ns
                      );

    pthread_mutex_lock(&ccrun.wait.condition_mutex);

    ccrun.wait.func_time = func_time;
    ccrun.wait.wait_time = wait_time;
    ccrun.wait.cmd       = wait_cmd;
    ccrun.wait.active    = true;

    while(ccrun.wait.active)
    {
        pthread_cond_wait(&ccrun.wait.condition_cond, &ccrun.wait.condition_mutex);
    }

    pthread_mutex_unlock(&ccrun.wait.condition_mutex);

    ccRtPrintInVerbose("%s %s command finished: %u",
                        cctimeNsPrintRelTime(cctimeNsSubRT(ccrun.wait.last_wakeup_iter_time, ccrun.start_time), strbuf[0]),
                        label,
                        ccrun.wait.last_wakeup_iteration_counter);
}



struct CC_ns_time ccThreadGetIterTime(void)
{
    pthread_mutex_lock(&ccrun.iter_time_mutex);

    struct CC_ns_time iter_time = ccrun.iter_time;

    pthread_mutex_unlock(&ccrun.iter_time_mutex);

    return iter_time;
}



int ccThreadWait(void)
{
    void *status;

    // Wait for Read Commands thread to exit

    pthread_join(cc_threads.read_cmds.thread, &status);

    // Cancel the other threads

    pthread_cancel(cc_threads.timer.thread);
    pthread_cancel(cc_threads.real_time.thread);
    pthread_cancel(cc_threads.event_log.thread);
    pthread_cancel(cc_threads.log_writing.thread);

    // Wait for all these threads to exit and ignore their exit statuses

    pthread_join(cc_threads.timer.thread      , NULL);
    pthread_join(cc_threads.real_time.thread  , NULL);
    pthread_join(cc_threads.event_log.thread  , NULL);
    pthread_join(cc_threads.log_writing.thread, NULL);

    // Return the status from the Read Commands thread

    return (int)(intptr_t)status;
}



void ccThreadPrintMissed(struct CC_thread *thread)
{
    if(thread->missed_counter != 0)
    {
        fprintf(stderr,"%-20s: %u missed iterations\n", thread->label, thread->missed_counter);
    }
}



void *ccThreadTimerThread(void *args)
{
    struct timespec sleep_time;

    sleep_time.tv_sec = 0;
    timer.period      = ((cc_double)regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) * 1.0E-9) / (cc_double)timer.acceleration;

    // Prepare to run timer loop

    double wakeup_time = getRealTime();

    timer.iter_time = floor(wakeup_time);

    // If necessary, wait till start of next second

    if(timer.iter_time != wakeup_time)
    {
        timer.iter_time   += 1.0;
        sleep_time.tv_nsec = (long)((timer.iter_time - wakeup_time) * 1.0E9);

        nanosleep(&sleep_time, NULL);
    }

    // Run timer until the thread is cancelled

    for(;;)
    {
        // Try to wake up the real-time thread

        ccThreadTryWake(&cc_threads.real_time);

        // Sleep until the next iteration

        double wakeup_time = getRealTime();

        sleep_time.tv_nsec = (long)((timer.iter_time + timer.period - wakeup_time) * 1.0E9);

        if(sleep_time.tv_nsec > 0)
        {
            nanosleep(&sleep_time, NULL);
        }
        else
        {
            cc_threads.timer.missed_counter++;
        }

        timer.iter_time += timer.period;
    }

    return NULL;
}

// EOF
