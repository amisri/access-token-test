//! @file  ccrt/src/ccInit.c
//!
//! @brief ccrt initialization functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <signal.h>
#include <pthread.h>

// Include ccrt program header files

#include "ccThread.h"
#include "ccCmds.h"
#include "ccPars.h"
#include "ccRt.h"
#include "ccParse.h"
#include "ccLog.h"
#include "ccRun.h"
#include "ccFile.h"
#include "ccSim.h"
#include "ccEvtlog.h"

// Include cclibs generated initialization or test header files

#include "logMenusInit.h"
#include "logStructsInit.h"
#include "libref/refParsLinkStructs.h"



void ccInitPars(void)
{
    struct CC_pars * par;
    struct CC_cmds * cmd;

    // Allocate space for the number of elements arrays for all the parameters

    for(cmd = cmds; cmd->name != NULL; cmd++)
    {
        // Keep track of maximum length of a command name

        cmd->name_len = strlen(cmd->name);

        if(cmd->name_len > ccfile.max_cmd_name_len)
        {
            ccfile.max_cmd_name_len = cmd->name_len;

            assert(ccfile.max_cmd_name_len < CC_PATH_LEN);
        }

        // Process each parameter (if cmd is a parameter group)

        par = cmd->pars;

        if(par != NULL)
        {
            while(par->name != NULL)
            {
//                fprintf(stderr,"ccInitPars: %s %s :",cmd->name,par->name);

                size_t const par_type_size = ccParsGetTypeSize(par->type);

                // Link each parameter to its parent command

                par->parent_cmd = cmd;

                // Accumulate the flags from all parameters to set the summary flags for the command

                cmd->flags |= par->flags;

                // Check that parameter is not SUB_SEL but not CYC_SEL

                if(ccParsIsSubSel(par) == true && ccParsIsCycSel(par) == false)
                {
                    ccRtPrintErrorAndExit("Par %s %s must have PARS_CYC_SEL set if PARS_SUB_SEL is set", cmd->name, par->name);
                }

                // Allocate the space for the number of elements and initialize the first number of elements from the default value

                uint32_t const num_cyc_sels = ccParsIsCycSel(par) == true
                                            ? CC_NUM_CYC_SELS
                                            : 1;

                uint32_t const num_sub_sels = ccParsIsSubSel(par) == true
                                            ? CC_NUM_SUB_SELS
                                            : 1;

                size_t const num_elements_array_len = num_cyc_sels * num_sub_sels;

                par->num_elements = calloc(num_elements_array_len, sizeof(uintptr_t));

                CC_ASSERT(par->num_elements != NULL);

                // Set array step according to these rules:
                //    1. Non sub/cyc_sel and struct size zero:    array_step = parameter size
                //    2. Non sub/cyc_sel and struct size defined: array_step = struct size
                //    3. Cyc_sel:                                 array_step = parameter size
                // Rule 2 can only apply to read-only parameters.

                if(ccParsIsCycSel(par) == false && par->size_of_struct > 0)
                {
                    if(ccParsIsReadWrite(par) == true)
                    {
                        ccRtPrintErrorAndExit("Par %s %s must be read-only to have a non-zero array step", cmd->name, par->name);
                    }

                    par->array_step = par->size_of_struct;
                }
                else
                {
                    par->array_step = par_type_size;
                }

                // Initialize parameter value and number of elements for every sub/cyc selector from the default value

                if(ccParsIsCycSel(par) == true)
                {
                    par->cyc_sel_step = par->size_of_struct;
                    par->sub_sel_step = par->size_of_struct * CC_NUM_CYC_SELS;

                    for(uint32_t sub_sel = 0; sub_sel < num_sub_sels; sub_sel++)
                    {
                        for(uint32_t cyc_sel = 0; cyc_sel < num_cyc_sels; cyc_sel++)
                        {
                            par->num_elements[sub_sel * CC_NUM_CYC_SELS + cyc_sel] = par->num_default_elements;

                            // Initialize value for read/write properties only

                            if(ccParsIsReadWrite(par) == true)
                            {
                                memmove(&par->value_p.c[(sub_sel * par->sub_sel_step) + (cyc_sel * par->cyc_sel_step)],
                                        &par->value_p.c[0],
                                        par_type_size * par->num_default_elements);
                            }
                        }
                    }
                }
                else
                {
                    // Parameter is not cyc_sel/sub_sel so initialize

                    par->num_elements[0] = par->num_default_elements;
                }

                // Keep track of maximum length of a parameter name for this cmd

                par->name_len = strlen(par->name);

                if(par->name_len > cmd->max_par_name_len)
                {
                    cmd->max_par_name_len = par->name_len;
                }

                // Set full parameter name

                ccFilePrintPath(par->full_name, "%s %s", cmd->name, par->name);

                par++;
            }
        }

        // Keep track of maximum length of a parameter name for every cmd

        if(cmd->max_par_name_len > ccfile.max_par_name_len)
        {
            ccfile.max_par_name_len = cmd->max_par_name_len;
        }
    }

    // Initialize number of elements for logspy signal parameters to be the number of signals

    logspy_pars[LOGSPY_SIG_META].num_elements           = &ccpars_logspy.num_signals;
    logspy_pars[LOGSPY_SIG_DIG_BIT_INDEX].num_elements  = &ccpars_logspy.num_signals;
    logspy_pars[LOGSPY_SIG_TIME_OFFSET_US].num_elements = &ccpars_logspy.num_signals;
    logspy_pars[LOGSPY_SIG_NAME].num_elements           = &ccpars_logspy.num_signals;
}



void ccInitLibreg(void)
{
    uint32_t const ac_period_iters = 200;

    // Initialize libreg to support field, current regulation and voltage regulation - regMgrInit will erase reg_mgr

    int32_t err_num = regMgrInit(&reg_mgr,
                                 &reg_pars,
                                 &ccsim.deco,
                                 &ccrun.reg_pars_mutex,
                                 (REG_mutex_callback *)pthread_mutex_lock,
                                 (REG_mutex_callback *)pthread_mutex_unlock,
                                 (cc_float *)calloc(ac_period_iters, sizeof(cc_float)),
                                 ac_period_iters,
                                 CC_ENABLED,
                                 CC_ENABLED,
                                 CC_ENABLED);

    if(err_num != 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("regMgrInit() reports error number %d", err_num);
    }

    // Prepare measurement FIR buffers

    int32_t * const i_meas_filter_buffer = calloc(CC_FILTER_BUF_LEN, sizeof(int32_t));
    int32_t * const b_meas_filter_buffer = calloc(CC_FILTER_BUF_LEN, sizeof(int32_t));

    if(i_meas_filter_buffer == NULL || b_meas_filter_buffer == NULL)
    {
        ccRtPrintErrorAndExit("Memory allocation failure during libreg initialization");
    }

    regMeasFilterInitBuffer(&reg_mgr.b.meas, i_meas_filter_buffer, CC_FILTER_BUF_LEN);
    regMeasFilterInitBuffer(&reg_mgr.i.meas, b_meas_filter_buffer, CC_FILTER_BUF_LEN);

    // Link libreg to liblog to keep the current and field regulation log periods correct

    regMgrInitRegPeriodPointers(&reg_mgr, &log_mgr.period[LOG_B_REG].ns, &log_mgr.period[LOG_I_REG].ns);

    // Set default parameter values

    regMgrParAppValue(&reg_pars, HARMONICS_AC_PERIOD_ITERS) = ac_period_iters;
}



void ccInitLibref(void)
{
    // Initialize libref parameter pointers to ccrt variables

     // ILC - Link to function parameter array lengths

     refMgrParValue(&ref_mgr, ILC_L_FUNC_NUM_ELS) = ilc_pars[ILC_L_FUNC].num_elements;
     refMgrParValue(&ref_mgr, ILC_Q_FUNC_NUM_ELS) = ilc_pars[ILC_Q_FUNC].num_elements;

     // TRANSACTION

     refMgrInitTransactionPars(&ref_mgr,
                               &ccpars_trans[0][0],
                               trans_pars->sub_sel_step,
                               trans_pars->cyc_sel_step);

     // CTRL - this shares ref_pars with REF FG_TYPE

     refMgrInitCtrlPars(&ref_mgr,
                        &ccpars_ctrl[0][0],
                         ref_pars[REF_PAR_PLAY].sub_sel_step,
                         ref_pars[REF_PAR_PLAY].cyc_sel_step);

     // REF - this shares ref_pars with REF PLAY and REF DYN_ECO_END_TIME

     refMgrInitRefPars(&ref_mgr,
                       &ccpars_ref[0][0],
                        ref_pars[REF_PAR_FG_TYPE].sub_sel_step,
                        ref_pars[REF_PAR_FG_TYPE].cyc_sel_step);

    // RAMP

    refMgrInitRampPars(&ref_mgr,
                       &ccpars_ramp[0][0],
                        ramp_pars->sub_sel_step,
                        ramp_pars->cyc_sel_step);

    // PULSE

    refMgrInitPulsePars(&ref_mgr,
                        &ccpars_pulse[0][0],
                         pulse_pars->sub_sel_step,
                         pulse_pars->cyc_sel_step);

    // PLEP

    refMgrInitPlepPars(&ref_mgr,
                       &ccpars_plep[0][0],
                        plep_pars->sub_sel_step,
                        plep_pars->cyc_sel_step);

    ref_mgr.test_plep_initial_rate = &ccpars_global.test_plep_initial_rate;   // Test inital_rate parameter for PLEP function testing
    ref_mgr.test_plep_final_rate   = &ccpars_global.test_plep_final_rate;     // Test final_rate parameter for PLEP function testing

    // PPPL

    refMgrInitPpplPars(&ref_mgr,
                       &ccpars_pppl[0][0],
                        pppl_pars->sub_sel_step,
                        pppl_pars->cyc_sel_step);

    // PPPL num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration1_num_els, pppl_pars[PPPL_ACCELERATION1].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration2_num_els, pppl_pars[PPPL_ACCELERATION2].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration3_num_els, pppl_pars[PPPL_ACCELERATION3].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_rate2_num_els        , pppl_pars[PPPL_RATE2].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_rate4_num_els        , pppl_pars[PPPL_RATE4].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_ref4_num_els         , pppl_pars[PPPL_REF4].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, pppl_duration4_num_els    , pppl_pars[PPPL_DURATION4].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t));

    // CUBEXP

    refMgrInitCubexpPars(&ref_mgr,
                         &ccpars_cubexp[0][0],
                          cubexp_pars->sub_sel_step,
                          cubexp_pars->cyc_sel_step);

    // CUBEXP num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    refMgrFgParInitPointer   (&ref_mgr, cubexp_ref_num_els        , cubexp_pars[CUBEXP_REF].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, cubexp_rate_num_els       , cubexp_pars[CUBEXP_RATE].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t));

    refMgrFgParInitPointer   (&ref_mgr, cubexp_time_num_els       , cubexp_pars[CUBEXP_TIME].num_elements);
    refMgrFgParInitSubSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    refMgrFgParInitCycSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t));

    // TABLE - Not a sub_sel function

    refMgrFgParInitPointer   (&ref_mgr, table_function            , ccpars_table[0].function);
    refMgrFgParInitCycSelStep(&ref_mgr, table_function            , table_pars[TABLE_FUNCTION].cyc_sel_step);

    refMgrFgParInitPointer   (&ref_mgr, table_function_num_els    , table_pars[TABLE_FUNCTION].num_elements);
    refMgrFgParInitCycSelStep(&ref_mgr, table_function_num_els    , sizeof(uintptr_t));

    // TRIM

    refMgrInitTrimPars(&ref_mgr,
                       &ccpars_trim[0][0],
                        trim_pars->sub_sel_step,
                        trim_pars->cyc_sel_step);

    // TEST

    refMgrInitTestPars(&ref_mgr,
                       &ccpars_test[0][0],
                        test_pars->sub_sel_step,
                        test_pars->cyc_sel_step);

    // PRBS

    refMgrInitPrbsPars(&ref_mgr,
                       &ccpars_prbs[0][0],
                        prbs_pars->sub_sel_step,
                        prbs_pars->cyc_sel_step);

    // Initialize links from libref function generator parameter indexes to ccrt parameter structures

    ccrun.fg_pars_link[REF_FG_PAR_REF_FG_TYPE          ] = &ref_pars[REF_PAR_FG_TYPE]         ;  // Function type.
    ccrun.fg_pars_link[REF_FG_PAR_RAMP_INITIAL_REF     ] = &ramp_pars[RAMP_INITIAL_REF]       ;  // Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_RAMP_FINAL_REF       ] = &ramp_pars[RAMP_FINAL_REF]         ;  // Final reference for a RAMP.
    ccrun.fg_pars_link[REF_FG_PAR_RAMP_ACCELERATION    ] = &ramp_pars[RAMP_ACCELERATION]      ;  // RAMP acceleration.
    ccrun.fg_pars_link[REF_FG_PAR_RAMP_DECELERATION    ] = &ramp_pars[RAMP_DECELERATION]      ;  // RAMP deceleration.
    ccrun.fg_pars_link[REF_FG_PAR_RAMP_LINEAR_RATE     ] = &ramp_pars[RAMP_LINEAR_RATE]       ;  // Maximum linear rate during the RAMP.
    ccrun.fg_pars_link[REF_FG_PAR_PULSE_REF            ] = &pulse_pars[PULSE_REF]             ;  // PULSE reference.
    ccrun.fg_pars_link[REF_FG_PAR_PULSE_DURATION       ] = &pulse_pars[PULSE_DURATION]        ;  // PULSE duration.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_INITIAL_REF     ] = &plep_pars[PLEP_INITIAL_REF]       ;  // Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_FINAL_REF       ] = &plep_pars[PLEP_FINAL_REF]         ;  // Final reference for a PLEP.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_ACCELERATION    ] = &plep_pars[PLEP_ACCELERATION]      ;  // PLEP acceleration.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_LINEAR_RATE     ] = &plep_pars[PLEP_LINEAR_RATE]       ;  // Maximum linear rate during the PLEP.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_TC          ] = &plep_pars[PLEP_EXP_TC]            ;  // PLEP exponential segment time constant.
    ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_FINAL       ] = &plep_pars[PLEP_EXP_FINAL]         ;  // PLEP exponential segment asymptote.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_INITIAL_REF     ] = &pppl_pars[PPPL_INITIAL_REF]       ;  // Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION1   ] = &pppl_pars[PPPL_ACCELERATION1]     ;  // Array of initial PPPL accelerations.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION2   ] = &pppl_pars[PPPL_ACCELERATION2]     ;  // Array of second PPPL accelerations.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION3   ] = &pppl_pars[PPPL_ACCELERATION3]     ;  // Array of third PPPL accelerations.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE2           ] = &pppl_pars[PPPL_RATE2]             ;  // Array of rates at the start of the second PPPL accelerations.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE4           ] = &pppl_pars[PPPL_RATE4]             ;  // Array of rates at the start of the linear PPPL segments.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_REF4            ] = &pppl_pars[PPPL_REF4]              ;  // Array of references at the start of the linear PPPL segments.
    ccrun.fg_pars_link[REF_FG_PAR_PPPL_DURATION4       ] = &pppl_pars[PPPL_DURATION4]         ;  // Array of durations of the fourth linear linear PPPL segments.
    ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_REF           ] = &cubexp_pars[CUBEXP_REF]           ;  // Array of cubexp start/end references.
    ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_RATE          ] = &cubexp_pars[CUBEXP_RATE]          ;  // Array of cubexp start/end rates.
    ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_TIME          ] = &cubexp_pars[CUBEXP_TIME]          ;  // Array of cubexp start/end times.
    ccrun.fg_pars_link[REF_FG_PAR_TABLE_FUNCTION       ] = &table_pars[TABLE_FUNCTION]        ;  // TABLE function points array.
    ccrun.fg_pars_link[REF_FG_PAR_TRIM_INITIAL_REF     ] = &trim_pars[TRIM_INITIAL_REF]       ;  // Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_TRIM_FINAL_REF       ] = &trim_pars[TRIM_FINAL_REF]         ;  // Final reference for a TRIM.
    ccrun.fg_pars_link[REF_FG_PAR_TRIM_DURATION        ] = &trim_pars[TRIM_DURATION]          ;  // Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_INITIAL_REF     ] = &test_pars[TEST_INITIAL_REF]       ;  // Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_AMPLITUDE_PP    ] = &test_pars[TEST_AMPLITUDE_PP]      ;  // TEST function peak-peak amplitude.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_PERIOD          ] = &test_pars[TEST_PERIOD]            ;  // TEST function period.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_NUM_PERIODS     ] = &test_pars[TEST_NUM_PERIODS]       ;  // Number of TEST function periods to play.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_PERIOD_ITERS    ] = &test_pars[TEST_PERIOD_ITERS]      ;  // Sampling period in function generator iterations of TEST function to play.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_WINDOW          ] = &test_pars[TEST_WINDOW]            ;  // Control of TEST function half-period sine window.
    ccrun.fg_pars_link[REF_FG_PAR_TEST_EXP_DECAY       ] = &test_pars[TEST_EXP_DECAY]         ;  // Control of TEST function exponential amplitude delay.
    ccrun.fg_pars_link[REF_FG_PAR_PRBS_INITIAL_REF     ] = &prbs_pars[PRBS_INITIAL_REF]       ;  // Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    ccrun.fg_pars_link[REF_FG_PAR_PRBS_AMPLITUDE_PP    ] = &prbs_pars[PRBS_AMPLITUDE_PP]      ;  // PRBS peak-peak amplitude.
    ccrun.fg_pars_link[REF_FG_PAR_PRBS_PERIOD_ITERS    ] = &prbs_pars[PRBS_PERIOD_ITERS]      ;  // PRBS period in iterations.
    ccrun.fg_pars_link[REF_FG_PAR_PRBS_NUM_SEQUENCES   ] = &prbs_pars[PRBS_NUM_SEQUENCES]     ;  // Number of PRBS sequences to play.
    ccrun.fg_pars_link[REF_FG_PAR_PRBS_K               ] = &prbs_pars[PRBS_K]                 ;  // PRBS K factor. Sequence length is 2^K - 1 periods.

    // Initialize memory for error, status and armed structures

    struct FG_error       * const fg_error   = calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct FG_error      ));
    struct REF_cyc_status * const cyc_status = calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_cyc_status));
    struct REF_armed      * const ref_armed  = calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_armed     ));

    // TABLE function does not support sub_sel in ccrt, so get memory only for CC_NUM_CYC_SELS - note that libref can support [sub_sel][cyc_sel] tables

    struct FG_point * const armed_table_function = calloc(REF_ARMED_TABLE_FUNCTION_LEN(CC_NUM_CYC_SELS, 1, TABLE_LEN), sizeof(struct FG_point));

    // ILC cycle data store

    struct REF_ilc_cyc_data * const ilc_cyc_data_store = calloc(REF_ILC_CYC_DATA_STORE_NUM_ELS(CC_ILC_MAX_SAMPLES,CC_MAX_CYC_SEL),sizeof(struct REF_ilc_cyc_data));

    // Running functions are triple buffered (two nexts and active) so allocated three more table buffers

    struct FG_point * const running_table_function = calloc(REF_RUNNING_TABLE_FUNCTION_LEN(TABLE_LEN), sizeof(struct FG_point));

    if(fg_error               == NULL ||
       cyc_status             == NULL ||
       ref_armed              == NULL ||
       armed_table_function   == NULL ||
       running_table_function == NULL ||
       ilc_cyc_data_store     == NULL)
    {
        ccRtPrintErrorAndExit("Memory allocation failure during libref initialization");
    }

    // Initialize the ref manager

    int32_t err_num =  refMgrInit(&ref_mgr,
                                  &polswitch_mgr,
                                  &reg_mgr,
                                  &ccrun.arm_event_mutex,
                                  fg_error,
                                  cyc_status,
                                  ref_armed,
                                  armed_table_function,
                                  running_table_function,
                                  ilc_cyc_data_store,
                                  CC_NUM_CYC_SELS,
                                  TABLE_LEN,
                                  CC_ILC_MAX_SAMPLES,
                                  4);                           // ref_to_tc_limit

    if(err_num < 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("refMgrInit() reports error number %d", err_num);
    }
    else if(err_num > 0)
    {
        // Report NULL parameter pointer

        ccRtPrintErrorAndExit("refMgrInit() reports FG parameter %d has a NULL pointer", err_num);
    }

    // Initialize fg_par_idx array

    enum REF_fg_par_idx * fg_pars_idx = &ccrun.fg_pars_idx[0][0][0];
    uint32_t              len = sizeof(ccrun.fg_pars_idx) / sizeof(enum REF_fg_par_idx);

    while(len-- > 0)
    {
        *(fg_pars_idx++) = REF_FG_PAR_NULL;
    }
}



void ccInitLogging(void)
{
    // Initialize the liblog structures with a pattern to check that logStructsInit() correctly resets them to zero

    memset(&log_mgr,     0x55, sizeof(log_mgr));
    memset(&log_structs, 0xAA, sizeof(log_structs));
    memset(&log_buffers, 0xAA, sizeof(log_buffers));

    // Initialize liblog structures (log_mgr, log_structs and log_read) for this application

    logStructsInit(&log_mgr, &log_structs, &log_buffers);     // logStructsInit() is generated automatically from def/log.csv

    // Initialize log_read structure

    memset(&log_read, 0x55, sizeof(log_read));

    logReadInit(&log_read, &log_structs, &log_buffers);                  // logReadInit() is generated automatically from def/log.csv

    // Initialize liblog menus (log_menus) for this application

    logMenusInit(&log_mgr, &log_menus, 0);     // logMenusInit() is generated automatically from def/log.csv

    // Change the name of two logs to test the logChangeMenuName()
    // which is generated automatically from def/log.csv

    logChangeMenuName(&log_menus, LOG_MENU_CODIM, "CODIM1");
    logChangeMenuName(&log_menus, LOG_MENU_DCDIM, "DCDIM1");

    // Change the name and/or units of signals, testing both logStoreSigNameAndUnits() and logPrepareStoreSigNameAndUnitsRequest()

    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_I_MEAS_RATE,    "",             "A/s",  false);
    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_I_REF_DIRECT_R, "I_REF_DIRECT", "",     false);
    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_I_REF_RATE_R,   "I_REF_RATE_R", "A/s",  false);
    ccLogStoreSigNamesAndUnits(LOG_FAULTS, LOG_FAULTS_B_ERR_FLT_R,   "B_ERR_FLT",    "",     false);

    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_MEAS_OHMS,      "",             "Ohms", true);
    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_I_MAG_SAT_D,    "I_MAG_SAT",    "",     true);
    ccLogStoreSigNamesAndUnits(LOG_I_REG,  LOG_I_REG_POWER_D,        "POWER",        "W",    true);
    ccLogStoreSigNamesAndUnits(LOG_FAULTS, LOG_FAULTS_B_RST_FLT_D,   "B_RST_FLT",    "",     true);

    // Initialize time offsets for DIM channels (convert ms to seconds)

    ccrun.dim.time_offset_a = DIM_ANA_A_TIME_OFFSET_MS * 1.0E-3;
    ccrun.dim.time_offset_b = DIM_ANA_B_TIME_OFFSET_MS * 1.0E-3;
    ccrun.dim.time_offset_c = DIM_ANA_C_TIME_OFFSET_MS * 1.0E-3;
    ccrun.dim.time_offset_d = DIM_ANA_D_TIME_OFFSET_MS * 1.0E-3;

    // Set number of samples per cycle in the discontinuous DIM logs

    ccrun.dim.dc_num_samples = log_structs.log[LOG_DCDIM].num_samples_per_sig / 2;
    ccrun.dim.dc_counter     = ccrun.dim.dc_num_samples;

    // Adjust log_mgr periods which are defined in the log.ods file assuming an iter_period of 1000

    for(uint32_t log_index = 0 ; log_index < LOG_NUM_LOGS ; log_index++)
    {
        log_mgr.period[log_index] = cctimeNsDivRT(cctimeNsMulRT(log_mgr.period[log_index], regMgrParAppValue(&reg_pars, ITER_PERIOD_NS)), CC_ITER_PERIOD_NS);
    }

    // Adjust polarity switch periods - they are defined by the event log period in the converter CLOCK file

    struct CC_ns_time const evt_log_period = { { 0 }, regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) };

    log_mgr.period[LOG_POLSWITCH]   = cctimeNsMulRT(evt_log_period, timer.event_log_period_iters);
    log_mgr.period[LOG_POLSWIFLAGS] = log_mgr.period[LOG_POLSWITCH];
}

// EOF
