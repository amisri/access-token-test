# Compile and run rampdown test program

echo Make output directories

mkdir -p powerspy/mbi powerspy/qf powerspy/qd functions/output/mbi functions/output/qf functions/output/qd

echo Compiling rampdown test program

gcc main.c rampdown.c -o rampdown

echo
echo ============= Generating example functions for MBI ==============

mkdir -p powerspy/mbi

./rampdown mbi MBI-LHC25NS.csv 2 > tmp; mv tmp powerspy/mbi/MBI-LHC25NS.csv
./rampdown mbi MBI-HIRADMT2.csv 2 > tmp; mv tmp powerspy/mbi/MBI-HIRADMT2.csv
./rampdown mbi MBI-AWAKE.csv   2 > tmp; mv tmp powerspy/mbi/MBI-AWAKE.csv
./rampdown mbi MBI-MD26.csv    2 > tmp; mv tmp powerspy/mbi/MBI-MD26.csv

echo
echo ============= Generating example functions for QD ==============

mkdir -p powerspy/qd

./rampdown qd QD-LHC25NS.csv  2 > tmp; mv tmp powerspy/qd/QD-LHC25NS.csv
./rampdown qd QD-HIRADMT2.csv 2 > tmp; mv tmp powerspy/qd/QD-HIRADMT2.csv
./rampdown qd QD-AWAKE.csv    2 > tmp; mv tmp powerspy/qd/QD-AWAKE.csv
./rampdown qd QD-MD26.csv     2 > tmp; mv tmp powerspy/qd/QD-MD26.csv

echo
echo ============= Generating example functions for QF ==============

mkdir -p powerspy/qf

./rampdown qf QF-LHC25NS.csv  2 > tmp; mv tmp powerspy/qf/QF-LHC25NS.csv
./rampdown qf QF-HIRADMT2.csv 2 > tmp; mv tmp powerspy/qf/QF-HIRADMT2.csv
./rampdown qf QF-AWAKE.csv    2 > tmp; mv tmp powerspy/qf/QF-AWAKE.csv
./rampdown qf QF-MD26.csv     2 > tmp; mv tmp powerspy/qf/QF-MD26.csv

echo Cleaning up

rm rampdown

# EOF
