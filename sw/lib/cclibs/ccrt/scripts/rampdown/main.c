// pep function test program

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include "rampdown.h"

// Constants

#define PATH_LEN            64
#define MAX_POINTS          5000

// Global variables

struct Rampdown    rampdown;

struct Point
{
    double      ref_time;
    double      ref;
};

struct Point    end_input_table_point;
struct Point    end_input_table_rampdown_point;

struct Config
{
    double      timestep;
    double      load_ohms_ser;
    double      load_henrys;
    double      load_henrys_sat;
    double      load_i_sat_start;
    double      load_i_sat_end;
    double      load_sat_smoothing;
    double      limits_i_min;
    double      limits_v_min;
    double      limits_v_rate;
    double      limits_p_min;
    double      limits_p_rate;
    double      limits_p_acc;
};

struct Config   config =
{
    .timestep           =   0.001,
    .load_ohms_ser      =   3.440000E+00,
    .load_henrys        =   6.700000E+00,
    .load_henrys_sat    =   3.200000E+00,
    .load_i_sat_start   =   4.000000E+03,
    .load_i_sat_end     =   5.500000E+03,
    .load_sat_smoothing =   3.500000E-01,
    .limits_i_min       =   1.500000E+02,
    .limits_v_min       =  -2.300000E+04,
    .limits_v_rate      =   5.000000E+05,
    .limits_p_min       =  -6.000000E+07,
    .limits_p_rate      =   1.000000E+09,
    .limits_p_acc       =   1.000000E+10,
};

struct Prev_vars
{
    double          ref_time;
    double          i_ref;
    double          v_ref;
    double          v_sat;
    double          power;
    double          sat_factor;
    enum Limit_type limit_type;
} prev_vars[2];

struct FG_table
{
    int                     seg_idx;             //!< Segment index - identifies point at the end of the segment
    int                     prev_seg_idx;        //!< Previous segment index for which the rate of change was calculated.
    int                     num_points;          //!< Number of points in table.
    double                  seg_rate;            //!< Rate of change of reference for segment fg_table::prev_seg_idx
} input_table;

struct Point table_function[MAX_POINTS];

// Static functions

void PrintErrorAndExit(const char * const format, ...)
{
    va_list args;

    va_start(args, format);

    vfprintf(stderr, format, args);

    va_end(args);

    // Append newline

    fputc('\n', stderr);

    exit(EXIT_FAILURE);
}



FILE * OpenFile(const char * const file_name, const char * const mode)
{
    FILE *f = fopen(file_name, mode);

    if(f == NULL)
    {
         PrintErrorAndExit("Error opening file '%s' : %s (%d)", file_name, strerror(errno), errno);
    }

    return f;
}



void CloseFile(const char * const file_name, FILE * const file)
{
    if(fclose(file) != 0)
    {
        PrintErrorAndExit("Error closing file '%s' : %s (%d)", file_name, strerror(errno), errno);
    }
}



void ReadAndWriteConfigFile(char * const converter_name)
{
    char  file_name    [PATH_LEN];
    char  old_file_name[PATH_LEN];

    snprintf(file_name,     PATH_LEN, "config/%s", converter_name);
    snprintf(old_file_name, PATH_LEN, "%s.old",    file_name);

    // Try to open the CLOCK file

    FILE *f = fopen(file_name, "r");

    if(f != NULL)
    {
        struct Config   file_config;

        int n = fscanf(f,"TIMESTEP           %lf "
                         "LOAD OHMS_SER      %lf "
                         "LOAD HENRYS        %lf "
                         "LOAD HENRYS_SAT    %lf "
                         "LOAD I_SAT_START   %lf "
                         "LOAD I_SAT_END     %lf "
                         "LOAD SAT_SMOOTHING %lf "
                         "LIMITS V_MIN       %lf "
                         "LIMITS V_RATE      %lf "
                         "LIMITS P_MIN       %lf "
                         "LIMITS P_RATE      %lf "
                         "LIMITS P_ACC       %lf",
                         &file_config.timestep          ,
                         &file_config.load_ohms_ser     ,
                         &file_config.load_henrys       ,
                         &file_config.load_henrys_sat   ,
                         &file_config.load_i_sat_start  ,
                         &file_config.load_i_sat_end    ,
                         &file_config.load_sat_smoothing,
                         &file_config.limits_v_min      ,
                         &file_config.limits_v_rate     ,
                         &file_config.limits_p_min      ,
                         &file_config.limits_p_rate     ,
                         &file_config.limits_p_acc      );

        if(n == 12)
        {
            // Configuration file read successfully

            fprintf(stderr,"Read config file '%s'\n",file_name);

            config = file_config;
        }
        else
        {
            // Reading configuration file failed

            fprintf(stderr,"Error: failed to read config file '%s' (%i/13) - moving it to '%s'\n",file_name, n, old_file_name);
        }

        CloseFile(file_name, f);

        // Rename config file to have ".old" suffix

        if(rename(file_name, old_file_name) != 0)
        {
            fprintf(stderr, "Error: failed to rename '%s' to '%s' : %s (%d)\n", file_name, old_file_name, strerror(errno), errno);
        }
    }
    else
    {
        fprintf(stderr,"Error: failed to open config file '%s' - generating default file\n",file_name);
    }

    // Try to rewrite the config file

    f = OpenFile(file_name, "w");

    fprintf(f,  "TIMESTEP           % .6f\n"
                "LOAD OHMS_SER      % .6E\n"
                "LOAD HENRYS        % .6E\n"
                "LOAD HENRYS_SAT    % .6E\n"
                "LOAD I_SAT_START   % .6E\n"
                "LOAD I_SAT_END     % .6E\n"
                "LOAD SAT_SMOOTHING % .6E\n"
                "LIMITS V_MIN       % .6E\n"
                "LIMITS V_RATE      % .6E\n"
                "LIMITS P_MIN       % .6E\n"
                "LIMITS P_RATE      % .6E\n"
                "LIMITS P_ACC       % .6E\n",
                config.timestep           ,
                config.load_ohms_ser      ,
                config.load_henrys        ,
                config.load_henrys_sat    ,
                config.load_i_sat_start   ,
                config.load_i_sat_end     ,
                config.load_sat_smoothing ,
                config.limits_v_min       ,
                config.limits_v_rate      ,
                config.limits_p_min       ,
                config.limits_p_rate      ,
                config.limits_p_acc       );

    CloseFile(file_name, f);
}



int ReadTableFunctionFile(char * const converter_name,
                          char * const function_name)
{
    char  file_name[PATH_LEN];

    snprintf(file_name, PATH_LEN, "functions/input/%s/%s", converter_name, function_name);

    FILE * f = OpenFile(file_name, "r");

    char c;

    if(fscanf(f, "REF_TIME,RE%c", &c) != 1 || c != 'F')
    {
        PrintErrorAndExit("Invalid header in table function file '%s'", file_name);
    }

    // Read all points from input table CSV file

    struct Point point;
    int    sample_index = -1;
    int    n;

    do
    {
        n = fscanf(f, " %lf , %lf", &point.ref_time, &point.ref);

        if(n == 2)
        {
            table_function[++sample_index] = point;
        }
        else if(n == 1)
        {
            PrintErrorAndExit("Incomplete point in table function file '%s' (%d)", file_name, sample_index+1);
        }
    }
    while(n == 2);

    if(sample_index < 1)
    {
        PrintErrorAndExit("Table function file '%s' contains fewer than 2 points", file_name);
    }

    end_input_table_point = table_function[sample_index];

    // Scan for last point before ramp down

    int last_sample_index = sample_index;

    while(last_sample_index > 0 && table_function[last_sample_index-1].ref == table_function[0].ref)
    {
        last_sample_index--;
    }

    end_input_table_rampdown_point = table_function[last_sample_index];

    while(last_sample_index > 0 && table_function[last_sample_index].ref < table_function[last_sample_index-1].ref)
    {
        last_sample_index--;
    }

    fprintf(stderr,"\nRead %u points from table function file '%s'\n",sample_index, file_name);

    // Prepare to generate the original table for the PowerSpy logging

    input_table.num_points = sample_index + 1;
    input_table.seg_idx    = last_sample_index;

    return last_sample_index;
}



double fgTable(const double func_time)
{
    // Post-function coast

    if(func_time >= end_input_table_point.ref_time)
    {
         return end_input_table_point.ref;
    }

    // Scan forward through table to find segment containing the current time

    while(func_time >= table_function[input_table.seg_idx].ref_time)
    {
        input_table.seg_idx++;
    }

    // If time is in a new segment, calculate the gradient

    const uint32_t seg_idx = input_table.seg_idx;

    if(seg_idx != input_table.prev_seg_idx)
    {
        input_table.prev_seg_idx = seg_idx;
        input_table.seg_rate     = (table_function[seg_idx].ref      - table_function[seg_idx - 1].ref) /
                                   (table_function[seg_idx].ref_time - table_function[seg_idx - 1].ref_time);
    }

    // Calculate reference using segment gradient

    return table_function[seg_idx].ref - (table_function[seg_idx].ref_time - func_time) * input_table.seg_rate;
}




void WritePowerSpy(double ref_time, double output_time_step)
{
    // Calculate rates and acceleration for previous iteration

    double i_ref_rate = (rampdown.i_ref - prev_vars[1].i_ref) / (2.0 * output_time_step);
    double v_sat_rate = (rampdown.v_sat - prev_vars[1].v_sat) / (2.0 * output_time_step);
    double power_rate = (rampdown.power - prev_vars[1].power) / (2.0 * output_time_step);
    double power_acc  = (rampdown.power + prev_vars[1].power - 2.0 * prev_vars[0].power) / (output_time_step * output_time_step);

    // Save data for previous iteration

    printf("%.3f,%.7E,", prev_vars[0].ref_time, fgTable(prev_vars[0].ref_time));

    printf("%.7E,%.7E,%.7E,%.7E,",prev_vars[0].i_ref, prev_vars[0].v_ref, prev_vars[0].v_sat, prev_vars[0].power);

    printf("%.7E,%.7E,%.7E,%.7E,", i_ref_rate, v_sat_rate, power_rate, power_acc);

    printf("%.6f,%d\n", prev_vars[0].sat_factor, -prev_vars[0].limit_type);

    // Shift previous sample and save the new one

    prev_vars[1] = prev_vars[0];

    prev_vars[0].ref_time   = ref_time;
    prev_vars[0].i_ref      = rampdown.i_ref;
    prev_vars[0].v_ref      = rampdown.v_ref;
    prev_vars[0].v_sat      = rampdown.v_sat;
    prev_vars[0].power      = rampdown.power;
    prev_vars[0].sat_factor = rampdown.sat_factor;
    prev_vars[0].limit_type = rampdown.limit_type;
}



// Main function

int main(int argc, char **argv)
{
    int i;

    // Prepare arguments

    if(argc != 4)
    {
        fputs("usage: rampdown converter_name function_name output_period_iters > powerspy.csv\n", stderr);
        exit(-1);
    }

    char * converter_name = argv[1];
    char * function_name  = argv[2];
    int output_period_iters = atoi(argv[3]);

    // Read table function and arm ramp down

    int last_sample_index = ReadTableFunctionFile(converter_name, function_name);

    double i_rate = (table_function[last_sample_index].ref      - table_function[last_sample_index - 1].ref) /
                    (table_function[last_sample_index].ref_time - table_function[last_sample_index - 1].ref_time);

    // Read configuration parameters and initialize rampdown structure

    ReadAndWriteConfigFile(converter_name);

    RampdownInit(config.timestep                   ,
                 config.load_ohms_ser              ,
                 config.load_henrys                ,
                 config.load_henrys_sat            ,
                 config.load_i_sat_start           ,
                 config.load_i_sat_end             ,
                 config.load_sat_smoothing         ,
                 end_input_table_rampdown_point.ref,
                 config.limits_v_min               ,
                 config.limits_v_rate              ,
                 config.limits_p_min               ,
                 config.limits_p_rate              ,
                 config.limits_p_acc               ,
                 &rampdown);

    RampdownArm(&rampdown, table_function[last_sample_index].ref, i_rate);
/*
    fprintf(stderr,"rampdown.i_ref      = %.6E\n",  rampdown.i_ref      );
    fprintf(stderr,"rampdown.v_ref      = %.6E\n",  rampdown.v_ref      );
    fprintf(stderr,"rampdown.v_sat      = %.6E\n",  rampdown.v_sat      );
    fprintf(stderr,"rampdown.power      = %.6E\n",  rampdown.power      );
    fprintf(stderr,"rampdown.power_rate = %.6E\n",  rampdown.power_rate );
    fprintf(stderr,"rampdown.i_rate     = %.6E\n",  rampdown.i_rate     );
    fprintf(stderr,"rampdown.sat_factor = %.6E\n",  rampdown.sat_factor );

    fprintf(stderr,"rampdown.sat.henrys           = %.6E\n",  rampdown.load.sat.henrys          );
    fprintf(stderr,"rampdown.sat.i_start          = %.6E\n",  rampdown.load.sat.i_start         );
    fprintf(stderr,"rampdown.sat.i_end            = %.6E\n",  rampdown.load.sat.i_end           );
    fprintf(stderr,"rampdown.sat.smoothing        = %.6E\n",  rampdown.load.sat.smoothing       );
    fprintf(stderr,"rampdown.sat.df_dI_linear     = %.6E\n",  rampdown.load.sat.df_dI_linear    );
    fprintf(stderr,"rampdown.sat.d2f_dI2_parabola = %.6E\n",  rampdown.load.sat.d2f_dI2_parabola);

    for(i = 0 ; i < NUM_SAT_MODEL_SEGS ; i++)
    {
        fprintf(stderr,"rampdown.sat.i[%u]             = %.6E\n",  i, rampdown.load.sat.i[i]);
        fprintf(stderr,"rampdown.sat.f[%u]             = %.6E\n",  i, rampdown.load.sat.f[i]);
        fprintf(stderr,"rampdown.sat.integrated_f[%u]  = %.6E\n",  i, rampdown.load.sat.integrated_f[i]);
    }
*/
    double ref_time = table_function[last_sample_index].ref_time;

    // Prepare to write PowerSpy data to stdout

    double output_time_step = config.timestep * (double)output_period_iters;

    printf("type:analog timeOrigin:%.3f,TABLE_REF,I_REF,V_REF,V_SAT,POWER,I_REF_RATE,V_SAT_RATE,POWER_RATE,POWER_ACC,SAT_FACTOR,LIMIT_TYPE STEP\n", ref_time);

    prev_vars[0].ref_time   = ref_time;
    prev_vars[0].i_ref      = rampdown.i_ref;
    prev_vars[0].v_ref      = rampdown.v_ref;
    prev_vars[0].v_sat      = rampdown.v_sat;
    prev_vars[0].power      = rampdown.power;
    prev_vars[0].sat_factor = rampdown.sat_factor;
    prev_vars[0].limit_type = rampdown.limit_type;

    prev_vars[1] = prev_vars[0];

    // Prepare to generate output table function

    char file_name[PATH_LEN];

    snprintf(file_name, PATH_LEN, "functions/output/%s/%s", converter_name, function_name);

    FILE * f = OpenFile(file_name, "w");

    fputs("REF_TIME,REF\n",f);

    for(i = 0 ; i <= last_sample_index ; i++)
    {
        fprintf(f, "%.3f,%.7E\n", table_function[i].ref_time, table_function[i].ref);
    }

    // Generate ramp down

    fprintf(stderr,"Start ramp down from point %u: %.3f|%.7E   Rate: %.3f\n",
                    last_sample_index,
                    ref_time,
                    table_function[last_sample_index].ref,
                    rampdown.i_rate);

    fprintf(stderr,"Original ramp down duration: %.3f s\n", end_input_table_rampdown_point.ref_time - ref_time);

    int    output_counter = output_period_iters;
    double end_rampdown_time = 0.0;

    for(;;)
    {
        // Generate ramp down

        RampdownGenerate(&rampdown, &ref_time);

        // Store time of end of rampdown

        if(end_rampdown_time == 0.0 && rampdown.i_ref <= rampdown.limits.i_min)
        {
            end_rampdown_time = ref_time;
        }

        // Report ramp down at the required period

        if(--output_counter == 0)
        {
            // Output current reference on this iteration

            output_counter = output_period_iters;

            // Write signal to stdout for PowerSpy

            WritePowerSpy(ref_time, output_time_step);

            // Write I_REF to output table function file

            fprintf(f, "%.3f,%.7E\n", ref_time, rampdown.i_ref);

            // Quit once ramp down has finished

            if(ref_time > end_input_table_point.ref_time && (rampdown.limit_type == I_MIN_LIMITED && prev_vars[1].i_ref <= rampdown.limits.i_min))
            {
                fprintf(f, "%.3f,%.7E\n", end_input_table_point.ref_time, rampdown.i_ref);

                CloseFile(file_name, f);

                fprintf(stderr,"New ramp down duration:      %.3f s\nChange in duration:         %.3f s\n",
                                end_rampdown_time - table_function[last_sample_index].ref_time,
                                end_rampdown_time - end_input_table_rampdown_point.ref_time);

                exit(EXIT_SUCCESS);
            }
        }
    }
}

// EOF
