/*
 * Ramp down of current reference function generator
 *
 * These functions generate a ramp down function for current that respects
 * limits in voltage and power.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "rampdown.h"

// static functions

/*
 * Initialize load saturation model
 *
 * The model is the same as used in the power converter controls software.
 * The reduction in differential inductance is modeled using five segments:
 *
 *      Linear - Parabolic - Linear - Parabolic - Linear
 */
static void LoadInitSat(struct Load * const load)
{
    if(load->sat.henrys  > 0.0              &&
       load->henrys      > load->sat.henrys &&
       load->sat.i_start > 0.0              &&
       load->sat.i_end   > load->sat.i_start)
    {
        // Clip saturation smoothing factor to 0.0001 - 0.5

        if(load->sat.smoothing <= 0.0)
        {
            load->sat.smoothing = 0.0001;
        }
        else if(load->sat.smoothing > 0.5)
        {
            load->sat.smoothing = 0.5;
        }

        // Calculate parameters defining the first four segments of the normalized saturation model:
        //    0.linear  1.parabola  2.linear  3.parabola
        // There is a fifth linear segment, but this requires no additional parameters.

        const double delta_I_sat = load->sat.i_end - load->sat.i_start;
        const double f_sat       = 1.0 - load->sat.henrys / load->henrys;

        load->sat.f[1] = f_sat * load->sat.smoothing;
        load->sat.f[2] = f_sat * (1.0 - load->sat.smoothing);
        load->sat.f[3] = f_sat;

        load->sat.i[1] = load->sat.i_start + delta_I_sat * load->sat.smoothing;
        load->sat.i[2] = load->sat.i_end   - delta_I_sat * load->sat.smoothing;

        load->sat.df_dI_linear = f_sat / delta_I_sat;

        const double delta_I_parabola = 2.0 * load->sat.f[1] / load->sat.df_dI_linear;

        load->sat.d2f_dI2_parabola = load->sat.df_dI_linear / delta_I_parabola;

        load->sat.i[0] = load->sat.i[1] - delta_I_parabola;
        load->sat.i[3] = load->sat.i[2] + delta_I_parabola;

        load->sat.integrated_f[1] = load->sat.d2f_dI2_parabola * delta_I_parabola * delta_I_parabola * delta_I_parabola / 6.0;
        load->sat.integrated_f[2] = load->sat.integrated_f[1] + f_sat * (load->sat.i[2] - load->sat.i[1]) / 2.0;
        load->sat.integrated_f[3] = f_sat * (load->sat.i[3] - load->sat.i[0]) / 2.0;
    }
    else
    {
        // Disable saturation

        load->sat.i_end = 0.0;
     }
}



/*
 * Calculate load saturation factor f, using load saturation model
 *
 * The differential inductance L(i_mag_sat) = f.Lnominal
 *
 * The saturation factor returned is 1 for low currents. It reduces as i_mag_sat
 * rises into the range where the magnet saturates.
 */
static double SatFactor(const struct Load * const load, const double i_mag_sat)
{
    // Return immediately if saturation model is not enabled

    if(load->sat.i_end <= 0.0)
    {
        return 1.0;
    }

    // Calculate normalized saturation factor (starts at zero and goes up)
    // out of five segments: linear-parabolic-linear-parabolic-linear.

    double i0;        // current relative to end of the segment 0 (linear)
    double i1;        // current relative to end of the segment 1 (parabola)
    double i3;        // current relative to end of the segment 3 (parabola)
    double f;         // normalized saturation factor

    const double abs_i_mag_sat = fabs(i_mag_sat);

    if((i0 = abs_i_mag_sat - load->sat.i[0]) <= 0.0)
    {
        // Segment 0: linear

        f = 0;
    }
    else if((i1 = abs_i_mag_sat - load->sat.i[1]) <= 0.0)
    {
        // Segment 1: parabola

        f = 0.5 * load->sat.d2f_dI2_parabola * i0 * i0;
    }
    else if(abs_i_mag_sat <= load->sat.i[2])
    {
        // Segment 2: linear

        f = load->sat.f[1] + load->sat.df_dI_linear * i1;
    }
    else if((i3 = abs_i_mag_sat - load->sat.i[3]) <= 0.0)
    {
        // Segment 3: parabola

        f = load->sat.f[3] - 0.5 * load->sat.d2f_dI2_parabola * i3 * i3;
    }
    else
    {
        // Segment 4: linear

        f = load->sat.f[3];
    }

    // Return saturation factor (starts at 1.0 and goes down)

    return 1.0 - f;
}



/*
 * Convert current reference into voltage reference using the load model
 */
static double Iref2Vref(struct Rampdown * const rampdown, const double i_ref)
{
    double i_ref_rate = (i_ref - rampdown->i_ref) / rampdown->time_step;

    return i_ref * rampdown->load.ohms + rampdown->load.henrys * i_ref_rate;
}



/*
 * Convert current reference into voltage reference after saturation compensation using the load model
 */
static double Iref2Vsat(struct Rampdown * const rampdown, const double i_ref)
{
    double i_ref_rate = (i_ref - rampdown->i_ref) / rampdown->time_step;

    return i_ref * rampdown->load.ohms + rampdown->load.henrys * i_ref_rate * rampdown->sat_factor;
}

// Public functions

void RampdownInit(double timestep           ,
                  double load_ohms          ,
                  double load_henrys        ,
                  double load_henrys_sat    ,
                  double load_i_sat_start   ,
                  double load_i_sat_end     ,
                  double load_sat_smoothing ,
                  double limits_i_min       ,
                  double limits_v_min       ,
                  double limits_v_rate      ,
                  double limits_p_min       ,
                  double limits_p_rate      ,
                  double limits_p_acc       ,
                  struct Rampdown * rampdown)
{
    rampdown->time_step          = timestep;
    rampdown->load.ohms          = load_ohms;
    rampdown->load.henrys        = load_henrys;
    rampdown->load.sat.henrys    = load_henrys_sat;
    rampdown->load.sat.i_start   = load_i_sat_start;
    rampdown->load.sat.i_end     = load_i_sat_end;
    rampdown->load.sat.smoothing = load_sat_smoothing;
    rampdown->limits.i_min       = limits_i_min;
    rampdown->limits.v_min       = limits_v_min;
    rampdown->limits.v_rate      = limits_v_rate;
    rampdown->limits.p_min       = limits_p_min;
    rampdown->limits.p_rate      = limits_p_rate;
    rampdown->limits.p_acc       = limits_p_acc;

    LoadInitSat(&rampdown->load);
}



void RampdownArm(struct Rampdown * rampdown, const double i_ref, const double i_rate)
{
    rampdown->i_rate = i_rate;

    // Back up two time steps...

    rampdown->i_ref = i_ref - 2.0 * rampdown->time_step * i_rate;

    // ...to calculate power at -1 time steps

    rampdown->sat_factor = SatFactor(&rampdown->load, rampdown->i_ref);

    double i_ref1 = i_ref  - rampdown->time_step * i_rate;
    double power1 = i_ref1 * Iref2Vsat(rampdown, i_ref1);

    // Calculate i_ref, v_ref, v_sat, power and power_rate at start of the ramp down

    rampdown->sat_factor = SatFactor(&rampdown->load, i_ref1);

    rampdown->i_ref = i_ref1;
    rampdown->v_ref = Iref2Vref(rampdown, i_ref);
    rampdown->v_sat = Iref2Vsat(rampdown, i_ref);
    rampdown->power = i_ref * rampdown->v_sat;

    rampdown->power_rate = (rampdown->power - power1) / rampdown->time_step;
    rampdown->i_ref = i_ref;
}



void RampdownGenerate(struct Rampdown * rampdown, double * ref_time)
{
    double i_ref;

    // Advance reference time by one time step

    *ref_time += rampdown->time_step;

    // Calculate saturation factor based on the current from the previous iteration

    rampdown->sat_factor = SatFactor(&rampdown->load, rampdown->i_ref);

    // If rampdown has not yet started the final deceleration to I_MIN

    if(rampdown->limit_type != I_MIN_LIMITED)
    {
        double p_acc_lim_factor = 2.0 * rampdown->time_step * rampdown->time_step * rampdown->limits.p_acc;

        // Limit 2: V_MIN - using P_ACC limit to compute the V_ACC limit

        double delta_v         = rampdown->limits.v_min - rampdown->v_sat;
        double v_acc_lim       = (rampdown->limits.p_acc - rampdown->i_acc * rampdown->v_sat - 2.0 * rampdown->i_rate * rampdown->v_rate) / rampdown->i_ref;
        double v_sat_limit_min = delta_v;

        if((delta_v + p_acc_lim_factor / rampdown->i_ref) < 0.0)
        {
            // Apply the voltage acceleration limit for the arrival at V_MIN

            v_sat_limit_min = -rampdown->time_step * sqrt(-2.0 * v_acc_lim * delta_v);
        }

        i_ref = (rampdown->time_step * (rampdown->v_sat + v_sat_limit_min) + rampdown->load.henrys * rampdown->sat_factor * rampdown->i_ref) /
                (rampdown->load.ohms * rampdown->time_step + rampdown->load.henrys * rampdown->sat_factor);

        rampdown->limit_type = V_MIN_LIMITED;


        // Limits 3: Power limits: P_MIN, P_RATE, P_ACC

        double power_limit_rate = -rampdown->time_step * rampdown->limits.p_rate;

        double power_limit_acc  =  rampdown->time_step * (rampdown->power_rate - rampdown->time_step * rampdown->limits.p_acc);

        double power_limit_min  =  rampdown->limits.p_min - rampdown->power;

        if((power_limit_min + p_acc_lim_factor) < 0.0)
        {
            power_limit_min = -sqrt(-p_acc_lim_factor * power_limit_min);
        }

        double power_limit = power_limit_rate;

        if(power_limit < power_limit_acc)
        {
            power_limit = power_limit_acc;
        }

        if(power_limit < power_limit_min)
        {
            power_limit = power_limit_min;
        }

        // Try to convert minimum power limit into a minimum current reference limit - it's a quadratic function

        double a =  rampdown->load.ohms   * rampdown->time_step  + rampdown->load.henrys * rampdown->sat_factor;
        double b = -rampdown->load.henrys * rampdown->sat_factor * rampdown->i_ref;
        double c = -rampdown->time_step   * (rampdown->power + power_limit);

        double det = b*b - 4.0 * a * c;

        if(det >= 0.0)
        {
            // Real roots are possible so calculate minimum current reference limit

            double i_ref_limit_min = (-b + sqrt(det)) / (2.0 * a);

            if(i_ref < i_ref_limit_min)
            {
                // Clip current reference at the minimum limit

                i_ref = i_ref_limit_min;

                rampdown->limit_type = POWER_LIMITED;
            }
        }

        // Limit 1: I_MIN - entry conditions for final deceleration to I_MIN

        double i_rate0 = (i_ref - rampdown->i_ref) / rampdown->time_step;
        double dt      = (rampdown->limits.i_min - i_ref) / i_rate0;
        double jerk    = -i_rate0 / (dt * dt);
        double i1      = rampdown->limits.i_min + jerk * dt * dt * dt / 6.0;
        double v1      = i1 * rampdown->load.ohms + 0.5 * i_rate0 * rampdown->load.henrys;
        double v_rate1 = 0.5 * i_rate0 * rampdown->load.ohms + jerk * dt *rampdown->load.henrys;
        double p_rate1 = i1 * v_rate1 + 0.5 * i_rate0 * v1;
        double p_acc0  = jerk * i_ref * rampdown->load.henrys + 2.0 * i_rate0 * i_rate0 * rampdown->load.ohms;

        // If max V_RATE or P_RATE (at time dt) or max P_ACC (at time 0) now exceeds the limits

        if(v_rate1 > rampdown->limits.v_rate ||
           p_rate1 > rampdown->limits.p_rate ||
           p_acc0  > rampdown->limits.p_acc)
        {
            // Prepare to start the final deceleration to I_MIN

            rampdown->limit_type    = I_MIN_LIMITED;
            rampdown->i_min.i0      = i_ref;
            rampdown->i_min.t0      = *ref_time;
            rampdown->i_min.dt      = dt;
            rampdown->i_min.i_jerk  = jerk;
            rampdown->i_min.i_rate0 = i_rate0;
        }
    }
    else
    {
        // Limit 1: I_MIN - execute final deceleration to i_min

        double t = *ref_time - rampdown->i_min.t0;

        if(t < rampdown->i_min.dt)
        {
            i_ref = t * (rampdown->i_min.i_jerk * t * t / 6.0 + rampdown->i_min.i_rate0) + rampdown->i_min.i0;
        }
        else
        {
            t = (2.0 * rampdown->i_min.dt) - t;

            if(t > 0.0)
            {
                i_ref = rampdown->i_min.i_jerk * t * t * t / 6.0 + rampdown->limits.i_min;
            }
            else
            {
                i_ref = rampdown->limits.i_min;
            }
        }
    }

    // Calculate all the ramp down variables for this iteration

    double    v_sat      = Iref2Vsat (rampdown, i_ref);
    double    power      = v_sat * i_ref;
    double    i_rate     = (i_ref  - rampdown->i_ref ) / rampdown->time_step;
    rampdown->i_acc      = (i_rate - rampdown->i_rate) / rampdown->time_step;
    rampdown->power_rate = (power  - rampdown->power ) / rampdown->time_step;
    rampdown->v_rate     = (v_sat  - rampdown->v_sat ) / rampdown->time_step;
    rampdown->v_ref      = Iref2Vref(rampdown, i_ref);
    rampdown->v_sat      = v_sat;
    rampdown->i_ref      = i_ref;
    rampdown->i_rate     = i_rate;
    rampdown->power      = power;
}

// EOF
