# Convert FGC tables to CSV

scriptpath=../../../../tableconv

fgc2csv=$scriptpath/fgc2csv.awk
lsa2csv=$scriptpath/lsa2csv.awk

$fgc2csv HIRADMT2.fgc > QD-HIRADMT2.csv

$lsa2csv AWAKE_1Inj_FB60_FT900_Q20_2017_V2-QD.csv > QD-AWAKE.csv
$lsa2csv LHC_25ns_SLOW_Q20_2017_V1-QD.csv         > QD-LHC25NS.csv
$lsa2csv MD_26_L7200_Q20_2017_V1-QD.csv           > QD-MD26.csv

# EOF
