# Convert FGC tables to CSV

scriptpath=../../../../tableconv

fgc2csv=$scriptpath/fgc2csv.awk
lsa2csv=$scriptpath/lsa2csv.awk

$fgc2csv HIRADMT2.fgc > QF-HIRADMT2.csv

$lsa2csv AWAKE_1Inj_FB60_FT900_Q20_2017_V2-QF.csv > QF-AWAKE.csv
$lsa2csv LHC_25ns_SLOW_Q20_2017_V1-QF.csv         > QF-LHC25NS.csv
$lsa2csv MD_26_L7200_Q20_2017_V1-QF.csv           > QF-MD26.csv

# EOF
