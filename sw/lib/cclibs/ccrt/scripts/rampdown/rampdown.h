// rampdown.h

#ifndef RAMPDOWN_H
#define RAMPDOWN_H

// Constants

#define NUM_SAT_MODEL_SEGS              4           // Number of parameters for saturation model: 0.linear 1.parabola 2.linear 3.parabola

enum Limit_type
{
    NOT_LIMITED    ,
    I_MIN_LIMITED  ,
    V_MIN_LIMITED  ,
    POWER_LIMITED  ,
};

// Structures

struct Limits                                       // Limits for current, voltage and power
{
    double      i_min;                              // Minimum current level (A) - the end of the ramp down
    double      v_min;                              // Minimum voltage level (V)
    double      v_rate;                             // Maximum rate of change of voltage (V/s)
    double      p_min;                              // Minimum power level (W)
    double      p_rate;                             // Maximum rate of change of power (W/s)
    double      p_acc;                              // Maximum power acceleration (W/s^2)
};

struct Load                                         // Load parameters structure
{
    double       ohms;                              // Load resistance
    double       henrys;                            // Load nominal inductance

    struct                                          // Load saturation structure
    {
        double   henrys;                            // Inductance for i >> i_sat_end
        double   i_start;                           // Current measurement at start of saturation (linear)
        double   i_end;                             // Current measurement at end of saturation (linear)
        double   smoothing;                         // Saturation smoothing factor (0.0001 - 0.5)
        double   df_dI_linear;                      // df/dI during linear segment
        double   d2f_dI2_parabola;                  // d2f/dI2 during parabolic segment

        double   i[NUM_SAT_MODEL_SEGS];             // Currents at end of saturation segments (LPLP)
        double   f[NUM_SAT_MODEL_SEGS];             // Normalized sat_factor at end of saturation segments (LPLP)
        double   integrated_f[NUM_SAT_MODEL_SEGS];  // Integral of f(I).dI up to the end of the segment (LPLP)
    } sat;
};

struct I_min
{
    double                  dt;                     // Half the duration of final deceleration to i_min
    double                  t0;                     // Time of start of final deceleration to i_min
    double                  i0;                     // Current at start of final deceleration to i_min
    double                  i_rate0;                // I_rate at start of final deceleration to i_min
    double                  i_jerk;                 // I_jerk for final deceleration to i_min
};


struct Rampdown                                     // Rampdown parameters and variables
{
    double                  time_step;              // Time step for ramp down generation

    struct Load             load;                   // Load model

    struct Limits           limits;                 // Limits

    struct I_min            i_min;                  // Variables for final deceleration to i_min

    double                  i_ref;                  // Current reference
    double                  v_ref;                  // Voltage reference
    double                  v_sat;                  // Voltage reference after saturation compensation
    double                  power;                  // Power

    double                  i_acc;                  // Acceleration of current
    double                  i_rate;                 // Rate of change of current
    double                  v_rate;                 // Rate of change of voltage after saturation compensation
    double                  power_rate;             // Rate of change of power

    double                  sat_factor;             // Saturation factor

    enum Limit_type         limit_type;             // Type of limit that is active
};

#ifdef __cplusplus
extern "C" {
#endif

void RampdownInit(double timestep           ,
                  double load_ohms          ,
                  double load_henrys        ,
                  double load_henrys_sat    ,
                  double load_i_sat_start   ,
                  double load_i_sat_end     ,
                  double load_sat_smoothing ,
                  double limits_i_min       ,
                  double limits_v_min       ,
                  double limits_v_rate      ,
                  double limits_p_min       ,
                  double limits_p_rate      ,
                  double limits_p_acc       ,
                  struct Rampdown * rampdown);

void RampdownArm(struct Rampdown * rampdown, double i_ref, double i_rate);

void RampdownGenerate(struct Rampdown * rampdown, double * ref_time);

#ifdef __cplusplus
}
#endif

#endif

// EOF
