#!/bin/bash

# cct.sh - run ccrt with cct test files

# usage: cct.sh             - display usage message
#        cct.sh ALL         - run all tests for all converters
#        cct.sh ALL commit  - run and commit failed logs for all converters
#        cct.sh ALL COMMIT  - run and commit all logs for all converters
#        cct.sh {name}      - run all tests for the named converter
#        cct.sh {name} {test_path} [TEST|NOTEST|commit|COMMIT]       - run all tests from test_path for the named converter
#        cct.sh {name} {test_file} [TEST|NOTEST|commit|COMMIT|count] - run the test file for the named converter
#
# If log files are generated then by default, they will be tested against the reference set of files.
#
# If the TEST option is specified, then the ccrt test script(s) will not be run but the log files
# will be tested again.
#
# If the NOTEST option is specified, then the ccrt test script(s) will be run, but the log files
# will not be tested.
#
# If the commit option is specified, then the ccrt test script(s) will not be run and instead, the
# log files for the failed tests only will be copied to become the reference set.
#
# If the COMMIT option is specified, then the ccrt test script(s) will not be run and instead, the
# log files for all the test will be copied to become the reference set.
#
# If the count value is provided, it will allow a single test file to be repeated.

# The path to a ccrt test script must have this structure:
#
#   ccrt/converters/{converter_name}/tests/{test_path}/{test_name}.cct
#
# test_path is normally one level, but multiple sub-directories are allowed.


# Function:  test_evtlog tap_index ref_evtlog_file new_evtlog_file tap_file log_report_file

# where test_name is, for example, evtlog/test.cct

# This function uses diff to compare the new and reference event log files, after sorting and then
# removing the timestamp columns. This is necessary, since the timestamps are different for every execution.

test_evtlog() {

    # Put test name as the head in the log_report_file

    echo $2 >> $5

    # Sort and strip timestamps (in the first comma delimited column) from the new and reference event log files

    sort -n $2 | cut -d, -f 2- > events_ref
    sort -n $3 | cut -d, -f 2- > events_new

    # Compare the temporary new and reference event log files using diff

    if ! diff events_ref events_new >> $5
    then

        # Difference detected so write "not ok" to TAP file and a newline in the log_report_file

        echo "not ok $1 | $2" >> $4

        ((not_ok_counter++))

    else
        # Files are the same so report "ok" in the TAP file

        echo "ok $1 | $2" >> $log_tap_file
    fi

    # Write a newline to the log_report_file to separate the blocks

    echo >> $5

    # remove the temporary files

    rm events_*
}

# Function:  test_cct converter_name test_path [commit|COMMIT|TEST|NOTEST]

# where test_path is {test_group}/[{path}/]{test_name}.cct and the working directory
# is ccrt/converters/converter_name. path is optional.

# Runs ccrt with the test file identified in test_path (if TEST or commit|COMMIT are not specified).
# Directs ccrt stdout (contains test results in TAP format) to log in results folder.
# Run tap_to_junit.awk script to convert TAP format to JUnit XML format.
# Run test_log.awk to compare new log files against reference log files if NOTEST is not specified.
# Run tap_to_junit.awk script to convert TAP format to JUnit XML format.

test_ccrt() {

    local test_path=$2

    test_path="${test_path%.*}"

    local new_logs_path=../../logs/$1/$test_path
    local results_path=results/$test_path
    local commit_drive_file=results/$test_path/commit.sh
    local ref_logs_path=logs/$test_path

    ccrt_tap_file=$results_path/ccrt.tap
    log_tap_file=$results_path/test_log.tap
    log_report_file=$results_path/test_log.csv

#   echo "test_path:$test_path"
#   echo "results_path:$results_path"
#   echo "commit_drive_file:$commit_drive_file"
#   echo "new_logs_path:$new_logs_path"
#   echo "ref_logs_path:$ref_logs_path"
#   echo "ccrt_tap_file:$ccrt_tap_file"
#   echo "log_tap_file:$log_tap_file"
#   echo "log_report_file:$log_report_file"

    # create ref log directory if it doesn't already exist

    mkdir -p $ref_logs_path $results_path

    # If COMMIT specified then sync all existing log files to results logs folder

    if [ "$3" == "commit" ]; then

        if [ -f $commit_drive_file ]; then

            echo "Copying only failed test logs to become reference logs"
            source $commit_drive_file

        fi

    elif [ "$3" == "COMMIT" ]; then

        if [ -d $new_logs_path ]; then

            echo "Copying all failed test logs to become reference logs"
            rsync -av --delete --exclude ".*"  $new_logs_path/ $ref_logs_path

        fi

    else

        if [ "$3" == "TEST" ]; then

            echo "cct: Skipping ccrt $1 $2"

        else

            # Run ccrt and direct stdout to TAP results file
            # Leave stderr error to be visible on the console

            $ccrt $1 $2 $acceleration > $ccrt_tap_file

        fi

        # convert TAP results file into a junit file for gitlab CI

        if [ -f $ccrt_tap_file ]; then

            if ! $tap_to_junit $1 $ccrt_tap_file ;then

                let "result=result|1"
            fi

        else

            echo "cct: ccrt.tap is missing - please run ccrt"
            exit 1
        fi

        if [ "$3" == "NOTEST" ]; then

            echo "cct: Skipping test of log files for $1 $2"
            return 0

        fi

        # Declare local variables

        local new_logs=
        local ref_logs=
        local log_path=
        local log_file=

        # Get list of new log files

        if [ -d $new_logs_path ]; then

            new_logs=`find $new_logs_path -name "*.csv"`

        fi

        # Get list of reference log files and check if any new files are missing

        ref_logs=`find $ref_logs_path -name "*.csv"`

        for log_path in $ref_logs ; do

            # log_path will be logs/{test_path}/{logpath}/{logfile}.csv

            # To compare with new_logs, we need to remove the "logs/" prefix.

            log_file=${log_path#*/}

            if [[ ! $new_logs == *"$log_file"* ]]; then

                echo -e "cct:$alert new $log_file is missing $normal"
                let "result=result|1"
            fi
        done

        # Check if any reference files are missing

        local tap_index=0
        local not_ok_counter=0

        rm -f $log_tap_file $log_report_file

        for log_path in $new_logs ; do

            ((tap_index++))

            # log_path will be ../../logs/{converter_name}/{test_path}/{logpath}/{logfile}.csv

            # To compare with ref_logs, we need to remove the "../../logs/{converter_name}/" prefix.

            log_file=${log_path#*/*/*/*/}

            local file_base_name=`basename $log_file`

            if [[ $file_base_name =~ ^_EVENTS.* ]]; then

                test_evtlog "$tap_index" "logs/$log_file" "../../logs/$1/$log_file" "$log_tap_file" "$log_report_file"

            elif [[ $file_base_name =~ ^_.* ]]; then

                echo -e "cct: warning: $file_base_name files are not automatically compared"

            elif [[ ! $ref_logs == *"$log_file"* ]]; then

                echo -e "cct: warning: reference $log_file is missing"

            elif ! $test_log $tap_index "$log_file" "../../logs/$1" logs $results_path >> $log_tap_file 2>> $log_report_file; then

                ((not_ok_counter++))
            fi
        done

        echo "1..$tap_index" >> $log_tap_file

        $tap_to_junit $1 $log_tap_file $log_report_file

        if [ $not_ok_counter -gt 0 ]; then

            echo -e "$alert Failed log tests   : $not_ok_counter $normal"
            let "result=result|2"

        fi
    fi
}



# Function: test_converter converter_name [test_group_or_file [commit|COMMIT|TEST]]

# Run ccrt tests for one type of converter or commit previous test logs to become reference logs

# if test_group_or_file specifes a .cct file, then only run that test
# if test_group_or_file specifed a path, then run all tests in that path

test_converter() {

    # Check that the converter name is known

    if [ ! -d $1 ]; then

        echo -e "cct:$alert unknown converter: $1 $normal"
        exit 1

    fi

    # Check if the tests folder is present

    if [ ! -d $1/tests ]; then

        echo "cct: No tests folder for $1"
        return 0

    fi

    # Change directory to ccrt/converters/converter_name

    cd $1

    # Check if test_group_or_file is a cct file

    if [[ "$2" =~ \.cct$ ]]; then

        # test file specifed so run this file

        local test_file=$2
        local counter=1

        # if third argument is a number greater than 1 then use it as the loop counter

        if [[ $3 =~ $regexp_number && $3 > 1 ]]; then
            counter=$3
        fi

        # run test file multiple times if required

        result=0

        while [[ $counter > 0 && $result == 0 ]]
        do
            test_ccrt "$1" "${test_file#*/}" "$3"

            ((counter--))
        done

        # Report ccrt tap file if errors reported (bit 1 in result)

        if ((($result & 1) > 0)); then

            echo $ccrt_tap_file
            cat $ccrt_tap_file
        fi

        # Report test_log tap file and report file if errors reported (bit 2 in result)

        if ((($result & 2) > 0)); then

            echo $log_tap_file
            cat $log_tap_file
            echo $log_report_file
            cat $log_report_file
        fi

    elif [[ -z $2 || -d tests$2 ]]; then

        if [[ $3 =~ $regexp_number ]]; then

            echo "Repeat counter is only supported for individual test files"
            exit 1
        fi

        # test path is valid so find all cct files in tests/[test_path]

        for cct_path in `find tests$2 -name "*.cct"` ; do

            # Drop "tests/" from cct_path

            local test_file=${cct_path#*/}

            # Run ccrt with this test file

            test_ccrt "$1" "$test_file" "$3"
        done

    else

        # test path specified is invalid

        echo -e "cct:$alert invalid test path: $2 $normal"
        exit 1

    fi

    # Return directory to ccrt/converters

    cd ..
}



# Main part of the script starts here

# Prepare path to ccrt executable

ccrt="../../`uname -s`/`uname -m`/ccrt"

# Prepare paths to awk scripts

tap_to_junit="gawk -f ../../../scripts/tap_to_junit.awk"
test_log="gawk -f ../../scripts/test_log.awk"

# Prepare formatting

alert="\033[41m\033[1m"
normal="\033[0m"

# Regular expression to match an integer

regexp_number='^[0-9]+$'

# Set default ccrt acceleration

acceleration='10'

# Change directory to ccrt/converters

cd `dirname $0`
cd ../converters

# Act according to the arguments

if [ $# -eq 0 -o $# -gt 3 ]; then

    echo "usage: cct.sh ALL|{converter_name} [{test_path_or_file} [COMMIT|TEST|NOTEST|count]]"
    exit 1

elif [ "$1" == "ALL" ]; then

    # cct.sh ALL -> No other argments are required

    if [[ $# -gt 2 || ($# -eq 2 && "$2" != "commit" && "$2" != "COMMIT") ]]; then

        echo "usage: cct.sh ALL [commit|COMMIT]"
        exit 1

    fi

    # run all tests for all converters with reduced acceleration

    acceleration='5'

    for conv in *
    do
        test_converter "$conv" "/" "$2"
    done

elif [ $# -gt 1 ]; then

    # cct.sh converter_name test_path_or_file [commit|COMMIT|TEST|NOTEST|count] -> run test file or all test files in the path

    if [ $# -eq 3 ]; then

        if ! [[ $3 =~ $regexp_number ]]; then

            if [[ "$3" != "commit" && "$3" != "COMMIT" && "$3" != "TEST" && "$3" != "NOTEST" ]]; then

                echo "Use commit to commit only log files that fail a test and COMMIT to commit all log files."
                echo "Use TEST to test existing log files or NOTEST to just run ccrt"
                exit 1

            fi

        else

            if [[ $3 < 1 ]]; then

                echo "count must >= 1"
                exit 1

            fi

        fi

    fi

    test_converter "$1" "/$2" "$3"

else

    # cct.sh converter_name - > run all tests for converter_name

    test_converter "$1"

fi

exit $result

# EOF
