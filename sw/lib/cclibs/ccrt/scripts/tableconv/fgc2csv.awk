#!/usr/bin/awk -f
#
# fgc2csv.awk
#
# The script will convert an FGC REF.TABLE.FUNCTION response into CSV format
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of ccrt
#
# ccrt is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    FS = ","

    getline

    if(NF < 2)
    {
        print "Invalid input" > "/dev/stderr"
    }

    print "REF_TIME,REF"

    for(i = 1 ; i <= NF ; i++)
    {
        split($i,f,"|")

        printf "%.3f,%.7E\n", f[1], f[2] 
    }
}

# EOF
