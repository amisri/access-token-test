#!/usr/bin/awk -f
#
# lsa2fgc.awk
#
# The script will convert an SPS function from CSV format produced from LSA to the CSV
# format needed by the rampdown test program.
#
# LSA CSV Format: 
#
# {SIGNAME},{SIGNAME}
# X,Y
# time_in_milliseconds,reference
# time_in_milliseconds,reference
#  ...
# time_in_milliseconds,reference

# Rampdown CSV Format
#
# REF_TIME,REF
# time_in_seconds,reference
#  ...
# time_in_seconds,reference
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of ccrt
#
# ccrt is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    FS = ","
    line_num = 0

# Skip header

    getline 
    getline

# Read CSV format data: time_in_milliseconds,reference

    while(getline > 0 && NF > 0)
    {
        line_num++

        if(NF != 2)
        {
            print "Invalid input at line " line_num ": " $0 > "/dev/stderr"
        }

        time[line_num] = $1 * 0.001
        ref[line_num]  = $2
    }

# Write in FGC.TABLE.FUNCTION format: time_in_seconds|reference,...

    print "REF_TIME,REF"

    for(i = 1 ; i <= line_num ; i++)
    {
        printf "%.3f,%.7E\n", time[i], ref[i]
    }
}

# EOF
