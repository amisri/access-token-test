#!/usr/bin/awk -f
#
# test_log.awk
#
# The script will compare a new ccrt log file against a reference log file.
# It reports a summary of the results to stdout in TAP protocol.
# It reports the details of the results to stderr in csv format.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of ccrt
#
# ccrt is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    FS = ","

    tolerance      = 1.0E-4
    time_tolerance = 1.5E-6
    status         = "ok    "

    # Keys to ignore

     ignore_key["firstSampleTime"] = 1
     ignore_key["timeOrigin"] = 1
     ignore_key["numSignals"] = 1
     ignore_key["epoch"] = 1

    # Signals to ignore

    ignore_sig["WAIT_CMDS"] = 1

    # Get paths to new and reference log files

    if(ARGC != 6)
    {
        printf "usage: test_log.awk test_index test_file new_log_path ref_log_path results_path\n" > "/dev/stderr"
        exit 1
    }

    # Set up commit drive file name

    commit_drive_file = ARGV[5] "/commit.sh"

    if(ARGV[1] == 1)
    {
        printf "# Commit drive file for %s\n", ARGV[5] > commit_drive_file
    }

    # Read new and reference CSV log files

    ReadLogFile(ARGV[3] "/" ARGV[2], new_key_values, new_headers, new_sig_index, new_data)
    ReadLogFile(ARGV[4] "/" ARGV[2], ref_key_values, ref_headers, ref_sig_index, ref_data)

    # Compare logs

    CompareKeyValues(ARGV[2], new_key_values, ref_key_values)

    CompareSignals(ARGV[2], new_key_values, new_headers, new_sig_index, new_data, ref_key_values, ref_headers, ref_sig_index, ref_data)

    CompareHeaders(ARGV[2], ref_key_values["numSignals"],new_headers, ref_headers)

    # Write report in TAP format to stdout

    WriteTAP(status, ARGV[2])

    exit (status == "not ok")
}



function CompareKeyValues(test_name, new_key_values, ref_key_values)
{
    # Check that allow reference keys are in the new keys

    for(key in ref_key_values)
    {
#        print key, ref_key_values[key]

         if(new_key_values[key] == "")
         {
             NotOk(test_name " - header key " key " not found")
         }
    }

    # Check that all new keys are in the references and have the same values
    # except for the time-related keys

    for(key in new_key_values)
    {
#        print key, new_key_values[key]

         if(ref_key_values[key] == "")
         {
#             NotOk(test_name " - unexpected header key: " key)
         }

         if(ignore_key[key] != 1)
         {
             if(new_key_values[key] != ref_key_values[key])
             {
                 NotOk(test_name " - expected " key ":" ref_key_values[key] " but found " key ":" new_key_values[key])
             }
         }
    }

    # Check difference between timeOrigin and firstSampleTime unless it's a DISC log

    if(index(test_name,"DISC") == 0)
    {
        ref_delta_time = ref_key_values["timeOrigin"] - ref_key_values["firstSampleTime"]
        new_delta_time = new_key_values["timeOrigin"] - new_key_values["firstSampleTime"]
        diff_time      = ref_delta_time - new_delta_time

        if(diff_time > 1.5E-6 || diff_time < -1.5E-6)
        {
            printf "%s,%-11s,%-11s,%-11s,%-11s\n", \
                   "status","ref_delta_time","new_delta_time"," diff_time "," tolerance ", test_name > "/dev/stderr"
            printf "NOT OK,%14.6f,%14.6f,%11.1E,%11.1E\n", \
                    ref_delta_time, new_delta_time, diff_time, time_tolerance > "/dev/stderr"

            NotOk(test_name " - (ref_delta_time - new_delta_time) (" diff_time "s) exceeds +/-" time_tolerance * 1E6 "us " test_name)
        }
    }
}



function CompareSignals(test_name, new_key_values, new_headers, new_sig_index, new_data, ref_key_values, ref_headers, ref_sig_index, ref_data)
{
    num_bad_signal = 0
    num_samples    = new_key_values["numSamples"]

    for(sig in new_headers)
    {
        if(ignore_sig[sig] == 1)
        {
            continue
        }

        ref_index = ref_sig_index[sig]

        if(ref_index > 0)
        {
            min       =  1.0E30
            max       = -1.0E30
            diff_min  =  1.0E30
            diff_max  = -1.0E30
            diff_sum  =  0.0
            diff_sd   =  0.0
            new_index = new_sig_index[sig]

            for(i = 0 ; i < num_samples ; i++)
            {
                # Calculate min and max for reference signal

                ref_value = ref_data[ref_index,i]

                if(ref_value > max)
                {
                    max = ref_value
                }

                if(ref_value < min)
                {
                    min = ref_value
                }

                # Calculate min, max for the different between reference and new signal

                diff_value = ref_value - new_data[new_index,i]

                if(diff_value > diff_max)
                {
                    diff_max = diff_value
                }

                if(diff_value < diff_min)
                {
                    diff_min = diff_value
                }
            }

            # Use the range to compute the signal tolerance

            range = max - min

            if(range < tolerance)
            {
                sig_tolerance = tolerance
            }
            else
            {
                sig_tolerance = range * tolerance
            }

            # Check the differences between new and reference signal value and the tolerance

            num_invalid            = 0
            first_invalid_rel_time = 0

            for(i = 0 ; i < num_samples ; i++)
            {
                diff_value = ref_data[ref_index,i] - new_data[new_index,i]

                if(diff_value > sig_tolerance || diff_value < -sig_tolerance)
                {
                    if(num_invalid++ == 0)
                    {
                        # Record the time of first sample that failes the check

                        first_invalid_rel_time = new_data[1,i] - new_key_values["timeOrigin"]
                    }
                }
            }

            # Check the value

            if(num_invalid > 0)
            {
                if(num_bad_signal++ == 0)
                {
                    printf "\n%s\n%-23s,%6s,%-11s,%-11s,%-11s,%-11s,%-11s,%-11s,%-11s,%-11s\n", test_name, "signal", "status", \
                           "    min    ","    max    ","   range   ","  diff_min ", \
                           "  diff_max "," tolerance ","num_invalid"," first_time" > "/dev/stderr"
                }

                printf "%-23s,NOT OK,", sig > "/dev/stderr"

                # Report analysis in CSV format to stderr

                printf "% .4E,% .4E,% .4E,% .4E,% .4E,% .4E,%11u,%11.4f\n",
                    min, max, range, diff_min, diff_max, sig_tolerance, num_invalid, first_invalid_rel_time > "/dev/stderr"

                # 1. Don't report bad status for MEAS_HENRYS signal.
                # Because of the divide, this signal is incredibly sensitive to tiny changes to other signals and exceed the limits.

                # 2. Don't report bad status for DEBUG_SIG signal.
                # This can be assigned to anything by the developer.

                if(index(sig,"MEAS_HENRYS") == 0 && sig != "DEBUG_SIG")
                {
                    status = "not ok"
                }
            }
        }
        else
        {
            if(num_bad_signal++ == 0)
            {
                printf "\n%s\n%-23s,%6s\n", test_name, "signal", "status" > "/dev/stderr"
            }

            printf "%-23s,NOT OK,UNEXPECTED\n", sig > "/dev/stderr"
            status = "not ok"
        }
    }

    # Check time of the last sample

    ref_last_time = ref_data[1,num_samples-1] - ref_key_values["firstSampleTime"]
    new_last_time = new_data[1,num_samples-1] - new_key_values["firstSampleTime"]
    diff_time      = ref_last_time - new_last_time

    if(diff_time > time_tolerance || diff_time < -time_tolerance)
    {
        printf "%-23s,%s,%-11s,%-11s,%-11s,%-11s\n", \
               "sample_index","status","  ref_time ","  new_time "," diff_time "," tolerance ", test_name > "/dev/stderr"
        printf "%-23u,NOT OK,%11.6f,%11.6f,%11.1E,%11.1E\n", \
                num_samples-1, ref_last_time, new_last_time, diff_time, time_tolerance > "/dev/stderr"

        NotOk(test_name " - (ref_last_time - new_last_time) (" diff_time ") exceeds +/-1.5us")
    }
}



function CompareHeaders(test_name, num_signals, new_headers, ref_headers)
{
    not_ok = 0

    for(sig in ref_headers)
    {
        if(new_headers[sig] == "")
        {
            printf "%-23s,NOT OK,NOT FOUND\n", sig > "/dev/stderr"
            status = "not ok"
        }
    }

    for(sig in new_headers)
    {
        if(ref_headers[sig] != "" && new_headers[sig] != "" && new_headers[sig] != ref_headers[sig])
        {
#           printf "%-23s,NOT OK,HEADERS,REF:%s,NEW:%s\n", sig,ref_headers[sig],new_headers[sig] > "/dev/stderr"
#           status = "not ok"
        }
    }
}



function ReadLogFile(filename, key_values, headers, sig_index, data)
{
    linenum = 0

    ReadHeaderLine(filename, key_values, headers, sig_index)

    ReadData(filename, data, key_values)

    close(filename)
}



function ReadHeaderLine(filename, key_values, headers,  sig_index,  num_keys, num_fields, kv, f, i)
{
    # Read PowerSpy CSV header line

    linenum++

    if((getline < filename) != 1)
    {
        Error(filename, line_num, "Failed to read header line")
    }

    if(NF < 2)
    {
        Error(filename, line_num, "Header line has less than two columns")
    }

    var["NUM_SIGNALS"] = NF - 1

    # Extract key:value pairs from first field

    num_keys = split($1, kv, " ")

    if(num_keys != 8)
    {
#        Error(filename, line_num, "Unexpected number of header key:value pairs - " num_keys " instead of 8")
    }

    for(i = 1 ; i <= num_keys ; i++)
    {
        num_fields = split(kv[i], f, ":")

        if(num_fields != 2)
        {
            Error(filename, line_num, "Invalid header key:value - " kv[i])
        }

        key_values[f[1]] = f[2]
    }

    key_values["numSignals"] = NF-1

    # Extract signal headers (space separated fields)

    for(i = 2 ; i <= NF ; i++)
    {
        num_fields = split($i, hf, " ")

        headers[hf[1]] = $i

        sig_index[hf[1]] = i

#        printf "%u.  headers[%s] = '%s'\n", sig_index[hf[1]], hf[1], headers[hf[1]]
    }
}



function ReadData(filename, data, key_values,  num_columns, data_index, i)
{
    data_index = 0
    num_columns = key_values["numSignals"] + 1

    while((getline < filename) == 1)
    {
        linenum++

        if(NF != num_columns)
        {
            Error(filename, line_num, "Invalid number of columns - " NF " instead of " num_columns)
        }

        for(i = 1 ; i <= NF ; i++)
        {
            data[i,data_index] = $i
        }

        data_index++
    }

    key_values["numSamples"] = data_index
}



function Error(filename, line_num, message)
{
    printf "\n%s,%u,%s\n", filename, linenum, message > "/dev/stderr"

    NotOk("Failed to read " filename ":" linenum " : " message)
}



function NotOk(message)
{
    WriteTAP("not ok", message)

    exit 1
}



function WriteTAP(status, message)
{
    printf "%s %3u | %s\n", status, ARGV[1], message

    if(status == "not ok")
    {
        printf "\necho Copying %s/%s to %s/%s\n", ARGV[3], message, ARGV[4], message >> commit_drive_file
        printf "cp %s/%s %s/%s\n", ARGV[3], message, ARGV[4], message >> commit_drive_file
    }
}

# EOF
