# Test arming of PPPL for sub_sel 0, cyc_sel 5

GLOBAL SUB_SEL              0
GLOBAL HIDE_ERRORS          ENABLED

# Set current limits

LIMITS I_POS                4000.0
LIMITS I_NEG                -4000.0
LIMITS I_RATE               50.0

# 1. Arm a PPPL - this should succeed

PPPL ACCELERATION1(5)            250,     -250,      -250
PPPL ACCELERATION2(5)              0,        0,         0
PPPL ACCELERATION3(5)            -20,      250,       250
PPPL RATE2(5)                     40,      -40,       -40
PPPL RATE4(5)                      0,        0,         0
PPPL REF4(5)                     100,       50,         0
PPPL DURATION4(5)                  1,        2,       0.1

MODE REG_MODE_CYC           CURRENT
REF FG_TYPE(5)              PPPL

ARM 5

REF PLAY(5)             =   ENABLED
REF FG_TYPE(5)          =   PPPL
REF DYN_ECO_END_TIME(5) =   0.000000E+00
REF ARMED_REG_MODE(5)   =   CURRENT
REF ARMED_FG_TYPE(5)    =   PPPL
REF PARS(5)             =   FG_TYPE  INITIAL_REF  ACCELERATION1  ACCELERATION2  ACCELERATION3  RATE2  RATE4  REF4  DURATION4
REF POLARITY(5)         =   POSITIVE
REF LIMITS_INVERTED(5)  =   FALSE
REF START_TIME(5)       ~   0.00000000E+00
REF END_TIME(5)         ~   9.50000000E+00
REF DURATION(5)         ~   9.50000000E+00
REF INITIAL_REF(5)      ~   0.00000000E+00
REF MIN_REF(5)          ~   0.00000000E+00
REF MAX_REF(5)          ~   1.00000000E+02
REF FINAL_REF(5)        ~   0.00000000E+00
REF FINAL_RATE(5)       ~   0.00000000E+00
REF MIN_LIMIT(5)        ~  -4.00800415E+03
REF MAX_LIMIT(5)        ~   4.00800415E+03
REF RATE_LIMIT(5)       ~   5.01000519E+01

PPPL INITIAL_REF(5)     =          0
PPPL ACCELERATION1(5)   =        250,     -250,      -250
PPPL ACCELERATION2(5)   =          0,        0,         0
PPPL ACCELERATION3(5)   =        -20,      250,       250
PPPL RATE2(5)           =         40,      -40,       -40
PPPL RATE4(5)           =          0,        0,         0
PPPL REF4(5)            =        100,       50,         0
PPPL DURATION4(5)       ~          1,        2,       0.1

FGERROR ERRNO(5)   =  OK
FGERROR FG_TYPE(5) =  PPPL
FGERROR INDEX(5)   =  0
FGERROR DATA(5)    =  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00

# 2. Try to arm a PPPL that fails with a PPPL already armed

PPPL INITIAL_REF(5)                1
PPPL ACCELERATION1(5)            255,     -300
PPPL ACCELERATION2(5)            -10,      -50
PPPL ACCELERATION3(5)            -50,       50
PPPL RATE2(5)                     30,      -50
PPPL RATE4(5)                      2,        3
PPPL REF4(5)                     150,      150
PPPL DURATION4(5)                  2,        3

MODE REG_MODE_CYC    VOLTAGE
REF FG_TYPE(5)       PPPL

ARM 5

REF PLAY(5)             =   ENABLED
REF FG_TYPE(5)          =   PPPL
REF DYN_ECO_END_TIME(5) =   0.000000E+00
REF ARMED_REG_MODE(5)   =   CURRENT
REF ARMED_FG_TYPE(5)    =   PPPL
REF PARS(5)             = 
REF POLARITY(5)         =   POSITIVE
REF LIMITS_INVERTED(5)  =   FALSE
REF START_TIME(5)       ~   0.00000000E+00
REF END_TIME(5)         ~   9.50000000E+00
REF DURATION(5)         ~   9.50000000E+00
REF INITIAL_REF(5)      ~   0.00000000E+00
REF MIN_REF(5)          ~   0.00000000E+00
REF MAX_REF(5)          ~   1.00000000E+02
REF FINAL_REF(5)        ~   0.00000000E+00
REF FINAL_RATE(5)       ~   0.00000000E+00
REF MIN_LIMIT(5)        ~  -4.00800415E+03
REF MAX_LIMIT(5)        ~   4.00800415E+03
REF RATE_LIMIT(5)       ~   5.01000519E+01

PPPL INITIAL_REF(5)     =          1
PPPL ACCELERATION1(5)   =        255,     -300
PPPL ACCELERATION2(5)   =        -10,      -50
PPPL ACCELERATION3(5)   =        -50,       50
PPPL RATE2(5)           =         30,      -50
PPPL RATE4(5)           =          2,        3
PPPL REF4(5)            =        150,      150
PPPL DURATION4(5)       =          2,        3

FGERROR ERRNO(5)   =  BAD_PARAMETER
FGERROR FG_TYPE(5) =  PPPL
FGERROR INDEX(5)   =  200
FGERROR DATA(5)    ~ -2.55688232E+03  2.76470590E+00  1.50000000E+02  1.17647059E-01

# RESTORE should recover the armed PPPL parameters

RESTORE                 5
REF PARS(5)             =   INITIAL_REF  ACCELERATION1  ACCELERATION2  ACCELERATION3  RATE2  RATE4  REF4  DURATION4

PPPL INITIAL_REF(5)     =          0
PPPL ACCELERATION1(5)   =        250,     -250,      -250
PPPL ACCELERATION2(5)   =          0,        0,         0
PPPL ACCELERATION3(5)   =        -20,      250,       250
PPPL RATE2(5)           =         40,      -40,       -40
PPPL RATE4(5)           =          0,        0,         0
PPPL REF4(5)            =        100,       50,         0
PPPL DURATION4(5)       ~          1,        2,       0.1

# 3. Arm a default PRBS

MODE REG_MODE_CYC    CURRENT
REF FG_TYPE(5)       PRBS

ARM 5

REF PLAY(5)             =   ENABLED
REF FG_TYPE(5)          =   PRBS
REF DYN_ECO_END_TIME(5) =   0.000000E+00
REF ARMED_REG_MODE(5)   =   CURRENT
REF ARMED_FG_TYPE(5)    =   PRBS
REF PARS(5)             =   FG_TYPE  INITIAL_REF  AMPLITUDE_PP  PERIOD_ITERS  NUM_SEQUENCES  K
REF POLARITY(5)         =   BOTH
REF LIMITS_INVERTED(5)  =   FALSE
REF START_TIME(5)       ~   0.00000000E+00
REF END_TIME(5)         ~   6.30000055E-01
REF DURATION(5)         ~   6.30000055E-01
REF INITIAL_REF(5)      ~   0.00000000E+00
REF MIN_REF(5)          ~  -5.00000000E-01
REF MAX_REF(5)          ~   5.00000000E-01
REF FINAL_REF(5)        ~   0.00000000E+00
REF FINAL_RATE(5)       ~   0.00000000E+00
REF MIN_LIMIT(5)        ~  -4.00800415E+03
REF MAX_LIMIT(5)        ~   4.00800415E+03
REF RATE_LIMIT(5)       ~   5.01000519E+01

FGERROR ERRNO(5)    =  OK
FGERROR FG_TYPE(5)  =  PRBS
FGERROR INDEX(5)    =  0
FGERROR DATA(5)     =  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00

# 4. Try to arm the PPPL with bad parameters with a PRBS already armed

PPPL ACCELERATION1(5)            255,     -300
PPPL ACCELERATION2(5)            -10,      -50
PPPL ACCELERATION3(5)            -50,       50
PPPL RATE2(5)                     30,      -50
PPPL RATE4(5)                      2,        3
PPPL REF4(5)                     150,      150
PPPL DURATION4(5)                  2,        3

MODE REG_MODE_CYC          VOLTAGE
REF FG_TYPE(5)             PPPL

ARM 5

REF PLAY(5)             =   ENABLED
REF FG_TYPE(5)          =   PPPL
REF DYN_ECO_END_TIME(5) =   0.000000E+00
REF ARMED_REG_MODE(5)   =   CURRENT
REF ARMED_FG_TYPE(5)    =   PRBS
REF PARS(5)             =
REF POLARITY(5)         =   BOTH
REF LIMITS_INVERTED(5)  =   FALSE
REF START_TIME(5)       ~   0.00000000E+00
REF END_TIME(5)         ~   6.30000055E-01
REF DURATION(5)         ~   6.30000055E-01
REF INITIAL_REF(5)      ~   0.00000000E+00
REF MIN_REF(5)          ~  -5.00000000E-01
REF MAX_REF(5)          ~   5.00000000E-01
REF FINAL_REF(5)        ~   0.00000000E+00
REF FINAL_RATE(5)       ~   0.00000000E+00
REF MIN_LIMIT(5)        ~  -4.00800415E+03
REF MAX_LIMIT(5)        ~   4.00800415E+03
REF RATE_LIMIT(5)       ~   5.01000519E+01

PPPL ACCELERATION1(5)   =        255,     -300
PPPL ACCELERATION2(5)   =        -10,      -50
PPPL ACCELERATION3(5)   =        -50,       50
PPPL RATE2(5)           =         30,      -50
PPPL RATE4(5)           =          2,        3
PPPL REF4(5)            =        150,      150
PPPL DURATION4(5)       =          2,        3

FGERROR ERRNO(5)   =  BAD_PARAMETER
FGERROR FG_TYPE(5) =  PPPL
FGERROR INDEX(5)   =  200
FGERROR DATA(5)    ~ -2.58188232E+03  1.76470590E+00  1.50000000E+02  1.17647059E-01

# RESTORE should not recover any parameters

RESTORE                 5
REF PARS(5)             =   FG_TYPE
REF FG_TYPE(5)          =   PRBS

PPPL ACCELERATION1(5)   =        255,     -300
PPPL ACCELERATION2(5)   =        -10,      -50
PPPL ACCELERATION3(5)   =        -50,       50
PPPL RATE2(5)           =         30,      -50
PPPL RATE4(5)           =          2,        3
PPPL REF4(5)            =        150,      150
PPPL DURATION4(5)       =          2,        3

# EOF
