# CCRT - libfg - RAMP function limits tests

# Purpose is to test the limits of the ramp function 

GLOBAL HIDE_ERRORS  ENABLED

# Disasble CYC_SEL for reference settings to allow cyc_sel 0 to be armed from any state

MODE   REF_CYC_SEL  DISABLED

# Use current as reference to also check standby

MODE REG_MODE     CURRENT
MODE REG_MODE_CYC CURRENT


################################################################################
#                       UNIPOLAR LOAD - NO POL SWITCH                          #
################################################################################


# Set load select to 1Q

LOAD SELECT 2
LOAD SELECT = 2

WAIT 1

# No polswitch

POLSWITCH TIMEOUT   0

# Check limits

LIMITS I_POS[2]     ~   1.000000E+01
LIMITS I_STANDBY[2] ~   1.000000E+00
LIMITS I_NEG[2]     ~   0.000000E+00

# Select RAMP function

REF FG_TYPE RAMP
REF FG_TYPE = RAMP

# Default initial values are for bipolar, set valid ones

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      9.000000E+00
ARM

REF ARMED_REG_MODE   =   CURRENT
REF ARMED_FG_TYPE    =   RAMP
REF POLARITY         =   POSITIVE
REF LIMITS_INVERTED  =   FALSE
REF INITIAL_REF      ~   2.000000E+00
REF MIN_REF          ~   2.000000E+00
REF MAX_REF          ~   9.000000E+00
REF FINAL_REF        ~   9.000000E+00
REF FINAL_RATE       ~   0.000000E+00
REF MIN_LIMIT        ~   0.999000E+00
REF MAX_LIMIT        ~   1.00200100E+01
REF RATE_LIMIT       ~   1.00200100E+01

FGERROR ERRNO = OK
FGERROR FG_TYPE = RAMP

# Check initial ref

# From standby to a positive value

RAMP INITIAL_REF    1.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   1.000000E+00
REF MIN_REF     ~   1.000000E+00

# From between zero and standby to a positive value

RAMP INITIAL_REF    0.500000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.500000E+00
FGERROR FG_TYPE =   RAMP

# From zero to a positive value

RAMP INITIAL_REF    0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.000000E+00
FGERROR FG_TYPE =   RAMP 

# From a negative value to positive value

RAMP INITIAL_REF    -1.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   -1.000000E+00
FGERROR FG_TYPE =   RAMP

# From above the positive limit to a positive value

RAMP INITIAL_REF    1.100000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   1.100000E+01
FGERROR FG_TYPE =   RAMP

# Check final ref

# From a positive value to the positive limit

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      1.000000E+01
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF FINAL_REF   ~   1.000000E+01
REF MAX_REF     ~   1.000000E+01

# From a positive value to above the positive limit

RAMP FINAL_REF      1.100000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   1.100000E+01
FGERROR FG_TYPE =   RAMP 

# From a positive value to between zero and standby

RAMP FINAL_REF      0.500000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.500000E+00
FGERROR FG_TYPE =   RAMP

# From a positive value to zero

RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.000000E+00
FGERROR FG_TYPE =   RAMP

# From a positive value to a negative value 

RAMP FINAL_REF      -1.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   -1.000000E+00
FGERROR FG_TYPE =   RAMP

# From zero to a negative value

RAMP INITIAL_REF    0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.000000E+00
FGERROR FG_TYPE =   RAMP

# From zero to zero

RAMP INITIAL_REF    0.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.000000E+00
FGERROR FG_TYPE =   RAMP


################################################################################
#                       UNIPOLAR LOAD - MANUAL POL SWITCH                      #
################################################################################


POLSWITCH SIMULATE      ENABLED
POLSWITCH AUTOMATIC     DISABLED
POLSWITCH TIMEOUT       2

SYNC 0.1 POLSWITCH STATUS ! IS_AUTOMATIC
POLSWITCH STATUS          = IS_SIMULATED

# Positive Polarity

POLSWITCH MODE_USER POSITIVE

# Wait for more than timeout + sim_delay in order to discover polswitch state

WAIT 3

POLSWITCH STATE          = POSITIVE
POLSWITCH STATUS         ! IS_NEGATIVE

# From a positive value to another positive value

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      9.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   2.000000E+00
REF FINAL_REF   ~   9.000000E+00

# From a negative value to another negative value

RAMP INITIAL_REF    -4.000000E+00
RAMP FINAL_REF      -2.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR FG_TYPE =   RAMP

# From zero to zero

RAMP INITIAL_REF    0.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   0.000000E+00
FGERROR FG_TYPE =   RAMP


# Negative Polarity

# Switch to simulation and change MODE

POLSWITCH MODE_USER NEGATIVE

# Wait for more than timeout + movement_time in order to discover polswitch state

WAIT 3

POLSWITCH STATE  = NEGATIVE
POLSWITCH STATUS = IS_NEGATIVE

# From a positive value to another positive value

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      9.000000E+00
ARM
FGERROR ERRNO   = OUT_OF_LIMITS
FGERROR FG_TYPE = RAMP

# From a negative value  to another negative value

RAMP INITIAL_REF    -4.000000E+00
RAMP FINAL_REF      -2.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   -4.000000E+00
REF FINAL_REF   ~   -2.000000E+00


################################################################################
#                       UNIPOLAR LOAD - AUTO POL SWITCH                        #
################################################################################


# Make sure simulation is enabled

POLSWITCH AUTOMATIC   ENABLED
POLSWITCH SIMULATE    ENABLED
SYNC 0.1 POLSWITCH STATUS = IS_AUTOMATIC IS_SIMULATED

# From a positive value to another positive value
# In auto, polswitch mode negative and inverted limits are ignored

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      9.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   2.000000E+00
REF FINAL_REF   ~   9.000000E+00

# From a positive value to zero

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR FG_TYPE =   RAMP
FGERROR DATA[0] =   0.000000E+00


# From a positive value to a negative value

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF     -2.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS

# From a positive value to between zero and standby

RAMP INITIAL_REF    2.000000E+00
RAMP FINAL_REF      0.500000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR FG_TYPE =   RAMP
FGERROR DATA[0] =   0.500000E+00

# From a negative value to another negative value

RAMP INITIAL_REF    -4.000000E+00
RAMP FINAL_REF      -2.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   -4.000000E+00
REF FINAL_REF   ~   -2.000000E+00

# From zero to zero

RAMP INITIAL_REF    0.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR FG_TYPE =   RAMP
FGERROR DATA[0] =   0.000000E+00


################################################################################
#                       BIPOLAR LOAD - NO POL SWITCH                           #
################################################################################


# Set load select to 4Q

LOAD SELECT 0
LOAD SELECT = 0

# No polswitch

POLSWITCH TIMEOUT  0

# Check limits

LIMITS I_POS[0]      ~   1.000000E+01
LIMITS I_STANDBY[0]  ~   0.000000E+00
LIMITS I_NEG[0]      ~  -1.000000E+01

RAMP INITIAL_REF    0.000000E+00
RAMP FINAL_REF      0.000000E+00

# Select RAMP function

REF FG_TYPE RAMP
REF FG_TYPE = RAMP

ARM

REF ARMED_REG_MODE   =   CURRENT
REF ARMED_FG_TYPE    =   RAMP
REF POLARITY         =   ZERO
REF LIMITS_INVERTED  =   FALSE
REF INITIAL_REF      ~   0.000000E+00
REF MIN_REF          ~   0.000000E+00
REF MAX_REF          ~   0.000000E+00
REF FINAL_REF        ~   0.000000E+00
REF FINAL_RATE       ~   0.000000E+00
REF MIN_LIMIT        ~  -1.00200100E+01
REF MAX_LIMIT        ~   1.00200100E+01
REF RATE_LIMIT       ~   1.00200100E+01

FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP

# Check initial ref

# From zero to a positive value

RAMP FINAL_REF      1.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF FINAL_REF   ~   1.000000E+00
REF MAX_REF     ~   1.000000E+00

# From a negative value to a positive value

RAMP INITIAL_REF    -1.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   -1.000000E+00
REF MIN_REF     ~   -1.000000E+00

# From a negative value to a positive value

RAMP INITIAL_REF    -1.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   -1.000000E+00
REF MIN_REF     ~   -1.000000E+00

# From the negative limit to the positive limit

RAMP INITIAL_REF    -1.00200100E+01
RAMP FINAL_REF      1.00200100E+01
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   -1.00200100E+01
REF FINAL_REF   ~    1.00200100E+01
REF MIN_REF     ~   -1.00200100E+01
REF MAX_REF     ~    1.00200100E+01

# From below the negative limit to the positive limit

RAMP INITIAL_REF    -1.10000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   -1.10000E+01
FGERROR FG_TYPE =   RAMP

# Check final ref

# From a positive value to zero

RAMP INITIAL_REF    1.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF FINAL_REF   ~   0.000000E+00

# From a positive value to a negative value

RAMP FINAL_REF      -1.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF FINAL_REF   =   -1.000000E+00

# From a positive value to below the negative limit

RAMP FINAL_REF      -1.100000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   -1.100000E+01
FGERROR FG_TYPE =   RAMP

# From a positive value to above the positive limit

RAMP FINAL_REF      1.100000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   1.100000E+01
FGERROR FG_TYPE =   RAMP

# From below the negative limit to above the positive limit

RAMP INITIAL_REF    -1.100000E+01
RAMP FINAL_REF      1.100000E+01
ARM
FGERROR ERRNO   =   OUT_OF_LIMITS
FGERROR DATA[0] =   -1.100000E+01
FGERROR FG_TYPE =   RAMP

# From zero to zero

RAMP INITIAL_REF    0.000000E+00
RAMP FINAL_REF      0.000000E+00
ARM
FGERROR ERRNO   =   OK
FGERROR FG_TYPE =   RAMP
REF INITIAL_REF ~   0.000000E+00
REF FINAL_REF   ~   0.000000E+00

# EOF
