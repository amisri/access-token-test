# CCRT - RST - Test RST coefficient calculations for current regulation

# Test libreg simulation of voltage source and load

MODE SIM_MEAS                ENABLED

# Logging parameters

LOGMENU I_MEAS_MPX           ENABLED
LOGMENU I_REG                ENABLED                          
LOGMPX  I_MEAS_MPX           I_REF_DELAYED  I_ERR
LOG DURATION_MS              400

# Limits parameters

LIMITS B_POS                12.0
LIMITS B_NEG               -12.0
LIMITS B_RATE             1000.0

LIMITS I_POS                12.0
LIMITS I_STANDBY             0.0
LIMITS I_NEG               -12.0
LIMITS I_RATE             1000.0

LIMITS V_POS                10.0
LIMITS V_NEG               -10.0
LIMITS V_RATE            10000.0

# Voltage source parameters

VS GAIN                      1.0
VS SIM_BANDWIDTH           200.0
VS SIM_TAU_ZERO              0.0
VS SIM_Z                     0.9

# Load parameters

LOAD OHMS_SER                0.0       0.0
LOAD OHMS_PAR                1.0E8     1.0E8
LOAD OHMS_MAG                0.1       0.1
LOAD HENRYS                  0.08      0.08
LOAD HENRYS_SAT              0.04      0.04
LOAD I_SAT_START             4.0       4.0
LOAD I_SAT_END               9.0       9.0
LOAD SAT_SMOOTHING           0.2       0.2
LOAD GAUSS_PER_AMP           1.0       1.0
LOAD SIM_TC_ERROR            0.0  

LOAD TEST_SELECT             1

# Measurement parameters

MEAS I_DELAY_ITERS           1.0
MEAS V_DELAY_ITERS           1.0

# TABLE parameters

TABLE FUNCTION               0.0|0  0.2|10  0.4|10  0.6|-10  0.8|-10  1.0|0

# Current regulator

MODE REG_MODE               CURRENT

VS ACT_DELAY_ITERS          0.2

IREG PERIOD_ITERS           10
IREG EXTERNAL_ALG           ENABLED
IREG EXT_OP_R               1.20426428E+00 -1.77490056E+00  7.02968299E-01  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00
IREG EXT_OP_S               9.60403755E-02 -1.63899109E-01  3.96770872E-02  2.81816423E-02  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00
IREG EXT_OP_T               1.00000000E+00 -1.78331351E+00  1.20025504E+00 -2.84609526E-01  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00  0.00000000E+00

# Use unfiltered measurement so that REF_DELAY_PERIODS will equal the TRACK_DELAY

IREG EXT_MEAS_SELECT        UNFILTERED

# Test track delay 0

IREG EXT_TRACK_DELAY_PERIODS 0.0
READ                        tests/rst/sequence.run
WRITELOGS                   td_0
IREG OP_REF_DELAY_PERIODS   ~ 0.1


# Test track delay 0.5

IREG EXT_TRACK_DELAY_PERIODS 0.5
READ                        tests/rst/sequence.run
WRITELOGS                   td_0.5
IREG OP_REF_DELAY_PERIODS   ~ 0.5

# Test track delay 1

IREG EXT_TRACK_DELAY_PERIODS 1.0
READ                        tests/rst/sequence.run
WRITELOGS                   td_1
IREG OP_REF_DELAY_PERIODS   ~ 1.0

# Test track delay 2.5

IREG EXT_TRACK_DELAY_PERIODS 2.5
READ                        tests/rst/sequence.run
WRITELOGS                   td_2.5
IREG OP_REF_DELAY_PERIODS   ~ 2.5


# Test track delay 14.0

IREG EXT_TRACK_DELAY_PERIODS 14.5
READ                        tests/rst/sequence.run
WRITELOGS                   td_14.5
IREG OP_REF_DELAY_PERIODS   ~ 14.5

# Test track delay 15

IREG EXT_TRACK_DELAY_PERIODS 15
READ                        tests/rst/sequence.run
WRITELOGS                   td_15
IREG OP_REF_DELAY_PERIODS   ~ 15.0

# Test track delay 15.5

IREG EXT_TRACK_DELAY_PERIODS 15.5
READ                        tests/rst/sequence.run
WRITELOGS                   td_15.5
IREG OP_REF_DELAY_PERIODS   ~ 15.0

# EOF
