# Test ILC with MAGCYCLE pre-function
# The pre-function learning disturbs the ILC learning and
# makes the convergence longer.

# Define logging

LOG     DURATION_MS  25500
LOGMENU ILC_CYC      ENABLED
LOGMPX  I_REG_MPX    I_REF_ADV  I_MEAS_REG  V_REF  I_ERR_ILC

# Actuation delay to define the RST algoritm and pure delay

VS ACT_DELAY_ITERS      1.0

# Define ILC

ILC START_TIME     -0.5
ILC L_ORDER          1
#ILC Q_FUNC           0.5
ILC LOG_CYC_SEL      2
ILC STOP_REF       -1E6

# PRE/POST functions

MODE PRE_FUNC               MAGCYCLE
MODE POST_FUNC              MINRMS
DEFAULT I_ACCELERATION      1E2
DEFAULT I_DECELERATION      1E2
DEFAULT I_LINEAR_RATE       0.0
DEFAULT I_PRE_FUNC_MAX      1.0
DEFAULT I_PRE_FUNC_MIN      0.0
DEFAULT I_MINRMS            0.0
#LIMITS I_RATE               100
LIMITS V_POS                7
DEFAULT PLATEAU_DURATION    0.02 0.02
GLOBAL EVENT_ADVANCE_MS      1000

SYNC 1.5 GLOBAL SUPER_CYCLE 201

# Run the cycling sequence

# Define load including magnet saturation and load TC error

LOAD SIM_TC_ERR         0.2
LOAD HENRYS_SAT         0.5
LOAD I_SAT_START        0.5
LOAD I_SAT_END          1.5
LOAD SAT_SMOOTHING      0.2

# Define a low performance regulator

IREG INT_MEAS_SELECT    UNFILTERED
IREG INT_AUXPOLE1_HZ    10
IREG INT_AUXPOLES2_HZ   10

# Enable function

MODE ILC                    ENABLED

# Arm short TABLE

TABLE FUNCTION(2)             0|0.5  0.2|0.7  0.4|0.2
#REF FG_TYPE(2)                TABLE
MODE REG_MODE                 CURRENT
MODE REG_MODE_CYC             CURRENT
ARM                           2

# Switch on and wait till cycling has started

RESET

MODE REF              CYCLING

SYNC 2 MODE PC        ON

WAIT 5 STATE REF = CYCLING

# Wait and then prepare to log

SYNC 25.5 LOG TIME_ORIGIN T

# Write ILC_CYC log first and then the other logs - this is necessary to ensure that the ILC_CYC long length is consistent

WRITELOGS

LOGMENU ILC_CYC     DISABLED
#LOGMENU I_REG       ENABLED
LOGMENU I_REG_MPX   ENABLED
#LOGMENU I_MEAS_MPX  ENABLED
#LOGMENU ILC_FLAGS   ENABLED
#LOGMENU FLAGS       ENABLED
#LOGMENU DEBUG       ENABLED

WRITELOGS

# EOF
