# The purpose of this file is to test DYNAMIC ECONOMY.
# The ECONOMY state is always entered from CYCLING.
# Reference function is always posititve.

# Configure supercycle: 2 cycles of 2 seconds

GLOBAL SUB_SEL               0
GLOBAL BASIC_PERIOD_MS       1000

SYNC 1.0  RTREF  FREQUENCY           3.0
SYNC 0.2  GLOBAL EVENT_ADVANCE_MS    900
SYNC 0.2  GLOBAL SUPER_CYCLE         202 102

RTREF AMPLITUDE                      0.05
RTREF OFFSET                         0.25
RTREF SAMPLING_ITERS                 0

# Define logs to record

LOGMENU DEBUG         ENABLED
LOGMENU I_REG         ENABLED
LOGMENU FLAGS         ENABLED
LOGMENU WARNINGS      ENABLED
#LOGMENU V_REF_DBG     ENABLED

# Configure modes

MODE   RT_REF                 ENABLED
MODE   POST_FUNC              MINRMS

# The test has to work with these values

DEFAULT V_ACCELERATION  3.0,   3.0,   3.0,   7.0
DEFAULT V_DECELERATION  7.0,   7.0,   7.0,   3.0
DEFAULT V_LINEAR_RATE   100.0, 100.0, 100.0, 100.0

DEFAULT I_ACCELERATION 1.0, 200.0, 1.0, 2.0
DEFAULT I_DECELERATION 2.0, 150.0, 2.0, 1.0
DEFAULT I_LINEAR_RATE  5.0, 10.0, 5.0, 5.0

LIMITS I_RATE          100.0 100.0 100.0 100.0
LIMITS V_RATE          1000.0

# Disable low, zero and regulation error flags to simplify viewing the logs

# Set load select to 2Q

LOAD    SELECT                    1

LIMITS B_LOW[1]                   1.000000E+02
LIMITS B_ZERO[1]                  0.000000E+00
LIMITS I_LOW[1]                   1.000000E+02
LIMITS I_ZERO[1]                  0.000000E+00
LIMITS I_ERR_WARNING[1]           0.000000E+00
LIMITS I_ERR_FAULT[1]             0.000000E+00

# Adjust I_STANDBY and MINRMS (DYN_ECO level) to be the same

LIMITS  I_STANDBY[1]              0.8
LIMITS  V_RATE                    2000.0
DEFAULT I_MINRMS[1]               0.8

# Set regulation mode to CURRENT

MODE REG_MODE                   CURRENT
MODE REG_MODE_CYC               CURRENT

# Configure function for user 1 : 1 -> 4 -> 1 : 1 seconds long

TABLE FUNCTION(1) 0.0|1, 0.2|4, 0.8|4, 1.0|1

REF PLAY(1)                     ENABLED
REF FG_TYPE(1)                  TABLE
REF DYN_ECO_END_TIME(1)         0.9

ARM 1

# Configure function for user 2 : 1 -> 3 -> 1 : 1.5 seconds long

TABLE FUNCTION(2) 0.0|1, 0.2|3, 1.3|3, 1.5|1

REF PLAY(2)                     ENABLED
REF FG_TYPE(2)                  TABLE
REF DYN_ECO_END_TIME(2)         1.4

ARM 2

# MODE ECONOMY is DISABLED - check that ECONOMY DYNAMIC has no effect

MODE ECONOMY              DISABLED
MODE REF                  CYCLING

# Turn on the converter in VOLTAGE regulation mode - wait in TO_CYCLING

SYNC 2.5 MODE PC ON

SYNC 6.15 ECONOMY DYNAMIC  ENABLED
STATE REF = CYCLING

SYNC 0.5 ECONOMY DYNAMIC = ENABLED
STATE REF = CYCLING

SYNC 0.5 STATE REF = TO_CYCLING
ECONOMY DYNAMIC = ENABLED

SYNC 0.2 STATE REF = CYCLING
ECONOMY DYNAMIC = DISABLED

SYNC 0.2 LOG TIME_ORIGIN    T

CYCSTATUS COUNTER    = 1
CYCSTATUS COUNTER(1) = 1
CYCSTATUS COUNTER(2) = 0

# Switch off

SYNC 0.2 MODE REF           OFF
SYNC 0.2 MODE PC            OFF

LOG DURATION_MS 1700

WRITELOGS disabled

# MODE ECONOMY is now ENABLED 

MODE    ECONOMY             ENABLED
MODE    REF                 CYCLING

# Cycle 2 DYN_ECO end time is beyond the end of the function so DYN_ECO_ARM_WRN is expected

REF DYN_ECO_END_TIME(2)   2

SYNC 5 MODE PC            ON

WAIT 6  STATE REF = CYCLING
STATE REF = CYCLING

# Set DYNAMIC ECONOMY and check that state become DYN_ECO

SYNC 0.9  ECONOMY DYNAMIC     ENABLED
SYNC 0.25 STATE REF = DYN_ECO

# Check that it returns to CYCLING and DYNAMIC ECONOMY request is reset automatically

SYNC 0.67 STATE REF = CYCLING
ECONOMY DYNAMIC = DISABLED
CYCSTATUS STATUS(1) = REG_OK  PRE_FUNC_WRN
CYCSTATUS STATUS(2) = INCOMPLETE_WRN

# Check that setting DYNAMIC ECONOMY again in the same cycle is ignored

SYNC 0.15 ECONOMY DYNAMIC     ENABLED
STATE REF = TO_CYCLING

# Check state returns to CYCLING and DYNAMIC ECONOMY has been reset automatically

SYNC 0.2 STATE REF = CYCLING
ECONOMY DYNAMIC = DISABLED

# Check that the second cycle generates the ARM warning

WARNINGS REF = DYN_ECO_ARM_WARNING
REF DYN_ECO_END_TIME(2)   1.4
SYNC 2 WARNINGS REF ! DYN_ECO_ARM_WARNING

CYCSTATUS STATUS(1) = REG_OK  PRE_FUNC_WRN  ECO_DYN
CYCSTATUS STATUS(2) = INCOMPLETE_WRN

# Test the next cycle reports a DYN_ECO_RUN warning if the DYN_ECO request is too late

SYNC 1 ECONOMY DYNAMIC   ENABLED

SYNC 0.7 WARNINGS REF = DYN_ECO_RUN_WARNING

CYCSTATUS STATUS(1) = REG_OK  ECO_DYN
CYCSTATUS STATUS(2) = REG_OK  PRE_FUNC_WRN

# Test that the DYN_ECO_RUN warning stays on until the end of the following cycle

SYNC 2.4 WARNINGS REF = DYN_ECO_RUN_WARNING
SYNC 0.2 WARNINGS REF ! DYN_ECO_RUN_WARNING

# Leave cycling and check that cycle is INCOMPLETE

SYNC 0.2 MODE REF STANDBY

SYNC 0.1 LOG TIME_ORIGIN    T

CYCSTATUS COUNTER(2) = 2

CYCSTATUS COUNTER    = 3
CYCSTATUS COUNTER(1) = 1
CYCSTATUS COUNTER(2) = 2
CYCSTATUS STATUS(1)  = INCOMPLETE_WRN
CYCSTATUS STATUS(2)  = REG_OK  PRE_FUNC_WRN

SYNC 0.2 STATE COUNTER = 1  0  1  1  5  0  0  1  7  2  0  0  0  0  0

LOG DURATION_MS 8200

WRITELOGS enabled

# EOF
