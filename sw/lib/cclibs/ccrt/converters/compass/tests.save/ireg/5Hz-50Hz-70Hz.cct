# Basic test of Current regulation, Voltage regulation and Filter Damping feedback

READ scripts/config.cct

# --- Set simulation errors ---

MODE        SIM_MEAS        DISABLED
LOAD        SIM_TC_ERROR    0.0

# --- Set noise and tone simulation ---

SYNC 0.2 SIM VS_TONE_PERIOD_ITERS    16.666666
SYNC 0.1 SIM VS_TONE_PP              1.0
SYNC 0.1 SIM MEAS_TONE_PERIOD_ITERS  200
SYNC 0.1 SIM DCCT_A_TONE_PP          0.1
SYNC 0.1 SIM DCCT_B_TONE_PP          0.1
SYNC 0.1 SIM NOISE_V_PROBE           0.02
SYNC 0.1 SIM NOISE_I_PROBE           0.1
SYNC 0.1 SIM NOISE_VS                0.5

SIM I_PROBE_TONE_PP         0.0
SIM NOISE_DCCT_A            0.0
SIM NOISE_DCCT_B            0.0
SIM NOISE_ADCS              0

SIM INVALID_PROBABILITY     0.02

# --- Log without overwriting ---

LOGMENU     V_REF           ENABLED
LOGMENU     V_REF_DBG       ENABLED
LOGMENU     I_MEAS          ENABLED
LOGMENU     I_REG           ENABLED
LOGMENU     V_REG           ENABLED
LOGMENU     V_REG_DBG       ENABLED
LOGMENU     FLAGS           ENABLED
LOGMENU     WARNINGS        ENABLED
LOGMENU     FAULTS          ENABLED
#LOGMENU     LIMITS          ENABLED

# --- Voltage regulation warning/fault levels ---

LIMITS      I_ERR_FAULT     0.08
LIMITS      I_ERR_WARNING   0.06
LIMITS      V_ERR_FAULT     1.4
LIMITS      V_ERR_WARNING   0.1

# --- Prepare to run with damping (70Hz 0.7 Z) and no voltage loop ---

VFILTER     V_MEAS_SOURCE   MEASUREMENT
VFILTER     I_CAPA_SOURCE   MEASUREMENT
VFILTER     I_CAPA_FILTER   1.0  0.0
VFILTER     K_U             1.77199626E+00
VFILTER     K_I             2.64572930E+00
VFILTER     K_D             5.04756696E-04

# THe PC SIM_Z/BANDWIDTH are used to calculate the voltage
# source response delay. This is used for the filter in the voltage
# regulation error limits. The true value is 50Hz, but it is increased
# to 5000Hz, in order to prove that the I_ERR limits are working.

VS          SIM_Z           0.7
VS          SIM_BANDWITH    5000.0

VREG        K_INT           8.88667221E+01
VREG        K_P            -8.97959247E-02
VREG        K_FF            6.00000024E-01
VREG        K_AW          ~ 4.05281084E-03
VREG        K_W           ~ 2.77654362E+00

VS          ACT_DELAY_ITERS 1
VS          FIRING_DELAY    0.83E-3

# --- Prepare a single sine wave ---

TEST        AMPLITUDE_PP    10
TEST        PERIOD          1
TEST        NUM_PERIODS     1

# --- Prepare to run in IDLE and voltage regulation mode ---

MODE        REF             IDLE
MODE        REG_MODE        CURRENT

# --- Firing delay = 0.83ms (12-pulse) - Actuation delay = 1 iteration ---

SYNC 2      MODE PC         ON
WAIT 5      STATE REF = IDLE
STATE       REF = IDLE

REF         FG_TYPE         SINE

SYNC 0.2 ARM
WAIT 10     STATE REF = ARMED
STATE       REF = ARMED

RUN 0.1

SYNC 0.7 SIM VS_PERTURBATION    1
SYNC 0.25 LOG TIME_ORIGIN        T

LOG         DURATION_MS         850

WRITELOGS BW=5Hz_50Hz_70Hz_FiringDelay=0.83ms_SINE

# EOF
