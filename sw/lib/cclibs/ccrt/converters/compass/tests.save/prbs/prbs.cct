# Basic test of Voltage regulation and Filter Damping feedback

READ scripts/config.cct

# --- Set simulation errors ---

MODE        SIM_MEAS        DISABLED
LOAD        SIM_TC_ERROR    0.0

# --- Set noise and tone simulation ---

SYNC 1   SIM VS_TONE_PERIOD_ITERS    16.666666
SYNC 0.2 SIM MEAS_TONE_PERIOD_ITERS  200
SYNC 0.2 SIM VS_TONE_PP              5.0
SIM NOISE_VS                0.5
SIM NOISE_V_PROBE           0.02
SIM NOISE_I_PROBE           0.1

SIM DCCT_A_TONE_PP          0.0
SIM DCCT_B_TONE_PP          0.0
SIM I_PROBE_TONE_PP         0.0
SIM NOISE_DCCT_A            0.0
SIM NOISE_DCCT_B            0.0
SIM NOISE_ADCS              0

# --- Log without overwriting ---

LOGMENU     V_REF           ENABLED
LOGMENU     I_MEAS_MPX      ENABLED
LOGMPX      I_MEAS_MPX      V_REF I_MEAS

# --- Prepare damping loop (70Hz 0.7 Z) and no voltage loop ---

VFILTER     V_MEAS_SOURCE   MEASUREMENT
VFILTER     I_CAPA_SOURCE   MEASUREMENT
VFILTER     I_CAPA_FILTER   1.0  0.0
VFILTER     K_U             1.77199626E+00
VFILTER     K_I             2.64572930E+00
VFILTER     K_D             5.04756696E-04
VFILTER     K_W          ~  2.77654362E+00

VS          SIM_Z           0.7
VS          SIM_BANDWIDTH   70.0

VREG        K_INT           0.0
VREG        K_P             0.0
VREG        K_FF            1.0

VS          ACT_DELAY_ITERS 1
VS          FIRING_DELAY    0.83E-3

# --- Prepare PRBS ---

PRBS        AMPLITUDE_PP    1000
PRBS        PERIOD_ITERS    100
PRBS        NUM_SEQUENCES   2
PRBS        K               6

# --- Prepare to run in IDLE and voltage regulation mode ---

MODE        REF             IDLE
MODE        REG_MODE        VOLTAGE

# --- Firing delay = 0.83ms (12-pulse) - Actuation delay = 1 iteration ---

SYNC 2      MODE PC         ON
WAIT 8      STATE REF = IDLE
STATE       REF = IDLE

REF         FG_TYPE         PRBS

SYNC 0.2 ARM
WAIT 10     STATE REF = ARMED
STATE       REF = ARMED

RUN 0.1

WAITFUNCTIME

SYNC 0.1 LOG TIME_ORIGIN T
LOG         DURATION_MS     1400
WRITELOGS BW=70Hz_FiringDelay=0.83ms_PRBS

# EOF
