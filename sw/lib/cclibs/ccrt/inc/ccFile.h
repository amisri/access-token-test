//! @file  ccrt/inc/ccFile.h
//!
//! @brief ccrt header file for ccFile.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCFILE_EXT
#else
#define CCFILE_EXT extern
#endif

#include <sys/stat.h>
#include <stdarg.h>
#include "ccThread.h"

// Constants

#define CC_TMP_EVT_FILE             "tmp_evtlog"
#define CC_TMP_CFG_FILE             "config/tmp"
#define CC_TMP_REF_FILE             "ref/tmp"
#define CC_TMP_LOG_FILE             "../../logs/tmp_spy_buf"
#define CC_TMP_PM_BUF_FILE          "../../logs/tmp_pm_buf"
#define CC_DEFAULT_CONVERTER_NAME   "default"
#define CC_EXIT_FILE                2

// Global i/o structure

struct CC_file
{
    uint32_t                read_cyc_sel;                           // (cyc_sel) specifier for READ command
    uint32_t                cyc_sel;                                // (cyc_sel) specifier for command or parameter
    uint32_t                array_idx;                              // [array_idx] specifer for command or parameter
    uint32_t                log_dir_idx;                            // Log directory index when LOG OVERWRITE is DISABLED
    uint32_t                max_cmd_name_len;
    uint32_t                max_par_name_len;
    char                    converter[CC_PATH_LEN];                 // Converter name (from command line parameter

    //! The structure keeps string variables related to test filename

    struct CC_file_test
    {
        char dirname        [CC_PATH_LEN];    //!< Test file dirname relative to {ccrt_home}/converters/{converter_name}/tests
        char basename       [CC_PATH_LEN];    //!< Test file basename in batch mode, "stdin" in interactive mode
        char basename_no_ext[CC_PATH_LEN];    //!< Like basename, but without the .cct extension in batch mode
    } test_file;

    char logs_dirname[CC_PATH_LEN];    //!< Logs directory for current test. Relative to ccrt home.
};

CCFILE_EXT struct CC_file ccfile
#ifdef GLOBALS
= {
    .read_cyc_sel =  CC_NO_INDEX,
    .cyc_sel      =  CC_NO_INDEX,
}
#endif
;

// Function declarations

//! Creates a path with given format
//!
//! The function will create a path with given format. It limits path length to
//! CC_PATH_LEN - 1 characters. If the limit's exceeded, the function will call exit().
//!
//! @param[out]  path  Pointer to char array of at least CC_PATH_LEN characters
//! @param[in]   fmt   Path format

void ccFilePrintPath(char * const path, const char * fmt, ...);

//! Creates given path recursively
//!
//! The function splits the given path into sub-directories and checks for their
//! existence in top-down manner. If any of the sub-directories doesn't exist then
//! the function creates it.
//!
//! The function calls exit() at failure.
//!
//! @warning  The function has a TOCTOU problem, but it should not be visible with ccrt.
//!
//! @param[in]  path  Path to create

void ccFileMakeDir(char const * const path);

//! Closes a file
//!
//! The function calls exit at failure.
//!
//! @param[in,out]  file  File to close

void ccFileClose(FILE * const file);

//! Opens a file in requested mode
//!
//! The functions calls exit on failure.
//!
//! @param[in]  filename  Name of the file
//! @param[in]  mode      File mode
//!
//! @return  Pointer to FILE structure

FILE * ccFileOpen(const char * const filename, const char * const mode);

//! Opens a text file in write mode
//!
//! The functions calls exit on failure.
//!
//! @param[in]  filename  Name of the file
//!
//! @return  Pointer to FILE structure

FILE * ccFileOpenTextFile(const char * const filename);

//! Opens a binary file in write mode
//!
//! The functions calls exit on failure.
//!
//! @param[in]  filename  Name of the file
//!
//! @return  Pointer to FILE structure

FILE * ccFileOpenBinaryFile(const char * const filename);

//! Removes file
//!
//! The function calls exit() on failure.
//!
//! @param[in]  filename  Name of the file to remove

void ccFileRemove(const char * const filename);

//! Read commands from a FILE stream (stdin or a real file)
//!
//! The function calls exit() on failure.
//!
//! @param[in]  cmd_idx   0 when called from main, !0 otherwise
//! @param[in]  filename  NULL for stdin, or filename to read
//!
//! @return  EXIT_SUCCESS or EXIT_FAILURE

uint32_t ccFileReadCmds(uint32_t cmd_idx, char *filename);


//! Calls READ command on each file in path
//!
//! The function calls exit on failure
//!
//! @param[in]  path  Path to files to read

void ccFileReadAll(const char * const path);

//! Creates all neccessary directories
//!
//! In {ccrt_home}/converters/{converter_name}:
//! * config
//! * ref/{sub_sel}
//! * tests

void ccFileCreateDirs(void);

//! Saves config parameter in proper file
//!
//! File basename follows {top-level parameter name}_{parameter name} pattern.
//!
//! Sub-sel parameters go into ref/{sub_sel} directory. Non sub-sel parameters
//! go into config directory.
//!
//! @retval  EXIT_SUCCESS  On success
//! @retval  EXIT_FAILURE  On failure

uint32_t ccFileConfigPar(const struct CC_pars * const par);



//! Closes temporary file and replaces permanent file with it
//!
//! The function calls exit on any failure. It also checks if the temporary
//! file exists and the file handler is not null (file already closed) before
//! proceeding.
//!
//! @param[in,out]  temp_file           Temporary file handler or NULL
//! @param[in]      temp_filename       Temporary file name
//! @param[in]      permanent_filename  Permanent file name

void ccFileCloseAndReplace(      FILE * const temp_file,
                           const char * const temp_filename,
                           const char * const permanent_filename);



//! Reads iteration and event log periods from the CLOCK file
//!
//! Tries to open CLOCK file and get iteration period and event log period values from it.
//! If the file doesnt't exist or if it's corrupted then default values will be used.
//! After setting the parameters, the function rewrites (or creates) the CLOCK file.

void ccFileClock(void);



//! Saves reference parameters in proper file
//!
//! File path will be ref/{sub_sel}/REF_{cyc_sel}
//!
//! @param[in]  sub_sel  Sub-device selector
//! @param[in]  cyc_sel  Cycle selector
//!
//! @retval  EXIT_SUCCESS  On success
//! @retval  EXIT_FAILURE  On failure

uint32_t ccFileRefArmedPars(const uint32_t sub_sel, const uint32_t cyc_sel);


//! Checks if a file exists
//!
//! @retval  true   File exists
//! @retval  false  File doesn't exist

static inline bool ccFileExists(char const * const filename)
{
    struct stat buffer;

    return stat(filename, &buffer) == 0;
}

// EOF
