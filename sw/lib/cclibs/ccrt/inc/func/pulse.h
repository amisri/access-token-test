//! @file  ccrt/inc/func/pulse.h
//!
//! @brief ccrt PULSE function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_PULSE_EXT
#else
#define CCPARS_PULSE_EXT extern
#endif

//! Instance of PULSE parameters structure

CCPARS_PULSE_EXT struct REF_pulse_pars ccpars_pulse[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .ref      = { 0.0 },      // PULSE REF
               .duration = { 0.6 }, }    // PULSE DURATION
}
#endif
;

//! PULSE parameters description structure

CCPARS_PULSE_EXT struct CC_pars pulse_pars[]
#ifdef GLOBALS
= {// name        type   max_n_els  *enum          *value                       n_els flags                                          size_of_struct
    { "REF",      PAR_FLOAT, 1,     NULL,   { .f = ccpars_pulse[0][0].ref      }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pulse_pars) },
    { "DURATION", PAR_FLOAT, 1,     NULL,   { .f = ccpars_pulse[0][0].duration }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pulse_pars) },
    { NULL }
}
#endif
;

// EOF
