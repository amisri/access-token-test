//! @file  ccrt/inc/func/plep.h
//!
//! @brief ccrt PLEP function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_PLEP_EXT
#else
#define CCPARS_PLEP_EXT extern
#endif

//! Instance of PLEP parameters structure

CCPARS_PLEP_EXT struct REF_plep_pars ccpars_plep[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref  = {  0.0 },      // PLEP INITIAL_REF
               .final_ref    = {  1.0 },      // PLEP FINAL_REF
               .acceleration = { 50.0 },      // PLEP ACCELERATION
               .linear_rate  = {  5.0 },      // PLEP LINEAR_RATE
               .exp_tc       = {  0.0 },      // PLEP EXP_TC
               .exp_final    = {  0.0 }, }    // PLEP EXP_FINAL
}
#endif
;

//! PLEP parameters description structure

CCPARS_PLEP_EXT struct CC_pars plep_pars[]
#ifdef GLOBALS
= {// name            type   max_n_els *enum          *value                                         n_els flags                                          size_of_struct
    { "INITIAL_REF",  PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].initial_ref                 }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "FINAL_REF",    PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].final_ref                   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "ACCELERATION", PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].acceleration                }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "LINEAR_RATE",  PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].linear_rate                 }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "EXP_TC",       PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].exp_tc                      }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "EXP_FINAL",    PAR_FLOAT, 1,    NULL,   { .f = ccpars_plep[0][0].exp_final                   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_plep_pars) },
    { "ARMED_RMS",    PAR_FLOAT, 1,    NULL,   { .f = refMgrVarPointer(&ref_mgr, REFARMED_PLEP_RMS) }, 1,  PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                           },
    { NULL }
}
#endif
;

// EOF

