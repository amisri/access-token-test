//! @file  ccrt/inc/func/table.h
//!
//! @brief ccrt TABLE function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_TABLE_EXT
#else
#define CCPARS_TABLE_EXT extern
#endif

#define TABLE_LEN    5000

//! TABLE parameters structure

struct CC_pars_table
{
    struct FG_point      function[TABLE_LEN];   //!< Function array in ccpars_table
};

//! Used for accessing TABLE parameters

enum table_pars_index_enum
{
    TABLE_FUNCTION
};

//! Instance of TABLE parameters structure

CCPARS_TABLE_EXT struct CC_pars_table ccpars_table[CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0] = { .function = { { 0.0, 0.0 }, { 0.2, 1.0 }, { 0.4, 1.0 }, { 0.6, 0.0 } } }  // TABLE FUNCTION
}
#endif
;

//! TABLE parameters description structure

CCPARS_TABLE_EXT struct CC_pars   table_pars[]
#ifdef GLOBALS
= {// name        type       max_n_els   *enum          *value                     n_els flags                             size_of_struct
    { "FUNCTION", PAR_POINT, TABLE_LEN,  NULL,   { .p = ccpars_table[0].function  }, 4,  PARS_RW|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct CC_pars_table) },
    { NULL }
}
#endif
;

// EOF
