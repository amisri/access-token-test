//! @file  ccrt/inc/func/cubexp.h
//!
//! @brief ccrt CUBEXP function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_CUBEXP_EXT
#else
#define CCPARS_CUBEXP_EXT extern
#endif

//! Instance of CUBEXP parameters structure

CCPARS_CUBEXP_EXT struct REF_cubexp_pars ccpars_cubexp[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .ref  = {  0.0  },      // CUBEXP REF
               .rate = {  0.0  },      // CUBEXP RATE
               .time = {  0.0  }, }    // CUBEXP TIME
}
#endif
;

//! CUBEXP parameters description structure

CCPARS_CUBEXP_EXT struct CC_pars cubexp_pars[]
#ifdef GLOBALS
= {// name        type       max_n_els            *enum         *value                                              n_els                 flags                                          size_of_struct
    { "REF",      PAR_FLOAT, FG_CUBEXP_MAX_POINTS, NULL, { .f = ccpars_cubexp[0][0].ref                            }, 2,                  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_cubexp_pars) },
    { "RATE",     PAR_FLOAT, FG_CUBEXP_MAX_POINTS, NULL, { .f = ccpars_cubexp[0][0].rate                           }, 2,                  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_cubexp_pars) },
    { "TIME",     PAR_FLOAT, FG_CUBEXP_MAX_POINTS, NULL, { .f = ccpars_cubexp[0][0].time                           }, 2,                  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_cubexp_pars) },
    { "MAX_REF",  PAR_FLOAT, FG_CUBEXP_MAX_SEGS,   NULL, { .f = refMgrVarValue(&ref_mgr, REFARMED_CUBEXP_MAX_REF)  }, FG_CUBEXP_MAX_SEGS, PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS                                  },
    { "MIN_REF",  PAR_FLOAT, FG_CUBEXP_MAX_SEGS,   NULL, { .f = refMgrVarValue(&ref_mgr, REFARMED_CUBEXP_MIN_REF)  }, FG_CUBEXP_MAX_SEGS, PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS                                  },
    { "MAX_RATE", PAR_FLOAT, FG_CUBEXP_MAX_SEGS,   NULL, { .f = refMgrVarValue(&ref_mgr, REFARMED_CUBEXP_MAX_RATE) }, FG_CUBEXP_MAX_SEGS, PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS                                  },
    { NULL }
}
#endif
;

// EOF
