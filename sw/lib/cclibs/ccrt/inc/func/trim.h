//! @file  ccrt/inc/func/trim.h
//!
//! @brief ccrt TRIM function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_TRIM_EXT
#else
#define CCPARS_TRIM_EXT extern
#endif

//! Instance of TRIM parameters structure

CCPARS_TRIM_EXT struct REF_trim_pars ccpars_trim[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref = { 0.0 },      // TRIM INITIAL_REF
               .final_ref   = { 1.0 },      // TRIM FINAL
               .duration    = { 0.6 }, }    // TRIM DURATION
}
#endif
;

//! TRIM parameters description structure

CCPARS_TRIM_EXT struct CC_pars trim_pars[]
#ifdef GLOBALS
= {// name           type    max_n_els  *enum          *value                         n_els flags                                          size_of_struct
    { "INITIAL_REF", PAR_FLOAT,  1,     NULL,   { .f = ccpars_trim[0][0].initial_ref }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_trim_pars) },
    { "FINAL_REF",   PAR_FLOAT,  1,     NULL,   { .f = ccpars_trim[0][0].final_ref   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_trim_pars) },
    { "DURATION",    PAR_FLOAT,  1,     NULL,   { .f = ccpars_trim[0][0].duration    }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_trim_pars) },
    { NULL }
}
#endif
;

// EOF
