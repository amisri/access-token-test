//! @file  ccrt/inc/func/ramp.h
//!
//! @brief ccrt RAMP function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_RAMP_EXT
#else
#define CCPARS_RAMP_EXT extern
#endif

//! Instance of RAMP parameters structure

CCPARS_RAMP_EXT struct REF_ramp_pars ccpars_ramp[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref  = {  0.0 },      // RAMP INITIAL_REF
               .final_ref    = {  1.0 },      // RAMP FINAL_REF
               .acceleration = { 50.0 },      // RAMP ACCELERATION
               .linear_rate  = {  5.0 },      // RAMP LINEAR_RATE
               .deceleration = { 30.0 }, }    // RAMP DECELERTION
}
#endif
;

//! RAMP parameters desctiption structure

CCPARS_RAMP_EXT struct CC_pars ramp_pars[]
#ifdef GLOBALS
= {// name            type    max_n_els  *enum          *value                          n_els flags                                          size_of_strut
    { "INITIAL_REF",  PAR_FLOAT,  1,     NULL,   { .f = ccpars_ramp[0][0].initial_ref  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_ramp_pars) },
    { "FINAL_REF",    PAR_FLOAT,  1,     NULL,   { .f = ccpars_ramp[0][0].final_ref    }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_ramp_pars) },
    { "ACCELERATION", PAR_FLOAT,  1,     NULL,   { .f = ccpars_ramp[0][0].acceleration }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_ramp_pars) },
    { "LINEAR_RATE",  PAR_FLOAT,  1,     NULL,   { .f = ccpars_ramp[0][0].linear_rate  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_ramp_pars) },
    { "DECELERATION", PAR_FLOAT,  1,     NULL,   { .f = ccpars_ramp[0][0].deceleration }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_ramp_pars) },
    { NULL }
}
#endif
;

// EOF

