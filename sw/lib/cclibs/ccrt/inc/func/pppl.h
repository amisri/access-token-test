//! @file  ccrt/inc/func/pppl.h
//!
//! @brief ccrt PPPL function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

//#include "ccRt.h"
#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_PPPL_EXT
#else
#define CCPARS_PPPL_EXT extern
#endif

//! Instance of PPPL parameters structure

CCPARS_PPPL_EXT struct REF_pppl_pars ccpars_pppl[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref   = {   0.0,  },      // PPPL INITIAL_REF
               .acceleration1 = {   50.0  },      // PPPL ACCELERATION1
               .acceleration2 = {   -5.0  },      // PPPL ACCELERATION2
               .acceleration3 = {  -30.0  },      // PPPL ACCELERATION3
               .rate2         = {    5.0  },      // PPPL RATE2
               .rate4         = {    0.0  },      // PPPL RATE4
               .ref4          = {    1.0  },      // PPPL REF4
               .duration4     = {    0.1  }, }    // PPPL DURATION4
}
#endif
;

//! PPPL parameters description structure

CCPARS_PPPL_EXT struct CC_pars pppl_pars[]
#ifdef GLOBALS
= {// name             type       max_n_els     *enum          *value                            n_els flags                                          size_of_struct
    { "INITIAL_REF",   PAR_FLOAT, 1,            NULL,   { .f =  ccpars_pppl[0][0].initial_ref   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "ACCELERATION1", PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].acceleration1 }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "ACCELERATION2", PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].acceleration2 }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "ACCELERATION3", PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].acceleration3 }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "RATE2",         PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].rate2         }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "RATE4",         PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].rate4         }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "REF4",          PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].ref4          }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { "DURATION4",     PAR_FLOAT, FG_MAX_PPPLS, NULL,   { .f =  ccpars_pppl[0][0].duration4     }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_pppl_pars) },
    { NULL }
}
#endif
;

// EOF
