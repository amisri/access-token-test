//! @file  ccrt/inc/func/prbs.h
//!
//! @brief ccrt PRBS function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_PRBS_EXT
#else
#define CCPARS_PRBS_EXT extern
#endif

//! Instance of PRBS parameters structure

CCPARS_PRBS_EXT struct REF_prbs_pars ccpars_prbs[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref   =  { 0.0 },      // PRBS INITIAL_REF
               .amplitude_pp  =  { 1.0 },      // PRBS AMPLITUDE_PP
               .period_iters  =  { 10  },      // PRBS PERIOD_ITERS
               .num_sequences =  { 1   },      // PRBS NUM_SEQUENCES
               .k             =  { 6   }, }    // PRBS K
}
#endif
;

//! PRBS parameters description structure

CCPARS_PRBS_EXT struct CC_pars prbs_pars[]
#ifdef GLOBALS
= {// name             type      max_n_els  *enum          *value                           n_els flag                                           size_of_struct
    { "INITIAL_REF"  , PAR_FLOAT,    1,     NULL,   { .f = ccpars_prbs[0][0].initial_ref   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_prbs_pars) },
    { "AMPLITUDE_PP" , PAR_FLOAT,    1,     NULL,   { .f = ccpars_prbs[0][0].amplitude_pp  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_prbs_pars) },
    { "PERIOD_ITERS" , PAR_UNSIGNED, 1,     NULL,   { .u = ccpars_prbs[0][0].period_iters  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_prbs_pars) },
    { "NUM_SEQUENCES", PAR_UNSIGNED, 1,     NULL,   { .u = ccpars_prbs[0][0].num_sequences }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_prbs_pars) },
    { "K"            , PAR_UNSIGNED, 1,     NULL,   { .u = ccpars_prbs[0][0].k             }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_prbs_pars) },
    { NULL }
}
#endif
;

// EOF
