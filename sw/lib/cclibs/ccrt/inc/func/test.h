//! @file  ccrt/inc/func/test.h
//!
//! @brief ccrt TEST function parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_TEST_EXT
#else
#define CCPARS_TEST_EXT extern
#endif

//! Instance of TEST parameters structure

CCPARS_TEST_EXT struct REF_test_pars ccpars_test[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .initial_ref   = { 0.0         },      // TEST INITIAL_REF
               .amplitude_pp  = { 1.0         },      // TEST AMPLITUDE_PP
               .num_periods   = { 1           },      // TEST NUM_PERIODS
               .period_iters  = { 0           },      // TEST PERIOD_ITERS
               .period        = { 0.8         },      // TEST PERIOD
               .window        = { CC_ENABLED  },      // TEST WINDOW
               .exp_decay     = { CC_DISABLED }, }    // TEST EXP_DECAY
}
#endif
;

//! TEST parameters description structure

CCPARS_TEST_EXT struct CC_pars test_pars[]
#ifdef GLOBALS
= {// name            type      max_n_els  *enum                         *value                           n_els flags                                          size_of_struct
    { "INITIAL_REF" , PAR_FLOAT,    1,     NULL,                  { .f = ccpars_test[0][0].initial_ref   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "AMPLITUDE_PP", PAR_FLOAT,    1,     NULL,                  { .f = ccpars_test[0][0].amplitude_pp  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "NUM_PERIODS" , PAR_UNSIGNED, 1,     NULL,                  { .u = ccpars_test[0][0].num_periods   }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "PERIOD_ITERS", PAR_UNSIGNED, 1,     NULL,                  { .u = ccpars_test[0][0].period_iters  }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "PERIOD"      , PAR_FLOAT,    1,     NULL,                  { .f = ccpars_test[0][0].period        }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "WINDOW"      , PAR_ENUM,     1,     enum_enabled_disabled, { .u = ccpars_test[0][0].window        }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { "EXP_DECAY"   , PAR_ENUM,     1,     enum_enabled_disabled, { .u = ccpars_test[0][0].exp_decay     }, 1,  PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_TRANS,  sizeof(struct REF_test_pars) },
    { NULL }
}
#endif
;

// EOF
