//! @file  ccrt/inc/ccEvtlog.h
//!
//! @brief ccrt header file for ccEvtlog.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "evtlogStructs.h"

extern struct EVTLOG_data evtlog_data;

// Function declarations

void     ccEvtlogInit               (void);
void   * ccEvtlogThread             (void *args);
void     ccEvtlogChangedFunction    (uint32_t const device_index, char const * name, void * user_data);
void     ccEvtlogLockFunction       (const uint32_t device_index, void * const mutex);
void     ccEvtlogUnlockFunction     (const uint32_t device_index, void * const mutex);
uint32_t ccEvtlogStore              (char *remaining_line);
void     ccEvtlogPrintCsv           (FILE * const f);
void     ccEvtlogPrintFgc           (FILE * const f);
void     ccEvtlogPrintBin           (FILE * const f);
void     ccEvtlogDebug              (void);

// EOF
