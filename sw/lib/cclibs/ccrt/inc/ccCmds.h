//! @file  ccrt/inc/ccCmds.h
//!
//! @brief ccrt header file for ccCmds.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCCMDS_EXT
#else
#define CCCMDS_EXT extern
#endif

// Include cclibs header files

#include "libsig.h"
#include "libref.h"
#include "liblog.h"
#include "libevtlog.h"

// Include cclibs generated header files for libsig, liglog and libevtlog

#include "sigStructs.h"
#include "logMenus.h"
#include "logStructs.h"
#include "evtlogStructs.h"

// Constants

#define CC_MAX_FILE_LINE_LEN  65536
#define CC_PROMPT             ">"

// Function declarations

uint32_t ccCmdsPar           (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsHelp          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsRead          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsArm           (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsRestore       (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsRun           (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsAbort         (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsPause         (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsUnpause       (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsCoast         (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsRecover       (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsStartHarmonics(uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsStopHarmonics (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsReset         (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsWait          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsSync          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsWaitFuncTime  (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsWriteLogs     (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsEnable        (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsDisable       (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsSigNames      (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsSave          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsStatus        (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsEvtStore      (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsEvtLog        (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsEvtDebug      (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsCalAdcs       (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsCalDccts      (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsCalDac        (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsFifo          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsUnfreeze      (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsIlcRst        (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsDebug         (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsExit          (uint32_t cmd_idx, char *remaining_line);
uint32_t ccCmdsQuit          (uint32_t cmd_idx, char *remaining_line);

// Wait command indexes - this identifies the specific commands that wait for the real-time thread - it is used for logging

enum CC_cmds_wait_commands
{
    CMD_WAIT_NONE         ,
    CMD_WAIT_WAIT         ,
    CMD_WAIT_SYNC         ,
    CMD_WAIT_WAITFUNCTIME ,
    CMD_WAIT_CAL_ADCS     ,
    CMD_WAIT_CAL_DCCTS    ,
    CMD_WAIT_CAL_DAC      ,
    CMD_WAIT_NUM_CMDS
};

// Command indexes - the order of enum cccmds_enum must match the cmds[] array below

enum CC_cmds_enum
{
    // Commands - logical order

    CMD_HELP           ,
    CMD_READ           ,
    CMD_RESET          ,
    CMD_ARM            ,
    CMD_RESTORE        ,
    CMD_RUN            ,
    CMD_ABORT          ,
    CMD_PAUSE          ,
    CMD_UNPAUSE        ,
    CMD_COAST          ,
    CMD_RECOVER        ,
    CMD_START_HARMONICS,
    CMD_STOP_HARMONICS ,
    CMD_WRITELOGS      ,
    CMD_ENABLE         ,
    CMD_DISABLE        ,
    CMD_SIGNAMES       ,
    CMD_WAIT           ,
    CMD_SYNC           ,
    CMD_WAITFUNCTIME   ,
    CMD_STATUS         ,
    CMD_EVTSTORE       ,
    CMD_EVETLOG        ,
    CMD_EVETDEBUG      ,
    CMD_CALADCS        ,
    CMD_CALDCCTS       ,
    CMD_CALDAC         ,
    CMD_FIFO           ,
    CMD_SAVE           ,
    CMD_UNFREEZE       ,
    CMD_ILCRST         ,
    CMD_DEBUG          ,
    CMD_EXIT           ,
    CMD_QUIT           ,

    // Global parameters - alphabetical

    CMD_BREG          ,
    CMD_CYCSTATUS     ,
    CMD_CAL           ,
    CMD_DECO          ,
    CMD_DEFAULT       ,
    CMD_DIRECT        ,
    CMD_ECONOMY       ,
    CMD_EVENT         ,
    CMD_EVT           ,
    CMD_FAULTS        ,
    CMD_FGERROR       ,
    CMD_GLOBAL        ,
    CMD_ILC           ,
    CMD_IREG          ,
    CMD_LIMITS        ,
    CMD_LOAD          ,
    CMD_LOG           ,
    CMD_LOGCTRL       ,
    CMD_LOGMENU       ,
    CMD_LOGMGR        ,
    CMD_LOGMPX        ,
    CMD_LOGREQ        ,
    CMD_LOGSPY        ,
    CMD_LOGSTATUS     ,
    CMD_LOGSTR        ,
    CMD_MEAS          ,
    CMD_MODE          ,
    CMD_POLSWITCH     ,
    CMD_PRE_FUNC      ,
    CMD_REF           ,
    CMD_RTREF         ,
    CMD_SIM           ,
    CMD_STATE         ,
    CMD_TEMP          ,
    CMD_TRANS         ,
    CMD_VFEEDFWD      ,
    CMD_VFILTER       ,
    CMD_VREG          ,
    CMD_VS            ,
    CMD_WARNINGS      ,

    // Function parameters - alphabetical

    CMD_CUBEXP        ,
    CMD_PLEP          ,
    CMD_PPPL          ,
    CMD_PRBS          ,
    CMD_PULSE         ,
    CMD_RAMP          ,
    CMD_TABLE         ,
    CMD_TEST          ,
    CMD_TRIM          ,

    NUM_CMDS
};

// Include ccrt function data header files

#include "func/cubexp.h"
#include "func/plep.h"
#include "func/pppl.h"
#include "func/pulse.h"
#include "func/ramp.h"
#include "func/table.h"
#include "func/test.h"
#include "func/trim.h"
#include "func/prbs.h"

// Include ccrt parameter header files - required by cmds[]

#include "pars/polswitch.h"
#include "pars/trans.h"
#include "pars/ref.h"
#include "pars/state.h"
#include "pars/mode.h"
#include "pars/pre_func.h"
#include "pars/global.h"
#include "pars/cycstatus.h"
#include "pars/deco.h"
#include "pars/default.h"
#include "pars/direct.h"
#include "pars/economy.h"
#include "pars/event.h"
#include "pars/ilc.h"
#include "pars/limits.h"
#include "pars/load.h"
#include "pars/log.h"
#include "pars/logstr.h"
#include "pars/logspy.h"
#include "pars/logctrl.h"
#include "pars/logmenu.h"
#include "pars/logstatus.h"
#include "pars/logmpx.h"
#include "pars/logmgr.h"
#include "pars/logreq.h"
#include "pars/evt.h"
#include "pars/meas.h"
#include "pars/reg.h"
#include "pars/vreg.h"
#include "pars/vfilter.h"
#include "pars/rtref.h"
#include "pars/vs.h"
#include "pars/faults.h"
#include "pars/warnings.h"
#include "pars/sim.h"
#include "pars/cal.h"
#include "pars/temp.h"
#include "pars/vfeedfwd.h"

// Define array of commands

typedef uint32_t (* const CC_cmds_func)(uint32_t cmd_idx, char * remaining_line);

struct CC_cmds
{
    const char     * const name;                //!< Name of command
    CC_cmds_func           cmd_func;            //!< Function called on command invocation
    bool                   sync_cmd;            //!< True if command is compatible with SYNC command
    struct CC_pars * const pars;                //!< Pointer to parameter structures associated with command
    const char     * const help_message;        //!< Help message associated with command

    // Don't move around the values above!

    uint32_t               flags;               //!< Gathers all parameter's flags
    uint32_t               name_len;            //!< Length of command's name
    uint32_t               max_par_name_len;    //!< Maximal parameter name length
};

CCCMDS_EXT struct CC_cmds cmds[] // The order must match enum cccmds_enum (above)
#ifdef GLOBALS
= {
    // Commands - logical order                SYNC_CMD
    { "HELP"           , ccCmdsHelp          , false, NULL          , "                             Print this help message"                               },
    { "READ"           , ccCmdsRead          , false, NULL          , "[filename]                   Read parameters from named file or from stdin"         },
    { "RESET"          , ccCmdsReset         , true , NULL          , "                             Reset faults"                                          },
    { "ARM"            , ccCmdsArm           , true , NULL          , "[cyc_sel]                    Arm reference function for cyc_sel"                    },
    { "RESTORE"        , ccCmdsRestore       , true , NULL          , "[cyc_sel]                    Restore armed reference parameters for cyc_sel"        },
    { "RUN"            , ccCmdsRun           , false, NULL          , "                             Run armed reference function in ARMED state"           },
    { "ABORT"          , ccCmdsAbort         , true , NULL          , "                             Abort running function"                                },
    { "PAUSE"          , ccCmdsPause         , false, NULL          , "                             Enter PAUSED state"                                    },
    { "UNPAUSE"        , ccCmdsUnpause       , false, NULL          , "                             Leave PAUSED state"                                    },
    { "COAST"          , ccCmdsCoast         , false, NULL          , "                             Start coast (IDLE state)"                              },
    { "RECOVER"        , ccCmdsRecover       , false, NULL          , "                             Recover from coast (return to CYCLING state)"          },
    { "START_HARMONICS",ccCmdsStartHarmonics, false, NULL          , "                             Start voltage harmonics generator"                     },
    { "STOP_HARMONICS" , ccCmdsStopHarmonics , false, NULL          , "                             Stop voltage harmonics generator"                      },
    { "WRITELOGS"      , ccCmdsWriteLogs     , false, NULL          , "[dir_name]                   Write log files"                                       },
    { "ENABLE"         , ccCmdsEnable        , true,  NULL          , "[log_name [log_name ...]]    Enable logs"                                           },
    { "DISABLE"        , ccCmdsDisable       , true,  NULL          , "[log_name [log_name ...]]    Disable logs"                                          },
    { "SIGNAMES"       , ccCmdsSigNames      , false, NULL          , "                             Print signal names and all signals for log menus"      },
    { "WAIT"           , ccCmdsWait          , false, NULL          , "[seconds [par cond val...]]  Wait until time since last wakeup or condition is true"},
    { "SYNC"           , ccCmdsSync          , false, NULL          , "seconds command              Wait until time since last wakeup, then run command"   },
    { "WAITFUNCTIME"   , ccCmdsWaitFuncTime  , false, NULL          , "[seconds]                    Wait until function time (default waits for func end)" },
    { "STATUS"         , ccCmdsStatus        , false, NULL          , "                             Print status information"                              },
    { "EVTSTORE"       , ccCmdsEvtStore      , false, NULL          , "Property Value Action Status Store record in event log"                             },
    { "EVTLOG"         , ccCmdsEvtLog        , false, NULL          , "                             Print event log"                                       },
    { "EVTDEBUG"       , ccCmdsEvtDebug      , false, NULL          , "                             Print event log debug information"                     },
    { "CALADCS"        , ccCmdsCalAdcs       , false, NULL          , "                             Calibrates the ADCs"                                   },
    { "CALDCCTS"       , ccCmdsCalDccts      , false, NULL          , "                             Calibrates the DCCTs"                                  },
    { "CALDAC"         , ccCmdsCalDac        , false, NULL          , "                             Calibrates the DAC"                                    },
    { "FIFO"           , ccCmdsFifo          , false, NULL          , "                             Print ADC fifo data"                                   },
    { "SAVE"           , ccCmdsSave          , false, NULL          , "[filename]                   Write configuration parameters to file"                },
    { "UNFREEZE"       , ccCmdsUnfreeze      , false, NULL          , "                             Clear and unfreeze post mortem logs"                   },
    { "ILCRST"         , ccCmdsIlcRst        , false, NULL          , "                             Reset ILC"                                             },
    { "DEBUG"          , ccCmdsDebug         , false, NULL          , "                             Print debug data"                                      },
    { "EXIT"           , ccCmdsExit          , false, NULL          , "                             Exit from current file or quit when from stdin"        },
    { "QUIT"           , ccCmdsQuit          , false, NULL          , "                             Quit program immediately\n"                            },
    // Global parameters - alphabetical        SYNC_CMD
    { "BREG"           , ccCmdsPar           , true , breg_pars     , "Get or set BREG      parameter(s)"   },
    { "CYCSTATUS"      , ccCmdsPar           , true , cycstatus_pars, "Get        CYCSTATUS parameter(s)"   },
    { "CAL"            , ccCmdsPar           , true , cal_pars      , "Get        CAL       parameter(s)"   },
    { "DECO"           , ccCmdsPar           , true , deco_pars     , "Get or set DECO      parameter(s)"   },
    { "DEFAULT"        , ccCmdsPar           , true , default_pars  , "Get or set DEFAULT   parameter(s)"   },
    { "DIRECT"         , ccCmdsPar           , true , direct_pars   , "Get or set DIRECT    parameter(s)"   },
    { "ECONOMY"        , ccCmdsPar           , true , economy_pars  , "Get        ECONOMY   parameter(s)"   },
    { "EVENT"          , ccCmdsPar           , true , event_pars    , "Get        EVENT     parameter(s)"   },
    { "EVT"            , ccCmdsPar           , true , evt_pars      , "Get        EVT       parameter(s)"   },
    { "FAULTS"         , ccCmdsPar           , true , faults_pars   , "Get        FAULTS    parameter(s)"   },
    { "FGERROR"        , ccCmdsPar           , true , fgerror_pars  , "Get        FGERROR   parameter(s)"   },
    { "GLOBAL"         , ccCmdsPar           , true , global_pars   , "Get or set GLOBAL    parameter(s)"   },
    { "ILC"            , ccCmdsPar           , true , ilc_pars      , "Get        ILC       parameter(s)"   },
    { "IREG"           , ccCmdsPar           , true , ireg_pars     , "Get or set IREG      parameter(s)"   },
    { "LIMITS"         , ccCmdsPar           , true , limits_pars   , "Get or set LIMITS    parameter(s)"   },
    { "LOAD"           , ccCmdsPar           , true , load_pars     , "Get or set LOAD      parameter(s)"   },
    { "LOG"            , ccCmdsPar           , true , log_pars      , "Get or set LOG       parameter(s)"   },
    { "LOGCTRL"        , ccCmdsPar           , true , logctrl_pars  , "Get        LOGCTRL   parameter(s)"   },
    { "LOGMENU"        , ccCmdsPar           , true , logmenu_pars  , "Get or set LOGMENU   parameter(s)"   },
    { "LOGMGR"         , ccCmdsPar           , true , logmgr_pars   , "Get        LOGMGR    parameter(s)"   },
    { "LOGMPX"         , ccCmdsPar           , true , logmpx_pars   , "Get or set LOGMPX    parameter(s)"   },
    { "LOGREQ"         , ccCmdsPar           , true , logreq_pars   , "Get        LOGREQ    parameter(s)"   },
    { "LOGSPY"         , ccCmdsPar           , true , logspy_pars   , "Get        LOGSPY    parameter(s)"   },
    { "LOGSTATUS"      , ccCmdsPar           , true , logstatus_pars, "Get        LOGSTATUS parameter(s)"   },
    { "LOGSTR"         , ccCmdsPar           , true , logstr_pars   , "Get        LOGSTR    parameter(s)"   },
    { "MEAS"           , ccCmdsPar           , true , meas_pars     , "Get or set MEAS      parameter(s)"   },
    { "MODE"           , ccCmdsPar           , true , mode_pars     , "Get or set MODE      parameter(s)"   },
    { "POLSWITCH"      , ccCmdsPar           , true , polswitch_pars, "Get or set POLSWITCH parameter(s)"   },
    { "PRE_FUNC"       , ccCmdsPar           , true , pre_func_pars , "Get        PRE_FUNC  parameter(s)"   },
    { "REF"            , ccCmdsPar           , true , ref_pars      , "Get or set REF       parameter(s)"   },
    { "RTREF"          , ccCmdsPar           , true , rtref_pars    , "Get or set RTREF     parameter(s)"   },
    { "SIM"            , ccCmdsPar           , true , sim_pars      , "Get or set SIM       parameter(s)"   },
    { "STATE"          , ccCmdsPar           , true , state_pars    , "Get        STATE     parameter(s)"   },
    { "TEMP"           , ccCmdsPar           , true , temp_pars     , "Get        TEMP      parameter(s)"   },
    { "TRANS"          , ccCmdsPar           , true , trans_pars    , "Get        TRANS     parameter(s)"   },
    { "VFEEDFWD"       , ccCmdsPar           , true , vfeedfwd_pars , "Get or set VFEEDFWD  parameter(s)"   },
    { "VFILTER"        , ccCmdsPar           , true , vfilter_pars  , "Get or set VFILTER   parameter(s)"   },
    { "VREG"           , ccCmdsPar           , true , vreg_pars     , "Get or set VREG      parameter(s)"   },
    { "VS"             , ccCmdsPar           , true , vs_pars       , "Get or set VS        parameter(s)"   },
    { "WARNINGS"       , ccCmdsPar           , true , warnings_pars , "Get        WARNINGS  parameter(s)\n" },
    // Function parameters - alphabetical      SYNC_CMD
    { "CUBEXP"         , ccCmdsPar           , true , cubexp_pars   , "Get or set CUBEXP function parameter(s)" },
    { "PLEP"           , ccCmdsPar           , true , plep_pars     , "Get or set PLEP   function parameter(s)" },
    { "PPPL"           , ccCmdsPar           , true , pppl_pars     , "Get or set PPPL   function parameter(s)" },
    { "PRBS"           , ccCmdsPar           , true , prbs_pars     , "Get or set PRBS   function parameter(s)" },
    { "PULSE"          , ccCmdsPar           , true , pulse_pars    , "Get or set PULSE  function parameter(s)" },
    { "RAMP"           , ccCmdsPar           , true , ramp_pars     , "Get or set RAMP   function parameter(s)" },
    { "TABLE"          , ccCmdsPar           , true , table_pars    , "Get or set TABLE  function parameter(s)" },
    { "TEST"           , ccCmdsPar           , true , test_pars     , "Get or set TEST   function parameter(s)" },
    { "TRIM"           , ccCmdsPar           , true , trim_pars     , "Get or set TRIM   function parameter(s)" },
    { NULL }
}
#endif
;

// Static inline function definitions

static inline bool ccCmdsIsParameter(const uint32_t command_index)
{
    return cmds[command_index].cmd_func == ccCmdsPar;
}

static inline bool ccCmdsIsSyncCmd(const uint32_t command_index)
{
    return cmds[command_index].sync_cmd;
}

// Static assertions

#ifdef GLOBALS

// Make sure that the enum matches the actual size of commands array

CC_STATIC_ASSERT(CC_ARRAY_LEN(cmds) == NUM_CMDS + 1, mismatch_between_cmds_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
