//! @file  ccrt/inc/ccRt.h
//!
//! @brief ccrt header file for ccRt.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!
//! @mainpage CERN Converter Control Library: Real-time Test Program
//!
//! <b>ccrt</b> was written to test the CERN converter control libraries. It can run test files in batch mode
//! or it can be used interactively. It can create log files in PowerSpy CSV format.
//!
//! It can generate a DEBUG log with many signals that are useful when trying to understand the behavior
//! of libref, libreg and selectors in libsig. Many of the signals in the DEBUG log are spread out between -8 and +6,
//! so that they will be visible with exponential scaling enabled in PowerSpy:
//!
//! @image html DEBUG_Signals_Part_1.png
//!
//! Here is the interpretation of the values:
//!
//! @image html DEBUG_Signals_Part_2.png
//!
//! The POLSWITCH log also has coded values:
//!
//! @image html POLSWITCH_Signals.png


#pragma once

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCRT_EXT
#else
#define CCRT_EXT extern
#endif

// Constants

// TODO Comment and remove unused macros

#define CC_PATH_LEN                 256
#define CC_MAX_SUPER_CYC_LEN        32                  // Max number of cycles in the simulated supercycle
#define CC_NUM_SUB_SELS             5                   // Max number of sub devices
#define CC_MAX_CYC_SEL              32                  // Max cycle selector
#define CC_NUM_CYC_SELS             33                  // CC_MAX_CYC_SEL + 1
#define CC_ITER_PERIOD_NS           1000000             // Default iter period: 1 ms -> 1 kHz
#define CC_EVENT_LOG_PERIOD_ITERS   5                   // Default event log thread period in iterations
#define CC_BATCH_ACCELERATION       10                  // Default acceleration factor when reading script file in batch mode
#define CC_FILTER_BUF_LEN           3000                // Max length of FIR and Extrapolation buffers
#define CC_EVENT_LOG_LENGTH         200                 // Lines
#define CC_MAX_DAC_V_ERROR          0.5                 // DAC calibration voltage tolerance
#define CC_CAL_NUM_SAMPLES          10000               // Number of samples averaged for ADC or DCCT calibration
#define CC_MAX_INVALID_SAMPLES      500                 // Max invalid samples during averaging for ADC or DCCT calibration
#define CC_ILC_MAX_SAMPLES          1000                // Max length of ILC reference buffer

//! ccrt flags

struct CC_rt_flags
{
    bool verbose;                  //!< Enables printing of additional debugging info
    bool batch;                    //!< Indicates that the program is running in batch mode
    bool no_config;                //!< Suppress initialization of config parameters (this the default for batch mode)
    bool initialized;              //!< Indicates that the initialization process has been completed
};

CCRT_EXT struct CC_rt_flags  ccrt_flags;

// Static inline function definitions

static inline bool ccRtModeIsVerbose(void)
{
    return ccrt_flags.verbose;
}

static inline bool ccRtModeIsBatch(void)
{
    return ccrt_flags.batch;
}

static inline bool ccRtStateIsInitialised(void)
{
    return ccrt_flags.initialized;
}

// Function declarations

void     ccRtWaitForRefStateMachine (void);
void     ccRtArmRef                 (uint32_t sub_sel, uint32_t cyc_sel, uint32_t num_pars, cc_float const * par_values);

//! Prints error message
//!
//! Message format:
//! * 'ERROR - {message}\n' when reading from stdin
//! * 'ERROR at {file_name}:{line} - {message}\n' when reading from a file
//!
//! File name and line number are deduced by the function. A newline is added at the
//! end of the message.
//!
//! @param[in]  format  Format string similar to printf format
//! @param[in]  ...     List of arguments corresponding to the format string

void ccRtPrintError(const char * const format, ...);

//! Prints error message and exits the program with failure code
//!
//! The message format is the same as in ccRtPrintError.
//!
//! @param[in]  format  Format string similar to printf format
//! @param[in]  ...     List of arguments corresponding to the format string

void ccRtPrintErrorAndExit(const char * const format, ...);

//! Prints warning message
//!
//! Message format:
//! * 'WARNING - {message}\n' when reading from stdin
//! * 'WARNING at {file_name}:{line} - {message}\n' when reading from a file
//!
//! File name and line number are deduced by the function. A newline is added at the
//! end of the message.
//!
//! @param[in]  format  Format string similar to printf format
//! @param[in]  ...     List of arguments corresponding to the format string

void ccRtPrintWarning(const char * const format, ...);

//! Prints message in verbose mode
//!
//! Message format:
//! * '{message}\n' when reading from stdin
//! * '{file_name}:{line_number} - {message}\n' when reading from a file
//!
//! File name and line number are deduced by the function. A newline is added at the
//! end of the message.
//!
//! @param[in]  format  Format string similar to printf format
//! @param[in]  ...     List of arguments corresponding to the format string

void ccRtPrintInVerbose(const char * const format, ...);


// EOF
