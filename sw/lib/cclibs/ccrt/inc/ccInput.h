//! @file  ccrt/inc/ccInput.h
//!
//! Input abstraction layer
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCINPUT_EXT
#else
#define CCINPUT_EXT extern
#endif

#include "ccFile.h"

// Constants

#define CC_INPUT_NESTED_FILES_LIMIT  4    //!< Max number of nested file reads
#define CC_INPUT_STDIN_INDEX        -1    //!< stdin input index on the input stack
#define CC_INPUT_PRIMARY_FILE_INDEX  0    //!< Primary input file index on the input stack

// Structure definitions

//! Contains info about input files

struct CC_input_info
{
    uint32_t line_number;          //!< Current line in input file
    char     path[CC_PATH_LEN];    //!< Input file path
};

//! Input stack

struct CC_input
{
    int_fast8_t          index;                                //!< -1 = stdin, 0-3 = nested READs from files
    struct CC_input_info file[CC_INPUT_NESTED_FILES_LIMIT];    //!< Input file info
};

CCFILE_EXT struct CC_input input
#ifdef GLOBALS
= {
    .index = -1,    // Start reading from stdin
}
#endif
;

// Static inline function definitions

static inline bool ccInputIsStdin(void)
{
    return input.index == CC_INPUT_STDIN_INDEX;
}

static inline bool ccInputIsFile(void)
{
    return input.index >= CC_INPUT_PRIMARY_FILE_INDEX;
}

static inline bool ccInputIsPrimaryFile(void)
{
    return input.index == CC_INPUT_PRIMARY_FILE_INDEX;
}

static inline bool ccInputIsSecondaryFile(void)
{
    return input.index > CC_INPUT_PRIMARY_FILE_INDEX;
}

//! Pushes new input file onto the stack

static inline uint32_t ccInputPush(const char * const path)
{
    if(input.index < (CC_INPUT_NESTED_FILES_LIMIT - 1))
    {
        input.index++;
        input.file[input.index].line_number = 1;
        ccFilePrintPath(input.file[input.index].path, "%s", path);

        return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}

//! Pops input file from the stack

static inline uint32_t ccInputPop(void)
{
    if(input.index > CC_INPUT_STDIN_INDEX)
    {
        input.file[input.index].line_number = 0;
        input.file[input.index].path[0]     = '\0';
        input.index--;

        return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}

//! Returns number of open files on the stack

static inline uint32_t ccInputGetNumberOfFiles(void)
{
    return input.index + 1;
}

//! Returns current file index

static inline int_fast8_t ccInputGetCurrentIndex(void)
{
    return input.index;
}

//! Increments line number in current input file

static inline void ccInputIncrementLineNumber(void)
{
    input.file[input.index].line_number++;
}

//! Returns current line number in input file

static inline uint32_t ccInputGetLineNumber(const uint32_t index)
{
    assert(index < CC_INPUT_NESTED_FILES_LIMIT);

    return input.file[index].line_number;
}

//! Returns string with input file path

static inline const char * ccInputGetPath(const uint32_t index)
{
    assert(index < CC_INPUT_NESTED_FILES_LIMIT);

    return input.file[index].path;
}

// EOF
