//! @file  ccrt/inc/ccPars.h
//!
//! @brief ccrt header file for ccPars.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <time.h>

#include "libref.h"
#include "ccRt.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_EXT
#else
#define CCPARS_EXT extern
#endif

// Constants

#define PARS_INDENT                 34
#define PARS_MAX_PRINT_LINE_LEN     (CC_MAX_FILE_LINE_LEN*8)      // Allow for longest print line for table
#define PARS_INT_FORMAT             " % d"
#define PARS_UINT_FORMAT            "  %u"
#define PARS_LONGUINT_FORMAT        "  %lu"
#define PARS_USHORT_FORMAT          "  %hu"
#define PARS_HEX_FORMAT             "  0x%X"
#define PARS_FLOAT_FORMAT           " % .6E"
#define PARS_DOUBLE_FORMAT          " % .10E"
#define PARS_RO_FLOAT_FORMAT        " % .8E"
#define PARS_TIME_FORMAT            " % .6f"
#define PARS_US_TIME_FORMAT         " %u.%06u"
#define PARS_STRING_FORMAT          "  %s"
#define PARS_POINT_FORMAT           " % .6E|%.6E"
#define PARS_POINTER_FORMAT         "  0x%p"
#define PARS_TIMEVAL_FORMAT         "%04d-%02d-%02d %02d:%02d:%02d.%0*u"

// struct ccpars flags

#define PARS_FIXLEN                 0x0001   //!< Fixed length parameter
#define PARS_RO                     0x0002   //!< Read-only parameter
#define PARS_RW                     0x0004   //!< Read/write parameter
#define PARS_CFG                    0x0008   //!< Config parameter must be saved in file to be non-volatile
#define PARS_REG                    0x0010   //!< Libreg must be notified using regMgrPars() when this parameter is changed
#define PARS_CYC_SEL                0x0020   //!< Parameter is a SUB_SEL array
#define PARS_SUB_SEL                0x0040   //!< Parameter is a SUB_SEL array
#define PARS_STAT                   0x0080   //!< Parameter is a status flag on the status display (see ccStatusFlags())
#define PARS_LOG_MPX                0x0100   //!< Parameter is a log multiplexor
#define PARS_TRANS                  0x0200   //!< Parameter is transactional

// struct ccpars_enum flags

#define CC_ENUM_READ_ONLY           0x0001   //!< Enum value is read-only - user cannot request this value explicitly
#define CC_ENUM_RED                 0x0002   //!< Enum value will be displayed in red on the terminal
#define CC_ENUM_YELLOW              0x0004   //!< Enum value will be displayed in yellow on the terminal
#define CC_ENUM_GREEN               0x0008   //!< Enum value will be displayed in green on the terminal
#define CC_ENUM_CYAN                0x0010   //!< Enum value will be displayed in cyan on the terminal

// Status from ccParsEnumStringToIndex()

#define CC_UNKNOWN_ENUM             -1       //!< ccParsEnumStringToIndex reports unknown enum string
#define CC_AMBIGUOUS_ENUM           -2       //!< ccParsEnumStringToIndex reports ambiguous enum string

// Enums - IMPORTANT this impacts the order of ccpars_test_func[] in ccTest.c and ccpars_sizeof_type[] below.

enum CC_pars_type
{
    PAR_SIGNED    ,
    PAR_UNSIGNED  ,
    PAR_UINTPTR  ,
    PAR_USHORT    ,
    PAR_HEX       ,
    PAR_FLOAT     ,
    PAR_DOUBLE    ,
    PAR_STRING    ,
    PAR_ENUM      ,
    PAR_BOOL      ,
    PAR_REL_TIME  ,
    PAR_ABS_TIME  ,
    PAR_US_TIME   ,
    PAR_BITMASK   ,
    PAR_POINT     ,
    PAR_POINTER   ,
    PAR_FGIDX ,
    PAR_NUM_TYPES ,
};

// Size of type array - must match order of enum CC_pars_type

CCPARS_EXT size_t ccpars_sizeof_type[]
#ifdef GLOBALS
= {
    sizeof(int32_t)            ,  // PAR_SIGNED
    sizeof(uint32_t)           ,  // PAR_UNSIGNED
    sizeof(uintptr_t)          ,  // PAR_UINTPTR
    sizeof(uint16_t)           ,  // PAR_USHORT
    sizeof(uint32_t)           ,  // PAR_HEX
    sizeof(cc_float)           ,  // PAR_FLOAT
    sizeof(cc_double)          ,  // PAR_DOUBLE
    sizeof(char *)             ,  // PAR_STRING
    sizeof(uint32_t)           ,  // PAR_ENUM
    sizeof(bool)               ,  // PAR_BOOL
    sizeof(struct CC_ns_time)  ,  // PAR_REL_TIME
    sizeof(struct CC_ns_time)  ,  // PAR_ABS_TIME
    sizeof(struct CC_us_time)  ,  // PAR_US_TIME
    sizeof(uint32_t)           ,  // PAR_BITMASK
    sizeof(struct FG_point)    ,  // PAR_POINT
    sizeof(void *)             ,  // PAR_POINTER
    sizeof(enum REF_fg_par_idx),  // PAR_FGIDX
}
#endif
;

// Structures and unions

union CC_value_p
{
    uint8_t                *c;    //!< Single character
    int32_t                *i;    //!< Signed integer   (32 bits)
    uint32_t               *u;    //!< Unsigned integer (32 bits)
    uintptr_t              *r;    //!< Integer that can hold an pointer (64 bits)
    uint16_t               *h;    //!< Unsigned short integer (16 bits)
    bool                   *b;    //!< Boolean
    cc_float               *f;    //!< cclibs float
    cc_double              *d;    //!< cclibs double
    struct CC_ns_time      *n;    //!< cclibs nanosecond time
    struct CC_us_time     *us;    //!< cclibs microsecond time
    struct FG_point        *p;    //!< libfg point (contains two floats: time & ref)
    char                  **s;    //!< String
    enum REF_fg_par_idx    *x;    //!< Function Generation parameter index
    void                  **t;    //!< Pointer
};

struct CC_pars
{
    const char                * name;                              //!< Parameter name
    enum CC_pars_type           type;                              //!< Type of value held by parameter
    uint32_t                    max_num_elements;                  //!< Max number of elements (values) that parameter can hold
    const struct CC_pars_enum * ccpars_enum;                       //!< Pointer to ccrt enum structure when parameter type is PAR_ENUM, NULL otherwise
    union CC_value_p            value_p;                           //!< Pointer to parameter value(s)
    uint32_t                    num_default_elements;              //!< Number of elements on program startup
    uint32_t                    flags;                             //!< Mask holding various info about parameter
    uint32_t                    size_of_struct;                    //!< Size of struct if variable is part of a structure
    uint32_t                    array_step;                        //!< Array step in bytes used for accessing parameter values in arrays
    uint32_t                    sub_sel_step;                      //!< Sub-device selector step in bytes used for accessing sub_sel parameters in arrays
    uint32_t                    cyc_sel_step;                      //!< Cycle selector step in bytes used for accessing cyc_sel parameters in arrays

    // Don't move around the values above!

    char                        full_name[CC_PATH_LEN];            //!< Command name + parameter name, separated with a single space
    uint32_t                    name_len;                          //!< Length of parameter name
    uintptr_t                 * num_elements;                      //!< Actual number of elements (values) that parameter holds. For cyc sel parameters
                                                                   //!< it's an array with a separate counter for each selector
    const struct CC_cmds      * parent_cmd;                        //!< Pointer to parent command structure
};

struct CC_pars_enum
{
    uint32_t     value;
    const char * string;
    uint32_t     flags;
};

// Libreg structures - with default parameter values

CCPARS_EXT struct REG_mgr  reg_mgr;

CCPARS_EXT struct REG_pars reg_pars
#ifdef GLOBALS
= {
    .values = {

        // LOAD

        .load_select                  =        0                                                                                         ,    // LOAD SELECT
        .load_test_select             =        0                                                                                         ,    // LOAD TEST_SELECT
        .load_sim_tc_error            =      0.0                                                                                         ,    // LOAD SIM_TC_ERROR

        //                                   4-Q                    2-Q                    1-Q                    4-Q*                         * With low OHMS_PAR
        .load_ohms_ser                = {    0.5,                   0.5,                   0.5,                   0.5                  } ,    // LOAD OHMS_SER
        .load_ohms_par                = {  1.0E9,                 1.0E9,                 1.0E9,                 100.0                  } ,    // LOAD OHMS_PAR
        .load_ohms_mag                = {    0.6,                   0.6,                   0.6,                   0.6                  } ,    // LOAD OHMS_MAG
        .load_henrys                  = {    1.0,                   1.0,                   1.0,                   1.0                  } ,    // LOAD HENRYS
        .load_henrys_sat              = {    1.0,                   1.0,                   1.0,                   1.0                  } ,    // LOAD HENRYS_SAT
        .load_i_sat_gain              = {    1.0,                   1.0,                   1.0,                   1.0                  } ,    // LOAD I_SAT_GAIN
        .load_i_sat_start             = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // LOAD I_SAT_START
        .load_i_sat_end               = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // LOAD I_SAT_END
        .load_sat_smoothing           = {    0.3,                   0.3,                   0.3,                   0.3                  } ,    // LOAD SAT_SMOOTHING
        .load_gauss_per_amp           = {    1.2,                   1.2,                   1.2,                   1.2                  } ,    // LOAD GAUSS_PER_AMP

        // BREG
        //                                   4-Q                    2-Q                    1-Q                    4-Q*                         * With low OHMS_PAR
        .breg_period_iters            = {     10,                    10,                    10,                    10                  } ,    // BREG PERIOD_ITERS
        .breg_external_alg            = {  CC_DISABLED,           CC_DISABLED,           CC_DISABLED,           CC_DISABLED            } ,    // BREG EXTERNAL_ALG
        .breg_int_meas_select         = {  REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED  } ,    // BREG INT_MEAS_SELECT
        .breg_int_pure_delay_periods  = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // BREG EXT_PURE_DELAY_PERIODS
        .breg_int_auxpole1_hz         = {   10.0,                  10.0,                  10.0,                  10.0                  } ,    // BREG EXT_AUXPOLE1_HZ
        .breg_int_auxpoles2_hz        = {   10.0,                  10.0,                  10.0,                  10.0                  } ,    // BREG EXT_AUXPOLES2_HZ
        .breg_int_auxpoles2_z         = {    0.5,                   0.5,                   0.5,                   0.5                  } ,    // BREG EXT_AUXPOLES2_Z
        .breg_int_auxpole4_hz         = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // BREG EXT_AUXPOLE4_HZ
        .breg_int_auxpole5_hz         = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // BREG EXT_AUXPOLE5_HZ

        .breg_ext_meas_select         = {  REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED  } ,    // BREG EXT_MEAS_SELECT
        .breg_ext_track_delay_periods = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // BREG EXT_TRACK_DELAY_PERIODS
        .breg_ext_op_r                = {    0.0    }                                                                                    ,    // BREG EXT_OP_R
        .breg_ext_op_s                = {    0.0    }                                                                                    ,    // BREG EXT_OP_S
        .breg_ext_op_t                = {    0.0    }                                                                                    ,    // BREG EXT_OP_T
        .breg_ext_test_r              = {    0.0    }                                                                                    ,    // BREG EXT_TEST_R
        .breg_ext_test_s              = {    0.0    }                                                                                    ,    // BREG EXT_TEST_S
        .breg_ext_test_t              = {    0.0    }                                                                                    ,    // BREG EXT_TEST_T

        // IREG
        //                                   4-Q                    2-Q                    1-Q                    4-Q*                         * With low OHMS_PAR
        .ireg_period_iters            = {     10,                    10,                    10,                    10                  } ,    // IREG PERIOD_ITERS
        .ireg_external_alg            = {  CC_DISABLED,           CC_DISABLED,           CC_DISABLED,           CC_DISABLED            } ,    // IREG EXTERNAL_ALG
        .ireg_int_meas_select         = {  REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED  } ,    // IREG INT_MEAS_SELECT
        .ireg_int_pure_delay_periods  = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // IREG EXT_PURE_DELAY_PERIODS
        .ireg_int_auxpole1_hz         = {   10.0,                  10.0,                  10.0,                  10.0                  } ,    // IREG EXT_AUXPOLE1_HZ
        .ireg_int_auxpoles2_hz        = {   10.0,                  10.0,                  10.0,                  10.0                  } ,    // IREG EXT_AUXPOLES2_HZ
        .ireg_int_auxpoles2_z         = {    0.5,                   0.5,                   0.5,                   0.5                  } ,    // IREG EXT_AUXPOLES2_Z
        .ireg_int_auxpole4_hz         = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // IREG EXT_AUXPOLE4_HZ
        .ireg_int_auxpole5_hz         = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // IREG EXT_AUXPOLE5_HZ

        .ireg_ext_meas_select         = {  REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED, REG_MEAS_EXTRAPOLATED  } ,    // IREG EXT_MEAS_SELECT
        .ireg_ext_track_delay_periods = {    0.0,                   0.0,                   0.0,                   0.0                  } ,    // IREG EXT_TRACK_DELAY_PERIODS
        .ireg_ext_op_r                = {    0.0    }                                                                                    ,    // IREG EXT_OP_R
        .ireg_ext_op_s                = {    0.0    }                                                                                    ,    // IREG EXT_OP_S
        .ireg_ext_op_t                = {    0.0    }                                                                                    ,    // IREG EXT_OP_T
        .ireg_ext_test_r              = {    0.0    }                                                                                    ,    // IREG EXT_TEST_R
        .ireg_ext_test_s              = {    0.0    }                                                                                    ,    // IREG EXT_TEST_S
        .ireg_ext_test_t              = {    0.0    }                                                                                    ,    // IREG EXT_TEST_T

        // VREG

        .vreg_ext_k_int               =      0.0                                                 ,    // VREG K_INT
        .vreg_ext_k_p                 =      0.0                                                 ,    // VREG K_P
        .vreg_ext_k_ff                =      1.0                                                 ,    // VREG K_FF

        // VFILTER

        .vfilter_v_meas_source        =      REG_MEASUREMENT                                     ,    // VFILTER V_MEAS_SOURCE
        .vfilter_i_capa_source        =      REG_MEASUREMENT                                     ,    // VFILTER I_CAPA_SOURCE
        .vfilter_i_capa_filter        = {    1.0,      0.0    }                                  ,    // VFILTER I_CAPA_FILTER
        .vfilter_ext_k_u              =      0.0                                                 ,    // VFILTER K_U
        .vfilter_ext_k_i              =      0.0                                                 ,    // VFILTER K_I
        .vfilter_ext_k_d              =      0.0                                                 ,    // VFILTER K_D

        // LIMITS
        //                                4-Q          2-Q          1-Q          4-Q*               * With low OHMS_PAR
        .limits_b_pos              = {   12.0,        12.0,        12.0,        12.0        } ,    // LIMITS B_POS
        .limits_b_standby          = {    0.0,         1.0,         1.0,         0.0        } ,    // LIMITS B_STANDBY
        .limits_b_neg              = {  -12.0,         0.0,         0.0,       -12.0        } ,    // LIMITS B_NEG
        .limits_b_rate             = {   12.0,        12.0,        12.0,        12.0        } ,    // LIMITS B_RATE
        .limits_b_closeloop        = {    0.0,         0.5,         0.5,         0.0        } ,    // LIMITS B_CLOSELOOP
        .limits_b_low              = {    0.0,         0.1,         0.1,         0.0        } ,    // LIMITS B_LOW
        .limits_b_zero             = {    0.0,        0.01,        0.01,         0.0        } ,    // LIMITS B_ZERO
        .limits_b_err_warning      = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS B_ERR_WARNING
        .limits_b_err_fault        = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS B_ERR_FAULT
        .limits_i_pos              = {   10.0,        10.0,        10.0,        10.0        } ,    // LIMITS I_POS
        .limits_i_standby          = {    0.0,         1.0,         1.0,         0.0        } ,    // LIMITS I_STANDBY
        .limits_i_neg              = {  -10.0,         0.0,         0.0,       -10.0        } ,    // LIMITS I_NEG
        .limits_i_rate             = {   10.0,        10.0,        10.0,        10.0        } ,    // LIMITS I_RATE
        .limits_i_closeloop        = {    0.0,         0.5,         0.5,         0.0        } ,    // LIMITS I_CLOSELOOP
        .limits_i_low              = {    0.0,         0.1,         0.1,         0.0        } ,    // LIMITS I_LOW
        .limits_i_zero             = {    0.0,        0.01,        0.01,         0.0        } ,    // LIMITS I_ZERO
        .limits_i_err_warning      = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS I_ERR_WARNING
        .limits_i_err_fault        = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS I_ERR_FAULT
        .limits_i_rms_load_tc      = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS I_RMS_LOAD_TC
        .limits_i_rms_load_warning = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS I_RMS_LOAD_WARNING
        .limits_i_rms_load_fault   = {    0.0,         0.0,         0.0,         0.0        } ,    // LIMITS I_RMS_LOAD_FAULT

        .limits_i_rms_tc           =      0.0                                                 ,    // LIMITS I_RMS_TC
        .limits_i_rms_warning      =      0.0                                                 ,    // LIMITS I_RMS_WARNING
        .limits_i_rms_fault        =      0.0                                                 ,    // LIMITS I_RMS_FAULT
        //                                4-Q          2-Q          1-Q          4-Q*               * With low OHMS_PAR
        .limits_v_pos              = {  100.0,       100.0,       100.0,       100.0        } ,    // LIMITS V_POS
        .limits_v_neg              = { -100.0,      -100.0,         0.0,      -100.0        } ,    // LIMITS V_NEG

        .limits_v_rate             =   1000.0                                                 ,    // LIMITS V_RATE
        .limits_v_rate_rms_tc      =      0.0                                                 ,    // LIMITS V_RATE_RMS_TC
        .limits_v_rate_rms_fault   =      0.0                                                 ,    // LIMITS V_RATE_RMS_FAULT
        .limits_v_err_warning      =      0.0                                                 ,    // LIMITS V_ERR_WARNING
        .limits_v_err_fault        =      0.0                                                 ,    // LIMITS V_ERR_FAULT

        .limits_i_quadrants41      = {    0.0,   0.0    }                                     ,    // LIMITS I_QUADRANTS41
        .limits_v_quadrants41      = {    0.0,   0.0    }                                     ,    // LIMITS V_QUADRANTS41

        // MEAS

        .meas_b_delay_iters        =      1.3                                                 ,    // MEAS B_DELAY_ITERS
        .meas_i_delay_iters        =      1.3                                                 ,    // MEAS I_DELAY_ITERS
        .meas_v_delay_iters        =      1.3                                                 ,    // MEAS V_DELAY_ITERS

        .meas_b_fir_lengths        = {    0.0,   0.0    }                                     ,    // MEAS B_FIR_LENGTHS
        .meas_i_fir_lengths        = {    0.0,   0.0    }                                     ,    // MEAS I_FIR_LENGTHS

        // VS

        .vs_actuation              =      REG_VOLTAGE_REF                                     ,    // VS ACTUATION
        .vs_act_delay_iters        =      1.0                                                 ,    // VS ACT_DELAY_ITERS
        .vs_num_vsources           =      1                                                   ,    // VS NUM_VSOURCES
        .vs_gain                   =     10.0                                                 ,    // VS GAIN
        .vs_offset                 =      0.0                                                 ,    // VS OFFSET
        .vs_firing_mode            =      REG_CASSEL                                          ,    // VS FIRING_MODE
        .vs_firing_delay           =      0.001                                               ,    // VS FIRING_DELAY
        .vs_sim_bandwidth          =    200.0                                                 ,    // VS SIM_BANDWIDTH
        .vs_sim_z                  =      0.9                                                 ,    // VS SIM_Z
        .vs_sim_tau_zero           =      0.0                                                 ,    // VS SIM_TAU_ZERO
        .vs_sim_num                = {    1.0    }                                            ,    // VS SIM_NUM
        .vs_sim_den                = {    1.0    }                                            ,    // VS SIM_DEN
        .vs_sim2_k                 = {    0.0    }                                            ,    // VS SIM2_K
        .vs_sim2_h                 = {    0.0    }                                            ,    // VS SIM2_H
        .vs_sim2_ma                = {    0.0    }                                            ,    // VS SIM2_MA
        .vs_sim2_mb                = {    0.0    }                                            ,    // VS SIM2_MB
    }
}
#endif
;

//! Libref structure - with default parameter values.


CCPARS_EXT struct REF_mgr ref_mgr
#ifdef GLOBALS
= {
    .pars = {
        .mode_ref_sub_sel         = { CC_ENABLED  },                        // MODE REF_SUB_SEL
        .mode_ref_cyc_sel         = { CC_ENABLED  },                        // MODE REF_CYC_SEL
        .direct_degauss_period    = {  1.0  },                              // DIRECT DEGAUSS_PERIOD

         //                              4-Q     2-Q     1-Q     4-Q
        .default_v_acceleration   = {  100.0,  101.0,  102.0,  103.0 },     // DEFAULT V_ACCELERATION
        .default_v_deceleration   = {   70.0,   71.0,   72.0,   73.0 },     // DEFAULT V_DECELERATION
        .default_v_linear_rate    = {   90.0,   91.0,   92.0,   93.0 },     // DEFAULT V_LINEAR_RATE
        .default_v_pre_func_max   = {   10.0,   10.0,   10.0,   10.0 },     // DEFAULT V_PRE_FUNC_MAX
        .default_v_pre_func_min   = {  -10.0,    0.0,    0.0,  -10.0 },     // DEFAULT V_PRE_FUNC_MIN
        .default_v_minrms         = {    0.0,    0.0,    0.0,    0.0 },     // DEFAULT V_MINRMS

        .default_i_acceleration   = {  100.3,  101.3,  102.3,  103.3 },     // DEFAULT I_ACCELERATION
        .default_i_deceleration   = {   70.3,   71.3,   72.3,   73.3 },     // DEFAULT I_DECELERATION
        .default_i_linear_rate    = {   90.3,   91.3,   92.3,   93.3 },     // DEFAULT I_LINEAR_RATE
        .default_i_pre_func_max   = {    9.0,    9.0,    9.0,    9.0 },     // DEFAULT I_PRE_FUNC_MAX
        .default_i_pre_func_min   = {   -9.0,    0.0,    0.3,   -9.0 },     // DEFAULT I_PRE_FUNC_MIN
        .default_i_minrms         = {    0.0,    0.1,    0.1,    0.0 },     // DEFAULT I_MINRMS

        .default_b_acceleration   = {  100.6,  101.6,  102.6,  103.6 },     // DEFAULT B_ACCELERATION
        .default_b_deceleration   = {   70.6,   71.6,   72.6,   73.6 },     // DEFAULT B_DECELERATION
        .default_b_linear_rate    = {   90.6,   91.6,   92.6,   93.6 },     // DEFAULT B_LINEAR_RATE
        .default_b_pre_func_max   = {   10.0,   10.0,   10.0,   10.0 },     // DEFAULT B_PRE_FUNC_MAX
        .default_b_pre_func_min   = {  -10.0,    0.0,    0.3,  -10.0 },     // DEFAULT B_PRE_FUNC_MIN
        .default_b_minrms         = {    0.0,    0.1,    0.1,    0.0 },     // DEFAULT B_MINRMS

        .default_plateau_duration = {    0.02,   0.05   }                   // DEFAULT PLATEAU_DURATION
    }
}
#endif
;

// ENABLED/DISABLED enum

CCPARS_EXT struct CC_pars_enum enum_enabled_disabled[]
#ifdef GLOBALS
= {
    { CC_DISABLED,     "DISABLED",    CC_ENUM_YELLOW },
    { CC_ENABLED ,     "ENABLED" ,    CC_ENUM_GREEN  },
    { 0          ,      NULL                         },
}
#endif
;

// TRUE/FALSE enum

CCPARS_EXT struct CC_pars_enum enum_true_false[]
#ifdef GLOBALS
= {
    { true ,    "TRUE" ,    CC_ENUM_GREEN   },
    { false,    "FALSE",    CC_ENUM_YELLOW  },
    { 0    ,     NULL                       },
}
#endif
;

// Static inline functions

static inline size_t ccParsGetTypeSize(const enum CC_pars_type par_type)
{
    return ccpars_sizeof_type[par_type];
}

static inline bool ccParsIsFixedLength(struct CC_pars const * const par)
{
    return (par->flags & PARS_FIXLEN) != 0;
}

static inline bool ccParsIsScalar(struct CC_pars const * const par)
{
    return par->max_num_elements == 1;
}

static inline bool ccParsIsSubSel(struct CC_pars const * const par)
{
    return (par->flags & PARS_SUB_SEL) != 0;
}

static inline bool ccParsIsCycSel(struct CC_pars const * const par)
{
    return (par->flags & PARS_CYC_SEL) != 0;
}

static inline bool ccParsIsReadWrite(struct CC_pars const * const par)
{
    return (par->flags & PARS_RW) != 0;
}

static inline bool ccParsIsConfig(struct CC_pars const * const par)
{
    return (par->flags & PARS_CFG) != 0;
}

static inline bool ccParsIsLibreg(struct CC_pars const * const par)
{
    return (par->flags & PARS_REG) != 0;
}

static inline bool ccParsIsStat(struct CC_pars const * const par)
{
    return (par->flags & PARS_STAT) != 0;
}

static inline bool ccParsIsLogMpx(struct CC_pars const * const par)
{
    return (par->flags & PARS_LOG_MPX) != 0;
}

static inline bool ccParsIsEnumValueReadOnly(struct CC_pars const * const par)
{
    return (par->ccpars_enum[*(par->value_p.u)].flags & CC_ENUM_READ_ONLY) != 0;
}

static inline bool ccParsIsType(struct CC_pars const * const par, enum CC_pars_type type)
{
    return par->type == type;
}

// Function declarations

void ccParsFullName (struct CC_pars * par, char * buf, size_t buflen);
uint32_t ccParsLong (const char * arg, long * long_value, int base);
uint32_t ccParsAbsTime (const char * arg, struct CC_ns_time * time_value);
uint32_t ccParsRelTime (const char * arg, struct CC_ns_time * time_value);

//! Performs string to double conversion
//!
//! The function will return an error and print appropriate message on any failure - ill-formed
//! string, under- or overflow, value out of float boundaries.
//!
//! @param[in]   arg           String for conversion
//! @param[out]  double_value  String as double on successful conversion
//!
//! @retval  EXIT_SUCCESS  On successful conversion
//! @retval  EXIT_FAILURE  On failure

uint32_t ccParsDouble(const char * const arg, double * const double_value);

//! Relaxed version of ccParsDouble
//!
//! The function will try to perform string to double conversion, but won't print any
//! error message if the string can't be converted. Otherwise, both functions behave the same way.
//!
//! @param[in]   arg           String for conversion
//! @param[out]  double_value  String as double on successful conversion
//!
//! @retval  EXIT_SUCCESS  On successful conversion
//! @retval  EXIT_FAILURE  On failure

uint32_t ccParsTryDouble(const char * const arg, double * const double_value);

uint32_t     ccParsSet              (const uint32_t cmd_idx, struct CC_pars * const par, char *remaining_line);
int32_t      ccParsEnumValueToIndex (const struct CC_pars_enum * const par_enum, const uint32_t value);
const char * ccParsEnumValueToString(const struct CC_pars_enum * par_enum, const uint32_t value);
int32_t      ccParsEnumStringToIndex(const struct CC_pars * par, char * string, bool test_flag);
void         ccParsPrintAbsTime     (FILE * const f, const struct CC_ns_time time, uint32_t ns_resolution);
void         ccParsPrintBitmask     (FILE * const file, uint32_t const bitmask, struct CC_pars_enum const * const par_enum);
void         ccParsPrintElement     (FILE * const                 f,
                                     struct CC_pars const * const par,
                                     uint32_t                     cyc_sel,
                                     uint32_t                     array_idx);

uint32_t ccParsGetAllMatchingFlags(      FILE           * const f,
                                   const struct CC_cmds * const cmd,
                                   const uint32_t               cyc_sel,
                                   const uint32_t               array_idx,
                                   const uint32_t               flags_mask,
                                   const uint32_t               flags_required,
                                   const uint32_t               cmd_name_width,
                                   const uint32_t               par_name_width);


void ccParsGet(      FILE           * const f,
               const struct CC_pars * const par,
                     uint32_t               cyc_sel,
               const uint32_t               array_idx,
               const uint32_t               cmd_name_width,
                     uint32_t               par_name_width);

uint32_t ccParsGetAll(      FILE           * const f,
                      const struct CC_cmds * const cmd,
                            uint32_t               cyc_sel,
                      const uint32_t               array_idx,
                      const uint32_t               flags_mask,
                      const uint32_t               flags_required);

void     ccParsGetRefArmedPars      (FILE *ref_file, uint32_t sub_sel, uint32_t cyc_sel);

//! This function sets the direct reference with respect to the actual regulation mode.
//!
//! @param[in]    direct_ref    Final reference value
//!
//! @retval    EXIT_SUCCESS    On success
//! @retval    EXIT_FAILURE    On failure

uint32_t ccParsSetDirectRef(double direct_ref);

//! Returns current iteration time and the offset contained by the string argument
//!
//! If string argument contains NULL, returned offset will equal 0.0.
//!
//! @param[out]     iter_time       Time of current iteration (s, ns)
//! @param[out]     offset_time     Offset (s,ns) taken from remaining line or 0 if the string is NULL
//! @param[in,out]  remaining_line  Remaining argument line
//!
//! @retval  EXIT_SUCCESS  On success
//! @retval  EXIT_FAILURE  On failure

uint32_t ccParsGetIterTimeAndOffset(struct CC_ns_time * const iter_time,
                                    struct CC_ns_time * const offset_time,
                                    char              **      remaining_line);

//! Returns sleep time offset by the value contained by the string argument
//!
//! If remaining line contains NULL, the iteration time will be advanced by 1s.
//!
//! The function performs additional checks on the offset argument. It will fail if the offset is less than 0,
//! greater than 10000s or the offset iteration time is already in the past.
//!
//! @param[out]     offset_time     Time of current iteration (s,ns) plus offset taken from the string argument
//! @param[in,out]  remaining_line  Remaining argument line
//!
//! @retval  EXIT_SUCCESS  On success
//! @retval  EXIT_FAILURE  On failure

uint32_t ccParsGetSleepOffset(struct CC_ns_time * const offset_time, char ** remaining_line);

// Static assertions

#ifdef GLOBALS

// Make sure that the type size array has the same number of elements as the corresponding enum
CC_STATIC_ASSERT(CC_ARRAY_LEN(ccpars_sizeof_type) == PAR_NUM_TYPES, mismatch_of_ccpars_sizeof_type_array_size_and_enum);

#endif // GLOBALS

// EOF
