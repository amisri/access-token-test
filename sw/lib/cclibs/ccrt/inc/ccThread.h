//! @file  ccrt/inc/ccThread.h
//!
//! @brief ccrt header file for ccThread.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <pthread.h>

#include "cclibs.h"
#include "ccCmds.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCTHREAD_EXT
#else
#define CCTHREAD_EXT extern
#endif

// Timer variables

struct CC_timer
{
    double                  iter_time;                  // ccrt iteration time
    double                  period;                     // ccrt iteration period (may be accelerated)
    uint32_t                acceleration;               // ccrt acceleration factor
    uint32_t                event_log_period_iters;     // Event log thread period in iterations
    uint32_t                iter_counter;               // Iteration counter to trigger event log thread
    pthread_mutex_t         timer_mutex;                // Mutex to use with freeze_rt
    bool                    freeze_rt;                  // Tells the timer thread to skip waking-up the real-time thread
};

CCTHREAD_EXT struct CC_timer    timer;                  // Timer variables

// Thread variables

struct CC_thread
{
    char                   *label;                      // Pointer to thread label
    pthread_t               thread;                     // Thread
    pthread_attr_t          thread_attr;                // Thread attribute
    struct sched_param      thread_param;               // Thread scheduling parameters
    pthread_mutex_t         condition_mutex;            // Mutex to use with condition variable
    pthread_cond_t          condition_cond;             // Condition variable
    uint32_t                missed_counter;             // Missed iterations counter
    bool                    wake_up_flag;               // Wake up flag
};

// Structure for all ccrt threads

struct CC_threads
{
    struct CC_thread        timer;                      // [0] Timer thread
    struct CC_thread        real_time;                  // [1] Real-time processing thread
    struct CC_thread        event_log;                  // [2] Event log thread
    struct CC_thread        log_writing;                // [3] Thread that will writing the analog log files
    struct CC_thread        read_cmds;                  // [4] Thread for command processing
};

CCTHREAD_EXT struct CC_threads cc_threads
#ifdef GLOBALS
= {
    .real_time       = { .condition_mutex = PTHREAD_MUTEX_INITIALIZER,
                         .condition_cond  = PTHREAD_COND_INITIALIZER
                       },
    .event_log       = { .condition_mutex = PTHREAD_MUTEX_INITIALIZER,
                         .condition_cond  = PTHREAD_COND_INITIALIZER
                       },
    .log_writing     = { .condition_mutex = PTHREAD_MUTEX_INITIALIZER,
                         .condition_cond  = PTHREAD_COND_INITIALIZER
                       },
}
#endif
;

// Function prototypes

void      ccThreadDetectRtPreempt (void);
void      ccThreadStart           (char *label, struct CC_thread *thread, void *(*function)(void *), int32_t priority);
void      ccThreadTryWake         (struct CC_thread *thread);
void      ccThreadwaitFor         (enum CC_cmds_wait_commands wait_cmd, const cc_double timeout, const char * const label);
void      ccThreadWaitUntil       (enum CC_cmds_wait_commands wait_cmd, const struct CC_ns_time func_time_s, const struct CC_ns_time wait_time_s, const char * const label);
int       ccThreadWait            (void);
void      ccThreadPrintMissed     (struct CC_thread *thread);
void     *ccThreadTimerThread     (void *args);

struct CC_ns_time ccThreadGetIterTime(void);

// Function prototype from ccTime.c - this source file comes from http://NadeauSoftware.com/

double  getRealTime             (void);

// EOF
