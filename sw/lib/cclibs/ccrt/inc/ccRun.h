//! @file  ccrt/inc/ccRun.h
//!
//! @brief ccrt header file for ccRun.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <time.h>
#include <signal.h>
#include <pthread.h>

#include "ccTest.h"
#include "ccLog.h"

// Constants

#define ACQ_DISC_INTERPOLATION_STEPS    10
#define DIM_NUM_SAMPLES                 10
#define DIM_ANA_A_TIME_OFFSET_MS        3
#define DIM_ANA_B_TIME_OFFSET_MS        4
#define DIM_ANA_C_TIME_OFFSET_MS        5
#define DIM_ANA_D_TIME_OFFSET_MS        6
#define DIM_STORE_TIME_OFFSET_MS        19
#define WAIT_FUNC_TIME_NOT_SET          -1000000

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCRUN_EXT
#else
#define CCRUN_EXT extern
#endif

// Real-time iteration variables

struct CC_run_vars
{
    uint32_t                        iteration_counter;                  // Accumulates the number of real-time iterations for debugging purposes
    int32_t                         acq_disc_counter;                   // Counts from start of cycle

    struct CC_ns_time               start_time;                         // Time of first iteration (s, ns)
    pthread_mutex_t                 evtlog_mutex;
    pthread_mutex_t                 arm_event_mutex;                    // Mutex to protect refArm and refEvent from being run at the same time
    pthread_mutex_t                 reg_pars_mutex;                     // Mutex to protect regParsCheck() and regParsProcess() from being run simultaneously
    pthread_mutex_t                 iter_time_mutex;                    // Mutex to protect iter_time
    struct CC_ns_time               iter_time;                          // Iteration time (s, ns)
    struct CC_us_time               iter_us_time;                       // Iteration time (s, us)
    struct CC_ns_time               iter_200ms_timestamp;               // Time of most recent 200ms boundary (s, ns)
    struct CC_us_time               evtlog_time;                        // Event log iteration time (s,us)
    uint32_t                        iter_ns_time_mod_200Mns;            // Iteration ns time modulo 200000000
    uint32_t                        iter_ns_time_mod_10T;               // Iteration ns time modulo 10 x iter_period
    uint32_t                        iter_ms_time_mod_20;                // Iteration ms time modulo 20
    bool                            iter_200ms_boundary;                // Boolean indicating 200ms boundary
    bool                            iter_1s_boundary;                   // Boolean indicating 1s boundary
    bool                            cycle_running;                      // True when the time has passed the event time

    // Libref function generator parameter arrays

    enum REF_fg_par_idx             fg_pars_idx[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS][REF_FG_PAR_MAX_PER_GROUP]; // Null terminal array of libref fg parameter indexes of armed or restored parameters

    struct CC_pars                * fg_pars_link[REF_NUM_FG_PARS];      // Array linking libref fg parameter indexes to ccrt parameter structures

    // Test assertions

    struct CC_run_test
    {
        uint32_t                    counter;                            // Test counter for Test Anything Protocol messages
        uint32_t                    not_ok_counter;                     // Not ok test counter
        pthread_mutex_t             mutex;                              // Mutex to protect the counter and output messages
    } test;

    // Signal Logging

    struct CC_run_log
    {
        enum LOG_read_action        action;                             // Log read action: (Get SPY or PM_BUF log data)
        char                      * dir_name;                           // Optional directory name for SPY logs (NULL for automatic name)
        uint32_t                    cyc_sel;                            // Cycle selector for the WRITELOGS command
        bool                        temp_meas_ready;                    // TODO Add proper comment
        bool                        in_progress;                        // Logging in progress flag
        enum REF_event_type         event_type;                         // Event type to be logged
        pthread_mutex_t             event_type_mutex;                   // Mutex to guarantee logging of event type in the DEBUG log

        struct CC_run_log_debug
        {
            cc_float                reg_mode;                           // DEBUG REG_MODE
            cc_float                ref_state;                          // DEBUG REF_STATE
            cc_float                fg_type;                            // DEBUG FG_TYPE
            cc_float                fg_status;                          // DEBUG FG_STATUS
            cc_float                ramp_mgr_status;                    // DEBUG RAMP_MGR_STATUS
            cc_float                cycle_status;                       // DEBUG CYCLE_STATUS
            cc_float                dcct_user_sel;                      // DEBUG DCCT_USER_SEL
            cc_float                dcct_act_sel;                       // DEBUG DCCT_ACT_SEL
            cc_float                event_type;                         // DEBUG EVENT_TYPE
            cc_float                cyc_sel;                            // DEBUG CYC_SEL
            cc_float                iter_time_ns;                       // DEBUG TIMESTAMP
            cc_float                iter_index_i;                       // DEBUG ITER_INDEX_I
            cc_float                iter_index_b;                       // DEBUG ITER_INDEX_B
        } debug;

        struct CC_run_polswitch
        {
            cc_float                mode_user;                          // POLSWITCH MODE_USER
            cc_float                mode_auto;                          // POLSWITCH MODE_AUTO
            cc_float                req_state;                          // POLSWITCH REQ_STATE
            cc_float                exp_state;                          // POLSWITCH EXP_STATE
            cc_float                state;                              // POLSWITCH STATE
            cc_float                time_remaining;                     // POLSWITCH TIME_REMAINING
        } polswitch;
    } log;

    // Discontinuous signals for logging

    struct CC_run_acq_disc
    {
        cc_float                    last_value;                         // Last measurement
        cc_float                    interpolated[ACQ_DISC_INTERPOLATION_STEPS]; // Buffer for interpolated ACQ_DISC
    } acq_disc[LOG_ACQ_DISC_NUM_SIG_BUFS];

    // DIM logging

    struct CC_dim_log
    {
        uint32_t                    chan_recorded_mask;                 // Bits 0-3 latch sampling channels A-D
        bool                        dc_recording;                       // True when discontinuous recording is active
        uint32_t                    dc_counter;                         // Counter of discontinuous samples recorded
        uint32_t                    dc_num_samples;                     // Number of samples per cycle in the discontinuous log
        struct CC_ns_time           dim_sample_time;                    // Time stamp for latest sample (s, ns)
//        cc_double                   dim_sample_time_s;                  // Time stamp for latest sample
        cc_float                    time_offset_a;                      // Time offset for channel A
        cc_float                    time_offset_b;                      // Time offset for channel B
        cc_float                    time_offset_c;                      // Time offset for channel C
        cc_float                    time_offset_d;                      // Time offset for channel D
        cc_float                    ana_a;                              // DIM Channel A sample
        cc_float                    ana_b;                              // DIM Channel B sample
        cc_float                    ana_c;                              // DIM Channel C sample
        cc_float                    ana_d;                              // DIM Channel D sample
    } dim;

    // Command wait

    struct CC_run_wait
    {
        struct CC_ns_time           last_wakeup_iter_time;              // Iteration time on which the command thread was woken up last time
        uint32_t                    last_wakeup_iteration_counter;      // Iteration counter on which the command thread was woken up last time
        struct CC_ns_time           func_time;                          // Function time for WaitFuncTime to end (0 to wait for end of func)
        struct CC_ns_time           wait_time;                          // Iteration time for WAIT to end
        enum CC_cmds_wait_commands  cmd;                                // Command that requested the wait
        struct CC_test              test;                               // Parameter to test during WAIT
        char                      * sync_command;                       // Pointer to command line following sync command
        uint32_t                    sync_command_exit_status;           // Exit status from sync command
        bool                        active;                             // Wait active flag
        pthread_mutex_t             condition_mutex;                    // Mutex to use with condition variable
        pthread_cond_t              condition_cond;                     // Condition variable
        cc_float                    log_value;                          // Value logged in DEBUG WAIT_CMD signal
    } wait;

    // Real-time reference generator

    struct CC_real_time
    {
        uint32_t                    iter_counter;                       // Sampling iteration counter
        double                      w;                                  // Radians for Sine wave
    } real_time;

    // Cycle events

    struct CC_run_cycle
    {
        pthread_mutex_t             mutex;                              // Mutex to protect the cycle structure
        struct REF_event            active_event;                       // Active event
    } cycle;

    struct CC_meas_signal           b_meas;                             // Field measurement
    struct CC_meas_signal           i_meas;                             // Current measurement
    struct CC_meas_signal           v_meas;                             // Voltage measurement
};

CCRUN_EXT struct CC_ns_time wait_func_time_not_set
#ifdef GLOBALS
 = {
    { WAIT_FUNC_TIME_NOT_SET },   // secs.rel
    0                             // ns
}
#endif
;

CCRUN_EXT struct CC_run_vars ccrun
#ifdef GLOBALS
= {
        .iter_time_mutex = PTHREAD_MUTEX_INITIALIZER,
        .reg_pars_mutex  = PTHREAD_MUTEX_INITIALIZER,
        .arm_event_mutex = PTHREAD_MUTEX_INITIALIZER,
        .evtlog_mutex    = PTHREAD_MUTEX_INITIALIZER,
        .test            = { .mutex           = PTHREAD_MUTEX_INITIALIZER },
        .wait            = { .condition_mutex = PTHREAD_MUTEX_INITIALIZER,
                             .condition_cond  = PTHREAD_COND_INITIALIZER,
                             .func_time       = { { WAIT_FUNC_TIME_NOT_SET }, 0 } },
        .cycle           = { .mutex           = PTHREAD_MUTEX_INITIALIZER },
        .real_time       = { .iter_counter    = 0xFFFFFFFE },
        .log             = { .event_type_mutex = PTHREAD_MUTEX_INITIALIZER }
}
#endif
;



static inline void ccRunLogEventStart(const enum REF_event_type event_type)
{
    // Record event type for logging in the DEBUG log using a mutex for protection

    pthread_mutex_lock(&ccrun.log.event_type_mutex);

    ccrun.log.event_type = event_type;

    pthread_mutex_unlock(&ccrun.log.event_type_mutex);
}

// Function prototypes

void   *ccRunRealTimeThread   (void *args);

// EOF
