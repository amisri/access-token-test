//! @file  ccrt/inc/pars/vfeedfwd.h
//!
//! @brief ccrt GLOBAL parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where vfeedfwd variables should be defined

#ifdef GLOBALS
#define CCPARS_VFEEDFWD_EXT
#else
#define CCPARS_VFEEDFWD_EXT extern
#endif

// Include header files to get enum structures

#include "pars/fgerror.h"

// VFEEDFWD parameter indexes

enum CC_vfeedfwd_pars_index_enum
{
    VFEEDFWD_GAIN             ,
    VFEEDFWD_DELAY_PERIODS    ,
    VFEEDFWD_DELAY_NUM        ,
    VFEEDFWD_DELAY_DEN        ,
    VFEEDFWD_NUM_PARS
};

//! VFEEDFWD parameters description structure

CCPARS_VFEEDFWD_EXT struct CC_pars vfeedfwd_pars[]
#ifdef GLOBALS
= {// name             type          max_n_els            *enum        *value                                                 n_els                flags
    { "GAIN"         , PAR_FLOAT,    1,                   NULL, { .f = refMgrParPointer(&ref_mgr, VFEEDFWD_GAIN)          }, 1,                   PARS_RW|PARS_CFG                      },
    { "DELAY_PERIODS", PAR_FLOAT,    1,                   NULL, { .f = refMgrParPointer(&ref_mgr, VFEEDFWD_DELAY_PERIODS) }, 1,                   PARS_RW|PARS_CFG                      },
    { "NUM"          , PAR_FLOAT,    REG_SIM_NUM_DEN_LEN, NULL, { .f = regMgrParAppValue(&reg_pars, VFEEDFWD_NUM)         }, REG_SIM_NUM_DEN_LEN, PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "DEN"          , PAR_FLOAT,    REG_SIM_NUM_DEN_LEN, NULL, { .f = regMgrParAppValue(&reg_pars, VFEEDFWD_DEN)         }, REG_SIM_NUM_DEN_LEN, PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(vfeedfwd_pars) == VFEEDFWD_NUM_PARS + 1, mismatch_between_vfeedfwd_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
