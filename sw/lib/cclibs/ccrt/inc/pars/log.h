//! @file  ccrt/inc/pars/log.h
//!
//! @brief ccrt LOG parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "logmenu.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOG_EXT
#else
#define CCPARS_LOG_EXT extern
#endif

// Constants

#define LOG_PATH                    "../../logs/%s/%s/%s/%s"

//! LOG parameters structure

struct CC_pars_log
{
    struct CC_ns_time        time_origin;         //!< Log time origin (zero for most recent data)
    uint32_t                 cyc_sel;             //!< Cycle selector to log automatically
    uint32_t                 sub_sampling_period; //!< Sub-sampling period in units of the log period (0 = automatic, 1-N = required sub-sampling period)
    struct LOG_read_timing   read_timing;         //!< Read request timing
    enum CC_enabled_disabled prev_cycle;          //!< Enabled:Log previous cycle.  Disabled:use LOG CYC_SEL to set cycle to be logged.
    enum CC_enabled_disabled overwrite;           //!< Enabled:each log is to a separate folder. Disabled:all logs written to top level log folder.
    enum CC_enabled_disabled csv_spy;             //!< Enables/disables creation of CSV Spy file on each writelog
    enum CC_enabled_disabled binary_spy;          //!< Enables/disables creation of binary Spy file on each writelog
    enum CC_enabled_disabled pm_buf;              //!< Enables/disables PM_BUF generation when converter goes OFF
    enum CC_enabled_disabled internal_tests;      //!< Enables/disables internal tests of log read control and header or PM_BUF
};

//! Instance of LOG parameters structure

CCPARS_LOG_EXT struct CC_pars_log ccpars_log
#ifdef GLOBALS
= {
    .time_origin          = { { 0 }, 0 },   // LOG TIME_ORIGIN
    .cyc_sel              = 0,              // LOG CYC_SEL
    .sub_sampling_period  = 0,              // LOG SUB_SAMPLING_PERIOD
    .prev_cycle           = CC_DISABLED,    // LOG PREV_CYCLE
    .overwrite            = CC_ENABLED,     // LOG OVERWRITE
    .csv_spy              = CC_ENABLED,     // LOG CSV_SPY
    .binary_spy           = CC_DISABLED,    // LOG BINARY_SPY
    .pm_buf               = CC_DISABLED,    // LOG PM_BUF
    .internal_tests       = CC_DISABLED,    // LOG INTERNAL_TESTS
}
#endif
;

//! LOG parameters description structure

CCPARS_LOG_EXT struct CC_pars log_pars[]
#ifdef GLOBALS
= {// name                    type          max_n_els  *enum                         *value                                  n_els flags
    { "TIME_ORIGIN"         , PAR_ABS_TIME, 1,         NULL,                  { .n = &ccpars_log.time_origin                }, 1,  PARS_RW },
    { "TIME_OFFSET_MS"      , PAR_SIGNED,   1,         NULL,                  { .i = &ccpars_log.read_timing.time_offset_ms }, 1,  PARS_RW },
    { "DURATION_MS"         , PAR_UNSIGNED, 1,         NULL,                  { .u = &ccpars_log.read_timing.duration_ms    }, 1,  PARS_RW },
    { "MAX_BUFFER_SIZE"     , PAR_UNSIGNED, 1,         NULL,                  { .u = &log_menus.max_buffer_size             }, 1,  PARS_RW },
    { "CYC_SEL"             , PAR_UNSIGNED, 1,         NULL,                  { .u = &ccpars_log.cyc_sel                    }, 1,  PARS_RW },
    { "SUB_SAMPLING_PERIOD" , PAR_UNSIGNED, 1,         NULL,                  { .u = &ccpars_log.sub_sampling_period        }, 1,  PARS_RW },
    { "PREV_CYCLE"          , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.prev_cycle                 }, 1,  PARS_RW },
    { "OVERWRITE"           , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.overwrite                  }, 1,  PARS_RW },
    { "CSV_SPY"             , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.csv_spy                    }, 1,  PARS_RW },
    { "BINARY_SPY"          , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.binary_spy                 }, 1,  PARS_RW },
    { "PM_BUF"              , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.pm_buf                     }, 1,  PARS_RW },
    { "INTERNAL_TESTS"      , PAR_ENUM,     1,         enum_enabled_disabled, { .u = &ccpars_log.internal_tests             }, 1,  PARS_RW },
    { NULL }
}
#endif // GLOBALS
;

// EOF
