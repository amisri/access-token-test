//! @file  ccrt/inc/pars/fgerror.h
//!
//! @brief ccrt FG ERROR parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "pars/ref.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_FGERROR_EXT
#else
#define CCPARS_FGERROR_EXT extern
#endif

//! Used for FGERROR ERRNO parameter value decoding

CCPARS_FGERROR_EXT struct CC_pars_enum enum_fgerror_errno[]
#ifdef GLOBALS
= {
    { FG_OK                 , "OK"                 },
    { FG_BAD_ARRAY_LEN      , "BAD_ARRAY_LEN"      },
    { FG_BAD_PARAMETER      , "BAD_PARAMETER"      },
    { FG_INVALID_TIME       , "INVALID_TIME"       },
    { FG_OUT_OF_LIMITS      , "OUT_OF_LIMITS"      },
    { FG_OUT_OF_RATE_LIMITS , "OUT_OF_RATE_LIMITS" },
    { FG_INIT_REF_MISMATCH  , "INIT_REF_MISMATCH"  },
    { FG_INVALID_REG_MODE   , "INVALID_REG_MODE"   },
    { FG_INVALID_REF_STATE  , "INVALID_REF_STATE"  },
    { FG_INVALID_SUB_SEL    , "INVALID_SUB_SEL"    },
    { FG_INVALID_CYC_SEL    , "INVALID_CYC_SEL"    },
    { 0                     , NULL                 },
}
#endif // GLOBALS
;

//! FGERROR parameters description structure

CCPARS_FGERROR_EXT struct CC_pars fgerror_pars[]
#ifdef GLOBALS
= {// name        type          max_n_els        *enum                      *value                                        n_els               flags
    { "ERRNO"   , PAR_ENUM,     1,               enum_fgerror_errno, { .u = refMgrVarPointer(&ref_mgr, FGERROR_ERRNO   ) }, 1,                PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL },
    { "FG_TYPE" , PAR_ENUM,     1,               enum_fg_type,       { .u = refMgrVarPointer(&ref_mgr, FGERROR_FG_TYPE ) }, 1,                PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL },
    { "INDEX"   , PAR_UNSIGNED, 1,               NULL,               { .u = refMgrVarPointer(&ref_mgr, FGERROR_INDEX   ) }, 1,                PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL },
    { "DATA"    , PAR_FLOAT,    FG_ERR_DATA_LEN, NULL,               { .f = refMgrVarValue  (&ref_mgr, FGERROR_DATA    ) }, FG_ERR_DATA_LEN,  PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_fgerror_errno) == FG_NUM_ERRORS + 1, mismatch_between_fgerror_errno_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

