//! @file  ccrt/inc/pars/logmpx.h
//!
//! @brief ccrt LOGMPX multiplexor parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGMPX_EXT
#else
#define CCPARS_LOGMPX_EXT extern
#endif

// Include automatically generated enums for liblog

#include "log_mpx_enums.h"

//! LOGMPX parameters description structure

CCPARS_LOGMPX_EXT struct CC_pars logmpx_pars[]
#ifdef GLOBALS
= {

#include "log_mpx_pars.h"      // generated automatically by liblog/scripts/write_ccrt.h from def/log.csv

}
#endif // GLOBALS
;

// Static assertion to check the RW MPX parameters are correctly defined

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_log_acq_mpx)      == LOG_ACQ_NUM_SIGNALS      + 1, mismatch_between_log_acq_mpx_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_log_acq_fltr_mpx) == LOG_ACQ_FLTR_NUM_SIGNALS + 1, mismatch_between_log_acq_fltr_mpx_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_log_b_meas_mpx)   == LOG_B_MEAS_NUM_SIGNALS   + 1, mismatch_between_log_b_meas_mpx_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_log_i_meas_mpx)   == LOG_I_MEAS_NUM_SIGNALS   + 1, mismatch_between_log_i_meas_mpx_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
