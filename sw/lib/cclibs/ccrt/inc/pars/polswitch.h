//! @file  ccrt/inc/pars/polswitch.h
//!
//! @brief ccrt POLSWITCH parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_POLSWITCH_EXT
#else
#define CCPARS_POLSWITCH_EXT extern
#endif

//! Used for:
//! * POLSWITCH STATE
//! * STATE POLSWITCH - alias of POLSWITCH STATE
//! * POLSWITCH REQ_STATE
//! * POLSWITCH EXP_STATE

CCPARS_POLSWITCH_EXT struct CC_pars_enum enum_polswitch_state[]
#ifdef GLOBALS
= {
    { POLSWITCH_STATE_NONE     , "NONE"     , 0              },
    { POLSWITCH_STATE_POSITIVE , "POSITIVE" , CC_ENUM_GREEN  },
    { POLSWITCH_STATE_NEGATIVE , "NEGATIVE" , CC_ENUM_GREEN  },
    { POLSWITCH_STATE_FAULT    , "FAULT"    , CC_ENUM_RED    },
    { POLSWITCH_STATE_MOVING   , "MOVING"   , CC_ENUM_YELLOW },
    { 0                        , NULL       , 0              },
}
#endif // GLOBALS
;

//! Used for:
//! * POLSWITCH MODE_USER
//! * POLSWITCH MODE_AUTO

CCPARS_POLSWITCH_EXT struct CC_pars_enum enum_polswitch_mode[]
#ifdef GLOBALS
= {
    { POLSWITCH_MODE_NOT_SET  , "NOT_SET"  , CC_ENUM_YELLOW },
    { POLSWITCH_MODE_POSITIVE , "POSITIVE" , CC_ENUM_GREEN  },
    { POLSWITCH_MODE_NEGATIVE , "NEGATIVE" , CC_ENUM_GREEN  },
    { 0                       , NULL                        },
}
#endif
;

//! Used for POLSWITCH SIGNALS parameter value decoding

CCPARS_POLSWITCH_EXT struct CC_pars_enum enum_polswitch_signals[]
#ifdef GLOBALS
= {
    { POLSWITCH_SIGNAL_POSITIVE_BIT , "POSITIVE"  },
    { POLSWITCH_SIGNAL_NEGATIVE_BIT , "NEGATIVE"  },
    { POLSWITCH_SIGNAL_FAULT_BIT    , "FAULT"     },
    { POLSWITCH_SIGNAL_LOCKED_BIT   , "LOCKED"    },
    { 0                             , NULL        },
}
#endif
;

//! Used for POLSWITCH STATUS parameter value decoding

CCPARS_POLSWITCH_EXT struct CC_pars_enum enum_polswitch_status[]
#ifdef GLOBALS
= {
    { POLSWITCH_IS_STARTING_BIT   , "IS_STARTING"   },
    { POLSWITCH_IS_SIMULATED_BIT  , "IS_SIMULATED"  },
    { POLSWITCH_IS_AUTOMATIC_BIT  , "IS_AUTOMATIC"  },
    { POLSWITCH_IS_NEGATIVE_BIT   , "IS_NEGATIVE"   },
    { POLSWITCH_IS_LOCKED_BIT     , "IS_LOCKED"     },
    { 0                           , NULL            },
}
#endif
;

//! Used for POLSWITCH FAULTS parameter value decoding

CCPARS_POLSWITCH_EXT struct CC_pars_enum enum_polswitch_faults[]
#ifdef GLOBALS
= {
    { POLSWITCH_EXTERNAL_FAULT_BIT   , "EXTERNAL"   },
    { POLSWITCH_INVALID_FAULT_BIT    , "INVALID"    },
    { POLSWITCH_UNEXPECTED_FAULT_BIT , "UNEXPECTED" },
    { POLSWITCH_TIMEOUT_FAULT_BIT    , "TIMEOUT"    },
    { 0                              , NULL         },
}
#endif
;

//! POLSWITCH parameters structure

struct CC_pars_polswitch
{
    enum CC_enabled_disabled    simulate;     //!< Enabled to simulate polarity switch
    enum CC_enabled_disabled    positive;     //!< Switch positive position status when POLSWITCH SIMULATE is DISABLED
    enum CC_enabled_disabled    negative;     //!< Switch negative position status when POLSWITCH SIMULATE is DISABLED
    enum CC_enabled_disabled    fault;        //!< Switch fault status when POLSWITCH SIMULATEis DISABLED
    enum CC_enabled_disabled    locked;       //!< Switch locked status when POLSWITCH SIMULATEis DISABLED
};

//! Instance of POLSWITCH parameters structure

CCPARS_POLSWITCH_EXT struct CC_pars_polswitch ccpars_polswitch
#ifdef GLOBALS
= {
    .simulate  = CC_DISABLED,    // POLSWITCH SIMULATE
    .positive  = CC_ENABLED,     // POLSWITCH POSITIVE
    .negative  = CC_DISABLED,    // POLSWITCH NEGATIVE
    .fault     = CC_DISABLED,    // POLSWITCH FAULT
    .locked    = CC_DISABLED,    // POLSWITCH LOCKED
}
#endif
;

//! Polarity switch manager. This is the main libpolswitch structure.

CCPARS_POLSWITCH_EXT struct POLSWITCH_mgr polswitch_mgr;

//! Used for accessing POLSWITCH parameters

enum polswitch_pars_index_enum
{
    POLSWITCH_TIMEOUT        ,
    POLSWITCH_MODE_USER      ,
    POLSWITCH_MODE_AUTO      ,
    POLSWITCH_AUTOMATIC      ,
    POLSWITCH_MOVEMENT_TIME  ,
    POLSWITCH_SIMULATE       ,
    POLSWITCH_POSITIVE       ,
    POLSWITCH_NEGATIVE       ,
    POLSWITCH_FAULT          ,
    POLSWITCH_LOCKED         ,
    POLSWITCH_REQ_STATE      ,
    POLSWITCH_EXP_STATE      ,
    POLSWITCH_STATE          ,
    POLSWITCH_SIGNALS        ,
    POLSWITCH_STATUS         ,
    POLSWITCH_FAULTS         ,
    POLSWITCH_NUM_PARS
};

//! POLSWITCH parameters description structure

CCPARS_POLSWITCH_EXT struct CC_pars polswitch_pars[]
#ifdef GLOBALS
= {// name             type          max_n_els *enum                         *value                                               n_els flags
    { "TIMEOUT"      , PAR_UNSIGNED, 1,        NULL,                  { .u = polswitchParPointer(&polswitch_mgr,TIMEOUT      )   }, 1,  PARS_RW|PARS_CFG  },
    { "MODE_USER"    , PAR_ENUM,     1,        enum_polswitch_mode,   { .u = polswitchParPointer(&polswitch_mgr,MODE_USER    )   }, 1,  PARS_RW           },
    { "MODE_AUTO"    , PAR_ENUM,     1,        enum_polswitch_mode,   { .u = polswitchParPointer(&polswitch_mgr,MODE_AUTO    )   }, 1,  PARS_RW           },
    { "AUTOMATIC"    , PAR_ENUM,     1,        enum_enabled_disabled, { .u = polswitchParPointer(&polswitch_mgr,AUTOMATIC    )   }, 1,  PARS_RW|PARS_CFG  },
    { "MOVEMENT_TIME", PAR_FLOAT,    1,        NULL,                  { .f = polswitchParPointer(&polswitch_mgr,MOVEMENT_TIME)   }, 1,  PARS_RW|PARS_CFG  },
    { "SIMULATE"     , PAR_ENUM,     1,        enum_enabled_disabled, { .u = &ccpars_polswitch.simulate                          }, 1,  PARS_RW           },
    { "POSITIVE"     , PAR_ENUM,     1,        enum_enabled_disabled, { .u = &ccpars_polswitch.positive                          }, 1,  PARS_RW           },
    { "NEGATIVE"     , PAR_ENUM,     1,        enum_enabled_disabled, { .u = &ccpars_polswitch.negative                          }, 1,  PARS_RW           },
    { "FAULT"        , PAR_ENUM,     1,        enum_enabled_disabled, { .u = &ccpars_polswitch.fault                             }, 1,  PARS_RW           },
    { "LOCKED"       , PAR_ENUM,     1,        enum_enabled_disabled, { .u = &ccpars_polswitch.locked                            }, 1,  PARS_RW           },
    { "REQ_STATE"    , PAR_ENUM,     1,        enum_polswitch_state,  { .u = polswitchVarPointer(&polswitch_mgr,REQUESTED_STATE) }, 1,  PARS_RO           },
    { "EXP_STATE"    , PAR_ENUM,     1,        enum_polswitch_state,  { .u = polswitchVarPointer(&polswitch_mgr,EXPECTED_STATE ) }, 1,  PARS_RO           },
    { "STATE"        , PAR_ENUM,     1,        enum_polswitch_state,  { .u = polswitchVarPointer(&polswitch_mgr,STATE          ) }, 1,  PARS_RO,          },
    { "SIGNALS"      , PAR_BITMASK,  1,        enum_polswitch_signals,{ .u = polswitchVarPointer(&polswitch_mgr,SIGNALS        ) }, 1,  PARS_RO           },
    { "STATUS"       , PAR_BITMASK,  1,        enum_polswitch_status, { .u = polswitchVarPointer(&polswitch_mgr,STATUS         ) }, 1,  PARS_RO           },
    { "FAULTS"       , PAR_BITMASK,  1,        enum_polswitch_faults, { .u = polswitchVarPointer(&polswitch_mgr,LATCHED_FAULTS ) }, 1,  PARS_RO|PARS_STAT },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_polswitch_mode)   == POLSWITCH_NUM_MODES       + 1, mismatch_between_enum_polswitch_mode_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_polswitch_state)  == POLSWITHC_NUM_STATES      + 1, mismatch_between_enum_polswitch_state_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_polswitch_status) == POLSWITCH_NUM_STATUS_BITS + 1, mismatch_between_enum_polswitch_status_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_polswitch_faults) == POLSWITCH_NUM_FAULT_BITS  + 1, mismatch_between_enum_polswitch_faults_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(polswitch_pars)        == POLSWITCH_NUM_PARS        + 1, mismatch_between_polswitch_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
