//! @file  ccrt/inc/pars/deco.h
//!
//! @brief ccrt DECO parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_DECO_EXT
#else
#define CCPARS_DECO_EXT extern
#endif

//! DECO parameters description structure

CCPARS_DECO_EXT struct CC_pars   deco_pars[]
#ifdef GLOBALS
= {// name              type          max_n_els     *enum        *value                                      n_els            flags
    { "PHASE"         , PAR_UNSIGNED, 1,            NULL, { .u = regMgrParAppPointer(&reg_pars, DECO_PHASE) }, 1,             PARS_RW|PARS_CFG|PARS_REG             },
    { "INDEX"         , PAR_UNSIGNED, 1,            NULL, { .u = regMgrParAppPointer(&reg_pars, DECO_INDEX) }, 1,             PARS_RW|PARS_CFG|PARS_REG             },
    { "K"             , PAR_FLOAT,    REG_NUM_DECO, NULL, { .f = regMgrParAppValue(  &reg_pars, DECO_K    ) }, REG_NUM_DECO,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "D"             , PAR_FLOAT,    REG_NUM_DECO, NULL, { .f = regMgrParAppValue(  &reg_pars, DECO_D    ) }, REG_NUM_DECO,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "PHASE_READBACK", PAR_UNSIGNED, 1,            NULL, { .u = regMgrVarPointer(   &reg_mgr,  DECO_PHASE) }, 1,             PARS_RO                               },
    { NULL }
}
#endif
;

// EOF
