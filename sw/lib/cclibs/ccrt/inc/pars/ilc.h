//! @file  ccrt/inc/pars/ilc.h
//!
//! @brief ccrt ILC parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "ccPars.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_ILC_EXT
#else
#define CCPARS_ILC_EXT extern
#endif


// Symbol list for ILC STATE

CCPARS_ILC_EXT struct CC_pars_enum enum_ilc_state[]
#ifdef GLOBALS
= {
    { REF_ILC_DISABLED   , "DISABLED"   },
    { REF_ILC_STANDBY    , "STANDBY"    },
    { REF_ILC_TO_CYCLING , "TO_CYCLING" },
    { REF_ILC_CYCLING    , "CYCLING"    },
    { REF_ILC_FAILED     , "FAILED"     },
    { 0                  , NULL         },
}
#endif // GLOBALS
;

//! Symbol list of ILC STATUS

CCPARS_ILC_EXT struct CC_pars_enum enum_ilc_status[]
#ifdef GLOBALS
= {
    { REF_ILC_CYC_STATES_BIT            , "CYC_STATES"            },
    { REF_ILC_RMS_ERR_FAILED_BIT        , "RMS_ERR_FAILED"        },
    { REF_ILC_RMS_V_REF_RATE_FAILED_BIT , "RMS_V_REF_RATE_FAILED" },
    { REF_ILC_L_FUNC_LEN_EVEN_BIT       , "L_FUNC_LEN_EVEN"       },
    { REF_ILC_REG_PERIOD_INVALID_BIT    , "REG_PERIOD_INVALID"    },
    { 0                                 , NULL                    },
}
#endif // GLOBALS
;


//! Used for accessing ILC parameters

enum CC_ilc_pars_index_enum
{
    ILC_START_TIME     ,
    ILC_STOP_REF       ,
    ILC_L_ORDER        ,
    ILC_L_FUNC         ,
    ILC_Q_FUNC         ,
    ILC_LOG_CYC_SEL    ,
    ILC_MAX_SAMPLES    ,
    ILC_STATE          ,
    ILC_STATUS         ,
    ILC_FAILED_CYC_SEL ,
    ILC_OP_Q_FUNC      ,
    ILC_NUM_PARS
};

//! ILC parameters description structure

CCPARS_ILC_EXT struct CC_pars   ilc_pars[]
#ifdef GLOBALS
= {//  name             type      max_n_els              *enum                         *value                                          n_els                   flags
    { "START_TIME"    , PAR_FLOAT,    1,                  NULL,                  { .f = refMgrParPointer(&ref_mgr,ILC_START_TIME    ) }, 1,                    PARS_RW|PARS_CFG },
    { "STOP_REF"      , PAR_FLOAT,    1,                  NULL,                  { .f = refMgrParPointer(&ref_mgr,ILC_STOP_REF      ) }, 1,                    PARS_RW|PARS_CFG },
    { "L_ORDER"       , PAR_UNSIGNED, 1,                  NULL,                  { .u = refMgrParPointer(&ref_mgr,ILC_L_ORDER       ) }, 1,                    PARS_RW|PARS_CFG },
    { "L_FUNC"        , PAR_FLOAT,    REF_ILC_L_FUNC_LEN, NULL,                  { .f = refMgrParPointer(&ref_mgr,ILC_L_FUNC        ) }, 0,                    PARS_RW|PARS_CFG },
    { "Q_FUNC"        , PAR_FLOAT,    REF_ILC_Q_PAR_LEN,  NULL,                  { .f = refMgrParPointer(&ref_mgr,ILC_Q_FUNC        ) }, 0,                    PARS_RW|PARS_CFG },
    { "LOG_CYC_SEL"   , PAR_UNSIGNED, 1,                  NULL,                  { .u = refMgrParPointer(&ref_mgr,ILC_LOG_CYC_SEL   ) }, 1,                    PARS_RW          },
    { "MAX_SAMPLES"   , PAR_UNSIGNED, 1,                  NULL,                  { .u = refMgrVarPointer(&ref_mgr,ILC_MAX_SAMPLES   ) }, 1,                    PARS_RW          },
    { "STATE"         , PAR_ENUM,     1,                  enum_ilc_state,        { .u = refMgrVarPointer(&ref_mgr,ILC_STATE         ) }, 1,                    PARS_RO          },
    { "STATUS"        , PAR_BITMASK,  1,                  enum_ilc_status,       { .u = refMgrVarPointer(&ref_mgr,ILC_STATUS        ) }, 1,                    PARS_RO          },
    { "FAILED_CYC_SEL", PAR_UNSIGNED, 1,                  NULL,                  { .u = refMgrVarPointer(&ref_mgr,ILC_FAILED_CYC_SEL) }, 1,                    PARS_RO          },
    { "OP_Q_FUNC"     , PAR_FLOAT,    REF_ILC_Q_FUNC_LEN, NULL,                  { .f = refMgrVarValue  (&ref_mgr,ILC_Q_FUNC        ) }, REF_ILC_Q_FUNC_LEN,   PARS_RO          },
    { NULL }
}
#endif
;

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_ilc_state)  == REF_ILC_NUM_STATES + 1, mismatch_between_ilc_state_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_ilc_status) == REF_ILC_NUM_STATUS + 1, mismatch_between_ilc_status_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(ilc_pars)        == ILC_NUM_PARS       + 1, mismatch_between_ilc_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
