//! @file  ccrt/inc/pars/pre_func.h
//!
//! @brief ccrt PRE_FUNC parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_PRE_FUNC_EXT
#else
#define CCPARS_PRE_FUNC_EXT extern
#endif

CCPARS_PRE_FUNC_EXT struct REF_pre_func_seq     pre_func_ramp;              //!< RAMP pre-function sequence
CCPARS_PRE_FUNC_EXT struct REF_pre_func_seq     pre_func_seq;               //!< Non-RAMP pre-function sequence

//! PRE_FUNC parameters description structure

CCPARS_PRE_FUNC_EXT struct CC_pars pre_func_pars[]
#ifdef GLOBALS
= {// name                   type          max_n_els                          *enum         *value                                                          n_els                      flags     size_of_struct
    { "B_DEFAULTS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULTS,          NULL, { .f =  ref_mgr.pre_func_mgr.b.defaults                    }, REF_PRE_FUNC_NUM_DEFAULTS,          PARS_RO,  0                               },
    { "I_DEFAULTS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULTS,          NULL, { .f =  ref_mgr.pre_func_mgr.i.defaults                    }, REF_PRE_FUNC_NUM_DEFAULTS,          PARS_RO,  0                               },
    { "INITIAL_REF",         PAR_FLOAT,    1,                                  NULL, { .f = &ref_mgr.pre_func_arm.initial_ref                   }, 1,                                  PARS_RO,  0                               },
    { "FINAL_REF",           PAR_FLOAT,    1,                                  NULL, { .f = &ref_mgr.pre_func_arm.final_ref                     }, 1,                                  PARS_RO,  0                               },
    { "DURATION_AVL",        PAR_FLOAT,    1,                                  NULL, { .f = &ref_mgr.pre_func_arm.duration_available            }, 1,                                  PARS_RO,  0                               },
    { "SEQ_NUM_SEGS",        PAR_UNSIGNED, 1,                                  NULL, { .u = &pre_func_seq.num_segs                              }, 1,                                  PARS_RO,  0                               },
    { "SEQ_SEG_TYPE",        PAR_UNSIGNED, REF_PRE_FUNC_MAX_NUM_SEGS,          NULL, { .u = &pre_func_seq.seg[0].type                           }, REF_PRE_FUNC_MAX_NUM_SEGS,          PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "SEQ_SEG_START_TIME",  PAR_FLOAT,    REF_PRE_FUNC_MAX_NUM_SEGS,          NULL, { .f = &pre_func_seq.seg[0].start_time                     }, REF_PRE_FUNC_MAX_NUM_SEGS,          PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "SEQ_SEG_FINAL_REF",   PAR_FLOAT,    REF_PRE_FUNC_MAX_NUM_SEGS,          NULL, { .f = &pre_func_seq.seg[0].final_ref                      }, REF_PRE_FUNC_MAX_NUM_SEGS,          PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "SEQ_RMS",             PAR_FLOAT,    1,                                  NULL, { .f = &pre_func_seq.rms                                   }, 1,                                  PARS_RO,  0                               },
    { "RAMP_NUM_SEGS",       PAR_UNSIGNED, 1,                                  NULL, { .u = &pre_func_ramp.num_segs                             }, 1,                                  PARS_RO,  0                               },
    { "RAMP_SEG_TYPE",       PAR_UNSIGNED, 3,                                  NULL, { .u = &pre_func_ramp.seg[0].type                          }, 3,                                  PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "RAMP_SEG_START_TIME", PAR_FLOAT,    3,                                  NULL, { .f = &pre_func_ramp.seg[0].start_time                    }, 3,                                  PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "RAMP_SEG_FINAL_REF",  PAR_FLOAT,    3,                                  NULL, { .f = &pre_func_ramp.seg[0].final_ref                     }, 3,                                  PARS_RO,  sizeof(struct REF_pre_func_seg) },
    { "RAMP_RMS",            PAR_FLOAT,    1,                                  NULL, { .f = &pre_func_ramp.rms                                  }, 1,                                  PARS_RO,  0                               },
    { "B_CALC_DURATION",     PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_B_CALC_DURATION  ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "B_MEAS_DURATION",     PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_B_MEAS_DURATION  ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "B_CALC_RMS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_B_CALC_RMS       ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "B_MEAS_RMS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_B_MEAS_RMS       ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "I_MEAS_DURATION",     PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_I_MEAS_DURATION  ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "I_CALC_DURATION",     PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_I_CALC_DURATION  ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "I_MEAS_RMS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_I_MEAS_RMS       ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { "I_CALC_RMS",          PAR_FLOAT,    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, NULL, { .f = refMgrVarValue(&ref_mgr,PRE_FUNC_I_CALC_RMS       ) }, REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES, PARS_RO,  0                               },
    { NULL }
}
#endif
;

// EOF
