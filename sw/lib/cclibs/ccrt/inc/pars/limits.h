//! @file  ccrt/inc/pars/limits.h
//!
//! @brief ccrt LIMITS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LIMITS_EXT
#else
#define CCPARS_LIMITS_EXT extern
#endif

//! GLOBAL parameters description structure

CCPARS_LIMITS_EXT struct CC_pars limits_pars[]
#ifdef GLOBALS
= {// name                       type       max_n_els      *enum          *value                                                                             n_els       flags
    { "B_POS"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_STANDBY"              , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_NEG"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_RATE"                 , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_CLOSELOOP"            , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_LOW"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_ZERO"                 , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_ERR_WARNING"          , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "B_ERR_FAULT"            , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_POS"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_STANDBY"              , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_NEG"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_RATE"                 , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_CLOSELOOP"            , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_LOW"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_ZERO"                 , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_ERR_WARNING"          , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_ERR_FAULT"            , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_QUADRANTS41"          , PAR_FLOAT, 2,             NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_QUADRANTS41                       ) }, 2,              PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_RMS_TC"               , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_TC                            ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "I_RMS_WARNING"          , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_WARNING                       ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "I_RMS_FAULT"            , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_FAULT                         ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "I_RMS_LOAD_TC"          , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_RMS_LOAD_WARNING"     , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_RMS_LOAD_FAULT"       , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "I_DIFF_FAULT"           , PAR_FLOAT, 1,             NULL,   { .f = sigVarPointer(&sig_struct, select, i_meas, SELECT_DIFF_FAULT_LIMIT        ) }, 1,              PARS_RW                               },
    { "I_DIFF_WARN"            , PAR_FLOAT, 1,             NULL,   { .f = sigVarPointer(&sig_struct, select, i_meas, SELECT_DIFF_WARN_LIMIT         ) }, 1,              PARS_RW                               },
    { "I_DIFF_FAULT_WARN_RATIO", PAR_FLOAT, 1,             NULL,   { .f = sigVarPointer(&sig_struct, select, i_meas, SELECT_DIFF_FAULT_WARN_RATIO   ) }, 1,              PARS_RW                               },
    { "V_POS"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "V_NEG"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "V_RATE"                 , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_V_RATE                              ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "V_RATE_RMS_TC"          , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_TC                       ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "V_RATE_RMS_FAULT"       , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_FAULT                    ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "V_ERR_WARNING"          , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_WARNING                       ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "V_ERR_FAULT"            , PAR_FLOAT, 1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_FAULT                         ) }, 1,              PARS_RW|PARS_REG|PARS_CFG             },
    { "V_QUADRANTS41"          , PAR_FLOAT, 2,             NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_V_QUADRANTS41                       ) }, 2,              PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "P_POS"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "P_NEG"                  , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ) }, REG_NUM_LOADS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "MAX_V_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_V_MAX                                  ) }, 1,              PARS_RO                               },
    { "MIN_V_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_V_MIN                                  ) }, 1,              PARS_RO                               },
    { "MAX_I_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_I_MAX                                  ) }, 1,              PARS_RO                               },
    { "MIN_I_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_I_MIN                                  ) }, 1,              PARS_RO                               },
    { "MAX_B_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_B_MAX                                  ) }, 1,              PARS_RO                               },
    { "MIN_B_REF"              , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_B_MIN                                  ) }, 1,              PARS_RO                               },
    { "MAX_REF"                , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_MAX                                    ) }, 1,              PARS_RO                               },
    { "MIN_REF"                , PAR_FLOAT, 1,             NULL,   { .f = refMgrVarPointer(   &ref_mgr,  REF_MIN                                    ) }, 1,              PARS_RO                               },
    { NULL }
}
#endif
;

// EOF
