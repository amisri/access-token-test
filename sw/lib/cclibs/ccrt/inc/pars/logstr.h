//! @file  ccrt/inc/pars/logstr.h
//!
//! @brief ccrt LOG structs parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGSTR_EXT
#else
#define CCPARS_LOGSTR_EXT extern
#endif

//! Liblog structures

CCPARS_LOGSTR_EXT struct LOG_structs log_structs;
CCPARS_LOGSTR_EXT struct LOG_buffers log_buffers;
CCPARS_LOGSTR_EXT struct LOG_read    log_read;

//! LOG parameters description structure

CCPARS_LOGSTR_EXT struct CC_pars logstr_pars[]
#ifdef GLOBALS
= {// name                         type          max_n_els  *enum         *value                                              n_els     flags     size_of_struct
    { "NAME"                   , PAR_STRING,   LOG_NUM_LOGS, NULL, { .s = (char **)&log_buffers.priv[0].name          }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_private) },
    { "SIG_BUFS_OFFSET"        , PAR_UNSIGNED, LOG_NUM_LOGS, NULL, { .u = &log_buffers.priv[0].sig_bufs_offset        }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_private) },
    { "SIG_BUFS_LEN"           , PAR_UNSIGNED, LOG_NUM_LOGS, NULL, { .u = &log_buffers.priv[0].sig_bufs_len           }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_private) },
    { "NUM_POSTMORTEM_SAMPLES" , PAR_UNSIGNED, LOG_NUM_LOGS, NULL, { .u = &log_buffers.priv[0].num_postmortem_samples }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_private) },
    { "INDEX"                  , PAR_USHORT,   LOG_NUM_LOGS, NULL, { .h = &log_structs.log[0].index                   }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_log)     },
    { "NUM_CYCLES"             , PAR_USHORT,   LOG_NUM_LOGS, NULL, { .h = &log_structs.log[0].num_cycles              }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_log)     },
    { "BIT_MASK"               , PAR_HEX,      LOG_NUM_LOGS, NULL, { .u = &log_structs.log[0].bit_mask                }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_log)     },
    { "META"                   , PAR_HEX,      LOG_NUM_LOGS, NULL, { .u = &log_structs.log[0].meta                    }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_log)     },
    { "NUM_SAMPLES_PER_SIG"    , PAR_UNSIGNED, LOG_NUM_LOGS, NULL, { .u = &log_structs.log[0].num_samples_per_sig     }, LOG_NUM_LOGS,  PARS_RO,  sizeof(struct LOG_log)     },
    { NULL }
}
#endif // GLOBALS
;

// EOF
