//! @file  ccrt/inc/pars/default.h
//!
//! @brief ccrt DEFAULT parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_DEFAULT_EXT
#else
#define CCPARS_DEFAULT_EXT extern
#endif

//! DEFAULT parameters description structure

CCPARS_DEFAULT_EXT struct CC_pars default_pars[]
#ifdef GLOBALS
= {// name                 type       max_n_els      *enum          *value                                                    n_els        flags
    { "V_ACCELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "V_DECELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_DECELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "V_LINEAR_RATE"    , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "V_PRE_FUNC_MAX"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "V_PRE_FUNC_MIN"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "V_MINRMS"         , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_V_MINRMS        ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_ACCELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_DECELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_DECELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_LINEAR_RATE"    , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_PRE_FUNC_MAX"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_PRE_FUNC_MIN"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "I_MINRMS"         , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_I_MINRMS        ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_ACCELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_DECELERATION"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_DECELERATION  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_LINEAR_RATE"    , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_PRE_FUNC_MAX"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_PRE_FUNC_MIN"   , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "B_MINRMS"         , PAR_FLOAT, REG_NUM_LOADS, NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_B_MINRMS        ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "PLATEAU_DURATION" , PAR_FLOAT, 2,             NULL,   { .f = refMgrParPointer(&ref_mgr,DEFAULT_PLATEAU_DURATION) }, 2,              PARS_RW|PARS_CFG|PARS_FIXLEN },
    { NULL }
}
#endif
;

// EOF
