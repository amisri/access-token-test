//! @file  ccrt/inc/pars/evt.h
//!
//! @brief ccrt Event Log test parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_EVT_EXT
#else
#define CCPARS_EVT_EXT extern
#endif

//! Event log test parameter enums

 enum EVT_enum
 {
     EVT_ENUM_0,
     EVT_ENUM_1,
     EVT_ENUM_2,
     EVT_ENUM_3,
     EVT_ENUM_4,
     EVT_ENUM_5,
     EVT_ENUM_6,
     EVT_ENUM_7,
 };

CCPARS_EVT_EXT struct CC_pars_enum enum_evt_enum[]
#ifdef GLOBALS
= {
    { EVT_ENUM_0 , "ENUM0" },
    { EVT_ENUM_1 , "ENUM1" },
    { EVT_ENUM_2 , "ENUM2" },
    { EVT_ENUM_3 , "ENUM3" },
    { EVT_ENUM_4 , "ENUM4" },
    { EVT_ENUM_5 , "ENUM5" },
    { EVT_ENUM_6 , "ENUM6" },
    { EVT_ENUM_7 , "ENUM7" },
    { 0          ,  NULL   },
}
#endif
;

CCPARS_EVT_EXT struct CC_pars_enum enum_evt_bitmask[]
#ifdef GLOBALS
= {
    { 0,  "BIT0"  },
    { 1,  "BIT1"  },
    { 2,  "BIT2"  },
    { 3,  "BIT3"  },
    { 4,  "BIT4"  },
    { 5,  "BIT5"  },
    { 6,  "BIT6"  },
    { 7,  "BIT7"  },
    { 8,  "BIT8"  },
    { 9,  "BIT9"  },
    { 10, "BIT10" },
    { 11, "BIT11" },
    { 12, "BIT12" },
    { 13, "BIT13" },
    { 14, "BIT14" },
    { 15, "BIT15" },
    { 0 ,  NULL   },
}
#endif
;

//! Event log test parameters structure

struct CC_pars_evt
{
    union
    {
        uint8_t             u8;                       //!< Mapped to EVT UINT8  in the event log
        uint16_t            u16;                      //!< Mapped to EVT UINT16 in the event log
        uint32_t            u32;                      //!< Mapped to EVT UINT32 in the event log
    }                       integer;

    union
    {
        enum EVT_enum       enum1;                    //!< Mapped to EVT ENUM1 in the event log
        uint32_t            value;                    //!< Mapped to EVT ENUM1 to allow values to be set that don't match a symbol in enum EVT_enum
    }                       enum1;

    enum EVT_enum           enum2;                    //!< Mapped to EVT ENUM2 in the event log using same enum

    uint32_t                bitmask1;                 //!< Mapped to EVT BITMASK1 in the event log
    uint32_t                bitmask2;                 //!< Mapped to EVT BITMASK2 in the event log
};

//! Instance of EVT parameters structure

CCPARS_EVT_EXT struct CC_pars_evt ccpars_evt;

// Global parameter indexes

enum CC_evt_pars_index_enum
{
    EVT_INTEGER     ,
    EVT_ENUM1_VALUE ,
    EVT_ENUM1       ,
    EVT_ENUM2       ,
    EVT_BITMASK1    ,
    EVT_BITMASK2    ,
    EVT_NUM_PARS
};

//! EVT parameters description structure

CCPARS_EVT_EXT struct CC_pars evt_pars[]
#ifdef GLOBALS
= {// name           type      max_n_els *enum                     *value                    n_els flags
    { "INTEGER"    , PAR_UNSIGNED, 1,     NULL,             { .u = &ccpars_evt.integer.u32  }, 1,  PARS_RW  },
    { "ENUM1_VALUE", PAR_UNSIGNED, 1,     NULL,             { .u = &ccpars_evt.enum1.enum1  }, 1,  PARS_RW  },
    { "ENUM1"      , PAR_ENUM,     1,     enum_evt_enum,    { .u = &ccpars_evt.enum1.value  }, 1,  PARS_RW  },
    { "ENUM2"      , PAR_ENUM,     1,     enum_evt_enum,    { .u = &ccpars_evt.enum2        }, 1,  PARS_RW  },
    { "BITMASK1"   , PAR_BITMASK,  1,     enum_evt_bitmask, { .u = &ccpars_evt.bitmask1     }, 1,  PARS_RW  },
    { "BITMASK2"   , PAR_BITMASK,  1,     enum_evt_bitmask, { .u = &ccpars_evt.bitmask2     }, 1,  PARS_RW  },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(evt_pars) == EVT_NUM_PARS + 1, mismatch_between_evt_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

