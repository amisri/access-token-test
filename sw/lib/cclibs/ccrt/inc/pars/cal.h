//! @file  ccrt/inc/pars/cal.h
//!
//! @brief ccrt CALIBRATION parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "reg.h"
#include "sigStructs.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_CAL_EXT
#else
#define CCPARS_CAL_EXT extern
#endif

//! Used for transducer position parameters value

CCPARS_CAL_EXT struct CC_pars_enum enum_transducer_position[]
#ifdef GLOBALS
= {
    { SIG_TRANSDUCER_POSITION_CONVERTER , "CONVERTER" },
    { SIG_TRANSDUCER_POSITION_LOAD      , "LOAD"      },
    { SIG_TRANSDUCER_POSITION_NORMAL    , "NORMAL"    },
    { SIG_TRANSDUCER_POSITION_INVERTED  , "INVERTED"  },
    { 0                                 , NULL        },
}
#endif
;

//! Signals structure. It's the main libsig structure and it's initialized in sigStructInit.h.

CCPARS_CAL_EXT struct SIG_struct sig_struct;


//! CAL parameters structure

struct CC_pars_cal
{
    uint32_t                    dcct_primary_turns; //!< Number of primary turns in both DCCT A and B
    uint32_t                    num_samples;        //!< Number of samples need for calibration of the ADCs or transducers
    enum CC_enabled_disabled    active;             //!< Enable to ignore DCCT A/B HEADERR when calibrating
};

//! Instance of CAL parameters structure

CCPARS_CAL_EXT struct CC_pars_cal ccpars_cal
#ifdef GLOBALS
= {
    .dcct_primary_turns = 1,            // DCCT_PRIMARY_TURNS
    .num_samples        = 10000,        // NUM_SAMPLES
    .active             = CC_DISABLED,  // ACTIVE
}
#endif
;

//! DAC structure

CCPARS_CAL_EXT struct SIG_dac dac;

// CAL parameters description structure

CCPARS_CAL_EXT struct CC_pars cal_pars[]
#ifdef GLOBALS
= {// name                  type      max_n_els *enum                            *value                                                                               n_els flags
    { "DCCT_PRIMARY_TURNS", PAR_UNSIGNED, 1,    NULL,                     { .u = &ccpars_cal.dcct_primary_turns                                                      }, 1,  PARS_RW|PARS_CFG    },
    { "NUM_SAMPLES"       , PAR_UNSIGNED, 1,    NULL,                     { .u = &ccpars_cal.num_samples                                                             }, 1,  PARS_RW|PARS_CFG    },
    { "ACTIVE"            , PAR_ENUM,     1,    enum_enabled_disabled,    { .u = &ccpars_cal.active                                                                  }, 1,  PARS_RW             },
    { "ADC_A_GAIN"        , PAR_UNSIGNED, 1,    NULL,                     { .u = sigVarPointer(&sig_struct, adc       , adc_a   , ADC_NOMINAL_GAIN                 ) }, 1,  PARS_RW             },
    { "ADC_A_ERR"         , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_a   , ADC_CAL_EVENT                    ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "ADC_A_TC"          , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_a   , ADC_CAL_TC                       ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_A_DTC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_a   , ADC_CAL_DTC                      ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_A_CAL_FLAG"    , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, adc       , adc_a   , ADC_CALIBRATING_FLAG             ) }, 1,  PARS_RW             },
    { "ADC_B_GAIN"        , PAR_UNSIGNED, 1,    NULL,                     { .u = sigVarPointer(&sig_struct, adc       , adc_b   , ADC_NOMINAL_GAIN                 ) }, 1,  PARS_RW             },
    { "ADC_B_ERR"         , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_b   , ADC_CAL_EVENT                    ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "ADC_B_TC"          , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_b   , ADC_CAL_TC                       ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_B_DTC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_b   , ADC_CAL_DTC                      ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_B_CAL_FLAG"    , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, adc       , adc_b   , ADC_CALIBRATING_FLAG             ) }, 1,  PARS_RW             },
    { "ADC_C_GAIN"        , PAR_UNSIGNED, 1,    NULL,                     { .u = sigVarPointer(&sig_struct, adc       , adc_c   , ADC_NOMINAL_GAIN                 ) }, 1,  PARS_RW             },
    { "ADC_C_ERR"         , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_c   , ADC_CAL_EVENT                    ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "ADC_C_TC"          , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_c   , ADC_CAL_TC                       ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_C_DTC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_c   , ADC_CAL_DTC                      ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_D_GAIN"        , PAR_UNSIGNED, 1,    NULL,                     { .u = sigVarPointer(&sig_struct, adc       , adc_d   , ADC_NOMINAL_GAIN                 ) }, 1,  PARS_RW             },
    { "ADC_D_ERR"         , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_d   , ADC_CAL_EVENT                    ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "ADC_D_TC"          , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_d   , ADC_CAL_TC                       ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "ADC_D_DTC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, adc       , adc_d   , ADC_CAL_DTC                      ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "DCCT_A_POSITION"   , PAR_ENUM,     1,    enum_transducer_position, { .u = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_POSITION              ) }, 1,  PARS_RW|PARS_CFG    },
    { "DCCT_A_GAIN"       , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_NOMINAL_GAIN          ) }, 1,  PARS_RW|PARS_CFG    },
    { "DCCT_A_HEADERR"    , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_GAIN_ERR_PPM          ) }, 1,  PARS_RW             },
    { "DCCT_A_ERR"        , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_a  , TRANSDUCER_CAL_EVENT             ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "DCCT_A_TC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_a  , TRANSDUCER_CAL_TC                ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "DCCT_A_DTC"        , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_a  , TRANSDUCER_CAL_DTC               ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "DCCT_A_EXT"        , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_EXT_MEAS_IS_ENABLED   ) }, 1,  PARS_RW             },
    { "DCCT_A_EXT_VALID"  , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_EXT_MEAS_IS_VALID     ) }, 1,  PARS_RW             },
    { "DCCT_A_EXT_SIG"    , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_EXT_MEAS_SIGNAL       ) }, 1,  PARS_RW             },
    { "DCCT_B_POSITION"   , PAR_ENUM,     1,    enum_transducer_position, { .u = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_POSITION              ) }, 1,  PARS_RW|PARS_CFG    },
    { "DCCT_B_GAIN"       , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_NOMINAL_GAIN          ) }, 1,  PARS_RW|PARS_CFG    },
    { "DCCT_B_HEADERR"    , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_GAIN_ERR_PPM          ) }, 1,  PARS_RW             },
    { "DCCT_B_ERR"        , PAR_FLOAT,    6,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_b  , TRANSDUCER_CAL_EVENT             ) }, 6,  PARS_RW|PARS_FIXLEN },
    { "DCCT_B_TC"         , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_b  , TRANSDUCER_CAL_TC                ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "DCCT_B_DTC"        , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, transducer, dcct_b  , TRANSDUCER_CAL_DTC               ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "DCCT_B_EXT"        , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_EXT_MEAS_IS_ENABLED   ) }, 1,  PARS_RW             },
    { "DCCT_A_EXT_VALID"  , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_EXT_MEAS_IS_VALID     ) }, 1,  PARS_RW             },
    { "DCCT_A_EXT_SIG"    , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_EXT_MEAS_SIGNAL       ) }, 1,  PARS_RW             },
    { "V_PROBE_GAIN"      , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, v_probe , TRANSDUCER_NOMINAL_GAIN          ) }, 1,  PARS_RW|PARS_CFG    },
    { "V_PROBE_EXT"       , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, v_probe , TRANSDUCER_EXT_MEAS_IS_ENABLED   ) }, 1,  PARS_RW             },
    { "V_PROBE_EXT_VALID" , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, v_probe , TRANSDUCER_EXT_MEAS_IS_VALID     ) }, 1,  PARS_RW             },
    { "V_PROBE_EXT_SIG"   , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, v_probe , TRANSDUCER_EXT_MEAS_SIGNAL       ) }, 1,  PARS_RW             },
    { "B_PROBE_GAIN"      , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_NOMINAL_GAIN          ) }, 1,  PARS_RW|PARS_CFG    },
    { "B_PROBE_VALID"     , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_MEAS_IS_VALID         ) }, 1,  PARS_RW             },
    { "B_PROBE_FLTR_VALID", PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_MEAS_FILTERED_IS_VALID) }, 1,  PARS_RW             },
    { "B_PROBE_EXT"       , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_EXT_MEAS_IS_ENABLED   ) }, 1,  PARS_RW             },
    { "B_PROBE_EXT_VALID" , PAR_BOOL,     1,    NULL,                     { .b = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_EXT_MEAS_IS_VALID     ) }, 1,  PARS_RW             },
    { "B_PROBE_EXT_SIG"   , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, b_probe , TRANSDUCER_EXT_MEAS_SIGNAL       ) }, 1,  PARS_RW             },
    { "I_PROBE_GAIN"      , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, transducer, i_probe , TRANSDUCER_NOMINAL_GAIN          ) }, 1,  PARS_RW|PARS_CFG    },
    { "INT_CAL_REF_ERR"   , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, cal_ref   , internal, CAL_REF_INT_CAL_EVENT            ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "INT_CAL_REF_TEMP"  , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, cal_ref   , internal, CAL_REF_INT_CAL_EVENT_TEMP_C     ) }, 1,  PARS_RW|PARS_FIXLEN },
    { "INT_CAL_REF_TC"    , PAR_FLOAT,    3,    NULL,                     { .f = sigVarValue  (&sig_struct, cal_ref   , internal, CAL_REF_INT_CAL_TC               ) }, 3,  PARS_RW|PARS_FIXLEN },
    { "EXT_CAL_REF_ERR"   , PAR_FLOAT,    1,    NULL,                     { .f = sigVarPointer(&sig_struct, cal_ref   , external, CAL_REF_EXT_CAL_ERR              ) }, 1,  PARS_RW             },
    { "DAC"               , PAR_FLOAT,    3,    NULL,                     { .f = dac.cal_v_dac_meas                                                                  }, 3,  PARS_RW|PARS_FIXLEN },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_transducer_position) == SIG_TRANSDUCER_NUM_POSITIONS + 1, mismatch_between_transducer_position_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
