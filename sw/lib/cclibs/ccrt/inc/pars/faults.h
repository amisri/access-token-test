//! @file  ccrt/inc/pars/faults.h
//!
//! @brief ccrt FAULTS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "cal.h"
#include "ccPars.h"
#include "polswitch.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_FAULTS_EXT
#else
#define CCPARS_FAULTS_EXT extern
#endif

//! Used for FAULTS REF parameter value decoding

CCPARS_FAULTS_EXT struct CC_pars_enum enum_ref_faults[]
#ifdef GLOBALS
= {
    { REF_B_MEAS_FAULT_BIT     , "B_MEAS_FAULT"     },
    { REF_B_MEAS_TRIP_BIT      , "B_MEAS_TRIP"      },
    { REF_B_ERR_FAULT_BIT      , "B_ERR_FAULT"      },
    { REF_B_RST_FAULT_BIT      , "B_RST_FAULT"      },
    { REF_I_MEAS_FAULT_BIT     , "I_MEAS_FAULT"     },
    { REF_I_MEAS_TRIP_BIT      , "I_MEAS_TRIP"      },
    { REF_I_ERR_FAULT_BIT      , "I_ERR_FAULT"      },
    { REF_I_RST_FAULT_BIT      , "I_RST_FAULT"      },
    { REF_I_RMS_FAULT_BIT      , "I_RMS_FAULT"      },
    { REF_I_RMS_LOAD_FAULT_BIT , "I_RMS_LOAD_FAULT" },
    { REF_V_MEAS_FAULT_BIT     , "V_MEAS_FAULT"     },
    { REF_V_MEAS_TRIP_BIT      , "V_MEAS_TRIP"      },
    { REF_V_ERR_FAULT_BIT      , "V_ERR_FAULT"      },
    { REF_V_RATE_RMS_FAULT_BIT , "V_RATE_RMS_FAULT" },
    { REF_I_CAPA_FAULT_BIT     , "I_CAPA_FAULT"     },
    { 0                        , NULL               },
}
#endif // GLOBALS
;

//! Libsig faults enums

CCPARS_FAULTS_EXT struct CC_pars_enum enum_sig_adc_faults[]
#ifdef GLOBALS
= {
    { SIG_ADC_FLT_BIT             , "FAULT"       },
    { SIG_ADC_STUCK_FLT_BIT       , "STUCK"       },
    { SIG_ADC_CAL_FLT_BIT         , "CAL"         },
    { SIG_ADC_XDCR_IN_USE_FLT_BIT , "XDCR_IN_USE" },
    { SIG_ADC_APP_0_FLT_BIT       , "APP_0"       },
    { SIG_ADC_APP_1_FLT_BIT       , "APP_1"       },
    { SIG_ADC_APP_2_FLT_BIT       , "APP_2"       },
    { SIG_ADC_APP_3_FLT_BIT       , "APP_2"       },
    { 0                           , NULL          },
}
#endif // GLOBALS
;

CCPARS_FAULTS_EXT struct CC_pars_enum enum_sig_transducer_faults[]
#ifdef GLOBALS
= {
    { SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT , "NOT_AVL" },
    { SIG_TRANSDUCER_ADC_FLT_BIT         , "ADC_FLT" },
    { SIG_TRANSDUCER_FLT_BIT             , "FAULT"   },
    { SIG_TRANSDUCER_CAL_FLT_BIT         , "CAL"     },
    { SIG_TRANSDUCER_EXT_FLT_BIT         , "EXT"     },
    { SIG_TRANSDUCER_APP_0_FLT_BIT       , "APP_0"   },
    { SIG_TRANSDUCER_APP_1_FLT_BIT       , "APP_1"   },
    { SIG_TRANSDUCER_APP_2_FLT_BIT       , "APP_2"   },
    { SIG_TRANSDUCER_APP_3_FLT_BIT       , "APP_3"   },
    { 0                                  , NULL      },
}
#endif // GLOBALS
;

CCPARS_FAULTS_EXT struct CC_pars_enum enum_sig_select_faults[]
#ifdef GLOBALS
= {
    { SIG_SELECT_SIG_FLT_BIT                , "SIG_FLT"       },
    { SIG_SELECT_DIFF_FLT_BIT               , "DIFF"          },
    { SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT , "NO_TRANSDUCER" },
    { 0                                     , NULL            },
}
#endif // GLOBALS
;

//! FAULTS parameters description structure

CCPARS_FAULTS_EXT struct CC_pars faults_pars[]
#ifdef GLOBALS
= {// name         type     max_n_els *enum                             *value                                                               n_els flags
    { "REF"      , PAR_BITMASK, 1,    enum_ref_faults,            { .u = refMgrVarPointer(&ref_mgr,REF_FAULTS)                              }, 1,  PARS_RO|PARS_STAT },
    { "IN_OFF"   , PAR_BITMASK, 1,    enum_ref_faults,            { .u = refMgrVarPointer(&ref_mgr,REF_FAULTS_IN_OFF)                       }, 1,  PARS_RO|PARS_STAT },
    { "POLSWITCH", PAR_BITMASK, 1,    enum_polswitch_faults,      { .u = polswitchVarPointer(&polswitch_mgr,LATCHED_FAULTS)                 }, 1,  PARS_RO|PARS_STAT },
    { "ADC_A"    , PAR_BITMASK, 1,    enum_sig_adc_faults,        { .u = sigVarPointer(&sig_struct, adc       , adc_a  , ADC_FAULTS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_B"    , PAR_BITMASK, 1,    enum_sig_adc_faults,        { .u = sigVarPointer(&sig_struct, adc       , adc_b  , ADC_FAULTS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_C"    , PAR_BITMASK, 1,    enum_sig_adc_faults,        { .u = sigVarPointer(&sig_struct, adc       , adc_c  , ADC_FAULTS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_D"    , PAR_BITMASK, 1,    enum_sig_adc_faults,        { .u = sigVarPointer(&sig_struct, adc       , adc_d  , ADC_FAULTS       ) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_A"   , PAR_BITMASK, 1,    enum_sig_transducer_faults, { .u = sigVarPointer(&sig_struct, transducer, dcct_a , TRANSDUCER_FAULTS) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_B"   , PAR_BITMASK, 1,    enum_sig_transducer_faults, { .u = sigVarPointer(&sig_struct, transducer, dcct_b , TRANSDUCER_FAULTS) }, 1,  PARS_RO|PARS_STAT },
    { "V_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_faults, { .u = sigVarPointer(&sig_struct, transducer, v_probe, TRANSDUCER_FAULTS) }, 1,  PARS_RO|PARS_STAT },
    { "B_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_faults, { .u = sigVarPointer(&sig_struct, transducer, b_probe, TRANSDUCER_FAULTS) }, 1,  PARS_RO|PARS_STAT },
    { "I_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_faults, { .u = sigVarPointer(&sig_struct, transducer, i_probe, TRANSDUCER_FAULTS) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_SEL" , PAR_BITMASK, 1,    enum_sig_select_faults,     { .u = sigVarPointer(&sig_struct, select    , i_meas , SELECT_FAULTS    ) }, 1,  PARS_RO|PARS_STAT },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_ref_faults)            == REF_NUM_FAULT_BITS            + 1, mismatch_between_ref_faults_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_adc_faults)        == SIG_ADC_NUM_FAULT_BITS        + 1, mismatch_between_sig_adc_faults_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_transducer_faults) == SIG_TRANSDUCER_NUM_FAULT_BITS + 1, mismatch_between_sig_transducer_faults_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_select_faults)     == SIG_SELECT_NUM_FAULT_BITS     + 1, mismatch_between_sig_select_faults_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
