//! @file  ccrt/inc/pars/logstatus.h
//!
//! @brief ccrt LOGSTATUS parameters
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "logmenu.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGSTATUS_EXT
#else
#define CCPARS_LOGSTATUS_EXT extern
#endif

//! Log menu status enum

CCPARS_LOGSTATUS_EXT struct CC_pars_enum enum_log_menu_status[]
#ifdef GLOBALS
= {
    { LOG_MENU_STATUS_CYCLIC     ,  "CYCLIC"     },
    { LOG_MENU_STATUS_ANALOG     ,  "ANALOG"     },
    { LOG_MENU_STATUS_POSTMORTEM ,  "POSTMORTEM" },
    { LOG_MENU_STATUS_CONTINUOUS ,  "CONTINUOUS" },
    { LOG_MENU_STATUS_FREEZABLE  ,  "FREEZABLE"  },
    { LOG_MENU_STATUS_MPX        ,  "MPX"        },
    { LOG_MENU_STATUS_PM_BUF     ,  "PM_BUF"     },
    { LOG_MENU_STATUS_DISABLED   ,  "DISABLED"   },
    { LOG_MENU_STATUS_RUNNING    ,  "RUNNING"    },
    { LOG_MENU_STATUS_SAVE       ,  "SAVE"       },
    { LOG_MENU_STATUS_FUTURE     ,  "FUTURE"     },
    { LOG_MENU_STATUS_DIM        ,  "DIM"        },
    { LOG_MENU_STATUS_VS1        ,  "VS1"        },
    { LOG_MENU_STATUS_VS2        ,  "VS2"        },
    { LOG_MENU_STATUS_TABLE      ,  "TABLE"      },
    { LOG_MENU_STATUS_ALIAS      ,  "ALIAS"      },
    { 0                          ,  NULL         },
}
#endif // GLOBALS
;

//! LOGSTATUS parameters description structure

CCPARS_LOGSTATUS_EXT struct CC_pars logstatus_pars[]
#ifdef GLOBALS
= {

#include "log_menu_status_pars.h"     // generated automatically by liblog/scripts/write_ccrt.awk from def/log.csv
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_log_menu_status) == LOG_NUM_MENU_STATUS_BITS + 1, mismatch_between_log_menu_status_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
