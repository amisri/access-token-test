//! @file  ccrt/inc/pars/state.h
//!
//! @brief ccrt STATE parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

extern struct REG_mgr reg_mgr;

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_STATE_EXT
#else
#define CCPARS_STATE_EXT extern
#endif

//! Used for:
//! * MODE PC
//! * STATE PC
//! parameters value decoding

CCPARS_STATE_EXT struct CC_pars_enum enum_state_pc[]
#ifdef GLOBALS
= {
    { REF_PC_OFF   , "OFF"   , 0                             },
    { REF_PC_ON    , "ON"    , CC_ENUM_GREEN                 },
    { REF_PC_FAULT , "FAULT" , CC_ENUM_RED|CC_ENUM_READ_ONLY },
    { 0            , NULL    , 0                             },
}
#endif // GLOBALS
;

//! Used for:
//! * MODE REF
//! * STATE REF
//! parameters value decoding

CCPARS_STATE_EXT struct CC_pars_enum enum_state_ref[]
#ifdef GLOBALS
= {
    { REF_OFF           , "OFF"           , 0                              },
    { REF_POL_SWITCHING , "POL_SWITCHING" , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_OFF        , "TO_OFF"        , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_STANDBY    , "TO_STANDBY"    , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_CYCLING    , "TO_CYCLING"    , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_TO_IDLE       , "TO_IDLE"       , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_DIRECT        , "DIRECT"        , CC_ENUM_GREEN                  },
    { REF_STANDBY       , "STANDBY"       , CC_ENUM_GREEN                  },
    { REF_CYCLING       , "CYCLING"       , CC_ENUM_GREEN                  },
    { REF_DYN_ECO       , "DYN_ECO"       , CC_ENUM_READ_ONLY              },
    { REF_FULL_ECO      , "FULL_ECO"      , CC_ENUM_READ_ONLY              },
    { REF_IDLE          , "IDLE"          , CC_ENUM_GREEN                  },
    { REF_ARMED         , "ARMED"         , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_RUNNING       , "RUNNING"       , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { REF_PAUSED        , "PAUSED"        , CC_ENUM_CYAN|CC_ENUM_READ_ONLY },
    { 0                 , NULL            , 0                              },
}
#endif // GLOBALS
;

//! Used for:
//! * STATE FG_STATUS
//! * STATE RAMP_MGR
//! parameters value decoding

CCPARS_STATE_EXT struct CC_pars_enum enum_fg_status[]
#ifdef GLOBALS
= {
    { FG_POST_FUNC   , "POST_FUNC"   , CC_ENUM_CYAN   },
    { FG_DURING_FUNC , "DURING_FUNC" , CC_ENUM_GREEN  },
    { FG_PRE_FUNC    , "PRE_FUNC"    , CC_ENUM_YELLOW },
    { FG_PAUSED      , "PAUSED"      , CC_ENUM_YELLOW },
    { 0              , NULL          , 0              },
}
#endif // GLOBALS
;

//! Used for:
//! * STATE DIRECT
//! parameters value decoding

CCPARS_STATE_EXT struct CC_pars_enum enum_state_direct[]
#ifdef GLOBALS
= {
    { REF_DIRECT_IDLE    , "IDLE"    },
    { REF_DIRECT_RUNNING , "RUNNING" },
    { 0                  , NULL      },
}
#endif // GLOBALS
;

//#include "pars/ref.h"

//! STATE parameters structure

struct CC_pars_state
{
    uint32_t             counter[REF_NUM_STATES];    //!< REF state transitions counter
};

//! Instance of STATE parameters structure

CCPARS_STATE_EXT struct CC_pars_state ccpars_state;

//! Used for accessing STATE parameters

enum CC_state_pars_index_enum
{
    STATE_PC              ,
    STATE_REF             ,
    STATE_REG_MODE        ,
    STATE_POLSWITCH       ,
    STATE_FG_STATUS       ,
    STATE_DIRECT          ,
    STATE_FG_TIME         ,
    STATE_RAMP_MGR        ,
    STATE_POST_FUNC       ,
    STATE_COUNTER         ,
    STATE_TO_OFF_TIME     ,
    STATE_TO_OFF_TIME_S   ,
    STATE_NUM_PARS
};

//! STATE parameters description structure

CCPARS_STATE_EXT struct CC_pars state_pars[]
#ifdef GLOBALS
= {// name             type          max_n_els       *enum                       *value                                            n_els             flags
    { "PC"           , PAR_ENUM,     1,              enum_state_pc,        { .u = refMgrParPointer(&ref_mgr, PC_STATE)            }, 1,              PARS_RO },
    { "REF"          , PAR_ENUM,     1,              enum_state_ref,       { .u = refMgrVarPointer(&ref_mgr, REF_STATE)           }, 1,              PARS_RO },
    { "REG_MODE"     , PAR_ENUM,     1,              enum_reg_mode,        { .u = regMgrVarPointer(&reg_mgr, REG_MODE)            }, 1,              PARS_RO },
    { "POLSWITCH"    , PAR_ENUM,     1,              enum_polswitch_state, { .u = polswitchVarPointer(&polswitch_mgr, STATE)      }, 1,              PARS_RO },
    { "FG_STATUS"    , PAR_ENUM,     1,              enum_fg_status,       { .u = refMgrVarPointer(&ref_mgr, REF_FG_STATUS)       }, 1,              PARS_RO },
    { "DIRECT"       , PAR_ENUM,     1,              enum_state_direct,    { .u = refMgrVarPointer(&ref_mgr, REF_DIRECT_STATE)    }, 1,              PARS_RO },
    { "FG_TIME"      , PAR_FLOAT,    1,              NULL,                 { .f = refMgrVarPointer(&ref_mgr, REF_FG_TIME)         }, 1,              PARS_RO },
    { "RAMP_MGR"     , PAR_ENUM,     1,              enum_fg_status,       { .u = refMgrVarPointer(&ref_mgr, REF_RAMP_MGR)        }, 1,              PARS_RO },
    { "POST_FUNC"    , PAR_BOOL,     1,              NULL,                 { .b = refMgrVarPointer(&ref_mgr, FLAG_POST_FUNC)      }, 1,              PARS_RO },
    { "COUNTER"      , PAR_UNSIGNED, REF_NUM_STATES, NULL,                 { .u = ccpars_state.counter                            }, REF_NUM_STATES, PARS_RO },
    { "TO_OFF_TIME"  , PAR_US_TIME,  1,              NULL,                 { .us = refMgrVarPointer(&ref_mgr, REF_TO_OFF_TIME)    }, 1,              PARS_RO },
    { "TO_OFF_TIME_S", PAR_UNSIGNED, 1,              NULL,                 { .u  = refMgrVarPointer(&ref_mgr, REF_TO_OFF_TIME_S)  }, 1,              PARS_RO },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(state_pars)           == STATE_NUM_PARS           + 1, mismatch_between_state_pars_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_state_pc)        == REF_PC_NUM_STATES        + 1, mismatch_between_enum_state_pc_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_state_ref)       == REF_NUM_STATES           + 1, mismatch_between_enum_state_ref_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_fg_status)       == FG_NUM_STATUSES          + 1, mismatch_between_enum_fg_status_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_state_direct)    == REF_NUM_DIRECT_STATES    + 1, mismatch_between_enum_state_direct_array_size_and_number_of_enum_elements);

#endif // GLOBALS


// EOF

