//! @file  ccrt/inc/pars/global.h
//!
//! @brief ccrt GLOBAL parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_GLOBAL_EXT
#else
#define CCPARS_GLOBAL_EXT extern
#endif

// Include header files to get enum structures

#include "pars/fgerror.h"

//! GLOBAL parameters structure

struct CC_pars_global
{
    uint32_t                    sub_sel;                                 //!< Cycle selector on which to use test RST parameters
    uint32_t                    super_cycle    [CC_MAX_SUPER_CYC_LEN];   //!< Super cycle: SCCBP,...   S=Sub-device selector CC=Cycle selector  BP=Length in basic periods
    uint32_t                    basic_period_ms;                         //!< Cycle basic period in milliseconds
    enum CC_enabled_disabled    test_arm;                                //!< Control test arming
    enum CC_enabled_disabled    hide_errors;                             //!< Hides some error messages when enabled
    enum FG_errno               fg_errno;                                //!< FG error number from arming
    int32_t                     event_advance_ms;                        //!< Cycling event advance in milliseconds
    cc_float                    test_plep_initial_rate;                  //!< Parameter to test the PLEP initial_rate feature
    cc_float                    test_plep_final_rate;                    //!< Parameter to test the PLEP final_rate feature
    cc_float                    float_tolerance;                         //!< Basic float assertion tolerance
    cc_float                    inc_float_tolerance;                     //!< Increased float assertion tolerance
    cc_float                    v_ac_hz;                                 //!< Frequency of simulated V_AC signal
};

//! Instance of GLOBAL parameters structure

CCPARS_GLOBAL_EXT struct CC_pars_global ccpars_global
#ifdef GLOBALS
= {
    .sub_sel                = 0,                                                        // GLOBAL SUB_SEL
    .super_cycle            = { 101, 201, 301, 401, 501, 601, 701, 801, 901, 1001 },    // GLOBAL SUPER_CYCLE
    .basic_period_ms        = 1000,                                                     // GLOBAL BASIC_PERIOD_MS
    .event_advance_ms       = 900,                                                      // GLOBAL EVENT_ADVANCE_MS
    .test_arm               = CC_DISABLED,                                              // GLOBAL TEST_ARM
    .hide_errors            = CC_DISABLED,                                              // GLOBAL HIDE_ERRORS
    .fg_errno               = FG_OK,                                                    // GLOBAL FG_ERRNO
    .float_tolerance        = 1.0E-4,                                                   // GLOBAL FLOAT_TOLERANCE
    .inc_float_tolerance    = 1.0E-2,                                                   // GLOBAL INC_FLOAT_TOLERANCE
    .test_plep_initial_rate = 0.0,                                                      // GLOBAL TEST_PLEP_INITIAL_RATE
    .test_plep_final_rate   = 0.0,                                                      // GLOBAL TEST_PLEP_FINAL_RATE
    .v_ac_hz                = 5.0,                                                      // GLOBAL V_AC_HZ
}
#endif
;

// Global parameter indexes

enum CC_global_pars_index_enum
{
    GLOBAL_ITER_PERIOD_NS             ,
    GLOBAL_ITER_PERIOD                ,
    GLOBAL_SUB_SEL                    ,
    GLOBAL_SUPER_CYCLE                ,
    GLOBAL_BASIC_PERIOD_MS            ,
    GLOBAL_TEST_ARM                   ,
    GLOBAL_HIDE_ERRORS                ,
    GLOBAL_FG_ERRNO                   ,
    GLOBAL_EVENT_ADVANCE_MS           ,
    GLOBAL_FLOAT_TOLERANCE            ,
    GLOBAL_INC_FLOAT_TOLERANCE        ,
    GLOBAL_TEST_PLEP_INITIAL_RATE     ,
    GLOBAL_TEST_PLEP_FINAL_RATE       ,
    GLOBAL_V_AC_HZ                    ,
    GLOBAL_HARMONICS_GAIN             ,
    GLOBAL_HARMONICS_PHASE            ,
    GLOBAL_NUM_PARS
};

//! GLOBAL parameters description structure

CCPARS_GLOBAL_EXT struct CC_pars global_pars[]
#ifdef GLOBALS
= {// name                          type          max_n_els             *enum                         *value                                               n_els                flags
    { "ITER_PERIOD_NS"            , PAR_UNSIGNED, 1,                    NULL,                  { .u = regMgrParAppPointer(&reg_pars, ITER_PERIOD_NS)      }, 1,                 PARS_RW|PARS_REG    },
    { "ITER_PERIOD"               , PAR_FLOAT,    1,                    NULL,                  { .f = regMgrVarPointer(&reg_mgr, ITER_PERIOD)             }, 1,                 PARS_RO             },
    { "SUB_SEL"                   , PAR_UNSIGNED, 1,                    NULL,                  { .u = &ccpars_global.sub_sel                              }, 1,                 PARS_RW             },
    { "SUPER_CYCLE"               , PAR_UNSIGNED, CC_MAX_SUPER_CYC_LEN, NULL,                  { .u =  ccpars_global.super_cycle                          }, 10,                PARS_RW|PARS_CFG    },
    { "BASIC_PERIOD_MS"           , PAR_UNSIGNED, 1,                    NULL,                  { .u = &ccpars_global.basic_period_ms                      }, 1,                 PARS_RW|PARS_CFG    },
    { "EVENT_ADVANCE_MS"          , PAR_SIGNED,   1,                    NULL,                  { .i = &ccpars_global.event_advance_ms                     }, 1,                 PARS_RW|PARS_CFG    },
    { "TEST_ARM"                  , PAR_ENUM,     1,                    enum_enabled_disabled, { .u = &ccpars_global.test_arm                             }, 1,                 PARS_RW             },
    { "HIDE_ERRORS"               , PAR_ENUM,     1,                    enum_enabled_disabled, { .u = &ccpars_global.hide_errors                          }, 1,                 PARS_RW             },
    { "FG_ERRNO"                  , PAR_ENUM,     1,                    enum_fgerror_errno,    { .u = &ccpars_global.fg_errno                             }, 1,                 PARS_RO             },
    { "FLOAT_TOLERANCE"           , PAR_FLOAT,    1,                    NULL,                  { .f = &ccpars_global.float_tolerance                      }, 1,                 PARS_RW|PARS_CFG    },
    { "INC_FLOAT_TOLERANCE"       , PAR_FLOAT,    1,                    NULL,                  { .f = &ccpars_global.inc_float_tolerance                  }, 1,                 PARS_RW|PARS_CFG    },
    { "TEST_PLEP_INITIAL_RATE"    , PAR_FLOAT,    1,                    NULL,                  { .f = &ccpars_global.test_plep_initial_rate               }, 1,                 PARS_RW             },
    { "TEST_PLEP_FINAL_RATE"      , PAR_FLOAT,    1,                    NULL,                  { .f = &ccpars_global.test_plep_final_rate                 }, 1,                 PARS_RW             },
    { "V_AC_HZ"                   , PAR_FLOAT,    1,                    NULL,                  { .f = &ccpars_global.v_ac_hz                              }, 1,                 PARS_RW             },
    { "HARMONICS_GAIN"            , PAR_FLOAT,    REG_NUM_HARMONICS,    NULL,                  { .f = regMgrParAppValue(&reg_pars, HARMONICS_GAIN)        }, REG_NUM_HARMONICS, PARS_RW|PARS_FIXLEN },
    { "HARMONICS_PHASE"           , PAR_FLOAT,    REG_NUM_HARMONICS,    NULL,                  { .f = regMgrParAppValue(&reg_pars, HARMONICS_PHASE)       }, REG_NUM_HARMONICS, PARS_RW|PARS_FIXLEN },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(global_pars) == GLOBAL_NUM_PARS + 1, mismatch_between_global_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
