//! @file  ccrt/inc/pars/vreg.h
//!
//! @brief ccrt voltage regulation parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_VREG_EXT
#else
#define CCPARS_VREG_EXT extern
#endif

//! VREG parameters description structure

CCPARS_VREG_EXT struct CC_pars vreg_pars[]
#ifdef GLOBALS
= {// name      type   max_n_els *enum         *value                                           n_els flags
    { "K_INT" , PAR_FLOAT, 1,     NULL,  { .f = regMgrParAppPointer(&reg_pars, VREG_EXT_K_INT) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_P"   , PAR_FLOAT, 1,     NULL,  { .f = regMgrParAppPointer(&reg_pars, VREG_EXT_K_P  ) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_FF"  , PAR_FLOAT, 1,     NULL,  { .f = regMgrParAppPointer(&reg_pars, VREG_EXT_K_FF ) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_AW"  , PAR_FLOAT, 1,     NULL,  { .f = regMgrVarPointer(   &reg_mgr,  VREG_OP_K_AW  ) }, 1,  PARS_RO                   },
    { "K_W"   , PAR_FLOAT, 1,     NULL,  { .f = regMgrVarPointer(   &reg_mgr,  VREG_OP_K_W   ) }, 1,  PARS_RO                   },
    { NULL }
}
#endif
;

// EOF
