//! @file  ccrt/inc/pars/logmenu.h
//!
//! @brief ccrt LOGMENU parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "logmenu.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGMENU_EXT
#else
#define CCPARS_LOGMENU_EXT extern
#endif

// Define log menus structure with names, properties and menus arrays

CCPARS_LOGMENU_EXT struct LOG_menus log_menus;

//! LOGMENU parameters array - no default values because they are all configuration parameters

CCPARS_LOGMENU_EXT enum CC_enabled_disabled ccpars_logmenu[LOG_NUM_MENUS];

//! LOGMENU parameters description structure

CCPARS_LOGMENU_EXT struct CC_pars logmenu_pars[]
#ifdef GLOBALS
= {

#include "log_menu_pars.h"     // generated automatically by liblog/scripts/write_ccrt.awk from def/log.csv

//     name            type        max_n_els   *enum                          *value                                n_els      flags
    { "NAMES"    , PAR_STRING  , LOG_NUM_MENUS, NULL,                  { .s = (char **)log_menus.names      }, LOG_NUM_MENUS,  PARS_RO },
    { "PROPS"    , PAR_STRING  , LOG_NUM_MENUS, NULL,                  { .s = (char **)log_menus.properties }, LOG_NUM_MENUS,  PARS_RO },
    { "LOG_INDEX", PAR_UNSIGNED, LOG_NUM_MENUS, NULL,                  { .u = log_menus.log_index           }, LOG_NUM_MENUS,  PARS_RO },
    { "PERIOD"   , PAR_FLOAT   , LOG_NUM_MENUS, NULL,                  { .f = log_menus.period              }, LOG_NUM_MENUS,  PARS_RO },
    { "STATUS"   , PAR_HEX     , LOG_NUM_MENUS, NULL,                  { .u = log_menus.status_bit_mask     }, LOG_NUM_MENUS,  PARS_RO },
    { "INIT_PM"  , PAR_ENUM    , LOG_NUM_MENUS, enum_enabled_disabled, { .u = log_menus.init_postmortem     }, LOG_NUM_MENUS,  PARS_RO },
    { "CTRL_PM"  , PAR_ENUM    , LOG_NUM_MENUS, enum_enabled_disabled, { .u = log_menus.ctrl_postmortem     }, LOG_NUM_MENUS,  PARS_RW|PARS_FIXLEN },
    { NULL }
}
#endif
;

// EOF
