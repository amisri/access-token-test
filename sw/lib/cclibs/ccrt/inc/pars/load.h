//! @file  ccrt/inc/pars/load.h
//!
//! @brief ccrt LOAD parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOAD_EXT
#else
#define CCPARS_LOAD_EXT extern
#endif

//! LOAD parameters description structure

CCPARS_LOAD_EXT struct CC_pars load_pars[]
#ifdef GLOBALS
= {// name              type          max_n_els      *enum          *value                                               n_els             flags
    { "SELECT"        , PAR_UNSIGNED, 1,             NULL,   { .u = regMgrParAppPointer(&reg_pars, LOAD_SELECT        ) }, 1,              PARS_RW|PARS_CFG|PARS_REG             },
    { "TEST_SELECT"   , PAR_UNSIGNED, 1,             NULL,   { .u = regMgrParAppPointer(&reg_pars, LOAD_TEST_SELECT   ) }, 1,              PARS_RW|         PARS_REG             },
    { "OHMS_SER"      , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "OHMS_PAR"      , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "OHMS_MAG"      , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "HENRYS"        , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "HENRYS_SAT"    , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "I_SAT_GAIN"    , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "I_SAT_START"   , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "I_SAT_END"     , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SAT_SMOOTHING" , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "GAUSS_PER_AMP" , PAR_FLOAT,    REG_NUM_LOADS, NULL,   { .f = regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ) }, REG_NUM_LOADS,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM_TC_ERROR"  , PAR_FLOAT,    1,             NULL,   { .f = regMgrParAppPointer(&reg_pars, LOAD_SIM_TC_ERROR  ) }, 1,              PARS_RW|PARS_CFG|PARS_REG             },
    { "MEAS_OHMS"     , PAR_FLOAT,    1,             NULL,   { .f = regMgrVarPointer(   &reg_mgr,  LOAD_MEAS_OHMS     ) }, 1,              PARS_RO                               },
    { "MEAS_HENRYS"   , PAR_FLOAT,    1,             NULL,   { .f = regMgrVarPointer(   &reg_mgr,  LOAD_MEAS_HENRYS   ) }, 1,              PARS_RO                               },
    { "I_MAG_SAT"     , PAR_FLOAT,    1,             NULL,   { .f = regMgrVarPointer(   &reg_mgr,  LOAD_I_MAG_SAT     ) }, 1,              PARS_RO                               },
    { NULL }
}
#endif
;

// EOF

