//! @file  ccrt/inc/pars/mode.h
//!
//! @brief ccrt MODE parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_MODE_EXT
#else
#define CCPARS_MODE_EXT extern
#endif

//! Used for:
//! * MODE REG_MODE
//! * MODE REG_MODE_CYC
//! * MODE TEST_REG_MODE
//! * CYCSTATUS PLAYED_REG_MODE
//! * REF ARMED_REG_MODE
//! * STATE REG_MODE
//! parameters value decoding

CCPARS_MODE_EXT struct CC_pars_enum enum_reg_mode[]
#ifdef GLOBALS
= {
    { REG_NONE    , "NONE",    CC_ENUM_READ_ONLY },
    { REG_VOLTAGE , "VOLTAGE", 0                 },
    { REG_CURRENT , "CURRENT", 0                 },
    { REG_FIELD   , "FIELD",   0                 },
    { 0           , NULL                         },
}
#endif
;

//! Used for MODE PRE_FUNC parameter value decoding

CCPARS_MODE_EXT struct CC_pars_enum enum_pre_func_mode[]
#ifdef GLOBALS
= {
    { REF_PRE_FUNC_RAMP            , "RAMP"            },
    { REF_PRE_FUNC_MINRMS          , "MINRMS"          },
    { REF_PRE_FUNC_OPENLOOP_MINRMS , "OPENLP_MINRMS"   },
    { REF_PRE_FUNC_MAGCYCLE        , "MAGCYCLE"        },
    { REF_PRE_FUNC_UPMINMAX        , "UPMINMAX"        },
    { REF_PRE_FUNC_DOWNMAXMIN      , "DOWNMAXMIN"      },
    { 0                            , NULL              },
}
#endif
;

//! Used for MODE POST_FUNC parameter value decoding

CCPARS_MODE_EXT struct CC_pars_enum enum_post_func_mode[]
#ifdef GLOBALS
= {
    { REF_POST_FUNC_MINRMS          , "MINRMS"          },
    { REF_POST_FUNC_OPENLOOP_MINRMS , "OPENLP_MINRMS"   },
    { REF_POST_FUNC_HOLD            , "HOLD"            },
    { 0                             , NULL              },
}
#endif
;

// Include header files to get enum structures

#include "faults.h"
#include "state.h"

//! MODE parameters structure

struct CC_pars_mode
{
    enum REF_pc_state           pc;                  //!< MODE PC - desired PC STATE
};

//! Instance of MODE parameters structure

CCPARS_MODE_EXT struct CC_pars_mode ccpars_mode;

//! Used for accessing MODE parameters

enum CC_mode_pars_index_enum
{
    MODE_PC                  ,
    MODE_REF                 ,
    MODE_REF_SUB_SEL         ,
    MODE_REF_CYC_SEL         ,
    MODE_TEST_CYC_SEL        ,
    MODE_TEST_REF_CYC_SEL    ,
    MODE_TEST_REG_MODE       ,
    MODE_EVENT_OFFSET_US     ,
    MODE_REG_MODE            ,
    MODE_REG_MODE_CYC        ,
    MODE_PRE_FUNC            ,
    MODE_POST_FUNC           ,
    MODE_SIM_MEAS            ,
    MODE_RT_REF              ,
    MODE_RT_NUM_STEPS        ,
    MODE_ECONOMY             ,
    MODE_USE_REF_NOW         ,
    MODE_TO_OFF_FAULTS       ,
    MODE_ILC                 ,
    MODE_HARMONICS           ,
    MODE_NUM_PARS
};

//! MODE parameters description structure

CCPARS_MODE_EXT struct CC_pars mode_pars[]
#ifdef GLOBALS
= {// name                 type       max_n_els                 *enum                         *value                                             n_els                   flags
    { "PC"                , PAR_ENUM,     1,                     enum_state_pc,         { .u = &ccpars_mode.pc                                  }, 1,                    PARS_RW          },
    { "REF"               , PAR_ENUM,     1,                     enum_state_ref,        { .u = refMgrParPointer(&ref_mgr,MODE_REF             ) }, 1,                    PARS_RW          },
    { "REF_SUB_SEL"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_REF_SUB_SEL     ) }, 1,                    PARS_RW|PARS_CFG },
    { "REF_CYC_SEL"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_REF_CYC_SEL     ) }, 1,                    PARS_RW|PARS_CFG },
    { "TEST_CYC_SEL"      , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParPointer(&ref_mgr,MODE_TEST_CYC_SEL    ) }, 1,                    PARS_RW          },
    { "TEST_REF_CYC_SEL"  , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParPointer(&ref_mgr,MODE_TEST_REF_CYC_SEL) }, 1,                    PARS_RW          },
    { "TEST_REG_MODE"     , PAR_ENUM,     1,                     enum_reg_mode,         { .u = refMgrParPointer(&ref_mgr,MODE_TEST_REG_MODE   ) }, 1,                    PARS_RW          },
    { "EVENT_OFFSET_US"   , PAR_SIGNED,   1,                     NULL,                  { .i = refMgrParPointer(&ref_mgr,MODE_EVENT_OFFSET_US ) }, 1,                    PARS_RW          },
    { "REG_MODE"          , PAR_ENUM,     1,                     enum_reg_mode,         { .u = refMgrParPointer(&ref_mgr,MODE_REG_MODE        ) }, 1,                    PARS_RW          },
    { "REG_MODE_CYC"      , PAR_ENUM,     1,                     enum_reg_mode,         { .u = refMgrParPointer(&ref_mgr,MODE_REG_MODE_CYC    ) }, 1,                    PARS_RW|PARS_CFG },
    { "PRE_FUNC"          , PAR_ENUM,     1,                     enum_pre_func_mode,    { .u = refMgrParPointer(&ref_mgr,MODE_PRE_FUNC        ) }, 1,                    PARS_RW|PARS_CFG },
    { "POST_FUNC"         , PAR_ENUM,     1,                     enum_post_func_mode,   { .u = refMgrParPointer(&ref_mgr,MODE_POST_FUNC       ) }, 1,                    PARS_RW|PARS_CFG },
    { "SIM_MEAS"          , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_SIM_MEAS        ) }, 1,                    PARS_RW|PARS_CFG },
    { "RT_REF"            , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_RT_REF          ) }, 1,                    PARS_RW|PARS_CFG },
    { "RT_NUM_STEPS"      , PAR_UNSIGNED, 1,                     NULL,                  { .u = refMgrParPointer(&ref_mgr,RT_NUM_STEPS         ) }, 1,                    PARS_RW|PARS_CFG },
    { "ECONOMY"           , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_ECONOMY         ) }, 1,                    PARS_RW|PARS_CFG },
    { "USE_ARM_NOW"       , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_USE_ARM_NOW     ) }, 1,                    PARS_RW|PARS_CFG },
    { "TO_OFF_FAULTS"     , PAR_BITMASK,  1,                     enum_ref_faults,       { .u = refMgrParPointer(&ref_mgr,MODE_TO_OFF_FAULTS   ) }, 1,                    PARS_RW|PARS_CFG },
    { "ILC"               , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_ILC             ) }, 1,                    PARS_RW|PARS_CFG },
    { "HARMONICS"         , PAR_ENUM,     1,                     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,MODE_HARMONICS       ) }, 1,                    PARS_RW          },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_post_func_mode) == REF_POST_FUNC_NUM_MODES  + 1, mismatch_between_enum_post_func_mode_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(mode_pars)           == MODE_NUM_PARS            + 1, mismatch_between_mode_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
