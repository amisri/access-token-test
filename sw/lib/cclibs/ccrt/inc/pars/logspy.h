//! @file  ccrt/inc/pars/logspy.h
//!
//! @brief ccrt LOG spy buffer parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGSPY_EXT
#else
#define CCPARS_LOGSPY_EXT extern
#endif

//! Spy buffer meta data bit mask
//! The bit indexes must be manually maintained in sync with enum SPY_buf_meta_bit_mask from spy_v3.h

CCPARS_LOGSPY_EXT struct CC_pars_enum enum_spy_meta[]
#ifdef GLOBALS
= {
    { 0 , "LITTLE_ENDIAN"       },
    { 1 , "ANA_SIGNALS"         },
    { 2 , "ANA_SIGS_IN_COLUMNS" },
    { 3 , "TIMESTAMP_IS_LST"    },
    { 0 , NULL                  },
}
#endif // GLOBALS
;

//! LOG parameters structure

struct CC_pars_logspy
{
    uint32_t                version;                                //!< Spy header version
    uint32_t                meta;                                   //!< Spy buffer meta data
    uintptr_t               num_signals;                            //!< Number of signals in the buffer
    uint32_t                sig_meta      [LOG_MAX_SIGS_PER_LOG];   //!< Signal meta data
    uint32_t                dig_bit_index [LOG_MAX_SIGS_PER_LOG];   //!< Digital signal bit index
    int32_t                 time_offset_ns[LOG_MAX_SIGS_PER_LOG];   //!< Signal time offset in nanoseconds
    char                  * name          [LOG_MAX_SIGS_PER_LOG];   //!< Signal name
};

CCPARS_LOGSPY_EXT struct CC_pars_logspy ccpars_logspy;

// Log spy parameter indexes

enum CC_logspy_pars_index_enum
{
    LOGSPY_VERSION            ,
    LOGSPY_META               ,
    LOGSPY_NUM_SIGNALS        ,
    LOGSPY_SIG_META           ,
    LOGSPY_SIG_DIG_BIT_INDEX  ,
    LOGSPY_SIG_TIME_OFFSET_US ,
    LOGSPY_SIG_NAME           ,
    LOGSPY_NUM_PARS
};

//! LOG manager parameters description structure

CCPARS_LOGSPY_EXT struct CC_pars logspy_pars[]
#ifdef GLOBALS
= {// name                  type           max_n_els              *enum                 *value                         n_els flags
    { "VERSION"           , PAR_UNSIGNED,  1,                     NULL,          { .u = &ccpars_logspy.version        }, 1,  PARS_RO },
    { "META"              , PAR_BITMASK,   1,                     enum_spy_meta, { .u = &ccpars_logspy.meta           }, 1,  PARS_RO },
    { "NUM_SIGNALS"       , PAR_UINTPTR,   1,                     NULL,          { .r = &ccpars_logspy.num_signals    }, 1,  PARS_RO },
    { "SIG_META"          , PAR_HEX,       LOG_MAX_SIGS_PER_LOG,  NULL,          { .u =  ccpars_logspy.sig_meta       }, 0,  PARS_RO },
    { "SIG_DIG_BIT_INDEX" , PAR_UNSIGNED,  LOG_MAX_SIGS_PER_LOG,  NULL,          { .u =  ccpars_logspy.dig_bit_index  }, 0,  PARS_RO },
    { "SIG_TIME_OFFSET_NS", PAR_SIGNED,    LOG_MAX_SIGS_PER_LOG,  NULL,          { .i =  ccpars_logspy.time_offset_ns }, 0,  PARS_RO },
    { "SIG_NAME"          , PAR_STRING,    LOG_MAX_SIGS_PER_LOG,  NULL,          { .s =  ccpars_logspy.name           }, 0,  PARS_RO },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(logspy_pars) == LOGSPY_NUM_PARS + 1, mismatch_between_logspy_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
