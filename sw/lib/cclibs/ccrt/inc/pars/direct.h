//! @file  ccrt/inc/pars/direct.h
//!
//! @brief ccrt DIRECT parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_DIRECT_EXT
#else
#define CCPARS_DIRECT_EXT extern
#endif

//! DIRECT parameters description structure
//!
//! V_REF, I_REF and B_REF should have the same order as reg modes in REG_mode enum - the enum is used to access them.

CCPARS_DIRECT_EXT struct CC_pars direct_pars[]
#ifdef GLOBALS
= {// name               type     max_n_els *enum           *value                                             n_els  flags
    { "V_REF"          , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&ref_mgr,DIRECT_V_REF          ) }, 1,   PARS_RW|PARS_CFG },
    { "I_REF"          , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&ref_mgr,DIRECT_I_REF          ) }, 1,   PARS_RW|PARS_CFG },
    { "B_REF"          , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&ref_mgr,DIRECT_B_REF          ) }, 1,   PARS_RW|PARS_CFG },
    { "DEGAUSS_AMP_PP" , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&ref_mgr,DIRECT_DEGAUSS_AMP_PP ) }, 1,   PARS_RW|PARS_CFG },
    { "DEGAUSS_PERIOD" , PAR_FLOAT,   1,     NULL,   { .f = refMgrParPointer(&ref_mgr,DIRECT_DEGAUSS_PERIOD ) }, 1,   PARS_RW|PARS_CFG },
    { NULL }
}
#endif
;

// EOF

