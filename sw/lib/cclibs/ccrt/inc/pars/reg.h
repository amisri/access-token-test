//! @file  ccrt/inc/pars/reg.h
//!
//! @brief ccrt REG parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_REG_EXT
#else
#define CCPARS_REG_EXT extern
#endif

//! Used for:
//! * BREG INT_MEAS_SELECT
//! * BREG EXT_MEAS_SELECT
//! * BREG OP_MEAS_SELECT
//! * BREG TEST_MEAS_SELECT
//! * IREG INT_MEAS_SELECT
//! * IREG EXT_MEAS_SELECT
//! * IREG OP_MEAS_SELECT
//! * IREG TEST_MEAS_SELECT
//! parameters value decoding

CCPARS_REG_EXT struct CC_pars_enum enum_reg_meas_select[]
#ifdef GLOBALS
= {
    { REG_MEAS_UNFILTERED   , "UNFILTERED"   },
    { REG_MEAS_FILTERED     , "FILTERED"     },
    { REG_MEAS_EXTRAPOLATED , "EXTRAPOLATED" },
    { 0                     , NULL           },
}
#endif
;

//! Used for:
//! * IREG OP_STATUS
//! * IREG TEST_STATUS
//! * BREG OP_STATUS
//! * BREG TEST_STATUS
//! * VREG OP_STATUS
//! parameters decoding

CCPARS_REG_EXT struct CC_pars_enum enum_reg_status[]
#ifdef GLOBALS
= {
    { REG_OK                                   , "OK"                                   },
    { REG_WARNING_LOW_MODULUS_MARGIN           , "WARNING_LOW_MODULUS_MARGIN"           },
    { REG_FAULT_OHMS_PAR_TOO_SMALL             , "FAULT_OHMS_PAR_TOO_SMALL"             },
    { REG_FAULT_PURE_DELAY_TOO_LARGE           , "FAULT_PURE_DELAY_TOO_LARGE"           },
    { REG_FAULT_R0_IS_ZERO                     , "FAULT_R0_IS_ZERO"                     },
    { REG_FAULT_S0_NOT_POSITIVE                , "FAULT_S0_NOT_POSITIVE"                },
    { REG_FAULT_T0_NOT_POSITIVE                , "FAULT_T0_NOT_POSITIVE"                },
    { REG_FAULT_S_HAS_UNSTABLE_ROOT            , "FAULT_S_HAS_UNSTABLE_ROOT"            },
    { REG_FAULT_T_HAS_UNSTABLE_ROOT            , "FAULT_T_HAS_UNSTABLE_ROOT"            },
    { 0                                        , NULL                                   },
}
#endif
;

//! BREG parameters description structure

CCPARS_REG_EXT struct CC_pars breg_pars[]
#ifdef GLOBALS
= {// name                        type          max_n_els,          *enum,                        *value,                                                             n_els             flags
    { "PERIOD_ITERS"            , PAR_UNSIGNED, REG_NUM_LOADS,      NULL,                  { .u = regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXTERNAL_ALG"            , PAR_ENUM,     REG_NUM_LOADS,      enum_enabled_disabled, { .u = regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_MEAS_SELECT"         , PAR_ENUM,     REG_NUM_LOADS,      enum_reg_meas_select,  { .u = regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_PURE_DELAY_PERIODS"  , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE1_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLES2_HZ"        , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLES2_Z"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE4_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE5_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_MEAS_SELECT"         , PAR_ENUM,     REG_NUM_LOADS,      enum_reg_meas_select,  { .u = regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_TRACK_DELAY_PERIODS" , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ) }, REG_NUM_LOADS,       PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_R"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_S"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_T"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_TEST_R"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_TEST_R               ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "EXT_TEST_S"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_TEST_S               ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "EXT_TEST_T"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, BREG_EXT_TEST_T               ) }, REG_NUM_RST_COEFFS,  PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "OP_STATUS"               , PAR_ENUM,     1,                  enum_reg_status,       { .u = regMgrVarPointer(  &reg_mgr, BREG_OP_STATUS                 ) }, 1,                   PARS_RO                               },
    { "OP_ALG_INDEX"            , PAR_UNSIGNED, 1,                  NULL,                  { .u = regMgrVarPointer(  &reg_mgr, BREG_OP_ALG_INDEX              ) }, 1,                   PARS_RO                               },
    { "OP_REF_ADVANCE"          , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_REF_ADVANCE            ) }, 1,                   PARS_RO                               },
    { "OP_MEAS_SELECT"          , PAR_ENUM,     1,                  enum_reg_meas_select,  { .u = regMgrVarPointer(  &reg_mgr, BREG_OP_MEAS_SELECT            ) }, 1,                   PARS_RO                               },
    { "OP_PURE_DELAY_PERIODS"   , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_PURE_DELAY_PERIODS     ) }, 1,                   PARS_RO                               },
    { "OP_TRACK_DELAY_PERIODS"  , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_TRACK_DELAY_PERIODS    ) }, 1,                   PARS_RO                               },
    { "OP_REF_DELAY_PERIODS"    , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_REF_DELAY_PERIODS      ) }, 1,                   PARS_RO                               },
    { "OP_MOD_MARGIN"           , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_MOD_MARGIN             ) }, 1,                   PARS_RO                               },
    { "OP_MOD_MARGIN_HZ"        , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_OP_MOD_MARGIN_HZ          ) }, 1,                   PARS_RO                               },
    { "OP_R"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_OP_R                      ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { "OP_S"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_OP_S                      ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { "OP_T"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_OP_T                      ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { "TEST_STATUS"             , PAR_ENUM,     1,                  enum_reg_status,       { .u = regMgrVarPointer(  &reg_mgr, BREG_TEST_STATUS               ) }, 1,                   PARS_RO                               },
    { "TEST_ALG_INDEX"          , PAR_UNSIGNED, 1,                  NULL,                  { .u = regMgrVarPointer(  &reg_mgr, BREG_TEST_ALG_INDEX            ) }, 1,                   PARS_RO                               },
    { "TEST_REF_ADVANCE"        , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_REF_ADVANCE          ) }, 1,                   PARS_RO                               },
    { "TEST_MEAS_SELECT"        , PAR_ENUM,     1,                  enum_reg_meas_select,  { .u = regMgrVarPointer(  &reg_mgr, BREG_TEST_MEAS_SELECT          ) }, 1,                   PARS_RO                               },
    { "TEST_PURE_DELAY_PERIODS" , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_PURE_DELAY_PERIODS   ) }, 1,                   PARS_RO                               },
    { "TEST_TRACK_DELAY_PERIODS", PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_TRACK_DELAY_PERIODS  ) }, 1,                   PARS_RO                               },
    { "TEST_REF_DELAY_PERIODS"  , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_REF_DELAY_PERIODS    ) }, 1,                   PARS_RO                               },
    { "TEST_MOD_MARGIN"         , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_MOD_MARGIN           ) }, 1,                   PARS_RO                               },
    { "TEST_MOD_MARGIN_HZ"      , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, BREG_TEST_MOD_MARGIN_HZ        ) }, 1,                   PARS_RO                               },
    { "TEST_R"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_TEST_R                    ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { "TEST_S"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_TEST_S                    ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { "TEST_T"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, BREG_TEST_T                    ) }, REG_NUM_RST_COEFFS,  PARS_RO                               },
    { NULL }
}
#endif
;

//! IREG parameters description structure

CCPARS_REG_EXT struct CC_pars ireg_pars[]
#ifdef GLOBALS
= {// name                        type          max_n_els           *enum                         *value                                                              n_els              flags
    { "PERIOD_ITERS"            , PAR_UNSIGNED, REG_NUM_LOADS,      NULL,                  { .u = regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXTERNAL_ALG"            , PAR_ENUM,     REG_NUM_LOADS,      enum_enabled_disabled, { .u = regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_MEAS_SELECT"         , PAR_ENUM,     REG_NUM_LOADS,      enum_reg_meas_select,  { .u = regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_PURE_DELAY_PERIODS"  , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE1_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLES2_HZ"        , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLES2_Z"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE4_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "INT_AUXPOLE5_HZ"         , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_MEAS_SELECT"         , PAR_ENUM,     REG_NUM_LOADS,      enum_reg_meas_select,  { .u = regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_TRACK_DELAY_PERIODS" , PAR_FLOAT,    REG_NUM_LOADS,      NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ) }, REG_NUM_LOADS,        PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_R"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_S"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_OP_T"                , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|PARS_CFG|PARS_FIXLEN },
    { "EXT_TEST_R"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_TEST_R               ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "EXT_TEST_S"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_TEST_S               ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "EXT_TEST_T"              , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrParAppValue( &reg_pars, IREG_EXT_TEST_T               ) }, REG_NUM_RST_COEFFS,   PARS_RW|PARS_REG|         PARS_FIXLEN },
    { "OP_STATUS"               , PAR_ENUM,     1,                  enum_reg_status,       { .u = regMgrVarPointer(  &reg_mgr, IREG_OP_STATUS                 ) }, 1,                    PARS_RO                               },
    { "OP_ALG_INDEX"            , PAR_UNSIGNED, 1,                  NULL,                  { .u = regMgrVarPointer(  &reg_mgr, IREG_OP_ALG_INDEX              ) }, 1,                    PARS_RO                               },
    { "OP_REF_ADVANCE"          , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_REF_ADVANCE            ) }, 1,                    PARS_RO                               },
    { "OP_MEAS_SELECT"          , PAR_ENUM,     1,                  enum_reg_meas_select,  { .u = regMgrVarPointer(  &reg_mgr, IREG_OP_MEAS_SELECT            ) }, 1,                    PARS_RO                               },
    { "OP_PURE_DELAY_PERIODS"   , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_PURE_DELAY_PERIODS     ) }, 1,                    PARS_RO                               },
    { "OP_TRACK_DELAY_PERIODS"  , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_TRACK_DELAY_PERIODS    ) }, 1,                    PARS_RO                               },
    { "OP_REF_DELAY_PERIODS"    , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_REF_DELAY_PERIODS      ) }, 1,                    PARS_RO                               },
    { "OP_MOD_MARGIN"           , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_MOD_MARGIN             ) }, 1,                    PARS_RO                               },
    { "OP_MOD_MARGIN_HZ"        , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_OP_MOD_MARGIN_HZ          ) }, 1,                    PARS_RO                               },
    { "OP_R"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_OP_R                      ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { "OP_S"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_OP_S                      ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { "OP_T"                    , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_OP_T                      ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { "TEST_STATUS"             , PAR_ENUM,     1,                  enum_reg_status,       { .u = regMgrVarPointer(  &reg_mgr, IREG_TEST_STATUS               ) }, 1,                    PARS_RO                               },
    { "TEST_ALG_INDEX"          , PAR_UNSIGNED, 1,                  NULL,                  { .u = regMgrVarPointer(  &reg_mgr, IREG_TEST_ALG_INDEX            ) }, 1,                    PARS_RO                               },
    { "TEST_REF_ADVANCE"        , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_REF_ADVANCE          ) }, 1,                    PARS_RO                               },
    { "TEST_MEAS_SELECT"        , PAR_ENUM,     1,                  enum_reg_meas_select,  { .u = regMgrVarPointer(  &reg_mgr, IREG_TEST_MEAS_SELECT          ) }, 1,                    PARS_RO                               },
    { "TEST_PURE_DELAY_PERIODS" , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_PURE_DELAY_PERIODS   ) }, 1,                    PARS_RO                               },
    { "TEST_TRACK_DELAY_PERIODS", PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_TRACK_DELAY_PERIODS  ) }, 1,                    PARS_RO                               },
    { "TEST_REF_DELAY_PERIODS"  , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_REF_DELAY_PERIODS    ) }, 1,                    PARS_RO                               },
    { "TEST_MOD_MARGIN"         , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_MOD_MARGIN           ) }, 1,                    PARS_RO                               },
    { "TEST_MOD_MARGIN_HZ"      , PAR_FLOAT,    1,                  NULL,                  { .f = regMgrVarPointer(  &reg_mgr, IREG_TEST_MOD_MARGIN_HZ        ) }, 1,                    PARS_RO                               },
    { "TEST_R"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_TEST_R                    ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { "TEST_S"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_TEST_S                    ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { "TEST_T"                  , PAR_FLOAT,    REG_NUM_RST_COEFFS, NULL,                  { .f = regMgrVarValue(    &reg_mgr, IREG_TEST_T                    ) }, REG_NUM_RST_COEFFS,   PARS_RO                               },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_reg_status) == REG_STATUS_LEN + 1, mismatch_between_enum_reg_status_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_reg_meas_select) == REG_MEAS_NUM_SELECTS + 1, mismatch_between_enum_reg_meas_select_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
