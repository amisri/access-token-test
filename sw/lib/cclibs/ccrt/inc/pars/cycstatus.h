//! @file  ccrt/inc/pars/cycstatus.h
//!
//! @brief ccrt Cycle Status parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_CYCSTATUS_EXT
#else
#define CCPARS_CYCSTATUS_EXT extern
#endif

//! Used for CYCSTATUS FUNC_STATUS parameter value decoding

CCPARS_CYCSTATUS_EXT struct CC_pars_enum enum_cyc_status[]
#ifdef GLOBALS
= {
    { REF_CYC_REG_OK_BIT          , "REG_OK"          },
    { REF_CYC_REG_WRN_BIT         , "REG_WRN"         },
    { REF_CYC_REG_FLT_BIT         , "REG_FLT"         },
    { REF_CYC_ABORTED_BIT         , "ABORTED"         },
    { REF_CYC_WAS_OVERRUN_BIT     , "WAS_OVERRUN"     },
    { REF_CYC_NO_PRE_FUNC_BIT     , "NO_PRE_FUNC"     },
    { REF_CYC_PRE_FUNC_WRN_BIT    , "PRE_FUNC_WRN"    },
    { REF_CYC_DYN_ECO_ARM_WRN_BIT , "DYN_ECO_ARM_WRN" },
    { REF_CYC_DYN_ECO_RUN_WRN_BIT , "DYN_ECO_RUN_WRN" },
    { REF_CYC_INCOMPLETE_WRN_BIT  , "INCOMPLETE_WRN"  },
    { REF_CYC_ECO_DYN_BIT         , "ECO_DYN"         },
    { REF_CYC_ECO_ONCE_BIT        , "ECO_ONCE"        },
    { 0                           , NULL              }
}
#endif // GLOBALS
;

//! CYCSTATUS parameters structure

struct CC_pars_cycstatus_pars
{
    cc_float           latched_unfltr_meas1;                    //!< First latched unfiltered measurement
    cc_float           latched_unfltr_meas2;                    //!< Second latched unfiltered measurement
};

CCPARS_CYCSTATUS_EXT struct CC_pars_cycstatus_pars ccpars_cycstatus[CC_NUM_CYC_SELS];

//! CYCSTATUS parameters description structure

CCPARS_CYCSTATUS_EXT struct CC_pars cycstatus_pars[]
#ifdef GLOBALS
= {// name                      type      max_n_els *enum                     *value                                                       n_els flags                             size_of_struct
    { "COUNTER"               , PAR_UNSIGNED, 1,     NULL,            { .u  = refMgrVarPointer(&ref_mgr, CYCSTATUS_COUNTER)               }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "STATUS"                , PAR_BITMASK,  1,     enum_cyc_status, { .u  = refMgrVarPointer(&ref_mgr, CYCSTATUS_STATUS)                }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "FG_TYPE"               , PAR_ENUM,     1,     enum_fg_type,    { .u  = refMgrVarPointer(&ref_mgr, CYCSTATUS_FG_TYPE)               }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "REG_MODE"              , PAR_ENUM,     1,     enum_reg_mode,   { .u  = refMgrVarPointer(&ref_mgr, CYCSTATUS_REG_MODE)              }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "EVENT_TIME"            , PAR_US_TIME,  1,     NULL,            { .us = refMgrVarPointer(&ref_mgr, CYCSTATUS_EVENT_TIME)            }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "MAX_ABS_REG_ERR"       , PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_MAX_ABS_REG_ERR)       }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "PRE_FUNC_PLATEAU_ERR"  , PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_PRE_FUNC_PLATEAU_ERR)  }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "ILC_INITIAL_RMS_ERR"   , PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_ILC_INITIAL_RMS_ERR)   }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "ILC_RMS_ERR"           , PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_ILC_RMS_ERR)           }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "ILC_RMS_V_REF_RATE"    , PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_ILC_RMS_V_REF_RATE)    }, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "ILC_RMS_V_REF_RATE_LIM", PAR_FLOAT,    1,     NULL,            { .f  = refMgrVarPointer(&ref_mgr, CYCSTATUS_ILC_RMS_V_REF_RATE_LIM)}, 1,  PARS_RO|PARS_CYC_SEL|PARS_SUB_SEL                                       },
    { "LATCHED_UNFLTR_MEAS1"  , PAR_FLOAT,    1,     NULL,            { .f  = &ccpars_cycstatus[0].latched_unfltr_meas1                   }, 1,  PARS_RO|PARS_CYC_SEL,             sizeof(struct CC_pars_cycstatus_pars) },
    { "LATCHED_UNFLTR_MEAS2"  , PAR_FLOAT,    1,     NULL,            { .f  = &ccpars_cycstatus[0].latched_unfltr_meas2                   }, 1,  PARS_RO|PARS_CYC_SEL,             sizeof(struct CC_pars_cycstatus_pars) },
    { NULL }
}
#endif // GLOBALS
;

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_cyc_status) == REF_CYC_NUM_STATUSES + 1, mismatch_between_cyc_status_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

