//! @file  ccrt/inc/pars/economy.h
//!
//! @brief ccrt ECONOMY parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_ECONOMY_EXT
#else
#define CCPARS_ECONOMY_EXT extern
#endif

//! ECONOMY parameters structure

struct CC_pars_economy
{
    enum CC_enabled_disabled    once;                                //!< ECONOMY ONCE
};

//! Instance of ECONOMY parameters structure

CCPARS_ECONOMY_EXT struct CC_pars_economy ccpars_economy;

//! ECONOMY parameters description structure

CCPARS_ECONOMY_EXT struct CC_pars economy_pars[]
#ifdef GLOBALS
= {// name         type   max_n_els *enum                          *value                                         n_els flags
    { "FULL"     , PAR_ENUM,  1,     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,ECONOMY_FULL     ) }, 1,  PARS_RW },
    { "ONCE"     , PAR_ENUM,  1,     enum_enabled_disabled, { .u = &ccpars_economy.once                         }, 1,  PARS_RW },
    { "DYNAMIC"  , PAR_ENUM,  1,     enum_enabled_disabled, { .u = refMgrParPointer(&ref_mgr,ECONOMY_DYNAMIC  ) }, 1,  PARS_RW },
    { NULL }
}
#endif
;

// EOF

