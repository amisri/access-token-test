//! @file  ccrt/inc/pars/ref.h
//!
//! @brief ccrt REF parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_REF_EXT
#else
#define CCPARS_REF_EXT extern
#endif

//! Used for REF FG_TYPE parameter value decoding

CCPARS_REF_EXT struct CC_pars_enum enum_fg_type[]
#ifdef GLOBALS
= {
    { FG_NONE    , "NONE"    , 0                               },
    { FG_ZERO    , "ZERO"    , CC_ENUM_GREEN|CC_ENUM_READ_ONLY },
    { FG_RAMP    , "RAMP"    , CC_ENUM_GREEN                   },
    { FG_PULSE   , "PULSE"   , CC_ENUM_GREEN                   },
    { FG_PLEP    , "PLEP"    , CC_ENUM_GREEN                   },
    { FG_PPPL    , "PPPL"    , CC_ENUM_GREEN                   },
    { FG_CUBEXP  , "CUBEXP"  , CC_ENUM_GREEN                   },
    { FG_TABLE   , "TABLE"   , CC_ENUM_GREEN                   },
    { FG_LTRIM   , "LTRIM"   , CC_ENUM_GREEN                   },
    { FG_CTRIM   , "CTRIM"   , CC_ENUM_GREEN                   },
    { FG_STEPS   , "STEPS"   , CC_ENUM_CYAN                    },
    { FG_SQUARE  , "SQUARE"  , CC_ENUM_CYAN                    },
    { FG_SINE    , "SINE"    , CC_ENUM_CYAN                    },
    { FG_COSINE  , "COSINE"  , CC_ENUM_CYAN                    },
    { FG_OFFCOS  , "OFFCOS"  , CC_ENUM_CYAN                    },
    { FG_PRBS    , "PRBS"    , CC_ENUM_CYAN                    },
    { 0          , NULL                                        },
}
#endif
;

//! Used for REF POLARITY parameter value decoding

CCPARS_REF_EXT struct CC_pars_enum enum_func_pol[]
#ifdef GLOBALS
= {
    { FG_FUNC_POL_ZERO     , "ZERO"     },
    { FG_FUNC_POL_POSITIVE , "POSITIVE" },
    { FG_FUNC_POL_NEGATIVE , "NEGATIVE" },
    { FG_FUNC_POL_BOTH     , "BOTH"     },
    { 0                    , NULL       },
}
#endif
;

// Include header files

#include "ccRun.h"
#include "mode.h"

// REF parameters are split into two structures, REF_ref and REF_ctrl. This is because REF FG_TYPE is transactional
// while REF PLAY and REF DYN_ECO_END_TIME are not. These cannot share a structure but they share the same REF top
// level ccrt parameter group.

//! Instance of REF_FUNC parameters structure

CCPARS_REF_EXT struct REF_ctrl_pars ccpars_ctrl[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .play             = { CC_ENABLED       },     // REF PLAY(0)
               .dyn_eco_end_time = { 0.0F             }      // REF DYN_ECO_END_TIME(0)
    }
}
#endif
;

//! Instance of REF parameters structure

CCPARS_REF_EXT struct REF_ref_pars ccpars_ref[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS]
#ifdef GLOBALS
= {
    [0][0] = { .fg_type = { FG_NONE          } }     // REF FG_TYPE(0)
}
#endif
;

// Global parameter indexes

enum CC_ref_pars_index_enum
{
    REF_PAR_PLAY              ,
    REF_PAR_DYN_ECO_END_TIME  ,
    REF_PAR_FG_TYPE           ,
};

//! REF parameters description structure

CCPARS_REF_EXT struct CC_pars ref_pars[]
#ifdef GLOBALS
= {// name                type           max_n_els                 *enum                  *value                                                        n_els                      flags                                                   size_of_struct
    { "PLAY"            , PAR_ENUM     , 1,                        enum_enabled_disabled, { .u =  ccpars_ctrl[0][0].play                              }, 1,                        PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_CFG,             sizeof(struct REF_ctrl_pars)  },
    { "DYN_ECO_END_TIME", PAR_FLOAT    , 1,                        NULL,                  { .f =  ccpars_ctrl[0][0].dyn_eco_end_time                  }, 1,                        PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_CFG,             sizeof(struct REF_ctrl_pars)  },
    { "FG_TYPE"         , PAR_ENUM     , 1,                        enum_fg_type,          { .u =  ccpars_ref[0][0].fg_type                            }, 1,                        PARS_RW|PARS_SUB_SEL|PARS_CYC_SEL|PARS_CFG|PARS_TRANS,  sizeof(struct REF_ref_pars)   },
    { "ARMED_REG_MODE"  , PAR_ENUM     , 1,                        enum_reg_mode,         { .u = refMgrVarPointer(&ref_mgr, REFARMED_REG_MODE)        }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "ARMED_FG_TYPE"   , PAR_ENUM     , 1,                        enum_fg_type,          { .u = refMgrVarPointer(&ref_mgr, REFARMED_FG_TYPE)         }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "PARS"            , PAR_FGIDX    , REF_FG_PAR_MAX_PER_GROUP, NULL,                  { .x = &ccrun.fg_pars_idx[0][0][0]                          }, REF_FG_PAR_MAX_PER_GROUP, PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL,sizeof(enum REF_fg_par_idx)*REF_FG_PAR_MAX_PER_GROUP},
    { "POLARITY"        , PAR_ENUM     , 1,                        enum_func_pol,         { .u = refMgrVarPointer(&ref_mgr, REFARMED_POLARITY)        }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "LIMITS_INVERTED" , PAR_BOOL     , 1,                        NULL,                  { .b = refMgrVarPointer(&ref_mgr, REFARMED_LIMITS_INVERTED) }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "START_TIME"      , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_START_TIME)      }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "END_TIME"        , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_END_TIME)        }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "DURATION"        , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_DURATION)        }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "INITIAL_REF"     , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_INITIAL_REF)     }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "MIN_REF"         , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_MIN_REF)         }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "MAX_REF"         , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_MAX_REF)         }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "FINAL_REF"       , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_FINAL_REF)       }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "FINAL_RATE"      , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_FINAL_RATE)      }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "MIN_LIMIT"       , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_MIN_LIMIT)       }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "MAX_LIMIT"       , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_MAX_LIMIT)       }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { "RATE_LIMIT"      , PAR_FLOAT    , 1,                        NULL,                  { .f = refMgrVarPointer(&ref_mgr, REFARMED_RATE_LIMIT)      }, 1,                        PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL                                                     },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_fg_type)  == FG_NUM_FUNCS           + 1, mismatch_between_enum_fg_type_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_reg_mode) == REG_NUM_MODES          + 1, mismatch_between_enum_reg_mode_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_func_pol) == FG_FUNC_NUM_POLARITIES + 1, mismatch_between_enum_func_pol_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

