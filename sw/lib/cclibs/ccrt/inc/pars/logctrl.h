//! @file  ccrt/inc/pars/logctrl.h
//!
//! @brief ccrt LOG request parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGCTRL_EXT
#else
#define CCPARS_LOGCTRL_EXT extern
#endif

// LOGCTRL enums

CCPARS_LOGCTRL_EXT struct CC_pars_enum enum_read_control_status[]
#ifdef GLOBALS
= {
    { LOG_READ_CONTROL_DISABLED           , "DISABLED"           },
    { LOG_READ_CONTROL_SUCCESS            , "SUCCESS"            },
    { LOG_READ_CONTROL_NO_SIGNALS         , "NO_SIGNALS"         },
    { LOG_READ_CONTROL_INVALID_CYC_SEL    , "INVALID_CYC_SEL"    },
    { LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES , "NOT_ENOUGH_SAMPLES" },
    { LOG_READ_CONTROL_LOG_OVERWRITTEN    , "LOG_OVERWRITTEN"    },
    { LOG_READ_CONTROL_BUFFER_TOO_SMALL   , "BUFFER_TOO_SMALL"   },
    { LOG_READ_CONTROL_INVALID_INDEX      , "INVALID_INDEX"      },
    { LOG_READ_CONTROL_NO_NEW_DATA        , "NO_NEW_DATA"        },
    { 0,                                     NULL                },
}
#endif // GLOBALS
;

// Instance of LOG read request parameters structure

CCPARS_LOGCTRL_EXT struct LOG_read_control log_read_control[LOG_NUM_LIBLOG_MENUS];

//! LOG read control parameters description structure

CCPARS_LOGCTRL_EXT struct CC_pars logctrl_pars[]
#ifdef GLOBALS
= {// name                    type          max_n_els            *enum                             *value                                              n_els          flags     size_of_struct
    { "STATUS"              , PAR_ENUM,     LOG_NUM_LIBLOG_MENUS, enum_read_control_status, { .u = &log_read_control->status                     }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "TIME_ORIGIN_S"       , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->time_origin.secs.abs       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "TIME_ORIGIN_NS"      , PAR_SIGNED,   LOG_NUM_LIBLOG_MENUS, NULL,                     { .i = &log_read_control->time_origin.ns             }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "FIRST_SAMPLE_TIME_S" , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->first_sample_time.secs.abs }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "FIRST_SAMPLE_TIME_NS", PAR_SIGNED,   LOG_NUM_LIBLOG_MENUS, NULL,                     { .i = &log_read_control->first_sample_time.ns       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "LAST_SAMPLE_TIME_S"  , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->last_sample_time.secs.abs  }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "LAST_SAMPLE_TIME_NS" , PAR_SIGNED,   LOG_NUM_LIBLOG_MENUS, NULL,                     { .i = &log_read_control->last_sample_time.ns        }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "PERIOD_S"            , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .i = &log_read_control->period.secs.rel            }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "PERIOD_NS"           , PAR_SIGNED,   LOG_NUM_LIBLOG_MENUS, NULL,                     { .i = &log_read_control->period.ns                  }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "META"                , PAR_HEX,      LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->meta                       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "NUM_SIGNALS"         , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->num_signals                }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "NUM_SIG_BUFS"        , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->num_sig_bufs               }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "NUM_SAMPLES"         , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->num_samples                }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "NUM_SAMPLES_FROM_LOG", PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->num_samples_from_log       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "SUB_SAMPLING_PERIOD" , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->sub_sampling_period        }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "HEADER_SIZE"         , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->header_size                }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "DATA_SIZE"           , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->data_size                  }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "TOTAL_SIZE"          , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->total_size                 }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "DATA_LEN"            , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->data_len                   }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "BUF_OFFSET"          , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->buf_offset                 }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "BUF_LEN"             , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->buf_len                    }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "ELEMENT_OFFSET"      , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->output.element_offset      }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { "STEP"                , PAR_UNSIGNED, LOG_NUM_LIBLOG_MENUS, NULL,                     { .u = &log_read_control->output.step                }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_control) },
    { NULL }
}
#endif // GLOBALS
;

// EOF
