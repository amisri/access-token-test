//! @file  ccrt/inc/pars/logreq.h
//!
//! @brief ccrt LOG request parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGREQ_EXT
#else
#define CCPARS_LOGREQ_EXT extern
#endif

// Instance of LOG read request parameters structure

CCPARS_LOGREQ_EXT struct LOG_read_request log_read_request[LOG_NUM_LIBLOG_MENUS];

//! LOG read request parameters description structure

CCPARS_LOGREQ_EXT struct CC_pars logreq_pars[]
#ifdef GLOBALS
= {// name                   type           max_n_els              *enum        *value                                                     n_els         flags    size_of_struct
    { "MENU_NAME"          , PAR_STRING,    LOG_NUM_LIBLOG_MENUS,  NULL, { .s = (char **)&log_menus.names                            }, LOG_NUM_LIBLOG_MENUS,  PARS_RO                                   },
    { "LOG_INDEX"          , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->log_index                         }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "ACTION"             , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->action                            }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "CYC_SEL"            , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.cyc_sel                     }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "TIME_ORIGIN_S"      , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.timing.time_origin.secs.abs }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "TIME_ORIGIN_NS"     , PAR_SIGNED,    LOG_NUM_LIBLOG_MENUS,  NULL, { .i = &log_read_request->u.get.timing.time_origin.ns       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "TIME_OFFSET_MS"     , PAR_SIGNED,    LOG_NUM_LIBLOG_MENUS,  NULL, { .i = &log_read_request->u.get.timing.time_offset_ms       }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "DURATION_MS"        , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.timing.duration_ms          }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "SELECTORS_BIT_MASK" , PAR_HEX,       LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.selectors_bit_mask          }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "SIGNALS_BIT_MASK"   , PAR_HEX,       LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.signals_bit_mask            }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "MAX_BUFFER_SIZE"    , PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.max_buffer_size             }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { "SUB_SAMPLING_PERIOD", PAR_UNSIGNED,  LOG_NUM_LIBLOG_MENUS,  NULL, { .u = &log_read_request->u.get.sub_sampling_period         }, LOG_NUM_LIBLOG_MENUS,  PARS_RO,  sizeof(struct LOG_read_request) },
    { NULL }
}
#endif // GLOBALS
;

// EOF
