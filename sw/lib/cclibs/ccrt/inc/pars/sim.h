//! @file  ccrt/inc/pars/sim.h
//!
//! @brief ccrt SIM parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "reg.h"
#include "ccSim.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_SIM_EXT
#else
#define CCPARS_SIM_EXT extern
#endif

//! Used for SIM ADC_SIGNAL parameter enum

CCPARS_SIM_EXT struct CC_pars_enum enum_adc_signal[]
#ifdef GLOBALS
= {
    { ADC_SIGNAL_CAL_OFFSET   , "CAL_OFFSET"   },
    { ADC_SIGNAL_CAL_POSITIVE , "CAL_POSITIVE" },
    { ADC_SIGNAL_CAL_NEGATIVE , "CAL_NEGATIVE" },
    { ADC_SIGNAL_DAC          , "DAC"          },
    { ADC_SIGNAL_I_DCCT_A     , "I_DCCT_A"     },
    { ADC_SIGNAL_I_DCCT_B     , "I_DCCT_B"     },
    { ADC_SIGNAL_V_MEAS       , "V_MEAS"       },
    { ADC_SIGNAL_V_AC         , "V_AC"         },
    { ADC_SIGNAL_B_PROBE_A    , "B_PROBE_A"    },
    { ADC_SIGNAL_I_CAPA       , "I_CAPA"       },
    { ADC_SIGNAL_NONE         , "NONE"         },
    { 0                       , NULL           },
}
#endif
;

//! Used for SIM DCCT_CAL parameter enum

CCPARS_SIM_EXT struct CC_pars_enum enum_dcct_cal[]
#ifdef GLOBALS
= {
    { SIG_CAL_OFFSET   , "CAL_OFFSET"   },
    { SIG_CAL_POSITIVE , "CAL_POSITIVE" },
    { SIG_CAL_NEGATIVE , "CAL_NEGATIVE" },
    { SIG_CAL_FAULT    , "FAULT"        },
    { SIG_CAL_NONE     , "OFF"          },
    { 0                , NULL           },
}
#endif
;

//! SIM parameters structure

struct CC_pars_sim
{
    enum CC_sim_adc_signal    adc_signal[SIG_NUM_ADCS];          //!< Connects the multiplexed signals with ADCs
    enum SIG_cal_level        dcct_cal;                          //!< DCCT calibration reference
    cc_float                  invalid_probability;               //!< Probability of invalid measurements (0-1)
    cc_float                  vs_quantization;                   //!< Simulated voltage source quantization
    cc_float                  vs_perturbation;                   //!< Simulated voltage source perturbation
    cc_float                  vs_perturbation_rate;              //!< Simulated voltage source perturbation rate
    cc_float                  vs_tone_period_iters;              //!< Simulated voltage source tone period in iterations
    cc_float                  meas_tone_period_iters;            //!< Simulated measurement tone period in iterations
    cc_float                  tone_pp[TONE_NUM_SOURCES];         //!< Simulated tone peak-to-peak amplitudes
    cc_float                  noise_vs;                          //!< Simulated voltage source output noise amplitude (standard deviation)
    cc_float                  noise_dcct_a;                      //!< Simulated DCCT A noise amplitude (standard deviation)
    cc_float                  noise_dcct_b;                      //!< Simulated DCCT B noise amplitude (standard deviation)
    cc_float                  noise_v_probe;                     //!< Simulated V PROBE noise amplitude (standard deviation)
    cc_float                  noise_b_probe;                     //!< Simulated B PROBE noise amplitude (standard deviation)
    cc_float                  noise_i_probe;                     //!< Simulated I PROBE noise amplitude (standard deviation)
    cc_float                  noise_adcs;                        //!< Simulated ADCs noise amplitude (standard deviation)
    uint32_t                  adc_stuck_limit;                   //!< ADC stuck limit
    cc_float                  int_cal_ref_temp;                  //!< Simulated internal calibration reference temperature
    cc_float                  internal_temp;                     //!< Simulated internal temperature control
    cc_float                  dcct_a_temp;                       //!< Simulated DCCT_A temperature control
    cc_float                  dcct_b_temp;                       //!< Simulated DCCT_B temperature control
    cc_float                  raw_i_mag_sat;                     //!< Simulated magnet saturation current
    struct CC_meas_signal     i_mag_sat;                         //!< Simulated filtered magnet saturation current
    struct CC_meas_signal     vfeedfwd;                         //!< Simulated feed forward voltage signal - driven by vs_perturbation
};

CCPARS_SIM_EXT struct CC_pars_sim ccpars_sim
#ifdef GLOBALS
= {
        .adc_signal             = { ADC_SIGNAL_I_DCCT_A,
                                    ADC_SIGNAL_I_DCCT_B,
                                    ADC_SIGNAL_V_MEAS,
                                    ADC_SIGNAL_B_PROBE_A, },
        .dcct_cal               = SIG_CAL_NONE,
        .vs_tone_period_iters   = 20.0,
        .meas_tone_period_iters = 20.0,
        .int_cal_ref_temp       = 40.0,
        .internal_temp          = 30.0,
        .dcct_a_temp            = 31.0,
        .dcct_b_temp            = 32.0,
        .i_mag_sat              = { .is_valid = true },
}
#endif // GLOBALS
;

//! SIM parameters description structure
//!
//! Note: There is no SIM V_PROBE_TONE_PP parameter - it's always zero

CCPARS_SIM_EXT struct CC_pars sim_pars[]
#ifdef GLOBALS
= {// name                      type          max_n_els     *enum                   *value                                n_els            flags
    { "ADC_SIGNAL"            , PAR_ENUM,     SIG_NUM_ADCS, enum_adc_signal, { .u =  ccpars_sim.adc_signal               }, SIG_NUM_ADCS,  PARS_RW|PARS_CFG|PARS_FIXLEN },
    { "DCCT_CAL"              , PAR_ENUM,     1,            enum_dcct_cal,   { .u = &ccpars_sim.dcct_cal                 }, 1,             PARS_RW                      },
    { "INVALID_PROBABILITY"   , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.invalid_probability      }, 1,             PARS_RW                      },
    { "VS_QUANTIZATION"       , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.vs_quantization          }, 1,             PARS_RW|PARS_CFG             },
    { "VS_PERTURBATION"       , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.vs_perturbation          }, 1,             PARS_RW                      },
    { "VS_PERTURBATION_RATE"  , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.vs_perturbation_rate     }, 1,             PARS_RW                      },
    { "VS_TONE_PERIOD_ITERS"  , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.vs_tone_period_iters     }, 1,             PARS_RW                      },
    { "VS_TONE_PP"            , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.tone_pp[TONE_VS]         }, 1,             PARS_RW|PARS_CFG             },
    { "VFEEDFWD_IS_VALID"     , PAR_BOOL,     1,            NULL,            { .b = &ccpars_sim.vfeedfwd.is_valid        }, 1,             PARS_RW                      },
    { "MEAS_TONE_PERIOD_ITERS", PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.meas_tone_period_iters   }, 1,             PARS_RW|PARS_CFG             },
    { "DCCT_A_TONE_PP"        , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.tone_pp[TONE_DCCT_A]     }, 1,             PARS_RW|PARS_CFG             },
    { "DCCT_B_TONE_PP"        , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.tone_pp[TONE_DCCT_B]     }, 1,             PARS_RW|PARS_CFG             },
    { "B_PROBE_TONE_PP"       , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.tone_pp[TONE_B_PROBE]    }, 1,             PARS_RW|PARS_CFG             },
    { "I_PROBE_TONE_PP"       , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.tone_pp[TONE_I_PROBE]    }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_VS"              , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_vs                 }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_DCCT_A"          , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_dcct_a             }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_DCCT_B"          , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_dcct_b             }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_V_PROBE"         , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_v_probe            }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_B_PROBE"         , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_b_probe            }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_I_PROBE"         , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_i_probe            }, 1,             PARS_RW|PARS_CFG             },
    { "NOISE_ADCS"            , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.noise_adcs               }, 1,             PARS_RW|PARS_CFG             },
    { "ADC_STUCK_LIMIT"       , PAR_UNSIGNED, 1,            NULL,            { .u = &ccpars_sim.adc_stuck_limit          }, 1,             PARS_RW                      },
    { "INT_CAL_REF_TEMP"      , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.int_cal_ref_temp         }, 1,             PARS_RW                      },
    { "INTERNAL_TEMP"         , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.internal_temp            }, 1,             PARS_RW                      },
    { "DCCT_A_TEMP"           , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.dcct_a_temp              }, 1,             PARS_RW                      },
    { "DCCT_B_TEMP"           , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.dcct_b_temp              }, 1,             PARS_RW                      },
    { "I_MAG_SAT"             , PAR_FLOAT,    1,            NULL,            { .f = &ccpars_sim.raw_i_mag_sat            }, 1,             PARS_RW                      },
    { "I_DCCT_A"              , PAR_FLOAT,    1,            NULL,            { .f = &ccsim.circuit.i_dcct_a              }, 1,             PARS_RO                      },
    { "I_DCCT_B"              , PAR_FLOAT,    1,            NULL,            { .f = &ccsim.circuit.i_dcct_b              }, 1,             PARS_RO                      },
    { "B_PROBE"               , PAR_FLOAT,    1,            NULL,            { .f = &ccsim.circuit.b_probe               }, 1,             PARS_RO                      },
    { "V_PROBE"               , PAR_FLOAT,    1,            NULL,            { .f = &ccsim.circuit.v_probe               }, 1,             PARS_RO                      },
    { "I_PROBE"               , PAR_FLOAT,    1,            NULL,            { .f = &ccsim.circuit.i_probe               }, 1,             PARS_RO                      },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_adc_signal) == ADC_SIGNAL_NUM_SOURCES + 1, mismatch_between_enum_adc_signal_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_dcct_cal)   == SIG_CAL_NONE           + 1, mismatch_between_enum_dcct_cal_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

