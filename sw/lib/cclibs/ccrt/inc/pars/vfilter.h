//! @file  ccrt/inc/pars/vfilter.h
//!
//! @brief ccrt voltage source filter vfilter parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_VFILTER_EXT
#else
#define CCPARS_VFILTER_EXT extern
#endif

//! Used for VFILTER I_CAPA_SOURCE and V_MEAS_SOURCE parameter value decoding

CCPARS_VFILTER_EXT struct CC_pars_enum enum_sig_source_mode[]
#ifdef GLOBALS
= {
    { REG_MEASUREMENT , "MEASUREMENT" },
    { REG_SIMULATION  , "SIMULATION"  },
    { 0               , NULL          },
}
#endif
;

//! VFILTER parameters description structure

CCPARS_VFILTER_EXT struct CC_pars vfilter_pars[]
#ifdef GLOBALS
= {// name              type   max_n_els *enum                          *value                                                 n_els flags
    { "V_MEAS_SOURCE" , PAR_ENUM,  1,     enum_sig_source_mode,  { .u = regMgrParAppPointer(&reg_pars, VFILTER_V_MEAS_SOURCE) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "I_CAPA_SOURCE" , PAR_ENUM,  1,     enum_sig_source_mode,  { .u = regMgrParAppPointer(&reg_pars, VFILTER_I_CAPA_SOURCE) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "I_CAPA_FILTER" , PAR_FLOAT, 2,     NULL,                  { .f = regMgrParAppValue  (&reg_pars, VFILTER_I_CAPA_FILTER) }, 2,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_U"           , PAR_FLOAT, 1,     NULL,                  { .f = regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_U      ) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_I"           , PAR_FLOAT, 1,     NULL,                  { .f = regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_I      ) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { "K_D"           , PAR_FLOAT, 1,     NULL,                  { .f = regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_D      ) }, 1,  PARS_RW|PARS_REG|PARS_CFG },
    { NULL }
}
#endif
;

// EOF
