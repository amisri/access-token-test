//! @file  ccrt/inc/pars/meas.h
//!
//! @brief ccrt MEAS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "reg.h"
#include "cal.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_MEAS_EXT
#else
#define CCPARS_MEAS_EXT extern
#endif

//! Used for MEAS DCCT_SELECT parameter value decoding

CCPARS_MEAS_EXT struct CC_pars_enum enum_dcct_select[]
#ifdef GLOBALS
= {
    { SIG_A    , "A"   , 0                 },
    { SIG_B    , "B"   , 0                 },
    { SIG_AB   , "AB"  , 0                 },
    { SIG_NONE , "NONE", CC_ENUM_READ_ONLY },
    { 0        , NULL  , 0                 },
}
#endif
;

//! MEAS parameters description structure

CCPARS_MEAS_EXT struct CC_pars meas_pars[]
#ifdef GLOBALS
= {// name                      type          max_n_els            *enum                         *value                                                                      n_els                   flags                                  size_of_struct
    { "DCCT_SELECT"           , PAR_ENUM,     1,                   enum_dcct_select,     { .u =  sigVarPointer(&sig_struct, select, i_meas, SELECT_SELECTOR               ) }, 1,                    PARS_RW|PARS_CFG                                                     },
    { "DCCT_ACT_SELECT"       , PAR_ENUM,     1,                   enum_dcct_select,     { .u =  sigVarPointer(&sig_struct, select, i_meas, SELECT_ACTUAL_SELECTOR        ) }, 1,                    PARS_RO                                                              },
    { "B_DELAY_ITERS"         , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrParAppPointer(&reg_pars, MEAS_B_DELAY_ITERS                         ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG                                            },
    { "I_DELAY_ITERS"         , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrParAppPointer(&reg_pars, MEAS_I_DELAY_ITERS                         ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG                                            },
    { "V_DELAY_ITERS"         , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrParAppPointer(&reg_pars, MEAS_V_DELAY_ITERS                         ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG                                            },
    { "B_FIR_LENGTHS"         , PAR_UNSIGNED, 2,                   NULL,                 { .u = regMgrParAppValue(  &reg_pars, MEAS_B_FIR_LENGTHS                         ) }, 2,                    PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN                                },
    { "I_FIR_LENGTHS"         , PAR_UNSIGNED, 2,                   NULL,                 { .u = regMgrParAppValue(  &reg_pars, MEAS_I_FIR_LENGTHS                         ) }, 2,                    PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN                                },
    { "ADC_RAW"               , PAR_SIGNED,   SIG_NUM_ADCS,        NULL,                 { .i = sigVarPointer( &sig_struct, adc       , adc_a , ADC_RAW                   ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_UNFILTERED"        , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_UNFILTERED       ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_FILTERED"          , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_FILTERED         ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_AVE1_MEAN"         , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_AVE1_MEAN        ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_AVE1_PP"           , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_AVE1_PP          ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_AVE2_MEAN"         , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_AVE2_MEAN        ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "ADC_AVE2_PP"           , PAR_FLOAT,    SIG_NUM_ADCS,        NULL,                 { .f = sigVarPointer( &sig_struct, adc       , adc_a , ADC_MEAS_AVE2_PP          ) }, SIG_NUM_ADCS,         PARS_RO,                               sizeof(struct SIG_adc)        },
    { "TR_UNFILTERED"         , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_UNFILTERED) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "TR_FILTERED"           , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_FILTERED  ) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "TR_AVE1_MEAN"          , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_AVE1_MEAN ) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "TR_AVE1_PP"            , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_AVE1_PP   ) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "TR_AVE2_MEAN"          , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_AVE2_MEAN ) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "TR_AVE2_PP"            , PAR_FLOAT,    SIG_NUM_TRANSDUCERS, NULL,                 { .f = sigVarPointer( &sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_AVE2_PP   ) }, SIG_NUM_TRANSDUCERS,  PARS_RO,                               sizeof(struct SIG_transducer) },
    { "SEL_UNFILTERED"        , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_UNFILTERED    ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_FILTERED"          , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_FILTERED      ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_AVE1_MEAN"         , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_AVE1_MEAN     ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_AVE1_PP"           , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_AVE1_PP       ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_AVE2_MEAN"         , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_AVE2_MEAN     ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_AVE2_PP"           , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_MEAS_AVE2_PP       ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "SEL_ABS_DIFF"          , PAR_FLOAT,    SIG_NUM_SELECTS,     NULL,                 { .f = sigVarPointer( &sig_struct, select    , i_meas, SELECT_ABS_DIFF           ) }, SIG_NUM_SELECTS,      PARS_RO,                               sizeof(struct SIG_select)     },
    { "B_FILTERED"            , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrVarPointer( &reg_mgr, MEAS_B_FILTERED                               ) }, 1,                    PARS_RO                                                              },
    { "I_FILTERED"            , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrVarPointer( &reg_mgr, MEAS_I_FILTERED                               ) }, 1,                    PARS_RO                                                              },
    { "V_UNFILTERED"          , PAR_FLOAT,    1,                   NULL,                 { .f = regMgrVarPointer( &reg_mgr, MEAS_V_UNFILTERED                             ) }, 1,                    PARS_RO                                                              },
    { "B_MEAS_LOW"            , PAR_BOOL,     1,                   NULL,                 { .b = regMgrVarPointer( &reg_mgr, FLAG_B_MEAS_LOW                               ) }, 1,                    PARS_RO|PARS_STAT                                                    },
    { "I_MEAS_LOW"            , PAR_BOOL,     1,                   NULL,                 { .b = regMgrVarPointer( &reg_mgr, FLAG_I_MEAS_LOW                               ) }, 1,                    PARS_RO|PARS_STAT                                                    },
    { "B_MEAS_ZERO"           , PAR_BOOL,     1,                   NULL,                 { .b = regMgrVarPointer( &reg_mgr, FLAG_B_MEAS_ZERO                              ) }, 1,                    PARS_RO|PARS_STAT                                                    },
    { "I_MEAS_ZERO"           , PAR_BOOL,     1,                   NULL,                 { .b = regMgrVarPointer( &reg_mgr, FLAG_I_MEAS_ZERO                              ) }, 1,                    PARS_RO|PARS_STAT                                                    },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_dcct_select) == SIG_NUM_SELECTORS + 1, mismatch_between_enum_dcct_select_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF

