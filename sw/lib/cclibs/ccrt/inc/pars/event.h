//! @file  ccrt/inc/pars/event.h
//!
//! @brief ccrt EVENT parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "logmgr.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_EVENT_EXT
#else
#define CCPARS_EVENT_EXT extern
#endif

//! EVENT parameters structure

struct CC_pars_event
{
    struct REF_event    active_event;    //!< Active event - copied by ccLogEvent from ccrun.cycle.active_event
};

//! Instance of EVENT parameters structure

CCPARS_EVENT_EXT struct CC_pars_event ccpars_event;

//! EVENT parameters description structure

CCPARS_EVENT_EXT struct CC_pars event_pars[]
#ifdef GLOBALS
= {// name             type            max_n_els   *enum          *value                         n_els flags
    { "ABS_TIME"     , PAR_ABS_TIME,   1,          NULL,   { .n = &ref_mgr.fg.event.abs_time    }, 1,  PARS_RO },
    { "SUB_SEL"      , PAR_UNSIGNED,   1,          NULL,   { .u = &ref_mgr.fg.event.sub_sel     }, 1,  PARS_RO },
    { "CYC_SEL"      , PAR_UNSIGNED,   1,          NULL,   { .u = &ref_mgr.fg.event.cyc_sel     }, 1,  PARS_RO },
    { "REF_CYC_SEL"  , PAR_UNSIGNED,   1,          NULL,   { .u = &ref_mgr.fg.event.ref_cyc_sel }, 1,  PARS_RO },
    { NULL }
}
#endif
;

// EOF
