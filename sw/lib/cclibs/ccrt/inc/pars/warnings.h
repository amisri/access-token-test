//! @file  ccrt/inc/pars/warnings.h
//!
//! @brief ccrt WARNINGS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_WARNINGS_EXT
#else
#define CCPARS_WARNINGS_EXT extern
#endif

//! Used for decoding WARNINGS REF parameter value

CCPARS_WARNINGS_EXT struct CC_pars_enum enum_ref_warnings[]
#ifdef GLOBALS
= {
    { REF_REF_CLIPPED_BIT         , "REF_CLIPPED"         },
    { REF_REF_RATE_CLIPPED_BIT    , "REF_RATE_CLIPPED"    },
    { REF_B_REF_CLIPPED_BIT       , "B_REF_CLIPPED"       },
    { REF_B_REF_RATE_CLIPPED_BIT  , "B_REF_RATE_CLIPPED"  },
    { REF_B_ERR_WARNING_BIT       , "B_ERR_WARNING"       },
    { REF_B_RST_WARNING_BIT       , "B_RST_WARNING"       },
    { REF_I_REF_CLIPPED_BIT       , "I_REF_CLIPPED"       },
    { REF_I_REF_RATE_CLIPPED_BIT  , "I_REF_RATE_CLIPPED"  },
    { REF_I_ERR_WARNING_BIT       , "I_ERR_WARNING"       },
    { REF_I_RST_WARNING_BIT       , "I_RST_WARNING"       },
    { REF_V_REF_CLIPPED_BIT       , "V_REF_CLIPPED"       },
    { REF_V_REF_RATE_CLIPPED_BIT  , "V_REF_RATE_CLIPPED"  },
    { REF_V_ERR_WARNING_BIT       , "V_ERR_WARNING"       },
    { REF_DYN_ECO_ARM_WARNING_BIT , "DYN_ECO_ARM_WARNING" },
    { REF_DYN_ECO_RUN_WARNING_BIT , "DYN_ECO_RUN_WARNING" },
    { REF_CYCLING_WARNING_BIT     , "REF_CYCLING_WARNING" },
    { REF_I_RMS_WARNING_BIT       , "I_RMS_WARNING"       },
    { REF_I_RMS_LOAD_WARNING_BIT  , "I_RMS_LOAD_WARNING"  },
    { REF_ILC_WARNING_BIT         , "ILC_WARNING"         },
    { 0                           , NULL                  },
}
#endif // GLOBALS
;

//! Libsig warnings enums

CCPARS_WARNINGS_EXT struct CC_pars_enum enum_sig_adc_warnings[]
#ifdef GLOBALS
= {
    { SIG_ADC_TEMP_WARN_BIT   , "TEMP"   },
    { SIG_ADC_CAL_WARN_BIT    , "CAL"    },
    { SIG_ADC_RSVD_0_WARN_BIT , "RSVD_0" },
    { SIG_ADC_RSVD_1_WARN_BIT , "RSVD_1" },
    { 0                       , NULL     },
}
#endif // GLOBALS
;

CCPARS_WARNINGS_EXT struct CC_pars_enum enum_sig_transducer_warnings[]
#ifdef GLOBALS
= {
    { SIG_TRANSDUCER_ADC_WARN_BIT   , "ADC"   },
    { SIG_TRANSDUCER_TEMP_WARN_BIT  , "TEMP"  },
    { SIG_TRANSDUCER_CAL_WARN_BIT   , "CAL"   },
    { SIG_TRANSDUCER_APP_0_WARN_BIT , "APP_0" },
    { SIG_TRANSDUCER_APP_1_WARN_BIT , "APP_1" },
    { SIG_TRANSDUCER_APP_2_WARN_BIT , "APP_2" },
    { SIG_TRANSDUCER_APP_3_WARN_BIT , "APP_3" },
    { SIG_TRANSDUCER_APP_4_WARN_BIT , "APP_4" },
    { SIG_TRANSDUCER_APP_5_WARN_BIT , "APP_5" },
    { 0                             , NULL    },
}
#endif // GLOBALS
;

CCPARS_WARNINGS_EXT struct CC_pars_enum enum_sig_select_warnings[]
#ifdef GLOBALS
= {
    { SIG_SELECT_SIG_INVALID_WARN_BIT, "SIG_INVALID" },
    { SIG_SELECT_SIG_WARN_WARN_BIT   , "SIG_WARN"    },
    { SIG_SELECT_DIFF_WARN_BIT       , "DIFF"        },
    { 0                              , NULL          },
}
#endif // GLOBALS
;

//! WARNINGS parameters description structure

CCPARS_WARNINGS_EXT struct CC_pars warnings_pars[]
#ifdef GLOBALS
= {// name         type     max_n_els *enum                                *value                                                                n_el  flags
    { "REF"      , PAR_BITMASK, 1,    enum_ref_warnings,            { .u = refMgrVarPointer(&ref_mgr, REF_WARNINGS                            ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_A"    , PAR_BITMASK, 1,    enum_sig_adc_warnings,        { .u = sigVarPointer(&sig_struct, adc       , adc_a  , ADC_WARNINGS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_B"    , PAR_BITMASK, 1,    enum_sig_adc_warnings,        { .u = sigVarPointer(&sig_struct, adc       , adc_b  , ADC_WARNINGS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_C"    , PAR_BITMASK, 1,    enum_sig_adc_warnings,        { .u = sigVarPointer(&sig_struct, adc       , adc_c  , ADC_WARNINGS       ) }, 1,  PARS_RO|PARS_STAT },
    { "ADC_D"    , PAR_BITMASK, 1,    enum_sig_adc_warnings,        { .u = sigVarPointer(&sig_struct, adc       , adc_d  , ADC_WARNINGS       ) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_A"   , PAR_BITMASK, 1,    enum_sig_transducer_warnings, { .u = sigVarPointer(&sig_struct, transducer, dcct_a , TRANSDUCER_WARNINGS) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_B"   , PAR_BITMASK, 1,    enum_sig_transducer_warnings, { .u = sigVarPointer(&sig_struct, transducer, dcct_b , TRANSDUCER_WARNINGS) }, 1,  PARS_RO|PARS_STAT },
    { "V_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_warnings, { .u = sigVarPointer(&sig_struct, transducer, v_probe, TRANSDUCER_WARNINGS) }, 1,  PARS_RO|PARS_STAT },
    { "B_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_warnings, { .u = sigVarPointer(&sig_struct, transducer, b_probe, TRANSDUCER_WARNINGS) }, 1,  PARS_RO|PARS_STAT },
    { "I_PROBE"  , PAR_BITMASK, 1,    enum_sig_transducer_warnings, { .u = sigVarPointer(&sig_struct, transducer, i_probe, TRANSDUCER_WARNINGS) }, 1,  PARS_RO|PARS_STAT },
    { "DCCT_SEL" , PAR_BITMASK, 1,    enum_sig_select_warnings,     { .u = sigVarPointer(&sig_struct, select    , i_meas , SELECT_WARNINGS    ) }, 1,  PARS_RO|PARS_STAT },
    { NULL }
}
#endif // GLOBALS
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_ref_warnings)            == REF_NUM_WARNING_BITS         + 1, mismatch_between_enum_ref_warnings_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_adc_warnings)        == SIG_ADC_NUM_WARNING_BITS     + 1, mismatch_between_enum_sig_adc_warnings_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_transducer_warnings) == SIG_TRANSDUCER_NUM_WARN_BITS + 1, mismatch_between_enum_sig_transducer_warnings_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_sig_select_warnings)     == SIG_SELECT_NUM_WARN_BITS     + 1, mismatch_between_enum_sig_select_warnings_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
