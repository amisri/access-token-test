//! @file  ccrt/inc/pars/trans.h
//!
//! @brief ccrt TRANS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_TRANS_EXT
#else
#define CCPARS_TRANS_EXT extern
#endif

//! Instance of REF parameters structure

CCPARS_TRANS_EXT struct REF_transaction_pars ccpars_trans[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS];

// Global parameter indexes

enum CC_trans_pars_index_enum
{
    REF_PAR_LAST_FG_PAR_INDEX ,
};

//! REF parameters description structure

CCPARS_TRANS_EXT struct CC_pars trans_pars[]
#ifdef GLOBALS
= {// name                 type          max_n_els *enum    *value                                        n_els  flags                              size_of_struct
    { "LAST_FG_PAR_INDEX", PAR_UNSIGNED, 1,        NULL,  { .u =  ccpars_trans[0][0].last_fg_par_index }, 1,     PARS_RO|PARS_SUB_SEL|PARS_CYC_SEL, sizeof(struct REF_transaction_pars) },
    { NULL }
}
#endif // GLOBALS
;

// EOF
