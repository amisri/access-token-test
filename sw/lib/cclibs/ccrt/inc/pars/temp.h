//! @file  ccrt/inc/pars/temp.h
//!
//! @brief ccrt TEMP parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"
#include "reg.h"
#include "cal.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_TEMP_EXT
#else
#define CCPARS_TEMP_EXT extern
#endif

//! TEMP parameters description structure

CCPARS_TEMP_EXT struct CC_pars temp_pars[]
#ifdef GLOBALS
= {// name                 type   max_n_els *enum   *value                                                                          n_els flags              struct_size
    { "INTERNAL_TEMP"    , PAR_FLOAT, 1,     NULL, { .f = sigVarPointer(&sig_struct, temp_filter, internal, TEMP_FILTER_TEMP_C  )  }, 1,  PARS_RO,           sizeof(struct SIG_temp_filter) },
    { "INTERNAL_INVALID" , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, internal, TEMP_FILTER_INVALID )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { "INTERNAL_WARNING" , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, internal, TEMP_FILTER_WARNING )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { "DCCT_A_TEMP"      , PAR_FLOAT, 1,     NULL, { .f = sigVarPointer(&sig_struct, temp_filter, dcct_a  , TEMP_FILTER_TEMP_C  )  }, 1,  PARS_RO,           sizeof(struct SIG_temp_filter) },
    { "DCCT_A_INVALID"   , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, dcct_a  , TEMP_FILTER_INVALID )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { "DCCT_A_WARNING"   , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, dcct_a  , TEMP_FILTER_WARNING )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { "DCCT_B_TEMP"      , PAR_FLOAT, 1,     NULL, { .f = sigVarPointer(&sig_struct, temp_filter, dcct_b  , TEMP_FILTER_TEMP_C  )  }, 1,  PARS_RO,           sizeof(struct SIG_temp_filter) },
    { "DCCT_B_INVALID"   , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, dcct_b  , TEMP_FILTER_INVALID )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { "DCCT_B_WARNING"   , PAR_BOOL,  1,     NULL, { .b = sigVarPointer(&sig_struct, temp_filter, dcct_b  , TEMP_FILTER_WARNING )  }, 1,  PARS_RO|PARS_STAT, sizeof(struct SIG_temp_filter) },
    { NULL }
}
#endif
;

// EOF

