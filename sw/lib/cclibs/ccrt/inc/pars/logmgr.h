//! @file  ccrt/inc/pars/logmgr.h
//!
//! @brief ccrt LOG manager parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// From log_mpx_enums.h, which is included in logmpx.h

extern struct CC_pars_enum enum_logs[];

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_LOGMGR_EXT
#else
#define CCPARS_LOGMGR_EXT extern
#endif

CCPARS_LOGMGR_EXT struct LOG_mgr log_mgr;

//! LOG manager parameters description structure

CCPARS_LOGMGR_EXT struct CC_pars logmgr_pars[]
#ifdef GLOBALS
= {// name                       type           max_n_els      *enum             *value                             n_els          flags
    { "NUM_LOGS"               , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.num_logs               }, 1           ,  PARS_RO },
    { "READ_DELAY_MS"          , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.read_delay_ms          }, 1           ,  PARS_RW },
    { "READ_RATE"              , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.read_rate              }, 1           ,  PARS_RW },
    { "CYCLIC_LOGS_MASK"       , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.cyclic_logs_mask       }, 1           ,  PARS_RO },
    { "ANALOG_LOGS_MASK"       , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.analog_logs_mask       }, 1           ,  PARS_RO },
    { "POSTMORTEM_LOGS_MASK"   , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.postmortem_logs_mask   }, 1           ,  PARS_RO },
    { "CONTINUOUS_LOGS_MASK"   , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.continuous_logs_mask   }, 1           ,  PARS_RO },
    { "FREEZABLE_LOGS_MASK"    , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.freezable_logs_mask    }, 1           ,  PARS_RO },
    { "RUNNING_LOGS_MASK"      , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.running_logs_mask      }, 1           ,  PARS_RO },
    { "DISABLED_LOGS_MASK"     , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.disabled_logs_mask     }, 1           ,  PARS_RW },
    { "FREEZE_MASK"            , PAR_BITMASK,   1,             enum_logs, { .u = &log_mgr.freeze_mask            }, 1           ,  PARS_RO },
    { "PERIOD"                 , PAR_REL_TIME,  LOG_NUM_LOGS,  NULL,      { .n =  log_mgr.period                 }, LOG_NUM_LOGS,  PARS_RO },
    { "POSTMORTEM_LOGS_FROZEN" , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.postmortem_logs_frozen }, 1           ,  PARS_RO },
    { "CUR_CYC_SEL"            , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.cur_cyc_sel            }, 1           ,  PARS_RO },
    { "PREV_CYC_SEL"           , PAR_UNSIGNED,  1,             NULL,      { .u = &log_mgr.prev_cyc_sel           }, 1           ,  PARS_RO },

    { NULL }
}
#endif // GLOBALS
;

// EOF
