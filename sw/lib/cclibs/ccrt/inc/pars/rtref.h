//! @file  ccrt/inc/pars/rtref.h
//!
//! @brief ccrt RTREF parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_RTREF_EXT
#else
#define CCPARS_RTREF_EXT extern
#endif

//! RTREF parameters structure

struct CC_pars_rtref
{
    uint32_t    sampling_iters;    //!< Real-time test sine wave sampling period in iterations
    cc_float    offset;            //!< Real-time test sine wave offset
    cc_float    amplitude;         //!< Real-time test sine wave amplitude
    cc_float    frequency;         //!< Real-time test sine wave frequency
};

//! Instance of RTREF parameters structure

CCPARS_RTREF_EXT struct CC_pars_rtref ccpars_rtref
#ifdef GLOBALS
= {
    .sampling_iters = 20,     // RTREF SAMPLING_ITERS
    .offset         = 0.0,    // RTREF OFFSET
    .amplitude      = 0.0,    // RTREF AMPLITUDE
    .frequency      = 1.0,    // RTREF FREQUENCY
}
#endif
;

//! RTREF parameters description structure

CCPARS_RTREF_EXT struct CC_pars rtref_pars[]
#ifdef GLOBALS
= {// name               type      max_n_els   *enum          *value                        n_els flags
    { "SAMPLING_ITERS" , PAR_UNSIGNED, 1,      NULL,   { .u = &ccpars_rtref.sampling_iters }, 1,  PARS_RW|PARS_CFG },
    { "OFFSET"         , PAR_FLOAT,    1,      NULL,   { .f = &ccpars_rtref.offset         }, 1,  PARS_RW|PARS_CFG },
    { "AMPLITUDE"      , PAR_FLOAT,    1,      NULL,   { .f = &ccpars_rtref.amplitude      }, 1,  PARS_RW|PARS_CFG },
    { "FREQUENCY"      , PAR_FLOAT,    1,      NULL,   { .f = &ccpars_rtref.frequency      }, 1,  PARS_RW|PARS_CFG },
    { NULL }
}
#endif
;

// EOF

