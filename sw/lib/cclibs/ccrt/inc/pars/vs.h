//! @file  ccrt/inc/pars/vs.h
//!
//! @brief ccrt VS parameters
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccCmds.h"

// GLOBALS is defined in source file where global variables should be defined

#ifdef GLOBALS
#define CCPARS_VS_EXT
#else
#define CCPARS_VS_EXT extern
#endif

//! Used for VS ACTUATION parameter value decoding

CCPARS_VS_EXT struct CC_pars_enum enum_reg_actuation[]
#ifdef GLOBALS
= {
    { REG_VOLTAGE_REF , "VOLTAGE_REF" , CC_ENUM_GREEN  },
    { REG_FIRING_REF  , "FIRING_REF"  , CC_ENUM_CYAN   },
    { 0               , NULL                           }
}
#endif
;

//! Used for MODE FIRING parameter value decoding

CCPARS_MODE_EXT struct CC_pars_enum enum_firing_mode[]
#ifdef GLOBALS
= {
    { REG_CASSEL      , "CASSEL"      },
    { REG_MIXED       , "MIXED"       },
    { REG_RECONSTITUE , "RECONSTITUE" },
    { REG_ASAD        , "ASAD"        },
    { 0               , NULL          },
}
#endif
;

//! Used for accessing VS parameters

enum CC_vs_pars_index_enum
{
    VS_ACTUATION             ,
    VS_ACT_DELAY_ITERS       ,
    VS_GAIN                  ,
    VS_NUM_VSOURCES          ,
    VS_OFFSET                ,
    DAC_CLAMP             ,
    VS_FIRING_MODE           ,
    VS_FIRING_DELAY          ,
    VS_FIRING_LUT            ,
    VS_SIM_BANDWIDTH         ,
    VS_SIM_Z                 ,
    VS_SIM_TAU_ZERO          ,
    VS_SIM_NUM               ,
    VS_SIM_DEN               ,
    VS_SIM2_K                ,
    VS_SIM2_H                ,
    VS_SIM2_MA               ,
    VS_SIM2_MB               ,
    VS_NUM_PARS
};

// Power converter parameters description structure

CCPARS_VS_EXT struct CC_pars vs_pars[]
#ifdef GLOBALS
= {// name                type       max_n_els              *enum                      *value                                               n_els                   flags
    { "ACTUATION"       , PAR_ENUM,     1,                   enum_reg_actuation, { .u = regMgrParAppPointer(&reg_pars, VS_ACTUATION      ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "ACT_DELAY_ITERS" , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_ACT_DELAY_ITERS) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "NUM_VSOURCES"    , PAR_UNSIGNED, 1,                   NULL,               { .u = regMgrParAppPointer(&reg_pars, VS_NUM_VSOURCES   ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "GAIN"            , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_GAIN           ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "OFFSET"          , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_OFFSET         ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "DAC_CLAMP"       , PAR_FLOAT,    1,                   NULL,               { .f = refMgrParPointer(   &ref_mgr,  DAC_CLAMP         ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "FIRING_MODE"     , PAR_ENUM,     1,                   enum_firing_mode,   { .u = regMgrParAppPointer(&reg_pars, VS_FIRING_MODE    ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "FIRING_DELAY"    , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_FIRING_DELAY   ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "FIRING_LUT"      , PAR_FLOAT,    REG_FIRING_LUT_LEN,  NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_FIRING_LUT     ) }, REG_FIRING_LUT_LEN,   PARS_RW|PARS_CFG|PARS_REG             },
    { "SIM_BANDWIDTH"   , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_SIM_BANDWIDTH  ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "SIM_Z"           , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_SIM_Z          ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "SIM_TAU_ZERO"    , PAR_FLOAT,    1,                   NULL,               { .f = regMgrParAppPointer(&reg_pars, VS_SIM_TAU_ZERO   ) }, 1,                    PARS_RW|PARS_CFG|PARS_REG             },
    { "SIM_NUM"         , PAR_FLOAT,    REG_SIM_NUM_DEN_LEN, NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM_NUM        ) }, REG_SIM_NUM_DEN_LEN,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM_DEN"         , PAR_FLOAT,    REG_SIM_NUM_DEN_LEN, NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM_DEN        ) }, REG_SIM_NUM_DEN_LEN,  PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM2_K"          , PAR_FLOAT,    REG_SIM2_K_LEN,      NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM2_K         ) }, REG_SIM2_K_LEN,       PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM2_H"          , PAR_FLOAT,    REG_SIM2_H_LEN,      NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM2_H         ) }, REG_SIM2_H_LEN,       PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM2_MA"         , PAR_FLOAT,    REG_SIM2_MA_LEN,     NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM2_MA        ) }, REG_SIM2_MA_LEN,      PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { "SIM2_MA"         , PAR_FLOAT,    REG_SIM2_MB_LEN,     NULL,               { .f = regMgrParAppValue(  &reg_pars, VS_SIM2_MB        ) }, REG_SIM2_MB_LEN,      PARS_RW|PARS_CFG|PARS_REG|PARS_FIXLEN },
    { NULL }
}
#endif
;

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_reg_actuation) == REG_NUM_ACTUATIONS   + 1, mismatch_between_enum_reg_actuation_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(enum_firing_mode)   == REG_NUM_FIRING_MODES + 1, mismatch_between_enum_firing_mode_array_size_and_number_of_enum_elements);
CC_STATIC_ASSERT(CC_ARRAY_LEN(vs_pars)            == VS_NUM_PARS          + 1, mismatch_between_vs_pars_array_size_and_number_of_enum_elements);

#endif // GLOBALS

// EOF
