//! @file  ccrt/inc/ccLog.h
//!
//! @brief ccrt header file for ccLog.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccParse.h"
#include "ccFile.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCLOG_EXT
#else
#define CCLOG_EXT extern
#endif

//! Local buffer for log data

CCLOG_EXT uint32_t      local_log_buffer[LOG_MAX_LOG_LEN + LOG_HEADER_LEN];

// Debug signal - this variable is logged in the ACQ log as DEBUG_SIG

CCLOG_EXT cc_float      log_debug_sig;

// Event log structure

struct CC_log_event_line
{
    struct CC_us_time           iter_time;
    struct CC_pars            * par;
    uint32_t                    buf[REG_NUM_LOADS];
};

// Function declarations

void     ccLogPrintTestTap         (bool, char const *, char const *, char const * fmt, ...);
void     ccLogGetOutput            (struct LOG_read_control *);
void     ccLogWriteSigNames        (FILE * f);
void     ccLogTestReadControl      (struct LOG_log const *, struct LOG_read_control *);
void     ccLogStoreSigNamesAndUnits(uint32_t log_index, uint32_t sig_index, char * sig_name, char * units, bool direct_store);
void     ccLogSetDirName           (char dir_name[CC_PATH_LEN]);
void     ccLogWakeThread           (enum LOG_read_action, char *);
uint32_t ccLogEnableDisable        (char * arg, bool);
void   * ccLogSigsWriteThread      (void * args);

// EOF
