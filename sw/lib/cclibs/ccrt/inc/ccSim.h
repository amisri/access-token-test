//! @file  ccrt/inc/ccSim.h
//!
//! @brief ccrt header file for ccSim.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <sys/time.h>
#include <pthread.h>

#include "pars/cal.h"
#include "pars/global.h"
#include "pars/sim.h"

// DAC calibration constants

#define CAL_DAC_RAW         367000
#define CAL_DAC_VOLTS       8.0
#define CAL_DAC_OFFSET      0.1

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCSIM_EXT
#else
#define CCSIM_EXT extern
#endif

// For easier ADC calibration, CAL_ZERO, CAL_POS and CAL_NEG should equal enum entries from libsig


enum CC_sim_adc_signal
{
    ADC_SIGNAL_CAL_OFFSET   = SIG_CAL_OFFSET,
    ADC_SIGNAL_CAL_POSITIVE = SIG_CAL_POSITIVE,
    ADC_SIGNAL_CAL_NEGATIVE = SIG_CAL_NEGATIVE,
    ADC_SIGNAL_DAC,
    ADC_SIGNAL_I_DCCT_A,
    ADC_SIGNAL_I_DCCT_B,
    ADC_SIGNAL_V_MEAS,
    ADC_SIGNAL_B_PROBE_A,
    ADC_SIGNAL_I_CAPA,
    ADC_SIGNAL_V_AC,
    ADC_SIGNAL_NONE,
    ADC_SIGNAL_NUM_SOURCES,
};

// The DCCTs MUST be the first transducers defined in sig.csv for ccrt


enum CC_sim_dcct_select
{
    DCCT_A = SIG_TRANSDUCER_DCCT_A,
    DCCT_B = SIG_TRANSDUCER_DCCT_B,
    DCCT_N_DCCTS,
};

enum CC_sim_tones
{
    TONE_VS,
    TONE_DCCT_A,
    TONE_DCCT_B,
    TONE_V_PROBE,
    TONE_B_PROBE,
    TONE_I_PROBE,
    TONE_NUM_SOURCES,
};

// Simulation related structures

struct CC_sim_sc_event
{
    uint32_t                    idx;                            //!< Cycle index in supercycle
    uint32_t                    basic_periods;                  //!< Cycle length in basic periods
    struct CC_ns_time           event_time;                     //!< Event time (s, ns)
    struct REF_event            event;                          //!< Event
};

struct CC_sim_error
{
    cc_float const              err_ppm[SIG_NUM_CALS];          //!< Simulated transducer/adc errors
    cc_float const * const      tc;                             //!< Simulated transducer/adc temp coefficient
    cc_float const * const      dtc;                            //!< Simulated transducer/adc 2nd order temp coefficient
};

struct CC_sim_vars
{
    bool                        cal_dac;                        //!< Indicates that DAC calibration is running

    enum SIG_cal_level          cal_dac_level;                  //!< Current DAC calibration level
    const uint32_t              cal_dac_raw[SIG_NUM_CALS];      //!< raw_dac values for all three DAC calibration levels

    struct CC_sim_pc_state
    {
        cc_float                timer;                          //!< Converter on/off timer (s)
    } pcstate;

    struct CC_sim_supercycle
    {
        struct CC_ns_time       prev_iter_time;                 //!< Iter time from previous iteration (s, ns)
        struct CC_ns_time       warning_time;                   //!< Next start function event warning time (s, ns)
        struct CC_ns_time       next_cycle_time;                //!< Next start of cycle time (s, ns)
        struct CC_us_time       next_event_us_time;             //!< Next start function event time (s, us)

        struct CC_sim_sc_event  next;                           //!< Next cycle event
        struct CC_sim_sc_event  active;                         //!< Active cycle event (set after warning time)
        struct CC_sim_sc_event  running;                        //!< Running cycle event (set after event time)
        pthread_mutex_t         event_mutex;                    //!< Mutex to protect the active event
    } supercycle;

    struct CC_sim_circuit
    {
        cc_float                i_dcct_a;                       //!< Simulated circuit current being measured by DCCT A
        cc_float                i_dcct_b;                       //!< Simulated circuit current being measured by DCCT B
        cc_float                v_probe;                        //!< Simulated voltage being measured by the V_PROBE
        cc_float                b_probe;                        //!< Simulated magnet field being measured by the Hall Probe
        cc_float                i_probe;                        //!< Simulated current measurement from output filter
        cc_float                v_dac;                          //!< Simulated DAC voltage
        cc_float                v_adc[ADC_SIGNAL_NUM_SOURCES];  //!< Simulated voltages available to the ADCs
        int32_t                 raw_adc  [SIG_NUM_ADCS];        //!< Simulated raw ADC values
        cc_float                raw_adc_f[SIG_NUM_ADCS];        //!< Simulated raw ADC values as floats
    } circuit;

    //! DCCT simulation structures
    //!
    //! DCCT tc and dtc temp coefficients are shared with libsig, while the errors are
    //! independent, to allow the DCCT calibration to discover their values.


    struct CC_sim_error         dcct_err[DCCT_N_DCCTS];         //!< Simulated DCCT electronics errors

    //! ADC simulation structures
    //!
    //! ADC tc and dtc temp coefficients are shared with libsig, while the nominal gain and errors are
    //! independent, to allow the ADC calibration to discover their values.

    struct CC_sim_adc
    {
        uint32_t                nominal_gain;                   //!< Simulated ADC nominal gain
        struct CC_sim_error     err;                            //!< Simulated ADC errors
    } adc[SIG_NUM_ADCS];

    //! DAC simulation structure
    //!
    //! For a 20 bit DAC with v_nominal = 11V the range (raw) is [-524288, 524287]
    //! With v_offset = 0.1 and calibration raw values 367000 (~70% of the max value)
    //! and measured voltages +-8V we have:
    //! - positive gain = 367000 raw / (8V - 0.1V) = 46455.67 raw/V
    //! - negative gain = 367000 raw / (8V + 0.1V) = 45308.64 raw/V

    struct CC_sim_dac
    {
        cc_float const          v_offset;                       //!< DAC voltage for zero calibration
        cc_float const          pos_raw_per_volt;               //!< Gain (raw/V) for positive voltages
        cc_float const          neg_raw_per_volt;               //!< Gain (raw/V) for negative voltages
        uint32_t const          resolution;                     //!< DAC resolution
        cc_float const          v_nominal;                      //!< Nominal voltage
    } dac;

    //! Simulation structures - this contains all the structures for vs, load and vs_and_load simulations

    struct REG_mgr_sim_vars     sim_vars;                       //!< Simulation variables
    struct REG_sim2_pars        vs_and_load_pars;               //!< VS and load pars structure with null Kalman filter

    cc_float                    null_k[REG_SIM2_K_LEN];         //!< Null Kalman filter for vs_and_load_pars

    //! Decoupling simulation variables

    struct REG_deco_shared volatile deco;                       //!< Shared deco structure
};

// Global simulation variables

CCSIM_EXT struct CC_sim_vars ccsim
#ifdef GLOBALS
= {
        .supercycle = { .event_mutex = PTHREAD_MUTEX_INITIALIZER,
                        .next        = { .idx = CC_MAX_SUPER_CYC_LEN } },

        .dac.v_offset                   = CAL_DAC_OFFSET,
        .dac.pos_raw_per_volt           = CAL_DAC_RAW / (CAL_DAC_VOLTS - CAL_DAC_OFFSET ),
        .dac.neg_raw_per_volt           = CAL_DAC_RAW / (CAL_DAC_VOLTS + CAL_DAC_OFFSET ),
        .dac.resolution                 = 20,
        .dac.v_nominal                  = 11.0,

        .dcct_err[DCCT_A].err_ppm       = { 10.0, -30.0, 50.0 },
        .dcct_err[DCCT_A].tc            = sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_CAL_TC),
        .dcct_err[DCCT_A].dtc           = sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_CAL_DTC),

        .dcct_err[DCCT_B].err_ppm       = { -20.0, 40.0, -60.0 },
        .dcct_err[DCCT_B].tc            = sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_CAL_TC),
        .dcct_err[DCCT_B].dtc           = sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_CAL_DTC),

        .adc[SIG_ADC_ADC_A].err.err_ppm = {  2.0, -12.0,  22.0 },
        .adc[SIG_ADC_ADC_A].err.tc      = sigVarValue(&sig_struct, adc, adc_a, ADC_CAL_TC),
        .adc[SIG_ADC_ADC_A].err.dtc     = sigVarValue(&sig_struct, adc, adc_a, ADC_CAL_DTC),

        .adc[SIG_ADC_ADC_B].err.err_ppm = { -4.0,  14.0, -24.0 },
        .adc[SIG_ADC_ADC_B].err.tc      = sigVarValue(&sig_struct, adc, adc_b, ADC_CAL_TC),
        .adc[SIG_ADC_ADC_B].err.dtc     = sigVarValue(&sig_struct, adc, adc_b, ADC_CAL_DTC),

        .adc[SIG_ADC_ADC_C].err.err_ppm = {  6.0, -16.0,  26.0 },
        .adc[SIG_ADC_ADC_C].err.tc      = sigVarValue(&sig_struct, adc, adc_c, ADC_CAL_TC),
        .adc[SIG_ADC_ADC_C].err.dtc     = sigVarValue(&sig_struct, adc, adc_c, ADC_CAL_DTC),

        .adc[SIG_ADC_ADC_D].err.err_ppm = { -8.0,  18.0, -28.0 },
        .adc[SIG_ADC_ADC_D].err.tc      = sigVarValue(&sig_struct, adc, adc_d, ADC_CAL_TC),
        .adc[SIG_ADC_ADC_D].err.dtc     = sigVarValue(&sig_struct, adc, adc_d, ADC_CAL_DTC),

        .vs_and_load_pars.k             = ccsim.null_k,
        .vs_and_load_pars.h             = regMgrParPointer(&reg_mgr,VS_SIM2_H),
        .vs_and_load_pars.ma            = regMgrParPointer(&reg_mgr,VS_SIM2_MA),
        .vs_and_load_pars.mb            = regMgrParPointer(&reg_mgr,VS_SIM2_MB),

        .cal_dac_raw                    = { 0, CAL_DAC_RAW, -CAL_DAC_RAW },
}
#endif
;

// Global Function prototypes

void     ccSimInit                  (void);
void     ccSimPcStateRT             (void);
void     ccSimCircuitRT             (bool set_vfeedfwd_flag);
void     ccSimSuperCycle            (void);
void     ccSimPrepareNextCycle      (void);

// Inline functions

static inline bool ccSimIsPcOn(void)
{
    return refMgrParValue(&ref_mgr, PC_STATE) == REF_PC_ON;
}

static inline void ccSimGetRunningCycle(struct CC_sim_sc_event * const running)
{
    pthread_mutex_lock(&ccsim.supercycle.event_mutex);

    *running = ccsim.supercycle.running;

    pthread_mutex_unlock(&ccsim.supercycle.event_mutex);
}

// EOF
