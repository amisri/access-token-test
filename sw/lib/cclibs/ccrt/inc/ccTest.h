//! @file  ccrt/inc/ccTest.h
//!
//! @brief ccrt header file for ccTest.c
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of ccrt.
//!
//! ccrt is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "ccPars.h"

// GLOBALS should be defined in the source file where global variables should be defined

#ifdef GLOBALS
#define CCTEST_EXT
#else
#define CCTEST_EXT extern
#endif

// Constants

#define CC_MAX_TEST_PARS_LEN    1024

// Typedef for pointer to test function

union   CC_value_p;
struct CC_test;

typedef uint32_t ccTestType (const struct CC_test * const test, bool * const result);   //!< Type for test assertion function

// Test structures

struct CC_test
{
    const struct CC_pars  * par;
    char                  * args_string;
    char                    condition;
    union CC_value_p        value_p;
    uint32_t                num_elements;
    uint32_t                cyc_sel;
    uint32_t                idx_from;
    uint32_t                idx_to;
};

// Test function array - IMPORTANT - this must match order of enum CC_pars_type in ccPars.h

uint32_t ccTestEnum             (const struct CC_test * const test, bool * const result);
uint32_t ccTestBitmask          (const struct CC_test * const test, bool * const result);
uint32_t ccTestBool             (const struct CC_test * const test, bool * const result);
uint32_t ccTestInteger          (const struct CC_test * const test, bool * const result);
uint32_t ccTestShort            (const struct CC_test * const test, bool * const result);
uint32_t ccTestFloat            (const struct CC_test * const test, bool * const result);
uint32_t ccTestRelTime          (const struct CC_test * const test, bool * const result);
uint32_t ccTestPoint            (const struct CC_test * const test, bool * const result);
uint32_t ccTestString           (const struct CC_test * const test, bool * const result);

CCTEST_EXT ccTestType * ccpars_test_func[]
#ifdef GLOBALS
= {
    ccTestInteger  ,             // PAR_SIGNED
    ccTestInteger  ,             // PAR_UNSIGNED
    NULL           ,             // PAR_UINTPTR
    ccTestShort    ,             // PAR_USHORT
    ccTestInteger  ,             // PAR_HEX
    ccTestFloat    ,             // PAR_FLOAT
    ccTestFloat    ,             // PAR_DOUBLE
    ccTestString   ,             // PAR_STRING
    ccTestEnum     ,             // PAR_ENUM
    ccTestBool     ,             // PAR_BOOL
    ccTestRelTime  ,             // PAR_REL_TIME
    NULL           ,             // PAR_ABS_TIME
    NULL           ,             // PAR_US_TIME
    ccTestBitmask  ,             // PAR_BITMASK
    ccTestPoint    ,             // PAR_POINT
    NULL           ,             // PAR_POINTER
    ccTestString   ,             // PAR_FGIDX
}
#endif
;

// Inline function definitions

static inline bool ccTestIsValidCondition(char condition)
{
    return condition == '=' || condition == '!' || condition == '~' || condition == '%' || condition == '<' || condition == '>';
}

// Function declarations

void     ccTestPrintTapHeader   (const bool test_result);
uint32_t ccTestPrepare          (struct CC_test * const test, const struct CC_pars * const par, char * remaining_line);
uint32_t ccTestPar              (const struct CC_pars * const par, char * remaining_line);
uint32_t ccTestPrepareWait      (char *remaining_line, bool * const test_result);

// Static assertions

#ifdef GLOBALS

CC_STATIC_ASSERT(CC_ARRAY_LEN(ccpars_test_func) == PAR_NUM_TYPES, size_of_ccpars_test_func_doesnt_match_PAR_NUM_TYPES);

#endif // GLOBALS

// EOF
