/*---------------------------------------------------------------------------------------------------------*\
  File:     libterm.h                                                                   Copyright CERN 2017

  License:  This file is part of libterm.

            libterm is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Contents: ANSI terminal support library header file
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LIBTERM_H
#define LIBTERM_H

#include <stdio.h>
#include <stdint.h>

// Useful ANSI terminal sequences

#define TERM_ESC                        0x1B
#define TERM_BELL                       '\a'                    // Bell character
#define TERM_BACKSPACE                  '\b'                    // Backspace character
#define TERM_RESET                      "\r      \33c"          // Spaces provide delay before reset
#define TERM_INIT                       "\33[2J   \33[?7h \r"   // Clear screen, enable line wrap
#define TERM_SAVE_POS                   "\33" "7"               // Save cursor position
#define TERM_RESTORE_POS                "\33" "8"               // Restore cursor position
#define TERM_REPORT_POS                 "\33[6n"                // Report cursor position
#define TERM_CLR_SCREEN                 "\33[2J"                // Clear complete screen
#define TERM_CLR_UP                     "\33[1J"                // Clear from current line to top of screen
#define TERM_CLR_DOWN                   "\33[J"                 // Clear from current line to bottom of screen
#define TERM_CLR_LINE                   "\33[2K"                // Clear complete line
#define TERM_CLR_LEFT                   "\33[1K"                // Clear from cursor to beginning of line
#define TERM_CLR_RIGHT                  "\33[K"                 // Clear from cursor to end of line
#define TERM_HOME                       "\33[H"                 // Move cursor to top left of screen
#define TERM_SET_SCROLL_LINES           "\33[H\33[%u;%ur"       // Define lines that scroll (as integers)
#define TERM_HIDE_CURSOR                "\33[?25l"              // Hide the cursor
#define TERM_SHOW_CURSOR                "\33[?25h"              // Show the cursor

// Control Sequence introducer (CSI) sequences (see http://en.wikipedia.org/wiki/ANSI_escape_code)
// For example: printf(TERM_CSI "%u;%u" TERM_GOTO, line, column);

#define TERM_CSI                        "\33["                  // Control sequence introducer
#define TERM_GOTO                       "H"                     // Goto: line;column
#define TERM_UP                         "A"                     // Cursor up: lines
#define TERM_DOWN                       "B"                     // Cursor down: lines
#define TERM_LEFT                       "D"                     // Cursor left: columns
#define TERM_RIGHT                      "C"                     // Cursor right: columns

// Option that can be used between TERM_CSI and TERM_SGR (use TERM_NORMAL to return to normal)
// For example: printf(TERM_CSI TERM_FG_MAGENTA TERM_BG_GREEN TERM_SGR "%s" TERM_NORMAL, string);

#define TERM_SGR                        "m"                     // Select Graphic Rendition
#define TERM_NORMAL                     TERM_CSI TERM_SGR       // Return to normal formatting
#define TERM_BOLD                       ";1"
#define TERM_UNDERLINE                  ";4"
#define TERM_REVERSE                    ";7"
#define TERM_FG_BLACK                   ";30"
#define TERM_FG_RED                     ";31"
#define TERM_FG_GREEN                   ";32"
#define TERM_FG_YELLOW                  ";33"
#define TERM_FG_BLUE                    ";34"
#define TERM_FG_MAGENTA                 ";35"
#define TERM_FG_CYAN                    ";36"
#define TERM_FG_WHITE                   ";37"
#define TERM_BG_BLACK                   ";40"
#define TERM_BG_RED                     ";41"
#define TERM_BG_GREEN                   ";42"
#define TERM_BG_YELLOW                  ";43"
#define TERM_BG_BLUE                    ";44"
#define TERM_BG_MAGENTA                 ";45"
#define TERM_BG_CYAN                    ";46"
#define TERM_BG_WHITE                   ";47"

// Structure constants

#define NUM_LINE_BUFS           32                              //!< Must be a power of 2 !!!
#define LINE_BUFS_MASK          (NUM_LINE_BUFS-1)               //!< Mask for circular history buffer
#define MAX_LINE_LEN            254                             //!< Maximum line length - the current maximum
                                                                //!< is given by term_s.max_line_len
// Libterm variables

typedef void TermLine(char *line, uint16_t line_len);           //!< Type terminal level functions
typedef void TermCallback(char keyboard_ch);                    //!< Callback function for dealing with certain characters

struct TERM_vars
{
    uint16_t     max_line_len;                                  //!< Mex line len is number_of_columns-1
    uint16_t     level;                                         //!< Keyboard character processing level
    uint16_t     line_idx;                                      //!< Line editor buffer index
    uint16_t     line_end;                                      //!< Line editor end of buffer contents index
    uint16_t     cur_line;                                      //!< Current line in line_bufs[] history
    uint16_t     recall_line;                                   //!< Recall line in line_bufs[] history
    uint16_t     cpr_value;                                     //!< Reported cursor position value
    uint16_t     line_len[NUM_LINE_BUFS];                       //!< History line lengths
    char         line_buf[MAX_LINE_LEN+2];                      //!< Edit line buffer
    char         line_bufs[NUM_LINE_BUFS][MAX_LINE_LEN+2];      //!< Line buffers for history
    FILE        *file;                                          //!< Stream for writing to the terminal
    TermLine    *term_line;                                     //!< User callback for command lines
    char         prompt;                                        //!< Prompt character
};

// Function declarations

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Initialize the library.
 *
 * @param[in,out]   term_vars           Pointer to terminal variables structure.
 * @param[in]       file                Pointer to FILE stream to send characters to the terminal.
 * @param[in]       term_line           Pointer to callback function to receive completed lines.
 * @param[in]       callback            Pointer to callback function to be called for CTRL-T, CTRL-L and ESC-ESC.
 * @param[in]       prompt              Character to use as a prompt.
 */
 void TermLibInit(struct TERM_vars * term_vars, FILE *file, TermLine *term_line, TermCallback *callback, char prompt);



/*!
 * Reset the terminal and set the number of columns
 *
 * @param[in,out]   term_vars           Pointer to terminal variables structure.
 * @param[in]       number_of_columns   Number of columns supported by the terminal
 */
void TermInit(struct TERM_vars * term_vars, uint16_t number_of_columns);



/*!
 * Process a new keyboard character
 *
 * @param[in,out]   term_vars           Pointer to terminal variables structure.
 * @param[in]       keyboard_ch         Character from the keyboard.
 *
 * @returns      Terminal character processing level
 */
uint16_t TermChar(struct TERM_vars * term_vars, char keyboard_ch);

#ifdef __cplusplus
}
#endif

#endif

// EOF
