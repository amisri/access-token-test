/*!
 * @file  term.c
 * @brief Converter Control ANSI terminal functions.
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2017. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libterm.
 *
 * libterm is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @mainpage CERN Converter Control Library: Terminal
 *
 * Libterm supports a command line interface on an ANSI terminal.
 */

#include <string.h>

#include "libterm.h"

// Typedefs

typedef uint16_t TermLevel(struct TERM_vars * term_vars, char keyboard_ch);



// Internal variable definitions

static TermCallback * term_callback;



// Static function prototypes

static  uint16_t TermLevel0               (struct TERM_vars * term_vars, char keyboard_ch);
static  uint16_t TermLevel1               (struct TERM_vars * term_vars, char keyboard_ch);
static  uint16_t TermLevel2               (struct TERM_vars * term_vars, char keyboard_ch);
static  uint16_t TermLevel3               (struct TERM_vars * term_vars, char keyboard_ch);
static  uint16_t TermLevel4               (struct TERM_vars * term_vars, char keyboard_ch);
static  void     TermInsertChar           (struct TERM_vars * term_vars, char keyboard_ch);
static  void     TermNewline              (struct TERM_vars * term_vars);
static  void     TermCursorLeft           (struct TERM_vars * term_vars);
static  void     TermCursorRight          (struct TERM_vars * term_vars);
static  void     TermStartOfLine          (struct TERM_vars * term_vars);
static  void     TermEndOfLine            (struct TERM_vars * term_vars);
static  void     TermDeleteLine           (struct TERM_vars * term_vars);
static  void     TermDeleteLeft           (struct TERM_vars * term_vars);
static  void     TermDeleteRight          (struct TERM_vars * term_vars);
static  void     TermShiftRemains         (struct TERM_vars * term_vars);
static  void     TermRepeatLine           (struct TERM_vars * term_vars);
static  void     TermPreviousLine         (struct TERM_vars * term_vars);
static  void     TermNextLine             (struct TERM_vars * term_vars);
static  void     TermRecallLine           (struct TERM_vars * term_vars, uint16_t recall_line);




void TermLibInit(struct TERM_vars * const term_vars,
                 FILE             *       file,
                 TermLine         *       term_line,
                 TermCallback     *       callback,
                 char               const prompt)
{
    term_vars->term_line = term_line;
    term_vars->file      = file;
    term_vars->prompt    = prompt;
    term_callback        = callback;
}



void TermInit(struct TERM_vars * const term_vars, uint16_t number_of_columns)
{
    uint16_t    i;

    // Reset terminal

    fputs(TERM_RESET, term_vars->file);

    // Send spaces to allow time for terminal to reset

    for ( i = 0 ; i < 32; i++ )
    {
        fputc(' ', term_vars->file);
    }

    // Clear screen, enable line wrap, ring bell

    fputs(TERM_INIT, term_vars->file);

    // Reset line buffer variables

    term_vars->line_idx     = 0;
    term_vars->line_end     = 0;
    term_vars->level        = 0;

    // Set max line length to number of terminal columns minus 2 (for the prompt character and cursor)

    term_vars->max_line_len = number_of_columns - 2;
}



uint16_t TermChar(struct TERM_vars * const term_vars, char const keyboard_ch)
{
    static TermLevel *term_func[] =                       // Keyboard character functions
    {
        TermLevel0,                                       // Level 0 : Normal characters
        TermLevel1,                                       // Level 1 : ESC pressed
        TermLevel2,                                       // Level 2 : Cursor keys or all function keys
        TermLevel3,                                       // Level 3 : All function keys except PF1-PF4
        TermLevel4,                                       // Level 4 : PF1-PF4 keys
    };

    // Process keyboard character according to level

    term_vars->level = term_func[term_vars->level](term_vars, keyboard_ch);

    // Flush the terminal stream

    fflush(term_vars->file);

    // Return the level

    return term_vars->level;
}



static uint16_t TermLevel0(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 *   Level 0: The keyboard character is analyzed directly.  If the ESC code (0x1B) is received, the level
 *   changes to 1, otherwise the character is processed and the state remains 0.
 */
{
    switch(keyboard_ch)                                            //-[CHARACTER]--ACTION-----------------------------
    {
//case 'A':  fputs(TERM_REPORT_POS, term_vars->file); break;

    case 0x1B:  return 1;                                          // [ESC]        Start escape sequence processing

    case 0x24:                                                     // [$]          Ignore (server protocol delimiter)

    case 0x7B:                                                     // [{]          Ignore (server protocol delimiter)

    case 0x7D:                                          break;     // [}]          Ignore (server protocol delimiter)

    case 0x7F:                                                     // [Delete]     Delete left

    case 0x08:  TermDeleteLeft (term_vars);             break;     // [Backspace]  Delete left

    case 0x04:  TermDeleteRight(term_vars);             break;     // [CTRL-D]     Delete right

    case 0x21:                                                     // [!]          Delete line

    case 0x15:  TermDeleteLine (term_vars);             break;     // [CTRL-U]     Delete line

    case 0x01:  TermStartOfLine(term_vars);             break;     // [CTRL-A]     Move to start of line

    case 0x05:  TermEndOfLine  (term_vars);             break;     // [CTRL-E]     Move to end of line

    case 0x12:  TermRepeatLine (term_vars);             break;     // [CTRL-R]     Repeat line

    case 0x0C:  term_callback  (keyboard_ch);           break;     // [CTRL-L]     Processed externally

    case 0x14:  term_callback  (keyboard_ch);           break;     // [CTRL-T]     Processed externally

    case 0x3B:                                                     // [;]          Semicolon - run command

    case 0x0D:  TermNewline    (term_vars);             break;     // [Return]     Carriage return - return line

    case 0x0A:                                          break;     // [LF]         Linefeed - ignore

    default:    TermInsertChar (term_vars,keyboard_ch); break;     // [others]     Insert character
    }

    return 0;                                               // Continue with level 0
}


static uint16_t TermLevel1(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 * Level 1: The previous character was [ESC].
 */
{
    switch(keyboard_ch)                                     // Switch according to next code
    {

    case 0x1B:  term_callback(keyboard_ch); break;          // [ESC]            Processed externally

    case 0x5B:                              return 2;       // '[' [Cursor/Fxx] Change state to analyse

    case 0x4F:                              return 4;       // 'O' [PF1-4]      Change state to ignore
    }

    return 0;                                       // All other characters - return to level 0
}



static uint16_t TermLevel2(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 * Level 2: The key pressed was either a cursor key or a function key.  Cursors keys have the code
 * sequence "ESC[A" to "ESC[D", while function keys have the code sequence "ESC[???~" where ??? is a
 * variable number of alternative codes.
 */
{
    switch(keyboard_ch)                              // Switch according to next code
    {
    case 0x41:  TermPreviousLine(term_vars);      return 0;       // [Up]         Previous history line

    case 0x42:  TermNextLine(term_vars);          return 0;       // [Down]       Next history line

    case 0x43:  TermCursorRight(term_vars);       return 0;       // [Right]      Move cursor right

    case 0x44:  TermCursorLeft(term_vars);        return 0;       // [Left]       Move cursor left

//    case 0x3B:
//
//    case 0x5B:
//
//    default:
    }

    return 3;                                       // Function key - change to level 3
}



static uint16_t TermLevel3(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 * Level 3:  Original Key was a Function with sequence terminated by 0x7E (~).  However, the function will
 * also accept a new [ESC] to allow an escape route in case of corrupted reception.
 */
{
    switch(keyboard_ch)                             // Switch according to next code
    {
    case 0x1B:                          return 1;       // [ESC]        Escape to level 1

    case 0x7E:                          return 0;       // [~]          Sequence complete - return to state 0
    }

    return 3;                                       // No change to level
}



static uint16_t TermLevel4(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 * Level 4:  The original key was PF1-4 and one more character must be ignored.
 */
{
    return 0;                                   // Return to level 0
}



static void TermInsertChar(struct TERM_vars * const term_vars, char keyboard_ch)
/*
 * This function is called from TermLevel0() if a standard character has been received.  It will try to enter
 * the character into the current line under the current cursor position.  The rest of the line will be
 * moved to the right to make space for the new character.  If the line is full, the bell will ring.
 */
{
    uint16_t      i;

    // If character is printable

    if(keyboard_ch >= 0x20)
    {
        if(term_vars->line_end >= term_vars->max_line_len)     // If line buffer is full then ring the bell
        {
            fputc(TERM_BELL, term_vars->file);
        }
        else
        {
            // Shift the rest of the line by one character and insert the new keyboard character

            for(i = term_vars->line_end++ ; i > term_vars->line_idx ; i--)
            {
                term_vars->line_buf[i] = term_vars->line_buf[i-1];
            }

            term_vars->line_buf[term_vars->line_idx] = keyboard_ch;

            // Print the new character and the rest of the line to the terminal

            for(i = term_vars->line_idx++ ; i < term_vars->line_end ; i++)
            {
                fputc(term_vars->line_buf[i], term_vars->file);
            }

            // If cursor is now offset from true position then shift cursor left again

            if((i = term_vars->line_end - term_vars->line_idx) > 0)
            {
                fprintf(term_vars->file,TERM_CSI "%hu" TERM_LEFT, i);
            }
        }
    }
}



static void TermNewline(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if Enter or Return have been received.  It skips leading
 * white space and enters the line into the command buffer and the line history.  It then calls the users
 * callback function to process the buffer.
 */
{
    char *      line_p;
    uint16_t    line_len;

    // Nul terminate the line and skip over leading spaces

    line_len = term_vars->line_end;
    term_vars->line_buf[term_vars->line_end] = '\0';

    for(line_p=term_vars->line_buf ; *line_p==' ' ; line_p++)
    {
        line_len--;
    }

    // If new line is different then store it in the line history buffer

    if(line_len > 0 &&
       (term_vars->line_end != term_vars->line_len[(term_vars->cur_line-1) & LINE_BUFS_MASK] ||
        memcmp(term_vars->line_buf, &term_vars->line_bufs[(term_vars->cur_line-1) & LINE_BUFS_MASK], term_vars->line_end)))
    {
        term_vars->line_len[term_vars->cur_line] = term_vars->line_end;
        memcpy(&term_vars->line_bufs[term_vars->cur_line], term_vars->line_buf, term_vars->line_end);
        term_vars->cur_line = (term_vars->cur_line + 1) & LINE_BUFS_MASK;
    }

    // Call user callback function to process line buffer

    term_vars->term_line(line_p, line_len);

    // Reset cursor and end of line indexes

    term_vars->line_idx = 0;
    term_vars->line_end = 0;
    term_vars->recall_line = term_vars->cur_line;

    // Move cursor to start of newline and print the prompt character

    fprintf(term_vars->file, "\n\r%c", term_vars->prompt);
}



static void TermCursorLeft(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel2() if the Cursor left key has been pressed.
 */
{
    // Move cursor left one column or ring bell if at start of line

    if(term_vars->line_idx > 0)
    {
        fputc(TERM_BACKSPACE, term_vars->file);
        term_vars->line_idx--;
    }
    else
    {
        fputc(TERM_BELL, term_vars->file);
    }
}



static void TermCursorRight(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel2() if the Cursor right key has been pressed.
 */
{
    // Move cursor right one column or ring bell if at end of content on the line

    if(term_vars->line_idx < term_vars->line_end)
    {
        fputs(TERM_CSI TERM_RIGHT, term_vars->file);
        term_vars->line_idx++;
    }
    else
    {
        fputc(TERM_BELL, term_vars->file);
    }
}



static void TermStartOfLine(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if the Ctrl-A key has been pressed.
 */
{
    // If cursor is not already at the start of the line then move cursor left to the start of line

    if(term_vars->line_idx > 0)
    {
        fprintf(term_vars->file, TERM_CSI "%hu" TERM_LEFT, term_vars->line_idx);
        term_vars->line_idx = 0;
    }
}



static void TermEndOfLine(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if the Ctrl-E key has been pressed.
 */
{
    uint16_t      shift_right;

    // If cursor is not already at the end of the contents of the line then move cursor to end of contents

    if((shift_right = term_vars->line_end - term_vars->line_idx) > 0)
    {
        fprintf(term_vars->file, TERM_CSI "%hu" TERM_RIGHT, shift_right);
        term_vars->line_idx = term_vars->line_end;
    }
}



static void TermDeleteLine(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if the Ctrl-U key has been pressed.
 */
{
    // Clear line and print prompt

    fprintf(term_vars->file, TERM_CLR_LINE "\r%c", term_vars->prompt);

    term_vars->line_idx = 0;
    term_vars->line_end = 0;
}



static void TermDeleteLeft(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if the backspace or delete keys have been pressed.
 */
{
    // Delete character to left of cursor or ring bell if at start of line

    if(term_vars->line_idx > 0)
    {
        fputc(TERM_BACKSPACE, term_vars->file);
        term_vars->line_idx--;

        // Shift remains of the line one character to the left

        TermShiftRemains(term_vars);
    }
    else
    {
        fputc(TERM_BELL, term_vars->file);
    }
}


static void TermDeleteRight(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel0() if the Ctrl-D key has been pressed.
 */
{
    // Delete to right of cursor or ring bell if at end of content on the line

    if(term_vars->line_idx < term_vars->line_end)
    {
        // Shift remains of the line one character to the left

        TermShiftRemains(term_vars);
    }
    else
    {
        fputc(TERM_BELL, term_vars->file);
    }
}



static void TermShiftRemains(struct TERM_vars * const term_vars)
/*
 * This function is called from TermDeleteLeft() and TermDeleteRight() to shift the remains of the line
 * one character to the left.
 */
{
    uint16_t      i;

    term_vars->line_end--;                           // Adjust end of line index

    // For the remainder of the line shift characters in buffer and display it

    for(i = term_vars->line_idx ; i < term_vars->line_end ; i++)
    {
        fputc( (term_vars->line_buf[i] = term_vars->line_buf[i+1]), term_vars->file);
    }

    // Clear last character and move cursor the required number of columns to the left

    fprintf(term_vars->file, " " TERM_CSI "%hu" TERM_LEFT, (uint16_t)(1 + term_vars->line_end - term_vars->line_idx));
}



static void TermRepeatLine(struct TERM_vars * const term_vars)
/*
  This function is called from TermLevel0() if Ctrl-R is entered.  It recovers the previous line from
  the line history and enters it.
 */
{
    uint16_t      recall_line;

    // Calc index of previous line in history and return if empty

    recall_line = (term_vars->cur_line - 1) & LINE_BUFS_MASK;

    if(!term_vars->line_len[recall_line])
    {
        return;
    }

    // Recall and process line and adjust current line index to avoid a repeat entry in the history

    term_vars->cur_line = recall_line;
    TermRecallLine(term_vars, recall_line);
    TermNewline(term_vars);
}



static void TermPreviousLine(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel2() if cursor up is pressed.  It will recover the previous line from
 * the line history buffers.
 */
{
    uint16_t      recall_line;

    // If editing current line then save it in the history

    if(term_vars->recall_line == term_vars->cur_line)
    {
        term_vars->line_len[term_vars->cur_line] = term_vars->line_end;
        memcpy(&term_vars->line_bufs[term_vars->cur_line], term_vars->line_buf, term_vars->line_end);
    }

    // Adjust line index to previous line and recall the line if it is not empty

    recall_line = (term_vars->recall_line - 1) & LINE_BUFS_MASK;

    if(recall_line == term_vars->cur_line ||
       !term_vars->line_len[recall_line])
    {
        fputc(TERM_BELL, term_vars->file);
    }
    else
    {
        TermRecallLine(term_vars, recall_line);
    }
}



static void TermNextLine(struct TERM_vars * const term_vars)
/*
 * This function is called from TermLevel2() if cursor down is pressed.  It will recover the next line from
 * the line history buffers.
 */
{
    // Recall next line unless already at the most recent line in the history

    if(term_vars->recall_line == term_vars->cur_line)
    {
        fputc(TERM_BELL, term_vars->file);
    }
    else
    {
        TermRecallLine(term_vars, (term_vars->recall_line + 1) & LINE_BUFS_MASK);
    }
}



static void TermRecallLine(struct TERM_vars * const term_vars, uint16_t recall_line)
/*
 * This function is called from TermRepeatLine(), TermPreviousLine() or TermNextLine() when a history line
 * needs to be recalled.
 */
{
    // Save recalled line index and recover line length and data
    // If window is now narrower then truncate the recovered line

    term_vars->recall_line = recall_line;
    term_vars->line_idx =
    term_vars->line_end = (term_vars->line_len[recall_line] < term_vars->max_line_len ?
                       term_vars->line_len[recall_line] : term_vars->max_line_len);

    memcpy(term_vars->line_buf,&term_vars->line_bufs[recall_line],term_vars->line_end);

    // Nul terminate recalled line and display it following the prompt

    term_vars->line_buf[term_vars->line_end] = '\0';

    fprintf(term_vars->file, TERM_CLR_LINE "\r%c%s", term_vars->prompt, term_vars->line_buf);
}

// EOF
