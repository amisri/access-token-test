/*---------------------------------------------------------------------------------------------------------*\
  File:     termtest.c                                                                  Copyright CERN 2017

  License:  This program is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Purpose:  Test program for libterm

  Contact:  cclibs-devs@cern.ch

  Authors:  Quentin.King@cern.ch

  Notes:    As well as testing libterm, this program provides an example of how it can be used with an
            ANSI standard terminal to use part of the terminal window as a shell (with scrolling) and
            part for static information.  This uses the ability to save the cursor position, then
            move and write a field, and then restore the cursor position.  The program also shows how
            to use the terminal control sequences that can set text or background colour, bold and
            underline.  These use the sequence TERM_CSI + formatting codes + TERM_SGR.
\*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ioctl.h>

#include "libterm.h"                    // Include libterm header file

// Constants

#define PROMPT          '>'             // Prompt can only be a single character
#define N_RTD_LINES     3               // Number of real-time display lines
#define RTD_RULER       2               // Ruler line row (from bottom)
#define RTD_REPORT      1               // Report line for new character (from bottom)
#define RTD_RESULT      0               // Resulting input line (from bottom)

// Macros

#define colourTest(C1,C2) printf(TERM_CSI TERM_FG_ ## C1 TERM_BG_ ## C2 TERM_SGR " %7s on %-7s " TERM_NORMAL, #C1, #C2)
#define boldColourTest(C1,C2) printf(TERM_CSI TERM_BOLD TERM_FG_ ## C1 TERM_BG_ ## C2 TERM_SGR " %7s on %-7s " TERM_NORMAL, #C1, #C2)
#define colourTests(C) colourTest(C,BLACK);colourTest(C,WHITE);colourTest(BLACK,C);colourTest(WHITE,C);fputs("\n\r",stdout)
#define boldColourTests(C) boldColourTest(C,BLACK);boldColourTest(C,WHITE);boldColourTest(BLACK,C);boldColourTest(WHITE,C);fputs("\n\r",stdout)

// Global variables

struct TERM_vars term_vars;             // Terminal variables structure
struct termios   stdin_config;          // Original stdin configuration used by ResetStdinConfig()
struct winsize   window;                // Window size is window.ws_row x window.ws_col

/*---------------------------------------------------------------------------------------------------------*/
static void ResetStdinConfig(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    TermInit(&term_vars, 0);                     // Initialize terminal on stdout (clear screen, etc...)

    tcsetattr(STDIN_FILENO, 0, &stdin_config);   // Restore stdin configuration
}
/*---------------------------------------------------------------------------------------------------------*/
static void ResetTerm(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t    i;

    if(window.ws_row < (N_RTD_LINES + 1))
    {
        puts("Too few rows - exiting");
        exit(-1);
    }

    TermInit(&term_vars, window.ws_col);             // Initialize terminal on stdout (clear screen, etc...)

    printf(TERM_SET_SCROLL_LINES, 1, window.ws_row - N_RTD_LINES);  // Set scroll zone

    // Print example formatting and help information in the scrolled zone and end with prompt

    printf(TERM_CSI TERM_BOLD       TERM_UNDERLINE  TERM_SGR "LibTerm Test Program\n\n\r" TERM_NORMAL);

    colourTests(BLACK);
    colourTests(RED);
    colourTests(GREEN);
    colourTests(YELLOW);
    colourTests(BLUE);
    colourTests(MAGENTA);
    colourTests(CYAN);

    boldColourTests(BLACK);
    boldColourTests(RED);
    boldColourTests(GREEN);
    boldColourTests(YELLOW);
    boldColourTests(BLUE);
    boldColourTests(MAGENTA);
    boldColourTests(CYAN);

    printf("\n\rCTRL-A : Start of line             Left arrow:  Move cursor left\n\r");
    printf("CTRL-E : Start of line             Right arrow: Move current right\n\r");
    printf("CTRL-R : Repeat last line          Up arrow:    Previous line from history\n\r");
    printf("CTRL-U : Clear line                Down arrow:  Next line from history\n\r");
    printf("CTRL-D : Delete right              ESC ESC:     Reset terminal\n\r");
    printf("CTRL-C : Quit termtest             Enter:       Process line\n\n\r");

    fputc(PROMPT,stdout);

    // Prepare information zone in non-scrolled lines at the bottom of the terminal

    printf(TERM_SAVE_POS TERM_CSI "%hu;1" TERM_GOTO, (uint16_t)(window.ws_row - RTD_RULER));

    for(i = 0 ; i < window.ws_col ; i++)
    {
        putchar('-');
    }

    printf(TERM_CSI "%hu;1" TERM_GOTO "Keyboard character:                     Line length:" TERM_RESTORE_POS,
           (uint16_t)(window.ws_row - RTD_REPORT));

    fflush(stdout);
}
/*---------------------------------------------------------------------------------------------------------*/
static void SigWinch(int sig)
/*---------------------------------------------------------------------------------------------------------*/
{
    // Read new window size

    ioctl(STDOUT_FILENO, TIOCGWINSZ, &window);

    // Reset the terminal window

    ResetTerm();
}
/*---------------------------------------------------------------------------------------------------------*/
void ProcessLine(char *line, uint16_t line_len)
/*---------------------------------------------------------------------------------------------------------*/
{
    // Report line length in info zone

    printf(TERM_SAVE_POS TERM_CSI "%hu;54" TERM_GOTO TERM_CSI TERM_BOLD TERM_SGR "%3hu",
           (uint16_t)(window.ws_row - RTD_REPORT), line_len);

    // Report line buffer in info zone

    printf(TERM_CSI "%hu;1" TERM_GOTO TERM_CLR_LINE "%s" TERM_RESTORE_POS,
           (uint16_t)(window.ws_row - RTD_RESULT), line);

}
/*---------------------------------------------------------------------------------------------------------*/
void Callback(char keyboard_ch)
/*---------------------------------------------------------------------------------------------------------*/
{
    printf("Implement callback function for this specific character");
}
/*---------------------------------------------------------------------------------------------------------*/
int main(int argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    int            keyboard_ch;
    uint16_t       term_level;
    struct termios stdin_config_raw;

    // Catch SIGWINCH

    signal(SIGWINCH, SigWinch);

    // Configure stdin to receive keyboard characters one at a time and without echo

    tcgetattr(STDIN_FILENO, &stdin_config);

    if(atexit(ResetStdinConfig))         // Link ResetStdinConfig() to be called on exit()
    {
        fprintf(stderr,"atexit failed\n");
        exit(1);
    }

    stdin_config_raw = stdin_config;      // Keep copy of original configuration for ResetStdinConfig()

    cfmakeraw(&stdin_config_raw);
    tcsetattr(STDIN_FILENO, 0, &stdin_config_raw);

    // Initialize libterm for stdout and reset the terminal display

    TermLibInit(&term_vars, stdout, ProcessLine, Callback, PROMPT);

    SigWinch(0);

    // Loop forever to process keyboard characters

    for(;;)
    {
        // Wait for the next keyboard character

        fflush(stdout);

        keyboard_ch = getchar();

        // Report character value in the info zone on the terminal

//        printf(" 0x%02X ", keyboard_ch);

        printf(TERM_SAVE_POS TERM_CSI "%hu;21" TERM_GOTO TERM_CSI TERM_BOLD TERM_SGR "%3d" TERM_RESTORE_POS,
                (uint16_t)(window.ws_row - RTD_REPORT), keyboard_ch);

        // Catch CTRL-C to exit

        if(keyboard_ch == 0x03)
        {
            printf("\nExiting\n");
            exit(0);
        }

        // Give characters to libterm to be processed

        term_level = TermChar(&term_vars, keyboard_ch);

        // Check for ESC pressed twice - this will appear as ESC at terminal level zero

        if(term_level == 0 && keyboard_ch == TERM_ESC)
        {
            ResetTerm();
        }

        // The terminal level is a state machine used in TermChar to process keyboard character
        // sequences that start with ESC (0x1B) which can correspond to cursor keys and function keys.
        // term_level goes from 0-4.  When the first ESC is received it becomes 1.  This can be due
        // to the user pressing the ESC key or pressing a cursor or function key (in which case other
        // characters will follow).
        // If the user presses ESC twice, then term_level will go to 1 and then back to zero.  So
        // this is a convenient way to capture a double ESC as a signal from the user that they want
        // to refresh the terminal.
    }
}
// EOF

