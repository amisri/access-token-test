# liblog/scripts/read_csv.awk
#
# Converter Control Logging library :  Definition CSV file reader
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of liblog.
#
# liblog is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ReadCSV() sets the following hardcoded global variables
#
#    MAX_CYC_SEL                                            Maximum cycle selector
#    MAX_SIG_NAME_LEN                                       Maximum length of name_(units) (without trailing null)
#    MAX_NAME_LEN                                           Maximum FGC symbol name length
#    PM_BUF_HEADER_SIZE                                     Length of PM_BUF header (3 long integers)
#    PM_BUF_EVTLOG_SIZE                                     Length of Event Log record in bytes
#
# ReadCSV() sets the following global variables from the log CSV file:
#
#    num_logs                                               Number of types of logs defined in the CSV file
#    num_ext_menus                                          Number of external menus
#    total_num_logs                                         Total number of logs, taking into account the $REPEAT count
#
#  global_par = "$CLASS_ID", "$ITER_PERIOD_NS", "$PM_BUF_VERSION", "$LITTLE_ENDIAN",
#               "$READ_DELAY_MS", "$READ_RATE", "$EVTLOG_LEN", "$LOG_MENU_LIBLOG"
#
#    value[global_par]                                      Global log parameter value
#
#  ext_menu_idx = 1 - num_ext_menus
#  heading      = "$EXT_MENU", "$EXT_MENU_PROPERTY", "$EXT_MENU_FLAGS" :
#
#    ext_menu[ext_menu_idx, heading]                        External log menu data for specified menu and heading
#
#  log_idx = 1 - num_logs :
#
#    num_signals                 [log_idx]                  Number of known signals in the log
#    num_log_menus               [log_idx]                  Number of menus defined for the log
#    num_postmortem_sig_bufs     [log_idx]                  Number of post mortem signal buffers (defined by first log menu)
#    num_pm_buf_sigs             [log_idx]                  Number of PM_BUF signals for the log
#    num_sig_bufs                [log_idx]                  Total number of signal buffers
#    log_is_cyclic               [log_idx]                  1 = Log is cyclic          0 = Log is non-cyclic
#    log_is_analog               [log_idx]                  1 = Log is analog          0 = Log is digital
#    log_is_postmortem           [log_idx]                  1 = Log is in postmortem   0 = Log is not in postmortem
#    log_is_continuous           [log_idx]                  1 = Log is continuous      0 = Log is discontinuous
#    log_is_freezable            [log_idx]                  1 = Log is freezable       0 = Log is not freezable
#    log_is_multiplexed          [log_idx]                  1 = Log is multiplexed     0 = Log is not multiplexed
#    log_is_pm_buf               [log_idx]                  1 = Log is in PM_BUF       0 = Log is not in PM_BUF
#    log_is_alias                [log_idx]                  1 = Log is ALIAS           0 = Log is not an ALIAS
#
#  log_header_par = "$LOG_NAME", "$LOG_TITLE", "$LOG_TYPE", "$LOG_STATUS_FLAGS", "$MAX_CYC_SEL", "$DEF_PERIOD_ITERS",
#                   "$PM_BUF_NAME", "$NUM_SAMPLES", "$NUM_POSTMORTEM_SAMPLES", "$NUM_POST_FREEZE_SAMPLES", "$REPEAT", "$DIG_SOURCE"
#
#    value[log_header_par, log_idx]                         Log heading parameter
#
#  menu_idx = 1 - num_log_menus[log_idx] :
#
#    log_menu_name               [log_idx, menu_idx]        Menu name without the .SUFFIX if present
#    log_menu_type               [log_idx, menu_idx]        Suffix from menu name (MPX or nothing)
#    log_menu_comment            [log_idx, menu_idx]        Menu comment
#    log_menu_is_postmortem      [log_idx, menu_idx]        1 if this menu is included in the post mortem
#    log_menu_start_sig_sel_index[log_idx, menu_idx]        Index of first analog selector for the menu (0 - N-1)
#    log_menu_sig_sel_bit_mask   [log_idx, menu_idx]        Digital signals or Analog selectors bit mask for the menu
#    log_menu_pm_buf_sel_bit_mask[log_idx, menu_idx]        PM BUF signal selectors bit mask - only applies to menu_idx 1
#    log_menu_num_sigs           [log_idx, menu_idx]        Number of signals defined in this menu
#
#  menu_sig_idx = 1 - log_menu_num_sigs[log_idx, menu_idx] :
#
#    log_menu_signal[log_idx, menu_idx, menu_sig_idx]       Signal index (log_sig_idx) for this menu item for this log
#
#  log_sig_idx = 1 - num_signals[log_idx] :
#  heading     = "$SIG_NAME", "$SIG_UNITS", "$PM_BUF_SIG_NAME", "$STEP", "$TIME_OFFSET", "$SOURCE", "$SOURCE_TYPE" or "$BIT" :
#
#     signal[log_idx, log_sig_idx, heading]                 Signal data for specified signal and heading
#

function ReadCSV(input_file,  heading_idx, read_state, sig_name, buf_idx, i)
{
    if(input_file == "")
    {
        print "ReadCSV: No input filename provided"
        exit 1
    }

    # Initialise or delete arrays

    if(FS != ";")
    {
        # Set field separater to comma to read csv file

        FS = ";"

        # Signal name prefixes to units

        unit_prefixes["U_"] = "V"
        unit_prefixes["F_"] = "V"
        unit_prefixes["D_"] = "V"
        unit_prefixes["V_"] = "V"
        unit_prefixes["I_"] = "A"
        unit_prefixes["B_"] = "G"
        unit_prefixes["T_"] = "C"
    }
    else
    {
        # Delete the arrays from the previous call

        delete value
        delete ext_menu
        delete num_signals
        delete num_log_menus
        delete num_postmortem_sig_bufs
        delete num_pm_buf_sigs
        delete num_sig_bufs
        delete log_is_cyclic
        delete log_is_analog
        delete log_is_postmortem
        delete log_is_continuous
        delete log_is_freezable
        delete log_is_multiplexed
        delete log_is_pm_buf
        delete log_is_alias
        delete log_menu_name
        delete log_menu_type
        delete log_menu_comment
        delete log_menu_is_postmortem
        delete log_menu_start_sig_sel_index
        delete log_menu_sig_sel_bit_mask
        delete log_menu_pm_buf_sel_bit_mask
        delete log_menu_num_sigs
        delete log_menu_signal
        delete signal
    }

    # Initialize variables

    total_num_logs    = 0   # Total number of logs, taking into account the $REPEAT count
    num_ext_menus     = 0   # Number of external menus
    heading_idx       = 0   # Column heading index
    num_logs          = 0   # Total number of logs
    line_num          = 0   # Number of lines in the definition file
    ext_sig_name_idx  = 0   # Index to external signal names array
    ext_units_idx     = 0   # Index to external units array

    # Read state machine states
    # READ_GLOBALS -> READ_NEW_LOG -> READ_HEADER -> READ_LOG_MENU -> READ_SIGNALS -> READ_NEW_LOG -> ...

    READ_GLOBALS  = 0
    READ_EXT_MENU = 1
    READ_NEW_LOG  = 2
    READ_HEADER   = 3
    READ_LOG_MENU = 4
    READ_SIGNALS  = 5

    # Set hardcoded global variables

    MAX_CYC_SEL        = 32    # Maximum cycle selector
    MAX_SIG_NAME_LEN   = 23    # Maximum length of name_(units) (without trailing null)
    MAX_NAME_LEN       = 19    # Maximum FGC symbol name length
    PM_BUF_HEADER_SIZE = 16    # Length of PM_BUF header (4 long integers)
    PM_BUF_EVTLOG_SIZE = 76    # Length of Event Log record in bytes

    read_state = READ_GLOBALS

    # Read CSV file

    print "Reading " input_file

    while((getline < input_file) > 0)
    {
        line_num++

        # Skip blank lines or comment lines

        if($0 == "" || $1 ~ "^#") continue

        # Record global parameters before first log name is specified

        if(read_state == READ_GLOBALS)
        {
            read_state = ReadGlobalParameter()
            continue
        }

        # Read external menus

        if(read_state == READ_EXT_MENU)
        {
            if($1 == "$LOG_NAME")
            {
                read_state = READ_NEW_LOG

                # Fall through to ReadNewLog below
            }
            else
            {
                ReadExtLogMenu()
                continue
            }
        }

        # Read log parameters

        if(read_state == READ_HEADER)
        {
            read_state = ReadLogHeaderParameters()
            continue
        }

        # Read log menus

        if(read_state == READ_LOG_MENU)
        {
            read_state = ReadLogMenus()
            continue
        }

        # Read signals

        if(read_state == READ_SIGNALS)
        {
            if($1 == "$LOG_NAME")
            {
                read_state = READ_NEW_LOG

                # Fall through to ReadNewLog below
            }
            else
            {
                ProcessSignal()
                continue
            }
        }

        if(read_state == READ_NEW_LOG)
        {
            read_state = ReadNewLog()
            continue
        }

        Error(input_file, line_num, "Unknown state: " read_state)
    }

    close(input_file)
    line_num = 9999

    # Validate the last log

    PostProcessLog()

    # Limit total number of logs to 32 because we use 32-bit bit masks

    if(total_num_logs > 32)
    {
        Error(input_file, line_num, "Total number of logs (" total_num_logs ") exceeds limit (32)")
    }
}



function ReadGlobalParameter( )
{
    if(NF < 2 || CheckKey($1))
    {
        Error(input_file, line_num, "Invalid global parameter: " $0)
    }

    if($1 == "$LOG_NAME")
    {
        # Start log parsing after first log name is specified

        return READ_NEW_LOG
    }

    # If $EXT_MENU heading encountered then start processing external log menus

    if($1 == "$EXT_MENU")
    {
        # Save external menu headings

        for(heading_idx = 1 ; !CheckKey($heading_idx) ; heading_idx++)
        {
            if($heading_idx !~ /\$EXT_MENU|\$EXT_MENU_PROPERTY|\$EXT_MENU_FLAGS/)
            {
                Error(input_file, line_num, "Unknown external menu heading " $heading_idx)
            }

            ext_menu_heading[heading_idx] = $heading_idx
        }

        num_ext_menu_headings = heading_idx - 1

        return READ_EXT_MENU
    }
    else
    {
        value[$1] = $2
    }

    # Return one to continue to next line

    return READ_GLOBALS
}



function ReadExtLogMenu(  num_flags, flags, ext_menu_status)
{
    num_ext_menus++

    # Check that the first value is no empty

    if($1 == "")
    {
        Error(input_file, line_num,"Ext log menu field is empty")
    }

    # Save external menu parameters

    for(heading_idx = 1 ; heading_idx <= num_ext_menu_headings ; heading_idx++)
    {
        ext_menu[num_ext_menus, ext_menu_heading[heading_idx]] = $heading_idx
    }

    ext_menu[num_ext_menus,"$EXT_MENU_FLAGS"] = LogMenuStatusFlags(ext_menu[num_ext_menus,"$EXT_MENU_FLAGS"], "0")
}



function ReadLogHeaderParameters( )
{
    if(NF < 2 || CheckKey($1))
    {
        Error(input_file, line_num, "Invalid header parameter line:" $0)
    }

    # Start parsing log menus after log menu heading was specified

    if($1 == "$LOG_MENU")
    {
        return READ_LOG_MENU
    }

    # Save parameter

    value[$1, num_logs] = $2

    return READ_HEADER
}



function ReadLogMenus(  heading_idx)
{
    # If $SIG_NAME heading encountered then start processing log signals

    if($1 == "$SIG_NAME")
    {
        for(heading_idx = 1 ; !CheckKey($heading_idx) ; heading_idx++)
        {
            if($heading_idx !~ /\$SIG_NAME|\$STEP|\$SOURCE|\$TIME_OFFSET|\$BIT|\$SOURCE_TYPE/)
            {
                Error(input_file, line_num,"Unknown heading " $heading_idx)
            }

            sig_heading[heading_idx] = $heading_idx
        }

        num_sig_headings[num_logs] = heading_idx - 1

        return READ_SIGNALS
    }
    else
    {
        # Pass log menu name and comment to be processed

        ProcessLogMenu($1, $2)
    }

    return READ_LOG_MENU
}



function ReadNewLog( )
{
    # Validate previous log

    if(num_logs != 0)
    {
        PostProcessLog()
    }

    # Start parsing next log

    num_logs++

    num_log_menus  [num_logs] = 0
    num_debug_menus[num_logs] = 0
    log_num_pm_sigs[num_logs] = 0
    num_signals    [num_logs] = 0

    # Save the log name and start parsing the header

    value[$1, num_logs] = $2

    return READ_HEADER
}



function ProcessSignal(  heading_idx, n, i, l, sig_name)
{
    num_signals[num_logs]++

    # Save signal parameters

    for(heading_idx = 1 ; heading_idx <= num_sig_headings[num_logs] ; heading_idx++)
    {
        signal[num_logs, num_signals[num_logs], sig_heading[heading_idx]] = $heading_idx
    }

    # Check signal name for spaces

    if(index(signal[num_logs, num_signals[num_logs], "$SIG_NAME"], " ") != 0)
    {
        Error(input_file, line_num, "Signal name '" signal[num_logs, num_signals[num_logs],"$SIG_NAME"] "' contains spaces")
    }

    # Name format: NAME[:UNITS][[.menu_indexes].PM_BUF_SIG_NAME]

    n = split(signal[num_logs, num_signals[num_logs], "$SIG_NAME"], sig_name, ".")

    if(n > 3)
    {
        Error(input_file, line_num, "Signal " signal[num_logs, num_signals[num_logs], "$SIG_NAME"] " in log " value["$LOG_NAME",num_logs] " does not have a well formed log menu specifier")
    }

    if(n == 1 && log_is_multiplexed[num_logs] == 0 && value["$DIG_SOURCE",num_logs] == "#")
    {
        Error(input_file, line_num, "Signal " sig_name[1] " is not assigned to a log menu in " value["$LOG_NAME", num_logs] )
    }

    if(n > 1)
    {
        # Overwrite signal name without the prefix

        signal[num_logs, num_signals[num_logs], "$SIG_NAME"] = sig_name[1]

        # Iterate through each digit (log menu) in the suffix

        suffix_len = length(sig_name[2])

        for(i = 1 ; i <= suffix_len ; i++)
        {
            log_menu_idx = substr(sig_name[2], i, 1)

            # Bail out if there's a non-digit (1-8) character in the suffix or if the index doesn't match any of the log menus

            if(log_menu_idx !~ /[1-8]/)
            {
                Error(input_file, line_num, "Error in the suffix " sig_name[2] " - " log_menu_idx " is not a digit from 1-8")
            }

            if(log_menu_idx > num_log_menus[num_logs])
            {
                Error(input_file, line_num, "Wrong log menu index (" log_menu_idx ") - there's no such menu")
            }

            if(log_menu_signal[num_logs, log_menu_idx, log_menu_num_sigs[num_logs, log_menu_idx]] == num_signals[num_logs])
            {
                Error(input_file, line_num, "Error in the log menu specifier " sig_name[2] " - " log_menu_idx " is repeated")
            }

            # Number of signals (and/or selectors) in the menu

            log_menu_num_sigs[num_logs, log_menu_idx]++
            log_menu_signal  [num_logs, log_menu_idx, log_menu_num_sigs[num_logs, log_menu_idx]] = num_signals[num_logs]
        }
    }

    if(n == 3)
    {
        # PM_BUF_SIG_NAME provided

        signal[num_logs, num_signals[num_logs], "$PM_BUF_SIG_NAME"] = sig_name[3]

        # Check PM_BUF signal name length

        l = length(signal[num_logs, num_signals[num_logs], "$PM_BUF_SIG_NAME"])

        if(l == 0)
        {
            Error(input_file, line_num, "PM_BUF signal name field is empty")
        }

        if(l > MAX_NAME_LEN)
        {
            Error(input_file, line_num, "PM_BUF Signal Name " signal[num_logs, num_signals[num_logs], "$PM_BUF_SIG_NAME"] " in log " value["$LOG_NAME",num_logs] " exceeds " MAX_NAME_LEN " characters")
        }

        # Check that log has a PM_BUF_NAME

        if(value["$PM_BUF_NAME", num_logs] == "#")
        {
            Error(input_file, line_num, "PM_BUF Signal Name " signal[num_logs, num_signals[num_logs], "$PM_BUF_SIG_NAME"] " defined in log " value["$LOG_NAME",num_logs] ", which does not have a PM_BUF_NAME")
        }

        # Check that the signal is in the first (PM) log menu

        if(log_menu_signal[num_logs, 1, log_menu_num_sigs[num_logs, 1]] != num_signals[num_logs])
        {
            Error(input_file, line_num, "PM_BUF Signal Name " signal[num_logs, num_signals[num_logs], "$PM_BUF_SIG_NAME"] " defined for a signal that is not in the first (PM) log menu")
        }
    }

    # Split signal name on a ":" - if present, the string following the : defines the units for the signal

    n = split(signal[num_logs, num_signals[num_logs], "$SIG_NAME"], sig_name, ":")

    if(n > 2 || (n == 2 && sig_name[2] == ""))
    {
        Error(input_file, line_num, "Signal " signal[num_logs, num_signals[num_logs], "$SIG_NAME"] " in log " value["$LOG_NAME",num_logs] " does not have a well formed units specifier")
    }

    if(n == 2)
    {
        # Units supplied so overwrite signal name without the suffix

        signal[num_logs, num_signals[num_logs], "$SIG_NAME"] = sig_name[1]

        # Save the units string separately

        signal[num_logs, num_signals[num_logs], "$SIG_UNITS"] = sig_name[2]
    }
    else
    {
        # Units not supplied so use default units if the signal name starts with a standard prefix

        signal[num_logs, num_signals[num_logs], "$SIG_UNITS"] = unit_prefixes[substr(sig_name[1],1,2)]
    }

    # Check signal name length

    l = length(signal[num_logs, num_signals[num_logs], "$SIG_NAME"])

    if(l == 0)
    {
        Error(input_file, line_num, "Signal name field in log " num_logs " is empty")
    }

    if(l > MAX_NAME_LEN)
    {
        printf("WARNING: Signal name " signal[num_logs, num_signals[num_logs],"$SIG_NAME"] " exceeds " MAX_NAME_LEN " characters - it will be truncated to create an FGC symbol\n")
    }

    # Check length of signal name + signal units

    if((3 + l + length(signal[num_logs, num_signals[num_logs], "$SIG_UNITS"])) > MAX_SIG_NAME_LEN)
    {
        Error(input_file, line_num, "Signal name " signal[num_logs, num_signals[num_logs],"$SIG_NAME"] " (with units) exceeds " MAX_SIG_NAME_LEN " characters")
    }
}



function ProcessLogMenu(menu_name, menu_comment)
{
    if(length(menu_name) > MAX_NAME_LEN)
    {
        Error(input_file, line_num, "Log menu name " menu_name " exceeds " MAX_NAME_LEN " characters")
    }

    num_log_menus[num_logs]++

    log_menu_name[num_logs, num_log_menus[num_logs]] = menu_name

    # Look for _MPX suffix - mark log and log_menu as multiplexed if the suffix is detected

    if(menu_name ~ /_MPX$/)
    {
        log_menu_type[num_logs, num_log_menus[num_logs]] = "MPX"

        log_is_multiplexed[num_logs] = 1
    }
    else
    {
        log_is_multiplexed[num_logs] = 0
    }

    # Save menu comment

    log_menu_comment[num_logs, num_log_menus[num_logs]] = menu_comment

    # Set default value 0, to be corrected by PostProcessLog

    log_menu_is_postmortem[num_logs, num_log_menus[num_logs]] = 0

    # Translate log status flags into bit masks

    value["$LOG_STATUS_FLAGS",num_logs] = LogMenuStatusFlags(value["$LOG_STATUS_FLAGS",num_logs])
}



function PostProcessLog(  n, log_type)
{
    # Keep track of total number of logs by counting the REPEAT values

    total_num_logs += value["$REPEAT", num_logs]

    # Process LOG_TYPE fields

    n = split(value["$LOG_TYPE", num_logs], log_type, ".")

    if(n < 4 || n > 5)
    {
        Error(input_file, line_num, "Number of field in $LOG_TYPE (" value["$LOG_TYPE", num_logs] ") is <4 or >5 for log " num_logs " : " value["$LOG_NAME",num_logs])
    }

    log_is_cyclic    [num_logs] = CheckLogType(log_type[1], "CY", "NC")
    log_is_analog    [num_logs] = CheckLogType(log_type[2], "AN", "DG")
    log_is_postmortem[num_logs] = CheckLogType(log_type[3], "PM", "NP")
    log_is_continuous[num_logs] = CheckLogType(log_type[4], "CO", "DC")

    if(log_is_postmortem[num_logs] == 1)
    {
         log_menu_is_postmortem[num_logs, 1] = 1
    }

    if(n == 5)
    {
        log_is_freezable[num_logs] = CheckLogType(log_type[5], "FR", "NF")

        if(!log_is_continuous[num_logs])
        {
            Error(input_file, line_num, "Freezable state (" log_type[5] ") must not be specified for DISCONTINOUS log " value["$LOG_NAME",num_logs])
        }
    }

    # Set MAX_CYC_SEL according to whether log is  CYCLIC or NON-CYCLIC

    if(log_is_cyclic[num_logs])
    {
        value["$MAX_CYC_SEL",num_logs] = MAX_CYC_SEL
    }
    else
    {
        value["$MAX_CYC_SEL",num_logs] = 0
    }

    # Check consistency for POSTMORTEM/NON-POSTMORTEM

    if(value["$NUM_POSTMORTEM_SAMPLES",num_logs] < 2)
    {
        Error(input_file, line_num, "$NUM_POSTMORTEM_SAMPLES (" value["$NUM_POSTMORTEM_SAMPLES", num_logs] ") for log " value["$LOG_NAME",num_logs] " must greater than one")
    }

    if(log_is_postmortem[num_logs] && log_menu_type[num_logs, 1] == "MPX")
    {
        Error(input_file, line_num, "First menu (" log_menu_name[num_logs, 1] ") of POSTMORTEM log " value["$LOG_NAME",num_logs] " must not end .MPX")
    }

    # Check consistency for CONTINUOUS/DISCONTINUOUS

    if(log_is_continuous[num_logs])
    {
        # Check consistency for FREEZABLE/NON-FREEZABLE

        if((log_is_freezable[num_logs] && value["$NUM_POST_FREEZE_SAMPLES",num_logs] < 1) || \
          (!log_is_freezable[num_logs] && value["$NUM_POST_FREEZE_SAMPLES",num_logs] != 0))
        {
            Error(input_file, line_num, "$NUM_POST_FREEZE_SAMPLES (" value["$NUM_POST_FREEZE_SAMPLES", num_logs] ") for log " value["$LOG_NAME",num_logs] " is inconsistent")
        }
    }
    else
    {
        if(value["$NUM_POST_FREEZE_SAMPLES",num_logs] != 0)
        {
            Error(input_file, line_num, "$NUM_POST_FREEZE_SAMPLES (" value["$NUM_POST_FREEZE_SAMPLES", num_logs] ") for DISCONTINUOUS log " value["$LOG_NAME",num_logs] " must be zero")
        }
    }

    # Check DIG_SOURCE is not specified for an analog log

    if(log_is_analog[num_logs] && value["$DIG_SOURCE",num_logs] != "#")
    {
        Error(input_file, line_num, "$DIG_SOURCE (" value["$DIG_SOURCE", num_logs] ") for ANALOG log " value["$LOG_NAME",num_logs] " must be '#'")
    }

    # Process log menus to calculate signal/selector/pm_buf_selector bit masks

    selector_idx = 0
    num_pm_buf_sigs[num_logs] = 0

    for(menu_idx = 1 ; menu_idx <= num_log_menus[num_logs] ; menu_idx++)
    {
        log_menu_start_sig_sel_index[num_logs,menu_idx] = selector_idx

        for(menu_sig_idx = 1 ; menu_sig_idx <= log_menu_num_sigs[num_logs, menu_idx]; menu_sig_idx++)
        {
            if(log_is_analog[num_logs])
            {
                log_menu_sig_sel_bit_mask[num_logs, menu_idx] += LeftShift(1, selector_idx)

                if(menu_idx == 1 && signal[num_logs, log_menu_signal[num_logs, menu_idx, menu_sig_idx], "$PM_BUF_SIG_NAME"] != "")
                {
                    log_menu_pm_buf_sel_bit_mask[num_logs, menu_idx] += LeftShift(1, selector_idx)
                    num_pm_buf_sigs[num_logs]++
                }
            }
            else
            {
                log_menu_sig_sel_bit_mask[num_logs, menu_idx] += LeftShift(1, (log_menu_signal[num_logs, menu_idx, menu_sig_idx] - 1))
            }

            selector_idx++
        }
    }

    if(selector_idx > num_signals[num_logs])
    {
        Error(input_file, line_num, "Total number of signal buffers (" selector_idx ") for log " value["$LOG_NAME",num_logs] " must be less the total number of known signals (" num_signals[num_logs] ")")
    }

    # Check that $PM_BUF_NAME is only set if PM_BUF signals are defined for that log

    if(value["$PM_BUF_NAME", num_logs] != "#" && log_menu_pm_buf_sel_bit_mask[num_logs, 1] == 0)
    {
        Error(input_file, line_num, "Log " value["$LOG_NAME",num_logs] " has PM_BUF name " value["$PM_BUF_NAME", num_logs] " but no signals have a PM_BUF_SIG_NAME defined")
    }

    # Check that $PM_BUF_NAME is only set for *.AN.PM.*.* logs (analog and post-mortem)

    if(value["$PM_BUF_NAME", num_logs] != "#" && log_is_analog[num_logs] == 0)
    {
        Error(input_file, line_num, "Digital log " value["$LOG_NAME",num_logs] " has PM_BUF name " value["$PM_BUF_NAME", num_logs] " set - only analog logs can be in the PM_BUF")
    }

    if(value["$PM_BUF_NAME", num_logs] != "#" && log_is_postmortem[num_logs] == 0)
    {
        Error(input_file, line_num, "Log " value["$LOG_NAME",num_logs] " has PM_BUF name " value["$PM_BUF_NAME", num_logs] " set but it is not part of the post-mortem (PM)")
    }

    if(value["$PM_BUF_NAME", num_logs] != "#" && value["$REPEAT",num_logs] > 1)
    {
        Error(input_file, line_num, "Log " value["$LOG_NAME",num_logs] " has PM_BUF name " value["$PM_BUF_NAME", num_logs] " set so $REPEAT must be 1")
    }

    log_is_pm_buf[num_logs] = (log_menu_pm_buf_sel_bit_mask[num_logs, 1] > 0)

    # Check if the log is an alias

    log_is_alias[num_logs] = (match(value["$LOG_STATUS_FLAGS", num_logs], "LOG_MENU_STATUS_ALIAS_BIT_MASK") > 0)

    # Set total number of signal buffers and number of postmortem signal buffers from the log menus

    if(log_is_analog[num_logs])
    {
        # Analog logs require 1 signal buffer per signal

        num_sig_bufs[num_logs]            = selector_idx
        num_postmortem_sig_bufs[num_logs] = log_menu_num_sigs[num_logs, 1]
    }
    else
    {
        # Digital logs require only 1 signal buffer for all signals

        num_sig_bufs[num_logs]            = 1
        num_postmortem_sig_bufs[num_logs] = 1
    }

    # Override num_postmortem_sig_bufs if log does not support postmortem

    if(!log_is_postmortem[num_logs])
    {
        num_postmortem_sig_bufs[num_logs] = 0
    }
}


function LogMenuStatusFlags(flags, status_flags,  num_flags, flag_index, flag)
{
    if(flags ~ "^#")
    {
        return ""
    }

    # Prepare external menu status by adding prefix/suffix to flags

    num_flags = split(flags, flag, " ")

    for(flag_index = 1 ; flag_index <= num_flags ; flag_index++)
    {
        if(flag[flag_index] !~ /CYCLIC|ANALOG|POSTMORTEM|CONTINUOUS|FREEZABLE|MPX|DISABLED|RUNNING|SAVE|FUTURE|DIM|VS1|VS2|ALIAS/)
        {
            Error(input_file, line_num, "Unknown log menu status flag " flag[flag_index])
        }

        status_flags = status_flags " | LOG_MENU_STATUS_" flag[flag_index] "_BIT_MASK"
    }

    return status_flags
}



function CheckLogType(field, true, false)
{
    if(field == true)
    {
        return 1
    }

    if(field == false)
    {
        return 0
    }

    Error(input_file, line_num, "Invalid $LOG_TYPE (" value["$LOG_TYPE", num_logs] ") for " value["$LOG_NAME", num_logs] " log : " field)
}



function CheckKey(key)
{
    return substr(key, 1, 1) != "$"
}



function LeftShift(mask, num_bits)
{
    if(num_bits > 0)
    {
        mask *= 2 ^ num_bits
    }

    return mask
}



function SigName(log_idx, sig_idx,  sig_name)
{
    sig_name = signal[log_idx,sig_idx,"$SIG_NAME"]

    if(sig_name == "*")
    {
        sig_name = "EXT_" (sig_idx-1)
    }

    return sig_name
}

# EOF
