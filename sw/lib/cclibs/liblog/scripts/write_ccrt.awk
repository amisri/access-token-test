# liblog/scripts/write_ccrt.awk
#
# Converter Control Logging library : ccrt header files writer
#
# Liblog manages multiple log structures. This file contains the functions
# that generate header files to ease integration with ccrt.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of liblog.
#
# liblog is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    # Get output path from command line

    if(ARGC != 3)
    {
        printf "usage: write_ccrt.awk input_csv_file output_path\n"
        exit 1
    }

    gsub(/\/+$/, "", ARGV[2]) # remove trailing slashes from output_path

    input_file  = ARGV[1]
    output_path = ARGV[2]

    # Read log CSV file (semicolon separated) and then generate logMenus and logStructs header files.

    ReadCSV(input_file)

    # Generate header file defining the LOG MPX enums

    WriteLogMpxEnums(output_path "/log_mpx_enums.h")

    # Generate header file defining the debug log pars records

    WriteLogMpxPars(output_path "/log_mpx_pars.h")

    # Generate header file defining the logmenu and logstatus pars records

    WriteLogMenuPars(output_path "/log_menu_pars.h",        "ENUM",    "ccpars_logmenu",           "enabled_disabled")
    WriteLogMenuPars(output_path "/log_menu_status_pars.h", "BITMASK", "log_menus.status_bit_mask","log_menu_status")
}



function WriteLogMpxEnums(of, label,  log_idx, menu_idx, sig_idx)
{
    WriteGnuLicense("liblog", "Logging", "ccrt log mpx enums header file", of)

    # Enum for log names

    printf "// Log names enum\n\n"                                                                            > of
    printf "CCPARS_LOG_EXT struct CC_pars_enum enum_logs[]\n"                                                 > of
    printf "#ifdef GLOBALS\n= {\n"                                                                            > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        const_name  = "LOG_" value["$LOG_NAME",log_idx]
        log_name = "\"" value["$LOG_NAME",log_idx] "\""

        printf "    { %-30s, %-20s },\n", const_name, log_name > of

        for(i = 2 ; i <= value["$REPEAT",log_idx] ; i++)
        {
            log_name = "\"" value["$LOG_NAME",log_idx] i "\""
            printf "    { %-30s, %-20s },\n", const_name i, log_name > of
        }
    }

    printf "    { %-30s, %-20s },\n", "0", "NULL" > of
    print  "}\n#endif\n;\n" > of

    # Enums for signal names per log for analog logs which do not repeat

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_analog[log_idx] && value["$REPEAT",log_idx] == 1)
        {
            for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
            {
                printf "// Log %s enum\n\n", value["$LOG_NAME", log_idx]                                                  > of
                printf "CCPARS_LOG_EXT struct CC_pars_enum enum_log_%s[]\n", tolower(log_menu_name[log_idx,menu_idx]) > of
                printf "#ifdef GLOBALS\n= {\n"                                                                            > of

                for(sig_idx = 1 ; sig_idx <= num_signals[log_idx] ; sig_idx++)
                {
                    sig_name = SigName(log_idx, sig_idx)

                    const_name  = "LOG_" value["$LOG_NAME",log_idx] "_" sig_name
                    signal_name = "\"" sig_name "\""

                    printf "    { %-30s, %-20s },\n", const_name, signal_name > of
                }

                printf "    { %-30s, %-20s },\n", "0", "NULL" > of
                print  "}\n#endif\n;\n" > of
            }
        }
    }

    print "// EOF" > of

    close(of)
}



function WriteLogMpxPars(of,  log_idx, menu_idx, menu_name, par_name, par_access)
{
    WriteGnuLicense("liblog", "Logging", "ccrt log mpx pars header file", of)

    # ccrt parameters for log memnus for analog logs which do not repeat

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_analog[log_idx] && value["$REPEAT",log_idx] == 1)
        {
            for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
            {
                if(log_menu_type[log_idx,menu_idx] == "MPX")
                {
                    par_access = "RW|PARS_CFG|PARS_LOG_MPX"
                    par_name   = "\"" log_menu_name[log_idx,menu_idx] "\""
                }
                else
                {
                    par_access = "RO                      "
                    par_name   = "\"" log_menu_name[log_idx,menu_idx] "\""
                }

                printf "{ %-20s, PAR_ENUM, ",      par_name                                                  > of
                printf "LOG_MENU_%-20s, "   ,      log_menu_name[log_idx,menu_idx]          "_LEN"           > of
                printf "enum_log_%-20s, ",         tolower(log_menu_name[log_idx,menu_idx])                  > of
                printf "{ .u = &log_buffers.%-26s",tolower(value["$LOG_NAME", log_idx])     ".selectors[0]"  > of
                printf "[LOG_MENU_%-20s }, ",      log_menu_name[log_idx,menu_idx]          "_IDX]"          > of
                printf "LOG_MENU_%-20s,  ",        log_menu_name[log_idx,menu_idx]          "_LEN"           > of
                printf "PARS_FIXLEN|PARS_%s },\n", par_access                                                > of
            }
        }
    }

    print "{ NULL }\n\n// EOF" > of

    close(of)
}



function WriteLogMenuPars(of, var_type, var_name, enum,  log_idx, menu_idx, repeat_idx)
{
    WriteGnuLicense("liblog", "Logging", "ccrt log menu pars header file", of)

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
        {
            for(repeat_idx = 1 ; repeat_idx <= value["$REPEAT",log_idx] ; repeat_idx++)
            {
                menu_name = log_menu_name[log_idx,menu_idx]

                if(repeat_idx > 1) menu_name = menu_name repeat_idx

                WriteLogMenuPar(of, menu_name, var_type, var_name, enum)
            }
        }
    }

    for(ext_menu_idx = 1 ; ext_menu_idx <= num_ext_menus ; ext_menu_idx++)
    {
        WriteLogMenuPar(of, ext_menu[ext_menu_idx,"$EXT_MENU"], var_type, var_name, enum)
    }

    print "\n// EOF" > of

    close(of)
}



function WriteLogMenuPar(of, menu_name, var_type, var_name, enum,  par_name)
{
    par_name = "\"" menu_name "\""

    printf "{ %-20s, PAR_%s, 1, enum_%s, ", par_name, var_type, enum > of
    printf "{ .u = &%s[LOG_MENU_%-20s]", var_name, menu_name         > of
    printf "}, 1, PARS_RW },\n"                                      > of
}

# EOF
