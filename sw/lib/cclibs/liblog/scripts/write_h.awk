# liblog/scripts/write_h.awk
#
# Converter Control Logging library : Structures header files writer
#
# Liblog manages multiple log structures. These functions convert a csv file defining
# the logs into header files that declare and initialize the structures.
# It writes logStructs.h and logStructsInit.h, logMenus.h and logMenusInit.h.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of liblog.
#
# liblog is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    # Set DEBUG_SIGNALS to 1 to inject static floating-point values LLSS into all analog signals,
    # where LL is the global log index and SS is signal index (both start at 1)

    DEBUG_SIGNALS = 0

    # Get output path from command line

    if(ARGC != 3)
    {
        printf "usage: write_h.awk input_csv_file output_path\n"
        exit 1
    }

    gsub(/\/+$/, "", ARGV[2]) # remove trailing slashes from output_path

    input_file  = ARGV[1]
    output_path = ARGV[2]

    # Read log CSV file (semicolon separated) and then generate logMenus and logStructs header files.

    ReadCSV(input_file)

    ProcessLogLengths()

    ProcessLogMeta()

    # Generate Log Menus header files

    WriteLogMenus(output_path "/logMenus.h")

    WriteLogMenusInit(output_path "/logMenusInit.h")

    # Generate Log Structures header files

    WriteLogStructs(output_path "/logStructs.h")

    WriteLogStructsInit(output_path "/logStructsInit.h")

    # If the version of the PM_BUF is not zero then generate the pm_buf header file

    if(value["$PM_BUF_VERSION"] > 0)
    {
        WritePmBuf(output_path "/pm_buf_" value["$CLASS_ID"] ".h")
    }
}



# ProcessLogLengths produces:
#
#    buffers_len                total length of all buffers in elements
#    max_buf_len                max value of buf_len[]
#    buf_len[log_idx]           length of buffer of log_idx
#    pm_buf_size                total size of pm_buf in bytes (version + event log + log headers + log data)
#
function ProcessLogLengths(  log_idx, offset)
{
    # Only floats and 32 bit integers are allowed, both have length of 4 bytes

    buffers_len = 0
    max_buf_len = 0

    # PM_BUF starts with the version word so start with the length of 4 bytes

    pm_buf_size  = 4

    # PM_BUF can optionally include the event log

    pm_buf_size += value["$EVTLOG_LEN"] * PM_BUF_EVTLOG_SIZE

    # Loop for all logs

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        buf_len[log_idx] = num_sig_bufs[log_idx] * value["$NUM_SAMPLES", log_idx]
        buffers_len     += buf_len[log_idx] * (log_is_alias[log_idx] == 0 ? value["$REPEAT", log_idx] : 1)

        # Keep track of the maximum log buffer length (in elements)

        if(buf_len[log_idx] > max_buf_len)
        {
            max_buf_len = buf_len[log_idx]
        }

        # Calculate the total length of all the PM_BUF headers and data

        if(log_is_pm_buf[log_idx])
        {
            pm_buf_size += PM_BUF_HEADER_SIZE + 4 * num_pm_buf_sigs[log_idx] * value["$NUM_POSTMORTEM_SAMPLES", log_idx]
        }
    }
}



# ProcessLogMeta produces:
#
#    log_meta[log_idx]       initialization string for log->meta
#
function ProcessLogMeta(  log_idx, global_log_idx)
{
    # Create log->meta initialization strings using LOG_META bit mask constants

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        log_meta[log_idx] = "0"

        AccumulateLogMeta(log_idx, value["$LITTLE_ENDIAN"] == "YES", "LITTLE_ENDIAN")
        AccumulateLogMeta(log_idx, log_is_cyclic    [log_idx], "CYCLIC")
        AccumulateLogMeta(log_idx, log_is_analog    [log_idx], "ANALOG")
        AccumulateLogMeta(log_idx, log_is_postmortem[log_idx], "POSTMORTEM")
        AccumulateLogMeta(log_idx, log_is_continuous[log_idx], "CONTINUOUS")
        AccumulateLogMeta(log_idx, log_is_freezable [log_idx], "FREEZABLE")
    }

    # Create log->meta bit masks for log_mgr initialization

    global_log_idx = 0

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(i = 1 ; i <= value["$REPEAT",log_idx] ; i++)
        {
            cyclic_logs_mask     += LeftShift(log_is_cyclic    [log_idx], global_log_idx)
            analog_logs_mask     += LeftShift(log_is_analog    [log_idx], global_log_idx)
            postmortem_logs_mask += LeftShift(log_is_postmortem[log_idx], global_log_idx)
            continuous_logs_mask += LeftShift(log_is_continuous[log_idx], global_log_idx)
            freezable_logs_mask  += LeftShift(log_is_freezable [log_idx], global_log_idx)

            global_log_idx++
        }
    }
}



function AccumulateLogMeta(log_idx, condition, label)
{
    if(condition)
    {
        log_meta[log_idx] = log_meta[log_idx] "|LOG_META_" label "_BIT_MASK"
    }
}



function WriteLogMenus(of,  log_idx, sig_idx)
{
    WriteGnuLicense("liblog", "Logging", "Menu structures header file", of)

    print "#pragma once\n" > of

    print "// Include liblog client header file\n" > of

    print "#include \"liblog/logClient.h\"\n" > of

    # Log constants

    print "// Global log constants (LENs are in elements, SIZEs are in bytes)\n" > of

    constant_name_len = 40

    printf "#define LOG_%-*s%d\n", constant_name_len, "MAX_LOG_LEN",     max_buf_len > of
    printf "#define LOG_%-*s%d\n", constant_name_len, "BUFFERS_LEN",     buffers_len > of
    printf "#define LOG_%-*s%d\n", constant_name_len, "PM_BUF_SIZE",     pm_buf_size > of
    printf "#define LOG_%-*s%d\n", constant_name_len, "PM_BUF_VERSION",  value["$PM_BUF_VERSION"] > of

    print "\n// Individual log constants\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        printf   "#define LOG_%-*s%d\n",   constant_name_len, value["$LOG_NAME",log_idx] "_NUM_LOGS",                value["$REPEAT",log_idx] > of
        printf   "#define LOG_%-*s%d\n",   constant_name_len, value["$LOG_NAME",log_idx] "_NUM_SIG_BUFS",            num_sig_bufs[log_idx] > of
        printf   "#define LOG_%-*s%d\n",   constant_name_len, value["$LOG_NAME",log_idx] "_NUM_POSTMORTEM_SIG_BUFS", num_postmortem_sig_bufs[log_idx] > of
        printf   "#define LOG_%-*s%d\n\n", constant_name_len, value["$LOG_NAME",log_idx] "_NUM_CYCLES",              value["$MAX_CYC_SEL",log_idx] + 1 > of
    }

    print "// Analog log menu constants\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_analog[log_idx])
        {
            for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
            {
                printf   "#define LOG_MENU_%-*s%d\n",35, log_menu_name[log_idx, menu_idx] "_IDX",
                                                         log_menu_start_sig_sel_index[log_idx,menu_idx] > of

                printf   "#define LOG_MENU_%-*s%d\n",35, log_menu_name[log_idx, menu_idx] "_LEN",
                                                         log_menu_num_sigs [log_idx, menu_idx] > of
            }
        }
    }

    # enum LOG_index

    print "\n// Enums\n" > of
    print "enum LOG_index\n{" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        printf "    LOG_%s,\n", value["$LOG_NAME",log_idx] > of

        for(i = 2 ; i <= value["$REPEAT",log_idx] ; i++)
        {
            printf "    LOG_%s%u,\n", value["$LOG_NAME",log_idx], i > of
        }
    }

    print "    LOG_NUM_LOGS\n};\n" > of

    # enum LOG_menu index

    printf "enum LOG_menu_index\n{\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
        {
            printf "    LOG_MENU_%s,\n", log_menu_name[log_idx, menu_idx] > of

            for(i = 2 ; i <= value["$REPEAT",log_idx] ; i++)
            {
                printf "    LOG_MENU_%s,\n", log_menu_name[log_idx, menu_idx] i > of
            }
        }
    }

    for(ext_menu_idx = 1 ; ext_menu_idx <= num_ext_menus ; ext_menu_idx++)
    {
        printf "    LOG_MENU_%s,\n", ext_menu[ext_menu_idx,"$EXT_MENU"] > of
    }

    print  "    LOG_NUM_MENUS," > of

    if(num_ext_menus > 0)
    {
        printf "    LOG_NUM_LIBLOG_MENUS = LOG_MENU_%s\n};\n\n",ext_menu[1,"$EXT_MENU"] > of
    }
    else
    {
        print "    LOG_NUM_LIBLOG_MENUS = LOG_NUM_MENUS};\n" > of
    }

    # enums LOG_{log_name}_sig_index

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        printf "enum LOG_%s_sig_index\n{\n", tolower(value["$LOG_NAME",log_idx]) > of

        for(sig_idx = 1 ; sig_idx <= num_signals[log_idx] ; sig_idx++)
        {
            printf "    LOG_%s,\n", value["$LOG_NAME",log_idx] "_" SigName(log_idx,sig_idx) > of
        }

        printf "    LOG_%s,\n", value["$LOG_NAME",log_idx] "_NUM_SIGNALS" > of

        print "};\n" > of

        printf "enum LOG_%s_log_sig_index\n{\n", tolower(value["$LOG_NAME",log_idx]) > of

        for(sig_idx = 1 ; sig_idx <= num_signals[log_idx] ; sig_idx++)
        {
            printf "    LOG_SIG_IDX_%-25s = 0x%04X,\n",
                    value["$LOG_NAME",log_idx] "_" SigName(log_idx,sig_idx),
                    (log_idx - 1) * 256 + (sig_idx - 1) > of
        }
        printf "    LOG_SIG_IDX_%-25s = %u,\n",
                    value["$LOG_NAME",log_idx] "_NUM_SIGNALS",
                    (sig_idx - 1) > of

        print "};\n" > of
    }

    # LOG_menus structure

    print "// Log menus structure - structure of arrays to be compatible with FGC properties\n" > of

    print "struct LOG_menus" > of
    print "{" > of
    print "    struct LOG_mgr    * log_mgr;" > of
    print "    uint32_t            max_buffer_size;                     // Filled in by liblog from log.ods" > of
    print "    char        const * names              [LOG_NUM_MENUS];" > of
    print "    char        const * properties         [LOG_NUM_MENUS];" > of
    print "    uint32_t            log_index          [LOG_NUM_MENUS];" > of
    print "    float               period             [LOG_NUM_MENUS];" > of
    print "    uint32_t            sig_sel_bit_mask   [LOG_NUM_MENUS];" > of
    print "    uint32_t            pm_buf_sel_bit_mask[LOG_NUM_MENUS];" > of
    print "    uint32_t            status_bit_mask    [LOG_NUM_MENUS];" > of
    print "    uint32_t            init_postmortem    [LOG_NUM_MENUS];" > of
    print "    uint32_t            ctrl_postmortem    [LOG_NUM_MENUS];" > of
    print "};\n" > of

    # Static inline functions

    print "// Static inline functions\n" > of

    print "//! Return log index from a log menu index" > of
    print "//!" > of
    print "//! @param[in]      log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu\n" > of

    print "static inline uint32_t logMenuIndexToLogIndex(struct LOG_menus const * const log_menus," > of
    print "                                              uint32_t                 const menu_index)" > of
    print "{" > of
    print "    return log_menus->log_index[menu_index];" > of
    print "}\n\n" > of

    print "//! Test if a log menu contributes to the postmortem data" > of
    print "//!" > of
    print "//! @param[in]      log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu to test\n" > of

    print "static inline bool logMenuIsInPostmortem(struct LOG_menus const * const log_menus," > of
    print "                                         uint32_t                 const menu_index)" > of
    print "{" > of
    print "    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_POSTMORTEM_BIT_MASK) != 0;" > of
    print "}\n\n" > of

    print "//! Test if a log menu is multiplexed" > of
    print "//!" > of
    print "//! @param[in]      log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu to test\n" > of

    print "static inline bool logMenuIsMpx(struct LOG_menus const * const log_menus," > of
    print "                                uint32_t                 const menu_index)" > of
    print "{" > of
    print "    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_MPX_BIT_MASK) != 0;" > of
    print "}\n\n" > of

    print "//! Test if a log menu is in the PM_BUF" > of
    print "//!" > of
    print "//! @param[in]      log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu to test\n" > of

    print "static inline bool logMenuIsInPmBuf(struct LOG_menus const * const log_menus," > of
    print "                                    uint32_t                 const menu_index)" > of
    print "{" > of
    print "    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_PM_BUF_BIT_MASK) != 0;" > of
    print "}\n\n" > of

    print "//! Prepare read_request structure to read out signals for a log menu" > of
    print "//!" > of
    print "//! Before calling logReadRequest(), the application must prepare the read_request structure." > of
    print "//! If the application wants to read out the signals for a log menu, then this function can" > of
    print "//! be used." > of
    print "//!" > of
    print "//! @param[in]    log_menus             Pointer to LOG_menus structure" > of
    print "//! @param[in]    menu_index            Index of menu whose signals should be read out" > of
    print "//! @param[in]    action                LOG_READ_GET, LOG_READ_GET_PM_BUF or LOG_READ_GET_SPY" > of
    print "//! @param[in]    cyc_sel               Cycle selector" > of
    print "//! @param[in]    timing                Pointer to LOG_read_timing structure" > of
    print "//! @param[in]    sub_sampling_period   Sub-sampling period in units of log periods  (zero = automatic)" > of
    print "//! @param[out]   read_request          Pointer to read_request structure to initialize\n" > of

    print "static inline void logPrepareReadMenuRequest(struct LOG_menus   const * const log_menus," > of
    print "                                             uint32_t                   const menu_index," > of
    print "                                             enum LOG_read_action const action," > of
    print "                                             uint32_t                   const cyc_sel," > of
    print "                                             struct LOG_read_timing   * const timing," > of
    print "                                             uint32_t                   const sub_sampling_period," > of
    print "                                             struct LOG_read_request  * const read_request)" > of
    print "{" > of
    print "    read_request->log_index = log_menus->log_index[menu_index];" > of
    print "    read_request->action    = action;\n" > of
    print "    read_request->u.get.cyc_sel             = cyc_sel;" > of
    print "    read_request->u.get.timing              = *timing;" > of
    print "    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;" > of
    print "    read_request->u.get.sub_sampling_period = sub_sampling_period;\n" > of

    print "    if((log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_ANALOG_BIT_MASK) != 0)" > of
    print "    {" > of
    print "        read_request->u.get.selectors_bit_mask = log_menus->sig_sel_bit_mask[menu_index];" > of
    print "        read_request->u.get.signals_bit_mask   = 0;" > of
    print "    }" > of
    print "    else" > of
    print "    {" > of
    print "        read_request->u.get.selectors_bit_mask = 0;" > of
    print "        read_request->u.get.signals_bit_mask   = log_menus->sig_sel_bit_mask[menu_index];" > of
    print "    }" > of
    print "}\n\n" > of

    print "//! Prepare read_request structure to read out all possible or the actual signal names for a menu" > of
    print "//!" > of
    print "//! Before calling logReadRequest(), the application must prepare the read_request structure." > of
    print "//! If the application wants to read out the list of signals names for a log menu, then this function can" > of
    print "//! be used." > of
    print "//!" > of
    print "//! The response will be in the header only - no log data is provided. Two get requests are supported:" > of
    print "//!" > of
    print "//! LOG_READ_GET_SIG_NAMES - Prepare read_request to return the actual signal names, separated by spaces" > of
    print "//!" > of
    print "//! LOG_READ_GET_ALL_SIG_NAMES - Only if the menu is multiplexed, the function will prepare the" > of
    print "//!                              read_request to return all possible signal names, otherwise the" > of
    print "//!                              function returns false and the application should return an empty string"> of
    print "//!" > of
    print "//! @param[in]    log_menus       Pointer to log_menus structure" > of
    print "//! @param[in]    menu_index      Index of menu whose signals should be read out" > of
    print "//! @param[in]    action          LOG_READ_GET_SIG_NAMES or LOG_READ_GET_ALL_SIG_NAMES" > of
    print "//! @param[out]   read_request    Pointer to read_request structure to initialize" > of
    print "//! @retval       true            read_request is ready to use" > of
    print "//! @retval       false           application should return an empty string\n" > of

    print "static inline bool logPrepareReadMenuSigNamesRequest(struct LOG_menus  const * const log_menus," > of
    print "                                                     uint32_t                  const menu_index," > of
    print "                                                     enum LOG_read_action      const action," > of
    print "                                                     struct LOG_read_request * const read_request)" > of
    print "{" > of
    print "    if(action == LOG_READ_GET_SIG_NAMES ||" > of
    print "      (action == LOG_READ_GET_ALL_SIG_NAMES && logMenuIsMpx(log_menus,menu_index)))" > of
    print "    {" > of
    print "        // Generate read_request to retrieve all or actual signal names" > of
    print "" > of
    print "        struct LOG_read_timing timing = { { { 0 } } };" > of
    print "" > of
    print "        logPrepareReadMenuRequest(log_menus," > of
    print "                                  menu_index," > of
    print "                                  action," > of
    print "                                  0,              // cyc_sel" > of
    print "                                  &timing," > of
    print "                                  0,              // sub_sampling_period" > of
    print "                                  read_request);" > of
    print "" > of
    print "        return true;" > of
    print "    }" > of
    print "" > of
    print "    // read_request not generated so return false - the application should return an empty string" > of
    print "" > of
    print "    return false;" > of
    print "}\n\n" > of

    print "//! Prepare read_request structure to read out one signal from a specified log" > of
    print "//!" > of
    print "//! Before calling logReadRequest(), the application must prepare the read_request structure." > of
    print "//! If the application wants to read out one signal from a specified log, then this function can" > of
    print "//! be used." > of
    print "//!" > of
    print "//! @param[in]    log_menus             Pointer to LOG_menus structure" > of
    print "//! @param[in]    log_sig_index         Combined indexes of log and signal 0xLLSS to be read out" > of
    print "//! @param[in]    action                LOG_READ_GET, LOG_READ_GET_PM_BUF or LOG_READ_GET_SPY" > of
    print "//! @param[in]    cyc_sel               Cycle selector" > of
    print "//! @param[in]    timing                Pointer to LOG_read_timing structure" > of
    print "//! @param[in]    sub_sampling_period   Sub-sampling period in units of log periods (zero = automatic)" > of
    print "//! @param[out]   read_request          Pointer to read_request structure to initialize\n" > of

    print "static inline void logPrepareReadSignalRequest(struct LOG_menus  const * const log_menus," > of
    print "                                               uint32_t                  const log_sig_index," > of
    print "                                               enum LOG_read_action      const action," > of
    print "                                               uint32_t                  const cyc_sel," > of
    print "                                               struct LOG_read_timing  * const timing," > of
    print "                                               uint32_t                  const sub_sampling_period," > of
    print "                                               struct LOG_read_request * const read_request)" > of
    print "{" > of
    print "    read_request->log_index = log_sig_index >> 8;" > of
    print "    read_request->action    = action;\n" > of
    print "    read_request->u.get.cyc_sel             = cyc_sel;" > of
    print "    read_request->u.get.timing              = *timing;" > of
    print "    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;" > of
    print "    read_request->u.get.sub_sampling_period = sub_sampling_period;" > of
    print "    read_request->u.get.selectors_bit_mask  = 0;" > of
    print "    read_request->u.get.signals_bit_mask    = (1 << (log_sig_index & 0xFF));" > of
    print "}\n\n" > of

    print "//! Prepare read_request structure to read out the PM_BUF signals for a log menu" > of
    print "//!" > of
    print "//! Before calling logReadRequest(), the application must prepare the read_request structure." > of
    print "//! If the application wants to read out the PM_BUF signals for a log menu, then this function can" > of
    print "//! be used." > of
    print "//!" > of
    print "//! @param[in]    log_menus             Pointer to LOG_menus structure" > of
    print "//! @param[in]    menu_index            Index of menu whose signals should be read out" > of
    print "//! @param[in]    timing                Pointer to LOG_read_timing structure" > of
    print "//! @param[out]   read_request          Pointer to read_request structure to initialize\n" > of

    print "static inline void logPrepareReadPmBufRequest(struct LOG_menus  const * const log_menus," > of
    print "                                              uint32_t                  const menu_index," > of
    print "                                              struct LOG_read_timing  * const timing," > of
    print "                                              struct LOG_read_request * const read_request)" > of
    print "{" > of
    print "    read_request->log_index = log_menus->log_index[menu_index];" > of
    print "    read_request->action    = LOG_READ_GET_PM_BUF;\n" > of
    print "    read_request->u.get.cyc_sel             = 0;" > of
    print "    read_request->u.get.timing              = *timing;" > of
    print "    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;" > of
    print "    read_request->u.get.sub_sampling_period = 1;" > of
    print "    read_request->u.get.selectors_bit_mask  = log_menus->pm_buf_sel_bit_mask[menu_index];" > of
    print "    read_request->u.get.signals_bit_mask    = 0;" > of
    print "}\n\n" > of

    print "//! Prepare read_request structure to change a signal's name and/or units." > of
    print "//!" > of
    print "//! Before calling logReadRequest(), the application must prepare the read_request structure." > of
    print "//! If the application wants to change a signal name or units, then this function" > of
    print "//! shoud be used. If sig_name or units are a nul string then the previous name or units" > of
    print "//! are not changed. If the name or units strings are too long, they will be truncated." > of
    print "//!" > of
    print "//! @param[in]    log_menus             Pointer to LOG_menus structure" > of
    print "//! @param[in]    log_index             Index of the log containing the signal" > of
    print "//! @param[in]    sig_index             Index of the signal within the log" > of
    print "//! @param[in]    sig_name              Pointer to the new signal name." > of
    print "//! @param[in]    units                 Pointer to the new units." > of
    print "//! @param[out]   read_request          Pointer to read_request structure to initialize\n" > of

    print "static inline void logPrepareStoreSigNameAndUnitsRequest(struct LOG_menus  const * const log_menus," > of
    print "                                                         uint32_t                  const log_index," > of
    print "                                                         uint32_t                  const sig_index," > of
    print "                                                         char                    * const sig_name," > of
    print "                                                         char                    * const units," > of
    print "                                                         struct LOG_read_request * const read_request)" > of
    print "{" > of
    print "    read_request->log_index = log_index;" > of
    print "    read_request->action = LOG_READ_STORE_SIG_NAMES_AND_UNITS;" > of
    print "    read_request->u.set.sig_index = sig_index;\n" > of
    print "    strncpy(read_request->u.set.sig_name, sig_name, LOG_SIG_NAME_LEN-1);" > of
    print "    strncpy(read_request->u.set.units,    units,    LOG_UNITS_LEN-1);\n" > of
    print "    read_request->u.set.sig_name[LOG_SIG_NAME_LEN-1] = '\\0';" > of
    print "    read_request->u.set.units   [LOG_UNITS_LEN-1]    = '\\0';" > of
    print "}\n\n" > of

    print "//! Change a log menu name pointer" > of
    print "//!" > of
    print "//! The application can replace the automatically generated log menu name string pointer" > of
    print "//! using this function. Note that the menu name string is not changed, only the pointer" > of
    print "//! to the name." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu whose name pointer should be replaced" > of
    print "//! @param[in]      menu_name           Pointer to new menu name string\n" > of

    print "static inline void logChangeMenuName(struct LOG_menus * const log_menus," > of
    print "                                     uint32_t           const menu_index," > of
    print "                                     char       const * const menu_name)" > of
    print "{" > of
    print "    log_menus->names[menu_index] = menu_name;" > of
    print "}\n\n" > of

    print "//! Get the log menu signals selector mask" > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu whose name pointer should be replaced" > of
    print "//! @retval         sig_sel_bit_mask for the log menu\n" > of

    print "static inline uint32_t logGetMenuSigSelMask(struct LOG_menus * const log_menus," > of
    print "                                            uint32_t           const menu_index)" > of
    print "{" > of
    print "    return log_menus->sig_sel_bit_mask[menu_index];" > of
    print "}\n\n" > of

    print "//! Modify a log menu name signals selector mask" > of
    print "//!" > of
    print "//! The application can modify the signals selector mask for a log menu using this function." > of
    print "//! The mask will be anded with the existing mask, so it is not possible to set bits that are" > of
    print "//! currently zero." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to log_menus structure" > of
    print "//! @param[in]      menu_index          Index of menu whose name pointer should be replaced" > of
    print "//! @param[in]      sig_sel_bit_mask    Signal/selector bit mask for the menu\n" > of

    print "static inline void logModifyMenuSigSelMask(struct LOG_menus * const log_menus," > of
    print "                                           uint32_t           const menu_index," > of
    print "                                           uint32_t           const sig_sel_bit_mask)" > of
    print "{" > of
    print "    log_menus->sig_sel_bit_mask[menu_index] &= sig_sel_bit_mask;" > of
    print "}\n\n" > of

    print "//! Update log menu status bit masks to reflect running and disabled statuses of the associated log" > of
    print "//! and transfer the sampling periods from log_mgr to log_menus." > of
    print "//!" > of
    print "//! The log menu bit masks have some static bits configured from the log definition spreadsheet." > of
    print "//! It also has two dynamic bits: RUNNING and DISABLED. This function will set/clear the RUNNING bit" > of
    print "//! based on the same bit for log that the log menu is associated with." > of
    print "//!" > of
    print "//! The DISABLED bit will be set if the log that the log menu is associated with is DISABLED. However," > of
    print "//! if the log is re-enabled, the log menu will not be automatically re-enabled, since the application" > of
    print "//! can choose to disable a log menu while other log menus associated with the same log remain enabled." > of
    print "//!" > of
    print "//! The function will also transfer the sampling period (s+ns) from log_mgr to the log menu float period (s)" > of
    print "//!" > of
    print "//! @param[in]      log_mgr             Pointer to the log_mgr structure" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure\n" > of

    print "static inline void logMenuUpdate(struct LOG_mgr   * const log_mgr," > of
    print "                                 struct LOG_menus * const log_menus)" > of
    print "{" > of
    print "    uint32_t const running_logs_mask  = log_mgr->running_logs_mask;" > of
    print "    uint32_t const disabled_logs_mask = log_mgr->disabled_logs_mask;" > of
    print "    uint32_t       menu_index;\n" > of

    print "    for(menu_index = 0 ; menu_index < LOG_NUM_LIBLOG_MENUS ; menu_index++)" > of
    print "    {" > of
    print "        uint32_t const log_index    = log_menus->log_index[menu_index];" > of
    print "        uint32_t const log_bit_mask = (1 << log_index);\n" > of

    print "        log_menus->period[menu_index] = (float)log_mgr->period[log_index].secs.rel" > of
    print "                                      + 1.0E-9 * (float)log_mgr->period[log_index].ns;\n" > of

    print "        if(log_menus->init_postmortem[menu_index] != 0 || log_menus->ctrl_postmortem[menu_index] != 0)" > of
    print "        {" > of
    print "            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;" > of
    print "        }" > of
    print "        else" > of
    print "        {" > of
    print "            log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;" > of
    print "        }\n" > of

    print "        if((running_logs_mask & log_bit_mask) != 0)" > of
    print "        {" > of
    print "            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_RUNNING_BIT_MASK;" > of
    print "        }" > of
    print "        else" > of
    print "        {" > of
    print "            log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_RUNNING_BIT_MASK;" > of
    print "        }\n" > of

    print "        if((disabled_logs_mask & log_bit_mask) != 0)" > of
    print "        {" > of
    print "            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_DISABLED_BIT_MASK;" > of
    print "        }" > of
    print "    }" > of
    print "}\n\n" > of

    print "//! Enable a log menu by clearing the DISABLED status bit" > of
    print "//!" > of
    print "//! This function will clear the DISABLED log menu status bit for the specified log menu." > of
    print "//! This does not guarantee that the associated log is actually enabled. That can be done with the" > of
    print "//! logEnable() function from logClient.h." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu to enable\n" > of

    print "static inline void logMenuEnable(struct LOG_menus * const log_menus," > of
    print "                                 uint32_t           const menu_index)" > of
    print "{" > of
    print "     log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_DISABLED_BIT_MASK;" > of
    print "}\n\n" > of

    print "//! Disable a log menu by setting the DISABLED status bit" > of
    print "//!" > of
    print "//! This function will set the DISABLED log menu status bit for the specified log menu." > of
    print "//! This does not automatically disable the associated log. That can be done with the" > of
    print "//! logDisable() function from logClient.h." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu to disable\n" > of

    print "static inline void logMenuDisable(struct LOG_menus * const log_menus," > of
    print "                                  uint32_t           const menu_index)" > of
    print "{" > of
    print "     log_menus->status_bit_mask[menu_index] |= LOG_MENU_STATUS_DISABLED_BIT_MASK;" > of
    print "}\n\n" > of

    print "//! Test if a log menu is DISABLED" > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu to verify" > of
    print "//! @retval         true                Log menu is DISABLED" > of
    print "//! @retval         false               Log menu is ENABLED\n" > of

    print "static inline bool logMenuIsDisabled(struct LOG_menus * const log_menus," > of
    print "                                     uint32_t           const menu_index)" > of
    print "{" > of
    print "     return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_DISABLED_BIT_MASK) != 0;" > of
    print "}\n\n" > of

    print "//! Test if a log menu is ALIAS" > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu to verify" > of
    print "//! @retval         true                Log menu is ALIAS" > of
    print "//! @retval         false               Log menu is not ALIAS\n" > of

    print "static inline bool logMenuIsAlias(struct LOG_menus * const log_menus," > of
    print "                                  uint32_t           const menu_index)" > of
    print "{" > of
    print "     return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_ALIAS_BIT_MASK) != 0;" > of
    print "}\n\n" > of

    print "//! Test if the log menu has to be saved. This is the case for all PM and DIM logs that" > of
    print "//! are not DISABLED" > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu to test" > of
    print "//! @retval         true                Log menu needs to be SAVE" > of
    print "//! @retval         false               Log menu does not need to be SAVE\n" > of

    print "static inline bool logMenuIsSave(struct LOG_menus * const log_menus," > of
    print "                                 uint32_t           const menu_index)" > of
    print "{" > of
    print "    return    logMenuIsDisabled(log_menus, menu_index) == false" > of
    print "           && (log_menus->status_bit_mask[menu_index] & (LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK)) != 0; " > of
    print "}\n\n" > of

    print "//! Set bit(s) in the log menu status" > of
    print "//!" > of
    print "//! This function will OR the supplied bit mask into the status mask for the specified log menu." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu" > of
    print "//! @param[in]      bit_mask            Bit mask to OR into log menu status\n" > of

    print "static inline void logMenuStatusSetBitMask(struct LOG_menus * const log_menus," > of
    print "                                           uint32_t           const menu_index," > of
    print "                                           uint32_t           const bit_mask)" > of
    print "{" > of
    print "     log_menus->status_bit_mask[menu_index] |= bit_mask;" > of
    print "}\n\n" > of

    print "//! Reset bit(s) in the log menu status" > of
    print "//!" > of
    print "//! This function will AND the complement of the supplied bit mask into the status mask for the" > of
    print "//! specified log menu." > of
    print "//!" > of
    print "//! @param[in,out]  log_menus           Pointer to the log_menus structure" > of
    print "//! @param[in]      menu_index          Index of log menu" > of
    print "//! @param[in]      bit_mask            Bit mask to complement and AND into log menu status\n" > of

    print "static inline void logMenuStatusResetBitMask(struct LOG_menus * const log_menus," > of
    print "                                             uint32_t           const menu_index," > of
    print "                                             uint32_t           const bit_mask)" > of
    print "{" > of
    print "     log_menus->status_bit_mask[menu_index] &= ~bit_mask;" > of
    print "}\n" > of

    print "// EOF" > of

    close(of)
}



function WriteLogMenusInit(of,  log_idx, sig_idx)
{
    WriteGnuLicense("liblog", "Logging", "Menu structures initialization header file", of)

    print "//! Initialize log structures per device" > of
    print "//!" > of
    print "//! @param[in]      log_mgr           Pointer to log manager structures" > of
    print "//! @param[in,out]  log_menus         Pointer to log menus structures to initialize" > of
    print "//! @param[in]      max_buffer_size   Max readout buffer size (header+data). Zero for no limit.\n" > of

    print "static inline void logMenusInit(struct LOG_mgr * const log_mgr, struct LOG_menus * const log_menus, uint32_t const max_buffer_size)" > of
    print "{" > of
    print "    log_menus->log_mgr         = log_mgr;" > of
    print "    log_menus->max_buffer_size = max_buffer_size;" > of

    global_menu_index = 0

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
        {
            WriteMenuInitialization(of, log_idx, menu_idx, global_menu_index++)

            for(repeat_idx = 2 ; repeat_idx <= value["$REPEAT",log_idx] ; repeat_idx++)
            {
                WriteMenuInitialization(of, log_idx, menu_idx, global_menu_index++, repeat_idx)
            }
        }
    }

    for(ext_menu_idx = 1 ; ext_menu_idx <= num_ext_menus ; ext_menu_idx++)
    {
        menu_name = ext_menu[ext_menu_idx, "$EXT_MENU"]
        menu_prop = ext_menu[ext_menu_idx, "$EXT_MENU_PROPERTY"]
        sub(/menu_index/, global_menu_index++, menu_prop)

        printf "\n    log_menus->names           [LOG_MENU_%-25s = \"%s\";\n", menu_name "]", menu_name > of
        printf   "    log_menus->properties      [LOG_MENU_%-25s = \"%s\";\n", menu_name "]", menu_prop > of
        printf   "    log_menus->status_bit_mask [LOG_MENU_%-25s = %s;\n",     menu_name "]", ext_menu[ext_menu_idx, "$EXT_MENU_FLAGS"] > of
    }

    print "\n// Initialize log menu status and period from log_mgr\n" > of

    print "    logMenuUpdate(log_mgr, log_menus);" > of
    print "}\n" > of

    print "// EOF" > of

    close(of)
}



function WriteMenuInitialization(of, log_idx, menu_idx, global_menu_index, repeat_idx,  menu_name, log_name, prop, is_in_postmortem, menu_prop)
{
    # Prepare log menu status bit mask

    menu_status_bit_mask = "0"
    init_postmortem      = 0

    if(log_is_cyclic[log_idx])
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_CYCLIC_BIT_MASK"
    }

    if(log_is_analog[log_idx])
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_ANALOG_BIT_MASK"
    }

    if(log_menu_start_sig_sel_index[log_idx,menu_idx] == 0 && log_is_postmortem[log_idx])
    {
        init_postmortem = 1
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK"
    }

    if(log_is_continuous[log_idx])
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK"
    }

    if(log_is_freezable[log_idx])
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_FREEZABLE_BIT_MASK"
    }

    if(log_menu_type[log_idx, menu_idx] == "MPX")
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_MPX_BIT_MASK"
    }

    if(log_menu_pm_buf_sel_bit_mask[log_idx, menu_idx] > 0)
    {
        menu_status_bit_mask = menu_status_bit_mask " | LOG_MENU_STATUS_PM_BUF_BIT_MASK"
    }

    menu_status_bit_mask = menu_status_bit_mask value["$LOG_STATUS_FLAGS", log_idx]

    # Print log menu initialization code

    menu_name = (log_menu_name[log_idx, menu_idx] repeat_idx)
    menu_prop = value["$LOG_MENU_LIBLOG"]
    sub(/menu_index/, global_menu_index, menu_prop)

    printf "\n" > of
    printf "    log_menus->names              [LOG_MENU_%-25s = \"%s\";\n", menu_name "]", menu_name > of
    printf "    log_menus->properties         [LOG_MENU_%-25s = \"%s\";\n", menu_name "]", menu_prop > of
    printf "    log_menus->log_index          [LOG_MENU_%-25s = LOG_%s;\n", menu_name "]", value["$LOG_NAME",log_idx] repeat_idx > of
    printf "    log_menus->sig_sel_bit_mask   [LOG_MENU_%-25s = 0x%08X;\n", menu_name "]", log_menu_sig_sel_bit_mask[log_idx, menu_idx] > of
    printf "    log_menus->pm_buf_sel_bit_mask[LOG_MENU_%-25s = 0x%08X;\n", menu_name "]", log_menu_pm_buf_sel_bit_mask[log_idx, menu_idx] > of
    printf "    log_menus->status_bit_mask    [LOG_MENU_%-25s = %s;\n",     menu_name "]", menu_status_bit_mask > of
    printf "    log_menus->init_postmortem    [LOG_MENU_%-25s = %d;\n",     menu_name "]", init_postmortem > of
}



function WriteLogStructs(of,  log_idx, sig_idx)
{
    WriteGnuLicense("liblog", "Logging", "Structures header file", of)

    print "#pragma once\n" > of

    print "// Include generated logMenus.h header file to have access to all the log specific constants\n" > of

    print "#include \"logMenus.h\"" > of
    print "#include \"liblog/logStore.h\"\n" > of

    # Log buffers structure - includes log buffers and private log data

    print "// Declare LOG_buffers structure dedicated to the application - can be instantiated in slow RAM\n" > of

    print "struct LOG_buffers" > of
    print "{" > of
    print "    struct LOG_private priv   [LOG_NUM_LOGS];" > of
    print "    uint32_t           buffers[LOG_BUFFERS_LEN];" > of

    # Structures for each log

    log_idx = 1

    for(i = 1 ; i <= num_logs ; i++)
    {
        printf "\n    // Log %u ", log_idx > of

        if(value["$REPEAT",i] > 1)
        {
            printf "- %u ", log_idx + value["$REPEAT",i] - 1 > of
        }

        printf " : %s - %s\n\n", value["$LOG_NAME",i], value["$LOG_TITLE",i] > of
        printf "    struct LOG_%s_structs\n    {\n",  tolower(value["$LOG_NAME",i]) > of

        alias_length  = log_is_alias[log_idx] == 0 ? value[("$REPEAT"),i] : 1
        repeat_length = value[("$REPEAT"),i]

        printf "        struct LOG_cycles          cycles   [%u][LOG_%s_NUM_CYCLES];\n" , alias_length, value["$LOG_NAME",i] > of

        if(log_is_analog[i])
        {
            printf "        struct LOG_priv_ana_signal signals  [%u][LOG_%s_NUM_SIGNALS];\n" ,repeat_length, value["$LOG_NAME",i] > of
            printf "        uint32_t                   selectors[%u][LOG_%s_NUM_SIG_BUFS];\n",alias_length , value["$LOG_NAME",i] > of
        }
        else
        {
            printf "        struct LOG_priv_dig_signal signals  [%u][LOG_%s_NUM_SIGNALS];\n",alias_length, value["$LOG_NAME",i] > of
        }

        printf "    } %s;\n", tolower(value["$LOG_NAME",i]) > of

        log_idx += value["$REPEAT",i]
    }

    print "};\n" > of

    # Log structs structure - includes LOG_log structures array and selector pointers

    print "// Declare LOG_structs structure dedicated to the application - should be in fast RAM\n" > of

    print "struct LOG_structs" > of
    print "{" > of
    print "    // Log structures array\n" > of
    print "    struct LOG_log  log[LOG_NUM_LOGS];\n" > of
    print "    // Start cycle structure\n" > of
    print "    struct LOG_start_cycle start_cycle;" > of

    # struct LOG_structs structure

    log_idx = 1

    for(i = 1 ; i <= num_logs ; i++)
    {
        printf "\n    // Log %u ", log_idx > of

        if(value["$REPEAT",i] > 1)
        {
            printf "- %u ", log_idx + value["$REPEAT",i] - 1 > of
        }

        printf " : %s - %s\n\n", value["$LOG_NAME",i], value["$LOG_TITLE",i] > of
        printf "    struct LOG_rt_%s_structs\n    {\n",  tolower(value["$LOG_NAME",i]) > of

        if(log_is_analog[i])
        {
            printf "        cc_float            * selector_ptrs[LOG_%s_NUM_SIG_BUFS];\n", value["$LOG_NAME",i] > of
        }
        else
        {
            printf "        struct LOG_dig_signal signals[LOG_%s_NUM_SIGNALS];\n", value["$LOG_NAME",i] > of
        }


        array_length  = log_is_alias[log_idx] == 0 ? value[("$REPEAT"),i] : 1

        printf "    } %s[%u];\n", tolower(value["$LOG_NAME",i]), array_length > of

        log_idx += value["$REPEAT",i]
    }

    print "};\n" > of

    # Define static inline function to cache changes to selectors that are multiplexers

    print "//! Cache selector pointers in case a selector has changed" > of
    print "//!" > of
    print "//! This should be called if a log menu multiplexer has been changed. It will cached the" > of
    print "//! pointers to the signal variables, which allows the storing of signals to be efficient." > of
    print "//!" > of
    print "//! @param[in,out]  log_structs         Pointer to log_structs structure\n" > of

    print "static inline void logCacheSelectors(struct LOG_structs * log_structs)\n{" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_multiplexed[log_idx])
        {
            printf "    logStoreAnalogSelector(&log_structs->log[LOG_%s]);\n", value["$LOG_NAME",log_idx] > of

            for(i = 2 ; i <= value["$REPEAT",log_idx] ; i++)
            {
                printf "    logStoreAnalogSelector(&log_structs->log[LOG_%s%u]);\n", value["$LOG_NAME",log_idx], i > of
            }
        }
    }

    print "}\n\n" > of

    # Define static inline function to clear freezable postmortem logs

    print "//! Clear freezable postmortem logs" > of
    print "//!" > of
    print "//! This can be called in the background to reset all freezable postmortem logs before unfreezing." > of
    print "//!" > of
    print "//! @param[in,out]  log_buffers         Pointer to log_buffers structure\n" > of

    print "static inline void logClearFreezablePostmortemLogs(struct LOG_buffers * const log_buffers)\n{" > of

    global_log_idx = 1
    buf_offset     = 0

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        # Only take into account the first log mneu for an ALIAS log

        repeat_idx_max = log_is_alias[log_idx] ? 1 : value["$REPEAT",log_idx]

        for(repeat_idx = 1 ; repeat_idx <= repeat_idx_max; repeat_idx++)
        {
            if(log_is_postmortem[log_idx] && log_is_continuous[log_idx] && log_is_freezable[log_idx])
            {
                printf "    memset(&log_buffers->buffers[%8u], 0,%9u);   // Clear log %s",\
                       buf_offset, log_is_alias[log_idx] ? 0 :  4 * buf_len[log_idx], value["$LOG_NAME",log_idx] > of

                if(repeat_idx > 1)
                {
                    printf "%u\n", repeat_idx > of
                }
                else
                {
                    printf "\n" > of
                }
            }

            buf_offset += buf_len[log_idx]
        }
    }
    print "}\n" > of
    print "// EOF" > of

    close(of)
}



function WriteLogStructsInit(of,  log_idx, sig_idx, read_rate)
{
    WriteGnuLicense("liblog", "Logging", "Structures initialization header file", of)

    print "//! This value is used as an analog signal source when it's marked as NULL in the definition file.\n" > of

    print "static cc_float const dummy_zero_float_value = 0.0F;\n" > of

    print "//! This value is used as a digital signal source when it's marked as NULL in the definition file.\n" > of

    print "static bool const dummy_zero_bool_value = false;\n" > of

    # Define the top-level static array of pointers to character strings for the log signal names and units

    WriteLogSigNamesInitialization(of)

    # Define logStructsInitDevice()

    print "//! Initialize log structures per device" > of
    print "//!" > of
    print "//! @param[in]    log_mgr        Pointer to log manager structure to initialize" > of
    print "//! @param[in]    log_structs    Pointer to log structs structure to initialize" > of
    print "//! @param[in]    log_buffers    Pointer to log buffers structure" > of
    print "//! @param[in]    device_index   Hook for calling application, to allow initialization of multiple devices\n" > of

    print "static void logStructsInitDevice(struct LOG_mgr     * const log_mgr," > of
    print "                                 struct LOG_structs * const log_structs," > of
    print "                                 struct LOG_buffers * const log_buffers," > of
    print "                                 uint32_t             const device_index)" > of
    print "{" > of

    # Reset log structures using memset

    print "    // Reset log structures\n" > of

    print "    memset(log_mgr,     0, sizeof(*log_mgr));" > of
    print "    memset(log_structs, 0, sizeof(*log_structs));" > of
    print "    memset(log_buffers, 0, sizeof(*log_buffers));\n" > of

    # Initialize the log manager structure

    printf "    // Initialize log_mgr structure\n\n" > of
    printf "    log_mgr->num_logs             = %d;\n",     total_num_logs > of
    printf "    log_mgr->read_delay_ms        = %d;\n",     value["$READ_DELAY_MS"] > of

    if(value["$READ_RATE"] > 0 && value["$READ_RATE"] < 4294967295)
    {
        read_rate = value["$READ_RATE"]
    }
    else
    {
        read_rate = 4294967295
    }

    printf "    log_mgr->read_rate            = %d;\n",       read_rate > of
    printf "    log_mgr->cyclic_logs_mask     = 0x%08X;\n",   cyclic_logs_mask > of
    printf "    log_mgr->analog_logs_mask     = 0x%08X;\n",   analog_logs_mask > of
    printf "    log_mgr->postmortem_logs_mask = 0x%08X;\n",   postmortem_logs_mask > of
    printf "    log_mgr->continuous_logs_mask = 0x%08X;\n",   continuous_logs_mask > of
    printf "    log_mgr->freezable_logs_mask  = 0x%08X;\n",   freezable_logs_mask > of
    printf "    log_mgr->running_logs_mask    = 0x%08X;\n\n", freezable_logs_mask > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        period_ns = value["$ITER_PERIOD_NS"] * value["$DEF_PERIOD_ITERS", log_idx]
        period_secs = int(period_ns * 1.0E-9)
        period_ns -= period_secs * 1000000000

        printf "    log_mgr->period[LOG_%-12s].secs.rel = %d;\n", value["$LOG_NAME",log_idx], period_secs > of
        printf "    log_mgr->period[LOG_%-12s].ns       = %u;\n", value["$LOG_NAME",log_idx], period_ns > of

        for(i = 2 ; i <= value["$REPEAT",log_idx] ; i++)
        {
            printf "    log_mgr->period[LOG_%-12s].secs.rel = %d;\n", value["$LOG_NAME",log_idx] i, period_secs > of
            printf "    log_mgr->period[LOG_%-12s].ns       = %u;\n", value["$LOG_NAME",log_idx] i, period_ns > of
        }
    }

    # Initialize the log structs structure

    printf "\n\n    // Initialize log structure array\n\n" > of

    printf "    struct LOG_log     * log  = log_structs->log;\n" > of
    printf "    struct LOG_private * priv = log_buffers->priv;\n" > of

    global_log_idx = 1
    buf_offset     = 0

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(repeat_idx = 1 ; repeat_idx <= value["$REPEAT",log_idx] ; repeat_idx++)
        {
            WriteLogInitialization(of, global_log_idx++, log_idx, repeat_idx )
        }
    }

    print "\n    // Initialize signal names and units\n" > of

    print "    logStoreAllSigNamesAndUnits(log_structs->log, log_sig_names_and_units);" > of
    print "}\n\n" > of

    # Generate static inline for mono-device classes

    print "//! Initialize log structures" > of
    print "//!" > of
    print "//! Wrapper around logStructsInitDevice() to initialize applications with one device" > of
    print "//!" > of
    print "//! @param[in]    log_mgr        Pointer to log manager structure to initialize" > of
    print "//! @param[in]    log_structs    Pointer to log structs structure to initialize" > of
    print "//! @param[in]    log_buffers    Pointer to log buffers structure\n" > of

    print "static inline void logStructsInit(struct LOG_mgr     * const log_mgr," > of
    print "                                  struct LOG_structs * const log_structs," > of
    print "                                  struct LOG_buffers * const log_buffers)" > of
    print "{" > of
    print "    logStructsInitDevice(log_mgr, log_structs, log_buffers, 0);" > of
    print "}\n\n" > of


    # Generate static inline for mono-device classes

    print "//! Initialize log_read structure" > of
    print "//!" > of
    print "//! This function will clear the log_read structure and set the links to the log_structs and log_buffers." > of
    print "//!" > of
    print "//! @param[in]    log_read       Pointer to log read structure to initialize" > of
    print "//! @param[in]    log_structs    Pointer to log structs structure" > of
    print "//! @param[in]    log_buffers    Pointer to log buffers structure\n" > of

    print "static inline void logReadInit(struct LOG_read    * const log_read," > of
    print "                               struct LOG_structs * const log_structs," > of
    print "                               struct LOG_buffers * const log_buffers)" > of
    print "{" > of
    print "    memset(log_read, 0, sizeof(*log_read));\n" > of

    print "    log_read->log     = log_structs->log;" > of
    print "    log_read->buffers = log_buffers->buffers;" > of
    print "}\n" > of


    print "// EOF" > of

    close(of)
}



function WriteLogSigNamesInitialization(of,   log_idx, repeat_idx, sig_idx, units)
{
    print "//! Declare static array of pointers to the log signal names and units\n" > of

    print "static char const log_sig_names_and_units[] =" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        for(repeat_idx = 1 ; repeat_idx <= value["$REPEAT",log_idx] ; repeat_idx++)
        {
            # Produce string that concatenates all the signal names (and units) for the log

            printf "    \"" > of

            for(sig_idx = 1 ; sig_idx <= num_signals[log_idx] ; sig_idx++)
            {
                # Add signal name delimiter (point) prefix, followed by the signal name

                printf ".%s", signal[log_idx,sig_idx,"$SIG_NAME"] > of

                # For analog signals with units, add the units delimiter (colon) prefix, followed by the units

                if(log_is_analog[log_idx])
                {
                    units = signal[log_idx,sig_idx,"$SIG_UNITS"]

                    if(units != "")
                    {
                        printf ":%s", units > of
                    }
                }
            }

            # Add log delimiter (comma) suffix

            print ";\"" > of
        }
    }

    print ";\n\n" > of
}



function WriteLogInitialization(of, global_log_idx, log_idx, repeat_idx,  log_name, global_log_name, menu_idx, selector_idx, sig_idx, menu_sig_idx, sig_name, units)
{
    log_name = value["$LOG_NAME",log_idx]

    if(repeat_idx > 1)
    {
        global_log_name = (log_name repeat_idx)
    }
    else
    {
        global_log_name = log_name
    }

    alias_idx = log_is_alias[log_idx] ? 1 : repeat_idx

    # Initialize global private log variables

    printf "\n    // Log %u in log_struct.log[%u]: %s - %s\n\n",
                                        global_log_idx, global_log_idx-1, global_log_name, value["$LOG_TITLE",log_idx] > of

    printf "    priv->name                    = \"%s\";\n", global_log_name > of
    printf "    priv->sig_bufs_offset         = %u;\n",     buf_offset > of
    printf "    priv->sig_bufs_len            = %u;\n",     buf_len[log_idx] > of
    printf "    priv->num_postmortem_sig_bufs = LOG_%s_NUM_POSTMORTEM_SIG_BUFS;\n", log_name > of
    printf "    priv->num_postmortem_samples  = %u;\n",     value["$NUM_POSTMORTEM_SAMPLES", log_idx] > of
    printf "    priv->num_signals             = LOG_%s_NUM_SIGNALS;\n", log_name > of

    # Initialize ANALOG/DIGITAL variables

    if(log_is_analog[log_idx])
    {
        printf "\n" > of
        printf "    priv->ad.analog.signals       = log_buffers->%s.signals[%d];\n",  tolower(log_name), (repeat_idx - 1) > of
        printf "    priv->ad.analog.selectors     = log_buffers->%s.selectors[%d];\n",tolower(log_name), (alias_idx - 1) > of
    }
    else
    {
        printf "    priv->ad.digital.signals      = log_buffers->%s.signals[%d];\n\n", tolower(log_name), (repeat_idx - 1) > of
    }

    # Initialize global log variables

    printf "    log->log_mgr                  = log_mgr;\n" > of
    printf "    log->priv                     = priv;\n" > of
    printf "    log->start_cycle              = &log_structs->start_cycle;\n" > of
    printf "    log->index                    = LOG_%s;\n",                       global_log_name > of
    printf "    log->num_cycles               = LOG_%s_NUM_CYCLES;\n",            log_name > of
    printf "    log->bit_mask                 = (uint32_t)(1 << LOG_%s);\n",      global_log_name > of
    printf "    log->meta                     = %s;\n",                           log_meta[log_idx] > of
    printf "    log->num_sig_bufs             = LOG_%s_NUM_SIG_BUFS;\n",          log_name > of
    printf "    log->num_samples_per_sig      = %u;\n",                           value["$NUM_SAMPLES",log_idx] > of
    printf "    log->last_sample_marker.index = %u;\n",                           (value["$NUM_SAMPLES",log_idx] - 1) > of
    printf "    log->sig_bufs                 = &log_buffers->buffers[%u];\n",    buf_offset > of
    printf "    log->cycles                   = log_buffers->%s.cycles[%d];\n",   tolower(log_name), (alias_idx - 1) > of
    printf "    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;\n\n" > of

    # Initialize ANALOG/DIGITAL variables

    if(log_is_analog[log_idx])
    {
        printf "    log->ad.analog.selector_ptrs  = log_structs->%s[%d].selector_ptrs;\n\n",tolower(log_name), (alias_idx - 1) > of
    }
    else
    {
        if(value["$DIG_SOURCE",log_idx] !~ "^#")
        {
            printf "    log->ad.digital.source        = %s;\n", value["$DIG_SOURCE",log_idx] > of
        }
        else
        {
            printf "    log->ad.digital.signals       = log_structs->%s[%d].signals;\n", tolower(log_name), (repeat_idx - 1) > of
        }

        printf "    log->ad.digital.num_signals   = LOG_%s_NUM_SIGNALS;\n\n", log_name > of
    }

    # Initialize CONTINUOUS/DISCONTINUOUS variables

    if(log_is_continuous[log_idx])
    {
        printf "    log->cd.continuous.state                   = LOG_RUNNING;\n" > of
        printf "    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;\n" > of
        printf "    log->cd.continuous.num_post_freeze_samples = %u;\n", value["$NUM_POST_FREEZE_SAMPLES", log_idx] > of
    }
    else
    {
        printf "    log->cd.discontinuous.last_cyc_sel = LOG_%s_NUM_CYCLES;\n",log_name > of
    }

    # Initialize signal selectors (for ANALOG signals)

    if(log_is_analog[log_idx])
    {
        printf "\n" > of

        selector_idx = 0

        for(menu_idx = 1 ; menu_idx <= num_log_menus[log_idx] ; menu_idx++)
        {
            for(menu_sig_idx = 1 ; menu_sig_idx <= log_menu_num_sigs[log_idx, menu_idx] ; menu_sig_idx++)
            {
                printf "    priv->ad.analog.selectors[%2u] = %2u;  // %s;\n", selector_idx++,
                                    (log_menu_signal[log_idx, menu_idx, menu_sig_idx] - 1),
                                    signal[log_idx, log_menu_signal[log_idx, menu_idx, menu_sig_idx], "$SIG_NAME"] > of
            }
        }
    }

    # Initialize signals

    for(sig_idx = 1 ; sig_idx <= num_signals[log_idx] ; sig_idx++)
    {
        if(log_is_analog[log_idx])
        {
            # Point to source provided in the ODS file

            signal_source = signal[log_idx, sig_idx, "$SOURCE"];

            if(DEBUG_SIGNALS == 1)
            {
                # Point to a static identifiable floating point value

                printf "    static cc_float debug_signal_%d_%d = %d.0F;\n", global_log_idx, sig_idx, global_log_idx * 100 + sig_idx > of

                signal_source = "&debug_signal_" global_log_idx "_" sig_idx ;
            }
            else if(signal_source == "NULL")
            {
                # NULL sources will point to a dummy constant equal to 0

                signal_source = "(cc_float*)&dummy_zero_float_value";
            }

            printf "    priv->ad.analog.signals[%2u].source = %s;\n", (sig_idx - 1), signal_source > of

            if(signal[log_idx,sig_idx,"$STEP"] == "YES")
            {
                printf "    priv->ad.analog.signals[%2u].meta   = SPY_SIG_META_STEPS;\n", (sig_idx - 1) > of
            }

            if(signal[log_idx,sig_idx,"$TIME_OFFSET"] != "")
            {
                printf "    priv->ad.analog.signals[%2u].time_offset = %s;\n", (sig_idx - 1), signal[log_idx,sig_idx,"$TIME_OFFSET"] > of
            }
        }
        else
        {
            # Digital signal: time offset if non-zero

            if(signal[log_idx,sig_idx,"$TIME_OFFSET"] != "")
            {
                printf "    priv->ad.digital.signals[%2u].time_offset = %s;\n", (sig_idx - 1), signal[log_idx,sig_idx,"$TIME_OFFSET"] > of
            }

            signal_source = signal[log_idx,sig_idx, "$SOURCE"]

            # NULL sources will point to a dummy constant equal to false

            if(value["$DIG_SOURCE",log_idx] == "#")
            {
                if(signal_source != "NULL")
                {
                    signal_source = signal[log_idx, sig_idx, "$SOURCE"]
                    signal_type   = tolower(signal[log_idx, sig_idx, "$SOURCE_TYPE"])

                    if(signal_type == "pointer")
                    {
                        signal_source = "(void**)" signal_source
                    }
                }
                else
                {
                    signal_source = "(bool*)&dummy_zero_bool_value"
                    signal_type   = "boolean"
                }

                printf "    log->ad.digital.signals[%2u].source.%s = %s;\n", (sig_idx - 1), signal_type, signal_source > of
                printf "    log->ad.digital.signals[%2u].source_type = LOG_DIG_SOURCE_TYPE_%s;\n", (sig_idx - 1), toupper(signal_type) > of

                if(signal[log_idx,sig_idx,"$BIT"] != "")
                {
                    printf "    log->ad.digital.signals[%2u].bit_mask = (1 << %s);\n", (sig_idx - 1), signal[log_idx,sig_idx,"$BIT"] > of
                }
            }
        }
    }

    if(log_is_analog[log_idx])
    {
        printf "\n    logStoreAnalogSelector(log);\n" > of
    }

    printf "\n    log++;\n" > of
    printf "    priv++;\n" > of
    buf_offset += (log_is_alias[log_idx] == 1 && value["$REPEAT",log_idx] != repeat_idx) ? 0 : buf_len[log_idx]
}



function WritePmBuf(of,  log_idx, log_name, max_pm_buf_log_len, class_id)
{
    WriteGnuLicense("liblog", "Logging", "PM_BUF data structures header file", of)

    printf "#include <stdint.h>\n" > of
    printf "#include <string.h>\n\n" > of

    class_id = value["$CLASS_ID"]

    # Generate constants

    WritePmBufConstants(of, class_id)

    # Generate sub structures

    WritePmBufSubStruct(of, class_id)

    # Generate PM_buf structure

    printf "// PM_buf structure\n\n" > of
    printf "struct PM_buf_%u\n{\n",class_id > of
    printf "    // PM_buf structure version in Network Byte Order - set to PM_BUF_%u_VERSION in the FGC\n\n", class_id > of
    printf "    uint32_t version;\n\n" > of
    printf "    // Event log - Network Byte Order\n\n" > of
    printf "    struct PM_buf_%u_evtlog evtlog[PM_BUF_%u_EVTLOG_LEN];\n", class_id, class_id > of

    # Write main PM_buf structure

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_pm_buf[log_idx])
        {
             WritePmBufStruct(of, log_idx, class_id)
        }
    }

    print "};\n" > of

    # Write union to get around strict aliasing rules

    printf "// PM_buf union to work around strict aliasing rules\n\n" > of
    printf "union  PM_buf_%u_union\n", class_id > of
    printf "{\n" > of
    printf "    char             buffer[PM_BUF_%u_SIZE];\n", class_id > of
    printf "    struct PM_buf_%u self;\n", class_id > of
    printf "};\n\n" > of

    # Generate converted structure

    WritePmBufConvertedStruct(of, class_id)

    # Generate extern function declarations

    WritePmBufFunctions(of, class_id)

    WritePmBufPmdFunctions(of, class_id)

    print "// EOF" > of

    close(of)
}



function WritePmBufConstants(of, class_id,  log_idx, pm_buf_log_len, max_pm_buf_log_len)
{
    # Generate constants

    printf "// Version and size of PM_buf_%u structure\n\n", class_id > of
    printf "#define PM_BUF_%u_%-20s %d\n",  class_id, "VERSION", value["$PM_BUF_VERSION"] > of
    printf "#define PM_BUF_%u_%-20s %d\n\n",class_id, "SIZE"   , pm_buf_size > of
    printf "// PM_buf_%u log lengths\n\n", class_id > of

    printf "#define PM_BUF_%u_%-20s %d\n", class_id, "EVTLOG_LEN",          value["$EVTLOG_LEN"] > of
    printf "#define PM_BUF_%u_%-20s %d\n", class_id, "EVTLOG_PROPERTY_LEN", 24 > of
    printf "#define PM_BUF_%u_%-20s %d\n", class_id, "EVTLOG_SYMBOL_LEN",   36 > of
    printf "#define PM_BUF_%u_%-20s %d\n", class_id, "EVTLOG_ACTION_LEN",    7 > of

    max_pm_buf_log_len = 0

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_pm_buf[log_idx])
        {
            pm_buf_log_len = value["$NUM_POSTMORTEM_SAMPLES", log_idx]

            printf "#define PM_BUF_%u_%-20s %d\n", class_id, value["$PM_BUF_NAME", log_idx] "_LEN", pm_buf_log_len > of

            if(pm_buf_log_len > max_pm_buf_log_len)
            {
                max_pm_buf_log_len = pm_buf_log_len
            }
        }
    }

    printf "#define PM_BUF_%u_%-20s %d\n", class_id, "MAX_LEN", max_pm_buf_log_len > of
}



function WritePmBufSubStruct(of, class_id)
{
    printf "\n// Event log record structure - Network Byte Order\n\n" > of
    printf "struct PM_buf_%u_evtlog\n",class_id > of
    printf "{\n" > of
    printf "    uint32_t timestamp_s;\n" > of
    printf "    uint32_t timestamp_us;\n\n" > of
    printf "    char     property[PM_BUF_%u_EVTLOG_PROPERTY_LEN];\n", class_id > of
    printf "    char     symbol  [PM_BUF_%u_EVTLOG_SYMBOL_LEN];\n",   class_id > of
    printf "    char     action  [PM_BUF_%u_EVTLOG_ACTION_LEN];\n",   class_id > of
    printf "    char     status;\n" > of
    printf "};\n\n" > of

    printf "// Liblog signal log header structure - Little Endian\n\n" > of
    printf "struct PM_buf_%u_signals_header\n",class_id > of
    printf "{\n" > of
    printf "    uint32_t first_sample_time_s;\n" > of
    printf "    uint32_t first_sample_time_ns;\n" > of
    printf "    uint32_t period_s;\n" > of
    printf "    uint32_t period_ns;\n" > of
    printf "};\n\n" > of
}



function WritePmBufStruct(of, log_idx, class_id,  menu_sig_idx, Log_Name, log_name)
{
    Log_Name = value["$PM_BUF_NAME", log_idx];
    log_name = tolower(Log_Name)

    printf "\n    // Log %s - Little Endian\n\n", Log_Name > of
    printf "    struct PM_buf_%u_%s\n", value["$CLASS_ID"], log_name > of
    printf "    {\n" > of
    printf "        struct PM_buf_%u_signals_header header;\n\n",value["$CLASS_ID"] > of
    printf "        struct PM_buf_%u_%s_signals\n", class_id, log_name > of
    printf "        {\n" > of

    for(menu_sig_idx = 1 ; menu_sig_idx <= log_menu_num_sigs[log_idx, 1]; menu_sig_idx++)
    {
        pm_buf_sig_name = signal[log_idx, log_menu_signal[log_idx, 1, menu_sig_idx], "$PM_BUF_SIG_NAME"]

        if(pm_buf_sig_name != "")
        {
            printf "            float %s[PM_BUF_%u_%s_LEN];\n", tolower(pm_buf_sig_name), class_id, Log_Name > of
        }
    }

    printf "        } signals;\n" > of
    printf "    } %s;\n", log_name > of
}



function WritePmBufConvertedStruct(of, class_id,  log_idx)
{
    # Generate static converted data structure

    printf "// Data converted structure\n\n" > of
    printf "struct PM_buf_%u_converted\n", value["$CLASS_ID"] > of
    printf "{\n" > of

    printf "    // Event log arrays\n\n" > of

    printf "    int64_t      evtlog_timestamps_ns[PM_BUF_%u_EVTLOG_LEN];\n", class_id > of
    printf "    char         evtlog_property     [PM_BUF_%u_EVTLOG_LEN][PM_BUF_%u_EVTLOG_PROPERTY_LEN+1];\n", class_id, class_id > of
    printf "    char         evtlog_symbol       [PM_BUF_%u_EVTLOG_LEN][PM_BUF_%u_EVTLOG_SYMBOL_LEN+1];\n",   class_id, class_id > of
    printf "    char         evtlog_action       [PM_BUF_%u_EVTLOG_LEN][PM_BUF_%u_EVTLOG_ACTION_LEN+1];\n",   class_id, class_id > of
    printf "    char       * evtlog_property_ptr [PM_BUF_%u_EVTLOG_LEN];\n",   class_id > of
    printf "    char       * evtlog_symbol_ptr   [PM_BUF_%u_EVTLOG_LEN];\n",   class_id > of
    printf "    char       * evtlog_action_ptr   [PM_BUF_%u_EVTLOG_LEN];\n",   class_id > of
    printf "    const char * evtlog_status_ptr   [PM_BUF_%u_EVTLOG_LEN];\n\n", class_id > of

    printf "    // Liblog timestamp arrays\n\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_pm_buf[log_idx])
        {
            Log_Name = value["$PM_BUF_NAME", log_idx]

            printf "    int64_t %s_timestamps_ns[PM_BUF_%u_%s_LEN];\n", tolower(Log_Name), class_id, Log_Name > of
        }
    }

    printf "\n} pm_buf_%u_converted;\n\n", class_id > of

    # Generate the event log status strings

    printf "// Event log status strings\n\n" > of

    printf "enum PM_buf_%u_evtlog_status\n", class_id > of
    printf "{\n" > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_DROPPED                   ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL                    ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL_FILTERED           ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL_TO_FREQ            ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL_TO_FREQ_FILTERED   ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL_FROM_FREQ          ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_NORMAL_FROM_FREQ_FILTERED ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_FREQ                      ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_FREQ_FILTERED             ,\n", class_id > of
    printf "    PM_BUF_%u_EVTLOG_STATUS_UNKNOWN                   ,\n", class_id > of
    printf "};\n\n" > of

    printf "static const char * pm_buf_%u_evtlog_status[] =\n", class_id > of
    printf "{\n" > of
    printf "    \"DROPPED\"                    ,\n" > of
    printf "    \"NORMAL\"                     ,\n" > of
    printf "    \"NORMAL_FILTERED\"            ,\n" > of
    printf "    \"NORMAL_TO_FREQ\"             ,\n" > of
    printf "    \"NORMAL_TO_FREQ_FILTERED\"    ,\n" > of
    printf "    \"NORMAL_FROM_FREQ\"           ,\n" > of
    printf "    \"NORMAL_FROM_FREQ_FILTERED\"  ,\n" > of
    printf "    \"FREQ\"                       ,\n" > of
    printf "    \"FREQ_FILTERED\"              ,\n" > of
    printf "    \"UNKNOWN\"                    ,\n" > of
    printf "};\n\n" > of
}


function WritePmBufFunctions(of, class_id)
{
    printf "// PM_BUF Static functions - these must be C functions to be tested by ccrt\n\n" > of

    # Generate static functions

    # Function : pm_buf_{class_id}_ConvertNsTimestamp

    printf "static int64_t pm_buf_%u_ConvertNsTimestamp(const uint32_t timestamp_s, const uint32_t timestamp_ns)\n", class_id > of
    printf "{\n" > of
    printf "    return (int64_t)timestamp_s * 1000000000LL + (int64_t)timestamp_ns;\n" > of
    printf "}\n\n\n\n" > of

    # Function : pm_buf_{class_id}_ConvertUsTimestamp

    printf "static int64_t pm_buf_%u_ConvertUsTimestamp(const uint32_t timestamp_s, const uint32_t timestamp_us)\n", class_id > of
    printf "{\n" > of
    printf "    return (int64_t)timestamp_s * 1000000000LL + (int64_t)timestamp_us * 1000LL;\n" > of
    printf "}\n\n\n\n" > of

    # Function : pm_buf_{class_id}_GenerateTimestamps

    printf "static void pm_buf_%u_GenerateTimestamps(struct PM_buf_%u_signals_header * const header, ", class_id, class_id > of
    printf "int64_t * timestamps_ns, uint32_t num_samples)\n" > of
    printf "{\n" > of

    printf "    int64_t       first_sample_time_ns = pm_buf_%u_ConvertNsTimestamp(header->first_sample_time_s, header->first_sample_time_ns);\n", class_id > of
    printf "    const int64_t period_ns = (int64_t)header->period_s * 1000000000LL + (int64_t)header->period_ns;\n\n" > of

    printf "    while(num_samples--)\n" > of
    printf "    {\n" > of
    printf "        *(timestamps_ns++)    = first_sample_time_ns;\n" > of
    printf "        first_sample_time_ns += period_ns;\n" > of
    printf "    }\n" > of
    printf "}\n\n\n\n" > of

    # Function : pm_buf_{class_id}_ConvertEvtlog

    printf "static void pm_buf_%u_ConvertEvtlog(struct PM_buf_%u * const pm_buf)\n", class_id, class_id > of
    printf "{\n" > of

    printf "    for(uint32_t i = 0; i < PM_BUF_%u_EVTLOG_LEN; i++)\n", class_id > of
    printf "    {\n" > of
    printf "        pm_buf_%u_converted.evtlog_timestamps_ns[i] = pm_buf_%u_ConvertUsTimestamp(ntohl(pm_buf->evtlog[i].timestamp_s), ntohl(pm_buf->evtlog[i].timestamp_us));\n", class_id, class_id > of
    printf "        pm_buf_%u_converted.evtlog_property_ptr [i] = (char *)memcpy(pm_buf_%u_converted.evtlog_property[i],pm_buf->evtlog[i].property, PM_BUF_%u_EVTLOG_PROPERTY_LEN);\n", class_id, class_id, class_id > of
    printf "        pm_buf_%u_converted.evtlog_symbol_ptr   [i] = (char *)memcpy(pm_buf_%u_converted.evtlog_symbol  [i],pm_buf->evtlog[i].symbol,   PM_BUF_%u_EVTLOG_SYMBOL_LEN);\n",   class_id, class_id, class_id > of
    printf "        pm_buf_%u_converted.evtlog_action_ptr   [i] = (char *)memcpy(pm_buf_%u_converted.evtlog_action  [i],pm_buf->evtlog[i].action,   PM_BUF_%u_EVTLOG_ACTION_LEN);\n\n", class_id, class_id, class_id > of

    printf "        switch(pm_buf->evtlog[i].status)\n", class_id > of
    printf "        {\n" > of
    printf "            case '-': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_DROPPED];                   break;\n", class_id, class_id, class_id > of
    printf "            case ' ': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL];                    break;\n", class_id, class_id, class_id > of
    printf "            case '*': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL_FILTERED];           break;\n", class_id, class_id, class_id > of
    printf "            case '^': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL_TO_FREQ];            break;\n", class_id, class_id, class_id > of
    printf "            case 't': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL_TO_FREQ_FILTERED];   break;\n", class_id, class_id, class_id > of
    printf "            case 'v': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL_FROM_FREQ];          break;\n", class_id, class_id, class_id > of
    printf "            case 'f': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_NORMAL_FROM_FREQ_FILTERED]; break;\n", class_id, class_id, class_id > of
    printf "            case '+': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_FREQ];                      break;\n", class_id, class_id, class_id > of
    printf "            case '#': pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_FREQ_FILTERED];             break;\n", class_id, class_id, class_id > of
    printf "            default : pm_buf_%u_converted.evtlog_status_ptr[i] = pm_buf_%u_evtlog_status[PM_BUF_%u_EVTLOG_STATUS_UNKNOWN];                   break;\n", class_id, class_id, class_id > of
    printf "        }\n" > of
    printf "    }\n" > of
    printf "}\n\n\n\n" > of

    # Function : pm_buf_{class_id}_ConvertLiblog

    printf "static void pm_buf_%u_ConvertLiblog(struct PM_buf_%u * const pm_buf)\n", class_id, class_id > of
    printf "{\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_pm_buf[log_idx])
        {
            Log_Name = value["$PM_BUF_NAME", log_idx];
            log_name = tolower(Log_Name)

            printf "    pm_buf_%u_GenerateTimestamps(&pm_buf->%s.header, pm_buf_%u_converted.%s_timestamps_ns, PM_BUF_%u_%s_LEN);\n", class_id, log_name, class_id, log_name, class_id, Log_Name > of
        }
    }

    printf "}\n\n\n" > of
}



function WritePmBufPmdFunctions(of, class_id)
{
    printf "// PMD Functions - these use C++ features and are not tested by ccrt\n\n" > of
    printf "#ifndef PM_BUF_NO_PMD_FUNCTIONS\n\n" > of

    printf "static void pmd_%uAddFgc(PMData &pmdata)\n", class_id > of
    printf "{\n" > of
    printf "    // Use a union to circumvent strict-aliasing warning to cast to the PM_buf_%u structure pointer\n\n", class_id > of

    printf "    union  PM_buf_%u_union * const self_union = (union PM_buf_%u_union *)pm_globals.buffer.self.std.fgc_buffers;\n", class_id, class_id > of
    printf "    struct PM_buf_%u       * const self       = &self_union->self;\n\n", class_id > of

    printf "    // Check the data version (always in network byte order)\n\n" > of

    printf "    if(ntohl(self->version) != PM_BUF_%u_VERSION)\n", class_id > of
    printf "    {\n" > of
    printf "        throw std::runtime_error(\"FGC's LOG.PM.BUF data version is not compatible with FGCD (%u expected)\");\n", value["$PM_BUF_VERSION"] > of
    printf "    }\n\n" > of

    printf "    // Reset the converted data arrays\n\n" > of

    printf "    memset(&pm_buf_%u_converted, 0, sizeof(struct PM_buf_%u_converted));\n\n", class_id, class_id > of

    printf "    // Convert event log data\n\n" > of

    printf "    pm_buf_%u_ConvertEvtlog(self);\n\n", class_id > of

    printf "    // Convert liblog data\n\n" > of

    printf "    pm_buf_%u_ConvertLiblog(self);\n\n", class_id, class_id > of

    printf "    // Submit event log data to PM system - evtlog timestamps are always in network byte order\n\n" > of

    printf "    pmdata.registerArray(\"EVENTS.TIMESTAMP\", pm_buf_%u_converted.evtlog_timestamps_ns, PM_BUF_%u_EVTLOG_LEN);\n\n", class_id, class_id > of

    printf "    pmdata.registerArray(\"EVENTS.PROPERTY\", const_cast<const char **>(pm_buf_%u_converted.evtlog_property_ptr), PM_BUF_%u_EVTLOG_LEN);\n", class_id, class_id > of
    printf "    pmdata.addAttribute (\"EVENTS.PROPERTY\", \"timescale\", \"EVENTS\");\n" > of
    printf "    pmdata.addAttribute (\"EVENTS.PROPERTY\", \"timestamps\", \"EVENTS.TIMESTAMP\",true);\n\n" > of

    printf "    pmdata.registerArray(\"EVENTS.SYMBOL\", const_cast<const char **>(pm_buf_%u_converted.evtlog_symbol_ptr), PM_BUF_%u_EVTLOG_LEN);\n", class_id, class_id > of
    printf "    pmdata.addAttribute (\"EVENTS.SYMBOL\", \"timescale\", \"EVENTS\");\n" > of
    printf "    pmdata.addAttribute (\"EVENTS.SYMBOL\", \"timestamps\", \"EVENTS.TIMESTAMP\",true);\n\n" > of

    printf "    pmdata.registerArray(\"EVENTS.ACTION\", const_cast<const char **>(pm_buf_%u_converted.evtlog_action_ptr), PM_BUF_%u_EVTLOG_LEN);\n", class_id, class_id > of
    printf "    pmdata.addAttribute (\"EVENTS.ACTION\", \"timescale\", \"EVENTS\");\n" > of
    printf "    pmdata.addAttribute (\"EVENTS.ACTION\", \"timestamps\", \"EVENTS.TIMESTAMP\",true);\n\n" > of

    printf "    pmdata.registerArray(\"EVENTS.STATUS\", const_cast<const char **>(pm_buf_%u_converted.evtlog_status_ptr), PM_BUF_%u_EVTLOG_LEN);\n", class_id, class_id > of
    printf "    pmdata.addAttribute (\"EVENTS.STATUS\", \"timescale\", \"EVENTS\");\n" > of
    printf "    pmdata.addAttribute (\"EVENTS.STATUS\", \"timestamps\", \"EVENTS.TIMESTAMP\",true);\n\n" > of

    printf "    // Submit liblog data to PM system - the data is always in little endian byte order\n\n" > of

    for(log_idx = 1 ; log_idx <= num_logs ; log_idx++)
    {
        if(log_is_pm_buf[log_idx])
        {
            Log_Name = value["$PM_BUF_NAME", log_idx];
            log_name = tolower(Log_Name)

            printf "    pmdata.registerArray(\"%s.TIMESTAMP\", pm_buf_%u_converted.%s_timestamps_ns, PM_BUF_%u_%s_LEN);\n\n", Log_Name, class_id, log_name, class_id, Log_Name > of

            for(menu_sig_idx = 1 ; menu_sig_idx <= log_menu_num_sigs[log_idx, 1]; menu_sig_idx++)
            {
                log_sig_idx     = log_menu_signal[log_idx, 1, menu_sig_idx]
                pm_buf_sig_name = signal[log_idx, log_sig_idx, "$PM_BUF_SIG_NAME"]

                if(pm_buf_sig_name != "")
                {
                    printf "    pmdata.registerArray(\"%s.%s\", self->%s.signals.%s, PM_BUF_%u_%s_LEN);\n", Log_Name, pm_buf_sig_name, log_name, tolower(pm_buf_sig_name), class_id, Log_Name > of
                    printf "    pmdata.addAttribute (\"%s.%s\", \"timescale\", \"%s\");\n", Log_Name, pm_buf_sig_name, Log_Name > of
                    printf "    pmdata.addAttribute (\"%s.%s\", \"timestamps\", \"%s.TIMESTAMP\", true);\n", Log_Name, pm_buf_sig_name, Log_Name > of
                    printf "    pmdata.addUnits     (\"%s.%s\", \"%s\");\n\n", Log_Name, pm_buf_sig_name, signal[log_idx, log_sig_idx, "$SIG_UNITS"] > of
                }
            }
        }
    }

    printf "};\n\n" > of

    printf "#endif // PM_BUF_NO_FUNCTIONS\n\n" > of
}

# EOF
