//! @file  logStore.c
//! @brief Converter Control Logging library data storage functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// #define CCPRINTF

#include "liblog.h"

// Static functions

#if LOGSTORE_DEBUG > 0

//! Debug version of function for storing continuous samples of analog signals
//!
//! Setting LOGSTORE_DEBUG to 1 will replace normal version of logStoreAnalogSampleRT with the debug version.
//! It will inject debug values in the form 0xccCCLLSS into all analog logs, where 0xLL is the log index,
//! 0xSS is the signal index and 0xCCcc is the bottom word of the sample index. When viewed on a little endian
//! machine, this will appear SSLLCCCcc
//!
//! Values injected by this function will not make sense when displayed as floats,
//! but may help greatly in case of visual inspection of log values with hexdump.
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      sample_index  Index in the log buffer at which the sample will be stored

static void logStoreContinuousAnalogSampleRT(struct LOG_log * const log, uint32_t const sample_index)
{
    uint32_t   num_sig_bufs = log->num_sig_bufs;
    uint32_t * sig_buf      = &(log->sig_bufs)[sample_index * num_sig_bufs];
    uint32_t * selectors    = log->priv->ad.analog.selectors;

    // Prepare debug signal value mask to be viewed on a little-endian machine using hexdump or a hex editor.
    // This requires the sample_index word to be byte-swapped.

    union
    {
        uint16_t    index_16;
        uint8_t     index_8[2];
    } const sample = {.index_16 = sample_index };

    uint32_t const signal_mask = ((uint8_t)log->index << 8) | (sample.index_8[1] << 16) | (sample.index_8[0] << 24);

    // For each signal buffer (each signal buffer has a selector associated with it)

    // Since num_sig_bufs is always greater than zero, we use do { } while so that we can
    // pre-decrement num_sig_bufs, which may be more efficient than a post-decrement.

    do
    {
        *(sig_buf++) = signal_mask | (uint8_t)*(selectors++);
    }
    while(--num_sig_bufs);
}


//! Debug version of function for storing discontinuous samples of analog signals
//!
//! Since the offset is not used in debug mode, this can be a wrapper for
//! logStoreContinuousAnalogSampleRT().
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      sample_index  Index in the log buffer at which the sample will be stored
//! @param[in]      offset        Not used

static void logStoreDiscontinuousAnalogSampleRT(struct LOG_log * const log, uint32_t const sample_index, uint32_t const offset)
{
    logStoreContinuousAnalogSampleRT(log, sample_index);
}
#else


//! This function stores one sample in a continuous analog log buffer.
//! One sample means one value for each signal.
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      sample_index  Index in the log buffer at which the sample will be stored

static void logStoreContinuousAnalogSampleRT(struct LOG_log * const log, uint32_t const sample_index)
{
    // For each signal buffer (each signal buffer has a selector associated with it)

    uint32_t       num_sig_bufs  = log->num_sig_bufs;
    cc_float     * sig_buf       = &((cc_float *)log->sig_bufs)[sample_index * num_sig_bufs];
    cc_float    ** selector_ptrs = log->ad.analog.selector_ptrs;

    // Since num_sig_bufs is always greater than zero, we use do { } while so that we can
    // pre-decrement num_sig_bufs, which may be more efficient than a post-decrement.

    do
    {
        // Store analog signal value after sanity check

        union
        {
            uint32_t    u;
            cc_float    f;
        } sample_value = { .f = **(selector_ptrs++) };

        // IEEE-758 32-bit floating point: S EEEEEEEE MMMMMMMMMMMMMMMMMMMMMMM
        //
        // Check exponent with mask 11000000. If both bits are set, it means the value is
        // greater than about 1E19 or is NaN.

        if((sample_value.u & 0x60000000) == 0x60000000)
        {
            // Sample is invalid so substitute a recognizable value

            sample_value.f = LOG_INVALID_ANA_VALUE;
        }

        *(sig_buf++) = sample_value.f;

    } while(--num_sig_bufs);

}


//! This function stores one sample in a discontinuous analog log buffer.
//! One sample means one value for each signal. The values to be logged can be in
//! a linear buffer pointed to by the selector pointer. The offset parameter defines
//! the element in the linear buffer that should be copied to the log.
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      sample_index  Index in the log buffer at which the sample will be stored
//! @param[in]      offset        Offset from signals source pointer to the element to be logged

static void logStoreDiscontinuousAnalogSampleRT(struct LOG_log * const log, uint32_t const sample_index, uint32_t const offset)
{
    // For each signal buffer (each signal buffer has a selector associated with it)

    uint32_t    num_sig_bufs  = log->num_sig_bufs;
    cc_float  * sig_buf       = &((cc_float *)log->sig_bufs)[sample_index * num_sig_bufs];
    cc_float ** selector_ptrs = log->ad.analog.selector_ptrs;

    // Since num_sig_bufs is always greater than zero, we use do { } while so that we can
    // pre-decrement num_sig_bufs, which may be more efficient than a post-decrement.

    do
    {
        *(sig_buf++) = (*(selector_ptrs++))[offset];

    } while(--num_sig_bufs);
}
#endif // LOGSTORE_DEBUG


//! This function stores one sample in digital log buffer. Each sample is a 32-bit mask that contains up to 32 digital signals.
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      sample_index  Index in the log buffer at which the sample will be stored
//! @param[in]      offset        Offset from ana.source pointer to the element to be logged

static void logStoreDigitalSampleRT(struct LOG_log * const log, uint32_t const sample_index, uint32_t const offset)
{
    uint32_t * const dig_source   = log->ad.digital.source;
    uint32_t         dig_bit_mask = 0;

    if(dig_source != NULL)
    {
        // Pointer to complete digital bit mask is provided to extract the value

        dig_bit_mask = *dig_source;
    }
    else
    {
        uint32_t  walking_bit  = 1;

        // Loop for each digital signal in the log

        uint32_t                num_signals = log->ad.digital.num_signals;    // Is always greater than zero
        struct LOG_dig_signal * dig_signal  = log->ad.digital.signals;

        // Since num_signals is always greater than zero, we use do { } while so that we can
        // pre-decrement num_signals, which may be more efficient than a post-decrement.

        do
        {
            // Get digital source value using appropriate source type and set
            // corresponding bit in logged value when source is true

            switch(dig_signal->source_type)
            {
                case LOG_DIG_SOURCE_TYPE_BOOLEAN:

                    if(dig_signal->source.boolean[offset])
                    {
                        dig_bit_mask |= walking_bit;
                    }
                    break;

                case LOG_DIG_SOURCE_TYPE_UINT8:

                    if(dig_signal->source.uint8[offset] & dig_signal->bit_mask)
                    {
                        dig_bit_mask |= walking_bit;
                    }
                    break;

                case LOG_DIG_SOURCE_TYPE_UINT16:

                    if(dig_signal->source.uint16[offset] & dig_signal->bit_mask)
                    {
                        dig_bit_mask |= walking_bit;
                    }
                    break;

                case LOG_DIG_SOURCE_TYPE_UINT32:

                    if(dig_signal->source.uint32[offset] & dig_signal->bit_mask)
                    {
                        dig_bit_mask |= walking_bit;
                    }
                    break;

                case LOG_DIG_SOURCE_TYPE_POINTER:

                    if(dig_signal->source.pointer[offset] != NULL)
                    {
                        dig_bit_mask |= walking_bit;
                    }
                    break;
            }

            walking_bit <<= 1;
            dig_signal++;

        } while(--num_signals);
    }

    // Store the complete digital bit mask in the log

    log->sig_bufs[sample_index] = dig_bit_mask;
}


//! Prepare to start a new cycle for a continuous log
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in,out]  start_cycle   Pointer to the start_cycle structure
//! @param[in,out]  cyc_sel       Cycle selector for the new cycle

static inline void logStoreStartCycleRT(struct LOG_log * const log, struct LOG_start_cycle * const start_cycle, uint32_t const cyc_sel)
{
    // Clear start_cycle flag for this log

    start_cycle->log_mask &= ~log->bit_mask;

    // End previous cycle if active

    if(log->cur_cycle < LOG_CYCLE_NOT_ACTIVE)
    {
        struct LOG_cycles * const cycles = &log->cycles[log->cur_cyc_sel];

        // Store the last sample marker

        cycles->cycle[log->cur_cycle].last_sample_marker = log->last_sample_marker;

        // The cycle is now complete (ready to be read out), so make it active

        cycles->active = log->cur_cycle;
    }

    // Save the new cyc_sel and tell the sample storing function that first_sample marker should be recorded

    log->cd.continuous.start_cycle_flag = true;

    log->cur_cyc_sel = cyc_sel;

    // Advance next cycle index

    log->cur_cycle = logStoreCalcNextIndex(log->cycles[cyc_sel].active, LOG_NUM_CYCLE_BUFS);

    ccprintf( "START CYCLE\n  log->index=%u  cyc_sel=%u  cycles[%u].active=%u  log->cur_cycle=%u\n"
            , log->index
            , cyc_sel
            , cyc_sel
            , log->cycles[cyc_sel].active
            , log->cur_cycle
            );

    // Save the time stamp as the cycle's time_origin and reset the number of samples for the new cycle

    struct LOG_cycle * const cycle = &log->cycles[cyc_sel].cycle[log->cur_cycle];

    cycle->num_samples = 0;
    cycle->time_origin = start_cycle->time;
}


//! For continuous logs, liblog uses the first element of the cycles array (cyc_sel=0) to register
//! log markers (time/index pairs) on the first sample of each UTC second.
//!
//! @param[in,out]  log           Pointer to the log structure

static inline void logStorePrepareNewTimeBaseRT(struct LOG_log * const log)
{
    struct LOG_cycles * const time_base = &log->cycles[0];

    // If next_time_base isn't set to LOG_CYC_NOT_ACTIVE

    if(log->cd.continuous.cur_time_base < LOG_CYCLE_NOT_ACTIVE)
    {
        struct LOG_cycle * const cycle = &time_base->cycle[log->cd.continuous.cur_time_base];

        cycle->last_sample_marker = log->last_sample_marker;

        // If only one sample was recorded, then make two samples by using the last sample of the previous
        // time base as the first of this one.

        if(cycle->num_samples == 1)
        {
            cycle->num_samples = 2;

            cycle->first_sample_marker = time_base->cycle[time_base->active].last_sample_marker;
        }

        time_base->active = log->cd.continuous.cur_time_base;
    }

    // Advance current time_base index

    log->cd.continuous.cur_time_base = logStoreCalcNextIndex(time_base->active, LOG_NUM_CYCLE_BUFS);

    // Reset the number of samples in this second of the time base

    time_base->cycle[log->cd.continuous.cur_time_base].num_samples = 0;
}


//! This function processes the log state machine for continuous freezable logs.
//!
//! The state machine is circular: RUNNING -> STOPPING -> FROZEN -> STARTING. The transition conditions are:
//!
//!  * RUNNING  -> STOPPING : Freeze mask bit is set
//!  * STOPPING -> FROZEN   : All post-freeze samples have been recorded
//!  * FROZEN   -> STARTING : Freeze mask bit is cleared
//!  * STARTING -> RUNNING  : Automatic after one iteration
//!
//! @param[in,out]  log            Pointer to the log structure
//! @param[in,out]  time_stamp     Time stamp of the current iteration
//!
//! @returns        new log state

static enum LOG_state logStoreStateMachineRT(struct LOG_log  * const log,
                                             struct CC_ns_time const time_stamp)
{
    struct LOG_mgr * const log_mgr      = log->log_mgr;
    uint32_t         const log_bit_mask = log->bit_mask;

    // Process log_mgr freeze flag

    if(log_mgr->freeze_mask & log_bit_mask)
    {
        // Freeze flag is active - transition from RUNNING to STOPPING to FROZEN

        switch(log->cd.continuous.state)
        {
            default:    // STARTING or RUNNING

                // Move to STOPPING state, record the time stamp and set the stopping down counter

                log->cd.continuous.freeze_time_stamp = time_stamp;
                log->cd.continuous.stopping_counter    = log->cd.continuous.num_post_freeze_samples;

                // Fall through to STOPPING

            case LOG_STOPPING:

                // Freeze the log if all post-trigger samples have been recorded and clear the bit in the log_running_mask

                if(--log->cd.continuous.stopping_counter > 0)
                {
                    return LOG_STOPPING;
                }

                // All post-freeze samples have been recorded so enter FROZEN state

                log_mgr->running_logs_mask &= ~log_bit_mask;

                break;

            case LOG_FROZEN:

                break;
        }

        return LOG_FROZEN;
    }

    // Freeze flag is not active - transition from FROZEN back to RUNNING

    if(log->cd.continuous.state == LOG_FROZEN)
    {
        // Log is unfreezing so reset the number of samples for all cycles

        struct LOG_cycles * cycles     = log->cycles;
        uint32_t            num_cycles = log->num_cycles;

        do
        {
            cycles->cycle[cycles->active].num_samples = 0;
            cycles++;
        }
        while (--num_cycles);

        // Mark time base stored in cycles[0] as NOT_ACTIVE

        log->cd.continuous.cur_time_base = LOG_CYCLE_NOT_ACTIVE;
        log->cur_cyc_sel = 0;

        // Mark log as running in the running logs mask

        log_mgr->running_logs_mask |= log->bit_mask;

        // Transition to STARTING for one iteration only

        return LOG_STARTING;
    }

    // State is now RUNNING

    return LOG_RUNNING;
}


// Global functions for continuous logs

void logStoreStartContinuousRT(struct LOG_log  * const log,
                               struct CC_ns_time const time_stamp,
                               uint32_t          const cyc_sel)
{
    CC_ASSERT(cyc_sel != 0);

    // Save current and previous cycle selectors in log_mgr

    struct LOG_mgr * const log_mgr = log->log_mgr;

    log_mgr->prev_cyc_sel = log_mgr->cur_cyc_sel;
    log_mgr->cur_cyc_sel  = cyc_sel;

    // Save start of cycle time and the mask of logs that are continuous, cyclic, not-disabled and not-frozen

    struct LOG_start_cycle * const start_cycle = log->start_cycle;

    start_cycle->time = time_stamp;
    start_cycle->log_mask = log_mgr->cyclic_logs_mask
                          & log_mgr->continuous_logs_mask
                          & ~log_mgr->disabled_logs_mask
                          & (~log_mgr->freezable_logs_mask | log_mgr->running_logs_mask);

    ccprintf( "START CONTINUOUS\n  log->index=%u  cyc_sel=%u  log_mgr->prev_cyc_sel=%u  log_mgr->cur_cyc_sel=%u  time_stamp=%u.%09d\n"
            , log->index
            , cyc_sel
            , log_mgr->prev_cyc_sel
            , log_mgr->cur_cyc_sel
            , time_stamp.secs.abs
            , time_stamp.ns
            );
}



void logStoreContinuousRT(struct LOG_log  * const log,
                          struct CC_ns_time const time_stamp)
{
    CC_ASSERT((log->meta & LOG_META_CONTINUOUS_BIT_MASK) != 0);

    struct LOG_mgr * const log_mgr = log->log_mgr;

    // Skip if log is disabled

    if((log_mgr->disabled_logs_mask & log->bit_mask) != 0)
    {
        return;
    }

    // Prepare to start a new cycle if flagged by logStoreStartContinuousRT()

    struct LOG_start_cycle * const start_cycle = log->start_cycle;

    if((start_cycle->log_mask & log->bit_mask) != 0)
    {
        logStoreStartCycleRT(log, start_cycle, log_mgr->cur_cyc_sel);
    }

    // If log is freezable, run the log state machine: ... RUNNING -> STOPPING -> FROZEN -> STARTING -> RUNNING ...
    // Only continuous logs can be freezable.

    bool starting = false;

    if((log->meta & LOG_META_FREEZABLE_BIT_MASK) != 0)
    {
        switch(log->cd.continuous.state = logStoreStateMachineRT(log, time_stamp))
        {
            case LOG_FROZEN:

                // Log is FROZEN so return immediately without storing a sample

                return;

            case LOG_STARTING:

                // Log is STARTING so set the starting flag

                starting = true;
                break;

            default:

                // Log is RUNNING or STOPPING so continue to store a sample

                break;
        }
    }

    // Set the new_time_base flag to true also when the log is STARTING, because all log data has been invalidated

    bool const new_time_base = starting || time_stamp.secs.abs != log->last_sample_marker.time_stamp.secs.abs;

    // Prepare new time base before the new sample is stored!

    if(new_time_base)
    {
        logStorePrepareNewTimeBaseRT(log);
    }

    // Store one sample for each signal in the next position in the log

    uint32_t const index = logStoreCalcNextIndex(log->last_sample_marker.index, log->num_samples_per_sig);

    if((log->meta & LOG_META_ANALOG_BIT_MASK) == 0)
    {
        logStoreDigitalSampleRT(log, index, 0);
    }
    else
    {
        logStoreContinuousAnalogSampleRT(log, index);
    }

    // Increment stored samples counter and save the index and time stamp for the new sample

    log->last_sample_marker.index      = index;
    log->last_sample_marker.time_stamp = time_stamp;

    // If starting a new second, record the first sample information in the time base

    struct LOG_cycles * const time_base = &log->cycles[0];

    if(new_time_base)
    {
        time_base->cycle[log->cd.continuous.cur_time_base].first_sample_marker = log->last_sample_marker;
    }

    time_base->cycle[log->cd.continuous.cur_time_base].num_samples++;

    // If starting a new cycle, record the first sample information for this cycle

    uint32_t const cyc_sel = log->cur_cyc_sel;

    if(cyc_sel > 0)
    {
        if(starting || log->cd.continuous.start_cycle_flag)
        {
            log->cycles[cyc_sel].cycle[log->cur_cycle].first_sample_marker = log->last_sample_marker;

            log->cd.continuous.start_cycle_flag = false;
        }

        log->cycles[cyc_sel].cycle[log->cur_cycle].num_samples++;
    }
}


// Global functions for discontinuous logs

void logStoreStartDiscontinuousRT(struct LOG_log  * const log,
                                  struct CC_ns_time const time_origin,
                                  struct CC_ns_time const first_sample_time_stamp,
                                  uint32_t          const cyc_sel)
{
    CC_ASSERT((log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0);
    CC_ASSERT(cyc_sel < log->num_cycles);

    // If logging is active then cancel it

    if(log->cur_cycle < LOG_CYCLE_NOT_ACTIVE)
    {
        logStoreCancelDiscontinuousRT(log);
    }

    // If log is not disabled then start a new cycle for this log

    if((log->log_mgr->disabled_logs_mask & log->bit_mask) == 0)
    {
        // Save the new cyc_sel to activate the logging for this cyc_sel

        log->cur_cyc_sel = cyc_sel;

        log->log_mgr->running_logs_mask |= log->bit_mask;

        // Get next cycle index

        log->cur_cycle = logStoreCalcNextIndex(log->cycles[cyc_sel].active, LOG_NUM_CYCLE_BUFS);

        struct LOG_cycle * const cycle = &log->cycles[cyc_sel].cycle[log->cur_cycle];

        // Reset cycle, store time origin, latch stored samples counter and save the first sample time stamp

        cycle->num_samples = 0;
        cycle->time_origin = time_origin;
        cycle->latched_stored_counter = log->cd.discontinuous.stored_samples_counter;
        cycle->first_sample_marker.time_stamp = first_sample_time_stamp;

        // Store the most recent index in first_sample index - this is one less than what the real first sample index
        // will be when data is actually stored by logStoreDiscontinuous(). This will be adjusted when the cycle ends.

        cycle->first_sample_marker.index = log->last_sample_marker.index;

        ccprintf( "START DISCONTINUOUS\n  log->index=%u  cyc_sel=%u  cycles[%u].active=%u  cur_cycle=%u  "
                  "time_origin=%u.%09d  first_sample_time_stamp=%u.%09d\n"
                , log->index
                , cyc_sel
                , cyc_sel
                , log->cycles[cyc_sel].active
                , log->cur_cycle
                , cycle->time_origin.secs.abs
                , cycle->time_origin.ns
                , cycle->first_sample_marker.time_stamp.secs.abs
                , cycle->first_sample_marker.time_stamp.ns
                );
    }
}



void logStoreDiscontinuousRT(struct LOG_log * const log,
                             uint32_t               num_samples)
{
    CC_ASSERT((log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0);

    if(log->cur_cycle < LOG_CYCLE_NOT_ACTIVE)
    {
        // Keep track of the number of samples stored in this cycle

        log->cd.discontinuous.stored_samples_counter                    += num_samples;
        log->cycles[log->cur_cyc_sel].cycle[log->cur_cycle].num_samples += num_samples;

        // Loop to store the required number of samples

        uint32_t index = log->last_sample_marker.index;
        uint32_t offset;

        for(offset = 0 ; num_samples-- ; offset++)
        {
            // Increment index to the next free sample in the log

            index = logStoreCalcNextIndex(index, log->num_samples_per_sig);

            // Store the digital or analog signals using the offset from the source pointer

            if((log->meta & LOG_META_ANALOG_BIT_MASK) == 0)
            {
                logStoreDigitalSampleRT(log, index, offset);
            }
            else
            {
                logStoreDiscontinuousAnalogSampleRT(log, index, offset);
            }
        }

        // Save the index the newest sample

        log->last_sample_marker.index = index;
    }
}



void logStoreEndDiscontinuousRT(struct LOG_log  * const log,
                                struct CC_ns_time const last_sample_time_stamp)
{
    CC_ASSERT((log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0);

    if(log->cur_cycle < LOG_CYCLE_NOT_ACTIVE)
    {
        uint32_t           const cur_cyc_sel = log->cur_cyc_sel;
        struct LOG_cycle * const cycle       = &log->cycles[cur_cyc_sel].cycle[log->cur_cycle];

        // Adjust the first sample index by 1 because it was the previous index that was stored by logStoreStartDiscontinuous()

        cycle->first_sample_marker.index = logStoreCalcNextIndex(cycle->first_sample_marker.index, log->num_samples_per_sig);

        // Store the last sample index and time stamp

        cycle->last_sample_marker.index      = log->last_sample_marker.index;
        cycle->last_sample_marker.time_stamp = last_sample_time_stamp;

        // The cycle is now complete and is ready to be read out, so make it active for this cur_cyc_sel

        log->cycles[cur_cyc_sel].active = log->cur_cycle;

        // Save new cycle selector as the last completed cycle. This can be used for the postmortem.

        log->cd.discontinuous.last_cyc_sel = cur_cyc_sel;

        // Reset logging of this discontinuous log

        log->log_mgr->running_logs_mask &= ~log->bit_mask;

        log->cur_cycle = LOG_CYCLE_NOT_ACTIVE;

        ccprintf( "END DISCONTINUOUS\n  log->index=%u  cur_cyc_sel=%u  cycles[%u].active=%u  "
                  "first_sample.time_stamp=%u.%09d  last_sample.time_stamp=%u.%09d\n"
                , log->index
                , cur_cyc_sel
                , cur_cyc_sel
                , log->cycles[cur_cyc_sel].active
                , cycle->first_sample_marker.time_stamp.secs.abs
                , cycle->first_sample_marker.time_stamp.ns
                , cycle->last_sample_marker.time_stamp.secs.abs
                , cycle->last_sample_marker.time_stamp.ns
                );
    }
}



void logStoreCancelDiscontinuousRT(struct LOG_log * const log)
{
    CC_ASSERT((log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0);

    // If logging is active

    if(log->cur_cycle < LOG_CYCLE_NOT_ACTIVE)
    {
        // Reset the most_recent index and disable logging

        log->last_sample_marker.index    =  log->cycles[log->cur_cyc_sel].cycle[log->cur_cycle].first_sample_marker.index;
        log->log_mgr->running_logs_mask &= ~log->bit_mask;

        log->cur_cycle = LOG_CYCLE_NOT_ACTIVE;
    }
}


// NON-RT   logStoreAnalogSelector

void logStoreAnalogSelector(struct LOG_log * const log)
{
    CC_ASSERT((log->meta & LOG_META_ANALOG_BIT_MASK) != 0);

    // Cache analog signal source pointers based on selector indexes

    struct LOG_priv_ana_signal * const signals = log->priv->ad.analog.signals;

    uint32_t  * selectors     = log->priv->ad.analog.selectors;
    cc_float ** selector_ptrs = log->ad.analog.selector_ptrs;
    uint32_t    num_sig_bufs  = log->num_sig_bufs;

    // Since num_sig_bufs is always greater than zero, we use do { } while so that we can
    // pre-decrement num_sig_bufs, which may be more efficient than a post-decrement.

    do
    {
        *(selector_ptrs++) = signals[*(selectors++)].source;
    }
    while(--num_sig_bufs);
}


// NON-RT   logStoreAllSigNamesAndUnits

void logStoreAllSigNamesAndUnits(struct LOG_log * const logs, char const * sig_names_and_units)
{
    uint32_t log_idx;
    int32_t  sig_idx;
    uint32_t const num_logs = logs->log_mgr->num_logs;

    // Loop through all logs to reset all signal names and units

    for(log_idx = 0 ; log_idx < num_logs ; log_idx++)
    {
        struct LOG_private * priv = logs[log_idx].priv;

        for(sig_idx = 0 ; sig_idx < priv->num_signals ; sig_idx++)
        {
            if((logs[log_idx].meta & LOG_META_ANALOG_BIT_MASK) != 0)
            {
                // Analog signal - reset signal units string

                memset(priv->ad.analog.signals[sig_idx].name, 0, LOG_SIG_NAME_LEN);
                memset(priv->ad.analog.signals[sig_idx].units, 0, LOG_UNITS_LEN);
            }
            else
            {
                // Digital signal

                memset(priv->ad.digital.signals[sig_idx].name, 0, LOG_SIG_NAME_LEN);
            }
        }
    }

    // Loop until all logs updated or the end of the sig_names_and_units string is reached

    char     c;
    char   * to = NULL;
    uint32_t len = 0;
    uint32_t max_len = 0;

    log_idx = 0;
    sig_idx = -1;

    while(log_idx < num_logs && (c = *(sig_names_and_units++)) != '\0')
    {
        switch(c)
        {
            case ';':

                // Start next log

                log_idx++;
                sig_idx = -1;
                break;

            case '.':

                // Start next signal name by resetting name length

                to = (logs[log_idx].meta & LOG_META_ANALOG_BIT_MASK) != 0
                   ? logs[log_idx].priv->ad.analog.signals[++sig_idx].name
                   : logs[log_idx].priv->ad.digital.signals[++sig_idx].name;
                max_len = LOG_SIG_NAME_LEN;
                len = 0;
                break;

            case ':':

                // Start signal units by resetting the units length

                to = logs[log_idx].priv->ad.analog.signals[sig_idx].units;
                max_len = LOG_UNITS_LEN;
                len = 0;
                break;

            default:

                // Store signal name or signal units character if space remains

                if(++len < max_len)
                {
                    *(to++) = c;
                }
                break;
        }
    }
}


// NON-RT   logStoreSigNameAndUnits

void logStoreSigNameAndUnits(struct LOG_log * const log,
                             uint32_t         const sig_index,
                             char     const * const sig_name,
                             char     const * const units)
{
    if((log->meta & LOG_META_ANALOG_BIT_MASK) != 0)
    {
        // Store analog signal name and/or units

        if(sig_name[0] != '\0')
        {
            strncpy(log->priv->ad.analog.signals[sig_index].name, sig_name, LOG_SIG_NAME_LEN-1);
        }

        if(units[0] != '\0')
        {
            strncpy(log->priv->ad.analog.signals[sig_index].units, units, LOG_UNITS_LEN-1);
        }
    }
    else if(sig_name[0] != '\0')
    {
        // Store digital signal name

        strncpy(log->priv->ad.digital.signals[sig_index].name, sig_name, LOG_SIG_NAME_LEN-1);
    }
}

// EOF
