//! @file  logRead.c
//! @brief Converter Control Logging library read request functions.
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! Debug output control

//#define CCPRINTF

//! Define the global Spy functions in spy_v3.h, which is included in logClient.h, which is included from liblog.h

#define SPY_GLOBALS

//! Include header files

#include "liblog.h"

//! Constants

#define LOG_INVALID_INDEX       0xFFFFFFFF                             //!< Time to Index was given an invalid time and returns this invalid index

enum LOG_read_timing_mode
{
    LOG_TIMING_MODE_FST ,                                              //! Timing mode: First Sample Time = Time Origin + Time Offset
    LOG_TIMING_MODE_LST ,                                              //! Timing mode: Last Sample Time  = Time Origin + Time Offset
};

//! Local structure declaration

struct LOG_read_range                                                  //!< Intermediate structure between read_request and read_control
{
    struct LOG_read          * log_read;                               //!< Pointer to log read structure
    struct LOG_read_control  * read_control;                           //!< Pointer to read control structure
    struct LOG_log     const * log;                                    //!< Pointer to log structure
    struct LOG_private const * priv;                                   //!< Pointer to private log structure
    struct LOG_cycle           cycle;                                  //!< Active cycle being read
    struct LOG_sample_marker   first_sample;                           //!< Marker of the first sample to be returned
    struct LOG_sample_marker   last_sample;                            //!< Marker of the last sample to be returned
    struct LOG_sample_marker   time_to_index;                          //!< Marker for time to/from index calculations
    enum LOG_read_timing_mode  timing_mode;                            //!< Request timing mode
    struct CC_ns_time          period;                                 //!< Sampling period
    struct CC_ns_time          time_origin;                            //!< Requested time origin (abs time)
    struct CC_ns_time          time_offset;                            //!< Requested time offset (abs time)
    struct CC_ns_time          duration;                               //!< Requested duration (rel time)
    struct CC_ns_time          clipped_read_duration;                  //!< Clipped read duration (rel time)
    struct CC_ns_time          available_read_duration;                //!< Available read duration (rel time)
    int32_t                    sig_sel_indexes[LOG_MAX_SIGS_PER_LOG];  //!< Digital signal indexes or analog selector indexes
    uint32_t                   cyc_sel;                                //!< Cycle selector
    uint32_t                   sub_sampling_period;                    //!< Requested sub-sampling period (forced to 1 for PM_BUF header requests)
    int32_t                    max_samples;                            //!< Maximum samples that can fit in the buffer
};


//! [5] Process request to read a log that is disabled
//!
//! NON-RT  logReadLogDisabled
//!
//! This function is called if the log is disabled. If the header type is not PM_BUF or
//! the log is digital, then this immediately returns LOG_READ_CONTROL_DISABLED.
//! When the PM_BUF header is requested, a disabled log must return
//! the same size of data as an enabled log, but the data is all zero. logOutput() will
//! handle this provided read_control->num_samples_from_log is zero and
//! read_control->num_samples is the number zero samples to return.
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @return  Read control status

static uint32_t logReadLogDisabled(struct LOG_read_range * const read_range)
{
    struct LOG_read_control * const read_control = read_range->read_control;

    read_control->meta |= LOG_META_DISABLED_BIT_MASK;

    if(read_range->log_read->request.action == LOG_READ_GET_PM_BUF)
    {
        // Request is for PM_BUF header - we must accept the request and prepare to return a full buffer of zero padding
        // Return header_size and total_size set to the nominal values, but leave num_samples_from_log set to zero

        read_control->header_size = sizeof(struct LOG_pm_buf_header);
        read_control->num_signals = read_range->log->priv->num_postmortem_sig_bufs;
        read_control->num_samples = read_range->log->priv->num_postmortem_samples;
        read_control->data_len    = read_control->num_samples * read_control->num_signals;
        read_control->data_size   = read_control->data_len * LOG_VALUE_SIZE;
        read_control->total_size  = read_control->data_size + read_control->header_size;

        // Return the log period in the PM_BUF header but we can leave the first sample time set to zero

        read_range->log_read->header.pm_buf.period = read_control->period;

        return LOG_READ_CONTROL_SUCCESS;
    }

    // Requested header is not PM_BUF - report error that the log is DISABLED

    return LOG_READ_CONTROL_DISABLED;
}


//! [6] Set signals/selectors indexes structure
//!
//! NON-RT  logReadSetSigSelIndexes
//!
//! The function will fill read_range.sig_sel_indexes array with:
//! * requested signal indexes for digital logs (all digital signals are always logged)
//! * requested signal selector indexes for analog logs for signals that are currently available in the log
//! The array will be sorted.
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @return  Number of requested signals that are available (can be zero)

static uint32_t logReadSetSigSelIndexes(struct LOG_read_range * const read_range)
{
    uint32_t req_num_signals = 0;

    // Reset sig_sel_indexes to -1 to aid debugging

    int32_t * const sig_sel_indexes = read_range->sig_sel_indexes;

    memset(sig_sel_indexes, -1, LOG_MAX_SIGS_PER_LOG * sizeof(int32_t));

    // Count the number of signals and signal buffers for the request
    // The following section fills the selectors array with selectors indexes in ascending order

    if((read_range->log->meta & LOG_META_ANALOG_BIT_MASK) != 0)
    {
        // Analog log

        uint32_t selectors_bit_mask = read_range->log_read->request.u.get.selectors_bit_mask;

        if(selectors_bit_mask != 0)
        {
            // Selector bit mask is used so simply copy indexes of all requested selectors

            uint32_t sel_index;

            for(sel_index = 0; selectors_bit_mask != 0; selectors_bit_mask >>= 1, sel_index++)
            {
                if((selectors_bit_mask & 1) != 0)
                {
                    // Selector's index is included in the bit mask so save it

                    sig_sel_indexes[req_num_signals++] = sel_index;
                }
            }
        }
        else
        {
            // If signal bit mask is used with an analog log then search all selectors for signals the user requested

            uint32_t const * const selectors        = read_range->priv->ad.analog.selectors;
            uint32_t         const num_sig_bufs     = read_range->log->num_sig_bufs;
            uint32_t               signals_bit_mask = read_range->log_read->request.u.get.signals_bit_mask;

            uint32_t sig_index;

            for(sig_index = 0; signals_bit_mask != 0; signals_bit_mask >>= 1, sig_index++)
            {
                if((signals_bit_mask & 1) != 0)
                {
                    // Signal is selected - search the selector array to see if it currently being logged

                    uint32_t sel_index;

                    for(sel_index = 0; sel_index < num_sig_bufs; sel_index++)
                    {
                        if(selectors[sel_index] == sig_index)
                        {
                            // Selector's index matches the requested signal so save selector index

                            sig_sel_indexes[req_num_signals++] = sel_index;

                            break;
                        }
                    }
                }
            }
        }
    }
    else
    {
        // Digital log - Return the digital signals defined in the bit mask

        uint32_t signals_bit_mask = read_range->log_read->request.u.get.signals_bit_mask;
        uint32_t sig_index;

        for(sig_index = 0; signals_bit_mask > 0; signals_bit_mask >>= 1, sig_index++)
        {
            if((signals_bit_mask & 1) != 0)
            {
                sig_sel_indexes[req_num_signals++] = sig_index;
            }
        }
    }

    return req_num_signals;
}


//! [8] Sets cycle selector according to the header type and log type
//!
//! NON-RT  logReadSetCycSel
//!
//! Note that read_range->log->cd.discontinuous.last_cyc_sel is initialized to log->num_cycles, which
//! is out of range (valid range for cyc_sel is 0 to num_cycles-1). So
//!
//! @param[out,in]  read_range    Pointer to read range structure
//!
//! @return  cycle selector

static uint32_t logReadSetCycSel(struct LOG_read_range * const read_range)
{
    bool     const log_is_continuous = ((read_range->log->meta & LOG_META_CONTINUOUS_BIT_MASK) != 0);
    bool     const log_is_cyclic     = ((read_range->log->meta & LOG_META_CYCLIC_BIT_MASK) != 0);
    uint32_t       cyc_sel           =   read_range->log_read->request.u.get.cyc_sel;

    // If non-cyclic request to a cyclic discontinuous log, then return data for the most recent cycle

    if(log_is_continuous == false && log_is_cyclic == true && cyc_sel == 0)
    {
        cyc_sel = read_range->log->cd.discontinuous.last_cyc_sel;
    }

    return cyc_sel;
}


//! [11] Calculates header size
//!
//! NON-RT  logReadHeaderSize
//!
//! @param[out,in]  read_range    Pointer to read range structure
//!
//! @return  Header size in bytes

static uint32_t logReadHeaderSize(struct LOG_read_range * const read_range)
{
    switch(read_range->log_read->request.action)
    {
        case LOG_READ_GET_PM_BUF:

            return sizeof(struct LOG_pm_buf_header);

        case LOG_READ_GET_SPY:

            return sizeof(struct SPY_v3_buf_header) + read_range->read_control->num_signals * sizeof(struct SPY_v3_sig_header);
    }

    // Header type: NONE

    return 0;
}


//! [13.1] Sets time to index fields in read range structure
//!
//! NON-RT  logReadSetTimeToIndex
//!
//! @param[in,out]  read_range    Pointer to read range structure

static void logReadSetTimeToIndex(struct LOG_read_range * const read_range)
{
    struct LOG_log const * const log = read_range->log;

    if((log->meta & LOG_META_CONTINUOUS_BIT_MASK) != 0)
    {
        // Continuous log - the time to index marker is the most recent sample

        // The most recent sample index can be copied atomically, but not the most recent sample time.
        // So we calculate the time of the most recent sample using last sample marker of cycles[0],
        // which holds the 1s time base.

        struct LOG_cycle * const time_base = &log->cycles[0].cycle[log->cycles[0].active];

        read_range->time_to_index.index = log->last_sample_marker.index;

        int32_t delta_index = read_range->time_to_index.index - time_base->last_sample_marker.index;

        if(delta_index < 0)
        {
            delta_index += log->num_samples_per_sig;
        }

        read_range->time_to_index.time_stamp = cctimeNsAddRT(time_base->last_sample_marker.time_stamp,
                                                             cctimeNsMulRT(read_range->period, delta_index));
    }
    else
    {
        // Discontinuous log - the time to index marker is always the last sample of the requested cycle

        read_range->time_to_index = read_range->cycle.last_sample_marker;
    }
}


//! [13.2] Calculates index of a sample with given time, which must be before the time_to_index time
//!
//! NON-RT  logReadTimeToIndex
//!
//! @param[in]  read_range    Pointer to read range structure
//! @param[in]  time_stamp    Sample time stamp
//!
//! @retval     LOG_INVALID_INDEX   if time_stamp is not in the range of valid data
//! @retval     index               Sample index corresponding to time_stamp_s

static uint32_t logReadTimeToIndex(struct LOG_read_range const * const read_range, struct CC_ns_time const time_stamp)
{
    // Protect against time_stamp being out of range - the time_to_index marker is the most recent sample.

    if(time_stamp.secs.abs == 0 || cctimeNsCmpAbsTimesRT(time_stamp, read_range->time_to_index.time_stamp) > 0)
    {
        ccprintf("\n  time_stamp.secs.abs=%u  cctimeNsCmpAbsTimesRT:%d\n"
                , time_stamp.secs.abs
                , cctimeNsCmpAbsTimesRT(time_stamp, read_range->time_to_index.time_stamp)
                );

        return LOG_INVALID_INDEX;
    }

    // Calculate offset in the buffer for the time_stamp and check it's not more than the number samples available

    int32_t const num_samples_per_sig = read_range->log->num_samples_per_sig;
    int32_t const offset = cctimeNsQuotientRT(cctimeNsSubRT(read_range->time_to_index.time_stamp, time_stamp), read_range->period);

    if(offset >= num_samples_per_sig)
    {
    char strbuf[22];
        ccprintf("\n  time_to_index.time_stamp=%u.%09d  time_stamp=%u.%09d  period=%u.%09d  Sub:%s  offset=%d  num_samples_per_sig:%d\n"
                , read_range->time_to_index.time_stamp.secs.abs
                , read_range->time_to_index.time_stamp.ns
                , time_stamp.secs.abs
                , time_stamp.ns
                , read_range->period.secs.abs
                , read_range->period.ns
                , cctimeNsPrintRelTime(cctimeNsSubRT(read_range->time_to_index.time_stamp, time_stamp), strbuf)
                , offset
                , num_samples_per_sig
                );

        return LOG_INVALID_INDEX;
    }

    // Adjust index by the offset and wrap if the start of the buffer is crossed

    uint32_t index = read_range->time_to_index.index;

    if(offset > index)
    {
        index += num_samples_per_sig;
    }

    return index - offset;
}


//! [13.3] Calculates time of the sample at given index in the log buffer
//!
//! NON-RT  logReadIndexToTime
//!
//! @param[in]  read_range  Pointer to read range structure
//! @param[in]  index       Sample index in the log buffer
//!
//! @return  Time stamp of the sample with given index

static struct CC_ns_time logReadIndexToTime(struct LOG_read_range const * const read_range, uint32_t const index)
{
    uint32_t delta_index;

    if(index == LOG_INVALID_INDEX)
    {
        // Return a time that is out of range of available data

        delta_index = read_range->log->num_samples_per_sig;
    }
    else
    {
        if(read_range->time_to_index.index < index)
        {
            delta_index = read_range->log->num_samples_per_sig;
        }
        else
        {
            delta_index = 0;
        }

        delta_index += read_range->time_to_index.index - index;
    }

    return cctimeNsSubRT(read_range->time_to_index.time_stamp,
                         cctimeNsMulRT(read_range->period, delta_index));
}


//! [14] Sets the timing fields in read_range based on the request and the log type.
//!
//! NON-RT  logReadSetTiming
//!
//! @param[out,in]  read_range    Pointer to read range structure

static void logReadSetTiming(struct LOG_read_range * const read_range)
{
    struct LOG_read_request const * const read_request          = &read_range->log_read->request;
    bool                            const log_is_discontinuous  = ((read_range->log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0);
    bool                            const req_is_cyclic         = (read_range->cyc_sel > 0);

    // 1. Set the time origin

    if(read_request->u.get.timing.time_origin.secs.abs > 0)
    {
        // Time origin provided so always use it

        read_range->time_origin = read_request->u.get.timing.time_origin;
    }
    else
    {
        // Time origin not provided

        if(req_is_cyclic == true || log_is_discontinuous == true)
        {
            // Cyclic access or log is discontinuous - set time origin to the start of the cycle/data

            read_range->time_origin = read_range->cycle.time_origin;
        }
        else
        {
            // Non-cyclic request to a continuous log
            //   Log frozen     : set time origin to the freeze time
            //   Log not-frozen : set time origin to the time of the most recent sample

            read_range->time_origin = read_range->log->cd.continuous.state == LOG_FROZEN
                                    ? read_range->log->cd.continuous.freeze_time_stamp
                                    : read_range->time_to_index.time_stamp;
        }
    }

    // 2,3. Set the time_offset and duration

    if(req_is_cyclic == false)
    {
        // Non-cycling request (cyc_sel = 0)

        // 2. Set time_offset
        //      Discontinuous log : Set the time_offset so that the first sample will be the first sample from the available data.
        //      Continuous log    : time_origin = 0 : Set the time_offset so that the last sample will be the most recent sample.
        //                        : time_origin > 0 : Set the time_offset so zero so that the last sample will be the time origin.

        read_range->time_offset = log_is_discontinuous == true
                                ? cctimeNsSubRT(read_range->cycle.first_sample_marker.time_stamp, read_range->time_origin)
                                :   read_request->u.get.timing.time_origin.secs.abs == 0
                                  ? cctimeNsSubRT(read_range->time_to_index.time_stamp, read_range->time_origin)
                                  : cctimeNsDoubleToRelTimeRT((cc_double)read_request->u.get.timing.time_offset_ms * 1.0E-3);

        // 3. Set duration
        //      duration_ms = 0 : Set duration to return the required number of post-mortem samples (which must be > 1)
        //      duration_ms > 0 : Set duration by converting duration_ms to seconds

        read_range->duration = read_request->u.get.timing.duration_ms == 0
                             ? cctimeNsMulRT(read_range->period, read_range->priv->num_postmortem_samples - 1)
                             : cctimeNsDoubleToRelTimeRT((cc_double)read_request->u.get.timing.duration_ms * 1.0E-3);
    }
    else
    {
        // Cyclic request - (cyc_sel > 0)

        // 2. Set time_offset by converting time_offset_ms to seconds

        read_range->time_offset = cctimeNsDoubleToRelTimeRT((cc_double)read_request->u.get.timing.time_offset_ms * 1.0E-3);

        // 3. Set duration by converting duration_ms to seconds

        read_range->duration = cctimeNsDoubleToRelTimeRT((cc_double)read_request->u.get.timing.duration_ms * 1.0E-3);
    }

    char time_offset_strbuf[22], duration_strbuf[22];

    ccprintf( "\n  time_origin=%u.%09d  time_offset=%s  duration=%s\n"
            , read_range->time_origin.secs.abs
            , read_range->time_origin.ns
            , cctimeNsPrintRelTime(read_range->time_offset, time_offset_strbuf)
            , cctimeNsPrintRelTime(read_range->duration, duration_strbuf)
            );
}


//! [15] Calculates the user's requested time of the first and the last sample to read out
//!
//! The actual first and last sample times will be the intersection of these times and the available data.
//!
//! NON-RT  logReadSetFirstAndLastSampleTimes
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @retval  LOG_READ_CONTROL_SUCCESS               Read request successful
//! @retval  LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES    Requested time range is less than one period

static uint32_t logReadSetFirstAndLastSampleTimes(struct LOG_read_range * const read_range)
{
    bool const log_is_discontinous = (read_range->log->meta & LOG_META_CONTINUOUS_BIT_MASK) == 0;
    bool const req_is_cyclic       = read_range->cyc_sel > 0;

    // Calculate the user's requested first and last sample times according to log type and request type:
    //
    //                     +-----------------------+
    //                     |        Log Type       |
    //                     +-----+-----+-----+-----+
    //                     |    CO     |    DC     |
    //                     +-----+-----+-----+-----+
    //                     | NC  |    CY     | NC  |
    // +--------------+----+=====+=====+=====+=====+
    // |              | NC |    LST    |    FST    |
    // | Request Type +----+-----+-----+     +-----+
    // |              | CY | N/A |    FST    | N/A |
    // +--------------+----+=====+=====+=====+=====+
    //
    // There are two alternative Timing Modes:
    //
    //  FST  = The first sample time is defined by the time origin plus the time offset
    //  LST  = The last sample time is defined by the time origin plus the time offset
    //
    //  FST: time origin
    //            |                 FST              LST
    //            |---time offset--->[<---duration--->]
    //            |
    //       If the duration is zero for a cyclic request, then the LST will
    //       be set to the end of the cycle.
    //
    //  LST:                                      time origin
    //            FST              LST                 |
    //             [<---duration--->]<---time offset---|
    //
    //       The duration must not be zero.

    read_range->timing_mode = (req_is_cyclic == true || log_is_discontinous == true)
                            ? LOG_TIMING_MODE_FST
                            : LOG_TIMING_MODE_LST;

    // Set requested FST and LST according to the timing mode

    struct CC_ns_time requested_first_sample_time;
    struct CC_ns_time requested_last_sample_time;

    switch(read_range->timing_mode)
    {
        case LOG_TIMING_MODE_FST:

            requested_first_sample_time = cctimeNsAddRT(read_range->time_origin, read_range->time_offset);
            requested_last_sample_time  = cctimeNsAddRT(requested_first_sample_time, read_range->duration);
            break;

        case LOG_TIMING_MODE_LST:

            requested_last_sample_time  = cctimeNsAddRT(read_range->time_origin, read_range->time_offset);
            requested_first_sample_time = cctimeNsSubRT(requested_last_sample_time, read_range->duration);
            break;
    }

    ccprintf( "\n  timing_mode=%u  requested_first_sample_time=%u.%09d  requested_last_sample_time=%u.%09d\n"
            , read_range->timing_mode
            , requested_first_sample_time.secs.abs
            , requested_first_sample_time.ns
            , requested_last_sample_time.secs.abs
            , requested_last_sample_time.ns
            );

    // Calculate the first and last sample times of the available data

    struct CC_ns_time data_first_sample_time;
    struct CC_ns_time data_last_sample_time;

    if(log_is_discontinous == true)
    {
        // Discontinuous log - available data is limited to one acquisition interval

        data_first_sample_time = read_range->cycle.first_sample_marker.time_stamp;
        data_last_sample_time  = read_range->cycle.last_sample_marker.time_stamp;
    }
    else
    {
        // Continuous log - available data runs up to the most recent sample for non-cyclic requests

        data_first_sample_time.secs.abs = 0;
        data_first_sample_time.ns = 0;
        data_last_sample_time  = req_is_cyclic == true
                               ? read_range->cycle.last_sample_marker.time_stamp
                               : read_range->time_to_index.time_stamp;
    }

    // Calculate the actual first and last sample times

    // First sample time : clip to start of available data

    read_range->first_sample.time_stamp = cctimeNsCmpAbsTimesRT(requested_first_sample_time, data_first_sample_time) >= 0
                                          ? requested_first_sample_time
                                          : data_first_sample_time;

    // Last sample time : a zero duration for a cyclic request or a discontinuous log returns data to the end of the cycle/data

    if(   cctimeNsCmpRelTimesRT(read_range->duration, cc_zero_ns) <= 0
       && (   req_is_cyclic == true
           || log_is_discontinous == true))
    {
        read_range->last_sample.time_stamp = read_range->cycle.last_sample_marker.time_stamp;
    }
    else
    {
        // Clip last sample time to end of available data

        read_range->last_sample.time_stamp = cctimeNsCmpAbsTimesRT(requested_last_sample_time, data_last_sample_time) <= 0
                                             ? requested_last_sample_time
                                             : data_last_sample_time;
    }

    char time_offset_strbuf[22], duration_strbuf[22];

    ccprintf( "\n  time_origin=%u.%09d  time_offset=%s  duration=%s\n"
              "  data_first_sample_time_s=%u.%09d   data_last_sample_time_s=%u.%09d\n"
              "  first_sample.time_stamp_s=%u.%09d  last_sample.time_stamp_s=%u.%09d\n"
            , read_range->time_origin.secs.abs
            , read_range->time_origin.ns
            , cctimeNsPrintRelTime(read_range->time_offset, time_offset_strbuf)
            , cctimeNsPrintRelTime(read_range->duration, duration_strbuf)
            , data_first_sample_time.secs.abs
            , data_first_sample_time.ns
            , data_last_sample_time.secs.abs
            , data_last_sample_time.ns
            , read_range->first_sample.time_stamp.secs.abs
            , read_range->first_sample.time_stamp.ns
            , read_range->last_sample.time_stamp.secs.abs
            , read_range->last_sample.time_stamp.ns
            );

    // Return error if the actual time range is not at least one log period

    if(cctimeNsCmpRelTimesRT(cctimeNsSubRT(read_range->last_sample.time_stamp, read_range->first_sample.time_stamp), read_range->period) < 0)
    {
        return LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES;
    }

    return LOG_READ_CONTROL_SUCCESS;
}


//! [16.1] Calculate the Sub-Sampling Period to avoid data being Overwritten (SSPO)
//!
//! NON-RT  logReadCalcSSPO
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @return Sub-Sampling Period base read rate to store rate ratio

static inline cc_double logReadCalcSSPO(struct LOG_read_range * const read_range)
{
    cc_double const num = cctimeNsRelTimeToDoubleRT(read_range->clipped_read_duration)
                        * (cc_double)(read_range->read_control->num_sig_bufs * LOG_VALUE_SIZE);

    cc_double const den = cctimeNsRelTimeToDoubleRT(read_range->period)
                        * cctimeNsRelTimeToDoubleRT(read_range->available_read_duration)
                        * (cc_double)read_range->log->log_mgr->read_rate;

    return num / den;
}


//! [16.2] Calculate the Sub-Sampling Period based on the maximum buffer Size. (SSPS)
//!
//! NON-RT  logReadCalcSSPS
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @return Sub-Sampling Period based on the maximum buffer size

static inline cc_double logReadCalcSSPS(struct LOG_read_range * const read_range)
{
    cc_double const period_s = cctimeNsRelTimeToDoubleRT(read_range->period);

    return (cctimeNsRelTimeToDoubleRT(read_range->clipped_read_duration) + period_s) / (period_s * (cc_double)read_range->max_samples);
}


//! [16] Checks the times of the first and the last samples to read out to see if the desired data
//!       was (or will be) overwritten before it can be read out, and if it can fit in the output data buffer.
//!       If the sub-sampling period is zero, then the function will set the period to avoid these limits.
//!
//! NON-RT  logReadCheckRangeValidity
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @retval  LOG_READ_CONTROL_SUCCESS               Read request successful
//! @retval  LOG_READ_CONTROL_LOG_OVERWRITTEN       Requested data was (or will be) overwritten
//! @retval  LOG_READ_CONTROL_BUFFER_TOO_SMALL      Requested data was too big to fit in the output buffer

static uint32_t logReadCheckRangeValidity(struct LOG_read_range * const read_range)
{
    uint32_t sub_sampling_period;
    uint32_t failure_status;

    struct LOG_log const * const log = read_range->log;

    // Check to see if the desired data has (or will be) overwritten before it can be read out

    if((log->meta & LOG_META_CONTINUOUS_BIT_MASK) != 0)
    {
        // Continuous log - calculate the available log duration by subtracting the read delay

        struct CC_ns_time const available_log_duration = cctimeNsSubRT(cctimeNsMulRT(read_range->period, log->num_samples_per_sig-1),
                                                                       cctimeMsToNsRT(cctimeMsAddDelayRT(cc_zero_ms, log->log_mgr->read_delay_ms)));

        if(cctimeNsCmpRelTimesRT(available_log_duration, read_range->period) <= 0)
        {
            return LOG_READ_CONTROL_LOG_OVERWRITTEN;
        }

        // Calculate the requested and available log duration

        struct CC_ns_time const requested_read_duration = cctimeNsSubRT(read_range->last_sample.time_stamp, read_range->first_sample.time_stamp);

        read_range->available_read_duration = cctimeNsAddRT(available_log_duration,
                                                            cctimeNsSubRT(read_range->last_sample.time_stamp, read_range->time_to_index.time_stamp));

        if(cctimeNsCmpRelTimesRT(requested_read_duration, read_range->available_read_duration) > 0)
        {
            // Requested log duration exceeds available log duration

            if(cctimeNsCmpRelTimesRT(read_range->available_read_duration, cc_zero_ns) <= 0)
            {
                // No data is available so return log overwritten error

                return LOG_READ_CONTROL_LOG_OVERWRITTEN;
            }

            // Clip read duration to what is available

            read_range->clipped_read_duration = read_range->available_read_duration;

            read_range->first_sample.time_stamp = cctimeNsSubRT(read_range->last_sample.time_stamp, read_range->clipped_read_duration);
        }
        else
        {
            // All requested data is available

            read_range->clipped_read_duration = requested_read_duration;
        }

        // Calculate sub-sampling period based on log overwrite and output buffer size limits

        cc_double const sspo = logReadCalcSSPO(read_range);
        cc_double const ssps = logReadCalcSSPS(read_range);

        sub_sampling_period = (uint32_t)ceil(sspo > ssps ? sspo : ssps);
        failure_status      = sspo > ssps ? LOG_READ_CONTROL_LOG_OVERWRITTEN : LOG_READ_CONTROL_BUFFER_TOO_SMALL;
    }
    else
    {
        // Discontinuous log

        if((log->cd.discontinuous.stored_samples_counter - read_range->cycle.latched_stored_counter) > log->num_samples_per_sig)
        {
            // Number of samples stored since this cycle was started is longer than the log, so the data has been overwritten.
            // Note: stored_samples_counter will roll over every 2^32 samples, so the condition will incorrectly conclude that the
            // data is valid during a short window in time, after each roll over.

            return LOG_READ_CONTROL_LOG_OVERWRITTEN;
        }

        // Calculate the read duration and the corresponding sub-sampling period that respects the output buffer size limit

        read_range->clipped_read_duration = cctimeNsSubRT(read_range->last_sample.time_stamp, read_range->first_sample.time_stamp);

        sub_sampling_period = (uint32_t)ceil(logReadCalcSSPS(read_range));
        failure_status      = LOG_READ_CONTROL_BUFFER_TOO_SMALL;
    }

    // Compare computed sub-sampling period with requested sub-sampling period
    // If the requested value is zero, then use the computed value.
    // If it is not zero, then check that it doesn't violate a limit.

    if(read_range->sub_sampling_period > 0)
    {
        // Application has specified the sub-sampling period

        if(read_range->sub_sampling_period < sub_sampling_period)
        {
            // Requested sub-sampling period will be cause data to be overwritten or the output buffer is too small

            return failure_status;
        }

        sub_sampling_period = read_range->sub_sampling_period;
    }

    read_range->read_control->sub_sampling_period = sub_sampling_period;

    CC_ASSERT(sub_sampling_period  > 0);

    return LOG_READ_CONTROL_SUCCESS;
}


//! [17.1] Calculates difference between two indexes in the log buffer
//!
//! NON-RT  logReadIndexPlusOffset
//!
//! @param[in]  read_range  Pointer to read range structure
//! @param[in]  index       Log index (unsigned)
//! @param[in]  offset      Offset from index (signed)
//!
//! @return  Return (index + offset) wrapped to be within the range of the buffer

static uint32_t logReadIndexPlusOffset(struct LOG_read_range const * const read_range, uint32_t const index, int32_t offset)
{
    offset += index;

    if(offset < 0)
    {
        return offset + read_range->log->num_samples_per_sig;
    }
    else if(offset >= read_range->log->num_samples_per_sig)
    {
        return offset - read_range->log->num_samples_per_sig;
    }

    return offset;
}


//! [17] Adjust the first/last sample times and indexes according to the sub-sampling period.
//!
//! NON-RT  logReadSubSample
//!
//! @param[in,out]  read_range   Pointer to intermediate read range structure
//!
//! @retval  LOG_READ_CONTROL_SUCCESS               Read request successful
//! @retval  LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES    Requested data has less then 2 samples

static uint32_t logReadSubSample(struct LOG_read_range * const read_range)
{
    struct LOG_read_control * const read_control = read_range->read_control;

    uint32_t const sub_sampling_period = read_control->sub_sampling_period;

    // Adjust the period and calculate the indexes for the first and last sample time stamps
    // (they will change if the sub-sampling removed the sample at that time)

    read_range->first_sample.index = logReadTimeToIndex(read_range, read_range->first_sample.time_stamp);
    read_range->last_sample.index  = logReadTimeToIndex(read_range, read_range->last_sample.time_stamp);

    if(   read_range->first_sample.index == LOG_INVALID_INDEX
       || read_range->last_sample.index  == LOG_INVALID_INDEX)
    {
        return LOG_READ_CONTROL_INVALID_INDEX;
    }

    // Calculate the delta index between the first and last samples

    uint32_t delta_index = 0;

    if(read_range->last_sample.index < read_range->first_sample.index)
    {
        // Last sample index is less than first sample index in the circular buffer so add log length to keep result positive

        delta_index = read_range->log->num_samples_per_sig;
    }

    delta_index += read_range->last_sample.index - read_range->first_sample.index;

    if(sub_sampling_period > 1)
    {
        // Sub-sampling is active - calculate delta index that matches sub-sampled period

        delta_index = sub_sampling_period * (delta_index / sub_sampling_period);

        switch(read_range->timing_mode)
        {
            case LOG_TIMING_MODE_FST:

                // Timing mode: First Sample Time - adjust last sample to match sub-sampling

                read_range->last_sample.index = logReadIndexPlusOffset(read_range, read_range->first_sample.index,  delta_index);
                break;

            case LOG_TIMING_MODE_LST:

                // Timing mode: Last Sample Time - adjust first sample to match sub-sampling

                read_range->first_sample.index = logReadIndexPlusOffset(read_range, read_range->last_sample.index, -delta_index);
                break;
        }
    }

    // Re-calculate the times from the indexes because the indexes may have changed

    read_range->first_sample.time_stamp = logReadIndexToTime(read_range, read_range->first_sample.index);
    read_range->last_sample.time_stamp  = logReadIndexToTime(read_range, read_range->last_sample.index);

    // Adjust period and number of samples to read from the log by the sub-sampling period

    read_control->sub_sampling_period  = sub_sampling_period;
    read_control->period = cctimeNsMulRT(read_control->period, sub_sampling_period);
    read_control->num_samples_from_log = delta_index / sub_sampling_period + 1;

    ccprintf( "\n  num_samples_from_log=%u  delta_index=%u  sub_sampling_period=%u\n"
            , read_control->num_samples_from_log
            , delta_index
            , sub_sampling_period
            );

    // For PM_BUF readout, the number of samples read must be exactly the number specified.
    // This may be more or less than the number of samples available in the log.

    if(read_range->log_read->request.action == LOG_READ_GET_PM_BUF)
    {
        read_control->num_samples = read_range->priv->num_postmortem_samples;

        if(read_control->num_samples_from_log > read_control->num_samples)
        {
            read_control->num_samples_from_log = read_control->num_samples;
        }
    }
    else
    {
        read_control->num_samples = read_control->num_samples_from_log;
    }

    if(read_control->num_samples_from_log < 2)
    {
        // At least two samples are needed in the log

        return LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES;
    }

    return LOG_READ_CONTROL_SUCCESS;
}


//! [21] Set logOutput variables in read_control
//!
//! NON-RT  logReadSetOutput
//!
//! The LOG_output structure in read_control guides logOutput() through the log buffer to find the
//! samples to readout. This supports stepping over rows when sub-sampled.
//! logOutput() transposes the signal data from columns in the circular log buffers to rows
//! in the returned data. To support this, logReadSetOutput calculates the offset in the circular
//! buffer to the first sample of each signal and initializes element_offset be the start_offset of the first signal.
//! It also calculates the step between values of the same signal.
//!
//! @param[in,out]  read_range     Pointer to read range structure

static void logReadSetOutput(struct LOG_read_range const * const read_range)
{
    struct LOG_read_control * const read_control        = read_range->read_control;
    struct LOG_output       * const output              = &read_control->output;
    uint32_t                  const num_sig_bufs        = read_range->log->num_sig_bufs;
    uint32_t                  const first_sample_offset = read_range->first_sample.index * num_sig_bufs;

    // Set the start_offset array according to the log type (analog or digital)

    if((read_range->log->meta & LOG_META_ANALOG_BIT_MASK) != 0)
    {
        // Analog log - calculate the start offset in the circular buffer for all the analog signals being read out

        uint32_t        const num_signals     = read_control->num_signals;
        int32_t const * const sig_sel_indexes = read_range->sig_sel_indexes;

        uint32_t sig_index;

        for(sig_index = 0; sig_index < num_signals; sig_index++)
        {
            output->start_offset[sig_index] = first_sample_offset + sig_sel_indexes[sig_index];
        }
    }
    else
    {
        // Digital log - there is only one 32-bit word per sample in the buffer

        output->start_offset[0] = first_sample_offset;
    }

    // Set element_offset to be the start_offset of the first signal

    output->element_offset = output->start_offset[0];

    // Set the step between elements of the same signal

    output->step = num_sig_bufs * read_control->sub_sampling_period;
}


//! [20.1.1] Copies a signal name string into the buffer and adds a trailing space
//!
//! NON-RT  logReadCatSigName
//!
//! This function is provided instead of using snprintf() because the printf family
//! adds a lot of code to the size of the resulting program, if it is not there already.
//!
//! @param[out]  buf        Pointer to buffer to receive the copy of the signal name
//! @param[in]   sig_name   Pointer to signal name string
//!
//! @return Number of characters added to buffer (includes the trailing space)

static uint32_t logReadCatSigName(char * buf, char const * sig_name)
{
    uint32_t    num_chars = 1;  // Initialize to 1 to account for the trailing space

    // Copy signal name into buffer, keeping track of the number of characters copied

    char  ch;

    while((ch = *(sig_name++)) != '\0')
    {
        *(buf++) = ch;
        num_chars++;
    }

    // Add trailing space and return the number of characters written to buf

    *buf = ' ';

    return num_chars;
}


//! [22.1] Prepares signal names header structure
//!
//! NON-RT  logReadHeaderSigNames
//!
//! For SIG_NAMES header type, it prepares a string containing the actual signal names for
//! the request, separated by spaces.
//!
//! For ALL_SIG_NAMES header type, it prepares a string containing all possible signals names
//! for the log, separated by spaces.
//!
//! @param[in]   read_range     Pointer to read range structure

static uint32_t logReadHeaderSigNames(struct LOG_read_range const * const read_range)
{
    struct LOG_read          * const log_read        = read_range->log_read;
    struct LOG_private const * const priv            = read_range->priv;
    int32_t            const * const sig_sel_indexes = read_range->sig_sel_indexes;

    uint32_t num_chars   = 0;
    uint32_t num_signals = read_range->read_control->num_signals;
    char   * sig_names   = log_read->header.sig_names;

    if((read_range->log->meta & LOG_META_ANALOG_BIT_MASK) != 0)
    {
        // Analog log

        struct LOG_priv_ana const * const analog = &priv->ad.analog;
        uint32_t sel_index;

        if(log_read->request.action == LOG_READ_GET_ALL_SIG_NAMES)
        {
            // ALL_SIG_NAMES : Return all signal names for this log

            num_signals = priv->num_signals;

            uint32_t sig_index;

            for(sig_index = 0; sig_index < num_signals; sig_index++)
            {
                num_chars += logReadCatSigName(sig_names + num_chars, analog->signals[sig_index].name);
            }
        }
        else
        {
            // SIG_NAMES : Return actual signal names for this request

            for(sel_index = 0; sel_index < num_signals; sel_index++)
            {
                num_chars += logReadCatSigName(sig_names + num_chars, analog->signals[analog->selectors[sig_sel_indexes[sel_index]]].name);
            }
        }
    }
    else
    {
        // Digital log - always return all the signal names

        uint32_t sig_index;

        for(sig_index = 0; sig_index < num_signals; sig_index++)
        {
            num_chars += logReadCatSigName(sig_names + num_chars, priv->ad.digital.signals[sig_sel_indexes[sig_index]].name);
        }
    }

    // Replace trailing space with a nul

    sig_names[--num_chars] = '\0';

    return num_chars;
}


//! [22.2] Prepares PM_BUF header structure
//!
//! NON-RT  logReadHeaderPmBuf
//!
//! @param[in]   read_range     Pointer to read range structure
//! @param[out]  header         Pointer to pm_buf header structure to complete

static void logReadHeaderPmBuf(struct LOG_read_range const * const read_range,
                               struct LOG_pm_buf_header    * const header)
{
    struct LOG_read_control const * const read_control = read_range->read_control;

    header->first_sample_time = read_control->first_sample_time;
    header->period            = read_control->period;

    ccprintf("first_sample_time:%u.%09d (0x%08X.0x%08X)   period=%u.%09d (0x%08X.0x%08X)\n"
            , header->first_sample_time.secs.abs
            , header->first_sample_time.ns
            , header->first_sample_time.secs.abs
            , header->first_sample_time.ns
            , header->period.secs.abs
            , header->period.ns
            , header->period.secs.abs
            , header->period.ns
            );
}


//! [22.3] Prepares Spy header structure
//!
//! NON-RT  logReadHeaderSpy
//!
//! @param[in]   read_range     Pointer to read range structure
//! @param[out]  header         Pointer to SPY header structure to complete

static void logReadHeaderSpy(struct LOG_read_range const * const read_range,
                             struct LOG_spy_header       * const header)
{
    // Prepare Spy buffer header

    struct LOG_read_control const * const read_control = read_range->read_control;
    uint32_t                        const num_signals  = read_control->num_signals;
    uint32_t                        const log_meta     = read_range->log->meta;
    bool                            const ana_signals  = (log_meta & LOG_META_ANALOG_BIT_MASK) != 0;

    spySetBufHeader(&header->buf_header,
                    (log_meta & LOG_META_LITTLE_ENDIAN_BIT_MASK) != 0,
                    ana_signals,
                    false,          // ana_sigs_in_columns
                    false,          // timestamp_is_lst
                    num_signals,
                    read_control->num_samples,
                    read_control->time_origin.secs.abs,
                    read_control->time_origin.ns,
                    read_control->first_sample_time.secs.abs,
                    read_control->first_sample_time.ns,
                    read_control->period.secs.rel,
                    read_control->period.ns);

    // Prepare Spy signal headers

    struct LOG_private  const * const priv            = read_range->priv;
    int32_t             const * const sig_sel_indexes = read_range->sig_sel_indexes;
    struct SPY_v3_sig_header  *       sig_header      = header->sig_headers;

    if(ana_signals == true)
    {
        // Analog log - traverse all requested analog selectors

        uint32_t sel_index;

        for(sel_index = 0; sel_index < num_signals; sel_index++, sig_header++)
        {
            struct LOG_priv_ana_signal const * const ana_signal = &priv->ad.analog.signals[priv->ad.analog.selectors[sig_sel_indexes[sel_index]]];

            int32_t const time_offset_ns = ana_signal->time_offset != NULL
                                         ? (int32_t)roundf(*ana_signal->time_offset * 1.0E+9F)
                                         : 0;

            spySetAnaSigHeader(sig_header,
                              (ana_signal->meta & SPY_SIG_META_STEPS) != 0,
                               false,                               // use_scaling
                               SPY_TYPE_FLOAT,
                               LOG_VALUE_SIZE,
                               0.0F,                                // gain
                               0.0F,                                // offset
                               time_offset_ns,
                               ana_signal->name,
                               ana_signal->units);
        }
    }
    else
    {
        // Digital log - traverse all requested digital signals

        uint32_t sig_index;

        for(sig_index = 0; sig_index < num_signals; sig_index++, sig_header++)
        {
            uint32_t                           const dig_bit_index = sig_sel_indexes[sig_index];
            struct LOG_priv_dig_signal const * const dig_signal    = &priv->ad.digital.signals[dig_bit_index];

            int32_t const time_offset_us = dig_signal->time_offset != NULL
                                         ? (int32_t)roundf(*dig_signal->time_offset * 1.0E+6F)
                                         : 0;

            spySetDigSigHeader(sig_header,
                               dig_bit_index,
                               time_offset_us,
                               dig_signal->name);
        }
    }
}


// Global functions

// NON-RT  logReadRequest

void logReadRequest(struct LOG_read * const log_read, struct LOG_read_control * const read_control)
{
    struct LOG_read_request const * const read_request = &log_read->request;
    struct LOG_log                * const log = &log_read->log[read_request->log_index];

    CC_ASSERT(read_request->log_index < log_read->log->log_mgr->num_logs);

    ccprintf( "\n  log_index=%u  action=%u\n"
            , read_request->log_index
            , read_request->action
            );

    // [0] Check action to store a signal's name and/or units

    if(read_request->action == LOG_READ_STORE_SIG_NAMES_AND_UNITS)
    {
        ccprintf( "LOG_READ_STORE_SIG_NAMES_AND_UNITS\n  sig_index=%u  sig_name='%s'  units='%s'\n"
                , read_request->u.set.sig_index
                , read_request->u.set.sig_name
                , read_request->u.set.units
                );

        logStoreSigNameAndUnits(log,read_request->u.set.sig_index,read_request->u.set.sig_name,read_request->u.set.units);
        return;
    }

    // [1] Get pointers to read_request and the log structure and reset log header and read_control

    ccprintf( "START\n  cyc_sel=%u  time_origin=%u.%09d  prev_first_sample_time=%u.%09d\n"
              "  time_offset_ms=%i  duration_ms=%u  sub_sampling=%u\n"
            , read_request->u.get.cyc_sel
            , read_request->u.get.timing.time_origin.secs.abs
            , read_request->u.get.timing.time_origin.ns
            , read_request->u.get.timing.prev_first_sample_time.secs.abs
            , read_request->u.get.timing.prev_first_sample_time.ns
            , read_request->u.get.timing.time_offset_ms
            , read_request->u.get.timing.duration_ms
            , read_request->u.get.sub_sampling_period
            );

    memset(&log_read->header, 0, sizeof(union  LOG_header));
    memset(read_control,      0, sizeof(struct LOG_read_control));

    // [2] Initialize part of read_control

    read_control->status  = LOG_READ_CONTROL_SUCCESS;
    read_control->meta    = log->meta;
    read_control->buf_len = log->priv->sig_bufs_len;
    read_control->period  = logPeriodGet(log->log_mgr, read_request->log_index);

    // [3] Validate cycle selector is in range

    if(read_request->u.get.cyc_sel >= log->num_cycles)
    {
        read_control->status = LOG_READ_CONTROL_INVALID_CYC_SEL;
        return;
    }

    // [4] Initialize read_range structure with pointers to the other structures

    struct LOG_read_range read_range;

    memset(&read_range, 0, sizeof(read_range));

    read_range.log_read            = log_read;
    read_range.read_control        = read_control;
    read_range.log                 = log;
    read_range.priv                = log->priv;
    read_range.sub_sampling_period = read_request->u.get.sub_sampling_period;

    // [5] Check if log is disabled

    if((log->log_mgr->disabled_logs_mask & (1 << read_request->log_index)) != 0)
    {
        read_control->status = logReadLogDisabled(&read_range);
        return;
    }

    // [6] Get a sorted array of requested digital signal or analog selector indexes and set number of requested signals

    read_control->num_signals = logReadSetSigSelIndexes(&read_range);

    if(read_control->num_signals == 0)
    {
        read_control->status = LOG_READ_CONTROL_NO_SIGNALS;
        return;
    }

    if(   read_request->action == LOG_READ_GET_SIG_NAMES
       || read_request->action == LOG_READ_GET_ALL_SIG_NAMES)
    {
        // (ALL_)SIG_NAMES header requested - returns the list of the signal names

        read_control->header_size = logReadHeaderSigNames(&read_range);
        return;
    }

    // [7] Set num_sig_bufs for this request

    read_control->num_sig_bufs = (log->meta & LOG_META_ANALOG_BIT_MASK) != 0
                               ? read_control->num_signals                     // analog private  - one signal buffer per signal
                               : 1;                                            // digital private - one signal buffer for all signals

    // [8] Set read_range.cyc_sel

    read_range.cyc_sel = logReadSetCycSel(&read_range);

    if(read_range.cyc_sel >= log->num_cycles)
    {
        // cyc_sel is out of range - this can only happen for non-cyclic access to cyclic discontinuous log,
        // due to no data being stored since last_cyc_sel is initialized to log->num_cycles

        read_control->status = LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES;
        return;
    }

    // [9] Set read_range.cycle from the active (last finished) cycle
    //     For continuous logs, if cyc_sel is zero then this gets the last 1s time base

    read_range.cycle = log->cycles[read_range.cyc_sel].cycle[log->cycles[read_range.cyc_sel].active];

    ccprintf( "READ RANGE\n  read_range.cyc_sel=%u  cycles[read_range.cyc_sel].active=%u\n"
              "  cycle.num_samples=%u  cycle.first_sample.time_stamp_s=%u.%09d  cycle.last_sample.time_stamp_s=%u.%09d\n"
            , read_range.cyc_sel
            , log->cycles[read_range.cyc_sel].active
            , read_range.cycle.num_samples
            , read_range.cycle.first_sample_marker.time_stamp.secs.abs
            , read_range.cycle.first_sample_marker.time_stamp.ns
            , read_range.cycle.last_sample_marker.time_stamp.secs.abs
            , read_range.cycle.last_sample_marker.time_stamp.ns
            );

    if(read_range.cycle.num_samples < 2)
    {
        // At least two samples are needed in the log to calculate the period

        read_control->status = LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES;
        return;
    }

    // [10] Set read_range.period

    read_range.period = read_control->period;

    // [11] Calculate header size based on the number of requested signals

    read_control->header_size = logReadHeaderSize(&read_range);

    // [12] Set read_range.max_samples from max_buffer_size if not zero, otherwise set max_samples to maximum 32-bit unsigned integer value

    if(read_request->u.get.max_buffer_size > 0 && read_request->u.get.max_buffer_size <= INT32_MAX)
    {
        // Max buffer size is defined - calculate the maximum number of samples that can be returned

        read_range.max_samples = (read_request->u.get.max_buffer_size - read_control->header_size) / (read_control->num_sig_bufs * LOG_VALUE_SIZE);

        CC_ASSERT(read_range.max_samples > 1);
    }
    else
    {
        // max_buffer_size is zero, or out of range to set to max value

        read_range.max_samples = INT32_MAX;
    }

    // [13] Set read_range.time_to_index marker

    logReadSetTimeToIndex(&read_range);

    // [14] Set timing fields in read_range (time_origin, time_offset, duration)

    logReadSetTiming(&read_range);

    // [15] Set read_range.first_sample and read_range.last_sample indexes

    read_control->status = logReadSetFirstAndLastSampleTimes(&read_range);

    if(read_control->status != LOG_READ_CONTROL_SUCCESS)
    {
        return;
    }

    // [16] Check the validity of read_range.first_sample and read_range.last_sample

    read_control->status = logReadCheckRangeValidity(&read_range);

    if(read_control->status != LOG_READ_CONTROL_SUCCESS)
    {
        return;
    }

    // [17] Sub-sample the data if required by the application or to fit in the maximum buffer size (if supplied)

    read_control->status = logReadSubSample(&read_range);

    if(read_control->status != LOG_READ_CONTROL_SUCCESS)
    {
        return;
    }

    // [18] Check if first sample time matches the previous first sample time and report NO_NEW_DATA if so

    if(cctimeNsCmpAbsTimesRT(read_range.first_sample.time_stamp, read_request->u.get.timing.prev_first_sample_time) == 0)
    {
        read_control->status = LOG_READ_CONTROL_NO_NEW_DATA;
        return;
    }

    // [19] Return in read_control

    read_control->time_origin       = read_range.time_origin;
    read_control->first_sample_time = read_range.first_sample.time_stamp;
    read_control->last_sample_time  = read_range.last_sample.time_stamp;

    // [20] Finish the read_control structure

    read_control->buf_offset = log->priv->sig_bufs_offset;
    read_control->data_len   = read_control->num_sig_bufs * read_control->num_samples;
    read_control->data_size  = read_control->data_len * LOG_VALUE_SIZE;
    read_control->total_size = read_control->header_size + read_control->data_size;

    // [21] Set up the logOutput variables in read_control

    logReadSetOutput(&read_range);

    // [22] Prepare the requested type of header

    switch(read_request->action)
    {
        case LOG_READ_GET_PM_BUF:

            logReadHeaderPmBuf(&read_range, &log_read->header.pm_buf);
            break;

        case LOG_READ_GET_SPY:

            logReadHeaderSpy(&read_range, &log_read->header.spy);
            break;

        default:

            break;
    }
}

// EOF
