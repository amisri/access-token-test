//! @file  logClient.h
//! @brief Converter Control Logging library client header file
//!
//! This file separates the structures and functions that might be needed on the MCU
//! of a dual CPU platform. It has access to the log menus and log manager structures,
//! but not the logs themselves, which might be private to a DSP. Communication
//! between the CPUs is restricted to two integer-only arrays, mapped to the
//! read request and read control structures.
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "cclibs.h"
#include "libcctime.h"
#include "spy_v3.h"
#include "log_menu_status.h"

// Liblog constants

#define LOG_VALUE_SIZE              4                               //!< Size in bytes of liblog log values (float for analog and uint32_t for digital)
#define LOG_MAX_NUM_LOGS            32                              //!< Maximum number of logs
#define LOG_MAX_SIGS_PER_LOG        32                              //!< Maximum number of signals that can be associated with one log
#define LOG_MENU_NAME_LEN           14                              //!< Length of a log name field (1 byte is needed for nul termination)
#define LOG_SIG_NAME_LEN            20                              //!< Length of signal name field (1 byte is needed for nul termination)
#define LOG_UNITS_LEN               8                               //!< Length of units field (1 byte is needed for nul termination)
#define LOG_INVALID_ANA_VALUE      -1.23456                         //!< Value stored if an analog signal is invalid (Nan or > +/-MAX_ANA_VALUE)

#define LOG_SIG_NAMES_LEN          (LOG_MAX_SIGS_PER_LOG*SPY_SIG_NAME_LEN) //!< Max length of MENU or MPX_MENU signal list header

#define LOG_HEADER_SIZE            (sizeof(union LOG_header))
#define LOG_HEADER_LEN             (sizeof(union LOG_header)/LOG_VALUE_SIZE)
#define LOG_READ_REQUEST_LEN       (sizeof(struct LOG_read_request)/sizeof(uint32_t))
#define LOG_READ_CONTROL_LEN       (sizeof(struct LOG_read_control)/sizeof(uint32_t))
#define LOG_MGR_LEN                (sizeof(struct LOG_mgr)/sizeof(uint32_t))

// Enums

enum LOG_read_action                                                //!< Log read action
{
    LOG_READ_STORE_SIG_NAMES_AND_UNITS       ,                      //!< Store signal name and/or units
    LOG_READ_GET                             ,                      //!< Get log data with no header
    LOG_READ_GET_PM_BUF                      ,                      //!< Get PM_BUF data with PM_BUF header
    LOG_READ_GET_SPY                         ,                      //!< Get log data with SPY header
    LOG_READ_GET_SIG_NAMES                   ,                      //!< Get actual signal names for a log menu
    LOG_READ_GET_ALL_SIG_NAMES               ,                      //!< Get all signal names for an MPX log menu
};

enum LOG_read_control_status                                        //!< Read control status
{
    LOG_READ_CONTROL_DISABLED                ,                      //!< read_control buffer disabled
    LOG_READ_CONTROL_SUCCESS                 ,                      //!< Request successful
    LOG_READ_CONTROL_NO_SIGNALS              ,                      //!< No requested signals are currently being logged
    LOG_READ_CONTROL_INVALID_CYC_SEL         ,                      //!< Invalid cycle selector: out of range
    LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES      ,                      //!< Not enough samples in the buffer - at least two are required
    LOG_READ_CONTROL_LOG_OVERWRITTEN         ,                      //!< Requested data overwritten
    LOG_READ_CONTROL_BUFFER_TOO_SMALL        ,                      //!< Buffer with declared size couldn't hold all post-mortem data
    LOG_READ_CONTROL_INVALID_INDEX           ,                      //!< Request resulted in an invalid array index
    LOG_READ_CONTROL_NO_NEW_DATA             ,                      //!< No new data - previous first_sample_time matches first_sample_time
};

enum LOG_meta_bit
{
    LOG_META_LITTLE_ENDIAN                   ,                      //!< Log data is little endian
    LOG_META_CYCLIC                          ,                      //!< Log is cyclic
    LOG_META_ANALOG                          ,                      //!< Log is analog (rather than digital)
    LOG_META_POSTMORTEM                      ,                      //!< Log is part of the post-mortem
    LOG_META_CONTINUOUS                      ,                      //!< Log is continuous (rather than discontinuous)
    LOG_META_FREEZABLE                       ,                      //!< Log is freezable (only an option for continuous logs)
    LOG_META_DISABLED                        ,                      //!< Log is disabled
    LOG_NUM_META_BITS
};

enum LOG_meta_bit_mask
{
    LOG_META_LITTLE_ENDIAN_BIT_MASK   = (1 << LOG_META_LITTLE_ENDIAN  )  ,
    LOG_META_CYCLIC_BIT_MASK          = (1 << LOG_META_CYCLIC         )  ,
    LOG_META_ANALOG_BIT_MASK          = (1 << LOG_META_ANALOG         )  ,
    LOG_META_POSTMORTEM_BIT_MASK      = (1 << LOG_META_POSTMORTEM     )  ,
    LOG_META_CONTINUOUS_BIT_MASK      = (1 << LOG_META_CONTINUOUS     )  ,
    LOG_META_FREEZABLE_BIT_MASK       = (1 << LOG_META_FREEZABLE      )  ,
    LOG_META_DISABLED_BIT_MASK        = (1 << LOG_META_DISABLED       )  ,
};

// Log manager structure

//! The log manager structure will be initialized by the auto-generated header file logStructsInit.h.
//! On a dual CPU platform, it must be visible to both CPUs - this is why it only uses uint32_t.

struct LOG_mgr
{
    uint32_t                    num_logs;                           //!< [ 0] Number of logs
    uint32_t                    read_delay_ms;                      //!< [ 1] Log data read delay in ms (time before first byte is read). Common for all logs.
    uint32_t                    read_rate;                          //!< [ 2] Log data read rate in bytes/second. Common for all logs.
    uint32_t                    cyclic_logs_mask;                   //!< [ 3] Mask for logs which are cyclic
    uint32_t                    analog_logs_mask;                   //!< [ 4] Mask for logs which are analog
    uint32_t                    postmortem_logs_mask;               //!< [ 5] Mask for logs which are part of the post-mortem
    uint32_t                    continuous_logs_mask;               //!< [ 6] Mask for logs which are continuous
    uint32_t                    freezable_logs_mask;                //!< [ 7] Mask for logs which are freezable (only an option for continuous logs)
    uint32_t                    running_logs_mask;                  //!< [ 8] Bit set to 1 means that the freezable or discontinuous log is recording data
    uint32_t                    disabled_logs_mask;                 //!< [ 9] Bit set to 1 means that the log is disabled
    uint32_t                    freeze_mask;                        //!< [10] Bit set to 1 means that the (freezable) log should freeze
    uint32_t                    postmortem_logs_frozen;             //!< [11] True if all PM logs are frozen
    uint32_t                    cur_cyc_sel;                        //!< [12] Cycle selector of the current cycle
    uint32_t                    prev_cyc_sel;                       //!< [13] Cycle selector of the previous cycle
    struct CC_ns_time           period[LOG_MAX_NUM_LOGS];           //!< [14] Log sampling periods (s + ns)
};

// Structure to support reading log signals

//! Read request to library
//!
//! max_buffer_size - non-zero value will trigger automatic sub-sampling if sub_sampling_period is zero.
//!
//! sub_sampling_period - set to zero for automatic sub-sampling to fix within max_buffer_size, or to satisfy
//! the read out before being overwritten.
//!
//! Signals and selectors bit masks - only one can be used at a time. The other has to be set to 0. For digital logs,
//! only signals_bit_mask may be used.
//!
//! Implementation notes:
//! * This structure should contain 32 bit integers only - then it can be easily mapped to a property.
//! * Header_type is effectively an enum, as it uses enum LOG_read_header.
//!   However, the underlying enum type depends on the platform, that's why we force it to be a 32 bit int.

struct LOG_read_timing
{
    struct CC_ns_time           time_origin;                        //!<  [3] Time origin (unix time + ns time)
    int32_t                     time_offset_ms;                     //!<  [5] Time to return data relative to the time origin (ms)
    uint32_t                    duration_ms;                        //!<  [6] Duration of data to return (ms)
    struct CC_ns_time           prev_first_sample_time;             //!<  [7] Previous first sample time (unix time + ns time)
};                                                                  //   (24) sizeof(struct LOG_read_timing)

struct LOG_read_set
{
    uint32_t                    sig_index;                          //!<  [2] Bit mask identifying all requested signals
    char                        sig_name[LOG_SIG_NAME_LEN];         //!<  [3] External signal name (empty to leave old name unchanged)
    char                        units[LOG_UNITS_LEN];               //!<  [8] External units (empty to leave old units unchanged)
};                                                                  //   (32) sizeof(struct LOG_read_set)

struct LOG_read_get
{
    uint32_t                    cyc_sel;                            //!<  [2] Cycle selector
    struct LOG_read_timing      timing;                             //!<  [3] Previous first sample time (unix time + us time)
    uint32_t                    selectors_bit_mask;                 //!<  [9] Bit mask identifying all requested selectors
    uint32_t                    signals_bit_mask;                   //!< [10] Bit mask identifying all requested signals
    uint32_t                    max_buffer_size;                    //!< [11] Max buffer size for header + data in bytes
    uint32_t                    sub_sampling_period;                //!< [12] Sub sampling period (0=calc based on max_size, 1-N=required sub-sampling period)
};                                                                  //   (44) sizeof(struct LOG_read_get)

struct LOG_read_request
{
    uint32_t                    log_index;                          //!<  [0] Log index
    uint32_t                    action;                             //!<  [1] Read request action (use enum LOG_read_request_action)
    union
    {
        struct LOG_read_set     set;                                //!<  [2] Set of externally provided signal name or units
        struct LOG_read_get     get;                                //!<  [2] Get of log header or data
    } u;
};                                                                  //   (52) sizeof(struct LOG_read_request)

//! Response to read request

//! Implementation notes:
//! * These structure should contain 32 bit integers only - then it can be easily mapped to a property.
//! * status is effectively an enum, as it uses enum LOG_read_control_status.
//!   However, the underlying enum type depends on the platform, that's why we force it to be a 32-bit integer.

struct LOG_output
{
    uint32_t                    total_values_returned;              //!< [20] Number of values returned
    uint32_t                    element_offset;                     //!< [21] Element offset in circular buffer, initialized to start_offset[0]
    uint32_t                    step;                               //!< [22] Step between elements
    uint32_t                    start_offset[LOG_MAX_SIGS_PER_LOG]; //!< [23] Offset to first element for each analog signal
};

struct LOG_read_control
{
    uint32_t                    status;                             //!<  [0] Status of the request (use enum LOG_read_control_status)
    struct CC_ns_time           time_origin;                        //!<  [1] Time origin / start of cycle (Unix time + ns time)
    struct CC_ns_time           first_sample_time;                  //!<  [3] Time of first sample (Unix time + ns time)
    struct CC_ns_time           last_sample_time;                   //!<  [5] Time of last sample (Unix time + ns time)
    struct CC_ns_time           period;                             //!<  [7] Sampling period (s + ns)
    uint32_t                    meta;                               //!<  [9] LOG_META bits:LOG_META_POSTMORTEM_BIT_MASK will be set if POST_MORTEM header requested
    uint32_t                    num_signals;                        //!< [10] Number of signals returned
    uint32_t                    num_sig_bufs;                       //!< [11] Number of signal buffers for this read request (always 1 for digital logs)
    uint32_t                    num_samples;                        //!< [12] Number of samples per signal that will be returned
    uint32_t                    num_samples_from_log;               //!< [13] Number of samples per signal that can come from the log (zero pad the rest - analog logs only)
    uint32_t                    sub_sampling_period;                //!< [14] Sub-sampling period (in units of the log period)
    uint32_t                    header_size;                        //!< [15] Header size in bytes
    uint32_t                    data_size;                          //!< [16] Data size in bytes
    uint32_t                    total_size;                         //!< [17] Size in bytes of Header + Data
    uint32_t                    data_len;                           //!< [18] Data length in elements
    uint32_t                    buf_offset;                         //!< [19] Offset in elements in log buffers to start of the buffer for this log
    uint32_t                    buf_len;                            //!< [20] Length in elements of the log's buffer
    struct LOG_output           output;                             //!< [21] logOutput() variables
};                                                                  //  (221) sizeof(struct LOG_read_control)

// Header for PM_BUF readout

struct LOG_pm_buf_header
{
    struct CC_ns_time           first_sample_time;                  //!< [0] Time of first sample of this signal (Unix time + ns)
    struct CC_ns_time           period;                             //!< [8] Period (s + ns)
};                                                                  //   16 bytes in total

// Header for Spy readout

struct LOG_spy_header
{
    struct SPY_v3_buf_header    buf_header;                         //!< [ 0] Buffer header
    struct SPY_v3_sig_header    sig_headers[LOG_MAX_SIGS_PER_LOG];  //!< [32] Signal headers
};                                                                  //   1312 bytes in total


// Header union

union LOG_header                                                    //!< Union of all possible log headers
{
    uint8_t                      byte[1];                           //!< [   1] Byte access to header
    struct LOG_pm_buf_header     pm_buf;                            //!< [  16] PM_BUF header
    struct LOG_spy_header        spy;                               //!< [1312] Spy header
    char                         sig_names[LOG_SIG_NAMES_LEN];      //!< [ 640] Signal names header
};

// Static inline functions

// Access to current and previous cycle selectors

static inline uint32_t logCurrentCycSel(const struct LOG_mgr * const log_mgr)
{
    return log_mgr->cur_cyc_sel;
}

static inline uint32_t logPrevCycSel(const struct LOG_mgr * const log_mgr)
{
    return log_mgr->prev_cyc_sel;
}


// Log types and status by log index

static inline bool logIsCyclic(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->cyclic_logs_mask & (1 << log_index)) != 0;
}

static inline bool logIsAnalog(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->analog_logs_mask & (1 << log_index)) != 0;
}

static inline bool logIsPostmortem(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->postmortem_logs_mask & (1 << log_index)) != 0;
}

static inline bool logIsContinuous(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->continuous_logs_mask & (1 << log_index)) != 0;
}

static inline bool logIsFreezable(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->freezable_logs_mask & (1 << log_index)) != 0;
}

static inline bool logIsDisabled(const struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return (log_mgr->disabled_logs_mask & (1 << log_index)) != 0;
}

static inline bool logArePostmortemLogsFrozen(const struct LOG_mgr * const log_mgr)
{
    return log_mgr->postmortem_logs_frozen != 0;
}


// Freeze/unfreeze logs functions

static inline void logFreezePostmortemLogs(struct LOG_mgr * const log_mgr)
{
    log_mgr->freeze_mask |= (log_mgr->freezable_logs_mask & log_mgr->postmortem_logs_mask & ~log_mgr->disabled_logs_mask);
}

static inline void logUnfreezePostmortemLogs(struct LOG_mgr * const log_mgr)
{
    log_mgr->freeze_mask &= ~(log_mgr->freezable_logs_mask & log_mgr->postmortem_logs_mask);
}

static inline void logFreezeLog(struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    log_mgr->freeze_mask |= (log_mgr->freezable_logs_mask & (1 << log_index));
}

static inline void logUnfreezeLog(struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    log_mgr->freeze_mask &= ~(1 << log_index);
}

static inline uint32_t logFreezeMaskGet(struct LOG_mgr * const log_mgr)
{
    return log_mgr->freeze_mask;
}

static inline uint32_t logPostmortemMaskGet(const struct LOG_mgr * const log_mgr)
{
    return log_mgr->postmortem_logs_mask;
}


// Enable/Disable a log

static inline void logEnable(struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    log_mgr->disabled_logs_mask &= ~(1 << log_index);
}

static inline void logDisable(struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    log_mgr->disabled_logs_mask |= (1 << log_index);
}


// Get/Set log sampling periods

static inline struct CC_ns_time logPeriodGet(struct LOG_mgr * const log_mgr, uint32_t const log_index)
{
    return log_mgr->period[log_index];
}

static inline void logPeriodSet(struct LOG_mgr * const log_mgr, uint32_t const log_index, struct CC_ns_time const period)
{
    log_mgr->period[log_index] = period;
}


// Check if logReadOutput() has returned all the data

static inline bool logReadOutputFinished(struct LOG_read_control * const read_control)
{
    return read_control->output.total_values_returned >= read_control->data_len;
}


//! Checks if all post-mortem logs just froze
//!
//! Continuous logs are considered frozen if the state is FROZEN.
//! Discontinuous logs are not "freezable" but are considered frozen if not running.
//!
//! This function returns true only on the iteration when the last post-mortem log transitions
//! from not frozen to frozen. This can be used as a signal to trigger the post-mortem log
//! readout.
//!
//! @param[in,out]  log_mgr  Pointer to log manager structure
//!
//! @retval  true if all post-mortem logs just froze

static inline bool logPostmortemLogsFroze(struct LOG_mgr * const log_mgr)
{
    bool const postmortem_logs_frozen = ((   log_mgr->postmortem_logs_mask
                                          &  log_mgr->running_logs_mask
                                          & ~log_mgr->disabled_logs_mask
                                          & (log_mgr->freezable_logs_mask | ~log_mgr->continuous_logs_mask)) == 0);

    bool const postmortem_logs_froze  = (postmortem_logs_frozen == true && log_mgr->postmortem_logs_frozen == false);

    log_mgr->postmortem_logs_frozen = postmortem_logs_frozen;

    return postmortem_logs_froze;
}


//! Change signal name in SPY header
//!
//! This function allows the signal name for a signal identified in a SPY header to be replaced.
//! If the new name is too long (length including null >= SPY_SIG_NAME_LEN) then it will be truncated.
//!
//! @param[in,out]  spy_header       Pointer to SPY header structure
//! @param[in]      sig_index        Index of signal whose name should be changed
//! @param[in]      new_signal_name  Pointer to new signal name

static inline void logChangeSpySignalName(struct LOG_spy_header * const spy_header,
                                          uint32_t                const sig_index,
                                          char            const * const new_signal_name)
{
    CC_ASSERT(sig_index < spy_header->buf_header.num_signals);

    strncpy(spy_header->sig_headers[sig_index].name, new_signal_name, (SPY_SIG_NAME_LEN - 1));
}


//! Change signal name in SPY header with byte swapping
//!
//! This function allows the signal name for a signal identified in a SPY header to be replaced.
//! If the new name is too long (length including null >= SPY_SIG_NAME_LEN) then it will be truncated.
//! The bytes within each long word are reversed.
//!
//! @param[in,out]  spy_header       Pointer to SPY header structure
//! @param[in]      sig_index        Index of signal whose name should be changed
//! @param[in]      new_signal_name  Pointer to new signal name

static inline void logChangeSpySignalNameSwap(struct LOG_spy_header * const spy_header,
                                              uint32_t                const sig_index,
                                              char            const *       new_signal_name)
{
    CC_ASSERT(sig_index < spy_header->buf_header.num_signals);


    uint32_t     index    = 0;
    char * const name_buf = spy_header->sig_headers[sig_index].name;
    char         name_ch;

    while(index < (SPY_SIG_NAME_LEN - 1) && (name_ch = *(new_signal_name++)) != '\0')
    {
        name_buf[index ^ 3] = name_ch;
        index++;
    }

    name_buf[index ^ 3] = '\0';
}

// EOF
