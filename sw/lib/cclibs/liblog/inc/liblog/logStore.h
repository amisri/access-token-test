//! @file  logStore.h
//! @brief Converter Control Logging library store functino header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "liblog.h"

// Liblog constants

#define LOGSTORE_DEBUG                  0                   //!< Set LOGSTORE_DEBUG to 0 for normal build and >0 for debug build

#define LOG_NUM_CYCLE_BUFS              3                   //!< Triple buffer cycle sample markers for concurrency protection
#define LOG_CYCLE_NOT_ACTIVE            LOG_NUM_CYCLE_BUFS  //!< When cycles.active is set to this, the whole cycle is not active

// Liblog enum constants

enum LOG_dig_source_type
{
    LOG_DIG_SOURCE_TYPE_BOOLEAN,
    LOG_DIG_SOURCE_TYPE_UINT8  ,
    LOG_DIG_SOURCE_TYPE_UINT16 ,
    LOG_DIG_SOURCE_TYPE_UINT32 ,
    LOG_DIG_SOURCE_TYPE_POINTER,
};

enum LOG_state
{
    LOG_RUNNING                ,
    LOG_STOPPING               ,
    LOG_FROZEN                 ,
    LOG_STARTING               ,
};

// Log management structures and unions

struct LOG_sample_marker
{
    uint32_t                        index;                      //!< Buffer index
    struct CC_ns_time               time_stamp;                 //!< Time stamp (Unix time + ns)
};

struct LOG_cycle
{
    struct CC_ns_time               time_origin;                //!< Time origin for cycling logs (Unix time + ns)
    uint32_t                        latched_stored_counter;     //!< Discontinuous stored_counter at time of logStoreStartDiscontinuous()
    uint32_t                        num_samples;                //!< Number of samples store during the cycle
    struct LOG_sample_marker        first_sample_marker;        //!< First sample marker
    struct LOG_sample_marker        last_sample_marker;         //!< Last sample marker
};

struct LOG_cycles
{
    uint32_t                        active;                     //!< Index (0,1,2) in cycle[] of active cycle record
    struct LOG_cycle                cycle[LOG_NUM_CYCLE_BUFS];  //!< Triple-buffered cycle records
};

// Private log structures (can be stored in large slow memory)

struct LOG_priv_ana_signal
{
    char                            name[LOG_SIG_NAME_LEN];     //!< Signal name - should be first field of the structure
    char                            units[LOG_UNITS_LEN];       //!< Signal units
    cc_float                      * time_offset;                //!< Pointer to the signal time offset
    cc_float                      * source;                     //!< Pointer to source of analog signal
    uint32_t                        meta;                       //!< Signal meta data
};


struct LOG_priv_ana                                             //!< Analog signal private log variables
{
    struct LOG_priv_ana_signal    * signals;                    //!< Pointer to analog signal description array (length priv.num_signals)
    uint32_t                      * selectors;                  //!< Pointer to analog signal selectors array   (length priv.num_sig_bufs)
};

struct LOG_priv_dig_signal
{
    char                            name[LOG_SIG_NAME_LEN];     //!< Signal name
    cc_float                      * time_offset;                //!< Pointer to the signal time offset
};

struct LOG_priv_dig                                             //!< Digital signal log variables
{
    struct LOG_priv_dig_signal    * signals;                    //!< Pointer to digital signal description array
};

struct LOG_private                                              // Private log structure - can be in slow memory
{
    char                    const * name;                       //!< Log name for ccrt testing only
    uint32_t                        sig_bufs_offset;            //!< Offset in elements to this log's signals buffer within global buffer
    uint32_t                        sig_bufs_len;               //!< Total size in elements of this log's signals buffer
    uint32_t                        num_postmortem_sig_bufs;    //!< Number of post-mortem signal buffers
    uint32_t                        num_postmortem_samples;     //!< Number of samples per signal for post-mortem (always greater than one)
    uint32_t                        num_signals;                //!< Number of signals

    union                                                       //!< Union of analog and digital log variables
    {
        struct LOG_priv_ana         analog;                     //!< Analog log variables
        struct LOG_priv_dig         digital;                    //!< Digital log variables
    }                               ad;
};

// Log structures (can be stored in small fast memory)

struct LOG_ana                                                  //!< Analog signal log variables
{
    cc_float                     ** selector_ptrs;              //!< Pointer to analog signal selector pointers array (length analog.num_sig_bufs)
};

union LOG_dig_source                                            //!< Digital source pointers union
{
    bool                          * boolean;
    uint8_t                       * uint8;
    uint16_t                      * uint16;
    uint32_t                      * uint32;
    void                         ** pointer;
};

struct LOG_dig_signal
{
    union LOG_dig_source            source;                     //!< Pointer to source of digital signal
    enum LOG_dig_source_type        source_type;                //!< Digital source type
    uint32_t                        bit_mask;                   //!< Bit mask when source type is not boolean
};

struct LOG_dig                                                  //!< Digital signal log variables
{
    uint32_t                      * source;                     //!< Optional pointer to complete digital signals bit mask
    struct LOG_dig_signal         * signals;                    //!< Pointer to digital signals array (length digital.num_signals)
    uint32_t                        num_signals;                //!< Number of digital signals
};

struct LOG_continuous                                           //!< Continuous log variables
{
    enum LOG_state                  state;                      //!< Logging state (RUNNING, STOPPING or FROZEN)
    uint16_t                        start_cycle_flag;           //!< True when a new cycle is starting
    uint16_t                        cur_time_base;              //!< Selects cycle[] in cycles[0] as the current time_base
    uint32_t                        num_post_freeze_samples;    //!< Number of post-freeze samples before logging is frozen
    uint32_t                        stopping_counter;           //!< Post-freeze samples down-counter during stopping state
    struct CC_ns_time               freeze_time_stamp;          //!< Time stamp when freeze was last activated (Unix time + ns)
};

struct LOG_discontinuous                                        //!< Discontinuous log variables
{
    uint32_t                        last_cyc_sel;               //!< Cycle selector of last completed cycle (for post-mortem)
    uint32_t                        stored_samples_counter;     //!< Free running counter of the number of samples stored
};

struct LOG_start_cycle
{
    uint32_t                        log_mask;                   //!< Bit set to 1 means that the (cyclic, continuous, non disabled, not frozen) log should prepare for a new cycle
    struct CC_ns_time               time;                       //!< Start cycle time stamp (Unix time + ns)
};

struct LOG_log                                                  //!< Should be in fast memory
{
    struct LOG_mgr                * log_mgr;                    //!< Pointer to log_mgr
    struct LOG_private            * priv;                       //!< Pointer to the private log structure
    struct LOG_start_cycle        * start_cycle;                //!< Pointer to the start_cycle structure in log_structs (fast memory)

    uint16_t                        index;                      //!< Log index
    uint16_t                        num_cycles;                 //!< Number of cycles in index (1 for Non-cyclic log, >1 for Cyclic log)
    uint32_t                        bit_mask;                   //!< Log bit mask = (1 << log.index)
    uint32_t                        meta;                       //!< Log meta data bit mask
    uint32_t                        num_sig_bufs;               //!< Number of signal buffers (always 1 for digital logs)
    uint32_t                        num_samples_per_sig;        //!< Number of samples per signal

    uint32_t                      * sig_bufs;                   //!< Pointer to signal buffers for this log
    struct LOG_cycles             * cycles;                     //!< Pointer to cycles index structures (length log.num_cycles)

    union                                                       //!< Union of analog and digital log variables
    {
        struct LOG_ana              analog;                     //!< Analog private variables
        struct LOG_dig              digital;                    //!< Digital private structures
    }                               ad;

    union                                                       //!< Union of continuous and discontinuous variables
    {
        struct LOG_continuous       continuous;                 //!< Continuous private variables
        struct LOG_discontinuous    discontinuous;              //!< Discontinuous private variables
    }                               cd;

    uint16_t                        cur_cyc_sel;                //!< Current cycles buffer index (0 to (num_cycles-1))
    uint16_t                        cur_cycle;                  //!< Current cycle buffer index (0 to 2) for cycles[cur_cyc_sel].cycles[]
    struct LOG_sample_marker        last_sample_marker;         //!< Marker of last stored sample
};


// Static inline function definitions

static inline uint32_t logStoreCalcNextIndex(const uint32_t current_index, const uint32_t array_size)
{
    // Advance next index to next value - avoid modulus (%) as it can be very slow on some DSPs

    uint32_t next_index = current_index + 1;

    return (next_index >= array_size) ? 0 : next_index;
}


// Global logging manager function declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Ends current cycle and starts a new cycle in all cyclic continuous unfrozen logs
//!
//! @param[in,out]  log_mgr       Pointer to the log_mgr structure
//! @param[in]      time_stamp    Time stamp for start of new cycle
//! @param[in]      cyc_sel       Cycle selector for the new cycle

void logStoreStartContinuousRT(struct LOG_log  * log,
                               struct CC_ns_time time_stamp,
                               uint32_t          cyc_sel);


//! Stores one sample in a continuous log
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      time_stamp    Time stamp for log sample

void logStoreContinuousRT(struct LOG_log  * log,
                          struct CC_ns_time time_stamp);


//! Prepare to store data in a discontinuous log
//!
//! Storing data in a discontinuous log should start with a call to logStoreStartDiscontinuous(),
//! followed by call(s) to logStoreDiscontinuous() and ending with a call to logStoreEndDiscontinuous().
//!
//! @param[in,out]  log                        Pointer to the log structure
//! @param[in]      time_origin                Time origin for cycle
//! @param[in]      first_sample_time_stamp    Time stamp for the first sample that will be stored in the log
//! @param[in]      cyc_sel                    Cycle selector for the new data to be logged

void logStoreStartDiscontinuousRT(struct LOG_log  * log,
                                  struct CC_ns_time time_origin,
                                  struct CC_ns_time first_sample_time_stamp,
                                  uint32_t          cyc_sel);


//! Store one or more samples in a discontinuous log.
//!
//! Storing data in a discontinuous log should start with a call to logStoreStartDiscontinuous(),
//! followed by call(s) to logStoreDiscontinuous() and ending with a call to logStoreEndDiscontinuous().
//!
//! If more than one sample is to be stored, they must be in consecutive memory locations, start with
//! the location given by the source pointer for each signal in the log.
//!
//! @param[in,out]  log           Pointer to the log structure
//! @param[in]      num_samples   Number of samples to be logged into the discontinuous log

void logStoreDiscontinuousRT(struct LOG_log * log,
                             uint32_t         num_samples);


//! End the logging of data to a discontinuous log
//!
//! Storing data in a discontinuous log should start with a call to logStoreStartDiscontinuous(),
//! followed by call(s) to logStoreDiscontinuous() and ending with a call to logStoreEndDiscontinuous().
//!
//! The function will clear the bit in post_pm_log_running_mask that corresponds to the log index.
//!
//! @param[in,out]  log                       Pointer to the log structure
//! @param[in]      last_sample_time_stamp    Time stamp for the last sample that was stored in the log

void logStoreEndDiscontinuousRT(struct LOG_log  * log,
                                struct CC_ns_time last_sample_time_stamp);


//! Cancel logging to a discontinuous log
//!
//! The function should be called if logStoreStartDiscontinuous() was called, potentially followed by
//! calls to logStoreDiscontinuous(), and now the data should be discarded.
//!
//! @param[in,out]  log           Pointer to the log structure

void logStoreCancelDiscontinuousRT(struct LOG_log * log);


//! Cache analog signal source pointers
//!
//! The function should be called if the analog signal selector indexes for the log
//! are set or changed. It caches the signal source pointers in an array in the
//! log structure. This allows the optimization of the gathering of the
//! analog signal values for each sample.
//!
//! @param[in,out]  log           Pointer to the log structure

void logStoreAnalogSelector(struct LOG_log * log);


//! Store all the signal names and units for a set of logs
//!
//! This function decomposes the sig_names_and_units string for a set of logs. This has three delimiters:
//!
//!   comma (,) - log delimiter (suffix)
//!   point (.) - signal name delimiter (prefix)
//!   colon (:) - signal units delimiter (prefix)
//!
//! This function can be called repeatedly and will reset all names and units before storing the
//! new names and units from the sig_names_and_units string. It is protected against names and units strings
//! being too long, or if there are too many signals in a log or too many logs. In all these cases,
//! extra characters or strings are just silently ignored.
//!
//! @param[in,out]  logs                  Pointer to array of log structs for the set of logs
//! @param[in]      sig_names_and_units   Pointer to string containing signal names and units for the set of logs

void logStoreAllSigNamesAndUnits(struct LOG_log * logs,
                                 char     const * sig_names_and_units);


//! Store name and/or units for one signal in one log
//!
//! The application can store the analog or digital signal name and/or an analog signal's units using this function.
//! If a string is empty then the old name or units are not changed.
//!
//! @param[in,out]  log              Pointer to log structure containing the signal
//! @param[in]      sig_index        Index of the signal whose name and/or units are to be updated
//! @param[in]      sig_name         Pointer to new the signal name
//! @param[in]      units            Pointer to new the units

void logStoreSigNameAndUnits(struct LOG_log * log,
                             uint32_t         sig_index,
                             char     const * sig_name,
                             char     const * units);
#ifdef __cplusplus
}
#endif

// EOF
