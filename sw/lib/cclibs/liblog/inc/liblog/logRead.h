//! @file  logRead.h
//! @brief Converter Control Logging library functions for reading the log.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

// #define LOG_READ_CONTROL_PRINT

//! Log read structure
//!
//! This contains the read request, control and header structures, and pointers to
//! the array of log structures and buffers.

struct LOG_read
{
    struct LOG_log            * log;
    uint32_t                  * buffers;

    struct LOG_read_request     request;
    union  LOG_header           header;

    union  CC_debug             debug;
};

// Global functions declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Prepares read control and header structures based on the read request
//!
//! The function takes the log read request in log_read that was previously prepared by the application,
//! validates it and, if everything's fine, fills up the header and/or the read control structure with all information
//! necessary to retrieve the required samples from the log. This will be in the read control structure,
//! which can then be passed to logReadOutput() to extract the log data.
//!
//! Note that the function won't return any status on failure. Instead it will set the status field in the read control
//! structure - it can be examined by the application to find the cause of failure.
//!
//! @param[in,out]  log_read      Pointer to log read structure which contains the read request and header structures
//! @param[out]     read_control  Pointer to log control structure to be filled

void logReadRequest(struct LOG_read * log_read, struct LOG_read_control * read_control);

#ifdef __cplusplus
}
#endif


//! Debug function that prints to stderr the contents of a read_control structure.
//!
//! The content of the function is only included if LOG_READ_DEBUG is defined
//!
//! @param[in]  read_control    Pointer to log read_control structure

static inline void logReadControlPrint(struct LOG_read_control * const read_control)
{
#ifdef LOG_READ_CONTROL_PRINT

    fputc('\n',stderr);

    fprintf(stderr,"read_control.status                %u\n",     read_control->status);
    fprintf(stderr,"read_control.time_origin           %u.%09d\n",read_control->time_origin.secs.abs,      read_control->time_origin.ns);
    fprintf(stderr,"read_control.first_sample_time     %u.%09d\n",read_control->first_sample_time.secs.abs,read_control->first_sample_time.ns);
    fprintf(stderr,"read_control.last_sample_time      %u.%09d\n",read_control->last_sample_time.secs.abs, read_control->last_sample_time.ns);
    fprintf(stderr,"read_control.period                %d.%09d\n",read_control->period.secs.rel,           read_control->period.ns);
    fprintf(stderr,"read_control.meta                  0x%X\n",   read_control->meta);
    fprintf(stderr,"read_control.num_signals           %u\n",     read_control->num_signals);
    fprintf(stderr,"read_control.num_sig_bufs          %u\n",     read_control->num_sig_bufs);
    fprintf(stderr,"read_control.num_samples           %u\n",     read_control->num_samples);
    fprintf(stderr,"read_control.num_samples_from_log  %u\n",     read_control->num_samples_from_log);
    fprintf(stderr,"read_control.sub_sampling_period   %u\n",     read_control->sub_sampling_period);
    fprintf(stderr,"read_control.header_size           %u\n",     read_control->header_size);
    fprintf(stderr,"read_control.data_size             %u\n",     read_control->data_size);
    fprintf(stderr,"read_control.total_size            %u\n",     read_control->total_size);
    fprintf(stderr,"read_control.data_len              %u\n",     read_control->data_len);
    fprintf(stderr,"read_control.buf_offset            %u\n",     read_control->buf_offset);
    fprintf(stderr,"read_control.buf_len               %u\n",     read_control->buf_len);
    fprintf(stderr,"read_control.element_offset        %u\n",     read_control->output.element_offset);
    fprintf(stderr,"read_control.step                  %u\n",     read_control->output.step);
    fprintf(stderr,"read_control.start_offset          ");

    uint32_t i;
    uint32_t const num_signals = read_control->num_signals;

    for(i = 0 ; i < num_signals ; i++)
    {
        fprintf(stderr,"%u ",read_control->output.start_offset[i]);
    }

    fputs("\n\n", stderr);

#endif
}

// EOF
