//! @file      libcctime.h
//! @brief     Converter Control Time library top-level header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libcctime.
//!
//! libcctime is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! \mainpage CERN Converter Control Library: Time Manipulation Functions
//!
//! Libcctime provides functions to manipulate absolute and relative times
//! with nanosecond, microsecond and millisecond resolutions.

#pragma once

#include "cclibs.h"

//! Constants

#define CCTIME_NEVER                0xFFFFFFFF
#define CCTIME_NS_PER_S             1000000000
#define CCTIME_US_PER_S             1000000
#define CCTIME_MS_PER_S             1000
#define CCTIME_NS_PER_S_2           1000000000000000000LL
#define CCTIME_US_PER_S_2           1000000000000LL
#define CCTIME_MS_PER_S_2           1000000LL
#define CCTIME_NS_RESOLUTION        9
#define CCTIME_US_RESOLUTION        6
#define CCTIME_MS_RESOLUTION        3

//! Include all libcctime header files

#include "libcctime/cctimeNs.h"
#include "libcctime/cctimeUs.h"
#include "libcctime/cctimeMs.h"

//! Static inline functions

static inline struct CC_ns_time cctimeUsToNsRT(struct CC_us_time const cc_us_time)
{
    struct CC_ns_time const cc_ns_time = { { cc_us_time.secs.abs }, cc_us_time.us * 1000 };

    return cc_ns_time;
}



static inline struct CC_ns_time cctimeMsToNsRT(struct CC_ms_time const cc_ms_time)
{
    struct CC_ns_time const cc_ns_time = { { cc_ms_time.secs.abs }, cc_ms_time.ms * 1000000 };

    return cc_ns_time;
}



static inline struct CC_us_time cctimeNsToUsRT(struct CC_ns_time const cc_ns_time)
{
    struct CC_us_time const cc_us_time = { { cc_ns_time.secs.abs }, cc_ns_time.ns / 1000 };

    return cc_us_time;
}



static inline struct CC_us_time cctimeMsToUsRT(struct CC_ms_time const cc_ms_time)
{
    struct CC_us_time const cc_us_time = { { cc_ms_time.secs.abs }, cc_ms_time.ms * 1000 };

    return cc_us_time;
}



static inline struct CC_ms_time cctimeNsToMsRT(struct CC_ns_time const cc_ns_time)
{
    struct CC_ms_time const cc_ms_time = { { cc_ns_time.secs.abs }, cc_ns_time.ns / 1000000 };

    return cc_ms_time;
}



static inline struct CC_ms_time cctimeUsToMsRT(struct CC_us_time const cc_us_time)
{
    struct CC_ms_time const cc_ms_time = { { cc_us_time.secs.abs }, cc_us_time.us / 1000 };

    return cc_ms_time;
}

// EOF
