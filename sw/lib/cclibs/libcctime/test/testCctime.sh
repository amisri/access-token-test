# testCctime.sh: Compile and run unit tests on libcctime

# Stop on error

set -e

# Create bin and results folders if they don't already exist

mkdir -p bin results

# Compile libcctime unit test program using int64_t

cc -o bin/testCctime_with_int64_t -I../../inc -I../inc/ -Iinc src/testCctime.c ../src/cctimeNs.c ../src/cctimeUs.c ../src/cctimeMs.c

# Running libcctime test program using int64_t

echo "Tests using int64_t"

./bin/testCctime_with_int64_t | tee results/with_int64_t.tap
gawk -f ../../scripts/tap_to_junit.awk cctime results/with_int64_t.tap

# Compile libcctime unit test program using cc_double instead of int64_t

cc -DCCTIME_NO_INT64_T -o bin/testCctime_without_int64_t -I../../inc -I../inc/ -Iinc src/testCctime.c ../src/cctimeNs.c ../src/cctimeUs.c ../src/cctimeMs.c

# Running libcctime test program without using int64_t

echo "Tests using double instead of int64_t"

./bin/testCctime_without_int64_t | tee results/without_int64_t.tap
gawk -f ../../scripts/tap_to_junit.awk cctime results/without_int64_t.tap

exit 0

# EOF
