// Test cctime functions

#include "libcctime.h"

// Array len macro

#define ARRAY_LEN(array) (sizeof array / sizeof *array)

// Static variables

static uint32_t test_index;


// Static check functions

static char * printRelTime(int32_t const secs, int32_t const frac, uint32_t const resolution, char * strbuf)
{
    switch(resolution)
    {
        case CCTIME_NS_RESOLUTION:
            {
                struct CC_ns_time const cc_ns_time = { secs, frac };

                cctimeNsPrintRelTime(cc_ns_time, strbuf);
            }
            break;

        case CCTIME_US_RESOLUTION:
            {
                struct CC_us_time const cc_us_time = { secs, frac };

                cctimeUsPrintRelTime(cc_us_time, strbuf);
            }

            break;

        case CCTIME_MS_RESOLUTION:
            {
                struct CC_ms_time const cc_ms_time = { secs, frac };

                cctimeMsPrintRelTime(cc_ms_time, strbuf);
            }

            break;

        default:

            return "invalid_resolution";
    }

    return strbuf;
}

static void checkDoubleTime(uint32_t   const label_index,
                            char     * const label,
                            cc_double  const result,
                            cc_double  const expected_result)
{
    test_index++;

    if(expected_result != result)
    {
        printf("not ok %3u | %s(%u) - exp:%.9f got:%.9f\n"
              , test_index
              , label
              , label_index
              , expected_result
              , result
              );
    }
    else
    {
        printf("ok     %3u | %s(%u)\n", test_index, label, label_index);
    }
}



static void checkAbsTime(uint32_t const label_index,
                         char   * const label,
                         uint32_t const s,
                         int32_t  const frac,
                         uint32_t const expected_s,
                         int32_t  const expected_frac,
                         uint32_t const resolution)
{
    test_index++;

    if(s == expected_s && frac == expected_frac)
    {
        printf("ok     %3u | %s(%u)\n", test_index, label, label_index);
    }
    else
    {
        printf("not ok %3u | %s(%u) - exp:%u.%0*u got:%u.%0*u\n"
              , test_index
              , label
              , label_index
              , expected_s
              , resolution
              , expected_frac
              , s
              , resolution
              , frac
              );
    }
}



static void checkRelTime(uint32_t const label_index,
                         char   * const label,
                         int32_t  const s,
                         int32_t  const frac,
                         int32_t  const expected_s,
                         int32_t  const expected_frac,
                         uint32_t const resolution)
{
    test_index++;

    if(s == expected_s && frac == expected_frac)
    {
        printf("ok     %3u | %s(%u)\n", test_index, label, label_index);
    }
    else
    {
        char exp_strbuf[24];
        char got_strbuf[24];

        printf("not ok %3u | %s(%u) - exp:%s got:%s (%d:%0*d)\n"
              , test_index
              , label
              , label_index
              , printRelTime(expected_s, expected_frac, resolution, exp_strbuf)
              , printRelTime(s, frac, resolution, got_strbuf)
              , s
              , resolution
              , frac
              );
    }
}



static void checkIntResult(uint32_t const label_index,
                           char   * const label,
                           int32_t  const result,
                           int32_t  const expected_result)
{
    test_index++;

    if(expected_result != result)
    {
        printf("not ok %3u | %s(%u) - exp:%d got:%d\n"
              , test_index
              , label
              , label_index
              , expected_result
              , result
              );
    }
    else
    {
        printf("ok     %3u | %s(%u)\n", test_index, label, label_index);
    }
}



static void checkCmp(uint32_t const label_index,
                     char   * const label,
                     int32_t  const result,
                     int32_t  const expected_result)
{
    test_index++;

    if(   (expected_result  < 0 && result >= 0)
       || (expected_result == 0 && result != 0)
       || (expected_result  > 0 && result <= 0))
    {
        printf("not ok %3u | %s(%u) - exp:%d got:%d\n"
              , test_index
              , label
              , label_index
              , expected_result
              , result
              );
    }
    else
    {
        printf("ok     %3u | %s(%u)\n", test_index, label, label_index);
    }
}

// Static test functions

#include "cctimeTestNs.h"
#include "cctimeTestUs.h"
#include "cctimeTestMs.h"


// Main function

int main(int argc, char **argv)
{
    uint32_t i;

    // Static inline conversion functions from libcctime.h

    {
        uint32_t index = 0;
        struct CC_ns_time const from_ns = { { 5 }, 123456789 };
        struct CC_us_time const from_us = { { 6 }, 456789 };
        struct CC_ms_time const from_ms = { { 7 }, 789 };

        struct CC_ns_time to_ns;
        struct CC_us_time to_us;
        struct CC_ms_time to_ms;

        to_ns = cctimeUsToNsRT(from_us);
        checkRelTime(++index, "cctimeUsToNsRT", to_ns.secs.rel, to_ns.ns, 6, 456789000, CCTIME_NS_RESOLUTION);

        to_ns = cctimeMsToNsRT(from_ms);
        checkRelTime(++index, "cctimeMsToNsRT", to_ns.secs.rel, to_ns.ns, 7, 789000000, CCTIME_NS_RESOLUTION);

        to_us = cctimeNsToUsRT(from_ns);
        checkRelTime(++index, "cctimeNsToUsRT", to_us.secs.rel, to_us.us, 5, 123456, CCTIME_US_RESOLUTION);

        to_us = cctimeMsToUsRT(from_ms);
        checkRelTime(++index, "cctimeUsToNsRT", to_us.secs.rel, to_us.us, 7, 789000, CCTIME_US_RESOLUTION);

        to_ms = cctimeNsToMsRT(from_ns);
        checkRelTime(++index, "cctimeNsToUsRT", to_ms.secs.rel, to_ms.ms, 5, 123, CCTIME_MS_RESOLUTION);

        to_ms = cctimeUsToMsRT(from_us);
        checkRelTime(++index, "cctimeUsToNsRT", to_ms.secs.rel, to_ms.ms, 6, 456, CCTIME_MS_RESOLUTION);
    }

    // cctimeNsAbsTimeToDoubleRT

    {
        struct cctime_test_ns_double tests[] =
        {
            { {          0,         0 },          0.000000000 },
            { {          1, 987654321 },          1.987654321 },
            { { 1234567890, 987654321 }, 1234567890.987654321 },
            { { 2147483648, 987654321 }, 2147483648.987654321 },
            { { 4294967295, 987654321 }, 4294967295.987654321 },
        };

        testCctimeNsAbsTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsAbsTimeToDoubleRT

    {
        struct cctime_test_us_double tests[] =
        {
            { {          0,      0 },          0.000000 },
            { {          1, 987654 },          1.987654 },
            { { 1234567890, 987654 }, 1234567890.987654 },
            { { 2147483648, 987654 }, 2147483648.987654 },
            { { 4294967295, 987654 }, 4294967295.987654 },
        };

        testCctimeUsAbsTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsAbsTimeToDoubleRT

    {
        struct cctime_test_ms_double tests[] =
        {
            { {          0,   0 },          0.000 },
            { {          1, 987 },          1.987 },
            { { 1234567890, 987 }, 1234567890.987 },
            { { 2147483648, 987 }, 2147483648.987 },
            { { 4294967295, 987 }, 4294967295.987 },
        };

        testCctimeMsAbsTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsRelTimeToDoubleRT

    {
        struct cctime_test_ns_double tests[] =
        {
            { {          0,         0 },          0.000000000 },
            { {          1,         0 },          1.000000000 },
            { {          2, 123456789 },          2.123456789 },
            { {         -2,  12345679 },         -1.987654321 },
        };

        testCctimeNsRelTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsRelTimeToDoubleRT

    {
        struct cctime_test_us_double tests[] =
        {
            { {          0,      0 },          0.000000 },
            { {          1,      0 },          1.000000 },
            { {          2, 123456 },          2.123456 },
            { {         -2,  12346 },         -1.987654 },
        };

        testCctimeUsRelTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsRelTimeToDoubleRT

    {
        struct cctime_test_ms_double tests[] =
        {
            { {          0,   0 },          0.000 },
            { {          1,   0 },          1.000 },
            { {          2, 123 },          2.123 },
            { {         -2,  13 },         -1.987 },
        };

        testCctimeMsRelTimeToDoubleRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsDoubleToRelTimeRT

    {
        struct cctime_test_ns_double tests[] =
        {
            { { 1987654321, 987654209 }, 1987654321.987654321 },
            { {          0,         0 },          0.000000000 },
            { {          1,         0 },          1.000000000 },
            { {          2, 123456789 },          2.123456789 },
            { {         -2,  12345679 },         -1.987654321 },
        };

        testCctimeNsDoubleToRelTimeRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsDoubleToRelTimeRT

    {
        struct cctime_test_us_double tests[] =
        {
            { { 1987654321, 987654 }, 1987654321.987654 },
            { {          0,      0 },          0.000000 },
            { {          1,      0 },          1.000000 },
            { {          2, 123456 },          2.123456 },
            { {         -2,  12346 },         -1.987654 },
        };

        testCctimeUsDoubleToRelTimeRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsDoubleToRelTimeRT

    {
        struct cctime_test_ms_double tests[] =
        {
            { { 1987654321, 987 }, 1987654321.987 },
            { {          0,   0 },          0.000 },
            { {          1,   0 },          1.000 },
            { {          2, 123 },          2.123 },
            { {         -2,  13 },         -1.987 },
        };

        testCctimeMsDoubleToRelTimeRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsAddDelayRT

    {
        struct cctime_test_ns_uint tests[] =
        {
            { {         12, 900000000 },          0, {         12 , 900000000 } },
            { {         12, 900000000 },   99999999, {         12 , 999999999 } },
            { {         12, 900000000 },  100000000, {         13 ,         0 } },
            { {         12, 900000000 }, 1000000000, {         13 , 900000000 } },
            { {         12, 900000000 }, 1099999999, {         13 , 999999999 } },
            { {         12, 900000000 }, 3099999999, {         15 , 999999999 } },
            { {         12, 900000000 }, 4100000000, {         17 ,         0 } },
            { { 2147483647, 500000000 },  499999999, { 2147483647 , 999999999 } },
            { { 2147483647, 500000000 }, 3499999999, { 2147483650 , 999999999 } },
        };

        testCctimeNsAddDelayRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsAddDelayRT

    {
        struct cctime_test_us_uint tests[] =
        {
            { {         12, 900000 },       0, {         12 , 900000 } },
            { {         12, 900000 },   99999, {         12 , 999999 } },
            { {         12, 900000 },  100000, {         13 ,      0 } },
            { {         12, 900000 }, 1000000, {         13 , 900000 } },
            { {         12, 900000 }, 1099999, {         13 , 999999 } },
            { {         12, 900000 }, 3099999, {         15 , 999999 } },
            { {         12, 900000 }, 4100000, {         17 ,      0 } },
            { { 2147483647, 500000 },  499999, { 2147483647 , 999999 } },
            { { 2147483647, 500000 }, 3499999, { 2147483650 , 999999 } },
        };

        testCctimeUsAddDelayRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsAddDelayRT

    {
        struct cctime_test_ms_uint tests[] =
        {
            { {         12, 900 },    0, {         12 , 900 } },
            { {         12, 900 },   99, {         12 , 999 } },
            { {         12, 900 },  100, {         13 ,   0 } },
            { {         12, 900 }, 1000, {         13 , 900 } },
            { {         12, 900 }, 1099, {         13 , 999 } },
            { {         12, 900 }, 3099, {         15 , 999 } },
            { {         12, 900 }, 4100, {         17 ,   0 } },
            { { 2147483647, 500 },  499, { 2147483647 , 999 } },
            { { 2147483647, 500 }, 3499, { 2147483650 , 999 } },
            { {         -2, 500 }, 4000, {          2 , 500 } },
        };

        testCctimeMsAddDelayRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsAddOffsetRT

    {
        struct cctime_test_ns_int tests[] =
        {
            { {          1, 900000000 },           0, {          1 , 900000000 } },
            { {          1, 900000000 },    99999999, {          1 , 999999999 } },
            { {          1, 900000000 },   100000000, {          2 ,         0 } },
            { {          1, 900000000 },  1000000000, {          2 , 900000000 } },
            { {          1, 900000000 },  1099999999, {          2 , 999999999 } },
            { {          1, 900000000 },  2099999999, {          3 , 999999999 } },
            { {          1, 900000000 },  2100000000, {          4 ,         0 } },
            { {          1, 900000000 },  -900000000, {          1 ,         0 } },
            { {          1, 900000000 },  -900000001, {          0 , 999999999 } },
            { {          1, 900000000 }, -1000000000, {          0 , 900000000 } },
            { {          1, 900000000 }, -1900000000, {          0 ,         0 } },
            { {          1, 900000000 }, -1900000001, {         -1 , 999999999 } },
            { {          1, 900000000 }, -2100000000, {         -1 , 800000000 } },
            { {         -1, 900000000 },  2100000000, {          2 ,         0 } },
            { {         -1, 900000000 }, -2100000000, {         -3 , 800000000 } },
        };

        testCctimeNsAddOffsetRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsAddOffsetRT

    {
        struct cctime_test_us_int tests[] =
        {
            { {          1, 900000 },        0, {          1 , 900000 } },
            { {          1, 900000 },    99999, {          1 , 999999 } },
            { {          1, 900000 },   100000, {          2 ,      0 } },
            { {          1, 900000 },  1000000, {          2 , 900000 } },
            { {          1, 900000 },  1099999, {          2 , 999999 } },
            { {          1, 900000 },  2099999, {          3 , 999999 } },
            { {          1, 900000 },  2100000, {          4 ,      0 } },
            { {          1, 900000 },  -900000, {          1 ,      0 } },
            { {          1, 900000 },  -900001, {          0 , 999999 } },
            { {          1, 900000 }, -1000000, {          0 , 900000 } },
            { {          1, 900000 }, -1900000, {          0 ,      0 } },
            { {          1, 900000 }, -1900001, {         -1 , 999999 } },
            { {          1, 900000 }, -2100000, {         -1 , 800000 } },
            { {         -1, 900000 },  2100000, {          2 ,      0 } },
            { {         -1, 900000 }, -2100000, {         -3 , 800000 } },
        };

        testCctimeUsAddOffsetRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsAddOffsetRT

    {
        struct cctime_test_ms_int tests[] =
        {
            { {          1, 900 },     0, {          1 , 900 } },
            { {          1, 900 },    99, {          1 , 999 } },
            { {          1, 900 },   100, {          2 ,   0 } },
            { {          1, 900 },  1000, {          2 , 900 } },
            { {          1, 900 },  1099, {          2 , 999 } },
            { {          1, 900 },  2099, {          3 , 999 } },
            { {          1, 900 },  2100, {          4 ,   0 } },
            { {          1, 900 },  -900, {          1 ,   0 } },
            { {          1, 900 },  -901, {          0 , 999 } },
            { {          1, 900 }, -1000, {          0 , 900 } },
            { {          1, 900 }, -1900, {          0 ,   0 } },
            { {          1, 900 }, -1901, {         -1 , 999 } },
            { {          1, 900 }, -2100, {         -1 , 800 } },
            { {         -1, 900 },  2100, {          2 ,   0 } },
            { {         -1, 900 }, -2100, {         -3 , 800 } },
        };

        testCctimeMsAddOffsetRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsAddRT

    {
        struct cctime_test_2ns_to_ns tests[] =
        {
            { {           6, 500000000 }, {           0,         0 }, {          6 , 500000000 } },
            { {           6, 500000000 }, {           0, 499999999 }, {          6 , 999999999 } },
            { {           6, 500000000 }, {           0, 500000000 }, {          7 ,         0 } },
            { {           6, 500000000 }, {           3, 700000000 }, {         10 , 200000000 } },
            { {  2000000000, 300000000 }, {  2000000000, 700000000 }, { 4000000001 ,         0 } },
            { {  4000000000, 300000000 }, { -1000000001, 300000000 }, { 2999999999 , 600000000 } },
            { { -1000000001, 300000000 }, {  4000000000, 300000000 }, { 2999999999 , 600000000 } },
        };

        testAbsCctimeNsAddRT(tests, ARRAY_LEN(tests));
    }

    {
        struct cctime_test_2ns_to_ns tests[] =
        {
            { {          6, 500000000 }, {          -7, 500000000 }, {          0 ,         0 } },
            { {          6, 500000000 }, {          -7, 499999999 }, {         -1 , 999999999 } },
            { {          6, 500000000 }, {          -6, 500000001 }, {          1 ,         1 } },
            { {         -6, 500000000 }, {          -6, 500000001 }, {        -11 ,         1 } },
            { {         -6, 500000000 }, {           6, 500000001 }, {          1 ,         1 } },
        };

        testRelCctimeNsAddRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsAddRT

    {
        struct cctime_test_2us_to_us tests[] =
        {
            { {           6, 500000 }, {           0,      0 }, {          6 , 500000 } },
            { {           6, 500000 }, {           0, 499999 }, {          6 , 999999 } },
            { {           6, 500000 }, {           0, 500000 }, {          7 ,      0 } },
            { {           6, 500000 }, {           3, 700000 }, {         10 , 200000 } },
            { {  2000000000, 300000 }, {  2000000000, 700000 }, { 4000000001 ,      0 } },
            { {  4000000000, 300000 }, { -1000000001, 300000 }, { 2999999999 , 600000 } },
            { { -1000000001, 300000 }, {  4000000000, 300000 }, { 2999999999 , 600000 } },
        };

        testAbsCctimeUsAddRT(tests, ARRAY_LEN(tests));
    }

    {
        struct cctime_test_2us_to_us tests[] =
        {
            { {          6, 500000 }, {          -7, 500000 }, {          0 ,      0 } },
            { {          6, 500000 }, {          -7, 499999 }, {         -1 , 999999 } },
            { {          6, 500000 }, {          -6, 500001 }, {          1 ,      1 } },
            { {         -6, 500000 }, {          -6, 500001 }, {        -11 ,      1 } },
            { {         -6, 500000 }, {           6, 500001 }, {          1 ,      1 } },
        };

        testRelCctimeUsAddRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsAddRT

    {
        struct cctime_test_2ms_to_ms tests[] =
        {
            { {           6, 500 }, {           0,   0 }, {          6 , 500 } },
            { {           6, 500 }, {           0, 499 }, {          6 , 999 } },
            { {           6, 500 }, {           0, 500 }, {          7 ,   0 } },
            { {           6, 500 }, {           3, 700 }, {         10 , 200 } },
            { {  2000000000, 300 }, {  2000000000, 700 }, { 4000000001 ,   0 } },
            { {  4000000000, 300 }, { -1000000001, 300 }, { 2999999999 , 600 } },
            { { -1000000001, 300 }, {  4000000000, 300 }, { 2999999999 , 600 } },
        };

        testAbsCctimeMsAddRT(tests, ARRAY_LEN(tests));
    }

    {
        struct cctime_test_2ms_to_ms tests[] =
        {
            { {          6, 500 }, {          -7, 500 }, {          0 ,   0 } },
            { {          6, 500 }, {          -7, 499 }, {         -1 , 999 } },
            { {          6, 500 }, {          -6, 501 }, {          1 ,   1 } },
            { {         -6, 500 }, {          -6, 501 }, {        -11 ,   1 } },
            { {         -6, 500 }, {           6, 501 }, {          1 ,   1 } },
        };

        testRelCctimeMsAddRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsSubRT

    {
        struct cctime_test_2ns_to_ns tests[] =
        {
            { {           6, 500000000 }, {           0,         0 }, {           6 , 500000000 } },
            { {           6, 500000000 }, {           0, 500000000 }, {           6 ,         0 } },
            { {           6, 500000000 }, {           0, 500000001 }, {           5 , 999999999 } },
            { {           6, 500000000 }, {           5, 500000000 }, {           1 ,         0 } },
            { {           6, 500000000 }, {           5, 500000001 }, {           0 , 999999999 } },
            { {           6, 500000000 }, {           5, 999999999 }, {           0 , 500000001 } },
            { {           6, 500000000 }, {           6,         0 }, {           0 , 500000000 } },
            { {           6, 500000000 }, {           6, 499999999 }, {           0 ,         1 } },
            { {           6, 500000000 }, {           6, 500000001 }, {          -1 , 999999999 } },
            { {           6, 500000000 }, {           6, 500000001 }, {          -1 , 999999999 } },
            { {          -6, 500000000 }, {           1, 100000000 }, {          -7 , 400000000 } },
            { {          -6, 500000000 }, {          -7, 100000000 }, {           1 , 400000000 } },
            { {  2000000000, 300000000 }, {  2000000000, 700000000 }, {          -1 , 600000000 } },
            { {  1000000000, 300000000 }, { -1000000001, 300000000 }, {  2000000001 ,         0 } },
            { { -1000000001, 300000000 }, {  1000000000, 300000000 }, { -2000000001 ,         0 } },
            { {           0,         0 }, {  2000000000,         0 }, { -2000000000 ,         0 } },
        };

        testRelCctimeNsSubRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsSubRT

    {
        struct cctime_test_2us_to_us tests[] =
        {
            { {           6, 500000 }, {           0,      0 }, {           6 , 500000 } },
            { {           6, 500000 }, {           0, 500000 }, {           6 ,      0 } },
            { {           6, 500000 }, {           0, 500001 }, {           5 , 999999 } },
            { {           6, 500000 }, {           5, 500000 }, {           1 ,      0 } },
            { {           6, 500000 }, {           5, 500001 }, {           0 , 999999 } },
            { {           6, 500000 }, {           5, 999999 }, {           0 , 500001 } },
            { {           6, 500000 }, {           6,      0 }, {           0 , 500000 } },
            { {           6, 500000 }, {           6, 499999 }, {           0 ,      1 } },
            { {           6, 500000 }, {           6, 500001 }, {          -1 , 999999 } },
            { {           6, 500000 }, {           6, 500001 }, {          -1 , 999999 } },
            { {          -6, 500000 }, {           1, 100000 }, {          -7 , 400000 } },
            { {          -6, 500000 }, {          -7, 100000 }, {           1 , 400000 } },
            { {  2000000000, 300000 }, {  2000000000, 700000 }, {          -1 , 600000 } },
            { {  1000000000, 300000 }, { -1000000001, 300000 }, {  2000000001 ,      0 } },
            { { -1000000001, 300000 }, {  1000000000, 300000 }, { -2000000001 ,      0 } },
            { {           0,      0 }, {  2000000000,      0 }, { -2000000000 ,      0 } },
        };

        testRelCctimeUsSubRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsSubRT

    {
        struct cctime_test_2ms_to_ms tests[] =
        {
            { {           6, 500 }, {           0,   0 }, {           6 , 500 } },
            { {           6, 500 }, {           0, 500 }, {           6 ,   0 } },
            { {           6, 500 }, {           0, 501 }, {           5 , 999 } },
            { {           6, 500 }, {           5, 500 }, {           1 ,   0 } },
            { {           6, 500 }, {           5, 501 }, {           0 , 999 } },
            { {           6, 500 }, {           5, 999 }, {           0 , 501 } },
            { {           6, 500 }, {           6,   0 }, {           0 , 500 } },
            { {           6, 500 }, {           6, 499 }, {           0 ,   1 } },
            { {           6, 500 }, {           6, 501 }, {          -1 , 999 } },
            { {           6, 500 }, {           6, 501 }, {          -1 , 999 } },
            { {          -6, 500 }, {           1, 100 }, {          -7 , 400 } },
            { {          -6, 500 }, {          -7, 100 }, {           1 , 400 } },
            { {  2000000000, 300 }, {  2000000000, 700 }, {          -1 , 600 } },
            { {  1000000000, 300 }, { -1000000001, 300 }, {  2000000001 ,   0 } },
            { { -1000000001, 300 }, {  1000000000, 300 }, { -2000000001 ,   0 } },
            { {           0,   0 }, {  2000000000,   0 }, { -2000000000 ,   0 } },
        };

        testRelCctimeMsSubRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsDiffRT

    {
        struct cctime_test_2ns_int tests[] =
        {
            { {           7, 500000000 }, {           7, 500000000 },           0 },
            { {           7, 500000000 }, {           7,         0 },   500000000 },
            { {           7, 500000000 }, {           6, 500000001 },   999999999 },
            { {           7, 500000000 }, {           6, 500000000 },  1000000000 },
            { {           7, 500000000 }, {           6, 499999999 },  1000000001 },
            { {           7, 500000000 }, {           5, 999999999 },  1500000001 },
            { {           7, 500000000 }, {           7, 500000001 },          -1 },
            { {           7, 500000000 }, {           7, 999999999 },  -499999999 },
            { {           7, 500000000 }, {           8,         0 },  -500000000 },
            { {           7, 500000000 }, {           8, 499999999 },  -999999999 },
            { {           7, 500000000 }, {           8, 500000000 }, -1000000000 },
            { {           7, 500000000 }, {           9, 600000000 }, -2100000000 },
            { {          -2, 500000000 }, {          -2, 500000000 },           0 },
            { {          -2, 500000000 }, {          -2, 100000000 },   400000000 },
            { {          -2, 500000000 }, {           0, 500000001 }, -2000000001 },
        };

        testCctimeNsDiffRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsDiffRT

    {
        struct cctime_test_2us_int tests[] =
        {
            { {           7, 500000 }, {           7, 500000 },        0 },
            { {           7, 500000 }, {           7,      0 },   500000 },
            { {           7, 500000 }, {           6, 500001 },   999999 },
            { {           7, 500000 }, {           6, 500000 },  1000000 },
            { {           7, 500000 }, {           6, 499999 },  1000001 },
            { {           7, 500000 }, {           5, 999999 },  1500001 },
            { {           7, 500000 }, {           7, 500001 },       -1 },
            { {           7, 500000 }, {           7, 999999 },  -499999 },
            { {           7, 500000 }, {           8,      0 },  -500000 },
            { {           7, 500000 }, {           8, 499999 },  -999999 },
            { {           7, 500000 }, {           8, 500000 }, -1000000 },
            { {           7, 500000 }, {           9, 600000 }, -2100000 },
            { {          -2, 500000 }, {          -2, 500000 },        0 },
            { {          -2, 500000 }, {          -2, 100000 },   400000 },
            { {          -2, 500000 }, {           0, 500001 }, -2000001 },
        };

        testCctimeUsDiffRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsDiffRT

    {
        struct cctime_test_2ms_int tests[] =
        {
            { {           7, 500 }, {           7, 500 },     0 },
            { {           7, 500 }, {           7,   0 },   500 },
            { {           7, 500 }, {           6, 501 },   999 },
            { {           7, 500 }, {           6, 500 },  1000 },
            { {           7, 500 }, {           6, 499 },  1001 },
            { {           7, 500 }, {           5, 999 },  1501 },
            { {           7, 500 }, {           7, 501 },    -1 },
            { {           7, 500 }, {           7, 999 },  -499 },
            { {           7, 500 }, {           8,   0 },  -500 },
            { {           7, 500 }, {           8, 499 },  -999 },
            { {           7, 500 }, {           8, 500 }, -1000 },
            { {           7, 500 }, {           9, 600 }, -2100 },
            { {          -2, 500 }, {          -2, 500 },     0 },
            { {          -2, 500 }, {          -2, 100 },   400 },
            { {          -2, 500 }, {           0, 501 }, -2001 },
        };

        testCctimeMsDiffRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsSubRT

    {
        struct cctime_test_2ns_to_ns tests[] =
        {
            { {           1, 123456789 }, {           2, 670329672 }, {           2 , 999999999 } },
            { {           1, 123456789 }, {           2, 670329674 }, {           3 ,         1 } },
            { {          -2, 700000000 }, {           2,         0 }, {          -3 , 400000000 } },
            { {          -2, 700000000 }, {          -3, 500000000 }, {           3 , 250000000 } },
        };

        testRelCctimeNsProdRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsSubRT

    {
        struct cctime_test_2us_to_us tests[] =
        {
            { {           1, 123456 }, {           2, 670331 }, {           2 , 999999 } },
            { {           1, 123456 }, {           2, 670332 }, {           3 ,      1 } },
            { {          -2, 700000 }, {           2,      0 }, {          -3 , 400000 } },
            { {          -2, 700000 }, {          -3, 500000 }, {           3 , 250000 } },
        };

        testRelCctimeUsProdRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsSubRT

    {
        struct cctime_test_2ms_to_ms tests[] =
        {
            { {           1, 123 }, {           2, 670 }, {           2 , 998 } },
            { {           1, 123 }, {           2, 671 }, {           3 ,   0 } },
            { {           0,   9 }, {          10, 277 }, {           0 ,  92 } },
            { {           0,   9 }, {          10, 278 }, {           0 ,  93 } },
            { {          -2, 700 }, {           2,   0 }, {          -3 , 400 } },
            { {          -2, 700 }, {          -3, 500 }, {           3 , 250 } },
        };

        testRelCctimeMsProdRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsMulRT

    {
        struct cctime_test_ns_int tests[] =
        {
            { {          6, 500000000 },           1, {          6 , 500000000 } },
            { {          6, 500000000 },           2, {         13 ,         0 } },
            { {          6, 500000000 },          -3, {        -20 , 500000000 } },
            { {         -6, 500000000 },           3, {        -17 , 500000000 } },
            { {         -7, 500000000 },           3, {        -20 , 500000000 } },
            { {         -6, 500000000 },          -4, {         22 ,         0 } },
        };

        testCctimeNsMulRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsMulRT

    {
        struct cctime_test_us_int tests[] =
        {
            { {          6, 500000 },           1, {          6 , 500000 } },
            { {          6, 500000 },           2, {         13 ,      0 } },
            { {          6, 500000 },          -3, {        -20 , 500000 } },
            { {         -6, 500000 },           3, {        -17 , 500000 } },
            { {         -7, 500000 },           3, {        -20 , 500000 } },
            { {         -6, 500000 },          -4, {         22 ,      0 } },
        };

        testCctimeUsMulRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsMulRT

    {
        struct cctime_test_ms_int tests[] =
        {
            { {          6, 500 },           1, {          6 , 500 } },
            { {          6, 500 },           2, {         13 ,   0 } },
            { {          6, 500 },          -3, {        -20 , 500 } },
            { {         -6, 500 },           3, {        -17 , 500 } },
            { {         -7, 500 },           3, {        -20 , 500 } },
            { {         -6, 500 },          -4, {         22 ,   0 } },
        };

        testCctimeMsMulRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsDivRT

    {
        struct cctime_test_ns_int tests[] =
        {
            { {          0, 500000000 },           2, {          0 , 250000000 } },
            { {          0, 500000000 },          -2, {         -1 , 750000000 } },
            { {          6, 500000000 },           1, {          6 , 500000000 } },
            { {          6, 500000000 },          -1, {         -7 , 500000000 } },
            { {         -7, 500000000 },           1, {         -7 , 500000000 } },
            { {         -7, 500000000 },          -1, {          6 , 500000000 } },
            { {          6, 500000000 },           2, {          3 , 250000000 } },
            { {          6, 500000000 },          -2, {         -4 , 750000000 } },
            { {         -7, 500000000 },           2, {         -4 , 750000000 } },
            { {         -7, 500000000 },          -2, {          3 , 250000000 } },
            { {          6, 500000000 },          -3, {         -3 , 833333333 } },
            { {          6, 500000000 },           3, {          2 , 166666667 } },
            { {         -7, 500000000 },          -3, {          2 , 166666667 } },
            { {         -7, 500000000 },           3, {         -3 , 833333333 } },
            { {          6, 300000000 },           1, {          6 , 300000000 } },
            { {          6, 300000000 },          -1, {         -7 , 700000000 } },
            { {         -7, 300000000 },           1, {         -7 , 300000000 } },
            { {         -7, 300000000 },          -1, {          6 , 700000000 } },
            { {          6, 300000000 },           2, {          3 , 150000000 } },
            { {          6, 300000000 },          -2, {         -4 , 850000000 } },
            { {         -7, 300000000 },           2, {         -4 , 650000000 } },
            { {         -7, 300000000 },          -2, {          3 , 350000000 } },
            { {          6, 300000000 },          -3, {         -3 , 900000000 } },
            { {          6, 300000000 },           3, {          2 , 100000000 } },
            { {         -7, 300000000 },          -3, {          2 , 233333333 } },
            { {         -7, 300000000 },           3, {         -3 , 766666667 } },
        };

        testCctimeNsDivRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsDivRT

    {
        struct cctime_test_us_int tests[] =
        {
            { {          0, 500000 },           2, {          0 , 250000 } },
            { {          0, 500000 },          -2, {         -1 , 750000 } },
            { {          6, 500000 },           1, {          6 , 500000 } },
            { {          6, 500000 },          -1, {         -7 , 500000 } },
            { {         -7, 500000 },           1, {         -7 , 500000 } },
            { {         -7, 500000 },          -1, {          6 , 500000 } },
            { {          6, 500000 },           2, {          3 , 250000 } },
            { {          6, 500000 },          -2, {         -4 , 750000 } },
            { {         -7, 500000 },           2, {         -4 , 750000 } },
            { {         -7, 500000 },          -2, {          3 , 250000 } },
            { {          6, 500000 },          -3, {         -3 , 833333 } },
            { {          6, 500000 },           3, {          2 , 166667 } },
            { {         -7, 500000 },          -3, {          2 , 166667 } },
            { {         -7, 500000 },           3, {         -3 , 833333 } },
            { {          6, 300000 },           1, {          6 , 300000 } },
            { {          6, 300000 },          -1, {         -7 , 700000 } },
            { {         -7, 300000 },           1, {         -7 , 300000 } },
            { {         -7, 300000 },          -1, {          6 , 700000 } },
            { {          6, 300000 },           2, {          3 , 150000 } },
            { {          6, 300000 },          -2, {         -4 , 850000 } },
            { {         -7, 300000 },           2, {         -4 , 650000 } },
            { {         -7, 300000 },          -2, {          3 , 350000 } },
            { {          6, 300000 },          -3, {         -3 , 900000 } },
            { {          6, 300000 },           3, {          2 , 100000 } },
            { {         -7, 300000 },          -3, {          2 , 233333 } },
            { {         -7, 300000 },           3, {         -3 , 766667 } },
        };

        testCctimeUsDivRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsDivRT

    {
        struct cctime_test_ms_int tests[] =
        {
            { {          0, 500 },           2, {          0 , 250 } },
            { {          0, 500 },          -2, {         -1 , 750 } },
            { {          6, 500 },           1, {          6 , 500 } },
            { {          6, 500 },          -1, {         -7 , 500 } },
            { {         -7, 500 },           1, {         -7 , 500 } },
            { {         -7, 500 },          -1, {          6 , 500 } },
            { {          6, 500 },           2, {          3 , 250 } },
            { {          6, 500 },          -2, {         -4 , 750 } },
            { {         -7, 500 },           2, {         -4 , 750 } },
            { {         -7, 500 },          -2, {          3 , 250 } },
            { {          6, 500 },          -3, {         -3 , 833 } },
            { {          6, 500 },           3, {          2 , 167 } },
            { {         -7, 500 },          -3, {          2 , 167 } },
            { {         -7, 500 },           3, {         -3 , 833 } },
            { {          6, 300 },           1, {          6 , 300 } },
            { {          6, 300 },          -1, {         -7 , 700 } },
            { {         -7, 300 },           1, {         -7 , 300 } },
            { {         -7, 300 },          -1, {          6 , 700 } },
            { {          6, 300 },           2, {          3 , 150 } },
            { {          6, 300 },          -2, {         -4 , 850 } },
            { {         -7, 300 },           2, {         -4 , 650 } },
            { {         -7, 300 },          -2, {          3 , 350 } },
            { {          6, 300 },          -3, {         -3 , 900 } },
            { {          6, 300 },           3, {          2 , 100 } },
            { {         -7, 300 },          -3, {          2 , 233 } },
            { {         -7, 300 },           3, {         -3 , 767 } },
        };

        testCctimeMsDivRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsQuotientRT

    {
        struct cctime_test_2ns_int tests[] =
        {
            { {          12, 345678987 }, {           0,        13 },   949667614 },
            { {          12, 345678987 }, {           0, 949667614 },          13 },
            { {          12, 345678987 }, {           0, 949667615 },          12 },
            { {     -100001,         0 }, {          10,         0 },      -10000 },
            { {     -100001,         0 }, {         -11,         1 },        9091 },
            { {     -100001,         0 }, {           0, 999999999 },     -100001 },
        };

        testCctimeNsQuotientRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsQuotientRT

    {
        struct cctime_test_2us_int tests[] =
        {
            { {          12, 345678 }, {           0,      7 },     1763668 },
            { {          12, 345678 }, {          -2, 236332 },          -7 },
            { {          12, 345678 }, {           1, 763669 },           6 },
        };

        testCctimeUsQuotientRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsQuotientRT

    {
        struct cctime_test_2ms_int tests[] =
        {
            { {          12, 345 }, {           0,   4 },  3086 },
            { {          12, 345 }, {          -4, 914 },    -4 },
            { {          12, 345 }, {           3,  87 },     3 },
        };

        testCctimeMsQuotientRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsCmpAbsTimesRT

    {
        struct cctime_test_2ns_int tests[] =
        {
            { {           4, 500000000 }, {           4, 500000000 },  0 },
            { {           4, 500000000 }, {           4, 500000001 }, -1 },
            { {           4, 500000000 }, {           4, 499999999 },  1 },
            { {           4, 500000000 }, {           3, 600000000 },  1 },
            { {           4, 500000000 }, {           5, 400000000 }, -1 },
            { {           4, 500000000 }, {           0,         1 },  1 },
            { {           4, 500000000 }, {  4294967295, 999999999 }, -1 },
            { {  4294967295, 999999999 }, {           0,         0 },  1 },
        };

        testCctimeNsCmpAbsTimesRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsCmpAbsTimesRT

    {
        struct cctime_test_2us_int tests[] =
        {
            { {           4, 500000 }, {           4, 500000 },  0 },
            { {           4, 500000 }, {           4, 500001 }, -1 },
            { {           4, 500000 }, {           4, 499999 },  1 },
            { {           4, 500000 }, {           3, 600000 },  1 },
            { {           4, 500000 }, {           5, 400000 }, -1 },
            { {           4, 500000 }, {           0,      1 },  1 },
            { {           4, 500000 }, {  4294967295, 999999 }, -1 },
            { {  4294967295, 999999 }, {           0,      0 },  1 },
        };

        testCctimeUsCmpAbsTimesRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsCmpAbsTimesRT

    {
        struct cctime_test_2ms_int tests[] =
        {
            { {           4, 500000000 }, {           4, 500000000 },  0 },
            { {           4, 500000000 }, {           4, 500000010 }, -1 },
            { {           4, 500000000 }, {           4, 499999999 },  1 },
            { {           4, 500000000 }, {           3, 600000000 },  1 },
            { {           4, 500000000 }, {           5, 400000000 }, -1 },
            { {           4, 500000000 }, {           0,         1 },  1 },
            { {           4, 500000000 }, {  4294967295, 999999999 }, -1 },
            { {  4294967295, 999999999 }, {           0,         0 },  1 },
        };

        testCctimeMsCmpAbsTimesRT(tests, ARRAY_LEN(tests));
    }

    // cctimeNsCmpRelTimesRT

    {
        struct cctime_test_2ns_int tests[] =
        {
            { {           4, 500000000 }, {           4, 500000000 },  0 },
            { {           4, 500000000 }, {           4, 500000001 }, -1 },
            { {           4, 500000000 }, {           4, 499999999 },  1 },
            { {           4, 500000000 }, {           3, 600000000 },  1 },
            { {           4, 500000000 }, {           5, 400000000 }, -1 },
            { {           4, 500000000 }, {           0,         1 },  1 },
            { {           4, 500000000 }, { -2147483648,         0 },  1 },
            { {           4, 500000000 }, {  2147483647, 999999999 }, -1 },
            { { -2147483648,         0 }, {           4, 500000000 }, -1 },
            { {  2147483647, 999999999 }, {           4, 500000000 },  1 },
            { {          -4, 500000000 }, {          -4, 500000000 },  0 },
            { {          -4, 500000000 }, {          -4, 500000001 }, -1 },
            { {          -4, 500000000 }, {          -4, 499999999 },  1 },
            { {          -4, 500000000 }, {          -3, 600000000 }, -1 },
            { {          -4, 500000000 }, {          -5, 400000000 },  1 },
            { {          -4, 500000000 }, {           0,         1 }, -1 },
            { {          -4, 500000000 }, { -2147483648,         0 },  1 },
            { {          -4, 500000000 }, {  2147483647, 999999999 }, -1 },
            { { -2147483648,         0 }, {          -4, 500000000 }, -1 },
            { {  2147483647, 999999999 }, {          -4, 500000000 },  1 },
        };

        testCctimeNsCmpRelTimesRT(tests, ARRAY_LEN(tests));
    }

    // cctimeUsCmpRelTimesRT

    {
        struct cctime_test_2us_int tests[] =
        {
            { {           4, 500000 }, {           4, 500000 },  0 },
            { {           4, 500000 }, {           4, 500001 }, -1 },
            { {           4, 500000 }, {           4, 499999 },  1 },
            { {           4, 500000 }, {           3, 600000 },  1 },
            { {           4, 500000 }, {           5, 400000 }, -1 },
            { {           4, 500000 }, {           0,      1 },  1 },
            { {           4, 500000 }, { -2147483648,      0 },  1 },
            { {           4, 500000 }, {  2147483647, 999999 }, -1 },
            { { -2147483648,      0 }, {           4, 500000 }, -1 },
            { {  2147483647, 999999 }, {           4, 500000 },  1 },
            { {          -4, 500000 }, {          -4, 500000 },  0 },
            { {          -4, 500000 }, {          -4, 500001 }, -1 },
            { {          -4, 500000 }, {          -4, 499999 },  1 },
            { {          -4, 500000 }, {          -3, 600000 }, -1 },
            { {          -4, 500000 }, {          -5, 400000 },  1 },
            { {          -4, 500000 }, {           0,      1 }, -1 },
            { {          -4, 500000 }, { -2147483648,      0 },  1 },
            { {          -4, 500000 }, {  2147483647, 999999 }, -1 },
            { { -2147483648,      0 }, {          -4, 500000 }, -1 },
            { {  2147483647, 999999 }, {          -4, 500000 },  1 },
        };

        testCctimeUsCmpRelTimesRT(tests, ARRAY_LEN(tests));
    }

    // cctimeMsCmpRelTimesRT

    {
        struct cctime_test_2ms_int tests[] =
        {
            { {           4, 500 }, {           4, 500 },  0 },
            { {           4, 500 }, {           4, 501 }, -1 },
            { {           4, 500 }, {           4, 499 },  1 },
            { {           4, 500 }, {           3, 600 },  1 },
            { {           4, 500 }, {           5, 400 }, -1 },
            { {           4, 500 }, {           0,   1 },  1 },
            { {           4, 500 }, { -2147483648,   0 },  1 },
            { {           4, 500 }, {  2147483647, 999 }, -1 },
            { { -2147483648,   0 }, {           4, 500 }, -1 },
            { {  2147483647, 999 }, {           4, 500 },  1 },
            { {          -4, 500 }, {          -4, 500 },  0 },
            { {          -4, 500 }, {          -4, 501 }, -1 },
            { {          -4, 500 }, {          -4, 499 },  1 },
            { {          -4, 500 }, {          -3, 600 }, -1 },
            { {          -4, 500 }, {          -5, 400 },  1 },
            { {          -4, 500 }, {           0,   1 }, -1 },
            { {          -4, 500 }, { -2147483648,   0 },  1 },
            { {          -4, 500 }, {  2147483647, 999 }, -1 },
            { { -2147483648,   0 }, {          -4, 500 }, -1 },
            { {  2147483647, 999 }, {          -4, 500 },  1 },
        };

        testCctimeMsCmpRelTimesRT(tests, ARRAY_LEN(tests));
    }

    // Psuedo TAP footer - this should really be a header, but how are we supposed to know
    // the number of tests until the end? What a crazy protocol choice! %-/

    printf("0..%u\n", test_index);
}

// EOF
