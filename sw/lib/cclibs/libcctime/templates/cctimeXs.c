//! @file  cctimeXs.c
//! @brief Converter Control Time Library xxxxsecond functions
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2022. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libcctime.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libcctime.h"

//! Global zero xxxxsecond time variable - initialize to zero because this isn't automatic on TI DSPs

struct CC_xs_time const cc_zero_xs = { { 0 }, 0 };;

// The TMS320C6727 doesn't support 64-bit integers, but it does support doubles

#if defined (__TMS320C6727__) || defined (CCTIME_NO_INT64_T)
#define CCTIME_USE_DOUBLE
#endif

//! Function definitions

char * cctimeXsPrintRelTime(struct CC_xs_time cc_xs_time, char * const strbuf)
{
#if defined (__TMS320C6727__)

    // Return empty string on TMS320C6727 processor to avoid including stdio library due to the sprintf

    strbuf[0] = '\0';
#else
    if(cc_xs_time.secs.rel < 0 && cc_xs_time.xs > 0)
    {
        cc_xs_time.xs = CCTIME_XS_PER_S - cc_xs_time.xs;
        cc_xs_time.secs.rel++;
    }

    sprintf(strbuf, "%d.%0*d", (int)cc_xs_time.secs.rel, CCTIME_XS_RESOLUTION, (int)cc_xs_time.xs);
#endif

    return strbuf;
}



cc_double cctimeXsAbsTimeToDoubleRT(struct CC_xs_time const cc_xs_time)
{
    return (cc_double)cc_xs_time.secs.abs + (cc_double)cc_xs_time.xs * (1.0 / CCTIME_XS_PER_S);
}



cc_double cctimeXsRelTimeToDoubleRT(struct CC_xs_time const cc_xs_time)
{
    return (cc_double)cc_xs_time.secs.rel + (cc_double)cc_xs_time.xs * (1.0 / CCTIME_XS_PER_S);
}



struct CC_xs_time cctimeXsDoubleToRelTimeRT(cc_double const double_time)
{
    cc_double int_part;
    cc_double frac_part;
    struct CC_xs_time cc_xs_time;

    if(double_time >= 0.0)
    {
        frac_part = modf(double_time + 0.5/CCTIME_XS_PER_S, &int_part);
        cc_xs_time.secs.rel = (int32_t)int_part;
        cc_xs_time.xs = (int32_t)(frac_part * CCTIME_XS_PER_S);
    }
    else
    {
        frac_part = modf(double_time - 0.5/CCTIME_XS_PER_S, &int_part);
        cc_xs_time.secs.rel = (int32_t)int_part - 1;
        cc_xs_time.xs = CCTIME_XS_PER_S + (int32_t)(frac_part * CCTIME_XS_PER_S);
    }

    return cc_xs_time;
}



struct CC_xs_time cctimeXsAddDelayRT(struct CC_xs_time cc_xs_time,
                                     uint32_t    const delay_xs)
{
    if(delay_xs != 0)
    {
        uint32_t delay_s = delay_xs / CCTIME_XS_PER_S;

        cc_xs_time.xs += (delay_xs - delay_s * CCTIME_XS_PER_S);
        cc_xs_time.secs.abs += delay_s;

        if(cc_xs_time.xs >= CCTIME_XS_PER_S)
        {
            cc_xs_time.xs -= CCTIME_XS_PER_S;
            cc_xs_time.secs.abs++;
        }
    }

    return cc_xs_time;
}



struct CC_xs_time cctimeXsAddOffsetRT(struct CC_xs_time cc_xs_time,
                                      int32_t     const offset_xs)
{
    if(offset_xs != 0)
    {
        int32_t const offset_s = offset_xs / CCTIME_XS_PER_S;

        cc_xs_time.xs += (offset_xs - offset_s * CCTIME_XS_PER_S);
        cc_xs_time.secs.abs += offset_s;

        if(cc_xs_time.xs < 0)
        {
            cc_xs_time.xs += CCTIME_XS_PER_S;
            cc_xs_time.secs.abs--;
        }
        else if(cc_xs_time.xs >= CCTIME_XS_PER_S)
        {
            cc_xs_time.xs -= CCTIME_XS_PER_S;
            cc_xs_time.secs.abs++;
        }
    }

    return cc_xs_time;
}



struct CC_xs_time cctimeXsAddRT(struct CC_xs_time       cc_xs_time1,
                                struct CC_xs_time const cc_xs_time2)
{
    cc_xs_time1.xs += cc_xs_time2.xs;
    cc_xs_time1.secs.abs += cc_xs_time2.secs.abs;

    if(cc_xs_time1.xs >= CCTIME_XS_PER_S)
    {
        cc_xs_time1.xs -= CCTIME_XS_PER_S;
        cc_xs_time1.secs.abs++;
    }

    return cc_xs_time1;
}



struct CC_xs_time cctimeXsSubRT(struct CC_xs_time       cc_xs_time1,
                                struct CC_xs_time const cc_xs_time2)
{
    cc_xs_time1.xs -= cc_xs_time2.xs;
    cc_xs_time1.secs.abs -= cc_xs_time2.secs.abs;

    if(cc_xs_time1.xs < 0)
    {
        cc_xs_time1.xs += CCTIME_XS_PER_S;
        cc_xs_time1.secs.abs--;
    }

    return cc_xs_time1;
}



int32_t cctimeXsDiffRT(struct CC_xs_time const cc_xs_time1,
                       struct CC_xs_time const cc_xs_time2)
{
    return (int32_t)(cc_xs_time1.secs.abs - cc_xs_time2.secs.abs) * CCTIME_XS_PER_S + cc_xs_time1.xs - cc_xs_time2.xs;
}



struct CC_xs_time cctimeXsProdRT(struct CC_xs_time       cc_xs_time1,
                                 struct CC_xs_time const cc_xs_time2)
{
#ifdef CCTIME_USE_DOUBLE

    return cctimeXsDoubleToRelTimeRT(((cc_double)cc_xs_time1.secs.rel + (cc_double)cc_xs_time1.xs * (1.0 / CCTIME_XS_PER_S))
                                   * ((cc_double)cc_xs_time2.secs.rel + (cc_double)cc_xs_time2.xs * (1.0 / CCTIME_XS_PER_S)));
#else
    int64_t time_xs_2 = ((int64_t)cc_xs_time1.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time1.xs)
                      * ((int64_t)cc_xs_time2.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time2.xs);

    if(time_xs_2 >= 0.0)
    {
        time_xs_2 += CCTIME_XS_PER_S/2;
        cc_xs_time1.secs.rel = (int32_t)(time_xs_2 / CCTIME_XS_PER_S_2);
        cc_xs_time1.xs = (int32_t)((time_xs_2 - cc_xs_time1.secs.rel * CCTIME_XS_PER_S_2) / CCTIME_XS_PER_S);
    }
    else
    {
        time_xs_2 -= CCTIME_XS_PER_S/2;
        cc_xs_time1.secs.rel = (int32_t)(time_xs_2 / CCTIME_XS_PER_S_2);
        cc_xs_time1.xs = CCTIME_XS_PER_S + (int32_t)((time_xs_2 - cc_xs_time1.secs.rel * CCTIME_XS_PER_S_2) / CCTIME_XS_PER_S);
        cc_xs_time1.secs.rel--;
    }

    return cc_xs_time1;
#endif
}



#ifndef CCTIME_USE_DOUBLE
static struct CC_xs_time cctimeXsInt64ToTime(int64_t const time_xs)
{
    struct CC_xs_time cc_xs_time;

    cc_xs_time.secs.rel = (int32_t)(time_xs / CCTIME_XS_PER_S);

    if(time_xs >= 0.0)
    {
        cc_xs_time.xs = (int32_t)(time_xs - cc_xs_time.secs.rel * CCTIME_XS_PER_S);
    }
    else
    {
        cc_xs_time.xs = CCTIME_XS_PER_S + (int32_t)(time_xs - cc_xs_time.secs.rel * CCTIME_XS_PER_S);
        cc_xs_time.secs.rel--;
    }

    return cc_xs_time;
}
#endif



struct CC_xs_time cctimeXsMulRT(struct CC_xs_time cc_xs_time,
                                int32_t     const multiplier)
{
#ifdef CCTIME_USE_DOUBLE

    return cctimeXsDoubleToRelTimeRT(((cc_double)cc_xs_time.secs.rel + (cc_double)cc_xs_time.xs * (1.0 / CCTIME_XS_PER_S)) * (cc_double)multiplier);
#else

    return cctimeXsInt64ToTime(((int64_t)cc_xs_time.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time.xs) * (int64_t)multiplier);
#endif
}



struct CC_xs_time cctimeXsDivRT(struct CC_xs_time const cc_xs_time,
                                int32_t           const divisor)
{
#ifdef CCTIME_USE_DOUBLE

    return cctimeXsDoubleToRelTimeRT(((cc_double)cc_xs_time.secs.rel + (cc_double)cc_xs_time.xs * (1.0 / CCTIME_XS_PER_S)) / (cc_double)divisor);
#else

// Rounding required adding half the divisor to the dividend in the direction away from zero.
// This means inverting the rounding if the sign of the divisor and dividend are different.
// This is most efficiently computed by taking the XOR of the sign bits.

    int64_t rounding = divisor / 2;

    if(((cc_xs_time.secs.rel ^ divisor) & 0x8000000) != 0)
    {
        rounding = -rounding;
    }

    return cctimeXsInt64ToTime(((int64_t)cc_xs_time.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time.xs + rounding) / (int64_t)divisor);
#endif
}



int32_t cctimeXsQuotientRT(struct CC_xs_time const cc_xs_time1,
                           struct CC_xs_time const cc_xs_time2)
{
#ifdef CCTIME_USE_DOUBLE
    return (int32_t)(((cc_double)cc_xs_time1.secs.rel * CCTIME_XS_PER_S + (cc_double)cc_xs_time1.xs)
                   / ((cc_double)cc_xs_time2.secs.rel * CCTIME_XS_PER_S + (cc_double)cc_xs_time2.xs));
#else
    return (int32_t)(((int64_t)cc_xs_time1.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time1.xs)
                   / ((int64_t)cc_xs_time2.secs.rel * CCTIME_XS_PER_S + (int64_t)cc_xs_time2.xs));
#endif
}



int32_t cctimeXsCmpAbsTimesRT(struct CC_xs_time const cc_xs_time1,
                              struct CC_xs_time const cc_xs_time2)
{
    int32_t result;

    if(cc_xs_time1.secs.abs == cc_xs_time2.secs.abs)
    {
        result = cc_xs_time1.xs - cc_xs_time2.xs;
    }
    else
    {
        result = 2 * (cc_xs_time1.secs.abs > cc_xs_time2.secs.abs) - 1;
    }

    return result;
}



int32_t cctimeXsCmpRelTimesRT(struct CC_xs_time const cc_xs_time1,
                              struct CC_xs_time const cc_xs_time2)
{
    int32_t result;

    if(cc_xs_time1.secs.rel == cc_xs_time2.secs.rel)
    {
        result = cc_xs_time1.xs - cc_xs_time2.xs;
    }
    else
    {
        result = 2 * (cc_xs_time1.secs.rel > cc_xs_time2.secs.rel) - 1;
    }

    return result;
}

// EOF
