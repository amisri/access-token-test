// Functions to test xxxxsecond libcctime functions

// test structures

struct cctime_test_xs_double
{
    struct CC_xs_time   t_xs;
    cc_double           t_dbl;
};

struct cctime_test_xs_int
{
    struct CC_xs_time   t_xs;
    int32_t             t_int;
    struct CC_xs_time   result_xs;
};

struct cctime_test_2xs_int
{
    struct CC_xs_time   t1_xs;
    struct CC_xs_time   t2_xs;
    int32_t             t_int;
};

struct cctime_test_xs_uint
{
    struct CC_xs_time   t_xs;
    uint32_t            t_uint;
    struct CC_xs_time   result_xs;
};

struct cctime_test_2xs_to_xs
{
    struct CC_xs_time   t1_xs;
    struct CC_xs_time   t2_xs;
    struct CC_xs_time   result_xs;
};

// static variables

static uint32_t i_xs;


// static functions

static void testCctimeXsAbsTimeToDoubleRT(struct cctime_test_xs_double * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkDoubleTime(i_xs + 1, "cctimeXsAbsTimeToDoubleRT", cctimeXsAbsTimeToDoubleRT(tests[i_xs].t_xs), tests[i_xs].t_dbl);
    }
}



static void testCctimeXsRelTimeToDoubleRT(struct cctime_test_xs_double * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkDoubleTime(i_xs + 1, "cctimeXsRelTimeToDoubleRT", cctimeXsRelTimeToDoubleRT(tests[i_xs].t_xs), tests[i_xs].t_dbl);
    }
}



static void testCctimeXsDoubleToRelTimeRT(struct cctime_test_xs_double * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsDoubleToRelTimeRT(tests[i_xs].t_dbl);

        checkRelTime(i_xs + 1, "cctimeXsDoubleToRelTimeRT", result_xs.secs.rel, result_xs.xs, tests[i_xs].t_xs.secs.rel, tests[i_xs].t_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsAddDelayRT(struct cctime_test_xs_uint * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsAddDelayRT(tests[i_xs].t_xs, tests[i_xs].t_uint);

        checkAbsTime(i_xs + 1, "cctimeXsAddDelayRT", result_xs.secs.abs, result_xs.xs, tests[i_xs].result_xs.secs.abs, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsAddOffsetRT(struct cctime_test_xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsAddOffsetRT(tests[i_xs].t_xs, tests[i_xs].t_int);

        checkRelTime(i_xs + 1, "cctimeXsAddDelayRT", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testAbsCctimeXsAddRT(struct cctime_test_2xs_to_xs * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsAddRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs);

        checkAbsTime(i_xs + 1, "cctimeXsAddRT:Abs", result_xs.secs.abs, result_xs.xs, tests[i_xs].result_xs.secs.abs, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testRelCctimeXsAddRT(struct cctime_test_2xs_to_xs * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsAddRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs);

        checkRelTime(i_xs + 1, "cctimeXsAddRT:Rel", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testRelCctimeXsSubRT(struct cctime_test_2xs_to_xs * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsSubRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs);

        checkRelTime(i_xs + 1, "cctimeXsSubRT:Rel", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsDiffRT(struct cctime_test_2xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkIntResult(i_xs + 1, "cctimeXsDiffRT", cctimeXsDiffRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs), tests[i_xs].t_int);
    }
}



static void testRelCctimeXsProdRT(struct cctime_test_2xs_to_xs * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsProdRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs);

        checkRelTime(i_xs + 1, "cctimeXsProdRT:Rel", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsMulRT(struct cctime_test_xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsMulRT(tests[i_xs].t_xs, tests[i_xs].t_int);

        checkRelTime(i_xs + 1, "cctimeXsMulRT", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsDivRT(struct cctime_test_xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        struct CC_xs_time const result_xs = cctimeXsDivRT(tests[i_xs].t_xs, tests[i_xs].t_int);

        checkRelTime(i_xs + 1, "cctimeXsDivRT", result_xs.secs.rel, result_xs.xs, tests[i_xs].result_xs.secs.rel, tests[i_xs].result_xs.xs, CCTIME_XS_RESOLUTION);
    }
}



static void testCctimeXsQuotientRT(struct cctime_test_2xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkIntResult(i_xs + 1, "cctimeXsQuotientRT", cctimeXsQuotientRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs), tests[i_xs].t_int);
    }
}



static void testCctimeXsCmpAbsTimesRT(struct cctime_test_2xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkCmp(i_xs + 1, "cctimeXsCmpAbsTimesRT", cctimeXsCmpAbsTimesRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs), tests[i_xs].t_int);
    }
}



static void testCctimeXsCmpRelTimesRT(struct cctime_test_2xs_int * const tests, uint32_t const num_tests)
{
    for(i_xs = 0 ; i_xs < num_tests ; i_xs++)
    {
        checkCmp(i_xs + 1, "cctimeXsCmpRelTimesRT", cctimeXsCmpRelTimesRT(tests[i_xs].t1_xs, tests[i_xs].t2_xs), tests[i_xs].t_int);
    }
}

// EOF
