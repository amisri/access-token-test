//! @file  cctimeXs.h
//! @brief Converter Control Time library header file for Xxxxsecond functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2018. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libcctime.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//! Global zero xxxxsecond time variable

extern struct CC_xs_time const cc_zero_xs;


//! This function converts a xxxxsecond resolution relative time into an ASCII string.
//!
//! IMPORTANT: It is the responsibility of the calling function to reserve enough space
//! in strbuf to contain the maximum length of the string. This is 22 characters.
//!
//! NOTE: There is no equivalent function for absolute time, since it is simple to
//! print an abs time using a fixed format, for example:
//!
//!    "%u.%0*d", cc_xs_time.secs.rel, CCTIME_XS_RESOLUTION, cc_xs_time.xs
//!
//! @param[in] cc_xs_time   Relative time to convert to ASCII
//! @param[in] strbuf       Pointer to string buffer (22 characters in length)
//!
//! @return  strbuf

char * cctimeXsPrintRelTime(struct CC_xs_time cc_xs_time,
                            char            * strbuf);


//! This function converts a xxxxsecond resolution absolute time into a double
//!
//! @param[in] cc_xs_time   Absolute time to convert to double
//!
//! @return  cc_xs_time as a double

cc_double cctimeXsAbsTimeToDoubleRT(struct CC_xs_time cc_xs_time);


//! This function converts a xxxxsecond resolution relative time into a double
//!
//! @param[in] cc_xs_time   Relative time to convert to double
//!
//! @return  cc_xs_time as a double

cc_double cctimeXsRelTimeToDoubleRT(struct CC_xs_time cc_xs_time);


//! This function converts a relative time in a double into a xxxxsecond
//! resolution relative time, rounding to the nearest xxxxsecond.
//!
//! @param[in] double_time   Relative time to convert from double
//!
//! @return  double_time as a relative time CC_ns_time

struct CC_xs_time cctimeXsDoubleToRelTimeRT(cc_double double_time);


//! This function adds a positive delay in xxxxseconds to a xxxxsecond resolution time.
//!
//! cc_xs_time can be an unsigned absolute time or signed relative time.
//!
//! @param[in] cc_xs_time   Time to add the delay to.
//! @param[in] delay_xs     Delay in xxxxseconds.
//!
//! @return  cc_xs_time + delay_xs

struct CC_xs_time cctimeXsAddDelayRT(struct CC_xs_time cc_xs_time,
                                     uint32_t          delay_xs);


//! This function adds a signed offset in xxxxseconds to a xxxxsecond resolution time.
//!
//! cc_xs_time can be an unsigned absolute time or signed relative time.
//!
//! @param[in] cc_xs_time   Time to add the offset to.
//! @param[in] offset_xs    Signed offset in xxxxseconds.
//!
//! @return  cc_xs_time + delay_xs

struct CC_xs_time cctimeXsAddOffsetRT(struct CC_xs_time cc_xs_time,
                                      int32_t           offset_xs);


//! This function adds two xxxxsecond resolution times.
//!
//! The times can be unsigned absolute times or signed relative times.
//!
//! @param[in] cc_xs_time1  First time
//! @param[in] cc_xs_time2  Second time
//!
//! @return  cc_xs_time1 + cc_xs_time2

struct CC_xs_time cctimeXsAddRT(struct CC_xs_time cc_xs_time1,
                                struct CC_xs_time cc_xs_time2);


//! This function subtracts two xxxxsecond times.
//!
//! The times can be unsigned absolute times or signed relative times.
//!
//! @param[in] cc_xs_time1  First time
//! @param[in] cc_xs_time2  Second time
//!
//! @return  cc_xs_time1 - cc_xs_time2

struct CC_xs_time cctimeXsSubRT(struct CC_xs_time cc_xs_time1,
                                struct CC_xs_time cc_xs_time2);


//! This function returns the signed difference in xxxxseconds between two xxxxsecond resolution times.
//!
//! The times can be unsigned absolute times or signed relative times.
//! Note: There is no protection against overflow of the result.
//!
//! @param[in] cc_xs_time1  First time
//! @param[in] cc_xs_time2  Second time
//!
//! @return  cc_xs_time1 - cc_xs_time2 in simple signed xxxxseconds

int32_t cctimeXsDiffRT(struct CC_xs_time cc_xs_time1,
                       struct CC_xs_time cc_xs_time2);


//! This function returns product of two signed xxxxsecond resolution relative times
//!
//! Note: The function does not protect against overflow.
//!
//! @param[in] cc_xs_time1  First relative time
//! @param[in] cc_xs_time2  Second relative time
//!
//! @return  cc_xs_time1 * cc_xs_time2

struct CC_xs_time cctimeXsProdRT(struct CC_xs_time cc_xs_time1,
                                 struct CC_xs_time cc_xs_time2);


//! This function returns product of a signed xxxxsecond resolution relative time and a signed multiplier
//!
//! Note: The function does not protect against overflow.
//!
//! @param[in] cc_xs_time  Signed relative time
//! @param[in] multiplier  Signed multiplier
//!
//! @return  cc_xs_time * multiplier

struct CC_xs_time cctimeXsMulRT(struct CC_xs_time cc_xs_time,
                                int32_t           multiplier);


//! This function returns the division of a signed xxxxsecond resolution relative time by a signed integer divisor
//!
//! Note: the result is rounded to the nearest xxxxsecond.
//!
//! @param[in] cc_xs_time   Signed relative time
//! @param[in] divisor      Signed divisor
//!
//! @return  cc_xs_time / divisor

struct CC_xs_time cctimeXsDivRT(struct CC_xs_time cc_xs_time,
                                int32_t           divisor);


//! This function returns quotient from the division of two signed xxxxsecond resolution relative times
//!
//! Note: the quotient is rounded to the nearest integer towards zero. E.g. 2.7 -> 2, -2.7 -> -2.
//!
//! @param[in] cc_xs_time1  First relative time
//! @param[in] cc_xs_time2  Second relative time
//!
//! @return  cc_xs_time1 / cc_xs_time2 rounded to the nearest integer towards zero

int32_t cctimeXsQuotientRT(struct CC_xs_time cc_xs_time1,
                           struct CC_xs_time cc_xs_time2);


//! This function compares two unsigned xxxxsecond resolution absolute times.
//!
//! @param[in] cc_xs_time1   First absolute time
//! @param[in] cc_xs_time2   Second absolute time
//!
//! @retval  negative value     cc_xs_time1 is earlier than cc_xs_time2
//! @retval  zero               cc_xs_time1 is equal to cc_xs_time2
//! @retval  positive value     cc_xs_time1 is later than cc_xs_time2

int32_t cctimeXsCmpAbsTimesRT(struct CC_xs_time cc_xs_time1,
                              struct CC_xs_time cc_xs_time2);


//! This function compares two signed xxxxsecond resolution relative times.
//!
//! @param[in] cc_xs_time1   First relative time
//! @param[in] cc_xs_time2   Second relative time
//!
//! @retval  negative value     cc_xs_time1 is less than cc_xs_time2
//! @retval  zero               cc_xs_time1 is equal to cc_xs_time2
//! @retval  positive value     cc_xs_time1 is greater than cc_xs_time2

int32_t cctimeXsCmpRelTimesRT(struct CC_xs_time cc_xs_time1,
                              struct CC_xs_time cc_xs_time2);

#ifdef __cplusplus
}
#endif

// EOF
