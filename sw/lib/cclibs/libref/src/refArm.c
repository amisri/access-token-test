//! @file  refArm.c
//! @brief Converter Control Function Manager library reference arming functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"
#include "libref/refFgPars.h"
#include "libref/refFgType.h"

// Constants

#define REF_ARM_MISMATCH_LIMIT        1.0E-5F

// Declare typedefs for pointers to parameter setting and arming functions

typedef void (*refArmFunc)(struct REF_mgr         * const,
                           uint32_t                 const,
                           uint32_t                 const,
                           enum REG_mode            const,
                           bool                     const,
                           struct FG_limits const * const,
                           union  FG_pars         * const,
                           struct FG_error        * const);

// Initialize array of the first parameter index per function type - IMPORTANT: this must be in the same order as enum FG_type

static enum REF_fg_par_idx const first_fg_par_idx[] =
{
    REF_FG_PAR_NULL             ,   // NONE
    REF_FG_PAR_NULL             ,   // ZERO
    REF_FG_PAR_RAMP_INITIAL_REF ,   // RAMP
    REF_FG_PAR_PULSE_REF        ,   // PULSE
    REF_FG_PAR_PLEP_INITIAL_REF ,   // PLEP
    REF_FG_PAR_PPPL_INITIAL_REF ,   // PPPL
    REF_FG_PAR_CUBEXP_REF       ,   // CUBEXP
    REF_FG_PAR_TABLE_FUNCTION   ,   // TABLE
    REF_FG_PAR_TRIM_INITIAL_REF ,   // LTRIM
    REF_FG_PAR_TRIM_INITIAL_REF ,   // CTRIM
    REF_FG_PAR_TEST_INITIAL_REF ,   // STEPS
    REF_FG_PAR_TEST_INITIAL_REF ,   // SQUARE
    REF_FG_PAR_TEST_INITIAL_REF ,   // SINE
    REF_FG_PAR_TEST_INITIAL_REF ,   // COSINE
    REF_FG_PAR_TEST_INITIAL_REF ,   // OFFCOS
    REF_FG_PAR_PRBS_INITIAL_REF ,   // PRBS
};


// Initialize array of the number of parameters per function type - IMPORTANT: this must be in the same order as enum FG_type

static uint8_t const num_fg_pars[] =
{
    0              ,   // NONE
    0              ,   // ZERO
    RAMP_NUM_PARS  ,   // RAMP
    PULSE_NUM_PARS ,   // PULSE
    PLEP_NUM_PARS  ,   // PLEP
    8              ,   // PPPL    Note: PPPL_NUM_PARS includes _NUM_ELS parameters which should not be counted
    3              ,   // CUBEXP  Note: CUBEXP_NUM_PARS includes _NUM_ELS parameters which should not be counted
    1              ,   // TABLE   Note: TABLE_NUM_PARS is not generated in refPars.h
    TRIM_NUM_PARS  ,   // LTRIM
    TRIM_NUM_PARS  ,   // CTRIM
    TEST_NUM_PARS  ,   // STEPS
    TEST_NUM_PARS  ,   // SQUARE
    TEST_NUM_PARS  ,   // SINE
    TEST_NUM_PARS  ,   // COSINE
    TEST_NUM_PARS  ,   // OFFCOS
    PRBS_NUM_PARS  ,   // PRBS
};


// NON-RT   refArmInitParGroupMasks

void refArmInitParGroupMasks(struct REF_mgr * const ref_mgr)
{
    CC_STATIC_ASSERT(CC_ARRAY_LEN(first_fg_par_idx) == FG_NUM_FUNCS, size_of_first_fg_par_idx_array_doesnt_correspond_to_FGC_NUM_FUNCS);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(num_fg_pars)      == FG_NUM_FUNCS, size_of_num_fg_pars_array_doesnt_correspond_to_FGC_NUM_FUNCS);

    // If any parameter that is part of a parameter group (e.g. for the PLEP function)
    // has a zero sub_sel_step or cyc_sel_step, then the bit for that group will be cleared in the
    // par_group_mask for sub_sel or cyc_sel.  This will allow refArm() to reject attempts
    // arm a [sub_sel][cyc_sel] that is not supported by the application.

    uint32_t fg_type;

    for(fg_type = 1 ; fg_type < FG_NUM_FUNCS ; fg_type++)
    {
        uint32_t            num_pars = num_fg_pars[fg_type];
        struct REF_fg_par * fg_par   = &ref_mgr->u.fg_par[first_fg_par_idx[fg_type]];

        while(num_pars-- > 0)
        {
            ref_mgr->not_sub_sel_fg_types[fg_type] |= fg_par->meta.sub_sel_step == 0;
            ref_mgr->not_cyc_sel_fg_types[fg_type] |= fg_par->meta.cyc_sel_step == 0;
        }

    }
}



cc_float refArmClipRefRT(struct REF_mgr const * const ref_mgr, cc_float ref)
{
    struct REG_lim_ref * const lim_ref = ref_mgr->reg_mgr->lim_ref;

    if(lim_ref->flags.unipolar == true)
    {
        cc_float const standby = lim_ref->standby;

        if(fabsf(ref) < standby)
        {
            ref = polswitchIsNegative(ref_mgr->polswitch_mgr) == true
                ? -standby
                : standby;
        }
    }

    return ref;
}

//! NON-RT   refArmInitialRef
//!
//! Return a pointer initial_ref parameter for arming a reference function.
//!
//! The initial reference to use when arming a function depends upon the reference state, sub_sel and cyc_sel.
//!
//! @param[in]  ref_mgr     Pointer to reference manager structure.
//! @param[in]  pointer     Pointer to variable to use for ref_fg_unlimited if IDLE and cyc_sel and sub_sel are zero.
//! @param[in]  sub_sel     Sub-device selector index - must be in range 0 to ref_mgr->num_sub_sels.
//! @param[in]  cyc_sel     Pointer to reference manager structure.
//! @param[in]  fg_par_idx  Intial_ref parameter index, e.g. REF_FG_PAR_PLEP_INITIAL_REF.
//!
//! @returns    Pointer to initial reference that can be overwritten if needed by the restore function

static cc_float * refArmInitialRef(struct REF_mgr    * const ref_mgr,
                                   cc_float          * const pointer,
                                   uint32_t            const sub_sel,
                                   uint32_t            const cyc_sel,
                                   enum REF_fg_par_idx const fg_par_idx)
{
    // When in IDLE state and sub/cyc_sel are equal to zero,
    // then the initial ref is taken from the actual function generator reference.
    // This is put in the variable pointed to by pointer, which can be on the stack of the calling function.

    if(ref_mgr->ref_state == REF_IDLE && cyc_sel == 0 && sub_sel == 0)
    {
        *pointer = ref_mgr->ref_fg_unlimited;

        return pointer;
    }

    // Otherwise the function is taken from the supplied INITIAL_REF parameter (always a float)

    return (cc_float *)refMgrFgParPointerSubCycFunc(ref_mgr, sub_sel, cyc_sel, fg_par_idx);
}


// NON-RT   refArmNone

static void refArmNone(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    fgNoneArmRT(fg_pars, fg_error);
}


// NON-RT   refArmZero

static void refArmZero(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    fgZeroArmRT(fg_pars, fg_error);
}


// NON-RT   refArmRamp

static void refArmRamp(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_RAMP_INITIAL_REF);

    // Try to the arm the RAMP

    fgRampArm(fg_limits,
              polswitchIsAutomatic(ref_mgr->polswitch_mgr),
              polswitchIsNegative(ref_mgr->polswitch_mgr),
              test_arm,
             *initial_ref_pointer,
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_FINAL_REF),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_ACCELERATION),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_LINEAR_RATE),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_DECELERATION),
              fg_pars,
              fg_error);
}


// NON-RT   refArmPulse

static void refArmPulse(struct REF_mgr        * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Reject attempt to arm a PULSE to be run in RUNNING state

    if(ref_mgr->ref_state == REF_IDLE && cyc_sel == 0)
    {
        fgError(FG_INVALID_REF_STATE,               // fg_errno
                FG_PULSE,                           // fg_type
                0,                                  // error index
                0.0F,                               // error data 0
                0.0F,                               // error data 1
                0.0F,                               // error data 2
                0.0F,                               // error data 3
                fg_error);

        return;
    }

    // Try to the arm the PULSE to be run in CYCLING state only

    fgPulseArm( fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PULSE_REF),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PULSE_DURATION),
                fg_pars,
                fg_error);
}


// NON-RT   refArmPlep

static void refArmPlep(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_PLEP_INITIAL_REF);
    cc_float   acceleration        = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_ACCELERATION);

    // Get initial_rate and final_rate if the pointers are defined - these is used by ccrt to validate the PLEP function

    cc_float test_initial_rate = 0.0F;

    if(ref_mgr->test_plep_initial_rate != NULL)
    {
        test_initial_rate = *ref_mgr->test_plep_initial_rate;
    }

    cc_float test_final_rate = 0.0F;

    if(ref_mgr->test_plep_final_rate != NULL)
    {
        test_final_rate = *ref_mgr->test_plep_final_rate;
    }

    // Prepare automatic exp_final for REG_CURRENT or REG_FIELD only

    cc_float       exp_final = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_EXP_FINAL);
    cc_float const final_ref = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_FINAL_REF);
    cc_float const exp_tc    = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_EXP_TC);

    bool const polswitch_auto = polswitchIsAutomatic(ref_mgr->polswitch_mgr);
    bool const polswitch_neg  = polswitchIsNegative(ref_mgr->polswitch_mgr);
    bool const invert_limits  =    (polswitch_auto == false && polswitch_neg == true)
                                || (polswitch_auto == true && *initial_ref_pointer < 0.0F && final_ref < 0.0F);

    if(reg_mode != REG_VOLTAGE && exp_final == 0.0F)
    {
        // Calculate exp_final by keeping a 5% margin in voltage

        struct REG_mgr * const reg_mgr   = ref_mgr->reg_mgr;
        cc_float         const v_pos     = *ref_mgr->ref_limits[REG_VOLTAGE].pos;
        cc_float         const v_neg     = *ref_mgr->ref_limits[REG_VOLTAGE].neg;

        // Set ascending flag to true if |V_REF| needs to increase to produce the ramp

        bool const ascending = invert_limits == false
                             ? final_ref >= *initial_ref_pointer
                             : final_ref <= *initial_ref_pointer;

        exp_final = ascending == true
                  ? (v_pos - 0.05 * (v_pos - v_neg)) * reg_mgr->load_pars.gain2
                  : (v_neg + 0.05 * (v_pos - v_neg)) * reg_mgr->load_pars.gain2;

        // If regulating field, convert exp_final from amps to gauss

        if(reg_mode == REG_FIELD)
        {
            exp_final *= regMgrParValue(reg_mgr, LOAD_GAUSS_PER_AMP);
        }

        // For descending PLEP on a 1 or 2-quadrant converter, clip exp_final at 80% of the standby level

        bool const unipolar = reg_mode == REG_CURRENT
                            ? regMgrVarValue(reg_mgr, FLAG_I_UNIPOLAR) == true
                            : regMgrVarValue(reg_mgr, FLAG_B_UNIPOLAR) == true;

        if(unipolar == true && ascending == false)
        {
            cc_float const max_exp_final = *ref_mgr->ref_limits[reg_mode].standby * 0.8F;

            if(exp_final > max_exp_final)
            {
                exp_final = max_exp_final;
            }
        }

        // If limits are inverted then invert exp_final

        if(invert_limits == true)
        {
            exp_final = -exp_final;
        }
    }

    // Try to the arm the PLEP

    fgPlepArm(fg_limits,
              polswitch_auto,
              polswitch_neg,
              test_arm,
              test_initial_rate,
              test_final_rate,
             *initial_ref_pointer,
              final_ref,
              acceleration,
              acceleration,
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_LINEAR_RATE),
              exp_tc,
              exp_final,
              fg_pars,
              fg_error);
}


// NON-RT   refArmPppl

static void refArmPppl(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Verify that PPPL parameter array lengths are all the same and in the range 1 - FG_MAX_PPPLS

    uint32_t num_pppls = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION1_NUM_ELS);

    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_PPPL_INITIAL_REF);

    if(num_pppls == 0 || num_pppls > FG_MAX_PPPLS ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION2_NUM_ELS) ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION3_NUM_ELS) ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE2_NUM_ELS        ) ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE4_NUM_ELS        ) ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_REF4_NUM_ELS         ) ||
       num_pppls != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_DURATION4_NUM_ELS    ))
    {
        fgError(FG_BAD_ARRAY_LEN,                                                                           // fg_errno
                FG_PPPL,                                                                                    // fg_type
                0,                                                                                          // error index
                (cc_float)num_pppls,                                                                        // error data 0
                (cc_float)refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION2_NUM_ELS), // error data 1
                (cc_float)refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION3_NUM_ELS), // error data 2
                (cc_float)refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE2_NUM_ELS        ), // error data 3
                fg_error);

        return;
    }

    // Try to the arm the PPPL

    fgPpplArm(fg_limits,
              polswitchIsAutomatic(ref_mgr->polswitch_mgr),
              polswitchIsNegative(ref_mgr->polswitch_mgr),
              test_arm,
             *initial_ref_pointer,
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION1),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION2),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION3),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE2),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE4),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_REF4),
              refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_DURATION4),
              num_pppls,
              fg_pars,
              fg_error);
}


// NON-RT   refArmCubexp

static void refArmCubexp(struct REF_mgr         * const ref_mgr,
                         uint32_t                 const sub_sel,
                         uint32_t                 const cyc_sel,
                         enum REG_mode            const reg_mode,
                         bool                     const test_arm,
                         struct FG_limits const * const fg_limits,
                         union  FG_pars         * const fg_pars,
                         struct FG_error        * const fg_error)
{
    // Verify that CUBEXP parameter array lengths are all the same and in the range 2 - FG_MAX_CUBEXPS

    uint32_t num_points = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_REF_NUM_ELS);

    if(num_points < 2 || num_points > FG_CUBEXP_MAX_POINTS ||
       num_points != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_RATE_NUM_ELS) ||
       num_points != refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME_NUM_ELS))
    {
        fgError(FG_BAD_ARRAY_LEN,                                                                            // fg_errno
                FG_CUBEXP,                                                                                   // fg_type
                0,                                                                                           // error index
                (cc_float)num_points,                                                                        // error data 0
                (cc_float)refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_RATE_NUM_ELS),         // error data 1
                (cc_float)refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME_NUM_ELS),         // error data 2
                0.0F,                                                                                        // error data 3
                fg_error);

        return;
    }

    // Confirm that the time of the first point is exactly zero

    cc_float * const cubexp_time = refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME);

    if(cubexp_time[0] != 0.0F)
    {
        fgError(FG_INVALID_TIME,                    // fg_errno
                FG_CUBEXP,                          // fg_type
                0,                                  // error index
                cubexp_time[0],                     // error data 0
                0.0F,                               // error data 1
                0.0F,                               // error data 2
                0.0F,                               // error data 3
                fg_error);

        return;
    }

    // When arming CUBEXP for cyc sel 0 in IDLE

    if(ref_mgr->ref_state == REF_IDLE && cyc_sel == 0)
    {
        // Confirm that reference of the first point is within the limit of the actual reference

        cc_float * const cubexp_ref = refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_REF);

        cc_float const mismatch = fabsf(cubexp_ref[0] - ref_mgr->ref_fg_unlimited) / fg_limits->pos;

        if(mismatch > REF_ARM_MISMATCH_LIMIT)
        {
            fgError(FG_INIT_REF_MISMATCH,           // fg_errno
                    FG_CUBEXP,                      // fg_type
                    0,                              // error index
                    mismatch,                       // error data 0
                    REF_ARM_MISMATCH_LIMIT,         // error data 1
                    cubexp_ref[0],                  // error data 2
                    ref_mgr->ref_fg_unlimited,      // error data 3
                    fg_error);

            return;
        }
    }

   // Try to the arm the CUBEXP

    fgCubexpArm(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_REF),
                refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_RATE),
                refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME),
                num_points,
                fg_pars,
                fg_error);
}


// NON-RT   refArmTable

static void refArmTable(struct REF_mgr         * const ref_mgr,
                        uint32_t                 const sub_sel,
                        uint32_t                 const cyc_sel,
                        enum REG_mode            const reg_mode,
                        bool                     const test_arm,
                        struct FG_limits const * const fg_limits,
                        union  FG_pars         * const fg_pars,
                        struct FG_error        * const fg_error)
{
    uint32_t          const num_points     = refMgrFgParValueSubCyc  (ref_mgr, sub_sel, cyc_sel, TABLE_FUNCTION_NUM_ELS);
    struct FG_point * const table_function = refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TABLE_FUNCTION);

    // Confirm that the time of the first point is exactly zero

    if(table_function[0].time != 0.0F)
    {
        fgError(FG_INVALID_TIME,                    // fg_errno
                FG_TABLE,                           // fg_type
                0,                                  // error index
                table_function[0].time,             // error data 0
                0.0F,                               // error data 1
                0.0F,                               // error data 2
                0.0F,                               // error data 3
                fg_error);

        return;
    }

    // When arming TABLE for cyc sel 0 in IDLE

    if(ref_mgr->ref_state == REF_IDLE && cyc_sel == 0)
    {
        // Confirm that reference of the first point is within the limit of the actual reference

        cc_float const mismatch = fabsf(table_function[0].ref - ref_mgr->ref_fg_unlimited) / fg_limits->pos;

        if(mismatch > REF_ARM_MISMATCH_LIMIT)
        {
            fgError(FG_INIT_REF_MISMATCH,           // fg_errno
                    FG_TABLE,                       // fg_type
                    0,                              // error index
                    mismatch,                       // error data 0
                    REF_ARM_MISMATCH_LIMIT,         // error data 1
                    table_function[0].ref,          // error data 2
                    ref_mgr->ref_fg_unlimited,      // error data 3
                    fg_error);

            return;
        }
    }

    // Try to arm the TABLE

    // Define base_idx in this local scope to avoid warnings with the previous "goto table_error"

    uint32_t const base_idx = (cyc_sel + sub_sel * ref_mgr->num_cyc_sels) * ref_mgr->max_table_points;

    fgTableArm( fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                table_function,
               &ref_mgr->armed_table_function[base_idx],
                num_points,
                ref_mgr->reg_mgr->iter_period,
                fg_pars,
                fg_error);
}


// NON-RT   refArmLtrim

static void refArmLtrim(struct REF_mgr         * const ref_mgr,
                        uint32_t                 const sub_sel,
                        uint32_t                 const cyc_sel,
                        enum REG_mode            const reg_mode,
                        bool                     const test_arm,
                        struct FG_limits const * const fg_limits,
                        union  FG_pars         * const fg_pars,
                        struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TRIM_INITIAL_REF);

    // Try to the arm the LTRIM

    fgTrimArm( fg_limits,
               polswitchIsAutomatic(ref_mgr->polswitch_mgr),
               polswitchIsNegative(ref_mgr->polswitch_mgr),
               test_arm,
               FG_LTRIM,
              *initial_ref_pointer,
               refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_FINAL_REF),
              *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][reg_mode].linear_rate,
               refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_DURATION),
               fg_pars,
               fg_error);
}


// NON-RT   refArmCtrim

static void refArmCtrim(struct REF_mgr         * const ref_mgr,
                        uint32_t                 const sub_sel,
                        uint32_t                 const cyc_sel,
                        enum REG_mode            const reg_mode,
                        bool                     const test_arm,
                        struct FG_limits const * const fg_limits,
                        union  FG_pars         * const fg_pars,
                        struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TRIM_INITIAL_REF);

    // Try to the arm the CTRIM

    fgTrimArm( fg_limits,
               polswitchIsAutomatic(ref_mgr->polswitch_mgr),
               polswitchIsNegative(ref_mgr->polswitch_mgr),
               test_arm,
               FG_CTRIM,
              *initial_ref_pointer,
               refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_FINAL_REF),
              *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][reg_mode].linear_rate,
               refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_DURATION),
               fg_pars,
               fg_error);
}


// NON-RT   refArmSteps

static void refArmSteps(struct REF_mgr         * const ref_mgr,
                        uint32_t                 const sub_sel,
                        uint32_t                 const cyc_sel,
                        enum REG_mode            const reg_mode,
                        bool                     const test_arm,
                        struct FG_limits const * const fg_limits,
                        union  FG_pars         * const fg_pars,
                        struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TEST_INITIAL_REF);

    // Try to the arm the STEPS

    fgTestArmRT(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                FG_STEPS,
               *initial_ref_pointer,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY),
                fg_pars,
                fg_error);
}


// NON-RT   refArmSquare

static void refArmSquare(struct REF_mgr         * const ref_mgr,
                         uint32_t                 const sub_sel,
                         uint32_t                 const cyc_sel,
                         enum REG_mode            const reg_mode,
                         bool                     const test_arm,
                         struct FG_limits const * const fg_limits,
                         union  FG_pars         * const fg_pars,
                         struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TEST_INITIAL_REF);

    // Try to the arm the SQUARE

    fgTestArmRT(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                FG_SQUARE,
                *initial_ref_pointer,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY),
                fg_pars,
                fg_error);
}


// NON-RT   refArmSine

static void refArmSine(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TEST_INITIAL_REF);

    // Try to the arm the SINE

    fgTestArmRT(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                FG_SINE,
                *initial_ref_pointer,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY),
                fg_pars,
                fg_error);
}


// NON-RT   refArmCosine

static void refArmCosine(struct REF_mgr         * const ref_mgr,
                         uint32_t                 const sub_sel,
                         uint32_t                 const cyc_sel,
                         enum REG_mode            const reg_mode,
                         bool                     const test_arm,
                         struct FG_limits const * const fg_limits,
                         union  FG_pars         * const fg_pars,
                         struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TEST_INITIAL_REF);

    // Try to the arm the COSINE

    fgTestArmRT(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                FG_COSINE,
                *initial_ref_pointer,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY),
                fg_pars,
                fg_error);
}


// NON-RT   refArmOffcos

static void refArmOffcos(struct REF_mgr         * const ref_mgr,
                         uint32_t                 const sub_sel,
                         uint32_t                 const cyc_sel,
                         enum REG_mode            const reg_mode,
                         bool                     const test_arm,
                         struct FG_limits const * const fg_limits,
                         union  FG_pars         * const fg_pars,
                         struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_TEST_INITIAL_REF);

    // Try to the arm the OFFCOS (offset cosine)

    fgTestArmRT(fg_limits,
                polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                polswitchIsNegative(ref_mgr->polswitch_mgr),
                test_arm,
                FG_OFFCOS,
                *initial_ref_pointer,
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY),
                fg_pars,
                fg_error);
}


// NON-RT   refArmPrbs

static void refArmPrbs(struct REF_mgr         * const ref_mgr,
                       uint32_t                 const sub_sel,
                       uint32_t                 const cyc_sel,
                       enum REG_mode            const reg_mode,
                       bool                     const test_arm,
                       struct FG_limits const * const fg_limits,
                       union  FG_pars         * const fg_pars,
                       struct FG_error        * const fg_error)
{
    // Get pointer to initial reference

    cc_float   initial_ref;
    cc_float * initial_ref_pointer = refArmInitialRef(ref_mgr, &initial_ref, sub_sel, cyc_sel, REF_FG_PAR_PRBS_INITIAL_REF);

    // Try to the arm the PRBS

    fgPrbsArm(fg_limits,
              polswitchIsAutomatic(ref_mgr->polswitch_mgr),
              polswitchIsNegative(ref_mgr->polswitch_mgr),
              test_arm,
              ref_mgr->reg_mgr->iter_period,
             *initial_ref_pointer,
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_AMPLITUDE_PP),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_PERIOD_ITERS),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_NUM_SEQUENCES),
              refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_K),
              fg_pars,
              fg_error);
}


//! NON-RT   refArmPrepareFgParsIdx
//!
//! Set the FG parameter indexes array for a given function type.
//!
//! This function will set the fg_pars_idx array elements with the parameter indexes for the specified function type.
//! The array should be previously reset to REF_FG_PAR_NULL for all elements (this is not zero).
//!
//! @param[in]  fg_type                   Function type.
//! @param[out] fg_pars_idx               Pointer to parameter indexes array.


static void refArmPrepareFgParsIdx(enum FG_type    const fg_type,
                                   enum REF_fg_par_idx * fg_pars_idx)
{
    // Include parameter indexes for all parameters associated with the specified function type

    enum REF_fg_par_idx par_idx  = first_fg_par_idx[fg_type];
    uint32_t            num_pars = num_fg_pars[fg_type];

    while(num_pars-- > 0)
    {
        *(fg_pars_idx++) = par_idx++;
    }
}


// NON-RT   refArm

enum FG_errno refArm(struct REF_mgr         * const ref_mgr,
                     uint32_t                       sub_sel,
                     uint32_t                 const cyc_sel,
                     enum CC_enabled_disabled const test_arm,
                     uint32_t                 const num_pars,
                     cc_float         const * const par_values,
                     enum REF_fg_par_idx            fg_pars_idx[REF_FG_PAR_MAX_PER_GROUP])
{
    // Initialize array of pointers to reference parameter setting functions - IMPORTANT: this must be in the same order as enum FG_type

    static refFgParsSetParsFunc const set_pars_func[] =
    {
        NULL                 ,          // NONE
        NULL                 ,          // ZERO
        refFgParsSetParsRamp ,          // RAMP
        NULL                 ,          // PULSE
        refFgParsSetParsPlep ,          // PLEP
        NULL                 ,          // PPPL
        NULL                 ,          // CUBEXP
        NULL                 ,          // TABLE
        refFgParsSetParsTrim ,          // LTRIM
        refFgParsSetParsTrim ,          // CTRIM
        refFgParsSetParsTest ,          // STEPS
        refFgParsSetParsTest ,          // SQUARE
        refFgParsSetParsTest ,          // SINE
        refFgParsSetParsTest ,          // COSINE
        refFgParsSetParsTest ,          // OFFCOS
        refFgParsSetParsPrbs ,          // PRBS
    };

    CC_STATIC_ASSERT(CC_ARRAY_LEN(set_pars_func) == FG_NUM_FUNCS, size_of_set_pars_func_array_doesnt_correspond_to_FGC_NUM_FUNCS);

    // Initialize array of pointers to reference arming functions - IMPORTANT: this must be in the same order as enum FG_type

    static refArmFunc const arm_func[] =
    {
        refArmNone   ,                 // NONE
        refArmZero   ,                 // ZERO
        refArmRamp   ,                 // RAMP
        refArmPulse  ,                 // PULSE
        refArmPlep   ,                 // PLEP
        refArmPppl   ,                 // PPPL
        refArmCubexp ,                 // CUBEXP
        refArmTable  ,                 // TABLE
        refArmLtrim  ,                 // LTRIM
        refArmCtrim  ,                 // CTRIM
        refArmSteps  ,                 // STEPS
        refArmSquare ,                 // SQUARE
        refArmSine   ,                 // SINE
        refArmCosine ,                 // COSINE
        refArmOffcos ,                 // OFFCOS
        refArmPrbs   ,                 // PRBS
    };

    CC_STATIC_ASSERT(CC_ARRAY_LEN(arm_func) == FG_NUM_FUNCS, size_of_arm_func_array_doesnt_correspond_to_FGC_NUM_FUNCS);

    // Reset the parameter indexes array immediately in case we return with an error (note: REF_FG_PAR_NULL is not zero)

    uint32_t i;

    for(i = 0 ; i < REF_FG_PAR_MAX_PER_GROUP ; i++)
    {
        fg_pars_idx[i] = REF_FG_PAR_NULL;
    }

    // Get function type from user parameters

    enum REF_fg_par_idx const transaction_last_fg_par_idx = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRANSACTION_LAST_FG_PAR_INDEX);
    enum FG_type fg_type = refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE);

    if(transaction_last_fg_par_idx > 0 && transaction_last_fg_par_idx < REF_NUM_FG_PARS)
    {
        // Look up fg_type linked to the transaction last FG parameter index

        enum FG_type const trans_fg_type = transaction_fg_type[transaction_last_fg_par_idx];

        if(trans_fg_type != FG_NONE)
        {
            refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE) = fg_type = trans_fg_type;
        }

        // Reset transaction last FG parameter index

        refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRANSACTION_LAST_FG_PAR_INDEX) = REF_FG_PAR_NULL;
    }

    CC_ASSERT(fg_type < FG_NUM_FUNCS);

    // Check if sub_sel and cyc_sel are compatible with the function parameters

    if(sub_sel > 0 && (ref_mgr->not_sub_sel_fg_types[fg_type] == true || num_pars > 0))
    {
        return FG_INVALID_SUB_SEL;
    }

    if(cyc_sel > 0 && (ref_mgr->not_cyc_sel_fg_types[fg_type] == true || num_pars > 0))
    {
        return FG_INVALID_CYC_SEL;
    }

    // Check that ref state is compatible with this arming request

    bool const sub_cyc_0 = sub_sel == 0 && cyc_sel == 0;

    if(   (ref_mgr->ref_state != REF_IDLE && num_pars > 0)
       || (ref_mgr->ref_state == REF_ARMED && sub_cyc_0 == true && fg_type != FG_NONE)
       || ((ref_mgr->ref_state_mask & (REF_RUNNING_MASK|REF_TO_IDLE_MASK)) != 0 && sub_cyc_0 == true)
       || (   refMgrParValue(ref_mgr, MODE_REF_CYC_SEL) == CC_ENABLED
           && (ref_mgr->ref_state_mask & ~(REF_IDLE_MASK|REF_ARMED_MASK)) != 0
           && sub_cyc_0 == true))
    {
        return FG_INVALID_REF_STATE;
    }

    // Set reg_mode

    bool const    arming_in_idle = ref_mgr->ref_state == REF_IDLE && sub_sel == 0 && cyc_sel == 0;
    enum REG_mode reg_mode;

    if(arming_in_idle == true)
    {
        // Arming in IDLE to run in RUNNING : use MODE REG_MODE

        reg_mode = refMgrParValue(ref_mgr, MODE_REG_MODE);
    }
    else if(   refMgrParValue(ref_mgr, MODE_TEST_CYC_SEL)     != 0
            && refMgrParValue(ref_mgr, MODE_TEST_REF_CYC_SEL) == cyc_sel)
    {
        // Arming with TEST_CYC_SEL active for the TEST_REF_CYC_SEL to run in CYCLING : use MODE TEST_REG_MODE

        reg_mode = refMgrParValue(ref_mgr, MODE_TEST_REG_MODE);
    }
    else
    {
        // Arming to run in CYCLING : use MODE REG_MODE_CYC

        reg_mode = refMgrParValue(ref_mgr, MODE_REG_MODE_CYC);
    }

    // Check that reg_mode is a valid value

    if(reg_mode != REG_VOLTAGE && reg_mode != REG_CURRENT && reg_mode != REG_FIELD)
    {
        return FG_INVALID_REG_MODE;
    }

    // Try to set parameters if supplied (cyc_sel and sub_sel are guaranteed to be zero by a previous test)

    if(num_pars > 0)
    {
        // Initialise fg_error structure for cyc_sel=sub_sel=0

        fgError(FG_OK, fg_type, 0, 0.0F, 0.0F, 0.0F, 0.0F, ref_mgr->fg_error);

        // Check that the function type supports setting parameters

        if(set_pars_func[fg_type] == NULL)
        {
            fgError(FG_BAD_ARRAY_LEN, fg_type, 10, 0.0F, 0.0F, 0.0F, 0.0F, ref_mgr->fg_error);
        }
        else
        {
            // Try to set function parameters from par_values

            set_pars_func[fg_type](ref_mgr, num_pars, par_values, ref_mgr->fg_error);
        }

        // If error occurred

        if(ref_mgr->fg_error->fg_errno != FG_OK)
        {
            // Set function's changed parameter indexes in fg_pars_idx and return the error number

            refArmPrepareFgParsIdx(fg_type, fg_pars_idx);

            return ref_mgr->fg_error->fg_errno;
        }
    }

    // Prepare the function generation limits based on the regulation mode

    struct FG_limits fg_limits;

    fg_limits.pos     = *ref_mgr->ref_limits[reg_mode].pos;
    fg_limits.standby = *ref_mgr->ref_limits[reg_mode].standby;
    fg_limits.neg     = *ref_mgr->ref_limits[reg_mode].neg;
    fg_limits.rate    = *ref_mgr->ref_limits[reg_mode].rate;

    // Lock mutex if provided, to protect against calling arm_func() at the same time as refEvent functions

    if(ref_mgr->mutex)
    {
        // Use lock callback defined for libreg

        ref_mgr->reg_mgr->mutex_lock(ref_mgr->mutex);
    }

    // Try to arm the function

    struct FG_error  * const fg_error  = &ref_mgr->fg_error [cyc_sel + sub_sel * ref_mgr->num_cyc_sels];
    struct REF_armed * const ref_armed = &ref_mgr->ref_armed[cyc_sel + sub_sel * ref_mgr->num_cyc_sels];
    enum FG_type       const prev_armed_fg_type  = ref_armed->fg_pars.meta.type;
    cc_float           const prev_armed_duration = ref_armed->fg_pars.meta.time.duration;

    arm_func[fg_type](ref_mgr, sub_sel, cyc_sel, reg_mode, test_arm, &fg_limits, &ref_armed->fg_pars, fg_error);

    // Unlock mutex if provided

    if(ref_mgr->mutex)
    {
        // Use unlock callback defined for libreg

        ref_mgr->reg_mgr->mutex_unlock(ref_mgr->mutex);
    }

    // Process returned error number

    if(fg_error->fg_errno == FG_OK && test_arm == false)
    {
        // Arming successful - store reg mode

        ref_armed->reg_mode = reg_mode;

        // Reset the ILC - do a full reset if the function type or duration has changed. This
        // forgets the learned ILC reference, otherwise request a partial reset which just updates
        // the minimum RMS V_REF rate for a few cycles (to avoid a spurious ILC failure due to the
        // RMS V_REF rate limit, which is set to three times the minimum value.

        enum REF_ilc_reset const reset_type =    (fg_type != prev_armed_fg_type)
                                              || (prev_armed_duration != ref_armed->fg_pars.meta.time.duration)
                                            ? REF_ILC_RESET_FULL
                                            : REF_ILC_RESET_PARTIAL;

        refIlcResetRef(ref_mgr, sub_sel, cyc_sel, reset_type);

        // Prepare to Use Arm Now if required

        if(   refMgrParValue(ref_mgr, MODE_USE_ARM_NOW) == CC_ENABLED
           && refEventMgrNextIsPending(&ref_mgr->event_mgr, REF_EVENT_USE_ARM_NOW) == false
           && refEventMgrRefStateIsValidRT(REF_EVENT_USE_ARM_NOW, ref_mgr->iter_cache.ref_mode, ref_mgr->ref_state_mask) == true)
        {
            refEventPrepareNextFunc(ref_mgr, sub_sel, cyc_sel, cyc_sel, REF_EVENT_USE_ARM_NOW);
        }

        // Set function's parameter indexes in fg_pars_idx after including FG_TYPE

        *(fg_pars_idx++) = REF_FG_PAR_REF_FG_TYPE;

        refArmPrepareFgParsIdx(fg_type, fg_pars_idx);
    }

    // Return fg_error structure

    return fg_error->fg_errno;
}



void refArmDisarm(struct REF_mgr * const ref_mgr)
{
    if(ref_mgr->ref_state == REF_ARMED)
    {
        // Disarm non-cyclic reference

        fgNoneArmRT(&ref_mgr->ref_armed->fg_pars, ref_mgr->fg_error);
    }
}

// EOF
