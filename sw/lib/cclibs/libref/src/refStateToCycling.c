//! @file  refStateToCycling.c
//! @brief Converter Control Reference Manager library: TO_CYCLING reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

RefState * refStateToCyclingInitRT(struct REF_mgr * const ref_mgr)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Change regulation mode to REG_MODE_CYC when coming from OFF with POST_FUNC_OPENLOOP_MINRMS

    if(ref_mgr->ref_state == REF_OFF)
    {
        ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, refMgrParValue(ref_mgr, MODE_REG_MODE_CYC), true);     // use_average_v_ref

        // For POST_FUNC_OPENLOOP_MINRMS, force smooth entry into openloop with V_REF staying zero across the transition

        if(refMgrParValue(ref_mgr, MODE_POST_FUNC) == REF_POST_FUNC_OPENLOOP_MINRMS)
        {
            reg_mgr->openloop = true;
            reg_mgr->reg_signal->rst_vars.ref_open[reg_mgr->reg_signal->rst_vars.history_index] = 0.0F;
            ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = reg_mgr->ref_rate = 0.0F;
        }
    }

    // Set final reference according to MODE POST_FUNC

    cc_float final_ref;

    if(refMgrParValue(ref_mgr, MODE_POST_FUNC) == REF_POST_FUNC_HOLD)
    {
        // MODE POST_FUNC is HOLD so go to the actual reference (clipped to valid range)

        final_ref = refArmClipRefRT(ref_mgr, ref_mgr->ref_fg_unlimited);
    }
    else
    {
        // MODE POST_FUNC is MINRMS or OPENLOOP_MINRMS so go to +/- MINRMS

        cc_float const min_rms = *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode].min_rms;

        final_ref = polswitchIsNegative(ref_mgr->polswitch_mgr) == true
                  ? -min_rms
                  :  min_rms;
    }

    // For current or field regulation, set the initial rate from previous function's meta data if available

    cc_float const initial_rate = ref_mgr->iter_cache.reg_mode == REG_VOLTAGE
                                ? 0.0F
                                :   (ref_mgr->ref_state == REF_CYCLING && ref_mgr->fg.status == FG_POST_FUNC)
                                  ? ref_mgr->fsm.cycling.final_rate
                                  : reg_mgr->ref_rate;

    // Clear active events

    refEventMgrClearActiveRT(&ref_mgr->event_mgr);

    // Initialize ramp to final reference

    ccprintf("\n  initial_rate=%g  initial_ref=%g  final_ref=%g\n"
            , initial_rate
            , ref_mgr->ref_fg_unlimited
            , final_ref
            );

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     initial_rate,
                     ref_mgr->ref_fg_unlimited,
                     final_ref,
                     REF_USE_ACC_FOR_DEC);

    ref_mgr->fsm.cycling.status = FG_POST_FUNC;

    // Reset flags and to TO_OFF time, in case we're coming from TO_OFF

    ref_mgr->event_mgr.abort           = false;
    ref_mgr->fsm.cycling.cycle_overrun = false;

    ref_mgr->to_off_time = cc_zero_us;

    // If previous state was anything except TO_OFF, then reset TO_OFF to TO_CYCLING transition counter

    if(ref_mgr->ref_state != REF_TO_OFF)
    {
        ref_mgr->fsm.to_off.to_tc_transition_counter = 0;
    }

    return refStateToCyclingRT;
}



enum REF_state refStateToCyclingRT(struct REF_mgr * const ref_mgr)
{
    // Run the ramp manager to execute post-function ramp, if required

    refRampMgrRT(ref_mgr);

    // Set force_openloop for a zero plateau if the post-function mode is OPENLOOP_MINRMS

    ref_mgr->force_openloop =    refMgrParValue(ref_mgr, MODE_POST_FUNC) == REF_POST_FUNC_OPENLOOP_MINRMS
                              && ref_mgr->fg.status == FG_POST_FUNC
                              && ref_mgr->ref_fg_unlimited == 0.0F;

    // Check if FULL_ECO request is active

    ref_mgr->fsm.full_eco.run = refStateFullEcoRunRT(ref_mgr);

    return REF_TO_CYCLING;
}

// EOF
