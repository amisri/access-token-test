//! @file  refStateFullEco.c
//! @brief Converter Control Reference Manager library: FULL_ECO reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"


bool refStateFullEcoRunRT(struct REF_mgr const * const ref_mgr)
{
    return    refMgrParValue(ref_mgr, MODE_ECONOMY) == CC_ENABLED
           && (refMgrParValue(ref_mgr, ECONOMY_FULL) == CC_ENABLED || ref_mgr->fsm.full_eco.once);
}



RefState * refStateFullEcoInitRT(struct REF_mgr * const ref_mgr)
{
    // Reset FULL_ECO run flag

    ref_mgr->fsm.full_eco.run = false;

    // Reset force_openloop flag

    ref_mgr->force_openloop = false;

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Set FULL_ECO level

    cc_float final_ref;

    if(refMgrParValue(ref_mgr, MODE_POST_FUNC) == REF_POST_FUNC_HOLD)
    {
        // MODE POST_FUNC is HOLD so keep the current reference

        final_ref = ref_mgr->ref_fg_unlimited;
    }
    else
    {
        // Otherwise, use MIN_RMS

        cc_float const min_rms = *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode].min_rms;

        final_ref = polswitchIsNegative(ref_mgr->polswitch_mgr) == true
                  ? -min_rms
                  :  min_rms;
    }

    // Arm RAMP to the FULL_ECO level

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     refMgrInitialRate(ref_mgr),
                     ref_mgr->ref_fg_unlimited,
                     final_ref,
                     REF_USE_ACC_FOR_DEC);

    return refStateFullEcoRT;
}



enum REF_state refStateFullEcoRT(struct REF_mgr * const ref_mgr)
{
    // Keep processing cycling reference events, but never clear the active event nor
    // allow for updating it with 'use armed now' events

    enum REF_active_status event_status;

    event_status = refEventMgrProcessCyclingEventsRT(&ref_mgr->event_mgr,
                                                     ref_mgr->iter_time,
                                                     false,                // clear_active
                                                     false);               // allow_use_arm_now

    // Use ramp manager to run the ramp to the standby level

    refRampMgrRT(ref_mgr);

    // If the event is updated but we have stayed in FULL_ECO state, then update the cycle status

    if(event_status == REF_ACTIVE_UPDATED)
    {
        refCycStatusMgrActivateRT(&ref_mgr->cyc_status_mgr);
        refCycStatusMgrFinishRT(ref_mgr, false);
    }

    return REF_FULL_ECO;
}

// EOF
