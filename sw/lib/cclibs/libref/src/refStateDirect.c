//! @file  refStateDirect.c
//!
//! @brief Converter Control Reference Manager library: DIRECT reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"


//! Clip reference within limits
//!
//! This function takes into account the state of the optional polarity switch if present.
//!
//! @param[in]  ref_mgr          Pointer to REF_mgr structure
//! @param[in]  ref              Reference value to clip according to the reference limits
//!
//! @returns reference after clipping

static cc_float refDirectClipRefRT(struct REF_mgr const * const ref_mgr, cc_float ref)
{
    struct REG_lim_ref const * const lim_ref = ref_mgr->reg_mgr->lim_ref;

    // Zero is always allowed

    if(ref != 0.0F)
    {
        if(ref_mgr->rt_ref.direct == true)
        {
            // External direct real time reference is in use so reset function generator reference

            ref = 0.0F;
        }
        else if(lim_ref->flags.unipolar)
        {
            // Converter is unipolar

            if(polswitchVarValue(ref_mgr->polswitch_mgr, TIMEOUT) != 0)
            {
                // Converter has a polarity switch

                cc_float ref_abs = fabsf(ref);

                if(ref_abs > lim_ref->pos)
                {
                    ref_abs = lim_ref->pos;
                }
                else if(ref_abs < lim_ref->standby)
                {
                    ref_abs = lim_ref->standby;
                }

                ref = (ref < 0.0F ? -ref_abs : ref_abs);
            }
            else
            {
                // else the converter doesn't have a polarity switch or it is not in bipolar mode, so clip to [STANDBY..POS].
                // Additionally, if the final reference is below zero then clip to zero, which is allowed.

                if(ref > lim_ref->pos)
                {
                    ref = lim_ref->pos;
                }
                else if(ref < 0.0F)
                {
                    ref = 0.0F;
                }
                else if(ref < lim_ref->standby)
                {
                    ref = lim_ref->standby;
                }
            }
        }
        else
        {
            // Converter is bipolar, so clip to [NEG..POS]

            if(ref > lim_ref->pos)
            {
                ref = lim_ref->pos;
            }
            else if(ref < lim_ref->neg)
            {
                ref = lim_ref->neg;
            }
        }
    }

    return ref;
}



//! Drive pre-function in DIRECT
//!
//! This arms UPMINMAX and DOWNMAXMIN pre-functions ramps in DIRECT
//!
//! @param[in]  ref_mgr             Pointer to REF_mgr structure
//! @param[in]  clipped_final_ref   Final reference value after clipping to limits
//!
//! @returns    start_time

static cc_float refDirectPreFuncRT(struct REF_mgr * const ref_mgr, cc_float const clipped_final_ref)
{
    struct REF_state_direct * const direct = &ref_mgr->fsm.direct;
    cc_float start_time = 0;

    // Enter on pre-func ramp and regular ramp boundaries

    if(ref_mgr->ramp_mgr.status == FG_POST_FUNC)
    {
        // If we need to start a new ramp (possibly with pre-func)

        if(direct->pre_func_ramp_index == 0 && clipped_final_ref != direct->prev_clipped_final_ref)
        {
            struct REF_default_ptrs const * const defaults = &ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode];

            // Check if pre-func is needed

            if(direct->pre_func_type == REF_PRE_FUNC_UPMINMAX && clipped_final_ref > direct->prev_clipped_final_ref)
            {
                // Set direct ref for the first pre-func ramp to clipped MIN value
                // and pre-set direct ref for the next ramp in pre-func sequence to clipped MAX

                direct->direct_ref   = refDirectClipRefRT(ref_mgr, *defaults->pre_func_min);
                direct->pre_func_ref = refDirectClipRefRT(ref_mgr, *defaults->pre_func_max);

                direct->pre_func_ramp_index = 1;
            }
            else if(direct->pre_func_type == REF_PRE_FUNC_DOWNMAXMIN && clipped_final_ref < direct->prev_clipped_final_ref)
            {
                // Set direct ref for the first pre-func ramp to clipped MAX value
                // and pre-set direct ref for the next ramp in pre-func sequence to clipped MIN

                direct->direct_ref   = refDirectClipRefRT(ref_mgr, *defaults->pre_func_max);
                direct->pre_func_ref = refDirectClipRefRT(ref_mgr, *defaults->pre_func_min);

                direct->pre_func_ramp_index = 1;
            }
            else
            {
                // No pre-func needed - go directly to clipped final ref

                direct->direct_ref = clipped_final_ref;

                direct->pre_func_ramp_index = 0;
            }

            direct->prev_clipped_final_ref = clipped_final_ref;
        }
        else if(direct->pre_func_ramp_index == 1)
        {
            start_time = (refMgrParPointer(ref_mgr, DEFAULT_PLATEAU_DURATION))[REF_MINMAX_PLATEAU_INDEX];

            // Go to pre-set clipped MIN or clipped MAX

            direct->direct_ref = direct->pre_func_ref;

            direct->pre_func_ramp_index = 2;
        }
        else
        {
            // Pre-func has ended - ramp to clipped final ref

            start_time = (refMgrParPointer(ref_mgr, DEFAULT_PLATEAU_DURATION))[REF_MINMAX_PLATEAU_INDEX];

            direct->direct_ref             = clipped_final_ref;
            direct->prev_clipped_final_ref = clipped_final_ref;

            direct->pre_func_ramp_index = 0;
        }
    }

    return start_time;
}



//! Arm a ramp in DIRECT state
//!
//! @param[in]  ref_mgr             Pointer to REF_mgr structure
//! @param[in]  start_time          Ramp start time

static void refDirectArmRampRT(struct REF_mgr * const ref_mgr, cc_float const start_time)
{
    struct REF_state_direct * const direct  = &ref_mgr->fsm.direct;
    struct REG_mgr          * const reg_mgr = ref_mgr->reg_mgr;

    direct->prev_direct_ref = direct->direct_ref;

    ccprintf("\n  start_time=%g  ref_mgr->ref_fg=%g  direct->direct_ref=%g  direct->pre_func_ramp_index=%u\n"
            , start_time
            , ref_mgr->ref_fg
            , direct->direct_ref
            , direct->pre_func_ramp_index
            );

    // Initialize the new ramp

    refRampMgrInitRT(ref_mgr,
                     start_time,
                     reg_mgr->ref_rate,
                     ref_mgr->ref_fg,
                     direct->direct_ref,
                     direct->pre_func_ramp_index > 0);  // use_acc_for_dec except for ramps to user setting

    // Degaussing is allowed only in current or field regulation mode and only for bipolar (4Q) converters.
    // It's triggered after ramping to zero, but not when a pre-function ramp is being played

    if(   refMgrParValue(ref_mgr, DIRECT_DEGAUSS_AMP_PP) > 0.0F
       && refMgrParValue(ref_mgr, DIRECT_DEGAUSS_PERIOD) > 0.0F
       && direct->pre_func_ramp_index                      == 0
       && direct->direct_ref                               == 0.0F
       && reg_mgr->lim_ref->flags.unipolar                 == false
       && (   ref_mgr->iter_cache.reg_mode == REG_CURRENT
           || ref_mgr->iter_cache.reg_mode == REG_FIELD))
    {
        direct->degauss = FG_PRE_FUNC;
    }
    else
    {
        direct->degauss = FG_POST_FUNC;
    }
}



//! Arm a degauss sine wave
//!
//! @param[in]  ref_mgr             Pointer to REF_mgr structure

static void refDirectArmDegaussRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_direct * const direct = &ref_mgr->fsm.direct;

    // In field regulation mode, scale the value of DEGAUSS_AMP_PP using GAUSS_PER_AMP parameter

    cc_float amplitude_pp = refMgrParValue(ref_mgr, DIRECT_DEGAUSS_AMP_PP);

    if(ref_mgr->iter_cache.reg_mode == REG_FIELD)
    {
        amplitude_pp *= regMgrParValue(ref_mgr->reg_mgr, LOAD_GAUSS_PER_AMP);
    }

    // Initialize a SINE function with exponential decay and half-period cosine window

    fgTestArmRT(NULL,                                                 // limits
                false,                                                // polswitch_auto
                false,                                                // invert_limits
                false,                                                // test_arm
                FG_SINE,
                0.0F,                                                 // initial_ref
                amplitude_pp,
                10,                                                   // Number of periods
                0,                                                    // Sampling period in iters
                refMgrParValue(ref_mgr, DIRECT_DEGAUSS_PERIOD),
                CC_ENABLED,                                           // Enable half-period cosine window
                CC_ENABLED,                                           // Enable exponential decay
               &ref_mgr->fsm.fg_pars,
                NULL);

    // Set event time to now and the degaussing status

    refFuncMgrUpdateEventTimeRT(&ref_mgr->fg, ref_mgr->iter_time);

    direct->degauss    = FG_DURING_FUNC;
    ref_mgr->fg.status = FG_DURING_FUNC;
}



void refStateDirectSetRT(struct REF_mgr * const ref_mgr)
{
    // Set REF DIRECT_STATE to IDLE or RUNNING

    if(ref_mgr->ref_state == REF_DIRECT && ref_mgr->ramp_mgr.status == FG_DURING_FUNC)
    {
        ref_mgr->fsm.direct.state = REF_DIRECT_RUNNING;
    }
    else
    {
        ref_mgr->fsm.direct.state = REF_DIRECT_IDLE;
    }
}



RefState * refStateDirectInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_direct * const direct = &ref_mgr->fsm.direct;

    if(   ref_mgr->ramp_mgr.status == FG_POST_FUNC
       || ref_mgr->ref_state != REF_POL_SWITCHING
       || ref_mgr->iter_cache.mode_reg_mode != ref_mgr->iter_cache.reg_mode)
    {
        direct->degauss                = FG_POST_FUNC;
        direct->pre_func_ramp_index    = 0;
        direct->prev_clipped_final_ref = 1.0E30F;
        direct->prev_direct_ref        = 1.0E30F;
        direct->pre_func_type          = refMgrParValue(ref_mgr, MODE_PRE_FUNC);

        // Allow real time control with a direct external reference

        refRtControlRealTimeRefRT(ref_mgr, REF_DIRECT);
    }

    // Reset prev_mode_reg_mode

    direct->prev_mode_reg_mode = REG_NONE;

    // Reset force_openloop flag

    ref_mgr->force_openloop = false;

    return refStateDirectRT;
}



enum REF_state refStateDirectRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_direct * const direct = &ref_mgr->fsm.direct;

    if(direct->degauss == FG_DURING_FUNC)
    {
        // Degauss sine wave was started

        if(ref_mgr->fg.status != FG_POST_FUNC)
        {
            // Degauss sine wave is still running so return immediately

            return REF_DIRECT;
        }

        // Degauss has finished so disarm and reset degauss status

        direct->degauss = FG_POST_FUNC;

        fgNoneArmRT(ref_mgr->fg.pars, NULL);
    }

    // Prepare to manage the direct state reference behavior

    bool const ramp_or_pre_func_running = (ref_mgr->ramp_mgr.status == FG_DURING_FUNC || direct->pre_func_ramp_index != 0);

    if(   ramp_or_pre_func_running == false
       && ref_mgr->iter_cache.mode_reg_mode != direct->prev_mode_reg_mode
       && ref_mgr->iter_cache.mode_reg_mode != ref_mgr->iter_cache.reg_mode)
    {
        // Remember mode_reg_mode to avoid try to set it every iteration, in case it's invalid

        direct->prev_mode_reg_mode = ref_mgr->iter_cache.mode_reg_mode;

        // Requested reg_mode is not the actual reg_mode and no ramp is running so set new reg_mode

        ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, ref_mgr->iter_cache.mode_reg_mode, false);

        // Allow real time control with a direct external reference

        refRtControlRealTimeRefRT(ref_mgr, REF_DIRECT);

        // Set previous clipped and direct ref to the actual reference to trigger a new ramp

        direct->prev_clipped_final_ref = ref_mgr->ref_fg;
        direct->prev_direct_ref        = ref_mgr->ref_fg;
    }

    // If real-time Reference is in use, no ref management required so return early

    if(ref_mgr->rt_ref.in_use == true)
    {
        return REF_DIRECT;
    }

    // Only allow the pre-function type to be changed when no ramp or pre-function is playing

    if(ramp_or_pre_func_running == false)
    {
        direct->pre_func_type = refMgrParValue(ref_mgr, MODE_PRE_FUNC);
    }

    // Get final reference for actual reg mode and clip it

    cc_float const final_ref         = *(ref_mgr->ref_direct[ref_mgr->iter_cache.reg_mode]);
    cc_float const clipped_final_ref = refDirectClipRefRT(ref_mgr, final_ref);
    cc_float       start_time        =  0.0F;

    if(direct->pre_func_type == REF_PRE_FUNC_UPMINMAX || direct->pre_func_type == REF_PRE_FUNC_DOWNMAXMIN)
    {
        // Drive pre-function segments starting when a ramp finishes

        if(ref_mgr->ramp_mgr.status == FG_POST_FUNC)
        {
            start_time = refDirectPreFuncRT(ref_mgr, clipped_final_ref);
        }
    }
    else if(clipped_final_ref != direct->prev_clipped_final_ref)
    {
        direct->direct_ref             = clipped_final_ref;
        direct->prev_clipped_final_ref = clipped_final_ref;
    }

    // If final reference for the direct ramp has changed then initialize the ramp

    if(direct->direct_ref != direct->prev_direct_ref)
    {
        refDirectArmRampRT(ref_mgr, start_time);
    }

    // Run the ramp manager real-time function

    refRampMgrRT(ref_mgr);

    // If the ramp manager has finished and we should start a degauss function

    if(ref_mgr->ramp_mgr.status == FG_POST_FUNC && direct->degauss == FG_PRE_FUNC)
    {
        refDirectArmDegaussRT(ref_mgr);
    }

    return REF_DIRECT;
}

// EOF
