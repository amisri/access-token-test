//! @file  refIlc.c
//! @brief Converter Control Reference Manager library - Iterative Learning Controller functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

// Constants

#define REF_ILC_ERR_MASK            (REF_ILC_ERR_LEN-1)                 //!< ILC error circular buffer index mask
#define REF_ILC_UNFLTR_REF_MASK     (REF_ILC_UNFLTR_REF_LEN-1)          //!< ILC unfiltered reference circular buffer index mask

#define REF_ILC_AVAILABLE_MASK      (REF_ILC_REF_ARM_BIT_MASK | REF_ILC_REF_RUN_BIT_MASK)

// REF_ILC_ERR_LEN must be a power of 2 and be greater than REF_ILC_MAX_L_FUNC_LEN

CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REF_ILC_ERR_LEN), error_REF_ILC_ERR_LEN_is_not_a_power_of_two);
CC_STATIC_ASSERT((REF_ILC_L_FUNC_LEN<=REF_ILC_ERR_LEN), error_REF_ILC_ERR_LEN_is_less_than_REF_ILC_L_FUNC_LEN);

// REF_ILC_UNFLTR_REF_LEN must be a power of 2 and be greater than REF_ILC_MAX_Q_FUNC_LEN

CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REF_ILC_UNFLTR_REF_LEN), error_REF_ILC_UNFLTR_REF_LEN_is_not_a_power_of_two);
CC_STATIC_ASSERT((REF_ILC_Q_FUNC_LEN<=REF_ILC_UNFLTR_REF_LEN), error_REF_ILC_UNFLTR_REF_LEN_is_less_than_REF_ILC_Q_FUNC_LEN);


// NON-RT   refIlcInit

void refIlcInit(struct REF_mgr          * const ref_mgr,
                struct REF_ilc_cyc_data * const cyc_data_store,
                uint32_t                  const max_samples,
                uint32_t                  const max_cyc_sel)
{
    // Save parameters

    ref_mgr->ilc.max_samples    = max_samples;
    ref_mgr->ilc.max_cyc_sel    = max_cyc_sel;
    ref_mgr->ilc.cyc_data_store = cyc_data_store;

    // If cycle data store is provided (which can be in slow memory) - link array of cyc_data structures to the ILC ref buffers that follow

    if(cyc_data_store != NULL)
    {
        // Then link cycle store ref buffer pointers to the remaining space in cyc_data_store

        cc_float * buf_ptr = (float *)(cyc_data_store + max_cyc_sel);

        uint32_t i;

        for(i = 0 ; i < max_cyc_sel ; i++)
        {
            cyc_data_store[i].ref_buf = buf_ptr;

            buf_ptr += max_samples;
        }
    }
}


// NON-RT   refIlcReset

void refIlcReset(struct REF_mgr  * const ref_mgr)
{
    // A reset request is not required in STANDBY because it is entry into STANDBY
    // that triggers the resetting of the ILC cycle data store

    if(ref_mgr->ilc.state != REF_ILC_STANDBY)
    {
        ref_mgr->ilc.reset = true;
    }
}


// NON-RT   refIlcEvent

void refIlcEvent(struct REF_mgr  * const ref_mgr,
                 uint32_t          const sub_sel,
                 uint32_t          const cyc_sel,
                 struct CC_ns_time const event_time,
                 cc_float          const ref_range)
{
    // If ILC is not DISABLED and sub_sel is zero then register the event

    if(   ref_mgr->ilc.state != REF_ILC_DISABLED
       && ref_mgr->ilc.event.cyc_sel == 0
       && sub_sel == 0)
    {
        // Record event time and set cyc_sel last as this is the signal that the event data is complete

        ref_mgr->ilc.event.time = event_time;
        ref_mgr->ilc.ref_range  = ref_range;

        CC_MEMORY_BARRIER;

        ref_mgr->ilc.event.cyc_sel = cyc_sel;

        ccprintf("\n   cyc_sel=%u  ref_range=%g\n",cyc_sel,ref_range);
    }
}


// NON-RT   refIlcControlInit

void refIlcControlInit(struct REF_ilc_control * const control,
                       struct CC_ns_time        const start_time,
                       uint32_t                 const rt_status_bit_mask)
{
    control->prev_start_flag    = 0x2;
    control->start_time         = start_time;
    control->index              = 0;
    control->rt_status_bit_mask = rt_status_bit_mask;
}


// NON-RT   refIlcResetData

void refIlcResetData(struct REF_mgr *   const ref_mgr,
                     uint32_t           const cyc_sel,
                     enum REF_ilc_reset const reset_type)
{
    // Return immediately if no reset action requested

    if(reset_type == REF_ILC_RESET_NONE)
    {
        return;
    }

    // If cyc_sel is zero then reset buffers for all cycle selectors.
    // Note: cyc_data_store array indexes are 0 to (max_cyc_sel-1) for cyc_sels 1 to max_cyc_sel.

    uint32_t const from = cyc_sel == 0
                        ? 0
                        : cyc_sel - 1;

    uint32_t const to   = cyc_sel == 0
                        ? ref_mgr->ilc.max_cyc_sel - 1
                        : from;
    uint32_t i;

    for(i = from ; i <= to ; i++)
    {
        if(reset_type == REF_ILC_RESET_FULL)
        {
            // Full reset: erase the learned ILC reference

            ref_mgr->ilc.cyc_data_store[i].ref_num_els        = 0;
            ref_mgr->ilc.cyc_data_store[i].min_rms_v_ref_rate = 0.0F;
            ref_mgr->ilc.cyc_data_store[i].initial_rms_err    = 0.0F;

            memset(ref_mgr->ilc.cyc_data_store[i].ref_buf, 0, ref_mgr->ilc.max_samples * sizeof(cc_float));
        }

        ref_mgr->ilc.cyc_data_store[i].downshifter = 1 << 3;
    }
}


// NON-RT   refIlcResetRef

void refIlcResetRef(struct REF_mgr   * const ref_mgr,
                    uint32_t           const sub_sel,
                    uint32_t           const cyc_sel,
                    enum REF_ilc_reset const reset_type)
{
    // For sub_sel zero and non-zero cyc_sel only

    if(sub_sel == 0 && cyc_sel > 0)
    {
        // If ILC is not running this cyc_sel then reset the stored data immediately,
        // otherwise, set a flag to tell refIlcStateToCycling() to do the reset once the cycle ends.

        if(ref_mgr->ilc.cyc_sel == 0 || ref_mgr->ilc.cyc_sel != cyc_sel)
        {
            refIlcResetData(ref_mgr, cyc_sel, reset_type);
        }
        else
        {
            // Remember the highest level of delayed reset (FULL > PARTIAL)

            if(reset_type > ref_mgr->ilc.delayed_reset_type)
            {
                ref_mgr->ilc.delayed_reset_type = reset_type;
            }
        }
    }
}


// NON-RT  refIlcSetStatus

void refIlcSetStatus(struct REF_mgr * const ref_mgr)
{
    // Set ILC_CYC_STATES bit if REF STATE is in a cycling state and reg_mode is CURRENT or FIELD

    if(   (ref_mgr->ref_state_mask & REF_ILC_CYCLING_STATES) != 0
       && ref_mgr->iter_cache.reg_mode != REG_VOLTAGE)
    {
        ref_mgr->ilc.status |= REF_ILC_CYC_STATES_BIT_MASK;

        // Set ILC_FUNC_LEN_EVEN bit if ILC_L_ORDER is zero and ILC_L_FUNC_NUM_ELS is even

        int32_t  const ilc_l_order        =  refMgrParValue(ref_mgr, ILC_L_ORDER);
        uint32_t const ilc_l_function_len = *refMgrParValue(ref_mgr, ILC_L_FUNC_NUM_ELS);

        if(ilc_l_order == 0 && (ilc_l_function_len & 1) == 0)
        {
            ref_mgr->ilc.status |= REF_ILC_L_FUNC_LEN_EVEN_BIT_MASK;
        }
        else
        {
            ref_mgr->ilc.status &= ~REF_ILC_L_FUNC_LEN_EVEN_BIT_MASK;
        }

        // Set REF_ILC_REG_PERIOD_INVALID bit if regulator period does not divide into 3s

        if((3000000000 % ref_mgr->reg_mgr->reg_period_ns) != 0)
        {
            ref_mgr->ilc.status |= REF_ILC_REG_PERIOD_INVALID_BIT_MASK;
        }
        else
        {
            ref_mgr->ilc.status &= ~REF_ILC_REG_PERIOD_INVALID_BIT_MASK;
        }
    }
    else
    {
        ref_mgr->ilc.status &= ~(REF_ILC_CYC_STATES_BIT_MASK | REF_ILC_L_FUNC_LEN_EVEN_BIT_MASK | REF_ILC_REG_PERIOD_INVALID_BIT_MASK);
    }
}


// NON-RT   refIlcSetLfunc

int32_t refIlcSetLfunc(struct REF_mgr * const ref_mgr,
                       uint32_t         const cyc_sel)
{
    int32_t l_order        =  refMgrParValue(ref_mgr, ILC_L_ORDER);
    int32_t l_func_num_els = *refMgrParValue(ref_mgr, ILC_L_FUNC_NUM_ELS);

    // If L order is zero then use the function, otherwise generate a unit function

    if(l_order == 0)
    {
        // L order is zero so copy the function to the active L function buffer

        memcpy(ref_mgr->ilc.l.function, refMgrParPointer(ref_mgr, ILC_L_FUNC), l_func_num_els * sizeof(cc_float));

        // L function is symmetric with an odd number of elements so set the order by simple integer division

        l_order = l_func_num_els / 2;
    }
    else
    {
        // L order is not zero so create a unit function

        ref_mgr->ilc.l.function[0] = 1.0F;

        l_func_num_els = 1;
    }

    // Save the function length and order and return the order

    ref_mgr->ilc.l.num_els = l_func_num_els;
    ref_mgr->ilc.l.order   = l_order;

    return l_order;
}


// NON-RT   refIlcSetQfunc

int32_t refIlcSetQfunc(struct REF_mgr * const ref_mgr,
                       uint32_t         const cyc_sel)
{
    int32_t   q_order        = 0;
    int32_t   q_func_num_els = *refMgrParValue(ref_mgr, ILC_Q_FUNC_NUM_ELS);
    cc_float * q_func        = refMgrParPointer(ref_mgr, ILC_Q_FUNC);

    memset(ref_mgr->ilc.q.function, 0, REF_ILC_Q_FUNC_LEN * sizeof(cc_float));

    if(q_func_num_els == 1)
    {
        // Q_FUNC has only 1 element so create first order FIR filter using this value

        ref_mgr->ilc.q.function[0] = 0.5F * (1.0F - q_func[0]);
        ref_mgr->ilc.q.function[1] = q_func[0];
        ref_mgr->ilc.q.function[2] = ref_mgr->ilc.q.function[0];

        q_func_num_els = 3;
        q_order        = 1;
    }
    else if(q_func_num_els > 1)
    {
        // Q_FUNC hs more than 1 element so reflect the coefficients to create the FIR filter

        uint32_t left_index  = q_func_num_els - 1;
        uint32_t right_index = left_index;

        uint32_t i;

        // Copy function and use reflection to

        for(i = 0 ; i < q_func_num_els ; i++)
        {
            ref_mgr->ilc.q.function[left_index-- ] = q_func[i];
            ref_mgr->ilc.q.function[right_index++] = q_func[i];
        }

        // Set the Q order and number of elements

        q_order         = q_func_num_els - 1;
        q_func_num_els += q_order;
    }

    // Save the order and function length and return the order

    ref_mgr->ilc.q.order = q_order;
    ref_mgr->ilc.q.num_els = q_func_num_els;

    return q_order;
}


// NON-RT   refIlcParInterpolation

//! Parabolic interpolation
//!
//! This function will interpolate between equally spaced points using parabolic functions.
//! It uses the supplied 4-element circular buffer to keep the history of the last four values of the ILC reference (r0, r1, r2, r3).
//! The interpolation is always between the middle two points (r1 and r2) and the interpolation factor is supplied
//! in f (0 .. 1):
//!
//!     If f is 0, the function will return r1
//!     If f is 1, the function will return r2
//!
//! The function returns a mix of two parabolic interpolations:
//!
//!     1. v0 = interpolation of the parabola through [r0,r1,r2] from r1 to r2.
//!     2. v1 = interpolation of the parabola through [r1,r2,r3] from r1 to r2.
//!
//! When f = 0, the function return v0 only and when f = 1, it will return v1 only. Between the extremes it will
//! return a linear mix of the two:
//!
//!     v0 * (1.0 - f) + v1 * f;
//!
//! @param[in]     r3                Next ILC reference value.
//! @param[in]     f                 Interpolation factor (0 .. 1).
//! @param[in]     buf_index         Free running circular buffer index (masked to count 0, 1, 2, 3, 0, 1, ...)
//! @param[in]     ref_buf           Circular reference buffer for the last four values of the ILC reference
//!
//! @retval        interpolated reference value

static cc_float refIlcParInterpolation(cc_float const r3, cc_float const f, uint32_t * const buf_index, cc_float ref_buf[4])
{
    uint32_t index = ++(*buf_index);

    ref_buf[index & 3] = r3;

    cc_float const r2 = ref_buf[--index & 3];
    cc_float const r1 = ref_buf[--index & 3];
    cc_float const r0 = ref_buf[--index & 3];

    cc_float const fp1 = f + 1.0F;  // f+1 = f + 1
    cc_float const fm1 = f - 1.0F;  // f-1 = f - 1
    cc_float const g   = fm1;       // g   = f - 1
    cc_float const gp1 = f;         // g+1 = f
    cc_float const gm1 = f - 2.0F;  // g-1 = f - 2

    cc_float const v0 = 0.5F * f * (r0 * fm1 + r2 * fp1) - r1 * fp1 * fm1;
    cc_float const v1 = 0.5F * g * (r1 * gm1 + r3 * gp1) - r2 * gp1 * gm1;

    return v0 * (1.0 - f) + v1 * f;
}


// NON-RT   refIlcResampleRef

//! Resample the ILC reference array when phase has changed
//!
//! This function does an in-situ shift by the phase change, which can be up to +/- 1 regulation period.
//! It uses parabolic interpolation, except for the first and last segments, where linear interpolation
//! is used.
//!
//! @param[in]     reg_period_ns     Regulation period in nanoseconds.
//! @param[in]     phase_change_ns   Phase change in nanoseconds.
//! @param[in]     ref_num_els       Length of the data in the ref_buf.
//! @param[in,out] ref_buf_p         Pointer to ILC reference buffer.

static void refIlcResampleRef(uint32_t   const reg_period_ns,
                              int32_t    const phase_change_ns,
                              uint32_t   const ref_num_els,
                              cc_float * const ref_buf_p)
{
    cc_float const start_factor  = fabsf((cc_float)phase_change_ns) / (cc_float)reg_period_ns;
    cc_float const end_factor    = 1.0F - start_factor;
    int32_t  const last_index    = ref_num_els - 1;
    int32_t  const last_index_m1 = ref_num_els - 2;

    cc_float ref_buf[4];
    uint32_t buf_idx = 0;
    uint32_t index;

    if(phase_change_ns > 0)
    {
        // Positive phase change : interpolate backwards through the ILC reference

        // Use linear interpolation between the last two points

        ref_buf_p[last_index] = start_factor * ref_buf_p[last_index_m1] + end_factor * ref_buf_p[last_index];

        // Prepare the 4-element circular ILC reference buffer with the last three points

        index = ref_num_els;

        ref_buf[  buf_idx] = ref_buf_p[--index];
        ref_buf[++buf_idx] = ref_buf_p[--index];
        ref_buf[++buf_idx] = ref_buf_p[--index];

        // Loop backwards through the ILC reference using parabolic interpolation

        for(index = last_index_m1 ; index > 1 ; index--)
        {
            ref_buf_p[index] = refIlcParInterpolation(ref_buf_p[index-2], start_factor, &buf_idx, ref_buf);
        }

        // Use linear interpolation between the first two points

        ref_buf_p[1] = start_factor * ref_buf_p[0] + end_factor * ref_buf_p[1];

        // Treat the first point as if there is an imaginary preceding point that is always zero

        ref_buf_p[0] *= end_factor;
    }
    else
    {
        // Negative phase change : interpolate forwards through the ILC reference - don't change last point

        // Use linear interpolation between the first two points

        ref_buf_p[0] = start_factor * ref_buf_p[1] + end_factor * ref_buf_p[0];

        // Prepare the 4-element circular ILC reference buffer with the first three points

        ref_buf[  buf_idx] = ref_buf_p[0];
        ref_buf[++buf_idx] = ref_buf_p[1];
        ref_buf[++buf_idx] = ref_buf_p[2];

        // Loop forwards through the ILC reference using parabolic interpolation

        for(index = 1 ; index < last_index_m1 ; index++)
        {
            ref_buf_p[index] = refIlcParInterpolation(ref_buf_p[index+2], start_factor, &buf_idx, ref_buf);
        }

        // Use linear interpolation between the last two points and leave the last point unchanged
        // since it is held constant anyway, when the calculation window closes

        ref_buf_p[last_index_m1] = start_factor * ref_buf_p[last_index] + end_factor * ref_buf_p[last_index_m1];
    }
}


// NON-RT   refIlcGetRef

uint32_t refIlcGetRef(struct REF_mgr * const ref_mgr,
                      uint32_t         const cyc_sel,
                      uint32_t         const reg_period_ns)
{
    cc_float * const ref_buf_p           =  refIlcCycDataRefBuf(&ref_mgr->ilc, cyc_sel);
    uint32_t   const ref_num_els         = *refIlcCycDataRefNumEls(&ref_mgr->ilc, cyc_sel);
    int32_t    const prev_event_phase_ns = *refIlcCycDataEventPhase(&ref_mgr->ilc, cyc_sel);
    uint32_t         rt_status           =  REF_ILC_CYCLING_BIT_MASK;
    int32_t          phase_change_ns     =  0;

    // Link to the active ILC ref buffer in the cycle data store

    ref_mgr->ilc.ref.buf = ref_buf_p;

    // Calculate the event phase change since the last call to this function with this cyc_sel.

    phase_change_ns = ref_mgr->ilc.event_phase_ns - prev_event_phase_ns;

    // Save the new event_phase_ns in the cycle data store

    *refIlcCycDataEventPhase(&ref_mgr->ilc, cyc_sel) = ref_mgr->ilc.event_phase_ns;

    // If the saved ILC reference length isn't zero then either copy or interpolate the saved ILC reference

    if(ref_num_els > 3)
    {
        // An ILC reference array is available so arm the use of the reference on this cycle

        rt_status |= REF_ILC_REF_ARM_BIT_MASK;

        // Check if ILC reference array should be re-sampled because the event phase changed.
        // This is suppressed if the MODE_EVENT_OFFSET_US is odd to allow tests with or without re-sampling.

        if(phase_change_ns != 0)
        {
            refIlcResampleRef(reg_period_ns, phase_change_ns, ref_num_els, ref_buf_p);
        }
    }

    // Save the ILC reference number of elements

    ref_mgr->ilc.ref.num_els = ref_num_els;

    // Return the initial real-time status bit mask

    return rt_status;
}



static void refIlcErrCalcRT(struct REF_mgr * const ref_mgr,
                            uint32_t       * const rt_status,
                            cc_float               ilc_err,
                            cc_float         const v_ref_rate)
{
    // Shift open loop down-shifter by 1 bit

    ref_mgr->ilc.openloop_downshifter >>= 1;

    // Load open loop down-shifter when regulation is open loop

    bool const openloop = ref_mgr->reg_mgr->openloop;

    if(openloop == true)
    {
        ref_mgr->ilc.openloop_downshifter |= ref_mgr->ilc.openloop_downshifter_input;
    }

    // Clear the ILC error if the conditions are not met

    if(   openloop == false
       && ref_mgr->ilc.ref.run_extended != 0
       && (*rt_status & REF_ILC_Q_CALC_BIT_MASK) != 0)
    {
        // Accumulate the ILC error and v_ref_rate squared so that their RMSs can be calculated at the end of the cycle

        ref_mgr->ilc.err.iter_counter++;

        ref_mgr->ilc.err.acc_ilc_err_2    += ilc_err    * ilc_err;
        ref_mgr->ilc.err.acc_v_ref_rate_2 += v_ref_rate * v_ref_rate;

        *rt_status |= REF_ILC_ERR_BIT_MASK;
    }
    else
    {
        // Reset the ILC error

        ilc_err = 0.0F;

        *rt_status &= ~REF_ILC_ERR_BIT_MASK;
    }

    // Save the ILC error in the circular buffer and in the logging structure

    ref_mgr->ilc.err.err_buf[ref_mgr->ilc.err.index++ & REF_ILC_ERR_MASK] = ref_mgr->ilc.logging.err = ilc_err;
}



static void refIlcLcalcRT(struct REF_mgr * const ref_mgr)
{
    // Prepare L convolution loop variables- the errors comes from a circular buffer so two loops may be needed

    uint32_t const l_func_num_els    = ref_mgr->ilc.l.num_els;
    uint32_t const err_index         = ref_mgr->ilc.err.index & REF_ILC_ERR_MASK;
    uint32_t const initial_err_index = (err_index - l_func_num_els) & REF_ILC_ERR_MASK;
    uint32_t const loop_1_count      = err_index < initial_err_index
                                     ? REF_ILC_UNFLTR_REF_LEN - initial_err_index
                                     : l_func_num_els;
    uint32_t const loop_2_count      = l_func_num_els - loop_1_count;
    cc_float     * err_p             = &ref_mgr->ilc.err.err_buf[initial_err_index];
    cc_float     * l_func_p          =  ref_mgr->ilc.l.function;

    // Run the L convolution of filtered error and the L function

    uint32_t i;
    cc_float delta_unfltr_ref = 0.0F;

    for(i = 0 ; i < loop_1_count ; i++)
    {
        delta_unfltr_ref += *(l_func_p++) * *(err_p++);
    }

    // Run optional second loop if the error vector spans the end of the circular error buffer

    err_p = ref_mgr->ilc.err.err_buf;

    for(i = 0 ; i < loop_2_count ; i++)
    {
        delta_unfltr_ref += *(l_func_p++) * *(err_p++);
    }

    // Save the result in the circular unfiltered ref buffer

    uint32_t const result_index = ref_mgr->ilc.l.control.index++ & REF_ILC_UNFLTR_REF_MASK;
    uint32_t const ref_index    = ref_mgr->ilc.q.control.index + ref_mgr->ilc.q.order;

    ref_mgr->ilc.q.ref_unfltr[result_index] = delta_unfltr_ref + ref_mgr->ilc.ref.buf[ref_index];
}



static void refIlcQcalcRT(struct REF_mgr * const ref_mgr)
{
    // Prepare the Q convolution loop variables - the unfiltered ref is in a circular buffer so two loops may be needed

    uint32_t const q_func_num_els   = ref_mgr->ilc.q.num_els;
    uint32_t const unfltr_ref_index = ref_mgr->ilc.l.control.index & REF_ILC_UNFLTR_REF_MASK;
    cc_float       fltr_ref         = 0.0F;

    if(q_func_num_els > 0)
    {
        uint32_t const initial_unfltr_ref_index = (unfltr_ref_index - q_func_num_els) & REF_ILC_UNFLTR_REF_MASK;
        uint32_t const loop_1_count             = unfltr_ref_index < initial_unfltr_ref_index
                                                ? REF_ILC_UNFLTR_REF_LEN - initial_unfltr_ref_index
                                                : q_func_num_els;
        uint32_t const loop_2_count             = q_func_num_els - loop_1_count;
        cc_float     * unfltr_ref_p             = &ref_mgr->ilc.q.ref_unfltr[initial_unfltr_ref_index];
        cc_float     * q_func_p                 =  ref_mgr->ilc.q.function;

        // Run first loop always

        uint32_t i;

        for(i = 0 ; i < loop_1_count ; i++)
        {
            fltr_ref += *(q_func_p++) * *(unfltr_ref_p++);
        }

        // Run optional second loop if the unfiltered ref vector spans the end of the circular buffer

        unfltr_ref_p = ref_mgr->ilc.q.ref_unfltr;

        for(i = 0 ; i < loop_2_count ; i++)
        {
            fltr_ref += *(q_func_p++) * *(unfltr_ref_p++);
        }
    }
    else
    {
        // Q function is empty so skip the Q calculation

        fltr_ref = ref_mgr->ilc.q.ref_unfltr[(unfltr_ref_index - 1) & REF_ILC_UNFLTR_REF_MASK];
    }

    // Save the filtered reference in the ILC ref array

    ref_mgr->ilc.ref.buf[ref_mgr->ilc.q.control.index++] = (ref_mgr->ilc.openloop_downshifter & 1) == 0
                                                         ? fltr_ref
                                                         : 0.0F;
}


//! Check ref_adv against the stop_ref threshold.
//!
//! This function will detect the transition of the advanced reference from being above the stop_ref
//! threshold to being below.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     ref_adv           Advanced reference.
//!
//! @retval        true              ref_adv just passed below the stop_ref threshold
//! @retval        false             ref_adv did not just pass below the stop_ref threshold

static bool refIlcStopRef(struct REF_mgr * const ref_mgr,
                          cc_float         const ref_adv)
{
    // Detect if the advanced reference just passed below the stop threshold

    bool const ref_stop = ref_adv < ref_mgr->ilc.stop_ref;
    bool const stop_now = (ref_stop == true && ref_mgr->ilc.prev_ref_stop == false);

    ref_mgr->ilc.prev_ref_stop = ref_stop;

    return stop_now;
}


//! Manage the real-time status bits.
//!
//! This function will detect the transition of the time from before to after the start time for different stages of the ILC.
//! It can also detect when the associated buffer index reaches the end of the buffer and will reset the associated bit.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in,out] control           Pointer to ILC control structure.
//! @param[in]     rt_status         Pointer to real-time status bit mask.
//! @param[in]     time              Function time (nanosecond resolution)
//! @param[in]     max_index         Maximum value for index before activity is suspended.
//! @param[in]     check_start_time  True: check the start time. False: do not check the start time.
//!
//! @retval        true              The ILC stage is active.
//! @retval        false             The ILC stage is inactive.

static bool refIlcControlRT(struct REF_mgr         * const ref_mgr,
                            struct REF_ilc_control * const control,
                            uint32_t               * const rt_status,
                            struct CC_ns_time        const time,
                            uint32_t                 const max_index,
                            bool                     const check_start_time)
{
    uint32_t const start_flag = cctimeNsCmpRelTimesRT(time, control->start_time) >= 0;

    // Detect the transition from time before to time after the start time when permitted to by the check_start_time flag

    if(check_start_time == true && (control->prev_start_flag | start_flag) == 0x1)
    {
        // Set bit in the real-time status to enable this stage of the ILC

        *rt_status |= control->rt_status_bit_mask;
    }

    // Move the start flag by one bit into the previous start flag

    control->prev_start_flag = start_flag << 1;

    // If max index is provided then check if the buffer is now full

    if(max_index > 0 && control->index >= max_index)
    {
        // Index has reached the upper limit so disable this ILC stage

        *rt_status &= ~control->rt_status_bit_mask;
    }

    // Return boolean flag indicating if this ILC stage is currently active

    return (*rt_status & control->rt_status_bit_mask) != 0;
}



void refIlcRT(struct REF_mgr * const ref_mgr,
              cc_float         const ref_adv,
              cc_float         const reg_meas,
              cc_float         const v_ref_rate)
{
    uint32_t rt_status = ref_mgr->ilc.rt_status;

    // If rt_status has already been reset by refIlcStateToCycling or
    // REF STATE is no longer in a cycling state then stop the ILC by requesting the transition to TO_CYCLING ILC state

    if(rt_status == 0 || (ref_mgr->ref_state_mask & REF_ILC_CYCLING_STATES) == 0)
    {
        ref_mgr->ilc.to_cycling = true;

        return;
    }

    // Process the ILC error and V_REF rate

    refIlcErrCalcRT(ref_mgr, &rt_status, ref_adv - reg_meas, v_ref_rate);

    // Run the other ILC stages when directed to do so by the bits in rt_status

    if((rt_status & REF_ILC_L_CALC_BIT_MASK) != 0)
    {
        refIlcLcalcRT(ref_mgr);

        if((rt_status & REF_ILC_Q_CALC_BIT_MASK) != 0)
        {
            refIlcQcalcRT(ref_mgr);
        }
    }

    // Manage the timing control for each ILC stage - only check the start time if ILC is enabled for this cycle
    // (ref_range != 0) and in CYCLING state

    bool check_start_time = ref_mgr->ilc.enabled == true && ref_mgr->ref_state == REF_CYCLING;

    // The earliest ILC stage to start can be either L or REF and either all stages or no stages should run
    // on a given cycle. So it is important to check the start time of the earliest stage first and only the other stages
    // if this stage has started

    struct CC_ns_time const fg_time = ref_mgr->fg.time;

    if(cctimeNsCmpRelTimesRT(ref_mgr->ilc.ref.control.start_time, ref_mgr->ilc.l.control.start_time) <= 0)
    {
        // Check the start time for ref first and then L

        check_start_time = refIlcControlRT(ref_mgr,
                                           &ref_mgr->ilc.ref.control,
                                           &rt_status,
                                           fg_time,
                                           ref_mgr->ilc.ref.num_els,
                                           check_start_time);

        refIlcControlRT(ref_mgr,
                        &ref_mgr->ilc.l.control,
                        &rt_status,
                        fg_time,
                        0,
                        check_start_time);
    }
    else
    {
        // Check the start time for L first and then ref

        check_start_time = refIlcControlRT(ref_mgr,
                                           &ref_mgr->ilc.l.control,
                                           &rt_status,
                                           fg_time,
                                           0,
                                           check_start_time);

        refIlcControlRT(ref_mgr,
                        &ref_mgr->ilc.ref.control,
                        &rt_status,
                        fg_time,
                        ref_mgr->ilc.ref.num_els,
                        check_start_time);
    }

    refIlcControlRT(ref_mgr,
                    &ref_mgr->ilc.q.control,
                    &rt_status,
                    fg_time,
                    ref_mgr->ilc.max_samples,
                    check_start_time);

    // Manage the gating of the ILC error after REF_RUN is reset - run_extended is a down shifter extends REF_RUN
    // by (L_ORDER + 1) iterations. This is a down shifter because L_ORDER will never exceed 32 and it avoids an extra branch.

    if((rt_status & REF_ILC_REF_RUN_BIT_MASK) != 0)
    {
        // REF_RUN is active: Initialise the down shifter to bit L_ORDER

        ref_mgr->ilc.ref.run_extended = 1 << ref_mgr->ilc.l.order;
    }
    else
    {
        // REF_RUN is not active: shift right run_extended to extend REF_RUN by the initial bit number iterations

        ref_mgr->ilc.ref.run_extended >>= 1;
    }

    // Check ref_adv against the stop_ref threshold and stop Q_CALC if it falls below the threshold

    if(refIlcStopRef(ref_mgr, ref_adv) == true)
    {
        rt_status &= ~REF_ILC_Q_CALC_BIT_MASK;
    }

    // Save the rt_status bit mask

    ref_mgr->ilc.rt_status = rt_status;
}



cc_float refIlcRefRT(struct REF_mgr * const ref_mgr)
{
    // Save the REF_ILC for logging and return it

    if((ref_mgr->ilc.rt_status & REF_ILC_AVAILABLE_MASK) == REF_ILC_AVAILABLE_MASK)
    {
        // The ILC reference is available (as computed on the previous cycle with the same cycle selector) so return it

        ref_mgr->ilc.logging.ref = ref_mgr->ilc.ref.buf[ref_mgr->ilc.ref.control.index++];
    }
    else if((ref_mgr->ilc.rt_status & REF_ILC_Q_CALC_BIT_MASK) == 0)
    {
        // The ILC reference is no longer available for this cycle and the calculation of the reference
        // for the next cycle has also finished so reset the ILC reference. Also reset the logged ILC error.

        ref_mgr->ilc.logging.ref = 0.0F;
        ref_mgr->ilc.logging.err = 0.0F;
    }

    // If the ILC reference is no longer available but it is still be computed for the next cycle then
    // the last value will be held. This is not perfect but is the simplest and best strategy, provided the
    // reference is no accelerating or decelerating.

    return ref_mgr->ilc.logging.ref;
}

// EOF
