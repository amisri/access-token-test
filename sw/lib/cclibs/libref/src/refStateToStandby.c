//! @file  refStateToStandby.c
//! @brief Converter Control Reference Manager library: TO_STANDBY reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStateToStandbyInitRT(struct REF_mgr * const ref_mgr)
{
    // Change regulation mode if required

    ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, ref_mgr->iter_cache.mode_reg_mode, false);

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Initialize ramp to standby reference

    cc_float const ref_standby = ref_mgr->reg_mgr->lim_ref->standby;

    // Initialize a RAMP to STANDBY reference, taking into account the optional polarity switch setting

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     refMgrInitialRate(ref_mgr),
                     ref_mgr->ref_fg_unlimited,
                     polswitchIsNegative(ref_mgr->polswitch_mgr) == true ? -ref_standby : ref_standby,
                     REF_USE_ACC_FOR_DEC);

    // Reset force_openloop flag

    ref_mgr->force_openloop = false;

    return refStateToStandbyRT;
}



enum REF_state refStateToStandbyRT(struct REF_mgr * const ref_mgr)
{
    refRampMgrRT(ref_mgr);

    return REF_TO_STANDBY;
}

// EOF
