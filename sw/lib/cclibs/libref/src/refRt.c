//! @file  refRt.c
//! @brief Converter Control Function Manager library real-time functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"


void refRtRegFaultsRT(struct REF_mgr * const ref_mgr, bool const latch_faults)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    uint32_t unlatched_faults = ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_MEAS_FAULT    ) << REF_B_MEAS_FAULT_BIT    ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_MEAS_TRIP     ) << REF_B_MEAS_TRIP_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_ERR_FAULT     ) << REF_B_ERR_FAULT_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_MEAS_FAULT    ) << REF_I_MEAS_FAULT_BIT    ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_MEAS_TRIP     ) << REF_I_MEAS_TRIP_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_ERR_FAULT     ) << REF_I_ERR_FAULT_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_RMS_FAULT     ) << REF_I_RMS_FAULT_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_RMS_LOAD_FAULT) << REF_I_RMS_LOAD_FAULT_BIT) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_MEAS_FAULT    ) << REF_V_MEAS_FAULT_BIT    ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_MEAS_TRIP     ) << REF_V_MEAS_TRIP_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_ERR_FAULT     ) << REF_V_ERR_FAULT_BIT     ) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_RATE_RMS_FAULT) << REF_V_RATE_RMS_FAULT_BIT) |
                                ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_CAPA_FAULT    ) << REF_I_CAPA_FAULT_BIT    );

    // Set B/I RST faults only when the ref_state is OFF

    if(ref_mgr->iter_cache.pc_state != REF_PC_ON)
    {
        // If field regulation is not needed for a circuit then the user will set LIMITS.B.POS/NEG to zero,
        // which disables field measurement limits in b.lim_meas

        if(   reg_mgr->b.regulation == CC_ENABLED
           && reg_mgr->b.lim_meas.flags.enabled == true
           && reg_mgr->b.last_op_rst_pars.status >= REG_FAULT)
        {
            unlatched_faults |= REF_B_RST_FAULT_BIT_MASK;
        }

        // If current regulation is not needed for a circuit then the user will set LIMITS.I.POS/NEG to zero,
        // which disables field measurement limits in i.lim_meas

        if(   reg_mgr->i.regulation == CC_ENABLED
           && reg_mgr->i.lim_meas.flags.enabled == true
           && reg_mgr->i.last_op_rst_pars.status >= REG_FAULT)
        {
            unlatched_faults |= REF_I_RST_FAULT_BIT_MASK;
        }
    }

    // Latch new faults or reset the faults latch according to the latch_faults flag

    if(latch_faults == true)
    {
        // Latch the faults

        ref_mgr->latched_faults |= unlatched_faults;
    }
    else
    {
        // Reset the faults latch

        ref_mgr->latched_faults = unlatched_faults;
    }

    // Save unlatched faults to help with debugging

    ref_mgr->unlatched_faults = unlatched_faults;
}



static inline void refRtRegWarningsRT(struct REF_mgr * const ref_mgr)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    // Combine warning libreg flags into the warnings bit mask

    uint32_t warnings = ((uint32_t)regMgrVarValue(reg_mgr, FLAG_REF_CLIPPED       ) << REF_REF_CLIPPED_BIT       ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_REF_CLIPPED     ) << REF_B_REF_CLIPPED_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_ERR_WARNING     ) << REF_B_ERR_WARNING_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_REF_CLIPPED     ) << REF_I_REF_CLIPPED_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_ERR_WARNING     ) << REF_I_ERR_WARNING_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_ERR_WARNING     ) << REF_V_ERR_WARNING_BIT     ) |
                        ((uint32_t)refMgrVarValue(ref_mgr, FLAG_CYCLING_WARNING   ) << REF_CYCLING_WARNING_BIT   ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_RMS_WARNING     ) << REF_I_RMS_WARNING_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_RMS_LOAD_WARNING) << REF_I_RMS_LOAD_WARNING_BIT) |
                        ((uint32_t)refMgrVarValue(ref_mgr, FLAG_ILC_FAILED        ) << REF_ILC_WARNING_BIT       );

    // Combine the ref rate warnings when

    if(ref_mgr->enable_reg_err)
    {
        warnings |=     ((uint32_t)regMgrVarValue(reg_mgr, FLAG_REF_RATE_CLIPPED  ) << REF_REF_RATE_CLIPPED_BIT  ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_B_REF_RATE_CLIPPED) << REF_B_REF_RATE_CLIPPED_BIT) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_I_REF_RATE_CLIPPED) << REF_I_REF_RATE_CLIPPED_BIT) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_REF_CLIPPED     ) << REF_V_REF_CLIPPED_BIT     ) |
                        ((uint32_t)regMgrVarValue(reg_mgr, FLAG_V_REF_RATE_CLIPPED) << REF_V_REF_RATE_CLIPPED_BIT);
    }

    // Report invalid B/I RST regulators as warnings in any state - they are also faults when the converter is off

    if(   reg_mgr->b.regulation              == CC_ENABLED
       && reg_mgr->b.lim_meas.flags.enabled  == true
       && reg_mgr->b.last_op_rst_pars.status >= REG_FAULT)
    {
        warnings |= REF_B_RST_WARNING_BIT_MASK;
    }

    if(   reg_mgr->i.regulation              == CC_ENABLED
       && reg_mgr->i.lim_meas.flags.enabled  == true
       && reg_mgr->i.last_op_rst_pars.status >= REG_FAULT)
    {
        warnings |= REF_I_RST_WARNING_BIT_MASK;
    }

    // Include dynamic economy warnings when in cycling related states

    if((ref_mgr->ref_state_mask & REF_STATES_DYN_ECO) != 0)
    {
        warnings |= ((uint32_t)ref_mgr->fsm.dyn_eco.arm_warning << REF_DYN_ECO_ARM_WARNING_BIT) |
                    ((uint32_t)ref_mgr->fsm.dyn_eco.run_warning << REF_DYN_ECO_RUN_WARNING_BIT);
    }
    else
    {
        ref_mgr->fsm.dyn_eco.arm_warning = false;
        ref_mgr->fsm.dyn_eco.run_warning = false;
    }

    ref_mgr->warnings = warnings;
}



static inline cc_float refRtRefActuationRT(struct REF_mgr * const ref_mgr, struct REG_mgr * const reg_mgr)
{
    // Calculate the actuation reference, taking into account the state of the polarity switch and the offset

    ref_mgr->ref_actuation = -regMgrParValue(reg_mgr, VS_OFFSET)
                           + (  polswitchIsNegative(ref_mgr->polswitch_mgr) == false
                             ?  regMgrVarValue(reg_mgr, REF_ACTUATION)
                             : -regMgrVarValue(reg_mgr, REF_ACTUATION));

    // Return the DAC reference - force DAC_CLAMP voltage when the converter is not on

    return ref_mgr->iter_cache.pc_state != REF_PC_ON
           ? refMgrParValue(ref_mgr, DAC_CLAMP)
           : ref_mgr->ref_actuation * regMgrVarValue(reg_mgr, VREG_OP_INV_VS_GAIN);
}



void refRtControlRealTimeRefRT(struct REF_mgr * const ref_mgr, enum REF_state const ref_state)
{
    ccprintf("\n  ref_state=%u  reg_mode=%u\n"
             "\n  rt_ref.direct=%u  rt_ref.delta=%u  rt_ref.in_use=%u\n"
             "\n  ref_fg=%g  ref_fg_unlimited=%g  rt_ref.value=%g  *ref=%g\n"
            , ref_state
            , ref_mgr->iter_cache.reg_mode
            , ref_mgr->rt_ref.direct
            , ref_mgr->rt_ref.delta
            , ref_mgr->rt_ref.in_use
            , ref_mgr->ref_fg
            , ref_mgr->ref_fg_unlimited
            , ref_mgr->rt_ref.value
            , *ref_mgr->reg_mgr->ref
            );

    // Set rt_ref_direct, rt_ref_delta and rt_ref.in_use flags

    bool rt_ref_direct;     // True if RT_REF is used directly as the reference in DIRECT state
    bool rt_ref_delta;      // True if RT_REF is used as a delta_ref in IDLE/ARMED/RUNNING or CYCLING states;
    bool rt_ref_in_use;     // True if either rt_ref_direct or rt_ref_delta is true (they cannot both be true)

    if(ref_state == REF_OFF)
    {
        // Disable RT ref if calling function signals with ref_state OFF

        rt_ref_direct = false;
        rt_ref_delta  = false;
        rt_ref_in_use = false;
    }
    else
    {
        // Determine if RT ref will be in use

        rt_ref_in_use =    refMgrParValue(ref_mgr, MODE_RT_REF) == CC_ENABLED
                        && refMgrParValue(ref_mgr, MODE_REG_MODE_CYC) == ref_mgr->iter_cache.reg_mode;

        // Determine if RT ref will be direct or delta according to ref_state

        if(ref_state == REF_DIRECT)
        {
            rt_ref_direct = rt_ref_in_use;
            rt_ref_delta  = false;
        }
        else
        {
            rt_ref_direct = false;
            rt_ref_delta  = rt_ref_in_use;
        }
    }

    ref_mgr->rt_ref.in_use = rt_ref_in_use;

    if(ref_mgr->rt_ref.direct != rt_ref_direct)
    {
        // rt_ref_direct flag has changed

        ref_mgr->rt_ref.direct = rt_ref_direct;

        if(rt_ref_direct == true)
        {
            // Enabling direct real time reference - reset function generator reference

            ref_mgr->ref_fg = 0.0F;
        }
        else
        {
            // Disabling direct real time reference - reset ref_fg to ref_rt (in ref_now)

            ref_mgr->ref_fg = *ref_mgr->reg_mgr->ref;
        }

        ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg;
    }
    else if(ref_mgr->rt_ref.delta != rt_ref_delta)
    {
        // rt_ref_delta flag has changed

        if(rt_ref_delta == true)
        {
            // Enabling delta real time reference - adjust to avoid discontinuity

            ref_mgr->ref_fg -= ref_mgr->rt_ref.value;
        }
        else
        {
            // Disabling delta real time reference - reset ref_fg to the combined ref_fg + ref_rt

            ref_mgr->ref_fg = *ref_mgr->reg_mgr->ref;
        }

        ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg;
    }

    ref_mgr->rt_ref.delta = rt_ref_delta;

    ccprintf("\n  rt_ref.direct=%u  rt_ref.delta=%u  rt_ref.in_use=%u\n"
             "\n  ref_fg=%g  ref_fg_unlimited=%g  rt_ref.value=%g\n"
            , ref_mgr->rt_ref.direct
            , ref_mgr->rt_ref.delta
            , ref_mgr->rt_ref.in_use
            , ref_mgr->ref_fg
            , ref_mgr->ref_fg_unlimited
            , ref_mgr->rt_ref.value
            );
}



static cc_float refRtRealTimeRefRT(struct REF_mgr * const ref_mgr)
{
    if(refMgrParValue(ref_mgr, MODE_RT_REF) == CC_DISABLED)
    {
        // RT delta reference is disabled so reset values and in use flag and return zero

        ref_mgr->rt_ref.down_counter = 0;
        ref_mgr->rt_ref.value        = 0.0F;
        ref_mgr->rt_ref.last_value   = 0.0F;
        ref_mgr->rt_ref.direct       = false;
        ref_mgr->rt_ref.delta        = false;
        ref_mgr->rt_ref.in_use       = false;

        return 0.0F;
    }

    // Real-time delta reference is enabled so get value from application

    cc_float const rt_ref = refMgrParValue(ref_mgr, RT_REF);

    // If reference value has changed

    if(rt_ref != ref_mgr->rt_ref.last_value)
    {
        ref_mgr->rt_ref.last_value = rt_ref;

        if(isnan(rt_ref) || fabsf(rt_ref) > 1.0E5F)
        {
            // New value is invalid so ignore it

            ref_mgr->rt_ref.num_invalid++;
        }
        else
        {
            // New value is valid

            ref_mgr->rt_ref.down_counter = refMgrParValue(ref_mgr, RT_NUM_STEPS);

            if(ref_mgr->rt_ref.down_counter != 0)
            {
                // Number of steps for interpolation is non-zero so prepare step size

                ref_mgr->rt_ref.step = (rt_ref - ref_mgr->rt_ref.value) / (cc_float)ref_mgr->rt_ref.down_counter;
            }
            else
            {
                // Number of steps for interpolation is zero so jump immediately to the new value

                ref_mgr->rt_ref.value = rt_ref;
            }
        }
    }

    // If interpolation down counter is running

    if(ref_mgr->rt_ref.down_counter != 0)
    {
        // Adjust rt_ref value by one interpolation step

        ref_mgr->rt_ref.value += ref_mgr->rt_ref.step;

        ref_mgr->rt_ref.down_counter--;
    }

    // Return zero if rt_delta_ref is not in use

    return ref_mgr->rt_ref.in_use ? ref_mgr->rt_ref.value : 0.0F;
}



static cc_float refRtCalcRefRT(struct REF_mgr * const ref_mgr)
{
    // Calculate the function generation time and time advanced by the ref_advance

    refFuncMgrSetFuncTimeRT(ref_mgr);

    // Set FG reference from function generator or external reference (this can modify ref_mgr->fg.time_adv_fp64)

    refFuncMgrSetRefRT(ref_mgr);

    // Take a copy of the function generator reference before it might be limited by regMgrRegRT()

    ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg;

    // Combine FG reference with real-time delta reference and the ILC delta reference to produce the reference for regulation

    ref_mgr->ref_rt = refRtRealTimeRefRT(ref_mgr);

    cc_float ref = ref_mgr->ref_fg + ref_mgr->ref_rt;

    // Compute enable_reg_err flag based on the state and the function generator status

    ref_mgr->enable_reg_err =    (ref_mgr->ref_state_mask & REF_STATES_REG_ERR_ENABLED) != 0
                              || (  (ref_mgr->ref_state_mask & (REF_CYCLING_MASK | REF_PAUSED_MASK)) != 0
                                  && ref_mgr->fg.status          == FG_DURING_FUNC
                                  && ref_mgr->fsm.cycling.status == FG_DURING_FUNC);

    // If the function generator indicates that the reference is paused, prepare internal PAUSE event

    if(   ref_mgr->fg.status == FG_PAUSED
       && ref_mgr->event_mgr.internal_pause_event.is_pending == false)
    {
        // Convert fg time_adv_fp64 returned from fgTable to the absolute time of the internal pause event

        struct CC_ns_time const paused_time = cctimeNsAddRT(ref_mgr->fg.event.abs_time,
                                                            cctimeNsAddOffsetRT(cctimeNsDoubleToRelTimeRT(ref_mgr->fg.time_adv_fp64),
                                                                                -ref_mgr->reg_mgr->ref_advance_ns));

        refEventMgrTriggerInternalPauseEventRT(&ref_mgr->event_mgr, paused_time);
    }

    if(ref_mgr->fg.status == FG_POST_FUNC)
    {
        // Do not disturb the ramp manager while it's changing polarity.
        // Also, do not clear the event in pre-func state in CYCLING, because
        // the times of all pre-func ramps are calculated relative to the start func
        // event time.

        if(   ref_mgr->fg.event.abs_time.secs.abs > 0
           && ref_mgr->ramp_mgr.status != FG_DURING_FUNC
           && (   ref_mgr->iter_cache.ref_mode != REF_CYCLING
               || ref_mgr->fsm.cycling.status == FG_POST_FUNC))
        {
            // Clear the function generator event structure and parameters

            refFuncMgrResetRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);
        }
    }

    return ref;
}



static enum REF_state refRtCoast(struct REF_mgr * const ref_mgr)
{
    enum REF_state const ref_mode = refMgrParValue(ref_mgr, MODE_REF);

    // Recover (from coast) if recover event is active or if the application has changed MODE_REF to not be CYCLING

    if(ref_mgr->event_mgr.recover_event.is_active == true || ref_mode != REF_CYCLING)
    {
        ref_mgr->event_mgr.coast = false;
    }

    // Coast event - latch the coast request to keep ref_mode as IDLE

    if(ref_mgr->event_mgr.coast_event.is_active == true || ref_mgr->event_mgr.coast == true)
    {
        ref_mgr->event_mgr.coast = true;

        return REF_IDLE;
    }

    // Not in coast so return MODE_REF

    return ref_mode;
}



static void refRtIterationPostRegRT(struct REF_mgr * const ref_mgr)
{
    // Activate action events if the time of the next iteration exceeds the time of the event

    refEventMgrActivateActionEventsRT(&ref_mgr->event_mgr, cctimeNsAddOffsetRT(ref_mgr->iter_time, ref_mgr->reg_mgr->reg_period_ns));

    // Start or stop the voltage harmonics generation, based on the associated timing events

    if(ref_mgr->event_mgr.stop_harmonics_event.is_active)
    {
        refMgrParValue(ref_mgr, MODE_HARMONICS) = CC_DISABLED;
    }
    else if(ref_mgr->event_mgr.start_harmonics_event.is_active)
    {
        refMgrParValue(ref_mgr, MODE_HARMONICS) = CC_ENABLED;
    }

    // Take copies of essential libref and libreg parameters to ensure that they are coherent during the whole regulation iteration (from this point)
    // For ref_mode, take into account coast/recover events

    ref_mgr->iter_cache.mode_reg_mode = refMgrParValue(ref_mgr, MODE_REG_MODE);
    ref_mgr->iter_cache.pc_state      = refMgrParValue(ref_mgr, PC_STATE);
    ref_mgr->iter_cache.ref_mode      = refRtCoast(ref_mgr);

    // Set ref_mgr->post_func when the function ends

    if(ref_mgr->fg.status == FG_POST_FUNC)
    {
        if(ref_mgr->post_func == false)
        {
            ref_mgr->post_func = true;

            // Set ref_rate to the final value for the function that just finished

            ref_mgr->reg_mgr->ref_rate = ref_mgr->fg.pars->meta.range.final_rate;
        }
    }
    else
    {
        ref_mgr->post_func = false;
    }

    // When running a RAMP, any modification to ref_fg must be fed back to fgRampRT(). So since the reference
    // can be limited or modified by the ILC in regMgrRegRT(), when running a RAMP the value of ref_fg must be recomputed.

    if(ref_mgr->fg.pars->meta.type == FG_RAMP)
    {
        ref_mgr->ref_fg = ref_mgr->ref_now - ref_mgr->ref_rt - ref_mgr->ilc.logging.ref;
    }

    // Run the Iterative Learning Controller if ILC state is CYCLING

    if(refIlcCyclingRT(&ref_mgr->ilc) == true)
    {
        struct REG_mgr const * reg_mgr = ref_mgr->reg_mgr;

        refIlcRT(ref_mgr, reg_mgr->reg_signal->ref_adv, reg_mgr->reg_signal->meas.reg, reg_mgr->v.ref_rate);
    }

    // Run the reference state machine

    refStateRT(ref_mgr);

    // Manage the cyc_status recording if the state transitions from a "CYCLING" state

    if(refCycStatusMgrRT(ref_mgr) == true)
    {
        // Reset CYCLING counter when exiting CYCLING state

        ref_mgr->fsm.cycling.counter = 0;
    }

    // Reset stale action events and next events

    refEventMgrResetStaleEventsRT(&ref_mgr->event_mgr, ref_mgr->iter_cache.ref_mode, ref_mgr->ref_state_mask);

    // Set REF DIRECT_STATE to IDLE or RUNNING

    refStateDirectSetRT(ref_mgr);

    // Set the regulation RST source according to the state machine if an event was just activated

    if(ref_mgr->fg.internal_evt > 0)
    {
        ref_mgr->reg_rst_source = (ref_mgr->ref_state_mask & REF_STATES_RST_SOURCE_ENABLED) != 0
                                ? ref_mgr->fg.event.rst_source
                                : REG_OPERATIONAL_RST_PARS;
    }
}



void refRtRegulationRT(struct REF_mgr * const ref_mgr, struct CC_ns_time const iter_time)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    // Cache the regulation mode from libreg

    ref_mgr->iter_cache.reg_mode = reg_mgr->reg_mode;

    // Store absolute iteration time in ref_mgr

    ref_mgr->iter_time   = iter_time;
    ref_mgr->iter_time_s = cctimeNsAbsTimeToDoubleRT(iter_time);

    // Simulate circuit voltage, current and field and filter capacitor current measurements

    regMgrSimMeasurementsRT(reg_mgr, &reg_mgr->sim_vars);

    // Interpolate the voltage feed forward signal at the iteration rate

    struct CC_meas_signal v_ff;

    if(refMgrParValue(ref_mgr, VFEEDFWD_GAIN) != 0.0F)
    {
        refMgrGetVffRT(ref_mgr, &v_ff);
    }
    else
    {
        v_ff.is_valid = false;
        v_ff.signal   = 0.0F;
    }

    regMgrMeasSetVffRT(reg_mgr, v_ff);

    // Start real-time iteration by registering new acquisitions

    ref_mgr->iteration_index = regMgrMeasSetRT(reg_mgr,
                                               ref_mgr->reg_rst_source,
                                               iter_time,
                                               polswitchIsNegative(ref_mgr->polswitch_mgr),
                                               ref_mgr->ref_state == REF_POL_SWITCHING);

    // Calculate the reference and get the ILC reference on regulation iterations

    cc_float ref_ilc = 0.0F;

    if(ref_mgr->iteration_index == 0)
    {
        ref_mgr->ref_now = refRtCalcRefRT(ref_mgr);
        ref_ilc          = refIlcRefRT(ref_mgr);
    }

    // Run regulation - this is called every iteration but the regulation algorithm
    // is only started when ref_mgr->iteration_index == 0 so ref and ref_ilc are ignored otherwise.
    // Regulation will complete on the same iteration unless decoupling is enabled,
    // in which case it will complete on a later iteration, defined by the decoupling phase.
    // reg_complete will be true on these iterations.

    // refIlcRefRunningRT() is used to set the ilc_active flag. When true, the regulation error
    // will be (reg_meas - ref_adv), otherwise, it is (meas[err_meas_select] - ref_delayed).
    // In addition, the regulation error calculation rate will be the regulation rate when ilc_active is true,
    // even if it is the measurement rate the rest of the time.

    ref_mgr->reg_complete = regMgrRegRT(reg_mgr,
                                        &ref_mgr->ref_now,
                                        ref_ilc,
                                        ref_mgr->enable_reg_err,
                                        ref_mgr->force_openloop,
                                        refMgrParValue(ref_mgr, MODE_HARMONICS) == CC_ENABLED,  // generate_harmonics
                                        refIlcRefRunningRT(&ref_mgr->ilc));                     // ilc_active

    // Process the libreg actuation and get the DAC reference, taking into account the PC offset, polarity switch position, DAC clamp and PC gain

    ref_mgr->ref_dac = refRtRefActuationRT(ref_mgr, reg_mgr);
}



void refRtStateRT(struct REF_mgr * const ref_mgr)
{
    // Set libreg warning flags and latch libreg and libref fault flags

    refRtRegWarningsRT(ref_mgr);
    refRtRegFaultsRT(ref_mgr, true);      // true to latch faults

    // Set or shift function manager and external flags for logging - ensures flags are set for at least one full iteration

    refFuncMgrShiftFlagsRT(ref_mgr);
    refEventMgrShiftFlagsRT(&ref_mgr->event_mgr);

    // Run the post regulation processing (include ref state machine) on the iterations when the regulation completes

    if(ref_mgr->reg_complete == true)
    {
        refRtIterationPostRegRT(ref_mgr);
    }

    // Simulate power converter and load response without taking into account the PC actuation delay.
    // So the simulated response is in advance by VS_ACT_DELAY and this must be taken into account if the signals are logged.

    regMgrSimVsAndLoadRT(ref_mgr->reg_mgr, (polswitchVarValue(ref_mgr->polswitch_mgr,STATE) == POLSWITCH_STATE_MOVING));   // Simulate open circuit when polarity switch is moving
}

// EOF
