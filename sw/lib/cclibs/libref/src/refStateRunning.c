//! @file  refStateRunning.c
//! @brief Converter Control Reference Manager library: RUNNING reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStateRunningInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_event_mgr * const event_mgr = &ref_mgr->event_mgr;

    // On first call, start running the next reference function
    // Order of activation matters!

    struct REF_event * const active_event = refEventMgrActivateNextEventRT(event_mgr);
    struct REF_func  * const active_func  = refEventMgrActivateNextFuncRT (event_mgr, REF_EVENT_RUN);

    CC_ASSERT(active_func->event_type == REF_EVENT_RUN);

    refFuncMgrUpdateParsRT (&ref_mgr->fg, &active_func->ref_armed.fg_pars);
    refFuncMgrUpdateEventRT(&ref_mgr->fg, active_event);

    // Reset abort flag

    event_mgr->abort = false;

    // Set fg status to PRE_FUNC

    ref_mgr->fg.status = FG_PRE_FUNC;

    return refStateRunningRT;
}



enum REF_state refStateRunningRT(struct REF_mgr * const ref_mgr)
{
    return REF_RUNNING;
}

// EOF
