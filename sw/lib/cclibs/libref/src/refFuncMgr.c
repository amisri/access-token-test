//! @file  refFuncMgr.c
//! @brief Converter Control Function Manager library function generation functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <libref.h>


static inline void refFuncMgrSetInternalEvtFlag(struct REF_func_mgr * const fg)
{
    fg->internal_evt = 3;
}



void refFuncMgrUpdateEventRT(struct REF_func_mgr    * const fg,
                             struct REF_event const * const new_event)
{
    if(new_event != NULL)
    {
        fg->event = *new_event;
    }
    else
    {
        fg->event.abs_time.secs.abs = 0;
        fg->event.sub_sel = 0;
        fg->event.cyc_sel = 0;
        fg->event.ref_cyc_sel = 0;
        fg->event.rst_source  = REG_OPERATIONAL_RST_PARS;
    }

    // Set internal event flag for logging

    refFuncMgrSetInternalEvtFlag(fg);
}



void refFuncMgrUpdateEventTimeRT(struct REF_func_mgr * const fg,
                                 struct CC_ns_time     const new_fg_event_time)
{
    fg->event.abs_time = new_fg_event_time;

    // Set internal event flag for logging

    refFuncMgrSetInternalEvtFlag(fg);
}



void refFuncMgrUpdateParsRT(struct REF_func_mgr * const fg,
                            union FG_pars       * const new_fg_pars)
{
    fg->pars = new_fg_pars;
}



void refFuncMgrResetRT(struct REF_func_mgr * const fg,
                       union FG_pars       * const fg_pars)
{
    refFuncMgrUpdateEventRT(fg, NULL);
    fgNoneArmRT(fg_pars, NULL);
    refFuncMgrUpdateParsRT(fg, fg_pars);
}



void refFuncMgrSetFuncTimeRT(struct REF_mgr * const ref_mgr)
{
    // Set function generator time and advanced time (by ref advance) based on the active event

    if(ref_mgr->ref_state == REF_PAUSED && ref_mgr->fsm.paused.ramp_running == false)
    {
        // When not ramping in PAUSED, time is frozen at the PAUSE start time

        ref_mgr->fg.time_adv = ref_mgr->fsm.paused.fg_time_adv;
    }
    else
    {
        // If FG event is active then set FG time

        if(ref_mgr->fg.event.abs_time.secs.abs > 0)
        {
            // Calculate function generation time using the event time

            ref_mgr->fg.time = cctimeNsSubRT(ref_mgr->iter_time, ref_mgr->fg.event.abs_time);

            ref_mgr->fg.time_adv = cctimeNsAddOffsetRT(ref_mgr->fg.time, ref_mgr->reg_mgr->ref_advance_ns);

            // Adjust advanced time by the track_delay if the ILC reference is running

            if(refIlcRefRunningRT(&ref_mgr->ilc) == true)
            {
                ref_mgr->fg.time_adv = cctimeNsAddOffsetRT(ref_mgr->fg.time_adv, -ref_mgr->reg_mgr->track_delay_ns);
            }
        }
        else
        {
            // No active event so zero FG times

            ref_mgr->fg.time     = cc_zero_ns;
            ref_mgr->fg.time_adv = cc_zero_ns;
        }
    }

    // Convert FG time and time_adv from CC_ns_time to cc_float and cc_double

    ref_mgr->fg.time_fp32     = (cc_float)cctimeNsRelTimeToDoubleRT(ref_mgr->fg.time);
    ref_mgr->fg.time_adv_fp64 = cctimeNsRelTimeToDoubleRT(ref_mgr->fg.time_adv);
    ref_mgr->fg.time_adv_fp32 = (cc_float)ref_mgr->fg.time_adv_fp64;
}



void refFuncMgrSetRefRT(struct REF_mgr * const ref_mgr)
{
    if(ref_mgr->rt_ref.direct == false)
    {
        // Not using direct real-time reference so use the function generator to calculate the reference
        // Note: This can modify ref_mgr->fg.time_adv_fp64 so that it mismatches ref_mgr->fg.time_adv.

        ref_mgr->fg.status = fgFuncRT(ref_mgr->fg.pars, &ref_mgr->fg.time_adv_fp64, &ref_mgr->ref_fg);
    }
    else
    {
        // Using the direct real-time reference so no need to use the function generator

        ref_mgr->ref_fg    = 0.0F;
        ref_mgr->fg.status = FG_POST_FUNC;
    }
}



void refFuncMgrShiftFlagsRT(struct REF_mgr * ref_mgr)
{
    // Shift internal event flag to reset after two iterations

    ref_mgr->fg.internal_evt >>= 1;

    // Set start_func flag while next_start_func.func is a non-zero pointer or shift to reset after two iterations

    if(ref_mgr->event_mgr.next_start_func.func != NULL)
    {
        refFuncMgrSetFlagStartFunc(&ref_mgr->fg);
    }
    else
    {
        ref_mgr->fg.start_func >>= 1;
    }

    // Set use_arm_now flag while next_use_arm_now.func is a non-zero pointer or shift to reset after two iterations

    if(ref_mgr->event_mgr.next_use_arm_now.func != NULL)
    {
        refFuncMgrSetFlagUseArmNow(&ref_mgr->fg);
    }
    else
    {
        ref_mgr->fg.use_arm_now >>= 1;
    }
}

// EOF
