//! @file  refStateDynEco.c
//! @brief Converter Control Reference Manager library: DYN_ECO reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"


void refStateDynEcoResetRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_dyn_eco * const dyn_eco = &ref_mgr->fsm.dyn_eco;

    // Clear dyn_eco run warning after two cycle starts, to ensure it is active for a least one cycle

    if(dyn_eco->run_warning == true)
    {
        if(dyn_eco->ramp_to_final_running == true)
        {
            dyn_eco->ramp_to_final_running = false;
        }
        else
        {
            dyn_eco->run_warning = false;
        }
    }

    // Clear plep complete flag as this is used in the CYtoDE transition condition

    dyn_eco->plep_complete = false;
}



RefState * refStateDynEcoInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_dyn_eco * const dyn_eco = &ref_mgr->fsm.dyn_eco;

    // Dynamic dyn_eco starting - reset dyn_eco internal variables

    dyn_eco->plep_running          = false;
    dyn_eco->ramp_to_final_running = false;
    dyn_eco->event_abs_time        = ref_mgr->fg.event.abs_time;

    // In full dyn_eco, we may not have an active event

    if(ref_mgr->event_mgr.active.func != NULL)
    {
        // Set PLEP start time

        dyn_eco->plep_abs_time = cctimeNsAddRT(dyn_eco->event_abs_time,
                                               cctimeNsDoubleToRelTimeRT(ref_mgr->event_mgr.active.func->dyn_eco_plep_start_time));
    }

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Go to the initial_ref level at the start of DYNAMIC DYN_ECO

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     ref_mgr->reg_mgr->ref_rate,
                     ref_mgr->ref_fg_unlimited,
                     ref_mgr->event_mgr.active.func->ref_armed.fg_pars.meta.range.initial_ref,
                     REF_USE_ACC_FOR_DEC);

    // Set DYN_ECO flag in cycle status

    refCycStatusMgrSetStatusBitMaskRT(&ref_mgr->cyc_status_mgr, REF_CYC_ECO_DYN_BIT_MASK);

    return refStateDynEcoRT;
}



enum REF_state refStateDynEcoRT(struct REF_mgr * const ref_mgr)
{
    // Keep processing cycling reference events, but never clear the active event nor
    // allow for updating it with 'use armed now' events

    enum REF_active_status const event_status = refEventMgrProcessCyclingEventsRT(&ref_mgr->event_mgr,
                                                                                  ref_mgr->iter_time,
                                                                                  false,                                // clear_active
                                                                                  false);                               // allow_use_arm_now

    // If the cycle is overrun by the next cycle then set the overrun flag to return to CYCLING immediately

    if(event_status == REF_ACTIVE_UPDATED)
    {
        ref_mgr->fsm.cycling.cycle_overrun = true;

        return REF_DYN_ECO;
    }

    // Check for end of RAMP or end of PLEP

    struct REF_state_dyn_eco * const dyn_eco   = &ref_mgr->fsm.dyn_eco;
    struct REF_event_mgr     * const event_mgr = &ref_mgr->event_mgr;

    if(ref_mgr->fg.status == FG_POST_FUNC)
    {
        ref_mgr->ramp_mgr.status = FG_POST_FUNC;

        struct REF_func * const active_func = event_mgr->active.func;

        if(   dyn_eco->plep_running          == true
           || dyn_eco->ramp_to_final_running == true)
        {
            // The PLEP or ramp to final reference has finished so go back to CYCLING

            dyn_eco->plep_complete = true;
        }
        else
        {
            // The initial ramp to standby has finished so prepare to run either
            // the PLEP to connect with the main function or the RAMP to final reference.

            if(cctimeNsCmpAbsTimesRT(ref_mgr->iter_time, dyn_eco->plep_abs_time) <= 0)
            {
                // There is still time to run the PLEP so start it

                refFuncMgrUpdateEventTimeRT(&ref_mgr->fg, dyn_eco->plep_abs_time);
                refFuncMgrUpdateParsRT     (&ref_mgr->fg, &active_func->dyn_eco_plep);

                dyn_eco->plep_running = true;
                dyn_eco->run_warning  = false;
            }
            else
            {
                // There is not enough time to run the PLEP so just RAMP to the final reference and set warning flag

                refRampMgrInitRT(ref_mgr,
                                 REF_START_RAMP_NOW,
                                 ref_mgr->reg_mgr->ref_rate,
                                 ref_mgr->ref_fg_unlimited,
                                 active_func->ref_armed.fg_pars.meta.range.final_ref,
                                 REF_USE_ACC_FOR_DEC);

                dyn_eco->ramp_to_final_running = true;
                dyn_eco->run_warning           = true;
            }
        }
    }

    // Run the ramp manager unless the PLEP is running

    if(dyn_eco->plep_running == false)
    {
        refRampMgrRT(ref_mgr);
    }

    return REF_DYN_ECO;
}

// EOF
