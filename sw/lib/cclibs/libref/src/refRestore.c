//! @file  refRestore.c
//! @brief Converter Control Function Manager library reference parameter restoring functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

// Declare typedefs for pointers to parameter restoring functions

typedef void (*refRestoreFunc)(struct REF_mgr      * const,
                               uint32_t              const,
                               uint32_t              const,
                               union  FG_pars      * const,
                               enum REF_fg_par_idx *);

//! NON-RT   refRestoreFgParsList
//!
//! Prepares the restored parameters list
//!
//! This function processes a bit mask corresponding to consecutive parameters identified
//! by fg_par_idx. If the bit is 1, the parameter was restored and the index for that parameter
//! is added to the list pointed to by fg_pars_idx. A parameter is only restored if its value
//! is different.
//!
//! @param[in,out]  fg_pars_idx             Pointer to array to receive the parameter indexes of all the restored parameters.
//! @param[in]      fg_par_idx              Index of first parameter, corresponding to the LSB in par_restored_bit_mask.
//! @param[in]      par_restored_bit_mask   Bit mask indicating which (consecutive) parameters were restored.

static void refRestoreFgParsList(enum REF_fg_par_idx * fg_pars_idx,
                                 enum REF_fg_par_idx   fg_par_idx,
                                 uint32_t              par_restored_bit_mask)
{
    while(par_restored_bit_mask > 0)
    {
        if((par_restored_bit_mask & 0x1) != 0)
        {
            *(fg_pars_idx++) = fg_par_idx;
        }

        par_restored_bit_mask >>= 1;
        fg_par_idx++;
    }
}


// NON-RT   refRestoreNone

static void refRestoreNone(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
}


// NON-RT   refRestoreRamp

static void refRestoreRamp(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_RAMP_INITIAL_REF,
                         fgRampRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_FINAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_ACCELERATION),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_LINEAR_RATE),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, RAMP_DECELERATION)));
}


// NON-RT   refRestorePulse

static void refRestorePulse(struct REF_mgr      * const ref_mgr,
                            uint32_t              const sub_sel,
                            uint32_t              const cyc_sel,
                            union  FG_pars      * const fg_pars,
                            enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_PULSE_REF,
                         fgPulseRestorePars(fg_pars,
                                            refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PULSE_REF),
                                            refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PULSE_DURATION)));
}


// NON-RT   refRestorePlep

static void refRestorePlep(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_PLEP_INITIAL_REF,
                         fgPlepRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_FINAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_ACCELERATION),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_LINEAR_RATE),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_EXP_TC),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PLEP_EXP_FINAL)));
}


// NON-RT   refRestorePppl

static void refRestorePppl(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_PPPL_INITIAL_REF,
                         fgPpplRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION1),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION2),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION3),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE2),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE4),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_REF4),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_DURATION4),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION1_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION2_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_ACCELERATION3_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE2_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_RATE4_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_REF4_NUM_ELS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PPPL_DURATION4_NUM_ELS)));
}


// NON-RT   refRestoreCubexp

static void refRestoreCubexp(struct REF_mgr      * const ref_mgr,
                             uint32_t              const sub_sel,
                             uint32_t              const cyc_sel,
                             union  FG_pars      * const fg_pars,
                             enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_CUBEXP_REF,
                         fgCubexpRestorePars(fg_pars,
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_REF),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_RATE),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_REF_NUM_ELS),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_RATE_NUM_ELS),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, CUBEXP_TIME_NUM_ELS)));
}


// NON-RT   refRestoreTable

static void refRestoreTable(struct REF_mgr      * const ref_mgr,
                            uint32_t              const sub_sel,
                            uint32_t              const cyc_sel,
                            union  FG_pars      * const fg_pars,
                            enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_TABLE_FUNCTION,
                         fgTableRestorePars(fg_pars,
                                             ref_mgr->max_table_points,
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TABLE_FUNCTION),
                                             refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TABLE_FUNCTION_NUM_ELS)));
}


// NON-RT   refRestoreTrim

static void refRestoreTrim(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_TRIM_INITIAL_REF,
                         fgTrimRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_FINAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TRIM_DURATION)));
}


// NON-RT   refRestoreTest

static void refRestoreTest(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_TEST_INITIAL_REF,
                         fgTestRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_AMPLITUDE_PP),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_NUM_PERIODS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD_ITERS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_PERIOD),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_WINDOW),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, TEST_EXP_DECAY)));
}


// NON-RT   refRestorePrbs

static void refRestorePrbs(struct REF_mgr      * const ref_mgr,
                           uint32_t              const sub_sel,
                           uint32_t              const cyc_sel,
                           union  FG_pars      * const fg_pars,
                           enum REF_fg_par_idx *       fg_pars_idx)
{
    refRestoreFgParsList(fg_pars_idx,
                         REF_FG_PAR_PRBS_INITIAL_REF,
                         fgPrbsRestorePars(fg_pars,
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_INITIAL_REF),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_AMPLITUDE_PP),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_PERIOD_ITERS),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_NUM_SEQUENCES),
                                           refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, PRBS_K)));
}


// NON-RT   refRestore

void refRestore(struct REF_mgr      * const ref_mgr,
                uint32_t              const sub_sel,
                uint32_t              const cyc_sel,
                enum REF_fg_par_idx         fg_pars_idx[REF_FG_PAR_MAX_PER_GROUP])
{
    // Initialize array of pointers to reference arming functions - IMPORTANT: this must be in the same order as enum FG_type

    static refRestoreFunc const restore_func[] =
    {
        refRestoreNone  ,          // NONE
        refRestoreNone  ,          // ZERO
        refRestoreRamp  ,          // RAMP
        refRestorePulse ,          // PULSE
        refRestorePlep  ,          // PLEP
        refRestorePppl  ,          // PPPL
        refRestoreCubexp,          // CUBEXP
        refRestoreTable ,          // TABLE
        refRestoreTrim  ,          // LTRIM
        refRestoreTrim  ,          // CTRIM
        refRestoreTest  ,          // STEPS
        refRestoreTest  ,          // SQUARE
        refRestoreTest  ,          // SINE
        refRestoreTest  ,          // COSINE
        refRestoreTest  ,          // OFFCOS
        refRestorePrbs  ,          // PRBS
    };

    CC_STATIC_ASSERT(CC_ARRAY_LEN(restore_func) == FG_NUM_FUNCS, size_of_restore_fun_array_doesnt_correspond_to_FGC_NUM_FUNCS);

    // Reset the parameter indexes array immediately in case we return with an error

    uint32_t i;

    for(i = 0 ; i < REF_FG_PAR_MAX_PER_GROUP ; i++)
    {
        fg_pars_idx[i] = REF_FG_PAR_NULL;
    }

    // Reset transaction last FG parameter index

    refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, TRANSACTION_LAST_FG_PAR_INDEX) = REF_FG_PAR_NULL;

    // Get pointers to and fg_error structures

    struct REF_armed * const ref_armed = &ref_mgr->ref_armed[cyc_sel + sub_sel * ref_mgr->num_cyc_sels];

    // Lock mutex if required, to protect against running at the same time as refEvent functions

    if(ref_mgr->mutex)
    {
        // Use lock callback defined for libreg

        ref_mgr->reg_mgr->mutex_lock(ref_mgr->mutex);
    }

    // If a function is armed then restore the parameters to matched the armed function

    if(ref_armed->fg_pars.meta.type != FG_NONE)
    {
        // Restore FG_TYPE if they are different

        enum FG_type * const fg_type_ptr = (enum FG_type *)refMgrFgParPointerSubCyc(ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE);

        if(*fg_type_ptr != ref_armed->fg_pars.meta.type)
        {
            *fg_type_ptr = ref_armed->fg_pars.meta.type;

            *(fg_pars_idx++) = REF_FG_PAR_REF_FG_TYPE;
        }

        // Restore function related parameters that are changed compared to the armed function

        restore_func[ref_armed->fg_pars.meta.type](ref_mgr, sub_sel, cyc_sel, &ref_armed->fg_pars, fg_pars_idx);
    }
    else
    {
        // No function was armed so reset the failing function type to NONE

        refMgrFgParValueSubCyc(ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE) = FG_NONE;
    }

    // Unlock mutex if in use

    if(ref_mgr->mutex)
    {
        // Use unlock callback defined for libreg

        (*ref_mgr->reg_mgr->mutex_unlock)(ref_mgr->mutex);
    }
}

// EOF
