//! @file  refStateToIdle.c
//! @brief Converter Control Reference Manager library: TO_IDLE reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStateToIdleInitRT(struct REF_mgr * const ref_mgr)
{
    // Change regulation mode if required

    ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, ref_mgr->iter_cache.mode_reg_mode, true);     // use_average_v_ref

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // The final reference is the initial reference, however, for a unipolar converter,
    // clip the reference to the STANDBY limit.

    cc_float const final_ref = refArmClipRefRT(ref_mgr, ref_mgr->ref_fg_unlimited);

    refEventMgrClearActiveRT(&ref_mgr->event_mgr);

    // Initialize ramp to final reference

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     refMgrInitialRate(ref_mgr),
                     ref_mgr->ref_fg_unlimited,
                     final_ref,
                     REF_DONT_USE_ACC_FOR_DEC);

    // Reset force_openloop flag

    ref_mgr->force_openloop = false;

    return refStateToIdleRT;
}



enum REF_state refStateToIdleRT(struct REF_mgr * const ref_mgr)
{
    refRampMgrRT(ref_mgr);

    return REF_TO_IDLE;
}

// EOF
