//! @file  refStateCycling.c
//! @brief Converter Control Reference Manager library: CYCLING reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"


RefState * refStateCyclingInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_cycling * const cycling  = &ref_mgr->fsm.cycling;
    struct REF_state_dyn_eco * const dyn_eco  = &ref_mgr->fsm.dyn_eco;

    enum REF_state const prev_ref_state = ref_mgr->ref_state;

    if(cycling->cycle_overrun == false)
    {
        switch(prev_ref_state)
        {
            case REF_TO_CYCLING:

                break;

            case REF_DYN_ECO:

                CC_ASSERT(cycling->status == FG_DURING_FUNC);

                // Only restore the function if the DYN_ECO PLEP was successfully played

                if(dyn_eco->ramp_to_final_running == false)
                {
                    // Restore function parameters and event time from before DYN_ECO

                    refFuncMgrUpdateEventTimeRT(&ref_mgr->fg, dyn_eco->event_abs_time);
                    refFuncMgrUpdateParsRT     (&ref_mgr->fg, &ref_mgr->event_mgr.active.func->ref_armed.fg_pars);

                    // Reset the function generation status - act like the function
                    // generator was playing the main function

                    ref_mgr->fg.status = FG_DURING_FUNC;
                }

                break;

            case REF_PAUSED:
            {
                CC_ASSERT(cycling->status == FG_DURING_FUNC);

                struct REF_state_paused * const paused = &ref_mgr->fsm.paused;

                // Advance cached function run event time by the PAUSED duration

                paused->cache.fg_event.abs_time = cctimeNsAddRT(paused->cache.fg_event.abs_time, ref_mgr->event_mgr.unpause_event.time);

                // Restore function parameters and event time from before PAUSED

                refFuncMgrUpdateEventRT(&ref_mgr->fg, &paused->cache.fg_event);
                refFuncMgrUpdateParsRT (&ref_mgr->fg, &ref_mgr->event_mgr.active.func->ref_armed.fg_pars);

                // Reset the function generation status - act like the function
                // generator was playing the main function

                ref_mgr->fg.status = FG_DURING_FUNC;

                break;
            }

            case REF_POL_SWITCHING:

                // Polarity switch is allowed only as part of pre-func. This means that
                // the ramp manager should be running and cycling status should be pre-func.
                // In case pol switch extended over the main function, or the active event
                // was altered in any way (next 'start func' or 'use armed now'), we should end up
                // in TO_CYCLING

                CC_ASSERT(cycling->status          == FG_PRE_FUNC);
                CC_ASSERT(ref_mgr->ramp_mgr.status == FG_DURING_FUNC);
                break;

            default:

                // All other previous states should be impossible

                CC_ASSERT(false);
                break;

        }
    }

    // Except if returning from POL_SWITCHING, we should cancel any lingering pre-function or ramp manager ramp

    if(prev_ref_state != REF_POL_SWITCHING)
    {
        ref_mgr->pre_func_mgr.seq_finished = true;

        // Terminate any ramps that may be running

        refRampMgrTerminateRT(ref_mgr);
    }

    // Reset the dynamic economy request flag

    refMgrParValue(ref_mgr, ECONOMY_DYNAMIC) = CC_DISABLED;

    return refStateCyclingRT;
}


//! Check if the new main function polarity is compatible with the polarity switch position (if present)
//!
//! @param[in,out]  ref_mgr   Pointer to REF_mgr structure
//!
//! @retval         true      Main function polarity is compatible with polarity switch position
//! @retval         false     Main function polarity is not compatible with polarity switch position

static bool refStateCyclingVerifyPolarityRT(struct REF_mgr * const ref_mgr)
{
    if(polswitchVarValue(ref_mgr->polswitch_mgr, TIMEOUT) > 0)
    {
        enum FG_func_pol const func_polarity = ref_mgr->event_mgr.active.func->ref_armed.fg_pars.meta.polarity;

        if(func_polarity != FG_FUNC_POL_ZERO)
        {
            if(polswitchIsNegative(ref_mgr->polswitch_mgr) == true)
            {
                return (func_polarity == FG_FUNC_POL_NEGATIVE || func_polarity == FG_FUNC_POL_ZERO);
            }
            else
            {
                return (func_polarity == FG_FUNC_POL_POSITIVE || func_polarity == FG_FUNC_POL_ZERO);
            }
        }
    }

    return true;
}



enum REF_state refStateCyclingRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_cycling * const cycling     = &ref_mgr->fsm.cycling;
    struct REF_func          *       active_func = ref_mgr->event_mgr.active.func;
    struct REG_mgr           * const reg_mgr     = ref_mgr->reg_mgr;
    enum   REF_active_status         event_status;

    // Increment CYCLING counter

    cycling->counter++;

    // Prepare event status

    if(cycling->cycle_overrun == true)
    {
        // If returning from PAUSED or DYN_ECO following an overrun, then the new function
        // has already been made active so treat the overrun

        event_status = REF_ACTIVE_UPDATED;
    }
    else
    {
        bool     const clear_active   = (cycling->status == FG_DURING_FUNC && ref_mgr->fg.status == FG_POST_FUNC);
        cc_float const time_remaining = (clear_active == true || active_func == NULL)
                                      ? 0.0F
                                      : active_func->ref_armed.fg_pars.meta.time.end - ref_mgr->fg.time_adv_fp32;

        // If function has finished then cache the final_rate from the function meta data for refStateToCyclingInitRT

        if(clear_active == true)
        {
            cycling->final_rate = active_func->ref_armed.fg_pars.meta.range.final_rate;
        }

        // Process events

        event_status = refEventMgrProcessCyclingEventsRT(&ref_mgr->event_mgr,
                                                         ref_mgr->iter_time,
                                                         clear_active,                         // clear_active
                                                         true);                                // allow_use_arm_now

        // Update local copy of active_func

        active_func = ref_mgr->event_mgr.active.func;

        // Set overrun flag if the new cycle started with more than two periods left in the previous cycles' function

        cycling->cycle_overrun =    event_status == REF_ACTIVE_UPDATED
                                 && time_remaining > (2.0F * reg_mgr->reg_period);
    }

    // Process the event status: REF_ACTIVE_FUNC_UPDATED, REF_ACTIVE_UPDATED, REF_ACTIVE_EMPTY or REF_ACTIVE_NOT_UPDATED

    if(event_status == REF_ACTIVE_FUNC_UPDATED)
    {
        // USE_ARM_NOW function was activated. Update during pre-function is not possible.

        if(cycling->status == FG_DURING_FUNC)
        {
            refFuncMgrUpdateParsRT(&ref_mgr->fg, &active_func->ref_armed.fg_pars);
        }
    }
    else if(event_status != REF_ACTIVE_NOT_UPDATED)
    {
        // REF_ACTIVE_UPDATED or REF_ACTIVE_EMPTY

        // Latch overrun flag in the cycle status

        if(cycling->cycle_overrun)
        {
            refCycStatusMgrSetStatusBitMaskRT(&ref_mgr->cyc_status_mgr, REF_CYC_WAS_OVERRUN_BIT_MASK);

            cycling->cycle_overrun = false;
        }

        // Finish the status for the previous cycle - finish_early flag is set to false

        cycling->warning = refCycStatusMgrFinishRT(ref_mgr, false);

        // Check if FULL_ECO request is active

        ref_mgr->fsm.full_eco.run = refStateFullEcoRunRT(ref_mgr);

        // Return immediately if FULL_ECO request is active or there was no pending event to take over

        if(ref_mgr->fsm.full_eco.run == true || event_status == REF_ACTIVE_EMPTY)
        {
            cycling->status = FG_POST_FUNC;

            // Stop processing the ILC for the previous cycle

            refIlcStopRT(&ref_mgr->ilc);

            return REF_CYCLING;
        }

        // event_status is REF_ACTIVE_UPDATED - prepare to run new cycle by setting reg_mode and the FG event and time

        // Activate the next cyc_status

        refCycStatusMgrActivateRT(&ref_mgr->cyc_status_mgr);

        // Set reg mode from the new active armed function

        ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, active_func->ref_armed.reg_mode, false);

        refFuncMgrUpdateEventRT(&ref_mgr->fg, ref_mgr->event_mgr.active.event);
        refFuncMgrSetFuncTimeRT(ref_mgr);

        // Stop processing the ILC for the previous cycle

        refIlcStopRT(&ref_mgr->ilc);

        // Try to start the pre-function unless the main function time has already arrived

        if(refPreFuncMgrStartRT(ref_mgr) == false)
        {
            // No time for the pre-function so report NO_PRE_FUNC in cycle status

            refCycStatusMgrSetStatusBitMaskRT(&ref_mgr->cyc_status_mgr, REF_CYC_NO_PRE_FUNC_BIT_MASK);
        }

        // Active event has changed - it's the start of a new cycle so reset
        // DYN_ECO warnings on the second cycle start and set cycling counter to 1

        refStateDynEcoResetRT(ref_mgr);

        ref_mgr->fsm.to_off.to_tc_transition_counter = 0;
        cycling->counter = 1;
        cycling->status  = FG_PRE_FUNC;
    }

    // If running the pre-function then check if the main function start time has arrived

    if(cycling->status == FG_PRE_FUNC)
    {
        // Check if it is time to start the main function

        if(refStateCyclingCheckStartMainFunctionRT(ref_mgr))
        {
            // Main function start time has arrived so terminate pre-function in case it is still running

            refPreFuncMgrEndRT(ref_mgr);

            // Check if main function polarity is compatible with polarity switch position

            if(refStateCyclingVerifyPolarityRT(ref_mgr) == true)
            {
                // Polarity switch position is compatible so activate the main function

                refFuncMgrUpdateParsRT(&ref_mgr->fg,  &active_func->ref_armed.fg_pars);

                // Zero max regulation error before running the main function

                reg_mgr->reg_signal->reg_err.max_abs_err = 0.0F;

                // Reset the dynamic economy request flag

                refMgrParValue(ref_mgr, ECONOMY_DYNAMIC) = CC_DISABLED;

                // Move to DURING_FUNC cycling status

                cycling->status = FG_DURING_FUNC;
            }
            else
            {
                // Function polarity doesn't match the polarity switch position so
                // we cannot play the reference function so end the function immediately.

                cycling->status = FG_POST_FUNC;
            }
        }
        else
        {
            // Main function time has not arrived so continue the pre-function

            refPreFuncMgrRT(ref_mgr);
        }
    }
    else if(reg_mgr->max_abs_reg_err > 0.0)
    {
        // Latch the max abs regulation error if it is non-zero

        refCycStatusMgrSetMaxAbsRegErrRT(&ref_mgr->cyc_status_mgr, reg_mgr->max_abs_reg_err);
    }

    return REF_CYCLING;
}



bool refStateCyclingCheckStartMainFunctionRT(struct REF_mgr * const ref_mgr)
{
        struct CC_ns_time const start_main_function_time = { { .rel = -1 }, CCTIME_NS_PER_S - ref_mgr->reg_mgr->reg_period_ns };

        return cctimeNsCmpRelTimesRT(ref_mgr->fg.time_adv, start_main_function_time) >= 0;
}

// EOF
