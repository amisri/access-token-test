//! @file  refStatePolSwitching.c
//! @brief Converter Control Reference Manager library: POL_SWITCHING reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStatePolSwitchingInitRT(struct REF_mgr * const ref_mgr)
{
    // Arm zero reference on first call if ramp manager is not running

    if(ref_mgr->ramp_mgr.status != FG_DURING_FUNC)
    {
        fgZeroArmRT(&ref_mgr->fsm.fg_pars, NULL);
        refFuncMgrUpdateParsRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);
    }

    ref_mgr->fsm.pol_switching.abandon_cycle = false;

    return refStatePolSwitchingRT;
}



enum REF_state refStatePolSwitchingRT(struct REF_mgr * const ref_mgr)
{
    if(ref_mgr->iter_cache.ref_mode == REF_CYCLING)
    {
        // Keep processing cycling reference events, but never clear the active event nor
        // allow for updating it with 'use armed now' events

        enum REF_active_status const event_status = refEventMgrProcessCyclingEventsRT(&ref_mgr->event_mgr,
                                                                                      ref_mgr->iter_time,
                                                                                      false,                                // clear_active
                                                                                      false);                               // allow_use_arm_now

        // Polarity switch movement should finish before the main function starts. If it's not the case,
        // we flag that the current cycle should be abandoned once the movement finishes. This will
        // result in falling back to TO_CYCLING.

        if(   event_status != REF_ACTIVE_NOT_UPDATED
           || refStateCyclingCheckStartMainFunctionRT(ref_mgr))
        {
            ref_mgr->fsm.pol_switching.abandon_cycle = true;
        }
    }

    return REF_POL_SWITCHING;
}

// EOF
