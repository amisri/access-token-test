//! @file  refState.c
//! @brief Converter Control Reference Manager library - Reference state machine functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libref.h"


// Transition condition functions from anonymous states


static RefStateInit * XXtoOF(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.pc_state != REF_PC_ON
       || (ref_mgr->latched_faults & ~refMgrParValue(ref_mgr, MODE_TO_OFF_FAULTS)) != 0)
    {
        return refStateOffInitRT;
    }

    return NULL;
}



static RefStateInit * XXtoTO(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->latched_faults != 0
       || ref_mgr->iter_cache.ref_mode == REF_OFF)
    {
        return refStateToOffInitRT;
    }

    return NULL;
}



static RefStateInit * XXtoTS(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_STANDBY
       && ref_mgr->latched_faults == 0)
    {
        return refStateToStandbyInitRT;
    }

    return NULL;
}



static RefStateInit * XXtoTC(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_CYCLING
       && ref_mgr->latched_faults == 0)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * XXtoTI(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_IDLE
       && ref_mgr->latched_faults == 0)
    {
        return refStateToIdleInitRT;
    }

    return NULL;
}



static RefStateInit * XXtoDT(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_DIRECT
       && ref_mgr->latched_faults == 0)
    {
        return refStateDirectInitRT;
    }

    return NULL;
}

// Transition condition functions from OFF state


static RefStateInit * OFtoDT(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_DIRECT
       && ref_mgr->fsm.off.init_rst_timer == REF_OFF_TIMER_READY)
    {
        return refStateDirectInitRT;
    }

    return NULL;
}



static RefStateInit * OFtoTS(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_STANDBY
       && ref_mgr->fsm.off.init_rst_timer == REF_OFF_TIMER_READY)
    {
        return refStateToStandbyInitRT;
    }

    return NULL;
}



static RefStateInit * OFtoTC(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_CYCLING
       && ref_mgr->fsm.off.init_rst_timer == REF_OFF_TIMER_READY)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * OFtoTI(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_IDLE
       && ref_mgr->fsm.off.init_rst_timer == REF_OFF_TIMER_READY)
    {
        return refStateToIdleInitRT;
    }

    return NULL;
}


// Transition condition functions from POL_SWITCHING state


static RefStateInit * PStoOF(struct REF_mgr const * const ref_mgr)
{
    if(   polswitchVarValue(ref_mgr->polswitch_mgr, LATCHED_FAULTS) != 0
       || (   ref_mgr->iter_cache.ref_mode == REF_OFF
           && polswitchVarValue(ref_mgr->polswitch_mgr,STATE) != POLSWITCH_STATE_MOVING))
    {
        return refStateOffInitRT;
    }

    return NULL;
}



static RefStateInit * PStoTC(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode  == REF_CYCLING
       && polswitchVarValue(ref_mgr->polswitch_mgr,STATE) != POLSWITCH_STATE_MOVING
       && ref_mgr->fsm.pol_switching.abandon_cycle == true)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * PStoCY(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_CYCLING
       && polswitchVarValue(ref_mgr->polswitch_mgr,STATE) != POLSWITCH_STATE_MOVING)
    {
        return refStateCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * PStoDT(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->iter_cache.ref_mode == REF_DIRECT
       && polswitchVarValue(ref_mgr->polswitch_mgr,STATE) != POLSWITCH_STATE_MOVING)
    {
        return refStateDirectInitRT;
    }

    return NULL;
}


// Transition condition function from TO_OFF state


static RefStateInit * TOtoOF(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->fsm.to_off.completed == true)
    {
        return refStateOffInitRT;
    }

    return NULL;
}


// Transition condition function from TO_STANDBY state


static RefStateInit * TStoSB(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ramp_mgr.status == FG_POST_FUNC)
    {
        return refStateStandbyInitRT;
    }

    return NULL;
}


// Transition condition functions from TO_CYCLING state


static RefStateInit * TCtoFE(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->fsm.full_eco.run == true)
    {
        return refStateFullEcoInitRT;
    }

    return NULL;
}



static RefStateInit * TCtoCY(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->fsm.full_eco.once == false
       && refEventMgrNextIsPendingRT(&ref_mgr->event_mgr, REF_EVENT_START_FUNC))
    {
        return refStateCyclingInitRT;
    }

    return NULL;
}


// Transition condition function from TO_IDLE state


static RefStateInit * TItoIL(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ramp_mgr.status == FG_POST_FUNC)
    {
        return refStateIdleInitRT;
    }

    return NULL;
}


// Transition condition function from DIRECT state


static RefStateInit * DTtoPS(struct REF_mgr const * const ref_mgr)
{
    if(polswitchVarValue(ref_mgr->polswitch_mgr,STATE) == POLSWITCH_STATE_MOVING)
    {
        return refStatePolSwitchingInitRT;
    }

    return NULL;
}


// Transition condition function from STANDBY state


static RefStateInit * SBtoTS(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->iter_cache.reg_mode != ref_mgr->iter_cache.mode_reg_mode)
    {
        return refStateToStandbyInitRT;
    }

    return NULL;
}


// Transition condition functions from CYCLING state


static RefStateInit * CYtoPS(struct REF_mgr const * const ref_mgr)
{
    if(polswitchVarValue(ref_mgr->polswitch_mgr,STATE) == POLSWITCH_STATE_MOVING)
    {
        return refStatePolSwitchingInitRT;
    }

    return NULL;
}



static RefStateInit * CYtoTC(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->fsm.cycling.status == FG_POST_FUNC
       || ref_mgr->event_mgr.abort    == true)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * CYtoDE(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->fsm.cycling.status == FG_DURING_FUNC
       && ref_mgr->event_mgr.active.func->dyn_eco_plep_start_time != 0.0F
       && ref_mgr->fsm.dyn_eco.plep_complete == false
       && refMgrParValue(ref_mgr, ECONOMY_DYNAMIC) == CC_ENABLED)
    {
        return refStateDynEcoInitRT;
    }

    return NULL;
}



static RefStateInit * CYtoFE(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->fsm.full_eco.run == true)
    {
        return refStateFullEcoInitRT;
    }

    return NULL;
}



static RefStateInit * CYtoPD(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->fsm.cycling.status == FG_DURING_FUNC
       && (   ref_mgr->event_mgr.pause_event.is_active == true
           || ref_mgr->event_mgr.internal_pause_event.is_active == true))
    {
        return refStatePausedInitRT;
    }

    return NULL;
}


// Transition condition functions from DYN_ECO state


static RefStateInit * DEtoTC(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->event_mgr.abort == true)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * DEtoCY(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->fsm.dyn_eco.plep_complete == true
       || ref_mgr->fsm.cycling.cycle_overrun == true)
    {
        return refStateCyclingInitRT;
    }

    return NULL;
}


// Transition condition functions from FULL_ECO state


static RefStateInit * FEtoTC(struct REF_mgr const * const ref_mgr)
{
    if(   refMgrParValue(ref_mgr, ECONOMY_FULL) == CC_DISABLED
       && ref_mgr->fsm.full_eco.once == false)
    {
        return refStateToCyclingInitRT;
    }

    return NULL;
}



// Transition condition function from PAUSED state


static RefStateInit * PDtoTC(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->event_mgr.abort == true)
    {
        return refStateCyclingInitRT;
    }

    return NULL;
}



static RefStateInit * PDtoCY(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->event_mgr.unpause_event.is_active == true
       || ref_mgr->fsm.cycling.cycle_overrun         == true)
    {
        return refStateCyclingInitRT;
    }

    return NULL;
}


// Transition condition function from IDLE state


static RefStateInit * ILtoAR(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ref_armed->fg_pars.meta.type != FG_NONE)
    {
        return refStateArmedInitRT;
    }

    return NULL;
}



static RefStateInit * ILtoTI(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->iter_cache.reg_mode != ref_mgr->iter_cache.mode_reg_mode)
    {
        return refStateToIdleInitRT;
    }

    return NULL;
}



// Transition condition functions from ARMED state


static RefStateInit * ARtoIL(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ref_armed->fg_pars.meta.type == FG_NONE)
    {
        return refStateIdleInitRT;
    }

    return NULL;
}



static RefStateInit * ARtoRN(struct REF_mgr const * const ref_mgr)
{
    if(refEventMgrNextIsPendingRT(&ref_mgr->event_mgr, REF_EVENT_RUN) == true)
    {
        return refStateRunningInitRT;
    }

    return NULL;
}


// Transition condition functions from RUNNING state


static RefStateInit * RNtoIL(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->fg.status == FG_POST_FUNC)
    {
        return refStateIdleInitRT;
    }

    return NULL;
}



static RefStateInit * RNtoTI(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->event_mgr.abort == true)
    {
        return refStateToIdleInitRT;
    }

    return NULL;
}

// State transition functions array : called in priority order, from left to right.

// The initialization order of the transitions array must match the enum REF_state
// constant values from refConsts.h.  This is checked with these static assertions.

CC_STATIC_ASSERT(REF_OFF           ==  0, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_POL_SWITCHING ==  1, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_TO_OFF        ==  2, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_TO_STANDBY    ==  3, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_TO_CYCLING    ==  4, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_TO_IDLE       ==  5, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_DIRECT        ==  6, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_STANDBY       ==  7, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_CYCLING       ==  8, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_DYN_ECO       ==  9, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_FULL_ECO      == 10, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_PAUSED        == 11, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_IDLE          == 12, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_ARMED         == 13, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_RUNNING       == 14, Unexpected_enum_REF_state_constant_value);
CC_STATIC_ASSERT(REF_NUM_STATES    == 15, Unexpected_enum_REF_state_constant_value);

#define REF_MAX_TRANSITIONS    10      //!< Maximum transitions from a state - currently CY state has the most

static RefTransition * const transitions[][REF_MAX_TRANSITIONS + 1] =
{
    /* OF */ {                                                       OFtoTS , OFtoTC , OFtoTI , OFtoDT ,          NULL },
    /* PS */ {                                                       PStoOF , PStoTC , PStoCY , PStoDT ,          NULL },
    /* TO */ { XXtoOF,           XXtoTS , XXtoTC , XXtoTI , XXtoDT , TOtoOF ,                                     NULL },
    /* TS */ { XXtoOF , XXtoTO ,          XXtoTC , XXtoTI , XXtoDT , TStoSB ,                                     NULL },
    /* TC */ { XXtoOF , XXtoTO , XXtoTS ,          XXtoTI , XXtoDT , TCtoFE , TCtoCY ,                            NULL },
    /* TI */ { XXtoOF , XXtoTO , XXtoTS , XXtoTC ,          XXtoDT , TItoIL ,                                     NULL },
    /* DT */ { XXtoOF , XXtoTO , XXtoTS , XXtoTC , XXtoTI ,          DTtoPS ,                                     NULL },
    /* SB */ { XXtoOF , XXtoTO ,          XXtoTC , XXtoTI , XXtoDT , SBtoTS ,                                     NULL },
    /* CY */ { XXtoOF , XXtoTO , XXtoTS ,          XXtoTI , XXtoDT , CYtoPS , CYtoFE , CYtoTC , CYtoPD , CYtoDE , NULL },
    /* DE */ { XXtoOF , XXtoTO , XXtoTS ,          XXtoTI , XXtoDT , DEtoTC , DEtoCY ,                            NULL },
    /* FE */ { XXtoOF , XXtoTO , XXtoTS ,          XXtoTI , XXtoDT , FEtoTC ,                                     NULL },
    /* PD */ { XXtoOF , XXtoTO , XXtoTS ,          XXtoTI , XXtoDT , PDtoTC,  PDtoCY ,                            NULL },
    /* IL */ { XXtoOF , XXtoTO , XXtoTS , XXtoTC ,          XXtoDT , ILtoAR , ILtoTI ,                            NULL },
    /* AR */ { XXtoOF , XXtoTO , XXtoTS , XXtoTC ,          XXtoDT , ARtoIL , ARtoRN ,                            NULL },
    /* RN */ { XXtoOF , XXtoTO , XXtoTS , XXtoTC ,          XXtoDT , RNtoIL , RNtoTI ,                            NULL },
};



void refStateRT(struct REF_mgr * const ref_mgr)
{
    RefStateInit          * next_state_init = NULL;
    RefTransition         * transition;
    RefTransition * const * state_transitions = &transitions[ref_mgr->ref_state][0];

    // Call transition condition functions for the current state, testing each condition in priority order

    while(   (transition      = *(state_transitions++)) != NULL
          && (next_state_init = (*transition)(ref_mgr)) == NULL)
    {
    }

    // If a transition condition is true then it returns the pointer to the new state's initialization function

    if(next_state_init != NULL)
    {
        // Run the next state's initialization function (it returns a pointer to the next state's function).
        // Note: ref_mgr->ref_state still contains index of previous state so the next state can know the previous state.

        RefState * const next_state_func = next_state_init(ref_mgr);

        // Run the new state's function (it returns the index of the new state)

        ref_mgr->ref_state = next_state_func(ref_mgr);

        // Create the state mask from the state index and save the pointer to the next state function

        ref_mgr->ref_state_mask = 1 << ref_mgr->ref_state;
        ref_mgr->ref_state_func = next_state_func;
    }
    else
    {
        // No transition condition is true so stay in the current state and run the state function

        ref_mgr->ref_state_func(ref_mgr);
    }

    // Increment the free-running state counter - this is a service to the application to allow a background
    // thread to wait for the state machine to run

    ref_mgr->ref_state_counter++;
}

// EOF
