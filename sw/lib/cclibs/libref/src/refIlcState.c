//! @file  refIlc.c
//! @brief Converter Control Reference Manager library - Iterative Learning Controller functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

// Typedefs

typedef enum REF_ilc_state RefIlcState     (struct REF_mgr       * const ref_mgr);     //!< Type for ILC state machine state function
typedef RefIlcState      * RefIlcTransition(struct REF_mgr const * const ref_mgr);     //!< Type for reference ILC state machine transition condition function

// Constants

#define ILC_TO_FAILED   (REF_ILC_RMS_ERR_FAILED_BIT_MASK|REF_ILC_RMS_V_REF_RATE_FAILED_BIT_MASK|REF_ILC_L_FUNC_LEN_EVEN_BIT_MASK|REF_ILC_REG_PERIOD_INVALID_BIT_MASK)

// State Functions

// NON-RT   refIlcStateDisabled

static enum REF_ilc_state refIlcStateDisabled(struct REF_mgr * const ref_mgr)
{
    ccprintf("Entered\n");

    // Reset ref and err signals for logging

    ref_mgr->ilc.logging.err = 0.0F;
    ref_mgr->ilc.logging.ref = 0.0F;

    // Reset flags

    ref_mgr->ilc.status         = 0;
    ref_mgr->ilc.failed         = false;
    ref_mgr->ilc.failed_cyc_sel = 0;

    return REF_ILC_DISABLED;
}


// NON-RT   refIlcStateStandby

static enum REF_ilc_state refIlcStateStandby(struct REF_mgr * const ref_mgr)
{
    ccprintf("Entered\n");

    // Clear the reset flag and delayed reset_type

    ref_mgr->ilc.reset = false;

    // Full reset the cycle data for all cycle selectors

    refIlcResetData(ref_mgr, 0, REF_ILC_RESET_FULL);

    return REF_ILC_STANDBY;
}


// NON-RT   refIlcStateToCycling

static enum REF_ilc_state refIlcStateToCycling(struct REF_mgr * const ref_mgr)
{
        ccprintf("\n  err.iter_counter=%u  err.idx=%u  l.control.idx=%u  q.control.idx=%u  ref.control.idx=%u\n"
                , ref_mgr->ilc.err.iter_counter
                , ref_mgr->ilc.err.index
                , ref_mgr->ilc.l.control.index
                , ref_mgr->ilc.q.control.index
                , ref_mgr->ilc.ref.control.index
                );

    // Reset the rt_status flags

    ref_mgr->ilc.rt_status = 0;

   // Abandon the cycle if less than two ILC ref samples were calculated or if the ref state machine is no longer in a cycling state

    if(ref_mgr->ilc.q.control.index < 2 || (ref_mgr->ref_state_mask & REF_ILC_CYCLING_STATES) == 0)
    {
        return REF_ILC_TO_CYCLING;
    }

    // Calculate the RMS error and v_ref_rate for the cycle that just ended

    cc_float const inv_iter_counter = 1.0F / (cc_float)ref_mgr->ilc.err.iter_counter;
    cc_float const rms_err          = sqrtf(ref_mgr->ilc.err.acc_ilc_err_2    * inv_iter_counter);
    cc_float const rms_v_ref_rate   = sqrtf(ref_mgr->ilc.err.acc_v_ref_rate_2 * inv_iter_counter);

    // Save RMS error after the first cycle and then use it as the upper limit for the RMS error from then on

    uint32_t const cyc_sel         = ref_mgr->ilc.cyc_sel;
    cc_float       initial_rms_err = *refIlcCycDataInitialRmsErr(&ref_mgr->ilc, cyc_sel);

    if(initial_rms_err == 0.0F)
    {
        initial_rms_err = rms_err;

        *refIlcCycDataInitialRmsErr(&ref_mgr->ilc, cyc_sel) = initial_rms_err;
    }
    else if(rms_err > initial_rms_err)
    {
        ref_mgr->ilc.status |= REF_ILC_RMS_ERR_FAILED_BIT_MASK;

        ref_mgr->ilc.failed_cyc_sel = cyc_sel;
    }

    // Keep track of the minimum RMS v_ref_rate seen so far for this cyc_sel

    cc_float min_rms_v_ref_rate = *refIlcCycDataMinRmsVrefRate(&ref_mgr->ilc, cyc_sel);
    uint32_t downshifter        = *refIlcCycDataDownShifter(&ref_mgr->ilc, cyc_sel) >> 1;

    if(   min_rms_v_ref_rate == 0.0F
       || rms_v_ref_rate < min_rms_v_ref_rate
       || downshifter > 0)
    {
        *refIlcCycDataMinRmsVrefRate(&ref_mgr->ilc, cyc_sel) = min_rms_v_ref_rate = rms_v_ref_rate;

        // Downshifter - use bit shift as it avoids the need for a branch

        *refIlcCycDataDownShifter(&ref_mgr->ilc, cyc_sel) = downshifter;
    }

    // Set the RMS v_ref_rate limit to 3 times the minimum value and check against this limit

    cc_float const rms_v_ref_rate_limit = min_rms_v_ref_rate * 3.0F;

    if(rms_v_ref_rate > rms_v_ref_rate_limit)
    {
        // RMS v_ref_rate exceeded the limit so set the v_ref_rate failed bit in the status

        ref_mgr->ilc.status |= REF_ILC_RMS_V_REF_RATE_FAILED_BIT_MASK;

        ref_mgr->ilc.failed_cyc_sel = cyc_sel;
    }

    // Save the cyc_status data

    ref_mgr->ilc.cyc_status.rms_err              = rms_err;
    ref_mgr->ilc.cyc_status.initial_rms_err      = initial_rms_err;
    ref_mgr->ilc.cyc_status.rms_v_ref_rate       = rms_v_ref_rate;
    ref_mgr->ilc.cyc_status.rms_v_ref_rate_limit = rms_v_ref_rate_limit;

    refCycStatusMgrSetIlc(ref_mgr, &ref_mgr->ilc.cyc_status);

    // If cyc_sel equals ILC_LOG_CYC_SEL then set the inter-cycle logging signals

    if(cyc_sel == refMgrParValue(ref_mgr, ILC_LOG_CYC_SEL))
    {
        ref_mgr->ilc.logging.rms_err              = rms_err;
        ref_mgr->ilc.logging.initial_rms_err      = initial_rms_err;
        ref_mgr->ilc.logging.rms_v_ref_rate       = rms_v_ref_rate;
        ref_mgr->ilc.logging.rms_v_ref_rate_limit = rms_v_ref_rate_limit;

        CC_MEMORY_BARRIER;

        // Set the cycling logging time stamp last to validate the inter-cycle logging signals

        ref_mgr->ilc.logging.time_stamp = ref_mgr->ilc.time_stamp;
    }

    // If the ILC reference array has grown shorter then set zeros in the trailing elements

    uint32_t const new_ref_num_els = ref_mgr->ilc.q.control.index;

    if(new_ref_num_els < ref_mgr->ilc.ref.num_els)
    {
        memset(&ref_mgr->ilc.ref.buf[new_ref_num_els],
               0,
               (ref_mgr->ilc.ref.num_els - new_ref_num_els) * sizeof(cc_float));
    }

    // Save the new number of elements

    *refIlcCycDataRefNumEls(&ref_mgr->ilc, cyc_sel) = new_ref_num_els;

    // Reset the ILC cyc_sel to re-enable immediate resets by refIlcResetRef()

    ref_mgr->ilc.cyc_sel = 0;

    // Execute a delayed reset if one was already requested

    refIlcResetData(ref_mgr, cyc_sel, ref_mgr->ilc.delayed_reset_type);

    return REF_ILC_TO_CYCLING;
}


// NON-RT   refIlcStateCycling

static enum REF_ilc_state refIlcStateCycling(struct REF_mgr * const ref_mgr)
{
    // Calculate the event phase and the event phase with the period and the ILC start time

    int32_t   const reg_period_ns = ref_mgr->reg_mgr->reg_period_ns;
    cc_double const reg_period_ns_fp64 = (cc_double)reg_period_ns;
    int32_t   const s1_factor = -1 + (int32_t)(CCTIME_NS_PER_S * refMgrParValue(ref_mgr, ILC_START_TIME) / reg_period_ns_fp64);
    uint32_t  const event_time_ns = (ref_mgr->ilc.event.time.secs.abs % 3) * CCTIME_NS_PER_S + ref_mgr->ilc.event.time.ns;

    ref_mgr->ilc.event_phase_ns = event_time_ns % reg_period_ns;

    struct CC_ns_time const s1 = cctimeNsDoubleToRelTimeRT((1.0 / CCTIME_NS_PER_S) * reg_period_ns_fp64 * s1_factor);
    struct CC_ns_time const s2 = { { 0 }, ref_mgr->ilc.event_phase_ns };
    struct CC_ns_time const start_time = cctimeNsSubRT(s1, s2);

    // Reset the accumulators to calculate the RMS error and v_ref_rate

    ref_mgr->ilc.err.iter_counter     = 0;
    ref_mgr->ilc.err.acc_ilc_err_2    = 0.0F;

    // Manage resets of the data in the ILC cycle data store for all cycle selector

    enum REF_ilc_reset reset_type = REF_ILC_RESET_NONE;

    if(ref_mgr->ilc.stop_ref != refMgrParValue(ref_mgr, ILC_STOP_REF))
    {
        // ILC stop reference threshold has changed so save the new STOP_REF and request a partial reset for all cycle selectors

        reset_type = REF_ILC_RESET_PARTIAL;

        ref_mgr->ilc.stop_ref = refMgrParValue(ref_mgr, ILC_STOP_REF);
    }

    if(refMgrParValue(ref_mgr, ILC_START_TIME) != ref_mgr->ilc.start_time)
    {
        // ILC start time has changed so save the new start time and request a full reset for all cycle selectors

        reset_type = REF_ILC_RESET_FULL;

        ref_mgr->ilc.start_time = refMgrParValue(ref_mgr, ILC_START_TIME);
    }

    refIlcResetData(ref_mgr, 0, reset_type);

    // Retrieve event data saved by refIlcEvent

    uint32_t const cyc_sel = ref_mgr->ilc.event.cyc_sel;

    ref_mgr->ilc.enabled    = ref_mgr->ilc.ref_range != 0.0F;
    ref_mgr->ilc.time_stamp = ref_mgr->ilc.event.time;

    CC_MEMORY_BARRIER;

    ref_mgr->ilc.event.cyc_sel = 0;

    // Initialise ILC L function

    int32_t const l_order = refIlcSetLfunc(ref_mgr, cyc_sel);

    // Initialise ILC Q function

    int32_t const q_order = refIlcSetQfunc(ref_mgr, cyc_sel);

    // Link to the ILC reference array for cyc_sel and re-sample it if the event time phase has changed

    uint32_t const rt_status = refIlcGetRef(ref_mgr, cyc_sel, reg_period_ns);

    // Set up the control timings for calculating and running the ILC stages

    struct CC_ns_time const reg_period = { { 0 }, reg_period_ns };

    refIlcControlInit(&ref_mgr->ilc.l.control,
                      cctimeNsAddRT(start_time, cctimeNsMulRT(reg_period, (l_order - q_order))),
                      REF_ILC_L_CALC_BIT_MASK);

    refIlcControlInit(&ref_mgr->ilc.q.control,
                      cctimeNsAddRT(start_time, cctimeNsMulRT(reg_period, (l_order + q_order))),
                      REF_ILC_Q_CALC_BIT_MASK);

    refIlcControlInit(&ref_mgr->ilc.ref.control,
                      start_time,
                      REF_ILC_REF_RUN_BIT_MASK);

    // Set the input value and initial value for the open loop down shifter

    ref_mgr->ilc.openloop_downshifter_input = (1 << (l_order + q_order));

    ref_mgr->ilc.openloop_downshifter = ref_mgr->reg_mgr->openloop == true
                                      ? (ref_mgr->ilc.openloop_downshifter_input << 1) - 1
                                      : 0;

    char strbuf[4][22];
    ccprintf("\n  cyc_sel=%u  rt_status=%02X  ILC_START_TIME=%.6f  start_time=%s"
             "\n  O(l)=%d  O(q)=%d  openloop_downshifter_input=%02X  openloop_downshifter=%02X"
             "\n  ref start_time=%s  q start_time=%s  l start_time=%s\n"
            , cyc_sel
            , rt_status
            , refMgrParValue(ref_mgr, ILC_START_TIME)
            , cctimeNsPrintRelTime(start_time, strbuf[0])
            , l_order
            , q_order
            , ref_mgr->ilc.openloop_downshifter_input
            , ref_mgr->ilc.openloop_downshifter
            , cctimeNsPrintRelTime(ref_mgr->ilc.ref.control.start_time, strbuf[1])
            , cctimeNsPrintRelTime(ref_mgr->ilc.q.control.start_time, strbuf[2])
            , cctimeNsPrintRelTime(ref_mgr->ilc.l.control.start_time, strbuf[3])
            );

    // Reset the accumulators to calculate the RMS error and v_ref_rate

    ref_mgr->ilc.err.iter_counter     = 0;
    ref_mgr->ilc.err.acc_ilc_err_2    = 0.0F;
    ref_mgr->ilc.err.acc_v_ref_rate_2 = 0.0F;

    // Reset flags

    ref_mgr->ilc.prev_ref_stop      = true;
    ref_mgr->ilc.to_cycling         = false;
    ref_mgr->ilc.delayed_reset_type = REF_ILC_RESET_NONE;

    // Set the ILC cyc_sel after clearing delayed_reset_type to enable refIlcResetRef()

    CC_MEMORY_BARRIER;

    ref_mgr->ilc.cyc_sel = cyc_sel;

    // Activate ILC by setting rt_status after setting the ILC cyc_sel

    CC_MEMORY_BARRIER;

    ref_mgr->ilc.rt_status = rt_status;

    return REF_ILC_CYCLING;
}


// NON-RT   refIlcStateFailed

static enum REF_ilc_state refIlcStateFailed(struct REF_mgr * const ref_mgr)
{
    ccprintf("Entered\n");

    ref_mgr->ilc.failed = true;

    return REF_ILC_FAILED;
}


// Transition condition functions

// NON-RT   XXtoDA

static RefIlcState * XXtoDA(struct REF_mgr const * const ref_mgr)
{
    if(   ref_mgr->ilc.reset == true
       || refMgrParValue(ref_mgr, MODE_ILC) == CC_DISABLED)
    {
        return refIlcStateDisabled;
    }

    return NULL;
}


// NON-RT   XXtoFL

static RefIlcState * XXtoFL(struct REF_mgr const * const ref_mgr)
{
    if((ref_mgr->ilc.status & ILC_TO_FAILED) != 0)
    {
        return refIlcStateFailed;
    }

    return NULL;
}


// NON-RT   XXtoCY

static RefIlcState * XXtoCY(struct REF_mgr const * const ref_mgr)
{
    if(   (ref_mgr->ilc.status & REF_ILC_CYC_STATES_BIT_MASK) != 0
       && ref_mgr->ilc.event.cyc_sel > 0)
    {
        return refIlcStateCycling;
    }

    return NULL;
}


// NON-RT   DAtoSB

static RefIlcState * DAtoSB(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ilc.cyc_data_store != NULL
       && refMgrParValue(ref_mgr, MODE_ILC) == CC_ENABLED)
    {
        return refIlcStateStandby;
    }

    return NULL;
}


// NON-RT   TCtoSB

static RefIlcState * TCtoSB(struct REF_mgr const * const ref_mgr)
{
    if((ref_mgr->ilc.status & REF_ILC_CYC_STATES_BIT_MASK) == 0)
    {
        return refIlcStateStandby;
    }

    return NULL;
}


// NON-RT   CYtoTC

static RefIlcState * CYtoTC(struct REF_mgr const * const ref_mgr)
{
    if(ref_mgr->ilc.to_cycling == true)
    {
        return refIlcStateToCycling;
    }

    return NULL;
}


// State transition functions array : called in priority order, from left to right.

// The initialization order of the transitions array must match the enum REF_ilc_state
// constant values from refConsts.h.  This is checked with these static assertions.

CC_STATIC_ASSERT(REF_ILC_DISABLED   == 0, Unexpected_enum_REF_ilc_state_constant_value);
CC_STATIC_ASSERT(REF_ILC_STANDBY    == 1, Unexpected_enum_REF_ilc_state_constant_value);
CC_STATIC_ASSERT(REF_ILC_TO_CYCLING == 2, Unexpected_enum_REF_ilc_state_constant_value);
CC_STATIC_ASSERT(REF_ILC_CYCLING    == 3, Unexpected_enum_REF_ilc_state_constant_value);
CC_STATIC_ASSERT(REF_ILC_FAILED     == 4, Unexpected_enum_REF_ilc_state_constant_value);
CC_STATIC_ASSERT(REF_ILC_NUM_STATES == 5, Unexpected_enum_REF_ilc_state_constant_value);

#define REF_ILC_MAX_TRANSITIONS    4      //!< Maximum transitions from a state - currently TC state has the most

static RefIlcTransition * const transitions[][REF_ILC_MAX_TRANSITIONS + 1] =
{
    /* DA */ {                            DAtoSB , NULL },
    /* SB */ { XXtoDA , XXtoFL , XXtoCY ,          NULL },
    /* TC */ { XXtoDA , XXtoFL , XXtoCY , TCtoSB , NULL },
    /* CY */ {                            CYtoTC , NULL },
    /* FL */ { XXtoDA ,                            NULL },
};

// NON-RT   refIlcState

void refIlcState(struct REF_mgr * const ref_mgr)
{
    // If ILC is enabled, set and clear background status bits used by the transition condition functions

    if(refMgrParValue(ref_mgr, MODE_ILC) == CC_ENABLED)
    {
        refIlcSetStatus(ref_mgr);
    }

    // Run the state machine

    RefIlcState              * next_state = NULL;
    RefIlcTransition         * transition;
    RefIlcTransition * const * state_transitions = &transitions[ref_mgr->ilc.state][0];

    // Call transition condition functions for the current state, testing each condition in priority order

    while(   (transition = *(state_transitions++)) != NULL
          && (next_state = (*transition)(ref_mgr)) == NULL)
    {
    }

    // If a transition condition is true then it returns the pointer to the new state's function

    if(next_state != NULL)
    {
        // Run the new state's function to enter the state (it returns the index of the new state)

        ref_mgr->ilc.state = next_state(ref_mgr);
    }
}

// EOF
