//! @file  refRampMgr.c
//!
//! @brief Generation of an arbitrary number of ramps using the ramp function from libfg.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

//! Clip reference to zero if the converter is unipolar and cannot access the requested value.
//!
//! This function takes into account the state of the optional polarity switch if present.
//!
//! @param[in]  ref_mgr          Pointer to REF_mgr structure
//! @param[in]  ref              Reference value to clip at zero if needed
//!
//! @returns reference after clipping

static cc_float refRampMgrClipRefRT(struct REF_mgr * const ref_mgr, cc_float ref)
{
    // For unipolar reference, clip to zero if in currently inaccessible range because of polarity switch position

    if(   ref_mgr->reg_mgr->lim_ref->flags.unipolar == true
       && (   (polswitchIsNegative(ref_mgr->polswitch_mgr) == true  && ref > 0.0F)
           || (polswitchIsNegative(ref_mgr->polswitch_mgr) == false && ref < 0.0F) ) )
    {
        ref = 0.0F;
    }

    return ref;
}



void refRampMgrInitRT(struct REF_mgr * const ref_mgr,
                      cc_float               start_time,
                      cc_float         const initial_rate,
                      cc_float         const initial_ref,
                      cc_float         const final_ref,
                      bool             const use_acc_for_dec)
{
    struct REF_ramp_mgr * const ramp_mgr = &ref_mgr->ramp_mgr;

    ccprintf("\n  start_time=%g\n  initial_rate=%g\n  initial_ref=%g\n  final_ref=%g\n  iteration_index=%u\n  use_acc_for_dec=%u\n"
            , start_time
            , initial_rate
            , initial_ref
            , final_ref
            , ref_mgr->reg_mgr->reg_signal->iteration_index
            , use_acc_for_dec
            );

    ramp_mgr->final_ref = final_ref;

    // If the ramp is not null

    if(initial_ref != final_ref || initial_rate != 0.0F)
    {
        ccprintf("\n  ramp_mgr->status=%u\n  ramp_mgr->ramp_ref=%g\n  refRampMgrClipRefRT(ref_mgr, final_ref)=%g\n"
                , ramp_mgr->status
                , ramp_mgr->ramp_ref
                ,refRampMgrClipRefRT(ref_mgr, final_ref)
                );

        struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

        // If the function should start now or in the future (with respect to func_time) then reset the event time
        // In cycling, start_time will be always negative - ramps are used for pre-funcs only

        if(start_time >= 0.0F)
        {
            // Change the fg event time, but don't alter the rest

            refFuncMgrUpdateEventTimeRT(&ref_mgr->fg, ref_mgr->iter_time);

            // Copy the new event time into the REF_RUN parameter, so that it can be read back by the class

            refMgrParValue(ref_mgr, REF_RUN) = cctimeNsToUsRT(ref_mgr->iter_time);
        }

        // Include ref_advance and compensate if in the middle of regulation period
        // (this is possible following a reg mode change)

        if(ref_mgr->iter_cache.reg_mode != REG_VOLTAGE)
        {
            start_time -= (reg_mgr->reg_signal->iteration_index * reg_mgr->iter_period);
        }

        start_time += reg_mgr->ref_advance;

        // Save new ramp parameters in ramp_mgr structure

        ramp_mgr->start_time      = start_time;
        ramp_mgr->initial_rate    = initial_rate;
        ramp_mgr->initial_ref     = initial_ref;
        ramp_mgr->ramp_ref        = 1.0E30F;
        ramp_mgr->new_ramp_ref    = true;
        ramp_mgr->use_acc_for_dec = use_acc_for_dec;

        // Set fg_pars to use the fsm fg_pars

        refFuncMgrUpdateParsRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);

        // Set statuses to show that the ramp hasn't finished

        ramp_mgr->status   = FG_DURING_FUNC;
        ref_mgr->fg.status = FG_DURING_FUNC;
        ref_mgr->post_func = false;

//        ref_mgr->debug.f[0] = start_time;
//        ref_mgr->debug.f[1] = initial_ref;
//        ref_mgr->debug.f[2] = final_ref;
//        ref_mgr->debug.f[3] = initial_rate;
//        ref_mgr->debug.f[4] = reg_mgr->ref_advance;
//        ref_mgr->debug.u[5] = ref_mgr->fg.event_updated;
//        ref_mgr->debug.u[6] = use_acc_for_dec;
//        ref_mgr->debug.u[7]++;
    }
    else
    {
        // Null ramp - configure NONE reference to finish immediately

        ref_mgr->ref_fg_unlimited = final_ref;

        fgNoneArmRT(&ref_mgr->fsm.fg_pars, NULL);

        refFuncMgrUpdateParsRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);

        ref_mgr->fg.status = FG_POST_FUNC;
        ramp_mgr->status   = FG_POST_FUNC;
        ref_mgr->post_func = true;

        ccprintf("Armed NONE\n");
    }
}


static void refRampMgrPrepareRampRT(struct REF_mgr * const ref_mgr, struct REF_ramp_mgr * const ramp_mgr)
{
    // If libref has automatic control of a polarity switch and the ramp has ended at 0.0

    if(polswitchIsAutomatic(ref_mgr->polswitch_mgr) == true && ramp_mgr->ramp_ref == 0.0F)
    {
        // Set the polarity switch mode to drive the polarity switch to required state

        if(ramp_mgr->final_ref > 0.0F)
        {
            if(polswitchVarValue(ref_mgr->polswitch_mgr, REQUESTED_STATE) == POLSWITCH_STATE_NONE)
            {
                polswitchParValue(ref_mgr->polswitch_mgr, MODE_AUTO) = POLSWITCH_MODE_POSITIVE;
            }
        }
        else if(ramp_mgr->final_ref < 0.0F)
        {
            if(polswitchVarValue(ref_mgr->polswitch_mgr, REQUESTED_STATE) == POLSWITCH_STATE_NONE)
            {
                polswitchParValue(ref_mgr->polswitch_mgr, MODE_AUTO) = POLSWITCH_MODE_NEGATIVE;
            }
        }
    }

    // Check if the ramp reference has changed and recalculate the ramp if it has

    cc_float const ramp_ref = refRampMgrClipRefRT(ref_mgr, ramp_mgr->final_ref);

    if(ramp_mgr->new_ramp_ref == true || ramp_ref != ramp_mgr->ramp_ref)
    {
        ramp_mgr->ramp_ref = ramp_ref;

        // Get the default parameters for ramp calculation based on reg_mode and load_select

        struct REF_default_ptrs * const defaults = &ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode];

        // If it's the second ramp following a polarity switch

        if(ramp_mgr->new_ramp_ref == false)
        {
            // Start at the actual function time

            ramp_mgr->start_time = ref_mgr->fg.time_adv_fp32;

            // Set initial ref and rate to 0. A non-zero rate is used only for a new ramp to smoothly connect with the previous function.

            ramp_mgr->initial_ref  = 0.0F;
            ramp_mgr->initial_rate = 0.0F;
        }
        else
        {
            ramp_mgr->new_ramp_ref = false;
        }

        // If ramping to zero to switch polarity then always use acceleration for deceleration

        cc_float const deceleration = ramp_mgr->ramp_ref == ramp_mgr->final_ref
                                    ? (ramp_mgr->use_acc_for_dec == true ? *defaults->acceleration : *defaults->deceleration)
                                    : *defaults->acceleration;

        // Set terminal_delta_ref to 1 ppm of positive limit if ramping to final_ref, otherwise specify
        // zero to end the ramp as soon as the unlimited reference function finishes

        cc_float const terminal_delta_ref = ramp_ref == ramp_mgr->final_ref
                                          ? *ref_mgr->ref_limits[ref_mgr->iter_cache.reg_mode].pos * 1.0E-6F
                                          : 0.0F;

        // Calculate the parameters for the new ramp

        fgRampCalcRT(polswitchIsAutomatic(ref_mgr->polswitch_mgr),
                     polswitchIsNegative(ref_mgr->polswitch_mgr),
                     ramp_mgr->start_time,
                     ramp_mgr->initial_rate,
                     ramp_mgr->initial_ref,
                     ramp_mgr->ramp_ref,
                    *defaults->acceleration,
                    *defaults->linear_rate,
                     deceleration,
                     terminal_delta_ref,
                    &ref_mgr->fsm.fg_pars.ramp);

        ccprintf("\n  start_time=%g\n  initial_rate=%g\n  initial_ref=%g\n  final_ref=%g\n  use_acc_for_dec=%u\n  "
                 "acceleration=%g\n  linear_rate=%g\n  deceleration=%g\n  terminal_delta_ref=%g\n"
                 , ramp_mgr->start_time
                 , ramp_mgr->initial_rate
                 , ramp_mgr->initial_ref
                 , ramp_mgr->final_ref
                 , ramp_mgr->use_acc_for_dec
                 , *defaults->acceleration
                 , *defaults->linear_rate
                 , deceleration
                 , terminal_delta_ref
                );

        ramp_mgr->status   = FG_DURING_FUNC;
        ref_mgr->fg.status = FG_DURING_FUNC;
    }
}



static void refRampMgrFinishRampRT(struct REF_mgr * const ref_mgr, struct REF_ramp_mgr * const ramp_mgr)
{
    // The ramp ends once the limited ref (ref_fg) is within 1ppm of the final ref (ref_fg_unlimited),
    // so force limited ref to be the final ref, to avoid residual error

    ref_mgr->ref_fg = ref_mgr->ref_fg_unlimited;

    // Check if complete RAMP has finished or if we have just finished ramp to zero to change polarity

    if(ref_mgr->ref_fg_unlimited == ramp_mgr->final_ref)
    {
        // Reference has reached final value

        if(ref_mgr->fg.pars->meta.type != FG_NONE)
        {
            // Function generator still has a reference so set POST_FUNC and arm NONE

            ramp_mgr->status = FG_POST_FUNC;
            fgNoneArmRT(ref_mgr->fg.pars, NULL);
        }
    }
    else if(ref_mgr->ref_fg_unlimited == ramp_mgr->ramp_ref)
    {
        // Reference has reached the end of the RAMP but has not yet reached the final reference.
        // Arm zero while waiting for polarity switch to act.

        fgZeroArmRT(ref_mgr->fg.pars, NULL);
    }
}


enum FG_status refRampMgrRT(struct REF_mgr * const ref_mgr)
{
    struct REF_ramp_mgr * const ramp_mgr = &ref_mgr->ramp_mgr;

    // We should be using the parameter structure initialized by the init function

    CC_ASSERT(ref_mgr->fg.pars == &ref_mgr->fsm.fg_pars);

    // Check if the ramp has just finished

    if(ref_mgr->fg.status == FG_POST_FUNC)
    {
        refRampMgrFinishRampRT(ref_mgr, ramp_mgr);
    }

    // Check if a new ramp should be started

    if(   ramp_mgr->new_ramp_ref == true
       || (   ramp_mgr->status == FG_DURING_FUNC
           && (   ref_mgr->fg.status == FG_POST_FUNC
               || (   ref_mgr->reg_mgr->i.lim_meas.flags.zero == true
                   && ramp_mgr->ramp_ref == 0.0F) )))
    {
        refRampMgrPrepareRampRT(ref_mgr, ramp_mgr);
    }

    return ramp_mgr->status;
}



void refRampMgrTerminateRT(struct REF_mgr * const ref_mgr)
{
    ref_mgr->ramp_mgr.status = FG_POST_FUNC;
}

// EOF
