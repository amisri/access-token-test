//! @file  refStateOff.c
//! @brief Converter Control Reference Manager library: OFF reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"


static void refStateOffRefLimits(struct REF_mgr * const ref_mgr)
{
    // Max reference limits

    ref_mgr->pc_on_cache.max_v_ref = *ref_mgr->ref_limits[REG_VOLTAGE].pos;
    ref_mgr->pc_on_cache.max_i_ref = *ref_mgr->ref_limits[REG_CURRENT].pos;
    ref_mgr->pc_on_cache.max_b_ref = *ref_mgr->ref_limits[REG_FIELD].pos;

    // Min reference limits

    if(ref_mgr->polswitch_mgr->pars.timeout > 0)
    {
        // Polarity switch is present - min limit is minus the max limit

        ref_mgr->pc_on_cache.min_v_ref = -ref_mgr->pc_on_cache.max_v_ref;
        ref_mgr->pc_on_cache.min_i_ref = -ref_mgr->pc_on_cache.max_i_ref;
        ref_mgr->pc_on_cache.min_b_ref = -ref_mgr->pc_on_cache.max_b_ref;
    }
    else
    {
        // No polarity switch is in use - use NEG or STANDBY limit

        ref_mgr->pc_on_cache.min_v_ref = *ref_mgr->ref_limits[REG_VOLTAGE].neg;

        ref_mgr->pc_on_cache.min_i_ref = *ref_mgr->ref_limits[REG_CURRENT].neg;

        if(ref_mgr->pc_on_cache.min_i_ref == 0.0F)
        {
            ref_mgr->pc_on_cache.min_i_ref = *ref_mgr->ref_limits[REG_CURRENT].standby;
        }

        ref_mgr->pc_on_cache.min_b_ref = *ref_mgr->ref_limits[REG_FIELD].neg;

        if(ref_mgr->pc_on_cache.min_b_ref == 0.0F)
        {
            ref_mgr->pc_on_cache.min_b_ref = *ref_mgr->ref_limits[REG_FIELD].standby;
        }
    }

    // PULSE and TABLE reference limits are based on REG_MODE_CYC

    switch(refMgrParValue(ref_mgr, MODE_REG_MODE_CYC))
    {
        default:

            ref_mgr->pc_on_cache.max_ref = ref_mgr->pc_on_cache.max_v_ref;
            ref_mgr->pc_on_cache.min_ref = ref_mgr->pc_on_cache.min_v_ref;
            break;

        case REG_CURRENT:

            ref_mgr->pc_on_cache.max_ref = ref_mgr->pc_on_cache.max_i_ref;
            ref_mgr->pc_on_cache.min_ref = ref_mgr->pc_on_cache.min_i_ref;
            break;

        case REG_FIELD:

            ref_mgr->pc_on_cache.max_ref = ref_mgr->pc_on_cache.max_b_ref;
            ref_mgr->pc_on_cache.min_ref = ref_mgr->pc_on_cache.min_b_ref;
            break;
    }
}


RefState * refStateOffInitRT(struct REF_mgr * const ref_mgr)
{
    // Disable use of real-time reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Set the function type to ZERO to force the reference to zero.

    fgZeroArmRT(ref_mgr->fg.pars, NULL);

    // Reset force openloop flag

    ref_mgr->force_openloop = false;

    // Reset start delay timer to ensure it is initialized before restarting

    ref_mgr->fsm.off.start_delay_timer = REF_OFF_TIMER_RESET;

    return refStateOffRT;
}



enum REF_state refStateOffRT(struct REF_mgr * const ref_mgr)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    if(   ref_mgr->fsm.off.start_delay_timer != REF_OFF_TIMER_RESET
       && ref_mgr->iter_cache.pc_state == REF_PC_ON
       && ref_mgr->latched_faults == 0)
    {
        // Start the delay time when the converter is on and no faults are latched

        if(ref_mgr->fsm.off.start_delay_timer == REF_OFF_TIMER_READY)
        {
            // Start delay has passed - this allows any perturbations linked to powering on the converter to decay

            if(ref_mgr->fsm.off.init_rst_timer == REF_OFF_TIMER_RESET && ref_mgr->iter_cache.ref_mode != REF_OFF)
            {
                // The init_rst timer is not running and the application requests leaving OFF state

                // Switch to voltage regulation mode to start accumulating the current and field RST histories.

                refMgrModeSetRT(ref_mgr, REG_VOLTAGE, false);

                // Initialize the timer based on the longest period * RST order for I and B regulation to give time for complete RST history to be filled.
                // If RST order is 5, then 6 iterations are required and we add one extra to be sure, thus the +2.

                uint32_t init_b_rst_timer = 0;
                uint32_t init_i_rst_timer = 0;

                if(reg_mgr->b.regulation)
                {
                    init_b_rst_timer = regMgrVarValue(reg_mgr, BREG_PERIOD_ITERS) * (regMgrVarValue(reg_mgr, BREG_OP_RST_ORDER) + 2);
                }

                if(reg_mgr->i.regulation)
                {
                    init_i_rst_timer = regMgrVarValue(reg_mgr, IREG_PERIOD_ITERS) * (regMgrVarValue(reg_mgr, IREG_OP_RST_ORDER) + 2);
                }

                ref_mgr->fsm.off.init_rst_timer = init_b_rst_timer > init_i_rst_timer ? init_b_rst_timer : init_i_rst_timer;
            }
            else
            {
                if(ref_mgr->fsm.off.init_rst_timer > REF_OFF_TIMER_READY)
                {
                    // Down count the init RST timer to delay the OFtoXX transitions until init_rst_timer equals REF_OFF_TIMER_READY

                    ref_mgr->fsm.off.init_rst_timer--;
                }
            }
        }
        else
        {
            // Start delay timer is running - decrement the start delay down-counter

            ref_mgr->fsm.off.start_delay_timer--;

            // Reset REF_TO_OFF_TIME

            ref_mgr->to_off_time = cc_zero_us;
        }
    }
    else
    {
        // PC is OFF so set the regulation mode to NONE

        ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = refMgrModeSetRT(ref_mgr, REG_NONE, false);

        // Reset the init_rst and start_delay_timer timers - note that the state machine is called every iteration when in OFF state

        ref_mgr->fsm.off.init_rst_timer    = REF_OFF_TIMER_RESET;
        ref_mgr->fsm.off.start_delay_timer = 1 + (uint32_t)(REF_PC_START_DELAY * ref_mgr->reg_mgr->inv_iter_period);
    }

    regMgrSimMeasSetRT(reg_mgr, (refMgrParValue(ref_mgr, MODE_SIM_MEAS) == CC_ENABLED));

    // Cache parameters to use when converter is ON

    ref_mgr->pc_on_cache.load_select = regMgrParValue(reg_mgr, LOAD_SELECT);
    ref_mgr->pc_on_cache.actuation   = regMgrParValue(reg_mgr, VS_ACTUATION);

    refStateOffRefLimits(ref_mgr);

    // Report the latched faults while in OFF state

    ref_mgr->faults_in_off = ref_mgr->latched_faults;

    return REF_OFF;
}

// EOF
