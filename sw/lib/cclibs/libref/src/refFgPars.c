//! @file  refFgPars.c
//! @brief Converter Control Reference Manager library function generation parameter functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"
#include "libref/refFgPars.h"


//! NON-RT  refFgParsSetFloat
//!
//! Help to set a floating point parameter.
//!
//! This function will return the default value if an error was already reported or the parameter
//! value is NaN or out of limits. If the value is ok and no previous error was reported then the value is returned.
//!
//! @param[in]  index             Parameter index [0..n].
//! @param[in]  num_pars          Number of parameters supplied.
//! @param[in]  par_values        Parameter values array.
//! @param[in]  min               Minimum limit.
//! @param[in]  max               Maximum limit.
//! @param[in]  default_value     Default value.
//! @param[in]  fg_error          Pointer to fg_error structure.
//!
//! @retval     par_value         Take from par_values[index] if it is a valid parameter value.
//! @retval     default_vaue      if parameter value is invalid or NaN or a previous error detected.

static cc_float refFgParsSetFloat(uint32_t          const index,
                                  uint32_t          const num_pars,
                                  cc_float  const * const par_values,
                                  cc_float          const min,
                                  cc_float          const max,
                                  cc_float          const default_value,
                                  struct FG_error * const fg_error)
{
    cc_float result = default_value;
    cc_float const par_value = par_values[index];

    // If no previous error reported then check for errors

    if(fg_error->fg_errno == FG_OK && index < num_pars)
    {
        if(isnan(par_value) == true)
        {
            if(index == 0)
            {
                // Invalid first value - report BAD_PARAMETER

                fgError(FG_BAD_PARAMETER, fg_error->fg_type, 11, 0.0F, 0.0F, 0.0F, 0.0F, fg_error);
            }

            // For the rest of the parameters, NaN signals that the default value already in result should be used
        }
        else if(par_value < min || par_value > max)
        {
            // Out of limits

            fgError(FG_OUT_OF_LIMITS, fg_error->fg_type, index + 11, par_value, min, max, 0.0F, fg_error);
        }
        else
        {
            // Valid value

            result = par_value;
        }
    }

    return result;
}


// NON-RT  refFgParsSetParsRamp

void refFgParsSetParsRamp(struct REF_mgr  * const ref_mgr,
                       uint32_t          const num_pars,
                       cc_float  const * const par_values,
                       struct FG_error * const fg_error)
{
    if(num_pars <= 4)
    {
        struct REF_default_ptrs const * const defaults = &ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode];

        refMgrFgParValue(ref_mgr, RAMP_FINAL_REF   ) = refFgParsSetFloat(0, num_pars, par_values,-1.0E6F , 1.0E6F,  0.0F                   , fg_error);
        refMgrFgParValue(ref_mgr, RAMP_ACCELERATION) = refFgParsSetFloat(1, num_pars, par_values, 1.0E-6F, 1.0E12F, *defaults->acceleration, fg_error);
        refMgrFgParValue(ref_mgr, RAMP_LINEAR_RATE ) = refFgParsSetFloat(2, num_pars, par_values, 0.0F   , 1.0E12F, *defaults->linear_rate , fg_error);
        refMgrFgParValue(ref_mgr, RAMP_DECELERATION) = refFgParsSetFloat(3, num_pars, par_values, 1.0E-6F, 1.0E12F, *defaults->deceleration, fg_error);
    }
    else
    {
        fgError(FG_BAD_ARRAY_LEN, fg_error->fg_type, 10, (float)num_pars, 1.0F, 4.0F, 0.0F, fg_error);
    }
}


// NON-RT  refFgParsSetParsPlep

void refFgParsSetParsPlep(struct REF_mgr  * const ref_mgr,
                       uint32_t          const num_pars,
                       cc_float  const * const par_values,
                       struct FG_error * const fg_error)
{
    if(num_pars <= 5)
    {
        struct REF_default_ptrs const * const defaults = &ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][ref_mgr->iter_cache.reg_mode];

        refMgrFgParValue(ref_mgr, PLEP_FINAL_REF   ) = refFgParsSetFloat(0, num_pars, par_values,-1.0E6F , 1.0E6F,  0.0F                   , fg_error);
        refMgrFgParValue(ref_mgr, PLEP_ACCELERATION) = refFgParsSetFloat(1, num_pars, par_values, 1.0E-6F, 1.0E12F, *defaults->acceleration, fg_error);
        refMgrFgParValue(ref_mgr, PLEP_LINEAR_RATE ) = refFgParsSetFloat(2, num_pars, par_values, 0.0F   , 1.0E12F, *defaults->linear_rate , fg_error);
        refMgrFgParValue(ref_mgr, PLEP_EXP_TC      ) = refFgParsSetFloat(3, num_pars, par_values, 0.0F   , 1.0E6F , 0.0F                   , fg_error);
        refMgrFgParValue(ref_mgr, PLEP_EXP_FINAL   ) = refFgParsSetFloat(4, num_pars, par_values, 0.0F   , 1.0E12F, 0.0F                   , fg_error);
    }
    else
    {
        fgError(FG_BAD_ARRAY_LEN, fg_error->fg_type, 10, (float)num_pars, 1.0F, 5.0F, 0.0F, fg_error);
    }
}


// NON-RT  refFgParsSetParsTrim

void refFgParsSetParsTrim(struct REF_mgr  * const ref_mgr,
                       uint32_t          const num_pars,
                       cc_float  const * const par_values,
                       struct FG_error * const fg_error)
{
    if(num_pars <= 2)
    {
        refMgrFgParValue(ref_mgr, TRIM_FINAL_REF) = refFgParsSetFloat(0, num_pars, par_values,-1.0E6F , 1.0E6F,  0.0F, fg_error);
        refMgrFgParValue(ref_mgr, TRIM_DURATION ) = refFgParsSetFloat(1, num_pars, par_values, 0.0F   , 1.0E6F,  0.0F, fg_error);
    }
    else
    {
        fgError(FG_BAD_ARRAY_LEN, fg_error->fg_type, 10, (float)num_pars, 1.0F, 2.0F, 0.0F, fg_error);
    }
}


// NON-RT  refFgParsSetParsTest

void refFgParsSetParsTest(struct REF_mgr  * const ref_mgr,
                       uint32_t          const num_pars,
                       cc_float  const * const par_values,
                       struct FG_error * const fg_error)
{
    if(num_pars <= 6)
    {
        refMgrFgParValue(ref_mgr, TEST_AMPLITUDE_PP) =                                     refFgParsSetFloat(0, num_pars, par_values,-1.0E6F , 1.0E6F, 0.0F, fg_error);
        refMgrFgParValue(ref_mgr, TEST_NUM_PERIODS ) =                           (uint32_t)refFgParsSetFloat(1, num_pars, par_values, 1.0F   , 1.0E6F, 1.0F, fg_error);
        refMgrFgParValue(ref_mgr, TEST_PERIOD      ) =                                     refFgParsSetFloat(2, num_pars, par_values, 2.0E-4F, 1.0E5F, 1.0F, fg_error);
        refMgrFgParValue(ref_mgr, TEST_WINDOW      ) = (enum CC_enabled_disabled)(uint32_t)refFgParsSetFloat(3, num_pars, par_values, 0.0F   , 1.0F  , 0.0F, fg_error);
        refMgrFgParValue(ref_mgr, TEST_EXP_DECAY   ) = (enum CC_enabled_disabled)(uint32_t)refFgParsSetFloat(4, num_pars, par_values, 0.0F   , 1.0F  , 0.0F, fg_error);
        refMgrFgParValue(ref_mgr, TEST_PERIOD_ITERS) =                           (uint32_t)refFgParsSetFloat(5, num_pars, par_values, 0.0F   , 1.0E4F, 0.0F, fg_error);
    }
    else
    {
        fgError(FG_BAD_ARRAY_LEN, fg_error->fg_type, 10, (float)num_pars, 1.0F, 6.0F, 0.0F, fg_error);
    }
}


// NON-RT  refFgParsSetParsPrbs

void refFgParsSetParsPrbs(struct REF_mgr  * const ref_mgr,
                       uint32_t          const num_pars,
                       cc_float  const * const par_values,
                       struct FG_error * const fg_error)
{
    if(num_pars <= 4)
    {
        refMgrFgParValue(ref_mgr, PRBS_AMPLITUDE_PP ) =           refFgParsSetFloat(0, num_pars, par_values,-1.0E6F,   1.0E6F,  0.0F, fg_error);
        refMgrFgParValue(ref_mgr, PRBS_PERIOD_ITERS ) = (uint32_t)refFgParsSetFloat(1, num_pars, par_values, 1.0F  , 10000.0F, 10.0F, fg_error);
        refMgrFgParValue(ref_mgr, PRBS_NUM_SEQUENCES) = (uint32_t)refFgParsSetFloat(2, num_pars, par_values, 1.0F  ,  1000.0F,  1.0F, fg_error);
        refMgrFgParValue(ref_mgr, PRBS_K            ) = (uint32_t)refFgParsSetFloat(3, num_pars, par_values, 3.0F  ,    31.0F,  7.0F, fg_error);
    }
    else
    {
        fgError(FG_BAD_ARRAY_LEN, fg_error->fg_type, 10, (float)num_pars, 1.0F, 4.0F, 0.0F, fg_error);
    }
}

// EOF
