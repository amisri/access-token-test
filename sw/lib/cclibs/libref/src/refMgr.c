//! @file  refMgr.c
//! @brief Converter Control Function Manager library ref_mgr functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


//#define CCPRINTF

// Include libref header files (must be after #define CCPRINTF)

#include "libref.h"
#include "libref/refParsInit.h"

// Constants

CC_STATIC_ASSERT(CC_ARE_MULTIPLE_BITS_SET(REF_VFF_DELAY_BUF_LEN), error_REF_VFF_DELAY_BUF_LEN_is_not_a_power_of_two);

#define REF_VFF_DELAY_BUF_LEN_MASK      (REF_VFF_DELAY_BUF_LEN-1)


// NON-RT   refMgrInit

int32_t refMgrInit(struct REF_mgr          * const ref_mgr,
                   struct POLSWITCH_mgr    * const polswitch_mgr,
                   struct REG_mgr          * const reg_mgr,
                   void                    * const mutex,
                   struct FG_error         * const fg_error,
                   struct REF_cyc_status   * const cyc_status,
                   struct REF_armed        * const ref_armed,
                   struct FG_point         * const armed_table_function,
                   struct FG_point         * const running_table_function,
                   struct REF_ilc_cyc_data * const ilc_cyc_data_store,
                   uint32_t                  const num_cyc_sels,
                   uint32_t                  const max_table_points,
                   uint32_t                  const max_ilc_samples,
                   uint32_t                  const ref_to_tc_limit)
{
    uint32_t par_idx;

    // Check that the application has set all the function generation parameter pointers

    for(par_idx = 0 ; par_idx < REF_NUM_FG_PARS ; par_idx++)
    {
        // If parameter point is still NULL then return parameter number (par_idx + 1)

        if(ref_mgr->u.fg_par[par_idx].value == REF_FG_PAR_NOT_USED)
        {
            return par_idx + 1;
        }
    }

    // Initialize the structure with zero variables to zero

    memset(&ref_mgr->zero_values, 0, sizeof(ref_mgr->zero_values));

    // Check if mutex is set that the reg_mgr pointers to lock/unlock a mutex are set

    if(mutex != NULL && (reg_mgr->mutex_lock == NULL || reg_mgr->mutex_unlock == NULL))
    {
        return -1;
    }

    // Initialize reference manager structure

    ref_mgr->reg_mgr                    = reg_mgr;
    ref_mgr->polswitch_mgr              = polswitch_mgr;
    ref_mgr->mutex                      = mutex;
    ref_mgr->fg_error                   = fg_error;
    ref_mgr->cyc_status                 = cyc_status;
    ref_mgr->ref_armed                  = ref_armed;
    ref_mgr->armed_table_function       = armed_table_function;
    ref_mgr->num_cyc_sels               = num_cyc_sels;
    ref_mgr->max_table_points           = max_table_points;
    ref_mgr->ref_to_tc_limit            = ref_to_tc_limit;

    // Initialize the pointers to the Direct CCVs for the regulation modes

    ref_mgr->ref_direct[REG_VOLTAGE] = refMgrParPointer(ref_mgr, DIRECT_V_REF);
    ref_mgr->ref_direct[REG_CURRENT] = refMgrParPointer(ref_mgr, DIRECT_I_REF);
    ref_mgr->ref_direct[REG_FIELD]   = refMgrParPointer(ref_mgr, DIRECT_B_REF);
    ref_mgr->ref_direct[REG_NONE]    = &ref_mgr->zero_values.zero_float;

    // Initialize the pointers to the default references

    uint8_t load_select;

    for(load_select = 0; load_select < REG_NUM_LOADS; load_select++)
    {
        struct REF_default_ptrs * const ref_defaults = ref_mgr->ref_defaults[load_select];

        ref_defaults[REG_VOLTAGE].acceleration = &(refMgrParPointer(ref_mgr, DEFAULT_V_ACCELERATION))[load_select];
        ref_defaults[REG_VOLTAGE].deceleration = &(refMgrParPointer(ref_mgr, DEFAULT_V_DECELERATION))[load_select];
        ref_defaults[REG_VOLTAGE].linear_rate  = &(refMgrParPointer(ref_mgr, DEFAULT_V_LINEAR_RATE) )[load_select];
        ref_defaults[REG_VOLTAGE].pre_func_max = &(refMgrParPointer(ref_mgr, DEFAULT_V_PRE_FUNC_MAX))[load_select];
        ref_defaults[REG_VOLTAGE].pre_func_min = &(refMgrParPointer(ref_mgr, DEFAULT_V_PRE_FUNC_MIN))[load_select];
        ref_defaults[REG_VOLTAGE].min_rms      = &(refMgrParPointer(ref_mgr, DEFAULT_V_MINRMS)      )[load_select];

        ref_defaults[REG_CURRENT].acceleration = &(refMgrParPointer(ref_mgr, DEFAULT_I_ACCELERATION))[load_select];
        ref_defaults[REG_CURRENT].deceleration = &(refMgrParPointer(ref_mgr, DEFAULT_I_DECELERATION))[load_select];
        ref_defaults[REG_CURRENT].linear_rate  = &(refMgrParPointer(ref_mgr, DEFAULT_I_LINEAR_RATE) )[load_select];
        ref_defaults[REG_CURRENT].pre_func_max = &(refMgrParPointer(ref_mgr, DEFAULT_I_PRE_FUNC_MAX))[load_select];
        ref_defaults[REG_CURRENT].pre_func_min = &(refMgrParPointer(ref_mgr, DEFAULT_I_PRE_FUNC_MIN))[load_select];
        ref_defaults[REG_CURRENT].min_rms      = &(refMgrParPointer(ref_mgr, DEFAULT_I_MINRMS)      )[load_select];

        ref_defaults[REG_FIELD]  .acceleration = &(refMgrParPointer(ref_mgr, DEFAULT_B_ACCELERATION))[load_select];
        ref_defaults[REG_FIELD]  .deceleration = &(refMgrParPointer(ref_mgr, DEFAULT_B_DECELERATION))[load_select];
        ref_defaults[REG_FIELD]  .linear_rate  = &(refMgrParPointer(ref_mgr, DEFAULT_B_LINEAR_RATE) )[load_select];
        ref_defaults[REG_FIELD]  .pre_func_max = &(refMgrParPointer(ref_mgr, DEFAULT_B_PRE_FUNC_MAX))[load_select];
        ref_defaults[REG_FIELD]  .pre_func_min = &(refMgrParPointer(ref_mgr, DEFAULT_B_PRE_FUNC_MIN))[load_select];
        ref_defaults[REG_FIELD]  .min_rms      = &(refMgrParPointer(ref_mgr, DEFAULT_B_MINRMS)      )[load_select];

        ref_defaults[REG_NONE]   .acceleration = &ref_mgr->zero_values.zero_float;
        ref_defaults[REG_NONE]   .deceleration = &ref_mgr->zero_values.zero_float;
        ref_defaults[REG_NONE]   .linear_rate  = &ref_mgr->zero_values.zero_float;
        ref_defaults[REG_NONE]   .pre_func_max = &ref_mgr->zero_values.zero_float;
        ref_defaults[REG_NONE]   .pre_func_min = &ref_mgr->zero_values.zero_float;
        ref_defaults[REG_NONE]   .min_rms      = &ref_mgr->zero_values.zero_float;
    }

    // Initialize the pointers to the operational reference limits

    ref_mgr->ref_limits[REG_VOLTAGE].pos     = &reg_mgr->v.lim_v_ref.max_clip_user;
    ref_mgr->ref_limits[REG_VOLTAGE].standby = &ref_mgr->zero_values.zero_float;
    ref_mgr->ref_limits[REG_VOLTAGE].neg     = &reg_mgr->v.lim_v_ref.min_clip_user;
    ref_mgr->ref_limits[REG_VOLTAGE].rate    = &reg_mgr->v.lim_ref.rate_clip;

    ref_mgr->ref_limits[REG_CURRENT].pos     = &reg_mgr->i.lim_ref.max_clip;
    ref_mgr->ref_limits[REG_CURRENT].standby = &reg_mgr->i.lim_ref.standby;
    ref_mgr->ref_limits[REG_CURRENT].neg     = &reg_mgr->i.lim_ref.min_clip;
    ref_mgr->ref_limits[REG_CURRENT].rate    = &reg_mgr->i.lim_ref.rate_clip;

    ref_mgr->ref_limits[REG_FIELD].  pos     = &reg_mgr->b.lim_ref.max_clip;
    ref_mgr->ref_limits[REG_FIELD].  standby = &reg_mgr->b.lim_ref.standby;
    ref_mgr->ref_limits[REG_FIELD].  neg     = &reg_mgr->b.lim_ref.min_clip;
    ref_mgr->ref_limits[REG_FIELD].  rate    = &reg_mgr->b.lim_ref.rate_clip;

    ref_mgr->ref_limits[REG_NONE].   pos     = &ref_mgr->zero_values.zero_float;
    ref_mgr->ref_limits[REG_NONE].   standby = &ref_mgr->zero_values.zero_float;
    ref_mgr->ref_limits[REG_NONE].   neg     = &ref_mgr->zero_values.zero_float;
    ref_mgr->ref_limits[REG_NONE].   rate    = &ref_mgr->zero_values.zero_float;

    // Initialize the iterative learning controller (ILC)

    refIlcInit(ref_mgr, ilc_cyc_data_store, max_ilc_samples, num_cyc_sels - 1);

    // Initialize libref event handling

    refEventMgrInit(&ref_mgr->event_mgr, running_table_function, max_table_points);

    // Initialize function generation pointers

    refFuncMgrUpdateParsRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);

    // Initialize parameters structure

    refParsInit(ref_mgr);

    // Prepare parameter group masks

    refArmInitParGroupMasks(ref_mgr);

    // Initialize the ref state machine so on first iteration the transition TOtoOF will be taken and
    // refStateOffInitRT() will be executed to correctly initialize OFF state.

    ref_mgr->ref_state = REF_TO_OFF;
    ref_mgr->fsm.to_off.completed = true;

    // Initialize regulation RST source

    ref_mgr->reg_rst_source = REG_OPERATIONAL_RST_PARS;

    // Report successful initialization by returning zero

    return 0;
}



void refMgrResetFaultsRT(struct REF_mgr * const ref_mgr)
{
    // Clear fault latches

    polswitchReset(ref_mgr->polswitch_mgr);

    refRtRegFaultsRT(ref_mgr, false);       // false to reset faults latch

    if(ref_mgr->iter_cache.pc_state != REF_PC_ON)
    {
        ref_mgr->faults_in_off = ref_mgr->latched_faults;
    }
}



bool refMgrLatchUnfltrMeasRT(struct REF_mgr * const ref_mgr,
                             cc_float       * const meas,
                             uint32_t         const meas_index,
                             bool             const latch)
{
    struct REG_mgr * const reg_mgr         = ref_mgr->reg_mgr;
    bool           * const prev_meas_latch = &ref_mgr->prev_meas_latch[meas_index > 0];  // Map any non-zero value to 1

    if(latch == true && *prev_meas_latch == false)
    {
        // Latch unfiltered measurement according to the active regulation mode

        switch(ref_mgr->iter_cache.reg_mode)
        {
            case REG_VOLTAGE:

                *meas = reg_mgr->v.unfiltered;
                break;

            case REG_CURRENT:

                *meas = reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED];
                break;

            case REG_FIELD:

                *meas = reg_mgr->b.meas.signal[REG_MEAS_UNFILTERED];
                break;

            default:

                *meas = 0.0F;
                break;
        }

        // Remember that latch is now true

        *prev_meas_latch = true;

        // Return true to report that the measurement was just latched

        return true;
    }

    // Latch has not just transitioned from false to true

    *prev_meas_latch = latch;

    return false;
}



cc_float refMgrModeSetRT(struct REF_mgr * const ref_mgr, enum REG_mode const reg_mode, bool const use_average_v_ref)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    ref_mgr->iter_cache.reg_mode = regMgrModeSetRT(reg_mgr, reg_mode, use_average_v_ref);

    return *reg_mgr->ref;
}



// NON-RT   refMgrInitialRate

cc_float refMgrInitialRate(struct REF_mgr * const ref_mgr)
{
    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    cc_float initial_ref_rate;

    if(ref_mgr->iter_cache.reg_mode == REG_VOLTAGE)
    {
        // Reg mode is voltage - always ramp down from zero rate of change

        initial_ref_rate = 0.0F;
    }
    else
    {
        // Reg mode is current or field

        initial_ref_rate = reg_mgr->ref_rate;

        // Clip the initial rate to the rate limit - note this always applies, so if the rate limit
        // is zero, the ramp down will always start from a zero rate of change.

        cc_float const rate_limit = reg_mgr->lim_ref->rate;

        if(initial_ref_rate > rate_limit)
        {
            return rate_limit;
        }
        else if(initial_ref_rate < -rate_limit)
        {
            return -rate_limit;
        }
    }

    return initial_ref_rate;
}



void refMgrSetVfeedfwdRT(struct REF_mgr * const ref_mgr, struct CC_meas_signal vfeedfwd, struct CC_ns_time const timestamp)
{
    // If valid, scale the vfeedfwd signal using the gain, otherwise use the previous value

    if(vfeedfwd.is_valid == true)
    {
        vfeedfwd.signal *= refMgrParValue(ref_mgr, VFEEDFWD_GAIN);
    }
    else
    {
        vfeedfwd.signal = ref_mgr->vfeedfwd_delay.latest.signal;
    }

    // Save new sample for logging

    ref_mgr->vfeedfwd_delay.latest = vfeedfwd;

    // Calculate inverse sample period and save the new sample time

    ref_mgr->vfeedfwd_delay.inv_period_s = 1.0F
                                         / (cc_float)cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(timestamp, ref_mgr->vfeedfwd_delay.timestamp));
    ref_mgr->vfeedfwd_delay.timestamp = timestamp;

    // Put the new sample in the circular buffer

    uint32_t const buf_index = ++ref_mgr->vfeedfwd_delay.buf_index & REF_VFF_DELAY_BUF_LEN_MASK;

    ref_mgr->vfeedfwd_delay.signal  [buf_index] = vfeedfwd.signal;
    ref_mgr->vfeedfwd_delay.is_valid[buf_index] = vfeedfwd.is_valid;

    ccprintf("ts=%u.%09d  1/dT=%.2f  sig=%.3f\n"
            , timestamp.secs.abs
            , timestamp.ns
            , ref_mgr->vfeedfwd_delay.inv_period_s
            , vfeedfwd.signal
            );
}



void refMgrGetVffRT(struct REF_mgr * const ref_mgr, struct CC_meas_signal * const v_ff)
{
    // Calculate delay from the last sample to the time when we want to sample V_FF

    cc_float const delay_periods = refMgrParValue(ref_mgr, VFEEDFWD_DELAY_PERIODS)
                                 - ref_mgr->vfeedfwd_delay.inv_period_s
                                 * (cc_float)cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(ref_mgr->iter_time, ref_mgr->vfeedfwd_delay.timestamp));

    // If delay is less than zero then return most recent sample

    ccprintf("ffdp=%.3f  it=%u.%09d  ts=%u.%09d  dT=%.2f  dp=%.2f\n"
            , refMgrParValue(ref_mgr, VFEEDFWD_DELAY_PERIODS)
            , ref_mgr->iter_time.secs.abs
            , ref_mgr->iter_time.ns
            , ref_mgr->vfeedfwd_delay.timestamp.secs.abs
            , ref_mgr->vfeedfwd_delay.timestamp.ns
            , ref_mgr->vfeedfwd_delay.inv_period_s * (cc_float)cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(ref_mgr->iter_time, ref_mgr->vfeedfwd_delay.timestamp))
            , delay_periods
            );

    if(delay_periods <= 0.0F)
    {
        *v_ff = ref_mgr->vfeedfwd_delay.latest;
        return;
    }

    // If delay is longer than the buffer then return the oldest sample

    if(delay_periods >= (cc_float)REF_VFF_DELAY_BUF_LEN_MASK)
    {
        uint32_t const oldest_buf_index = (ref_mgr->vfeedfwd_delay.buf_index + 1) & REF_VFF_DELAY_BUF_LEN_MASK;

        v_ff->is_valid = ref_mgr->vfeedfwd_delay.is_valid[oldest_buf_index];
        v_ff->signal   = ref_mgr->vfeedfwd_delay.signal  [oldest_buf_index];
        return;
    }

    // Calculate the delay as integer and fractional parts

    cc_float delay_frac_iters = 0.0F;
    uint32_t delay_int_iters  = 0;
    cc_float delay_int;

    delay_frac_iters = modff(delay_periods, &delay_int);
    delay_int_iters  = (int32_t)delay_int;

    // Calculate indexes into the circular buffer for the segment to interpolate

    uint32_t const buf_index = ref_mgr->vfeedfwd_delay.buf_index;
    uint32_t const idx0      = (buf_index - delay_int_iters) & REF_VFF_DELAY_BUF_LEN_MASK;
    uint32_t const idx1      = (idx0 - 1) & REF_VFF_DELAY_BUF_LEN_MASK;

    // Calculate the extracted signal validity from the validity of the two samples of the segment

    v_ff->is_valid = ref_mgr->vfeedfwd_delay.is_valid[idx0] && ref_mgr->vfeedfwd_delay.is_valid[idx1];

    // Calculate the interpolated signal

    cc_float const s0 = ref_mgr->vfeedfwd_delay.signal[idx0];
    cc_float const s1 = ref_mgr->vfeedfwd_delay.signal[idx1];

    v_ff->signal = s0 + delay_frac_iters * (s1 - s0);

    ccprintf( "dp=%u %.2f  bi=%5u  ix0=%2u  ix1=%2u  b0=%.3f  b1=%.3f  b2=%.3f  b3=%.3f  s=%.4f\n"
            , delay_int_iters
            , delay_frac_iters
            , buf_index
            , idx0
            , idx1
            , ref_mgr->vfeedfwd_delay.signal[(buf_index - 0) & REF_VFF_DELAY_BUF_LEN_MASK]
            , ref_mgr->vfeedfwd_delay.signal[(buf_index - 1) & REF_VFF_DELAY_BUF_LEN_MASK]
            , ref_mgr->vfeedfwd_delay.signal[(buf_index - 2) & REF_VFF_DELAY_BUF_LEN_MASK]
            , ref_mgr->vfeedfwd_delay.signal[(buf_index - 3) & REF_VFF_DELAY_BUF_LEN_MASK]
            , v_ff->signal
            );
}


// NON-RT   refMgrVarPointerFunc

bool refMgrVarPointerFunc(struct REF_mgr * const ref_mgr,
                          uint8_t        * const base_address,
                          uint32_t               sub_sel,
                          uint32_t         const cyc_sel,
                          uint32_t         const sub_sel_step,
                          uint32_t         const cyc_sel_step,
                          uint8_t        **      var_pointer)
{
    uint8_t  * static_base_address;
    uint8_t  * app_base_address;
    uint32_t   struct_size;

    // If variable is outside the zone of libref fg_error, ref_status and ref_armed structures then return the address
    // with the optional offset for sub_sel and cyc_sel. If this isn't needed then sub_sel_step and/or cyc_sel_step can
    // be set to zero.

    if(base_address < (uint8_t *)&ref_mgr->static_vars || base_address >= (uint8_t *)&((&ref_mgr->static_vars)[1]))
    {
        *var_pointer = base_address + sub_sel * sub_sel_step + cyc_sel * cyc_sel_step;

        return false;
    }

    // Note the order of the fields fg_error, ref_status and ref_armed must match the order of the comparisons.

    if(base_address < (uint8_t *)&ref_mgr->static_vars.cyc_status)
    {
        // Access fg_error

        static_base_address = (uint8_t *)&ref_mgr->static_vars.fg_error;
        app_base_address    = (uint8_t *)ref_mgr->fg_error;
        struct_size         = sizeof(struct FG_error);
    }
    else if(base_address < (uint8_t *)&ref_mgr->static_vars.ref_armed)
    {
        // Access cyc_status

        static_base_address = (uint8_t *)&ref_mgr->static_vars.cyc_status;
        app_base_address    = (uint8_t *)ref_mgr->cyc_status;
        struct_size         = sizeof(struct REF_cyc_status);
    }
    else
    {
        // Access ref_armed

        static_base_address = (uint8_t *)&ref_mgr->static_vars.ref_armed;
        app_base_address    = (uint8_t *)ref_mgr->ref_armed;
        struct_size         = sizeof(struct REF_armed);
    }

    *var_pointer = base_address + (app_base_address - static_base_address) + (cyc_sel + sub_sel * ref_mgr->num_cyc_sels) * struct_size;

    return true;
}


// NON-RT   refMgrFgParPointerSubCycFunc

char * refMgrFgParPointerSubCycFunc(struct REF_mgr    * const ref_mgr,
                                       uint32_t            const sub_sel,
                                       uint32_t            const cyc_sel,
                                       enum REF_fg_par_idx const fg_par_idx)
{
    struct REF_fg_par const * const fg_par = &ref_mgr->u.fg_par[fg_par_idx];

    return fg_par->value + cyc_sel * fg_par->meta.cyc_sel_step + sub_sel * fg_par->meta.sub_sel_step;
}

// EOF
