//! @file  refStatePaused.c
//! @brief Converter Control Reference Manager library: PAUSED reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStatePausedInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_event_mgr    const * const event_mgr = &ref_mgr->event_mgr;
    struct REF_state_paused       * const paused    = &ref_mgr->fsm.paused;

    // Active can't be NULL! PAUSED needs an initial event to work with, otherwise all attempts to alter
    // the currently armed function (rearm now events) would fail

    CC_ASSERT(ref_mgr->event_mgr.active.func != NULL && ref_mgr->event_mgr.active.event != NULL);

    // Save current cycling variables - they will be restored on PD -> CY transition

    struct CC_ns_time pause_time;

    if(event_mgr->internal_pause_event.is_active)
    {
        pause_time = event_mgr->internal_pause_event.time;
    }
    else
    {
        pause_time = event_mgr->pause_event.time;
    }

    // Save current function time which will be fed to the function generator for the duration of PAUSED

    paused->fg_time_adv = ref_mgr->fg.time_adv;
    paused->ref         = ref_mgr->ref_fg_unlimited;

    // Adjust cached event time to account for the pause event time

    paused->cache.fg_event = ref_mgr->fg.event;
    paused->cache.fg_event.abs_time = cctimeNsSubRT(paused->cache.fg_event.abs_time, pause_time);

    // Reset initialization iteration counter

    paused->init_iter_counter = 0;
    paused->ramp_running      = false;

    return refStatePausedRT;
}



enum REF_state refStatePausedRT(struct REF_mgr * const ref_mgr)
{
    struct REF_state_paused * const paused = &ref_mgr->fsm.paused;

    // Keep processing cycling reference events, but never clear the active event

    enum REF_active_status const event_status = refEventMgrProcessCyclingEventsRT(&ref_mgr->event_mgr,
                                                                                  ref_mgr->iter_time,
                                                                                  false,                                // clear_active
                                                                                  true);                                // allow_use_arm_now

    // If the cycle is overrun by the next cycle then set the overrun flag to return to CYCLING immediately

    if(event_status == REF_ACTIVE_UPDATED)
    {
        ref_mgr->fsm.cycling.cycle_overrun = true;

        return REF_PAUSED;
    }

    // If USE_ARM_NOW function was just activated then prepare to ramp to the new reference value, if changed

    if(event_status == REF_ACTIVE_FUNC_UPDATED)
    {
        struct REF_container * const active = &ref_mgr->event_mgr.active;

        // Update function generation parameters

        //refMgrUpdateFgPars(ref_mgr, &active->func->ref_armed.fg_pars);

        cc_float const prev_paused_ref = paused->ref;

        // Calculate reference at the time of pausing with the newly rearmed function parameters and check if it has changed

        cc_double paused_fg_time_adv = cctimeNsRelTimeToDoubleRT(paused->fg_time_adv);

        fgFuncRT(&active->func->ref_armed.fg_pars, &paused_fg_time_adv, &paused->ref);

        if(paused->ref != prev_paused_ref)
        {
            // The reference changed so initialize a ramp to make a smooth connection between the reference levels

            refRampMgrInitRT(ref_mgr,
                             REF_START_RAMP_NOW,
                             0.0F,                          // initial_rate
                             ref_mgr->ref_fg_unlimited,
                             paused->ref,
                             REF_DONT_USE_ACC_FOR_DEC);

            paused->ramp_running = true;
        }
    }

    // Run the ramp manager until the ramp ends

    if(paused->ramp_running == true && refRampMgrRT(ref_mgr) == FG_POST_FUNC)
    {
        paused->ramp_running = false;
    }

    return REF_PAUSED;
}

// EOF
