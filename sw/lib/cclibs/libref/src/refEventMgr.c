//! @file  refEventMgr.c
//! @brief Converter Control Function Manager library Event functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"
#include "libref/refEvent.h"


static inline struct REF_container const * refEventMgrGetNextConstPointerRT(struct REF_event_mgr const * const event_mgr,
                                                                            enum REF_event_type          const event_type)
{
    return   event_type == REF_EVENT_USE_ARM_NOW
           ? &event_mgr->next_use_arm_now
           : &event_mgr->next_start_func;
}



struct REF_container * refEventMgrGetNextPointerRT(struct REF_event_mgr * const event_mgr,
                                                   enum REF_event_type    const event_type)
{
    return   event_type == REF_EVENT_USE_ARM_NOW
           ? &event_mgr->next_use_arm_now
           : &event_mgr->next_start_func;
}


// NON-RT  refEventMgrGetNextFuncBuffer

struct REF_func * refEventMgrGetNextFuncBuffer(struct REF_event_mgr * const event_mgr, enum REF_event_type const event_type)
{
    uint32_t const func_idx = event_type == REF_EVENT_USE_ARM_NOW
                            ? event_mgr->next_use_arm_now.func_idx
                            : event_mgr->next_start_func.func_idx;

    return &event_mgr->buffers.funcs[func_idx];
}


// NON-RT  refEventMgrGetNextEventBuffer

struct REF_event * refEventMgrGetNextEventBuffer(struct REF_event_mgr * const event_mgr)
{
    CC_STATIC_ASSERT(CC_ARRAY_LEN(event_mgr->buffers.events) == 2, code_expects_event_buffer_with_2_elements);

    return &event_mgr->buffers.events[1 - event_mgr->active.event_idx];
}



static void refEventMgrSwapNextFuncsRT(struct REF_event_mgr * const event_mgr)
{
    struct REF_container * use_arm_now = &event_mgr->next_use_arm_now;
    struct REF_container * start_func  = &event_mgr->next_start_func;

    CC_ASSERT(   use_arm_now->owner == REF_THREAD_RT
              && start_func->owner  == REF_THREAD_RT);

    uint32_t const temp_idx = start_func->func_idx;

    start_func->func_idx  = use_arm_now->func_idx;
    use_arm_now->func_idx = temp_idx;

    struct REF_func * const temp_func = start_func->func;

    start_func->func  = use_arm_now->func;
    use_arm_now->func = temp_func;
}



static void refEventMgrCancelNextFuncRT(struct REF_event_mgr * const event_mgr,
                                        enum REF_event_type    const event_type)
{
    struct REF_container * const next = refEventMgrGetNextPointerRT(event_mgr, event_type);

    next->func = NULL;

    CC_MEMORY_BARRIER;

    next->owner = REF_THREAD_BG;
}



//! Cancels pending event
//!
//! The function clears both the event structure and the function structure.
//!
//! @param[in,out]  event_mgr   Pointer to event manager structure
//! @param[in]      event_type  Type of the event which should be cancelled.
//!                             Can be REF_EVENT_START_FUNC or REF_EVENT_USE_ARM_NOW.

static void refEventMgrCancelNextRT(struct REF_event_mgr * const event_mgr,
                                    enum REF_event_type    const event_type)
{
    if(event_type != REF_EVENT_USE_ARM_NOW)
    {
        event_mgr->next_start_func.event = NULL;
    }

    refEventMgrCancelNextFuncRT(event_mgr, event_type);
}


// NON-RT  refEventMgrInit

void refEventMgrInit(struct REF_event_mgr * const event_mgr,
                     struct FG_point      * const running_table_function,
                     uint32_t               const max_table_points)
{
    refEventMgrClearActiveRT(event_mgr);

    event_mgr->next_start_func.event  = NULL;
    event_mgr->next_start_func.func   = NULL;
    event_mgr->next_start_func.owner  = REF_THREAD_BG;

    event_mgr->next_use_arm_now.event = NULL;
    event_mgr->next_use_arm_now.func  = NULL;
    event_mgr->next_use_arm_now.owner = REF_THREAD_BG;

    event_mgr->next_start_func.func_idx  = 0;
    event_mgr->next_use_arm_now.func_idx = 1;
    event_mgr->active.func_idx           = 2;

    event_mgr->buffers.funcs[0].table_function = running_table_function;
    event_mgr->buffers.funcs[1].table_function = running_table_function + max_table_points;
    event_mgr->buffers.funcs[2].table_function = running_table_function + max_table_points * 2;
}



void refEventMgrClearActiveRT(struct REF_event_mgr * const event_mgr)
{
    event_mgr->active.event = NULL;
    event_mgr->active.func  = NULL;
    event_mgr->active.owner = REF_THREAD_RT;
}



struct REF_func * refEventMgrActivateNextFuncRT(struct REF_event_mgr * const event_mgr,
                                                enum REF_event_type    const event_type)
{
    struct REF_container * const next = refEventMgrGetNextPointerRT(event_mgr, event_type);

    if(event_type == REF_EVENT_START_FUNC && next->discard_func == true)
    {
        // Discard the next function and repeat the active function

        refEventMgrCancelNextFuncRT(event_mgr, REF_EVENT_START_FUNC);

        struct REF_func * const active_func = event_mgr->active.func;

        // If the active function is a TABLE then reset the key variables including segment index

        if(active_func->ref_armed.fg_pars.meta.type == FG_TABLE)
        {
            fgTableResetParsRT(&active_func->ref_armed.fg_pars);
        }
    }
    else
    {
        // Swap active and next indexes

        uint32_t const temp_func_idx = event_mgr->active.func_idx;
        event_mgr->active.func_idx   = next->func_idx;
        next->func_idx               = temp_func_idx;

        // Set the convenience pointer

        event_mgr->active.func = next->func;
        next->func             = NULL;

        // Transfer ownership over next to the background

        CC_MEMORY_BARRIER;

        next->owner = REF_THREAD_BG;
    }

    return event_mgr->active.func;
}


// NON-RT  refEventMgrRegisterFuncBuffer

void refEventMgrRegisterFuncBuffer(struct REF_event_mgr * const event_mgr,
                                   struct REF_func      * const next_func,
                                   enum REF_event_type    const event_type)
{
    struct REF_container * const next = refEventMgrGetNextPointerRT(event_mgr, event_type);

    CC_ASSERT(next->func  == NULL);
    CC_ASSERT(next->owner == REF_THREAD_BG);

    next->func         = next_func;
    next->discard_func = false;
}


// NON-RT  refEventMgrRegisterNextFunc

void refEventMgrRegisterNextFunc(struct REF_event_mgr * const event_mgr,
                                 enum REF_event_type    const event_type)
{
    struct REF_container * const next = refEventMgrGetNextPointerRT(event_mgr, event_type);

    // Transfer ownership of the buffer to the real-time thread

    CC_MEMORY_BARRIER;

    next->owner = REF_THREAD_RT;
}



struct REF_event * refEventMgrActivateNextEventRT(struct REF_event_mgr * const event_mgr)
{
    CC_STATIC_ASSERT(CC_ARRAY_LEN(event_mgr->buffers.events) == 2, code_expects_event_buffer_with_2_elements);

    uint32_t const next_event_idx = 1 - event_mgr->active.event_idx;
    event_mgr->active.event_idx   = next_event_idx;
    event_mgr->active.event       = &event_mgr->buffers.events[next_event_idx];

    event_mgr->next_start_func.event = NULL;

    return event_mgr->active.event;
}


// NON-RT  refEventMgrNextIsPending

bool refEventMgrNextIsPending(struct REF_event_mgr const * const event_mgr,
                              enum REF_event_type          const event_type)
{
    return refEventMgrGetNextConstPointerRT(event_mgr, event_type)->owner == REF_THREAD_RT;
}



bool refEventMgrNextIsPendingRT(struct REF_event_mgr const * const event_mgr,
                                enum REF_event_type          const event_type)
{
    struct REF_container const * const next = refEventMgrGetNextConstPointerRT(event_mgr, event_type);

    if(next->owner == REF_THREAD_RT)
    {
        // In case of start_func event type, there may be a start_func or a use_arm_now event pending

        if(event_type == REF_EVENT_START_FUNC)
        {
            return next->func->event_type != REF_EVENT_RUN;
        }
        else
        {
            return next->func->event_type == event_type;
        }
    }

    return false;
}



static bool refEventMgrFuncsMatchRT(struct REF_func const * const func1, struct REF_func const * const func2)
{
    struct REF_armed const * const armed1 = &func1->ref_armed;
    struct REF_armed const * const armed2 = &func2->ref_armed;

    return (   func1->cyc_sel                == func2->cyc_sel
            && func1->sub_sel                == func2->sub_sel
            && func1->ref_cyc_sel            == func2->ref_cyc_sel
            && armed1->reg_mode              == armed2->reg_mode
            && armed1->fg_pars.meta.type     == armed2->fg_pars.meta.type
            && (   armed1->fg_pars.meta.polarity       == armed2->fg_pars.meta.polarity
                || armed1->fg_pars.meta.polswitch_auto == false));
}



enum REF_active_status refEventMgrProcessCyclingEventsRT(struct REF_event_mgr * const event_mgr,
                                                         struct CC_ns_time      const iter_time,
                                                         bool                   const clear_active,
                                                         bool                   const allow_use_arm_now)
{
    // Check which events are pending

    struct REF_container * const active                = &event_mgr->active;
    struct REF_container * const next_start_func       = &event_mgr->next_start_func;
    struct REF_func      * const next_use_arm_now_func = event_mgr->next_use_arm_now.func;

    bool next_start_func_pending = refEventMgrNextIsPendingRT(event_mgr, REF_EVENT_START_FUNC);

    // Clear active event and func if requested, unless active will be needed for the next cycle because of USE_ARM_NOW

    if(   clear_active == true
       && (   next_start_func_pending       == false
           || next_start_func->discard_func == false))
    {
        refEventMgrClearActiveRT(event_mgr);
    }

    bool use_arm_now_pending            = false;
    bool use_arm_now_matches_active     = false;
    bool use_arm_now_matches_start_func = false;

    if(allow_use_arm_now == true)
    {
        use_arm_now_pending = refEventMgrNextIsPendingRT(event_mgr, REF_EVENT_USE_ARM_NOW);

        // if USE_ARM_NOW event is pending than compare with active and next START_FUNC events

        if(use_arm_now_pending == true)
        {
            use_arm_now_matches_active     = active->event != NULL
                                           ? refEventMgrFuncsMatchRT(next_use_arm_now_func, active->func)
                                           : false;

            use_arm_now_matches_start_func = next_start_func_pending == true
                                           ? refEventMgrFuncsMatchRT(next_use_arm_now_func, next_start_func->func)
                                           : false;
        }
    }

    // Check if the active event needs to be updated

    enum REF_active_status status = REF_ACTIVE_NOT_UPDATED;

    if(use_arm_now_matches_active == true)
    {
        // USE_ARM_NOW matches active so update the active function (but not the event timing)

        status = REF_ACTIVE_FUNC_UPDATED;

        // USE_ARM_NOW is only used for TABLE functions and the next table must inherit the segment index of the active TABLE

        if(   active->func->ref_armed.fg_pars.meta.type == FG_TABLE
           && next_use_arm_now_func->ref_armed.fg_pars.meta.type == FG_TABLE)
        {
            fgTableReplaceParsRT(&next_use_arm_now_func->ref_armed.fg_pars, &active->func->ref_armed.fg_pars);
        }

        // Update only the function of the active event

        refEventMgrActivateNextFuncRT(event_mgr, REF_EVENT_USE_ARM_NOW);

        // If USE_ARM_NOW matches the next START_FUNC, then discard the START_FUNC event
        // as it will be older than the USE_ARM_NOW

        if(use_arm_now_matches_start_func == true)
        {
            next_start_func->discard_func = true;
        }
    }
    else if(use_arm_now_matches_start_func == true)
    {
        // USE_ARM_NOW matches the next START_FUNC but does not match active, so
        // swap START_FUNC and USE_ARM_NOW functions so that the newer USE_ARM_NOW
        // function will be activated on the next cycle

        refEventMgrSwapNextFuncsRT(event_mgr);
    }

    // Always discard the USE_ARM_NOW as it is has either been used or needs to be ignored

    if(use_arm_now_pending == true)
    {
        refEventMgrCancelNextFuncRT(event_mgr, REF_EVENT_USE_ARM_NOW);
    }

    // If next START_FUNC is pending, then check if it should be activated

    if(next_start_func_pending == true)
    {
        // Calculate if the time has arrived to activate the pending event

        bool const start_func_time_passed = cctimeNsCmpAbsTimesRT(iter_time, next_start_func->event->activation_time) > 0;

        if(start_func_time_passed == true || active->event == NULL)
        {
            // Activate the pending START_FUNC event

            refEventMgrActivateNextEventRT(event_mgr);
            refEventMgrActivateNextFuncRT (event_mgr, REF_EVENT_START_FUNC);

            status = REF_ACTIVE_UPDATED;
        }
    }

    // If not event is now active then report that active is empty

    if(active->event == NULL)
    {
        status = REF_ACTIVE_EMPTY;
    }

    return status;
}



static inline void refEventMgrResetStaleActionEventRT(struct REF_action_event * const event,
                                                      enum REF_state            const ref_mode,
                                                      uint32_t                  const ref_state_mask)
{
    if(event->is_active || refEventMgrRefStateIsValidRT(event->type, ref_mode, ref_state_mask) == false)
    {
        event->is_active  = false;
        event->is_pending = false;
    }
}



static void refEventMgrResetStaleNextEventRT(struct REF_event_mgr * const event_mgr,
                                             enum REF_event_type    const event_type,
                                             enum REF_state         const ref_mode,
                                             uint32_t               const ref_state_mask)
{
    // It may happen that the background event preparation had started in a proper state, but ended in an invalid one.
    // Example: started in CY and ended in TO.

    if(   refEventMgrNextIsPendingRT(event_mgr, event_type) == true
       && refEventMgrRefStateIsValidRT(event_type, ref_mode, ref_state_mask) == false)
    {
        refEventMgrCancelNextRT(event_mgr, event_type);
    }
}



void refEventMgrResetStaleEventsRT(struct REF_event_mgr * const event_mgr,
                                   enum REF_state         const ref_mode,
                                   uint32_t               const ref_state_mask)
{
    refEventMgrResetStaleActionEventRT(&event_mgr->internal_pause_event, ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->pause_event,          ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->unpause_event,        ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->coast_event,          ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->recover_event,        ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->start_harmonics_event,ref_mode, ref_state_mask);
    refEventMgrResetStaleActionEventRT(&event_mgr->stop_harmonics_event, ref_mode, ref_state_mask);

    refEventMgrResetStaleNextEventRT(event_mgr, REF_EVENT_RUN,         ref_mode, ref_state_mask);
    refEventMgrResetStaleNextEventRT(event_mgr, REF_EVENT_START_FUNC,  ref_mode, ref_state_mask);
    refEventMgrResetStaleNextEventRT(event_mgr, REF_EVENT_USE_ARM_NOW, ref_mode, ref_state_mask);
}



void refEventMgrActivateActionEventsRT(struct REF_event_mgr * const event_mgr, struct CC_ns_time const time)
{
    // Note: unpause time must be exceeded before returning to cycling

    event_mgr->pause_event.is_active           = (event_mgr->pause_event.is_pending           && cctimeNsCmpAbsTimesRT(time, event_mgr->pause_event.time          ) >= 0);
    event_mgr->unpause_event.is_active         = (event_mgr->unpause_event.is_pending         && cctimeNsCmpAbsTimesRT(time, event_mgr->unpause_event.time        ) >  0);
    event_mgr->coast_event.is_active           = (event_mgr->coast_event.is_pending           && cctimeNsCmpAbsTimesRT(time, event_mgr->coast_event.time          ) >= 0);
    event_mgr->recover_event.is_active         = (event_mgr->recover_event.is_pending         && cctimeNsCmpAbsTimesRT(time, event_mgr->recover_event.time        ) >= 0);
    event_mgr->start_harmonics_event.is_active = (event_mgr->start_harmonics_event.is_pending && cctimeNsCmpAbsTimesRT(time, event_mgr->start_harmonics_event.time) >= 0);
    event_mgr->stop_harmonics_event.is_active  = (event_mgr->stop_harmonics_event.is_pending  && cctimeNsCmpAbsTimesRT(time, event_mgr->stop_harmonics_event.time ) >= 0);
}



void refEventMgrTriggerInternalPauseEventRT(struct REF_event_mgr * const event_mgr, struct CC_ns_time const event_time)
{
    struct REF_action_event * const internal_pause = &event_mgr->internal_pause_event;

    internal_pause->is_active  = true;
    internal_pause->is_pending = true;
    internal_pause->type       = REF_EVENT_PAUSE;
    internal_pause->time       = event_time;
}



bool refEventMgrRefStateIsValidRT(enum REF_event_type const event_type,
                                  enum REF_state      const ref_mode,
                                  uint32_t            const ref_state_mask)
{
    static uint32_t const valid_ref_state_masks[] =
    {
        /* REF_EVENT_NONE            */  0                                                                                                                                           ,
        /* REF_EVENT_START_FUNC      */  REF_TO_OFF_MASK  | REF_TO_CYCLING_MASK | REF_CYCLING_MASK | REF_POL_SWITCHING_MASK | REF_PAUSED_MASK | REF_DYN_ECO_MASK | REF_FULL_ECO_MASK ,
        /* REF_EVENT_USE_ARM_NOW     */                                           REF_CYCLING_MASK | REF_POL_SWITCHING_MASK | REF_PAUSED_MASK                                        ,
        /* REF_EVENT_RUN             */  REF_ARMED_MASK                                                                                                                              ,
        /* REF_EVENT_ABORT           */  REF_RUNNING_MASK                       | REF_CYCLING_MASK | REF_POL_SWITCHING_MASK | REF_PAUSED_MASK | REF_DYN_ECO_MASK                     ,
        /* REF_EVENT_PAUSE           */                     REF_TO_CYCLING_MASK | REF_CYCLING_MASK | REF_POL_SWITCHING_MASK | REF_PAUSED_MASK                                        ,
        /* REF_EVENT_UNPAUSE         */                     REF_TO_CYCLING_MASK | REF_CYCLING_MASK | REF_POL_SWITCHING_MASK | REF_PAUSED_MASK                                        ,
        /* REF_EVENT_COAST           */ ~0                                                                                                                                           ,
        /* REF_EVENT_RECOVER         */ ~0                                                                                                                                           ,
        /* REF_EVENT_START_HARMONICS */ ~0                                                                                                                                           ,
        /* REF_EVENT_STOP_HARMONICS  */ ~0                                                                                                                                           ,
    };

    CC_STATIC_ASSERT(CC_ARRAY_LEN(valid_ref_state_masks) == REF_NUM_EVENT_TYPES, mismatch_between_valid_ref_state_mask_array_size_and_number_of_enum_elements);

    static uint32_t const valid_ref_mode_masks[] =
    {
        /* REF_EVENT_NONE            */  0                                ,
        /* REF_EVENT_START_FUNC      */                  REF_CYCLING_MASK ,
        /* REF_EVENT_USE_ARM_NOW     */                  REF_CYCLING_MASK ,
        /* REF_EVENT_RUN             */  REF_IDLE_MASK                    ,
        /* REF_EVENT_ABORT           */  REF_IDLE_MASK | REF_CYCLING_MASK ,
        /* REF_EVENT_PAUSE           */                  REF_CYCLING_MASK ,
        /* REF_EVENT_UNPAUSE         */                  REF_CYCLING_MASK ,
        /* REF_EVENT_COAST           */                  REF_CYCLING_MASK ,
        /* REF_EVENT_RECOVER         */  REF_IDLE_MASK                    ,
        /* REF_EVENT_START_HARMONICS */ ~0                                ,
        /* REF_EVENT_STOP_HARMONICS  */ ~0                                ,
    };

    CC_STATIC_ASSERT(CC_ARRAY_LEN(valid_ref_mode_masks) == REF_NUM_EVENT_TYPES, mismatch_between_valid_ref_mode_array_size_and_number_of_enum_elements);

    return    ( ref_state_mask & valid_ref_state_masks[event_type]) != 0
           && ((1 << ref_mode) & valid_ref_mode_masks [event_type]) != 0;
}


// NON-RT   refEventMgrRegisterNextEvent

void refEventMgrRegisterNextEvent(struct REF_event_mgr * const event_mgr, struct REF_event * const next_event)
{
    event_mgr->next_start_func.event = next_event;
}


// NON-RT   refEventMgrSetExtEvtFlags

bool refEventMgrSetExtEvtFlags(struct REF_event_mgr * const event_mgr, bool const status)
{
    // Always set two bits in the external event received flag to ensure it will be logged

    event_mgr->ext_evt_rcvd = 3;

    // If status is true then set two bits in the external event ok flag to ensure it will be logged

    if(status == true)
    {
        event_mgr->ext_evt_ok = 3;
    }

    return status;
}

// EOF
