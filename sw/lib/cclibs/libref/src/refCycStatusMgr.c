//! @file  refCycStatusMgr.c
//! @brief Converter Control Function Manager library function generation functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include <libref.h>

// Static constants

static uint32_t const cycling_states_bit_mask = (REF_TO_CYCLING_MASK |  REF_CYCLING_MASK | REF_PAUSED_MASK | REF_POL_SWITCHING_MASK | REF_DYN_ECO_MASK);
static uint32_t const cycle_warning_bit_mask  = (REF_CYC_REG_WRN_BIT_MASK | REF_CYC_WAS_OVERRUN_BIT_MASK | REF_CYC_NO_PRE_FUNC_BIT_MASK | REF_CYC_PRE_FUNC_WRN_BIT_MASK);
static uint32_t const cycle_counter_bit_mask  = (REF_CYC_REG_FLT_BIT_MASK | REF_CYC_ECO_DYN_BIT_MASK | REF_CYC_ECO_ONCE_BIT_MASK | REF_CYC_INCOMPLETE_WRN_BIT_MASK);



bool refCycStatusMgrRT(struct REF_mgr * const ref_mgr)
{
    // Check if reference state has transitioned away from a CYCLING state

    bool const in_cycling_state     = ref_mgr->iter_cache.ref_mode == REF_CYCLING && (ref_mgr->ref_state_mask & cycling_states_bit_mask) != 0;
    bool const exited_cycling_state = in_cycling_state == false && ref_mgr->cyc_status_mgr.prev_in_cycling_state == true;

    ref_mgr->cyc_status_mgr.prev_in_cycling_state = in_cycling_state;

    if(exited_cycling_state == true)
    {
        // Exited cycling state before the end of the current cycle so set finish_early flag

        ccprintf("\n  ref_mgr->ref_state=%u Exited cycling state.\n",ref_mgr->ref_state);

        refCycStatusMgrFinishRT(ref_mgr, true);
    }

    return exited_cycling_state;
}


// NON-RT   refCycStatusMgrNext

void refCycStatusMgrNext(struct REF_mgr * const ref_mgr)
{
    // Initialize pointer to cyc_status structure for sub_sel, cyc_sel

    struct REF_event * const next_event = ref_mgr->event_mgr.next_start_func.event;
    struct REF_func  * const next_func  = ref_mgr->event_mgr.next_start_func.func;

    CC_ASSERT(next_event != NULL);

    // Initialise the next cyc_status structure

    uint32_t              const next_idx = 1 - ref_mgr->cyc_status_mgr.active_idx;
    struct REF_cyc_data * const next     = &ref_mgr->cyc_status_mgr.cyc_data_buffers[next_idx];

    // Set the fields that are already known

    next->fg_type              = next_func->ref_armed.fg_pars.meta.type;
    next->reg_mode             = next_func->ref_armed.reg_mode;
    next->event_us_time        = cctimeNsToUsRT(next_event->abs_time);
    next->status_bit_mask      = 0;
    next->max_abs_reg_err      = 0.0F;
    next->pre_func_plateau_err = 0.0F;

    refMgrVarPointerFunc(ref_mgr,
                         (uint8_t *)&ref_mgr->static_vars.cyc_status,
                         next_event->sub_sel,
                         next_event->cyc_sel,
                         0,
                         0,
                         (uint8_t **)&ref_mgr->cyc_status_mgr.archive[next_idx]);

    // Record in status if FULL_ECO ONCE is active

    if(ref_mgr->fsm.full_eco.once == true)
    {
        next->status_bit_mask = REF_CYC_ECO_ONCE_BIT_MASK;
    }

    ccprintf("\n  full_eco.once=%u  status_bit_mask=%x\n"
            , ref_mgr->fsm.full_eco.once
            , next->status_bit_mask
            );

    // Initialise regulation error fault and warning limits according to the regulation mode

    struct REG_err * reg_err_lim;

    switch(next_func->ref_armed.reg_mode)
    {
        case REG_FIELD:

            reg_err_lim = &ref_mgr->reg_mgr->b.reg_err;
            break;

        case REG_CURRENT:

            reg_err_lim = &ref_mgr->reg_mgr->i.reg_err;
            break;

        default:

            reg_err_lim = &ref_mgr->reg_mgr->v.reg_err;
            break;
    }

    ref_mgr->cyc_status_mgr.err_fault_limit[next_idx] = reg_err_lim->fault.threshold > 0.0F
                                                      ? reg_err_lim->fault.threshold
                                                      : 1.0E+30F;

    ref_mgr->cyc_status_mgr.err_warning_limit[next_idx] = reg_err_lim->warning.threshold > 0.0F
                                                        ? reg_err_lim->warning.threshold
                                                        : 1.0E+30F;
}



void refCycStatusMgrActivateRT(struct REF_cyc_status_mgr * const cyc_status_mgr)
{
    // Called at the start of a cycle to activate next by toggling the active index (0 -> 1 -> 0 -> 1 ...)

    cyc_status_mgr->active_idx = 1 - cyc_status_mgr->active_idx;

    ccprintf("\n   active_idx=%u\n",cyc_status_mgr->active_idx);
}



bool refCycStatusMgrFinishRT(struct REF_mgr * const ref_mgr, bool const finish_early)
{
    uint32_t                const active_idx         = ref_mgr->cyc_status_mgr.active_idx;
    struct REF_cyc_status * const archive_cyc_status = ref_mgr->cyc_status_mgr.archive[active_idx];

    ccprintf("\n  finish_early=%u  (archive_cyc_status==NULL)=%u\n"
            , finish_early
            , archive_cyc_status == NULL
            );

    // Return immediately if the active cyc_status was already saved

    if(archive_cyc_status == NULL)
    {
        return false;
    }

    // Prepare status_bit_mask with additional flags based on the regulation error and the finish_early flag

    struct REF_cyc_data * const active_cyc_data = &ref_mgr->cyc_status_mgr.cyc_data_buffers[active_idx];

    uint32_t status_bit_mask;

    if(active_cyc_data->max_abs_reg_err > ref_mgr->cyc_status_mgr.err_fault_limit[active_idx])
    {
        status_bit_mask = REF_CYC_REG_FLT_BIT_MASK;
    }
    else if(active_cyc_data->max_abs_reg_err > ref_mgr->cyc_status_mgr.err_warning_limit[active_idx])
    {
        status_bit_mask = REF_CYC_REG_WRN_BIT_MASK;
    }
    else
    {
        status_bit_mask = REF_CYC_REG_OK_BIT_MASK;
    }

    if(finish_early == true)
    {
        status_bit_mask = REF_CYC_INCOMPLETE_WRN_BIT_MASK;
    }

    active_cyc_data->status_bit_mask |= status_bit_mask;

    // Prepare cycle warning flag that is true for any warning (pre-function or function)

    bool const cycle_warning = (active_cyc_data->status_bit_mask & cycle_warning_bit_mask) != 0;

    ccprintf("\n  status_bit_mask=%x  cyc_wrn_mask=%x  cyc_wrn=%u\n"
            , active_cyc_data->status_bit_mask
            , cycle_warning_bit_mask
            , cycle_warning
            );

    // Increment counters only if FULL_ECO, DYN_ECO and REG_FLT are inactive

    if((active_cyc_data->status_bit_mask & cycle_counter_bit_mask) == 0)
    {
        // Increment counter for the global cycle counter (sub_sel=cyc_sel=0) and the active sub_sel and cyc_sel

        ref_mgr->cyc_status->counter++;
        archive_cyc_status->counter++;
    }

    // Set ILC structure pointer so that it can be set by the background calling refCycStatusMgrSetIlc() in the future

    ref_mgr->cyc_status_mgr.ilc = &archive_cyc_status->ilc;

    // Store completed cyc_data in the global cyc_data (sub_sel=cyc_sel=0) and in the archive for [sub_sel][cyc_sel]

    ref_mgr->cyc_status->data = *active_cyc_data;
    archive_cyc_status->data  = *active_cyc_data;

    // Reset active archive pointer to mark that the cyc_status for the active_idx has been saved

    ref_mgr->cyc_status_mgr.archive[active_idx] = NULL;

    // return cycle warning flag

    return cycle_warning;
}



void refCycStatusMgrSetStatusBitMaskRT(struct REF_cyc_status_mgr * const cyc_status_mgr,
                                       uint32_t                    const status_bit_mask)
{
    cyc_status_mgr->cyc_data_buffers[cyc_status_mgr->active_idx].status_bit_mask |= status_bit_mask;
}



void refCycStatusMgrSetPreFuncPlateaurErrRT(struct REF_cyc_status_mgr * const cyc_status_mgr,
                                            cc_float                    const pre_func_plateau_err)
{
    cyc_status_mgr->cyc_data_buffers[cyc_status_mgr->active_idx].pre_func_plateau_err = pre_func_plateau_err;
}



void refCycStatusMgrSetMaxAbsRegErrRT(struct REF_cyc_status_mgr * const cyc_status_mgr,
                                      cc_float                    const max_abs_reg_err)
{
    cyc_status_mgr->cyc_data_buffers[cyc_status_mgr->active_idx].max_abs_reg_err = max_abs_reg_err;
}


// NON-RT   refCycStatusMgrSetIlc

void refCycStatusMgrSetIlc(struct REF_mgr            * const ref_mgr,
                           struct REF_ilc_cyc_status * const ilc)
{
    // Save the ILC cyc_status data in the global cyc_status (sub_sel=cyc_sel=0) and in the cyc_status for the last active cycle

    ref_mgr->cyc_status->ilc = *ilc;

    *ref_mgr->cyc_status_mgr.ilc = *ilc;
}

// EOF
