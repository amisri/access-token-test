//! @file  refStateArmed.c
//! @brief Converter Control Reference Manager library: ARMED reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStateArmedInitRT(struct REF_mgr * const ref_mgr)
{
    // Discard all events on first call

    refEventMgrClearActiveRT(&ref_mgr->event_mgr);

    // Reset REF_RUN on entry in to ARMED state

    refMgrParValue(ref_mgr, REF_RUN) = cc_zero_us;

    return refStateArmedRT;
}



enum REF_state refStateArmedRT(struct REF_mgr * const ref_mgr)
{
    return REF_ARMED;
}

// EOF
