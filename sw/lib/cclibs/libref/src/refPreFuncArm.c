//! @file  refPreFuncArm.c
//! @brief Converter Control Reference Manager library pre-function arming functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

//! Segment start_time constants

static cc_float const WAIT              =  0.0F;                //!< Wait on this plateau
static cc_float const NO_PLATEAU        =  1.0E-12F;            //!< Continue to next segment immediately with no plateau

//! Segment RMS constants

static cc_float const SEQ_TOO_LONG      = -1.0F;                //!< Sequence is too long for the time available

//! Static function declarations

static void refPreFuncArmPrepareRamp      (struct REF_mgr *, struct REF_event *);
static void refPreFuncArmPrepareMinrms    (struct REF_mgr *, struct REF_event *);
static void refPreFuncArmPrepareMagcycle  (struct REF_mgr *, struct REF_event *);
static void refPreFuncArmPrepareUpminmax  (struct REF_mgr *, struct REF_event *);
static void refPreFuncArmPrepareDownmaxmin(struct REF_mgr *, struct REF_event *);

//! Pointers to pre-function sequence preparation functions for the different pre-function modes

typedef void (*REF_prepare_seq)(struct REF_mgr *, struct REF_event *);

static REF_prepare_seq const prepare_sequence[] =
{   /* Pre-function modes */
    /* RAMP               */  refPreFuncArmPrepareRamp       ,
    /* MINRMS             */  refPreFuncArmPrepareMinrms     ,
    /* OPENLOOP_MINRMS    */  refPreFuncArmPrepareMinrms     ,
    /* MAGCYCLE           */  refPreFuncArmPrepareMagcycle   ,
    /* UPMINMAX           */  refPreFuncArmPrepareUpminmax   ,
    /* DOWNMAXMIN         */  refPreFuncArmPrepareDownmaxmin ,
};

CC_STATIC_ASSERT(CC_ARRAY_LEN(prepare_sequence) == REF_PRE_FUNC_NUM_MODES, size_of_prepare_sequence_array_doesnt_correspond_to_REF_PRE_FUNC_NUM_MODES);

//! Final reference indexes for the different types of segment

static enum REF_pre_func_defaults const final_ref_indexes[] =
{   /*      Segment Types     */
    /* NEGMINRMS_TO_MIN       */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ,
    /* POSMINRMS_TO_MIN       */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ,
    /* MAX_TO_MIN             */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ,
    /* MIN_TO_NEGMINRMS       */  REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS  ,
    /* POSMINRMS_TO_NEGMINRMS */  REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS  ,
    /* MAX_TO_NEGMINRMS       */  REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS  ,
    /* MIN_TO_POSMINRMS       */  REF_PRE_FUNC_DEFAULT_POS_MIN_RMS  ,
    /* NEGMINRMS_TO_POSMINRMS */  REF_PRE_FUNC_DEFAULT_POS_MIN_RMS  ,
    /* MAX_TO_POSMINRMS       */  REF_PRE_FUNC_DEFAULT_POS_MIN_RMS  ,
    /* MIN_TO_MAX             */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ,
    /* NEGMINRMS_TO_MAX       */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ,
    /* POSMINRMS_TO_MAX       */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ,
    /* REF1_TO_REF1           */  REF_PRE_FUNC_NUM_DEFAULTS         ,
    /* REF1_TO_MIN            */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ,
    /* REF1_TO_NEGMINRMS      */  REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS  ,
    /* REF1_TO_POSMINRMS      */  REF_PRE_FUNC_DEFAULT_POS_MIN_RMS  ,
    /* REF1_TO_MAX            */  REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ,
    /* XXX_TO_REF2            */  REF_PRE_FUNC_NUM_DEFAULTS         ,
};

CC_STATIC_ASSERT(CC_ARRAY_LEN(final_ref_indexes) == REF_PRE_FUNC_NUM_SEG_TYPES, size_of_prepare_sequence_array_doesnt_correspond_to_REF_PRE_FUNC_NUM_SEG_TYPES);

//! Calculate the duration and RMS of a ramp by arming a PLEP function
//!
//! NON-RT   refPreFuncArmRamp
//!
//! A PLEP function can be used to model the behaviour of a RAMP function, provide the load
//! resistance is well known. It will not model the effects of magnet saturation and will over-estimate
//! the duration and RMS if the saturation is significant.
//!
//! The function uses the voltage limits to set the PLEP EXP_FINAL parameter, using the load resistance
//! and Ohm's law: I = V / R
//!
//! @param[in]   ref_mgr             Pointer to REF_mgr structure
//! @param[in]   initial_ref         Initial reference
//! @param[in]   initial_rate        Initial rate of change of the reference
//! @param[in]   final_ref           Final reference
//! @param[in]   use_acc_for_dec     True to use default acceleration for the deceleration
//! @param[out]  plep_pars           Pointer to PLEP parameters structure
//!
//! @returns    Time integral of the reference squared

static cc_float refPreFuncArmRamp(struct REF_mgr * const ref_mgr,
                                  cc_float         const initial_ref,
                                  cc_float         const initial_rate,
                                  cc_float         const final_ref,
                                  bool             const use_acc_for_dec,
                                  union FG_pars  * const plep_pars)
{
    struct REG_mgr          * const reg_mgr      =  ref_mgr->reg_mgr;
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;

    // Calculate voltage limit according to the polarity switch and ramp direction

    bool const invert_limits  = polswitchIsAutomatic(ref_mgr->polswitch_mgr) == true
                              ? final_ref < 0.0F
                              : polswitchIsNegative(ref_mgr->polswitch_mgr);

    bool const ascending_flag = (final_ref >= initial_ref);

    // Set v_ref to theoretical clipped voltage

    cc_float v_ref;

    if(invert_limits == true)
    {
        // Invert voltage limits

        v_ref = ascending_flag == true ? -reg_mgr->v.lim_ref.min_clip : -reg_mgr->v.lim_ref.max_clip;
    }
    else
    {
        // Do not invert limits

        v_ref = ascending_flag == true ?  reg_mgr->v.lim_ref.max_clip :  reg_mgr->v.lim_ref.min_clip;
    }

    // Calculate the final reference of an exponential response using Ohms law: I=V/R

    cc_float exp_final = v_ref * reg_mgr->load_pars.gain2;

    if(pre_func_arm->reg_mode == REG_FIELD)
    {
        exp_final *= reg_mgr->load_pars.gauss_per_amp;
    }

    ccprintf("\n  initial_ref=%.6f  final_ref=%.6f  ascending_flag=%u  is_negative=%u  v_ref=%.4f  exp_final=%.6f  exp_tc=%.7E\n"
            , initial_ref
            , final_ref
            , ascending_flag
            , polswitchIsNegative(ref_mgr->polswitch_mgr)
            , v_ref
            , exp_final
            , reg_mgr->load_pars.tc
            );

    // Use the acceleration for the deceleration when required by the calling function

    cc_float * const defaults = pre_func_arm->pre_func_reg_mode->defaults;

    cc_float const deceleration = use_acc_for_dec == true
                                ? defaults[REF_PRE_FUNC_DEFAULT_ACCELERATION]
                                : defaults[REF_PRE_FUNC_DEFAULT_DECELERATION];

    // Arm a PLEP for the segment to estimate the duration and RMS

    struct FG_error fg_error;

    fgPlepArm(NULL,
              polswitchIsAutomatic(ref_mgr->polswitch_mgr),
              polswitchIsNegative(ref_mgr->polswitch_mgr),
              false,        // test_arm
              initial_rate,
              0.0F,         // final_rate
              initial_ref,
              final_ref,
              defaults[REF_PRE_FUNC_DEFAULT_ACCELERATION],
              deceleration,
              defaults[REF_PRE_FUNC_DEFAULT_LINEAR_RATE],
              reg_mgr->load_pars.tc,
              exp_final,
              plep_pars,
              &fg_error);

    // Return the integral of the RMS squared

    ccprintf("\n  initial_ref=%.6f  initial_rate=%.6f  final_ref=%.6f  rms=%.6f  duration=%.6f\n"
            , initial_ref
            , initial_rate
            , final_ref
            , plep_pars->plep.rms
            , plep_pars->meta.time.duration
            );

    return plep_pars->plep.rms * plep_pars->plep.rms * plep_pars->meta.time.duration;
}


//! Calculate the duration and RMS for a pre-function segment
//!
//! NON-RT   refPreFuncArmSegment
//!
//! A segment will normally use one ramp, but in the case of switching polarity, it will need two ramps
//! in order to model the behaviour of the ramp manager.
//!
//! @param[in]   ref_mgr                     Pointer to REF_mgr structure
//! @param[in]   pref_func_seq               Pointer to the sequence
//! @param[in]   seg_index                   Index of segment within the sequence to evaluate
//! @param[out]  integrated_ref_squared_ptr  Pointer to variable to set with the mean-square
//!
//! @returns    Segment duration

static cc_float refPreFuncArmSegment(struct REF_mgr          * const ref_mgr,
                                     struct REF_pre_func_seq * const pref_func_seq,
                                     uint32_t                  const seg_index,
                                     cc_float                * const integrated_ref_squared_ptr)
{
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seg * const seg          = &pref_func_seq->seg[seg_index];

    cc_float const initial_ref  = seg_index == 0 ? pre_func_arm->initial_ref  : pref_func_seq->seg[seg_index-1].final_ref;
    cc_float const initial_rate = seg_index == 0 ? pre_func_arm->initial_rate : 0.0F;

    union FG_pars plep_pars;
    cc_float      integrated_ref_squared;
    cc_float      seg_duration;

    if(   polswitchIsAutomatic(ref_mgr->polswitch_mgr) == true
       && (   seg->type == REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS
           || seg->type == REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS))
    {
        // Crossing zero with a polarity switch so use two separate ramps

        cc_float zero = pre_func_arm->reg_mode == REG_CURRENT
                      ? ref_mgr->reg_mgr->i.lim_meas.zero
                      : ref_mgr->reg_mgr->b.lim_meas.zero;


        integrated_ref_squared =  refPreFuncArmRamp(ref_mgr,
                                                    initial_ref,
                                                    initial_rate,
                                                    seg->type == REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS ? -zero : zero,
                                                    true,
                                                    &plep_pars);

        seg_duration = plep_pars.meta.time.duration + pre_func_arm->reg_period;

        ccprintf("\n  (1) integrated_ref_squared=%.6f  seg_duration=%.6f\n",integrated_ref_squared,seg_duration);

        integrated_ref_squared += refPreFuncArmRamp(ref_mgr,
                                                    0.0F,
                                                    0.0F,
                                                    seg->final_ref,
                                                    true,
                                                    &plep_pars);

        seg_duration += plep_pars.meta.time.duration + pre_func_arm->reg_period;

        ccprintf("\n  (2) integrated_ref_squared=%.6f  seg_duration=%.6f\n",integrated_ref_squared,seg_duration);

        // Assume that the polarity switch movement takes 10% of the switch timeout.
        // The timeout is an integer so the resolution of this estimated movement time is only 100ms.

        seg_duration += 0.1F * (cc_float)polswitchVarValue(ref_mgr->polswitch_mgr, TIMEOUT);

        ccprintf("\n  (3) integrated_ref_squared=%.6f  seg_duration=%.6f\n",integrated_ref_squared,seg_duration);
    }
    else
    {
        // No polarity switch or the reference is not crossing zero so use one ramp only

        integrated_ref_squared = refPreFuncArmRamp(ref_mgr,
                                                   initial_ref,
                                                   initial_rate,
                                                   seg->final_ref,
                                                   seg->type < REF_PRE_FUNC_XXX_TO_REF2,
                                                   &plep_pars);

        seg_duration = plep_pars.meta.time.duration + pre_func_arm->reg_period;

        ccprintf("\n  (4) init_ref=%g  final_ref=%g  integrated_ref_squared=%.6f  seg_duration=%.6f\n"
                , initial_ref
                , seg->final_ref
                , integrated_ref_squared
                , seg_duration
                );
    }

    // Save the calculated duration and RMS for segments from/to default values

    if(seg->type < REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES)
    {
        pre_func_arm->pre_func_reg_mode->duration.calculated[seg->type] = seg_duration;
        pre_func_arm->pre_func_reg_mode->rms.calculated     [seg->type] = sqrtf(integrated_ref_squared / seg_duration);

        ccprintf("\n  type=%u  Calculated duration=%.6f  rms=%.6f\n"
                , seg->type
                , pre_func_arm->pre_func_reg_mode->duration.calculated[seg->type]
                , pre_func_arm->pre_func_reg_mode->rms.calculated     [seg->type]
                );
    }

    // Return the segment's integrated reference squared by reference and the segment duration by value

    *integrated_ref_squared_ptr += integrated_ref_squared;

    return seg_duration;
}


//! Calculate the duration, RMS and segment start times for a pre-function sequence
//!
//! NON-RT   refPreFuncArmSequence
//!
//! The sequence is prepared as a
//! in order to model the behaviour of the ramp manager.
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   pre_func_seq  Pointer to the sequence

static void refPreFuncArmSequence(struct REF_mgr * const ref_mgr, struct REF_pre_func_seq * const pre_func_seq)
{
    struct REF_pre_func_arm      * const pre_func_arm      = &ref_mgr->pre_func_arm;
    struct REF_pre_func_reg_mode * const pre_func_reg_mode = pre_func_arm->pre_func_reg_mode;
    struct REF_pre_func_seg      * const seg               = pre_func_seq->seg;

    // Reset sequence duration and meas-squared accumulator

    cc_float integrated_ref_squared = 0.0F;
    cc_float seq_duration           = 0.0F;

    // Process each segment to accumulate the mean-squared reference and the sequence duration

    cc_float seg_duration[REF_PRE_FUNC_MAX_NUM_SEGS];
    int32_t seg_index;

    for(seg_index = 0 ; seg_index < pre_func_seq->num_segs ; seg_index++)
    {
        ccprintf("\n  seg_index=%u/%u  type=%u  start_time=%.6f  final_ref=%.6f\n"
                , seg_index
                , pre_func_seq->num_segs
                , seg[seg_index].type
                , seg[seg_index].start_time
                , seg[seg_index].final_ref
                );

        if(   seg[seg_index].type >= REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES
           || pre_func_reg_mode->duration.measured[seg[seg_index].type] == 0.0F)
        {
            // Segment is to/from a user reference or the measured duration and RMS are not yet available

            seg_duration[seg_index] = refPreFuncArmSegment(ref_mgr, pre_func_seq, seg_index, &integrated_ref_squared);
        }
        else
        {
            // Segment is to/from default references and a measured RMS and duration are available

            seg_duration[seg_index] = pre_func_reg_mode->duration.measured[seg[seg_index].type];
            cc_float const seg_rms  = pre_func_reg_mode->rms.measured[seg[seg_index].type];
            integrated_ref_squared += seg_rms * seg_rms * seg_duration[seg_index];
        }

        // Process the plateau that follows the ramp - seg[seg_index].start_time has been initialized with the plateau duration

        seq_duration           += seg_duration[seg_index] + seg[seg_index].start_time;
        integrated_ref_squared += seg[seg_index].final_ref * seg[seg_index].final_ref * seg[seg_index].start_time;

        ccprintf("\n  seg_index=%u  seq_duration=%.6f  integrated_ref_squared=%.6f\n"
                , seg_index
                , seq_duration
                , integrated_ref_squared
                );
    }

    // Scan the segments backwards to set the segment start times and identify the segment that will wait.
    // seg[].start_time contains the plateau durations at this point, so they need to be overwritten with
    // the real start times.

    uint32_t wait_seg_index = REF_PRE_FUNC_MAX_NUM_SEGS;
    cc_float start_time     = 0.0F;

    while(--seg_index >= 0)
    {
        if(wait_seg_index == REF_PRE_FUNC_MAX_NUM_SEGS)
        {
            if(seg[seg_index].start_time == WAIT)
            {
                wait_seg_index = seg_index;

                // For a reason that I don't understand, the ramp following the REF1_TO_REF1 segment takes
                // one extra period, so this hack compensated for this. It's very mysterious - I've tried
                // to understand why without success.

                if(seg[seg_index].type == REF_PRE_FUNC_REF1_TO_REF1)
                {
                    start_time += pre_func_arm->reg_period;
                    seg[seg_index+1].start_time += pre_func_arm->reg_period;
                }
            }
            else
            {
                // Set start time to allow for the segment ramp, plateau_duration (stored in start_time by refPreFuncArmSaveSeg())
                // and one extra regulation period, to allow the end of the ramp to be detected.

                start_time -= (seg_duration[seg_index] + seg[seg_index].start_time + pre_func_arm->reg_period);

                seg[seg_index].start_time = start_time;
            }
        }
        else
        {
            // For segments preceding the WAIT segment, use the start_time indicates the plateau duration of the previous segment

            seg[seg_index+1].start_time = seg[seg_index].start_time;
        }

        ccprintf("\n  [%u]  wait_seg_index=%u  type=%u  start_time=%.6f  seg[%u].start_time=%.6f  seg[%u].start_time=%.6f\n"
                , seg_index
                , wait_seg_index
                , seg[seg_index].type
                , start_time
                , seg_index,seg[seg_index].start_time
                , seg_index+1,seg[seg_index+1].start_time
                );
    }

    // Set the first segment start time to always runs immediately

    seg[0].start_time = REF_START_PRE_FUNC_IMMEDIATELY;

    // Estimate the duration of the elastic segment based on the duration available for the whole pre-function

    cc_float wait_seg_duration = pre_func_arm->duration_available - seq_duration;

    if(wait_seg_duration < 0.0F)
    {
        // Insufficient time for the pre-function sequence - set RMS to signal that the calculation failed

        pre_func_seq->rms = SEQ_TOO_LONG;
    }
    else
    {
        // Available duration is sufficient - estimate the RMS for the entire pre-function sequence

        seq_duration           += wait_seg_duration;
        integrated_ref_squared += seg[wait_seg_index].final_ref * seg[wait_seg_index].final_ref * wait_seg_duration;

        pre_func_seq->rms = sqrtf(integrated_ref_squared / seq_duration);
    }

    ccprintf("\n  wait_seg_duration=%.6f  seq_duration=%.6f  integrated_ref_squared=%.6f  seq->rms=%.6f\n"
            , wait_seg_duration
            , seq_duration
            , integrated_ref_squared
            , pre_func_seq->rms
            );
}


//! Save the specification for a pre-function segment
//!
//! NON-RT   refPreFuncArmSaveSeg
//!
//! Each segment is a RAMP followed by a plateau. The segment is defined by the final reference,
//! the segment type and the plateau duration, which will be translated by refPreFuncArmSequence()
//! into segment start time. The last segment in a sequence to have a zero plateau duration will
//! be extended as required, so that the sequence ends at the required time.
//!
//! @param[in]  pre_func_arm        Pointer to REF_pre_func_arm structure
//! @param[in]  pre_func_seq        Pointer to REF_pre_func_seq structure
//! @param[in]  seg_type            Segment type
//! @param[in]  plateau_duration    Segment plateau duration

static void refPreFuncArmSaveSeg(struct REF_pre_func_arm    * const pre_func_arm,
                                 struct REF_pre_func_seq    * const pre_func_seq,
                                 enum   REF_pre_func_seg_type const seg_type,
                                 cc_float                     const plateau_duration)
{
    struct REF_pre_func_seg * const seg = &pre_func_seq->seg[pre_func_seq->num_segs++];

    seg->type = seg_type;

    // Save the plateau_duration in the start_time for this phase of the process
    // It will be converted later into a real start_time relative to the timing event

    seg->start_time = plateau_duration;

    // Set the segment's final reference based on the segment type

    enum REF_pre_func_defaults const final_ref_index = final_ref_indexes[seg_type];

    if(final_ref_index >= REF_PRE_FUNC_NUM_DEFAULTS)
    {
        // For the segments that end in a variable reference, it's either the initial or final reference of the pre-function

        seg->final_ref = (seg_type == REF_PRE_FUNC_REF1_TO_REF1)
                       ? pre_func_arm->initial_ref
                       : pre_func_arm->final_ref;
    }
    else
    {
        // For segments that end in a default value, look up the value

        seg->final_ref = pre_func_arm->pre_func_reg_mode->defaults[final_ref_index];
    }
}


//! Prepare first part of a sequence that must wait at MIN_RMS
//!
//! NON-RT   refPreFuncArmRef1ToMinrms
//!
//! Several types of pre-function must start with a ramp to wait at MIN_RMS. This prepares this part
//! of the sequence, taking into account the fact that the reference may have to cross zero.
//!
//! @param[in]  pre_func_arm         Pointer to REF_pre_func_arm structure
//! @param[in]  pre_func_seq         Pointer to pre-function sequence structure

static void refPreFuncArmRef1ToMinrms(struct REF_pre_func_arm * const pre_func_arm, struct REF_pre_func_seq * const pre_func_seq)
{
    if(pre_func_arm->auto_pol_switch == true && pre_func_arm->bipolar_refs == true)
    {
        if(pre_func_arm->initial_ref >= 0.0F)
        {
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_POSMINRMS     , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS, WAIT);
        }
        else
        {
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_NEGMINRMS     , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS, WAIT);
        }
    }
    else
    {
        if(pre_func_arm->final_ref >= 0.0F)
        {
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_POSMINRMS, WAIT);
        }
        else
        {
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_NEGMINRMS, WAIT);
        }
    }
}


//! Prepare final part of a sequence that ramps to the final reference, including the overshoot if active.
//!
//! NON-RT   refPreFuncArmXXXToRef2
//!
//! @param[in]  ref_mgr              Pointer to REF_mgr structure
//! @param[in]  pre_func_arm         Pointer to REF_pre_func_arm structure
//! @param[in]  pre_func_seq         Pointer to pre-function sequence structure
//! @param[in]  plateau_duration     Final plateau duration

static void refPreFuncArmXXXToRef2(struct REF_mgr          * const ref_mgr,
                                   struct REF_pre_func_arm * const pre_func_arm,
                                   struct REF_pre_func_seq * const pre_func_seq,
                                   cc_float                        plateau_duration)
{
    // Ramp to directly to the final reference

    refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_XXX_TO_REF2, plateau_duration);

    // Arm the sequence

    refPreFuncArmSequence(ref_mgr, pre_func_seq);
}


//! Prepare a RAMP sequence
//!
//! NON-RT   refPreFuncArmPrepareRamp
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   next_event    Pointer to reference function structure

static void refPreFuncArmPrepareRamp(struct REF_mgr * const ref_mgr, struct REF_event * const next_event)
{
    struct REF_pre_func_arm * const pre_func_arm  = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seq * const pre_func_ramp = &next_event->pre_func_ramp;

    if(pre_func_arm->bipolar_refs == false)
    {
        // Unipolar pre-function

        if(fabs(pre_func_arm->final_ref) <= fabs(pre_func_arm->initial_ref))
        {
            // Ramp to final reference and arm the sequence

            refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_ramp, WAIT);

            return;
        }
        else
        {
            // Increasing unipolar amplitude - wait at the initial reference

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_ramp, REF_PRE_FUNC_REF1_TO_REF1, WAIT);
        }
    }
    else
    {
        // Bipolar pre-function - wait at MIN_RMS

        refPreFuncArmRef1ToMinrms(pre_func_arm, pre_func_ramp);
    }

    // Ramp to final reference and arm the sequence

    refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_ramp, pre_func_ramp->final_plateau_duration);
}


//! Prepare a MINRMS or OPENLOOP_MINRMS sequence
//!
//! NON-RT   refPreFuncArmPrepareMinrms
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   next_event    Pointer to the function to arm structure

static void refPreFuncArmPrepareMinrms(struct REF_mgr * const ref_mgr, struct REF_event * const next_event)
{
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seq * const pre_func_seq = &next_event->pre_func_seq;

    // Ramp to MIN RMS and wait

    refPreFuncArmRef1ToMinrms(pre_func_arm, pre_func_seq);

    // Ramp to final reference and arm the sequence

    refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_seq, pre_func_seq->final_plateau_duration);
}


//! Prepare a MAGCYCLE sequence
//!
//! NON-RT   refPreFuncArmPrepareMagcycle
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   next_event    Pointer to the function to arm structure

static void refPreFuncArmPrepareMagcycle(struct REF_mgr * const ref_mgr, struct REF_event * const next_event)
{
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seq * const pre_func_seq = &next_event->pre_func_seq;

    // Ramp to MIN RMS and wait

    refPreFuncArmRef1ToMinrms(pre_func_arm, pre_func_seq);

    // Ramp to MIN or MAX - which ever is closest to the final_ref

    if(ref_mgr->pre_func_arm.final_ref >= 0.0F)
    {
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_MAX, pre_func_seq->minmax_plateau_duration);
    }
    else
    {
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_MIN, pre_func_seq->minmax_plateau_duration);
    }

    // Ramp to final reference and arm the sequence

    refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_seq, pre_func_seq->final_plateau_duration);
}


//! Prepare a UPMINMAX sequence
//!
//! NON-RT   refPreFuncArmPrepareUpminmax
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   next_event    Pointer to the function to arm structure

static void refPreFuncArmPrepareUpminmax(struct REF_mgr * const ref_mgr, struct REF_event * const next_event)
{
    struct REF_pre_func_arm * const pre_func_arm  = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seq * const pre_func_seq  = &next_event->pre_func_seq;

    if(pre_func_arm->final_ref <= pre_func_arm->initial_ref)
    {
        // Descending reference - Prepare RAMP sequence to compare with MIN-MAX for RMS

        refPreFuncArmPrepareRamp(ref_mgr, next_event);

        // Calculate RAMP deceleration factor for refPreFuncMgrStartRT()

        next_event->pre_func_ramp.dec_delta_ref_factor = -0.5 / *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][pre_func_arm->reg_mode].deceleration;
    }

    // Prepare MIN-MAX sequence

    if(pre_func_arm->use_pol_switch == false)
    {
        // Polarity switching is not required so sequence is REF1->MIN->Wait at POSMINMAX->MAX->REF2

        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_MIN     , pre_func_seq->minmax_plateau_duration);
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MIN_TO_POSMINRMS, WAIT);
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_MAX, pre_func_seq->minmax_plateau_duration);
    }
    else
    {
        // Polarity switching is required - Start by ramping to MIN

        if(pre_func_arm->initial_ref < 0.0F)
        {
            // initial reference is negative so go directly to MIN

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_MIN, pre_func_seq->minmax_plateau_duration);
        }
        else
        {
            // initial reference is not negative so change polarity to go to MIN

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_POSMINRMS     , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS, NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_MIN      , pre_func_seq->minmax_plateau_duration);
        }

        // Ramp from MIN to NEGMINRMS

        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MIN_TO_NEGMINRMS, NO_PLATEAU);

        if(pre_func_arm->final_ref < 0.0F)
        {
            // final reference is negative so wait at NEGMINRMS on the way to final reference

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS, NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_MAX      , pre_func_seq->minmax_plateau_duration);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MAX_TO_POSMINRMS      , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS, WAIT);
        }
        else
        {
            // final reference is not negative so wait at POSMINRMS on the way to MAX

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS, WAIT);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_MAX      , pre_func_seq->minmax_plateau_duration);
        }
    }

    // Ramp to final reference and arm the sequence

    refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_seq, pre_func_seq->final_plateau_duration);
}


//! Prepare a DOWNMAXMIN sequence
//!
//! NON-RT   refPreFuncArmPrepareDownmaxmin
//!
//! @param[in]   ref_mgr       Pointer to REF_mgr structure
//! @param[in]   next_event    Pointer to the function to arm structure

static void refPreFuncArmPrepareDownmaxmin(struct REF_mgr * const ref_mgr, struct REF_event * const next_event)
{
    struct REF_pre_func_arm * const pre_func_arm  = &ref_mgr->pre_func_arm;
    struct REF_pre_func_seq * const pre_func_seq  = &next_event->pre_func_seq;

    if(pre_func_arm->final_ref >= pre_func_arm->initial_ref)
    {
        // Ascending reference - Prepare RAMP sequence to compare with MAX-MIN for RMS

        refPreFuncArmPrepareRamp(ref_mgr, next_event);

        // Calculate RAMP deceleration factor for refPreFuncMgrStartRT()

        next_event->pre_func_ramp.dec_delta_ref_factor = 0.5 / *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][pre_func_arm->reg_mode].deceleration;
    }

    // Prepare MAX-MIN sequence

    if(pre_func_arm->use_pol_switch == false)
    {
        // Polarity switching is not required so sequence is REF1->MAX->Wait at NEGMINMAX->MIN->REF2

        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_MAX     , pre_func_seq->minmax_plateau_duration);
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MAX_TO_NEGMINRMS, WAIT);
        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_MIN, pre_func_seq->minmax_plateau_duration);
    }
    else
    {
        // Polarity switching is required - Start by ramping to MAX

        if(pre_func_arm->initial_ref > 0.0F)
        {
            // initial reference is positive so go directly to MAX

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_MAX, pre_func_seq->minmax_plateau_duration);
        }
        else
        {
            // initial reference is not positive so change polarity to go to MAX

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_REF1_TO_NEGMINRMS     , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS, NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_MAX      , pre_func_seq->minmax_plateau_duration);
        }

        // Ramp from MAX to POSMINRMS

        refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MAX_TO_POSMINRMS, NO_PLATEAU);

        if(pre_func_arm->final_ref > 0.0F)
        {
            // final reference is positive so wait at POSMINRMS on the way from MIN to final reference

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS, NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_MIN      , pre_func_seq->minmax_plateau_duration);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_MIN_TO_NEGMINRMS      , NO_PLATEAU);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS, WAIT);
        }
        else
        {
            // final reference is not positive so wait at NEGMINRMS on the way to MIN

            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS, WAIT);
            refPreFuncArmSaveSeg(pre_func_arm, pre_func_seq, REF_PRE_FUNC_NEGMINRMS_TO_MIN      , pre_func_seq->minmax_plateau_duration);
        }
    }

    // Ramp to final reference and arm the sequence

    refPreFuncArmXXXToRef2(ref_mgr, pre_func_arm, pre_func_seq, pre_func_seq->final_plateau_duration);
}


//! Check if default parameters have changed for the active reg_mode and
//! reset the associated calculated, measured and error durations and RMS values.
//!
//! NON-RT   refPreFuncCheckDefaults
//!
//! @param[in]  ref_mgr             Pointer to REF_mgr structure

static void refPreFuncCheckDefaults(struct REF_mgr * const ref_mgr)
{
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;

    // Take a local copy of the default values for the actual load select and the reg_mode for the function to arm

    struct REF_default_ptrs * const defaults_ptr = &ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][pre_func_arm->reg_mode];

    cc_float defaults[REF_PRE_FUNC_NUM_DEFAULTS];

    defaults[REF_PRE_FUNC_DEFAULT_ACCELERATION]  = *defaults_ptr->acceleration;
    defaults[REF_PRE_FUNC_DEFAULT_DECELERATION]  = *defaults_ptr->deceleration;
    defaults[REF_PRE_FUNC_DEFAULT_LINEAR_RATE ]  = *defaults_ptr->linear_rate;
    defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ] = *defaults_ptr->pre_func_max;
    defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ] = *defaults_ptr->pre_func_min;
    defaults[REF_PRE_FUNC_DEFAULT_POS_MIN_RMS ]  = *defaults_ptr->min_rms;
    defaults[REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS ]  = -defaults[REF_PRE_FUNC_DEFAULT_POS_MIN_RMS];

    // Perform sanity checks on MIN and MAX compared to MIN_RMS

    if(defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] <= 0.0F && defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] > defaults[REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS])
    {
        defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] = 0.0F;
    }

    if(defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] > defaults[REF_PRE_FUNC_DEFAULT_POS_MIN_RMS])
    {
        defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] = defaults[REF_PRE_FUNC_DEFAULT_POS_MIN_RMS];
    }

    if(defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX] >= 0.0F && defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX] < defaults[REF_PRE_FUNC_DEFAULT_POS_MIN_RMS])
    {
        defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX] = 0.0F;
    }

    if(defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX] < defaults[REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS])
    {
        defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX] = defaults[REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS];
    }

    // Check if any default parameters have changed

    cc_float * const defaults_for_reg_mode = pre_func_arm->pre_func_reg_mode->defaults;

    if(memcmp(defaults_for_reg_mode, defaults, sizeof(defaults)) != 0)
    {
        // Reset the calculated and measured RMS and duration for all the default segment types

        memset(pre_func_arm->pre_func_reg_mode, 0, sizeof(struct REF_pre_func_reg_mode));

        // Update the copy of the default values so that future changes can be detected

        memcpy(defaults_for_reg_mode, defaults, sizeof(defaults));
    }
}


//! Set the initial ref and rate for the pre-function, according to the regulation mode
//!
//! NON-RT   refPreFuncInitialRefAndRate
//!
//! This function will try to return the initial reference for the first segment of the pre-function.
//! This is used to estimate the duration and RMS of the first segment. If the previous function
//! meta data is still valid (i.e. we are still in CYCLING) then we can use the meta data.
//! Otherwise, we have to rely on the latest measured value.
//!
//! @param[in]  ref_mgr               Pointer to REF_mgr structure
//! @param[in]  reg_mode              Regulation mode for the pre-function (FIELD or CURRENT only)
//! @param[in]  prev_func_meta_valid  True if pre_func_arm.prev_func_meta is valid

static void refPreFuncInitialRefAndRate(struct REF_mgr  * const ref_mgr,
                                        enum   REG_mode   const reg_mode,
                                        bool              const prev_func_meta_valid)

{
    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;
    struct REG_mgr          * const reg_mgr      =  ref_mgr->reg_mgr;

    if(prev_func_meta_valid == false)
    {
        // Previous function meta is not valid so used measured field or current to set the initial ref and rate for the pre-function

        switch(reg_mode)
        {
            case REG_FIELD:

                pre_func_arm->initial_ref  = reg_mgr->b.meas.signal[REG_MEAS_UNFILTERED];
                pre_func_arm->initial_rate = reg_mgr->b.meas.rate;
                break;

            case REG_CURRENT:

                pre_func_arm->initial_ref  = reg_mgr->i.meas.signal[REG_MEAS_UNFILTERED];
                pre_func_arm->initial_rate = reg_mgr->i.meas.rate;
                break;

            default:

                break;
        }
    }
    else
    {
        // Previous function meta data is valid, so use it to set the initial rate and ref for the pre-function

        // There is an active function so we are currently cycling

        if(reg_mode == ref_mgr->iter_cache.reg_mode)
        {
            // Reg mode is unchanged so directly use the final reference and rate of the running function

            pre_func_arm->initial_ref  = ref_mgr->pre_func_mgr.prev_func_meta.final_ref;
            pre_func_arm->initial_rate = ref_mgr->pre_func_mgr.prev_func_meta.final_rate;
        }
        else
        {
            // Regulation mode is switching from current to field, or from field to current

            if(reg_mode == REG_FIELD)
            {
                // Convert current to field, ignoring saturation of the magnet

                pre_func_arm->initial_ref  = ref_mgr->pre_func_mgr.prev_func_meta.final_ref  * reg_mgr->load_pars.gauss_per_amp;
                pre_func_arm->initial_rate = ref_mgr->pre_func_mgr.prev_func_meta.final_rate * reg_mgr->load_pars.gauss_per_amp;
            }
            else
            {
                // Convert field to current, ignoring saturation of the magnet

                pre_func_arm->initial_ref  = ref_mgr->pre_func_mgr.prev_func_meta.final_ref  / reg_mgr->load_pars.gauss_per_amp;
                pre_func_arm->initial_rate = ref_mgr->pre_func_mgr.prev_func_meta.final_rate / reg_mgr->load_pars.gauss_per_amp;
            }
        }
    }

    ccprintf("\n  reg_mode=%u  prev_reg_mode=%u  initial_ref=%.6f  initial_rate=%.6f\n"
            , reg_mode
            , ref_mgr->iter_cache.reg_mode
            , pre_func_arm->initial_ref
            , pre_func_arm->initial_rate
            );
}


//! NON-RT   refPreFuncSaveMeta

void refPreFuncSaveMeta(struct REF_mgr  * const ref_mgr,
                        struct CC_ns_time const abs_end_time,
                        cc_float          const final_ref,
                        cc_float          const final_rate)
{
    ref_mgr->pre_func_mgr.prev_func_meta.abs_end_time = abs_end_time;
    ref_mgr->pre_func_mgr.prev_func_meta.final_ref    = final_ref;
    ref_mgr->pre_func_mgr.prev_func_meta.final_rate   = final_rate;
}


// NON-RT   refPreFuncArm

void refPreFuncArm(struct REF_mgr   * const ref_mgr,
                   struct REF_event * const next_event,
                   struct REF_func  * const func_to_arm,
                   cc_float           const duration_available,
                   bool               const prev_func_meta_valid)
{
    ccprintf("\n  func_to_arm->cyc_sel=%u  duration_available=%.6f  prev_func_meta_valid=%u\n"
            , func_to_arm->cyc_sel
            , duration_available
            , prev_func_meta_valid
            );

    struct REF_pre_func_arm * const pre_func_arm = &ref_mgr->pre_func_arm;

    pre_func_arm->duration_available = duration_available;

    // Disable both the nominal sequence and the RAMP sequences

    struct REF_pre_func_seq * const pre_func_seq  = &next_event->pre_func_seq;
    struct REF_pre_func_seq * const pre_func_ramp = &next_event->pre_func_ramp;

    memset(pre_func_seq,  0, sizeof(struct REF_pre_func_seq));
    memset(pre_func_ramp, 0, sizeof(struct REF_pre_func_seq));

    // The pre-function final reference is the initial reference of the main function

    pre_func_arm->final_ref = func_to_arm->ref_armed.fg_pars.meta.range.initial_ref;

    // Identify the regulation mode for the function to arm

    pre_func_arm->reg_mode = func_to_arm->ref_armed.reg_mode;

    switch(pre_func_arm->reg_mode)
    {
        case REG_FIELD:

            pre_func_arm->pre_func_reg_mode = &ref_mgr->pre_func_mgr.b;
            pre_func_arm->reg_period        = ref_mgr->reg_mgr->b.reg_period;
            break;

        case REG_CURRENT:

            pre_func_arm->pre_func_reg_mode = &ref_mgr->pre_func_mgr.i;
            pre_func_arm->reg_period        = ref_mgr->reg_mgr->i.reg_period;
            break;

        case REG_VOLTAGE:

            // For voltage regulation mode, the armed pre-function mode is ignored and a simple sequence with one immediate RAMP is used

            pre_func_ramp->num_segs               = 1;
            pre_func_ramp->final_plateau_duration = (refMgrParPointer(ref_mgr, DEFAULT_PLATEAU_DURATION))[REF_FINAL_PLATEAU_INDEX];
            pre_func_ramp->mode                   = REF_PRE_FUNC_RAMP;

            pre_func_ramp->seg[0].type       = REF_PRE_FUNC_XXX_TO_REF2;
            pre_func_ramp->seg[0].start_time = REF_START_PRE_FUNC_IMMEDIATELY;
            pre_func_ramp->seg[0].final_ref  = pre_func_arm->final_ref;
            return;

        default:

            return;
    }

    // Reset the calculated, measured and error values for the duration and RMS of all default segment types
    // affected by any changes to default values for the given regulation mode

    refPreFuncCheckDefaults(ref_mgr);

    // Field or current cycles: Set up the pre_func structure for the standard sequence and the ramp sequence (used for UPMINMAX and DOWNMAXMIN only)
    // Adjust the plateau durations by one period because it take one period for libref to realise that a ramp has finished and protect against
    // the plateau duration becoming less than one regulation period.

    cc_float minmax_plateau_duration = (refMgrParPointer(ref_mgr, DEFAULT_PLATEAU_DURATION))[REF_MINMAX_PLATEAU_INDEX];
    cc_float final_plateau_duration  = (refMgrParPointer(ref_mgr, DEFAULT_PLATEAU_DURATION))[REF_FINAL_PLATEAU_INDEX];

    pre_func_seq->minmax_plateau_duration = minmax_plateau_duration > pre_func_arm->reg_period
                                          ? minmax_plateau_duration
                                          : pre_func_arm->reg_period;

    pre_func_seq->final_plateau_duration  = final_plateau_duration > pre_func_arm->reg_period
                                          ? final_plateau_duration
                                          : pre_func_arm->reg_period;

    pre_func_seq->mode                    = refMgrParValue(ref_mgr,MODE_PRE_FUNC);
    pre_func_ramp->mode                   = REF_PRE_FUNC_RAMP;
    pre_func_ramp->final_plateau_duration = pre_func_seq->final_plateau_duration;

    // Set the initial reference based on the previous cycle if there is one or on measurements if not

    refPreFuncInitialRefAndRate(ref_mgr, func_to_arm->ref_armed.reg_mode, prev_func_meta_valid);

    // Set flags that will be used during the preparation of the segments

    pre_func_arm->bipolar_refs    = pre_func_arm->initial_ref * pre_func_arm->final_ref < 0.0F;
    pre_func_arm->bipolar_minmax  = pre_func_arm->pre_func_reg_mode->defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX]
                                  * pre_func_arm->pre_func_reg_mode->defaults[REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN] < 0.0F;
    pre_func_arm->auto_pol_switch = polswitchIsAutomatic(ref_mgr->polswitch_mgr);
    pre_func_arm->use_pol_switch  = pre_func_arm->bipolar_minmax == true && pre_func_arm->auto_pol_switch == true;

    // Try to prepare and arm the nominal sequence - for UPMINMAX and DOWNMAXMIN, the alternative RAMP sequence may also be prepared and armed

    prepare_sequence[pre_func_seq->mode](ref_mgr, next_event);

    ccprintf("\n  seq.rms=%.6f  ramp.rms=%.6f\n",  pre_func_seq->rms,  pre_func_ramp->rms);

    // For UPMINMAX or DOWNMAXMIN, if the nominal sequence is too long then force use of the alternative RAMP sequence by canceling the nominal sequence

    if(  (   pre_func_seq->mode == REF_PRE_FUNC_UPMINMAX
          || pre_func_seq->mode == REF_PRE_FUNC_DOWNMAXMIN)
       && pre_func_ramp->num_segs != 0
       && pre_func_seq->rms == SEQ_TOO_LONG)
    {
        // Use RAMP instead of MIN/MAX or MAX/MIN

        pre_func_seq->num_segs = 0;
    }
}

// EOF
