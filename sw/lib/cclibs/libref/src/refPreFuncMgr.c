//! @file  refPreFuncMgr.c
//! @brief Converter Control Reference Manager library pre-function real-time functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


// #define CCPRINTF

#include "libref.h"

//! Segment start_time constants

static cc_float const NEVER =  1.0E6F;              //!< Time stamp to never run - used for the next segment when running the last segment


//! Set the time of the next pre-function segment
//!
//! This sets next_seg_time so that refPreFuncRT() will know when to activate the next segment.
//! If there is no next segment, then the time is set to NEVER, to be clearly after the start of
//! the main function. A zero segment start time has a special meaning and is interpreted as
//! a signal to start the segment immediately after the previous segment finishes.
//!
//! @param[in,out]  ref_mgr          Pointer to REF_mgr structure
//! @param[in]      next_seg_index   Index of the next segment

static void refPreFuncSetNextSegTimeRT(struct REF_mgr * const ref_mgr, uint32_t const next_seg_index)
{
    struct REF_event const * const active_event = ref_mgr->event_mgr.active.event;

    ref_mgr->pre_func_mgr.next_seg_time = next_seg_index < active_event->pre_func->num_segs
                                        ? active_event->pre_func->seg[next_seg_index].start_time
                                        : NEVER;
}



bool refPreFuncMgrStartRT(struct REF_mgr * const ref_mgr)
{
    struct REF_pre_func_mgr * const pre_func_mgr = &ref_mgr->pre_func_mgr;

    // Reset flags to prepare for the new pre-function

    pre_func_mgr->seq_finished = false;
    pre_func_mgr->seg_finished = true;

    // Return immediately if the function time has already arrived - no time to run a pre-function

    if(refStateCyclingCheckStartMainFunctionRT(ref_mgr))
    {
        return false;
    }

    // Prepare to run the pre-function

    struct REF_event * const active_event = ref_mgr->event_mgr.active.event;

    char strbuf[22];
    ccprintf("\n  cyc_sel=%u  fg.time_adv=%s  ref_rate=%g  reg_mode=%u\n"
            , active_event->cyc_sel
            , cctimeNsPrintRelTime(ref_mgr->fg.time_adv, strbuf)
            , ref_mgr->reg_mgr->ref_rate
            , ref_mgr->iter_cache.reg_mode
            );

    cc_float                  const fg_time_adv   = ref_mgr->fg.time_adv_fp32;
    struct REF_pre_func_seq * const pre_func_ramp = &active_event->pre_func_ramp;
    struct REF_pre_func_seq * const pre_func_seq  = &active_event->pre_func_seq;
    struct REF_pre_func_seq *       pre_func      = pre_func_seq;
    int32_t                         seg_index     = -1;

    if(pre_func_seq->num_segs > 0)
    {
        // Choose between nominal sequence and RAMP sequence for UPMINMAX or DOWNMAXMIN based on
        // the expected RMS and if there is time to use RAMP without overshoot or undershoot

        enum REF_pre_func_mode const mode = pre_func_seq->mode;

        if(  (   mode == REF_PRE_FUNC_UPMINMAX
              || mode == REF_PRE_FUNC_DOWNMAXMIN)
           && pre_func_ramp->num_segs > 0
           && pre_func_seq->rms > pre_func_ramp->rms)
        {
            ccprintf("\n  Ramp: num_segs:%u  rms:%.6E  Nom: rms:%.6E\n"
                    , pre_func_ramp->num_segs
                    , pre_func_ramp->rms
                    , pre_func_seq->rms
                    );

            // UPMINMAX or DOWNMAXMIN and a lower RMS RAMP is also armed so select the RAMP by default

            pre_func = pre_func_ramp;

            // Find which RAMP segment we are in

            seg_index = pre_func_ramp->num_segs;

            while(   --seg_index > 0
                  && (   pre_func_ramp->seg[seg_index].start_time >= 0.0F
                      || fg_time_adv < pre_func_ramp->seg[seg_index].start_time));

            cc_float const rate = ref_mgr->reg_mgr->ref_rate;

            ccprintf("\n  seg_index:%u  rate:%.6E\n", seg_index, rate);

            if(   pre_func_ramp->seg[seg_index].type >= REF_PRE_FUNC_XXX_TO_REF2
               && (   (mode == REF_PRE_FUNC_DOWNMAXMIN && rate > 0.0F)
                   || (mode == REF_PRE_FUNC_UPMINMAX   && rate < 0.0F)))
            {
                // Calculate the final reference if we decelerate immediately

                cc_float const final_ref = ref_mgr->ref_fg + pre_func_ramp->dec_delta_ref_factor * rate * rate;

                ccprintf("\n  final_ref:%.6E  seg.final_ref:%.6E\n", final_ref, pre_func_ramp->seg[seg_index].final_ref);

                if(   (   mode == REF_PRE_FUNC_DOWNMAXMIN
                       && final_ref >= pre_func_ramp->seg[seg_index].final_ref)
                   || (   mode ==  REF_PRE_FUNC_UPMINMAX
                       && final_ref <= pre_func_ramp->seg[seg_index].final_ref))
                {
                    // Switching to the RAMP will overshoot the final reference so revert to the nominal sequence

                    pre_func = pre_func_seq;
                }
            }
        }
    }
    else
    {
        CC_ASSERT(pre_func_ramp->num_segs > 0);

        pre_func = pre_func_ramp;
    }

    active_event->pre_func = pre_func;

    // Identify which pre-function segment we are starting in and set the next segment time to the start of this segment

    if(seg_index == -1)
    {
        seg_index = pre_func->num_segs;

        while(   --seg_index > 0
              && (   pre_func->seg[seg_index].start_time >= 0.0F
                  || fg_time_adv < pre_func->seg[seg_index].start_time));
    }

    pre_func_mgr->seg_index = seg_index - 1;

    refPreFuncSetNextSegTimeRT(ref_mgr, seg_index);

    // Disable use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    ccprintf("\n  seg_index=%d  next_seg_time=%.4f  final_ref=%.6f\n"
            , pre_func_mgr->seg_index
            , pre_func_mgr->next_seg_time
            , active_event->pre_func_seq.seg[0].final_ref
            );

    // Return true to report that pre-function has been prepared

    return true;
}



void refPreFuncMgrEndRT(struct REF_mgr * const ref_mgr)
{
    if(ref_mgr->pre_func_mgr.seq_finished == false)
    {
        // Terminate any active ramp manager ramp

        refRampMgrTerminateRT(ref_mgr);

        // Set PRE_FUNC warning bit in the cycle status

        refCycStatusMgrSetStatusBitMaskRT(&ref_mgr->cyc_status_mgr, REF_CYC_PRE_FUNC_WRN_BIT_MASK);

        ref_mgr->pre_func_mgr.seq_finished = true;

        // Allow real-time delta_ref to be activated in case last pre-function segment wasn't started

        refRtControlRealTimeRefRT(ref_mgr, REF_CYCLING);
    }

    // Pre-function is still running so reset force_openloop, in case it is active

    ref_mgr->force_openloop = false;
}



//! Start a pre-function segment
//!
//! This function is called to start the next pre-function ramp.
//!
//! @param[in,out]  ref_mgr          Pointer to REF_mgr structure

static void refPreFuncStartSegmentRT(struct REF_mgr * const ref_mgr)
{
    struct REF_pre_func_mgr * const pre_func_mgr = &ref_mgr->pre_func_mgr;
    struct REF_event        * const active_event =  ref_mgr->event_mgr.active.event;
    struct REF_pre_func_seq * const pre_func_seq = active_event->pre_func;

    // Start the next pre-function segment in the sequence

    uint32_t const next_seg_index = ++pre_func_mgr->seg_index + 1;
    bool     const last_seg_flag  = next_seg_index >= pre_func_seq->num_segs;

    struct REF_pre_func_seg * const seg = &pre_func_seq->seg[pre_func_mgr->seg_index];

    // Allow real-time delta ref for the last segment

    if(last_seg_flag == true)
    {
        refRtControlRealTimeRefRT(ref_mgr, REF_CYCLING);
    }

    cc_float ref_rate = ref_mgr->reg_mgr->ref_rate;

    if(ref_rate != 0.0F)
    {
        // Reference rate calculated from the last two iterations is not zero, so improve on
        // the calculation by using the last three.

        struct REG_mgr_signal * const reg_signal = ref_mgr->reg_mgr->reg_signal;
        struct REG_rst_vars   * const rst_vars   = &reg_signal->rst_vars;
        uint32_t                const var_idx0   = rst_vars->history_index;
        uint32_t                const var_idx1   = (var_idx0 - 1) & REG_RST_HISTORY_MASK;
        uint32_t const var_idx2 = (var_idx0 - 2) & REG_RST_HISTORY_MASK;

        // Compute ref_rate using a parabola through the last three reference values.
        // https://www.derivative-calculator.net/ can differentiate the parabola:
        // r0(t+p)(t+2p)/((0+p)(0+2p))+r1(t)(t+2p)/((-p-0)(-p+2p))+r2(t)(t+p)/((-2p-0)(-2p+p))

        // Reduce the calculated rate by 0.1% to avoid overshoot/undershoot at the end of the ramp

        ref_rate = 0.999F * 0.5F * reg_signal->rst_pars->inv_reg_period
                 * (3.0F * rst_vars->ref_closed[var_idx0] - 4.0F * rst_vars->ref_closed[var_idx1] + rst_vars->ref_closed[var_idx2]);
    }

    ccprintf("\n  cyc_sel=%u  seg_index=%u   type=%u  fg.time_adv=%.6f  next_seg_time=%.4f  init_rate=%.6f  init_ref=%.6f  final_ref=%.6f\n"
            , active_event->cyc_sel
            , pre_func_mgr->seg_index
            , active_event->pre_func_seq.seg[pre_func_mgr->seg_index].type
            , ref_mgr->fg.time_adv_fp32
            , pre_func_mgr->next_seg_time
            , ref_rate
            , ref_mgr->ref_fg
            , seg->final_ref
            );

    refRampMgrInitRT(ref_mgr,
                     ref_mgr->fg.time_fp32,
                     ref_rate,
                     ref_mgr->ref_fg,
                     seg->final_ref,
                     last_seg_flag == false);               // Use acceleration for deceleration except for last segment

    refRampMgrRT(ref_mgr);

    // Update start time for this segment

    pre_func_seq->seg[pre_func_mgr->seg_index].start_time = ref_mgr->fg.time_adv_fp32;

    // Set next segment time

    refPreFuncSetNextSegTimeRT(ref_mgr, next_seg_index);

    ccprintf("\n  next_seg_time=%.4f\n", pre_func_mgr->next_seg_time);

    // Prepare for new segment

    ref_mgr->force_openloop = (   pre_func_seq->mode == REF_PRE_FUNC_OPENLOOP_MINRMS
                               && ref_mgr->ref_fg_unlimited == 0.0F
                               && seg->final_ref == 0.0F);

    pre_func_mgr->seg_finished    = false;
    pre_func_mgr->num_samples     = 0;
    pre_func_mgr->sum_ref_squared = 0.0F;
}



//! Finish a pre-function segment
//!
//! This function is called when a pre-function ramp finishes, or is terminated early by the next
//! pre-function segment. It computes and records the segment time and RMS.
//!
//! @param[in,out]  ref_mgr            Pointer to REF_mgr structure
//! @param[in]      ramp_mgr_status    Ramp manager status

static void refPreFuncFinishSegmentRT(struct REF_mgr * const ref_mgr, enum FG_status const ramp_mgr_status)
{
    struct REF_pre_func_mgr * const pre_func_mgr = &ref_mgr->pre_func_mgr;
    struct REF_func         * const active_func  =  ref_mgr->event_mgr.active.func;
    struct REF_pre_func_seq * const pre_func_seq = ref_mgr->event_mgr.active.event->pre_func;

    char strbuf[22];
    ccprintf("\n  cyc_sel=%u  seg_index=%u   type=%u  fg.time_adv=%s\n"
            , active_func->cyc_sel
            , pre_func_mgr->seg_index
            , pre_func_seq->seg[pre_func_mgr->seg_index].type
            , cctimeNsPrintRelTime(ref_mgr->fg.time_adv, strbuf)
            );

    // Set pre_func_reg_mode according to the reg_mode of the active cycle

    struct REF_pre_func_reg_mode * pre_func_reg_mode = NULL;

    if(active_func->ref_armed.reg_mode == REG_FIELD)
    {
        pre_func_reg_mode = &pre_func_mgr->b;
    }
    else if(active_func->ref_armed.reg_mode == REG_CURRENT)
    {
        pre_func_reg_mode = &pre_func_mgr->i;
    }

    // Analyze current or field segments

    struct REF_pre_func_seg    * const seg      = &pre_func_seq->seg[pre_func_mgr->seg_index];
    enum   REF_pre_func_seg_type const seg_type = seg->type;

    if(ramp_mgr_status == FG_POST_FUNC)
    {
        // There was enough time to completely finish the segment's ramp

        if(   active_func->ref_armed.reg_mode != REG_VOLTAGE
           && seg_type < REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES)
        {
            // Default segment with field or current regulation : save the measured duration and RMS for the segment's ramp

            pre_func_reg_mode->duration.measured[seg_type] = ref_mgr->fg.time_adv_fp32 - seg->start_time;
            pre_func_reg_mode->rms.measured     [seg_type] = sqrtf(pre_func_mgr->sum_ref_squared / (cc_float)pre_func_mgr->num_samples);
        }
    }
    else
    {
        // Ramp was terminated early because the time available was insufficient so set PRE_FUNC_WRN bit in cyc_status

        refCycStatusMgrSetStatusBitMaskRT(&ref_mgr->cyc_status_mgr, REF_CYC_PRE_FUNC_WRN_BIT_MASK);
    }

    if(seg_type >= REF_PRE_FUNC_XXX_TO_REF2)
    {
        // Final segment - mark the sequence as finished and clear force_openloop

        pre_func_mgr->seq_finished = true;

        // Save plateau time error in the cycle status

        cc_float pre_func_plateau_err = ref_mgr->fg.time_adv_fp32 + pre_func_seq->final_plateau_duration;

        // Sequences with only one segment are allowed to arrive early on the plateau,
        // so only set the plateau error (default value is 0.0) if the error is positive (late)

        if(pre_func_seq->num_segs != 1 || pre_func_plateau_err > 0.0F)
        {
            refCycStatusMgrSetPreFuncPlateaurErrRT(&ref_mgr->cyc_status_mgr, pre_func_plateau_err);
        }

        char strbuf[2][22];
        ccprintf("\n  num_segs=%u  cyc_sel=%u  fg.time=%s  fg.time_adv =%s  final_plateau_duration=%.8f  pre_func_plateau_err=%.7f  period=%g\n"
                , pre_func_seq->num_segs
                , active_func->cyc_sel
                , cctimeNsPrintRelTime(ref_mgr->fg.time, strbuf[0])
                , cctimeNsPrintRelTime(ref_mgr->fg.time_adv, strbuf[1])
                , pre_func_seq->final_plateau_duration
                , pre_func_plateau_err
                , ref_mgr->reg_mgr->reg_period
                );
    }

    // Flag completion of pre-function segment

    pre_func_mgr->seg_finished = true;
}



void refPreFuncMgrRT(struct REF_mgr * const ref_mgr)
{
    // Run the ramp manager once it has been initialized

    enum FG_status ramp_mgr_status = FG_PRE_FUNC;

    if(ref_mgr->fg.pars == &ref_mgr->fsm.fg_pars)
    {
        ramp_mgr_status = refRampMgrRT(ref_mgr);
    }

    // Accumulate square of the RST reference when ramping with current or field regulation

    struct REF_pre_func_mgr * const pre_func_mgr = &ref_mgr->pre_func_mgr;
    struct REF_event        * const active_event = ref_mgr->event_mgr.active.event;
    struct REF_func         * const active_func  = ref_mgr->event_mgr.active.func;

    if(pre_func_mgr->seg_finished == false && active_func->ref_armed.reg_mode != REG_VOLTAGE)
    {
        pre_func_mgr->num_samples++;

        cc_float const ref_closed = ref_mgr->reg_mgr->reg_signal->ref_closed;

        pre_func_mgr->sum_ref_squared += ref_closed * ref_closed;
    }

    bool const start_next_segment = (ref_mgr->fg.time_adv_fp32 >= pre_func_mgr->next_seg_time);

    if(start_next_segment == false)
    {
        // Time of next segment has not arrived

        if(pre_func_mgr->seg_finished == false && ramp_mgr_status == FG_POST_FUNC)
        {
            // Set force_openloop for a zero plateau if the pre-function mode is OPENLOOP_MINRMS

            ref_mgr->force_openloop = (   active_event->pre_func->mode == REF_PRE_FUNC_OPENLOOP_MINRMS
                                       && ref_mgr->ref_fg_unlimited == 0.0F);

            // Pre-function segment has just finished

            refPreFuncFinishSegmentRT(ref_mgr, FG_POST_FUNC);

            char strbuf[2][22];
            ccprintf("\n  cyc_sel=%u  fg.time_adv=%s  force_openloop=%u  ref_fg=%.4f\n"
                    , active_func->cyc_sel
                    , cctimeNsPrintRelTime(ref_mgr->fg.time_adv, strbuf[0])
                    , ref_mgr->force_openloop
                    , ref_mgr->ref_fg_unlimited
                    );

            // If pre_func_mgr->next_seg_time >= 0, it signals that the next segment should start after this delay

            if(pre_func_mgr->next_seg_time >= 0.0F)
            {
                ccprintf("\n  cyc_sel=%u  fg.time_adv=%s  next_seg_time=%.4f  new_next_seg_time=%s\n"
                        , active_func->cyc_sel
                        , cctimeNsPrintRelTime(ref_mgr->fg.time_adv, strbuf[0])
                        , pre_func_mgr->next_seg_time
                        , cctimeNsPrintRelTime(cctimeNsAddRT(ref_mgr->fg.time_adv, cctimeNsDoubleToRelTimeRT(pre_func_mgr->next_seg_time)), strbuf[1])
                        );

                pre_func_mgr->next_seg_time += ref_mgr->fg.time_adv_fp32;
            }
        }

        // If time is within the final plateau then reset force_openloop

        if(   ref_mgr->force_openloop == true
           && (ref_mgr->fg.time_adv_fp32 + active_event->pre_func->final_plateau_duration) > 0.0F)
        {
            ref_mgr->force_openloop = false;
        }

        return;
    }

    // It is time to start the next pre-function segment

    if(pre_func_mgr->seg_finished == false)
    {
        // Previous pre-function segment hasn't finished, so it must be terminated early

        refPreFuncFinishSegmentRT(ref_mgr, ramp_mgr_status);

        ccprintf("pre_func_too_long\n");
    }

    refPreFuncStartSegmentRT(ref_mgr);
}

// EOF
