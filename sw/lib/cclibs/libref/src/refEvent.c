//! @file  refEvent.c
//! @brief Converter Control Function Manager library Event functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"


// NON-RT   refEventPrepareAction

static bool refEventPrepareAction(struct REF_action_event * const event,
                                  enum REF_event_type       const event_type,
                                  enum REF_state            const ref_mode,
                                  uint32_t                  const ref_state_mask,
                                  struct CC_us_time         const event_us_time)
{
    if(event->is_pending == false && refEventMgrRefStateIsValidRT(event_type, ref_mode, ref_state_mask) == true)
    {
        event->type = event_type;

        // Convert event time from microsecond resolution CC_us_time to nanosecond resolution CC_ns_time

        event->time = cctimeUsToNsRT(event_us_time);

        // Mark event as pending but not active

        event->is_active = false;

        CC_MEMORY_BARRIER;

        event->is_pending = true;

        return true;
    }

    return false;
}


// NON-RT   refEventPrepareFuncRef

static void refEventPrepareFuncRef(struct REF_func        * const next_func,
                                   struct REF_armed const * const ref_armed)
{
    next_func->ref_armed = *ref_armed;

    // If TABLE function was armed, then copy table time and reference arrays

    if(next_func->ref_armed.fg_pars.meta.type == FG_TABLE)
    {
        struct FG_table * const next_table = &next_func->ref_armed.fg_pars.table;

        memcpy(next_func->table_function, next_table->points, sizeof(struct FG_point) * next_table->num_points);

        next_table->points = next_func->table_function;
    }
}


// NON-RT   refEventPrepareDynEco

static bool refEventPrepareDynEco(struct REF_mgr   * const ref_mgr,
                                  struct REF_armed * const ref_armed,
                                  struct REF_func  * const next_func,
                                  uint32_t           const sub_sel,
                                  uint32_t           const ref_cyc_sel)
{
    // De-activate dyn_eco PLEP by default

    next_func->dyn_eco_plep_start_time = 0.0F;

    // Extract DYN_ECO_END_TIME parameter

    cc_double dyn_eco_end_time1 = (cc_double)refMgrFgParValueSubCyc(ref_mgr, sub_sel, ref_cyc_sel, CTRL_DYN_ECO_END_TIME);

    // Return no warning (false) if ECCONOMY is DISABLED, regulation mode isn't CURRENT or DYN_ECO_END_TIME is not positive

    if(   refMgrParValue(ref_mgr, MODE_ECONOMY) == CC_DISABLED
       || ref_armed->reg_mode != REG_CURRENT
       || dyn_eco_end_time1 <= 0.0)
    {
        return false;
    }

    // Return warning (true) if DYN_ECO_END_TIME is after the end of the function

    if(dyn_eco_end_time1 > ref_armed->fg_pars.meta.time.end)
    {
        return true;
    }

    // Set the initial reference from the current regulation standby limit and regulation period

    struct REG_mgr const * const reg_mgr    = ref_mgr->reg_mgr;
    cc_float               const reg_period = reg_mgr->i.reg_period;

    // Get reference from the armed function at the time of dynamic economy mode end and one regulation period after that
    // Two references are needed for PLEP final rate calculation, to smoothly connect PLEP and the armed function

    cc_float ref1;
    cc_float ref2;
    cc_double dyn_eco_end_time2 = dyn_eco_end_time1 + reg_period;

    fgFuncRT(&ref_armed->fg_pars, &dyn_eco_end_time1, &ref1);
    fgFuncRT(&ref_armed->fg_pars, &dyn_eco_end_time2, &ref2);

    // Define the PLEP acceleration and linear_rate from the default parameters

    cc_float const acceleration = *ref_mgr->ref_defaults[ref_mgr->pc_on_cache.load_select][REG_CURRENT].acceleration;
    cc_float const linear_rate  = 0.97F * (regMgrParValue(reg_mgr,LIMITS_V_POS) - ref1 * reg_mgr->load_pars.ohms) * reg_mgr->load_pars.inv_henrys;
    cc_float const initial_ref  = ref_armed->fg_pars.meta.range.initial_ref;

    // Try to arm DYN_ECO PLEP (we actually use a PLP)

    struct FG_error fg_error;

    fgPlepArm(NULL,                             // limits
              false,                            // polswitch_auto
              false,                            // invert_limits
              false,                            // test_arm
              0.0F,                             // initial_rate
              (ref2 - ref1) / reg_period,       // final_rate
              initial_ref,                      // initial_ref
              ref1,                             // final_ref
              acceleration,
              acceleration,                     // deceleration = acceleration
              linear_rate,
              0.0F,                             // exp_tc
              0.0F,                             // exp_final
              &next_func->dyn_eco_plep,
              &fg_error);

    // Dummy call to rewind TABLE/PPPL
    // The function generator for these two function types tracks position in the function.
    // Two calls to fgFuncRT above made the function generator advance the position.
    // Resetting it to the starting point may take a lot of time, that's why we do it now in the background.

    cc_double null_time = 0.0;

    fgFuncRT(&ref_armed->fg_pars, &null_time, &ref1);

    // If PLEP armed successfully

    if(fg_error.fg_errno == FG_OK)
    {
        // Save PLEP start time and return no warning (false)

        next_func->dyn_eco_plep_start_time = dyn_eco_end_time1 - next_func->dyn_eco_plep.meta.time.duration;

        return false;
    }

    // Return warning (true)

    return true;
}


// NON-RT   refEventPrepareNextEvent

static void refEventPrepareNextEvent(struct REF_event_mgr * const event_mgr,
                                     struct CC_ns_time      const event_time,
                                     struct CC_ns_time      const event_activation_time,
                                     uint32_t               const sub_sel,
                                     uint32_t               const cyc_sel,
                                     uint32_t               const ref_cyc_sel,
                                     enum REG_rst_source    const rst_source)
{
    struct REF_event * const next_event = refEventMgrGetNextEventBuffer(event_mgr);

    next_event->abs_time        = event_time;
    next_event->activation_time = event_activation_time;
    next_event->sub_sel         = sub_sel;
    next_event->cyc_sel         = cyc_sel;
    next_event->ref_cyc_sel     = ref_cyc_sel;
    next_event->rst_source      = rst_source;

    refEventMgrRegisterNextEvent(event_mgr, next_event);
}


// NON-RT   refEventPrepareNextFunc

cc_float refEventPrepareNextFunc(struct REF_mgr    * const ref_mgr,
                                 uint32_t            const sub_sel,
                                 uint32_t            const cyc_sel,
                                 uint32_t            const ref_cyc_sel,
                                 enum REF_event_type const event_type)
{
    struct REF_event_mgr * const event_mgr = &ref_mgr->event_mgr;
    struct REF_armed     * const ref_armed = &ref_mgr->ref_armed[ref_cyc_sel + sub_sel * ref_mgr->num_cyc_sels];
    struct REF_func      * const next_func = refEventMgrGetNextFuncBuffer(event_mgr, event_type);

    // Copy armed reference data into the next function

    next_func->sub_sel     = sub_sel;
    next_func->cyc_sel     = cyc_sel;
    next_func->ref_cyc_sel = ref_cyc_sel;
    next_func->event_type  = event_type;

    refEventPrepareFuncRef(next_func, ref_armed);

    // If economy mode is enabled for this cycle then try to arm a PLEP connecting the initial_ref level
    // with the requested point in the armed reference function

    ref_mgr->fsm.dyn_eco.arm_warning = refEventPrepareDynEco(ref_mgr, ref_armed, next_func, sub_sel, ref_cyc_sel);

    // Finish preparations according to the event type

    switch(event_type)
    {
        case REF_EVENT_START_FUNC:

            // START_FUNC: Prepare to arm the pre-function

            {
                struct REF_event * const next_event = event_mgr->next_start_func.event;

                cc_float pre_func_duration_available;
                bool     prev_func_meta_valid = (event_mgr->active.event != NULL);

                if(prev_func_meta_valid == true)
                {
                    // Still in a cycling state so calculate the duration available for the pre-function based on
                    // the previously armed function's meta data

                    pre_func_duration_available = cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(next_event->abs_time, ref_mgr->pre_func_mgr.prev_func_meta.abs_end_time));
                }
                else
                {
                    // Not in a cycling state (e.g. TO_CYCLING) so time for pre-function is based on the time remaining
                    // until the next event time

                    pre_func_duration_available = cctimeNsRelTimeToDoubleRT(cctimeNsSubRT(next_event->abs_time, ref_mgr->iter_time));
                }

                // Arm the pre-function sequence

                refPreFuncArm(ref_mgr, next_event, next_func, pre_func_duration_available, prev_func_meta_valid);

                // Save some meta data for the next function to be used on the next call to this function for REF_EVENT_START_FUNC

                refPreFuncSaveMeta(ref_mgr,
                                   cctimeNsAddRT(next_event->abs_time, cctimeNsDoubleToRelTimeRT(next_func->ref_armed.fg_pars.meta.time.end)),
                                   next_func->ref_armed.fg_pars.meta.range.final_ref,
                                   next_func->ref_armed.fg_pars.meta.range.final_rate);

                refEventMgrRegisterFuncBuffer(event_mgr, next_func, event_type);

                refFuncMgrSetFlagStartFunc(&ref_mgr->fg);

                refCycStatusMgrNext(ref_mgr);
            }

            break;


        case REF_EVENT_USE_ARM_NOW:

            // USE_ARM_NOW: Set flag and fall through to register the next function

            refFuncMgrSetFlagUseArmNow(&ref_mgr->fg);

            // Fall through to EVENT_RUN

        case REF_EVENT_RUN:

            // RUN & USE_ARM_NOW: Register the next function

            refEventMgrRegisterFuncBuffer(event_mgr, next_func, event_type);
            break;

        default: break;
    }

    refEventMgrRegisterNextFunc(event_mgr, event_type);

    // Return peak-peak range of the armed function

    return ref_armed->fg_pars.meta.range.max_ref - ref_armed->fg_pars.meta.range.min_ref;
}


// NON-RT   refEventGetRefAdvance

static int32_t refEventGetRefAdvance(struct REG_mgr const * const reg_mgr, enum REG_mode const reg_mode)
{
    // Return
    switch(reg_mode)
    {
        case REG_FIELD:

            return reg_mgr->b.rst_pars->ref_advance_ns;

        case REG_CURRENT:

            return reg_mgr->i.rst_pars->ref_advance_ns;

        case REG_VOLTAGE:

            return reg_mgr->v.reg_pars.ref_advance_ns;

        default:

            break;
    }

    return 0.0F;
}


// NON-RT   refEventStartFunc

bool refEventStartFunc(struct REF_mgr    * const ref_mgr,
                       struct CC_us_time * const event_us_time,
                       uint32_t                  sub_sel,
                       uint32_t            const cyc_sel,
                       bool                const economy)
{
    // return immediately if cyc_sel is out of range

    if(cyc_sel == 0 || cyc_sel >= ref_mgr->num_cyc_sels)
    {
        return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, false);
    }

    // Lock mutex if required, to protect against running at the same time as refArm()

    bool status = false;

    if(ref_mgr->mutex)
    {
        // Use lock callback defined for libreg

        ref_mgr->reg_mgr->mutex_lock(ref_mgr->mutex);
    }

    // Check that a start func event is not already pending and the state is compatible with this type of event

    if(   refEventMgrNextIsPending(&ref_mgr->event_mgr, REF_EVENT_START_FUNC) == false
       && refEventMgrRefStateIsValidRT(REF_EVENT_START_FUNC, ref_mgr->iter_cache.ref_mode, ref_mgr->ref_state_mask) == true)
    {
        // Set or clear the full_eco once flag using the supplied economy flag, gated by MODE_ECONOMY

        ref_mgr->fsm.full_eco.once = (economy == true && refMgrParValue(ref_mgr, MODE_ECONOMY) == CC_ENABLED);

        // Update sub_sel according to MODE REF_SUB_SEL

        sub_sel = refMgrSubSel(ref_mgr, sub_sel);

        // Calculate ref_cyc_sel

        uint32_t const test_cyc_sel = refMgrParValue(ref_mgr, MODE_TEST_CYC_SEL);
        bool     const test_cyc     = test_cyc_sel > 0 && cyc_sel == test_cyc_sel;
        uint32_t const ref_cyc_sel  = test_cyc == true
                                    ? refMgrParValue(ref_mgr, MODE_TEST_REF_CYC_SEL)
                                    : refMgrCycSel(ref_mgr, cyc_sel);

        // Set pointer to armed reference for ref_cyc_sel

        struct REF_armed * const ref_armed = &ref_mgr->ref_armed[ref_cyc_sel + sub_sel * ref_mgr->num_cyc_sels];

        // If reference is armed and CTRL PLAY is ENABLED

        if(   ref_armed->fg_pars.meta.type != FG_NONE
           && refMgrFgParValueSubCyc(ref_mgr, sub_sel, ref_cyc_sel, CTRL_PLAY) == CC_ENABLED)
        {
            // Prepare the next event and next function

            // Adjust event time by MODE EVENT_OFFSET_US and return the adjusted value through the event_us_time pointer

            *event_us_time = cctimeUsAddOffsetRT(*event_us_time, refMgrParValue(ref_mgr, MODE_EVENT_OFFSET_US));

            // Convert event time from CC_us_time to CC_ns_time

            struct CC_ns_time const event_time = cctimeUsToNsRT(*event_us_time);

            // Event activation time = event_time + (function_start_time - ref_advance)

            struct CC_ns_time const fg_start = cctimeNsDoubleToRelTimeRT(ref_armed->fg_pars.meta.time.start);
            struct CC_ns_time const fg_start_ref_adv = cctimeNsAddOffsetRT(fg_start, -refEventGetRefAdvance(ref_mgr->reg_mgr, ref_armed->reg_mode));
            struct CC_ns_time const event_activation_time = cctimeNsAddRT(event_time, fg_start_ref_adv);

            enum REG_rst_source const rst_source = (   test_cyc == true
                                                    && ref_mgr->pc_on_cache.load_select != regMgrParValue(ref_mgr->reg_mgr, LOAD_TEST_SELECT))
                                                 ? REG_TEST_RST_PARS
                                                 : REG_OPERATIONAL_RST_PARS;

            refEventPrepareNextEvent(&ref_mgr->event_mgr,
                                     event_time,
                                     event_activation_time,
                                     sub_sel,
                                     cyc_sel,
                                     ref_cyc_sel,
                                     rst_source);

            cc_float const func_ref_range = refEventPrepareNextFunc(ref_mgr,
                                                                    sub_sel,
                                                                    cyc_sel,
                                                                    ref_cyc_sel,
                                                                    REF_EVENT_START_FUNC);

            // Notify the Iterative Learning Controller for cyc_sel and not ref_cyc_sel
            // (which is zero if MODE REF_CYC_SEL is DISABLED). This is because even if the function
            // is the same for all CYC_SELs, the timing may not be and this requires independent
            // learning by the ILC.
            // Supply sub_sel, which must be zero for the ILC to be active on this cycle.

            refIlcEvent(ref_mgr, sub_sel, cyc_sel, event_time, func_ref_range);

            status = true;
        }
    }

    // Unlock mutex if in use

    if(ref_mgr->mutex)
    {
        // Use unlock callback defined for libreg

        ref_mgr->reg_mgr->mutex_unlock(ref_mgr->mutex);
    }

    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, status);
}


// NON-RT   refEventRun

bool refEventRun(struct REF_mgr * const ref_mgr)
{
    // If next ref_run structure is available and an armed reference is waiting to run

    if(   refEventMgrNextIsPending(&ref_mgr->event_mgr, REF_EVENT_RUN) == false
       && refEventMgrRefStateIsValidRT(REF_EVENT_RUN, ref_mgr->iter_cache.ref_mode, ref_mgr->ref_state_mask) == true)
    {
        // Convert event time from CC_us_time to CC_ns_time

        struct CC_ns_time event_time = cctimeUsToNsRT(refMgrParValue(ref_mgr, REF_RUN));

        // If event time is in the past then set the event time to be 1s in the future

        if(cctimeNsCmpAbsTimesRT(event_time, ref_mgr->iter_time) < 0)
        {
            event_time = ref_mgr->iter_time;
            event_time.secs.abs++;

            // Update REF_RUN with the updated run time

            refMgrParValue(ref_mgr, REF_RUN) = cctimeNsToUsRT(event_time);
        }

        refEventPrepareNextEvent(&ref_mgr->event_mgr,
                                 event_time,
                                 cc_zero_ns,                      // event_activation_time
                                 0,                               // sub_sel
                                 0,                               // cyc_sel
                                 0,                               // ref_cyc_sel
                                 REG_OPERATIONAL_RST_PARS);

        refEventPrepareNextFunc(ref_mgr,
                                0,                               // sub_sel
                                0,                               // cyc_sel
                                0,                               // ref_cyc_sel
                                REF_EVENT_RUN);                  // event_type

        return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, true);
    }

    // Run event ignored

    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, false);
}


// NON-RT   refEventAbort

bool refEventAbort(struct REF_mgr * const ref_mgr)
{
    if(refEventMgrRefStateIsValidRT(REF_EVENT_ABORT, ref_mgr->iter_cache.ref_mode, ref_mgr->ref_state_mask))
    {
        // Abort event accepted

        ref_mgr->event_mgr.abort = true;

        return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, true);
    }

    // Abort event ignored

    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, false);
}


// NON-RT   refEventPause

bool refEventPause(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.pause_event,
                                                                                REF_EVENT_PAUSE,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}


// NON-RT   refEventUnpause

bool refEventUnpause(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.unpause_event,
                                                                                REF_EVENT_UNPAUSE,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}


// NON-RT   refEventCoast

bool refEventCoast(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.coast_event,
                                                                                REF_EVENT_COAST,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}


// NON-RT   refEventRecover

bool refEventRecover(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.recover_event,
                                                                                REF_EVENT_RECOVER,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}


// NON-RT   refEventStartHarmonics

bool refEventStartHarmonics(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.start_harmonics_event,
                                                                                REF_EVENT_START_HARMONICS,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}


// NON-RT   refEventStopHarmonics

bool refEventStopHarmonics(struct REF_mgr * const ref_mgr, struct CC_us_time * const event_us_time)
{
    return refEventMgrSetExtEvtFlags(&ref_mgr->event_mgr, refEventPrepareAction(&ref_mgr->event_mgr.stop_harmonics_event,
                                                                                REF_EVENT_STOP_HARMONICS,
                                                                                ref_mgr->iter_cache.ref_mode,
                                                                                ref_mgr->ref_state_mask,
                                                                                *event_us_time));
}

// EOF
