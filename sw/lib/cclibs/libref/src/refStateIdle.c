//! @file  refStateIdle.c
//! @brief Converter Control Reference Manager library: IDLE reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"

RefState * refStateIdleInitRT(struct REF_mgr * const ref_mgr)
{
    struct REF_armed * const ref_armed = ref_mgr->ref_armed;

    // If requested then disarm reference for cyc_sel=0, sub_sel=0 if not NONE already

    if(ref_armed->fg_pars.meta.type != FG_NONE)
    {
        refMgrFgParValue(ref_mgr, REF_FG_TYPE) = FG_NONE;

        // Initializing NONE can never fail so no checks are needed

        fgNoneArmRT(&ref_armed->fg_pars, ref_mgr->fg_error);
    }

    // Reset active references and arm NONE in fsm fg_pars

    refEventMgrClearActiveRT(&ref_mgr->event_mgr);

    fgNoneArmRT(&ref_mgr->fsm.fg_pars, NULL);

    refFuncMgrUpdateParsRT(&ref_mgr->fg, &ref_mgr->fsm.fg_pars);

    // Request use of real-time delta reference

    refRtControlRealTimeRefRT(ref_mgr, REF_IDLE);

    return refStateIdleRT;
}



enum REF_state refStateIdleRT(struct REF_mgr * const ref_mgr)
{
    return REF_IDLE;
}

// EOF
