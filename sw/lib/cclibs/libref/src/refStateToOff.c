//! @file  refStateToOff.c
//! @brief Converter Control Reference Manager library: TO_OFF reference state functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libref.h"



RefState * refStateToOffInitRT(struct REF_mgr * const ref_mgr)
{
    // Disable use of real-time reference

    refRtControlRealTimeRefRT(ref_mgr, REF_OFF);

    // Force RST source to OPERATIONAL in case there was a fault while testing RST coefficients in CYCLING

    ref_mgr->reg_rst_source = REG_OPERATIONAL_RST_PARS;

    if(ref_mgr->latched_faults != 0)
    {
        // If a fault is present then cancel TEST_CYC_SEL

        refMgrParValue(ref_mgr, MODE_TEST_CYC_SEL) = 0;
    }

    // The real-time reference is not used in TO_OFF so set the function generator to the actual combined reference
    // in *reg_mgr->ref (ref_fg + ref_rt), in case ref_rt was being used in the previous state. If regulation field or current
    // and faults are present, then refStateToOffRT() will force open loop, so in this case, the initial reference should be the
    // open loop reference

    struct REG_mgr * const reg_mgr = ref_mgr->reg_mgr;

    cc_float initial_ref;
    cc_float initial_rate;

    if(ref_mgr->latched_faults == 0 || ref_mgr->iter_cache.reg_mode == REG_VOLTAGE)
    {
        initial_ref  = *reg_mgr->ref;
        initial_rate = refMgrInitialRate(ref_mgr);
    }
    else
    {
        // Regulating current or field and faults have caused the transition to TO_OFF state

        struct REG_mgr_signal * const reg_signal = reg_mgr->reg_signal;
        struct REG_rst_vars   * const rst_vars   = &reg_signal->rst_vars;

        // Use open loop reference and rate of reference to initialise the ramp down

        initial_rate = regRstDeltaOpenRefRT(rst_vars) * reg_signal->inv_reg_period;
        initial_ref  = reg_signal->ref_open + initial_rate * reg_mgr->ref_advance;

        // Clip initial rate to zero if it is current increasing the magnitude of the reference

        if(   (initial_ref >= 0.0F && initial_rate > 0.0F)
           || (initial_ref <= 0.0F && initial_rate < 0.0F))
        {
            initial_rate = 0.0F;
        }
    }

    ref_mgr->ref_fg_unlimited = ref_mgr->ref_fg = initial_ref;

    // Clear active events and then initialize the ramp down to zero

    refEventMgrClearActiveRT(&ref_mgr->event_mgr);

    refRampMgrInitRT(ref_mgr,
                     REF_START_RAMP_NOW,
                     initial_rate,
                     initial_ref,
                     0.0F,                   // final_ref
                     REF_USE_ACC_FOR_DEC);

    ref_mgr->fsm.to_off.completed = false;

    return refStateToOffRT;
}



enum REF_state refStateToOffRT(struct REF_mgr * const ref_mgr)
{
    refRampMgrRT(ref_mgr);

    // Force open-loop and save TO_OFF time if faults have become active

    if(ref_mgr->latched_faults != 0 && ref_mgr->to_off_time.secs.abs == 0)
    {
        ref_mgr->to_off_time = cctimeNsToUsRT(ref_mgr->iter_time);
        ref_mgr->force_openloop = true;
    }

    // Ramp down completed when current is below the low threshold and regulator is openloop or function has finished
    // INPORTANT : If reg_mode is voltage, then openloop is false always.
    //             If no current measurement is provided then flags.low == true always.

    struct REG_mgr        * const reg_mgr    = ref_mgr->reg_mgr;
    struct REG_mgr_signal * const reg_signal = reg_mgr->reg_signal;

    if(   reg_signal->lim_meas.flags.low == true
       && (reg_mgr->openloop == true || ref_mgr->post_func == true))
    {
        // If desired state is CYCLING then allow a limited number of consecutive attempts to continue cycling

        if(   ref_mgr->iter_cache.ref_mode == REF_CYCLING
           && ++ref_mgr->fsm.to_off.to_tc_transition_counter < ref_mgr->ref_to_tc_limit)
        {
            // If V_RATE_RMS fault was latched, then reset the RMS value to clear the fault

            if((ref_mgr->latched_faults & REF_V_RATE_RMS_FAULT_BIT_MASK) != 0)
            {
                regLimRmsResetRT(&ref_mgr->reg_mgr->lim_v_rate_rms);
            }

            // Reset latched faults

            refMgrResetFaultsRT(ref_mgr);
        }

        // Flag that the ramp down has finished - REF STATE will either go to OFF or TO_CYCLING

        ref_mgr->fsm.to_off.completed = true;
    }

    return REF_TO_OFF;
}

// EOF
