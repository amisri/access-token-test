/*!
 * @file  refState__State__.c
 * @brief Converter Control Reference Manager library: __STATE__ reference state functions
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2017. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libref.
 *
 * libref is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libref.h"

void refState__State__Init(struct REF_mgr * const ref_mgr)
{
    return;
}



RefState * refState__State__InitRT(struct REF_mgr * const ref_mgr)
{
    return refState__State__RT;
}



enum REF_state refState__State__RT(struct REF_mgr * const ref_mgr)
{
    return REF___STATE__;
}

// EOF
