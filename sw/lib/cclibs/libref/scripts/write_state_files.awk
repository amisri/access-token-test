#!/usr/bin/awk -f
#
# write_state_files.awk
# Converter Control Reference Manager library - state function file generator
#
# The script will generate skeleton src/refStateXXXXX.c and inc/libref/refStateXXXXX.h files.
# It was written to help during the development of the library and is maintained in case
# it may be useful for other state machines. It could be extended to generate
# the xxxState.c with the skeleton transition functions.
#
# The list of states must be hardcoded below.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libevtlog.
#
# libevtlog is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    system("rm -rf inc src; mkdir -p inc src")

    i = 0

    full_state_name[i++] = "OFF"
    full_state_name[i++] = "POL_SWITCHING"
    full_state_name[i++] = "TO_FAULT"
    full_state_name[i++] = "TO_OFF"
    full_state_name[i++] = "TO_STANDBY"
    full_state_name[i++] = "TO_CYCLING"
    full_state_name[i++] = "TO_IDLE"
    full_state_name[i++] = "DIRECT"
    full_state_name[i++] = "STANDBY"
    full_state_name[i++] = "CYCLING"
    full_state_name[i++] = "ECONOMY"
    full_state_name[i++] = "PAUSED"
    full_state_name[i++] = "IDLE"
    full_state_name[i++] = "ARMED"
    full_state_name[i++] = "RUNNING"

    i = 0

    state_name[i++]      = "Off"
    state_name[i++]      = "PolSwitching"
    state_name[i++]      = "ToFault"
    state_name[i++]      = "ToOff"
    state_name[i++]      = "ToStandby"
    state_name[i++]      = "ToCycling"
    state_name[i++]      = "ToIdle"
    state_name[i++]      = "Direct"
    state_name[i++]      = "Standby"
    state_name[i++]      = "Cycling"
    state_name[i++]      = "Economy"
    state_name[i++]      = "Paused"
    state_name[i++]      = "Idle"
    state_name[i++]      = "Armed"
    state_name[i++]      = "Running"

    for(i in state_name)
    {
        WriteStateFile("state_template.h", full_state_name[i], state_name[i], "inc/refState" state_name[i] ".h")
        WriteStateFile("state_template.c", full_state_name[i], state_name[i], "src/refState" state_name[i] ".c")
    }
}



function WriteStateFile(template_file, full_name, name, of)
{
    print "Reading " template_file " to generate " of

    while((getline < template_file) > 0)
    {
        print SubstituteStateName($0, full_name, name) > of
    }

    close(template_file)
    close(of)
}



function SubstituteStateName(line, full_name, name)
{
    gsub("__STATE__", full_name,          line)
    gsub("__State__", name,               line)
    gsub("__state__", tolower(full_name), line)

    return line
}

# EOF
