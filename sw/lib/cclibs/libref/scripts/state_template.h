/*!
 * @file  refState__State__.h
 * @brief Converter Control Reference Manager library: __STATE__ reference state functions header file
 *
 * <h4> Copyright </h4>
 *
 * Copyright CERN 2017. This project is released under the GNU Lesser General
 * Public License version 3.
 *
 * <h4> License </h4>
 *
 * This file is part of libref.
 *
 * libref is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "libref.h"

//! __STATE__ state structure

struct REF_state___state__
{
    char	unused;		    //!< To suppress compiler warnings
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Initialize variables for __STATE__ state.
 *
 * This is called on start up to initialize variables used in __STATE__ state.
 *
 * @param[in,out]     ref_mgr              Pointer to reference manager structure.
 */
void refState__State__Init(struct REF_mgr * const ref_mgr);



/*!
 * Initialize operation in __STATE__ state.
 *
 * This is called on entry into the __STATE__ reference state to prepare operation of the refState__State__RT() function.
 *
 * @param[in,out]     ref_mgr              Pointer to reference manager structure.
 *
 * @retval refState__State__RT     Pointer to function for __STATE__ state.
 */
RefState * refState__State__InitRT(struct REF_mgr * const ref_mgr);



/*!
 * Operate __STATE__ state.
 *
 * This is called every iteration of the reference state machine when the state is __STATE__.
 * This includes the first iteration, following the call to refState__State__InitRT(), during
 * which ref_mgr->ref_state still has the index of the previous state.
 *
 * @param[in,out]     ref_mgr              Pointer to reference manager structure.
 *
 * @retval REF___STATE__     Index of the __STATE__ state.
 */
enum REF_state refState__State__RT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
