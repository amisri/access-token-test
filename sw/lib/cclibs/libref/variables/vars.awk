# libref/variables/vars.awk
#
# Converter Control Reference Manager library read-only variables header file generator
#
# All libref variables that might be interesting to an application are
# identified in vars.csv. This allows this script to create a
# header file with a macro and constants that allow the application developer
# easy read-only access the variables.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libref.
#
# libref is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

# Set field separater to comma to read csv file

    FS = ","

# Identify the columns in the csv file

    var_pars_column    = 1
    var_name_column    = 2
    var_type_column    = 3
    var_ref_mgr_column = 4
    var_comment_column = 5

# Prepare to calculate maximum var_id length and var_type length

    max_var_id_len   = 0
    max_var_type_len = 0
    max_var_ref_mgr  = 0

# Read and discard heading line from stdin

    getline

# Read variable definitions from stdin

    n_vars   = 0
    line_num = 1

    while(getline > 0)
    {
        line_num++

        # Skip blank lines

        if($var_pars_column == "") continue

        # Stop if non-blank lines do not have the correct number of colums

        if($var_comment_column == "")
        {
            Error("stdin", line_num, "Missing data")
        }

        # Save contents

        var_id      [n_vars] = $var_pars_column "_" $var_name_column
        var_type    [n_vars] = $var_type_column
        var_ref_mgr [n_vars] = $var_ref_mgr_column
        var_comment [n_vars] = $var_comment_column

        var_id_len      = length(var_id     [n_vars])
        var_type_len    = length(var_type   [n_vars])
        var_ref_mgr_len = length(var_ref_mgr[n_vars])

        if(var_id_len > max_var_id_len)
        {
            max_var_id_len = var_id_len
        }

        if(var_type_len > max_var_type_len)
        {
            max_var_type_len = var_type_len
        }

        if(var_ref_mgr_len > max_var_ref_mgr)
        {
            max_var_ref_mgr = var_ref_mgr_len
        }

        n_vars++
    }

# Generate variable header file

    of = "inc/libref/refVars.h"   # Set output file (of)

    WriteGnuLicense("libref", "Reference Manager", "Variables header file", of)

    print "#ifndef REFVARS_H"                                                                                        > of
    print "#define REFVARS_H\n"                                                                                      > of

    print "//! Use refMgrVarValue() with a pointer to the reg_mgr structure.\n"                                   > of

    print "#define refMgrVarValue(REF_MGR, VAR_NAME)    ((REF_MGR)->REF_VAR_ ## VAR_NAME)"                        > of
    print "#define refMgrVarPointer(REF_MGR, VAR_NAME)  (&(REF_MGR)->REF_VAR_ ## VAR_NAME)\n"                     > of

    for(i=0 ; i < n_vars ; i++)
    {
        printf "#define REF_VAR_%-*s %-*s // %-*s %s\n",
                max_var_id_len,   var_id[i],
                max_var_ref_mgr,  var_ref_mgr[i],
                max_var_type_len, var_type[i],
                var_comment[i] > of
    }

    print "\n#endif // REFVARS_H\n"                                                                                  > of
    print "// EOF"                                                                                                   > of

    close(of)
}

# EOF

