# libref/parameters/pars.awk
#
# Converter Control Reference management library parameter header file generator
#
# All libref parameters are defined in libref/parameteres/pars.csv and this
# script translates the csv file into a header file that declares and defines
# structures that describe all the parameters.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libref.
#
# libref is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    # Set field separater to comma to read csv file

    FS = ","

    # Identify the leading columns in the csv files

    pars_column           = 1
    par_name_column       = 2
    par_type_column       = 3
    par_length_column     = 4
    par_doc_column        = 5

    par_fg_type_column    = 5
    par_fg_doc_column     = 6

    # Read pars.csv and fg_pars.csv

    ReadCsv("parameters/pars.csv")

    ReadFgCsv("parameters/fg_pars.csv")

    # Generate header files and parameters template file

    WriteRefParsHeader("inc/libref/refPars.h")
    WriteRefFgParIdxHeader("inc/libref/refFgParIdx.h")
    WriteRefFgTypeHeader("inc/libref/refFgType.h")

    WriteRefParsInitHeader("inc/libref/refParsInit.h")
    WriteRefParsLinkStructsHeader("inc/libref/refParsLinkStructs.h")

    WriteRefParsInitTemplate("parameters/refParsInitTemplate.txt")
}

# -------------------------------------------------------------------------------------------------

function max_length(var, max_var_len)
{
    var_len = length(var)

    if(var_len > max_var_len)
    {
        return(var_len)
    }

    return max_var_len
}

# -------------------------------------------------------------------------------------------------

function ReadCsv(input_file,  i)
{
    # Read and ignore heading line from stdin

    getline < input_file

    # Prepare to calculate max length of par_variable

    max_par_variable_len = 0
    max_par_type_len     = 0

    # Read parameter definitions from stdin

    n_pars = 0

    while((getline < input_file) > 0)
    {
        # Skip blank lines

        if($par_name_column == "") continue

        # Check for new PARS value

        if($pars_column != "") pars = $pars_column

        # Save contents

        par_label      [n_pars] = pars " " $par_name_column
        par_variable   [n_pars] = tolower(pars) "_" tolower($par_name_column)
        par_type       [n_pars] = $par_type_column
        par_length     [n_pars] = $par_length_column
        par_doc        [n_pars] = $par_doc_column

        max_par_variable_len = max_length(par_variable[n_pars], max_par_variable_len)
        max_par_type_len     = max_length(par_type[n_pars],     max_par_type_len)

        n_pars++
    }
}

# -------------------------------------------------------------------------------------------------

function ReadFgCsv(input_file,  i)
{
    # Read heading line from stdin

    getline < input_file

    n_fg_pars = 0
    n_groups  = 0

    # Prepare to calculate max length of par_variable

    max_fg_par_variable_len = 0
    max_fg_par_type_len     = 0

    # Read parameter definitions from stdin

    line_num = 1

    while((getline < input_file) > 0)
    {
        line_num++

        # Skip blank lines

        if($par_name_column == "") continue

        # Check for new PARS value which is the start of a new parameter group

        if($pars_column != "")
        {
            n_groups++
            par_idx           = n_fg_pars
            pars              = $pars_column
            fg_PAR[n_fg_pars] = pars
            fg_par[n_fg_pars] = tolower(pars)
            fg_Par[n_fg_pars] = substr(pars,1,1) tolower(substr(pars,2))
        }

        # Save contents

        n_par_names     [par_idx]++
        fg_par_label    [n_fg_pars] = pars " " $par_name_column
        fg_par_name     [n_fg_pars] = $par_name_column
        fg_par_variable [n_fg_pars] = tolower(pars) "_" tolower($par_name_column)
        fg_par_type     [n_fg_pars] = $par_type_column
        fg_par_length   [n_fg_pars] = $par_length_column
        fg_par_fg_type  [n_fg_pars] = $par_fg_type_column
        fg_par_doc      [n_fg_pars] = $par_fg_doc_column

        max_fg_par_variable_len = max_length(fg_par_variable[n_fg_pars], max_fg_par_variable_len)
        max_fg_par_type_len     = max_length(fg_par_type[n_fg_pars],     max_fg_par_type_len)
        max_fg_par_fg_type_len  = max_length(fg_par_fg_type[n_fg_pars],  max_fg_par_fg_type_len)

        if(index($par_name_column,"_NUM_ELS") == 0)
        {
            num_fg_pars_per_group[n_groups]++
        }

        n_fg_pars++
    }

    # Calculate the maximum number of parameters per group

    max_fg_pars_per_group = 0

    for(i=1; i <= n_groups ; i++)
    {
        if(num_fg_pars_per_group[i] > max_fg_pars_per_group)
        {
            max_fg_pars_per_group = num_fg_pars_per_group[i]
        }
    }
}

# -------------------------------------------------------------------------------------------------

function WriteRefParsHeader(of,  i)
{
    WriteGnuLicense("libref", "Reference Manager", "Parameter header file", of)

    print "#pragma once\n" > of

    print "#include \"libref/refFgParIdx.h\"\n" > of

    print "// Non-cyclic parameter\n" > of

    print "#define refMgrParValue(REF_MGR,PAR_NAME)                (REF_MGR)->REF_PAR_ ## PAR_NAME[0]" > of
    print "#define refMgrParValueByIndex(REF_MGR,PAR_NAME,INDEX)   (REF_MGR)->REF_PAR_ ## PAR_NAME[INDEX]" > of
    print "#define refMgrParPointer(REF_MGR,PAR_NAME)            (&(REF_MGR)->REF_PAR_ ## PAR_NAME[0])\n" > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "#define REF_PAR_%-*s  pars.%-*s  // %s\n", max_par_variable_len, toupper(par_variable[i]), max_par_variable_len, par_variable[i], par_doc[i] > of
    }

    print "\nstruct REF_pars" > of
    print "{" > of

    for(i=0 ; i < n_pars ; i++)
    {
        printf "    %-*s  %-*s [%s];\n", max_par_type_len,par_type[i],max_par_variable_len,par_variable[i],par_length[i] > of
    }

    print "};\n" > of

    print "// Function generator parameter\n" > of

    print "#define REF_FG_PAR_NOT_USED            NULL\n" > of

    print "#define refMgrFgParInitPointer(REF_MGR,FG_PAR_NAME,VALUE_P)               (REF_MGR)->u.fg_pars.FG_PAR_NAME.value=VALUE_P" > of
    print "#define refMgrFgParInitCycSelStep(REF_MGR,FG_PAR_NAME,CYC_SEL_STEP)       (REF_MGR)->u.fg_pars.FG_PAR_NAME.meta.cyc_sel_step=CYC_SEL_STEP" > of
    print "#define refMgrFgParInitSubSelStep(REF_MGR,FG_PAR_NAME,SUB_SEL_STEP)       (REF_MGR)->u.fg_pars.FG_PAR_NAME.meta.sub_sel_step=SUB_SEL_STEP\n" > of

    print "#define refMgrFgParValue(REF_MGR,FG_PAR_NAME)                             (REF_MGR)->u.fg_pars.REF_FG_PAR_ ## FG_PAR_NAME ## _STRUCT.value[0]" > of
    print "#define refMgrFgParValueSubCyc(REF_MGR,SUB_SEL,CYC_SEL,FG_PAR_NAME)      *(REF_FG_PAR_ ## FG_PAR_NAME ## _TYPE *)(refMgrFgParPointerSubCycFunc(REF_MGR, SUB_SEL, CYC_SEL, REF_FG_PAR_ ## FG_PAR_NAME))" > of
    print "#define refMgrFgParPointerSubCyc(REF_MGR,SUB_SEL,CYC_SEL,FG_PAR_NAME)     (REF_FG_PAR_ ## FG_PAR_NAME ## _TYPE *)(refMgrFgParPointerSubCycFunc(REF_MGR, SUB_SEL, CYC_SEL, REF_FG_PAR_ ## FG_PAR_NAME))" > of

    print "\n// Function Generator Parameter types\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "#define REF_FG_PAR_%-*s%s\n",
                max_fg_par_variable_len + 11,  # Add 6 to allow for "_TYPE "
                toupper(fg_par_variable[i]) "_TYPE", fg_par_type[i] > of
    }

    print "\n// Function generator Parameter variables\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "#define REF_FG_PAR_%-*s%s\n",
                max_fg_par_variable_len + 11,  # Add 8 to allow for "_STRUCT "
                toupper(fg_par_variable[i]) "_STRUCT", fg_par_variable[i] > of
    }

    print "\n// Function Generation Parameter structures and union\n" > of

    print "struct REF_fg_par_meta" > of
    print "{" > of
    print "    uint32_t                 size_of_element;" > of
    print "    uint32_t                 cyc_sel_step;" > of
    print "    uint32_t                 sub_sel_step;" > of
    print "};\n" > of

    print "struct REF_fg_par" > of
    print "{" > of
    print "    char                   * value;" > of
    print "    struct REF_fg_par_meta   meta;" > of
    print "};\n" > of

    print "union REF_fg_pars" > of
    print "{" > of
    print "    struct REF_fg_par        fg_par[REF_NUM_FG_PARS];" > of
    print "    struct" > of
    print "    {" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "        struct  // %3u. %s\n", i+1, fg_par_label[i] > of
        print  "        {" > of
        printf "            %-*s * value;\n", max_fg_par_type_len,fg_par_type[i] > of
        printf "            %-*s   meta;\n",  max_fg_par_type_len, "struct REF_fg_par_meta" > of
        printf "        } %s;\n\n", fg_par_variable[i] > of
    }

    print "    } fg_pars;" > of
    print "};\n" > of

    print "// Enums for the parameters for each type of reference function except TABLE\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(fg_par_length[i] == 0) continue

        if(fg_par[i] != "")
        {
            if(i > 0) printf "        %s\n    };\n\n", num_pars > of

            num_pars = fg_PAR[i] "_NUM_PARS"

            printf "    // Enum for %s parameters\n\n", fg_PAR[i] > of
            printf "    enum REF_%s\n", fg_par[i] > of
            print  "    {" > of
        }

        printf "        %-*s ,\n", max_fg_par_variable_len,toupper(fg_par_variable[i]) > of
    }

    printf "        %s\n    };\n\n", num_pars > of

    print "// Structures for the parameters for each type of reference function except TABLE\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(fg_par_length[i] == 0) continue

        if(fg_par[i] != "")
        {
            if(i > 0) printf "    };\n\n" > of

            printf "    // Structure for %s parameters\n\n", fg_PAR[i] > of
            printf "    struct REF_%s_pars\n", fg_par[i] > of
            print  "    {" > of

            max_var_len = max_fg_par_variable_len - length(fg_par[i])
        }

        printf "        %-*s  %-*s[%s];\n", max_fg_par_type_len,fg_par_type[i],max_var_len,tolower(fg_par_name[i]),fg_par_length[i] > of
    }

    printf "    };\n\n" > of

    print "// EOF" > of

    close(of)
}

# -------------------------------------------------------------------------------------------------

function WriteRefFgParIdxHeader(of,  i, first_transactional_idx)
{
    WriteGnuLicense("libref", "Reference Manager", "Function Generator Parameter Indexes header file", of)

    print "#pragma once\n" > of

    print "// Constants\n" > of

    print "#define REF_FG_PAR_MAX_ARM_PAR_VALUES    ", max_fg_pars_per_group - 1 > of

    # REF_FG_PAR_MAX_PER_GROUP needs 3 extra elements for REF FG_TYPE, REF REG_MODE and the terminating NULL

    print "#define REF_FG_PAR_MAX_PER_GROUP        ", max_fg_pars_per_group+3 > of

    print "\n// Function Generator Parameter indices enum\n" > of

    print "enum REF_fg_par_idx  " > of
    print "{" > of

    first_transactional_idx = 0

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "    REF_FG_PAR_%-*s ,      // %2u  %s\n", max_fg_par_variable_len, toupper(fg_par_variable[i]), i, fg_par_doc[i] > of

        # Find first transactional parameter based on the documentation string not containing "Non-transactional"

        if(first_transactional_idx == 0 && index(fg_par_doc[i], "Non-transactional") == 0)
        {
            first_transactional_idx = i
        }
    }

    print "    REF_NUM_FG_PARS," > of
    print "    REF_FG_PAR_NULL," > of
    print "    REF_FG_FIRST_TRANSACTIONAL_PAR = REF_FG_PAR_" toupper(fg_par_variable[first_transactional_idx])  > of
    print "};\n" > of

    print "// EOF" > of

    close(of)
}

# -------------------------------------------------------------------------------------------------

function WriteRefFgTypeHeader(of,  i)
{
    WriteGnuLicense("libref", "Reference Manager", "Function Generator Parameter default fg_type header file", of)

    print "// Static array of default FG_TYPE to arm if a given FG Parameter is set\n" > of

    print "static enum FG_type const transaction_fg_type[] = " > of
    print "{" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "    FG_%-*s ,      // %2u  %-*s   %s\n",
                max_fg_par_fg_type_len, fg_par_fg_type[i], i,
                max_fg_par_variable_len, toupper(fg_par_variable[i]), fg_par_doc[i] > of
    }

    print "};\n" > of

    print "// EOF" > of

    close(of)
}

# -------------------------------------------------------------------------------------------------

function WriteRefParsInitHeader(of,  i)
{
    WriteGnuLicense("libref", "Reference Manager", "Parameter initialization header file", of)

    print "static void refParsInit(struct REF_mgr *ref_mgr)" > of
    print "{" > of
    print "    // Initialise the size_of_element for each function generation parameter\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        printf "    ref_mgr->u.fg_pars.%-*s = sizeof(%s);\n", max_fg_par_variable_len+21, fg_par_variable[i] ".meta.size_of_element", fg_par_type[i] > of
    }

    print "}\n" > of

    print "// EOF" > of

    close(of)
}

# -------------------------------------------------------------------------------------------------

function WriteRefParsLinkStructsHeader(of,  i)
{
    WriteGnuLicense("libref", "Reference Manager", "Parameter initialization header file", of)

    print "// Static inline functions to link function generation parameter structures" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(fg_par_length[i] == 0) continue

        if(fg_par[i] != "")
        {
            staticInlineBody(of, i)
        }
    }

    print "// EOF" > of

    close(of)
}

# -------------------------------------------------------------------------------------------------

function staticInlineBody(of, idx,  i, par, n_names, name)
{
    indent = length(fg_Par[idx]) + 30

    printf "\nstatic inline void refMgrInit%sPars(struct REF_mgr * const ref_mgr,\n", fg_Par[idx] > of
    printf "    %-*s%s,\n", indent, " ",  "struct REF_" fg_par[idx] "_pars * const " fg_par[idx] > of
    printf "    %-*s%s,\n", indent, " ",  "uint32_t const sub_sel_step" > of
    printf "    %-*s%s)\n", indent, " ",  "uint32_t const cyc_sel_step" > of
    printf "{\n" > of
    printf "    // Link to %s function generation parameters\n", fg_PAR[idx] > of

    n_names = n_par_names[idx]
    par = fg_par[idx]

    for(i=0 ; i < n_names ; i++)
    {
        name = tolower(fg_par_name[idx])

        printf "\n    refMgrFgParInitPointer   (ref_mgr, %-*s, %s->%s);\n",       max_fg_par_variable_len, fg_par_variable[idx],par,name > of
        printf   "    refMgrFgParInitSubSelStep(ref_mgr, %-*s, sub_sel_step);\n", max_fg_par_variable_len, fg_par_variable[idx] > of
        printf   "    refMgrFgParInitCycSelStep(ref_mgr, %-*s, cyc_sel_step);\n", max_fg_par_variable_len, fg_par_variable[idx] > of

        idx++
    }

    print "}\n" > of
}

# -------------------------------------------------------------------------------------------------

function WriteRefParsInitTemplate(of,  i)
{
    print "// Libref individual function generation parameter initialization template\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(fg_par[i] != "")
        {
            printf "    // Link to function generation parameters for %s\n\n", fg_PAR[i] > of
        }

        printf "    refMgrFgParInitPointer   (&ref_mgr, %-*s, pointer_to_your_variable);\n", max_fg_par_variable_len, fg_par_variable[i] > of
        printf "    refMgrFgParInitSubSelStep(&ref_mgr, %-*s, 0);\n", max_fg_par_variable_len, fg_par_variable[i] > of
        printf "    refMgrFgParInitCycSelStep(&ref_mgr, %-*s, 0);\n\n", max_fg_par_variable_len, fg_par_variable[i] > of
    }

    print "// Libref function generation parameter structures initialization template\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(fg_par_length[i] == 0) continue

        if(fg_par[i] != "")
        {
            indent = length(fg_Par[i]) + 15

            if(i > 0) print ");\n" > of

            printf "    // Link to function generation parameter structure for %s\n\n", fg_PAR[i] > of

            printf "    refMgrInit%sPars(&ref_mgr,\n", fg_Par[i] > of
            printf "    %-*s%s,\n", indent, " ",  "pointer_to_your_" fg_Par[i] "_structure" > of
            printf "    %-*s%s,\n", indent, " ",  "sub_sel_step" > of
            printf "    %-*s%s",    indent, " ",  "cyc_sel_step" > of
        }
    }

    print ");\n" > of

    print "// Libref function generation parameter user pointers\n" > of

    for(i=0 ; i < n_fg_pars ; i++)
    {
        if(index(fg_par_variable[i], "num_els") == 0)
        {
            printf "    fg_pars_link[REF_FG_PAR_%-*s] = pointer_to_your_user_data;  // %s\n", max_fg_par_variable_len, toupper(fg_par_variable[i]),  fg_par_doc[i] > of
        }
    }

    print "\n// EOF" > of

    close(of)
}

# EOF
