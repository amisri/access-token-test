//! @file      libref.h
//! @brief     Reference management library top-level header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.
//!
//! @mainpage CERN Converter Control Library: Reference Manager
//!
//! Libref manages the generation of the reference function and the polarity switch (if present).
//! It is based on the reference state machine:
//!
//! @image html ref_states_diagram.png
//!
//! The states have abbreviated names and can be stable or transitional.
//!
//! @image html ref_states_overview.png
//!
//! Here is a summary of some aspects of the states:
//!
//! @image html ref_states_summary.png
//!
//! Some states exploit the features of the Ramp Manager, which is a facility in libreg that can smoothly
//! transition from one reference value to another, if necessary changing the polarity switch position.
//!
//! @image html ref_states_use_of_RampMgr.png

#pragma once

// Private Constants

#define REF_TIME_ERROR          0.4E-6  //!< Time adjustment to compensate for rounding errors with double precision time

// Macros defining sizes of structures arrays that the application must allocate

#define REF_ARMED_TABLE_FUNCTION_LEN(NUM_CYC_SELS, NUM_SUB_SELS, NUM_TABLE_POINTS) ((NUM_CYC_SELS)*(NUM_SUB_SELS)*(NUM_TABLE_POINTS))
#define REF_RUNNING_TABLE_FUNCTION_LEN(NUM_TABLE_POINTS)                           ((NUM_TABLE_POINTS)*3)

// Private enum constants

// IMPORTANT: Static arrays for refEventMgrRefStateIsValid() in refEventMgr.c depend on the order of enum REF_event_type

enum REF_event_type
{
    REF_EVENT_NONE            ,
    REF_EVENT_START_FUNC      ,
    REF_EVENT_USE_ARM_NOW     ,
    REF_EVENT_RUN             ,
    REF_EVENT_ABORT           ,
    REF_EVENT_PAUSE           ,
    REF_EVENT_UNPAUSE         ,
    REF_EVENT_COAST           ,
    REF_EVENT_RECOVER         ,
    REF_EVENT_START_HARMONICS ,
    REF_EVENT_STOP_HARMONICS  ,
    REF_NUM_EVENT_TYPES
};

enum REF_thread
{
    REF_THREAD_RT ,
    REF_THREAD_BG ,
    REF_NUM_THREADS
};

//! Forward declaration of structs

struct REF_mgr;
struct REF_func;
struct REF_event;


//! Include header files

#include "libcctime.h"
#include "libfg.h"
#include "libpolswitch.h"
#include "libreg.h"
#include "libref/refVars.h"
#include "libref/refPars.h"
#include "libref/refFgPars.h"
#include "libref/refRampMgr.h"
#include "libref/refPreFuncArm.h"
#include "libref/refPreFuncMgr.h"
#include "libref/refEvent.h"
#include "libref/refIlc.h"
#include "libref/refIlcState.h"
#include "libref/refArm.h"
#include "libref/refRestore.h"
#include "libref/refEventMgr.h"
#include "libref/refFuncMgr.h"
#include "libref/refCycStatusMgr.h"
#include "libref/refState.h"
#include "libref/refRt.h"
#include "libref/refMgr.h"

//! REF_defaults_ptrs structure contains pointers to default reference values

struct REF_default_ptrs
{
    cc_float                  * acceleration;                               //!< Pointer to default acceleration for a given regulation mode and load select
    cc_float                  * deceleration;                               //!< Pointer to default deceleration for a given regulation mode and load select
    cc_float                  * linear_rate;                                //!< Pointer to default linear_rate for a given regulation mode and load select
    cc_float                  * min_rms;                                    //!< Pointer to default min_rms for a given regulation mode and load select
    cc_float                  * pre_func_max;                               //!< Pointer to default MAX pre-function value for a given regulation mode and load select
    cc_float                  * pre_func_min;                               //!< Pointer to default MIN pre-function value for a given regulation mode and load select
};

//! REF_limits structure contains pointers to operational limits values

struct REF_limits
{
    cc_float                  * pos;                                        //!< Pointer to the operational reference limit for a given regulation mode
    cc_float                  * standby;                                    //!< Pointer to the operational reference limit for a given regulation mode
    cc_float                  * neg;                                        //!< Pointer to the operational reference limit for a given regulation mode
    cc_float                  * rate;                                       //!< Pointer to the operational reference limit for a given regulation mode
};

//! REF_zero_values structure of variables with the value zero. These variables can then be referenced
//! by pointers that must hold such value, for example in ref_mgr.defaults


struct REF_zero_values
{
    cc_float                    zero_float;                                 //!< Float with a zero value
};

//! REF_rt_ref structure with variables needed for the real-time reference interpolation

struct REF_rt_ref
{
    uint32_t                    num_invalid;                                //!< Counter of invalid rt_ref values
    uint32_t                    down_counter;                               //!< Down counter for each period of interpolation
    cc_float                    value;                                      //!< Actual value
    cc_float                    last_value;                                 //!< Last value from application (can be invalid)
    cc_float                    step;                                       //!< Interpolation step per iteration
    bool                        direct;                                     //!< True when rt_ref.value is in use in DIRECT state
    bool                        delta;                                      //!< True when rt_ref.value is in use in IDLE or CYCLING states
    bool                        in_use;                                     //!< True if rt_ref.direct or rt_ref.delta is true
};

//! Voltage Feed forward signal structure

struct REF_vfeedfwd_delay
{
    struct CC_ns_time           timestamp;                                  //!< Timestamp of the latest sample
    struct CC_meas_signal       latest;                                     //!< Latest sample for logging
    cc_float                    inv_period_s;                               //!< 1/Period for the latest sample
    cc_float                    signal  [REF_VFF_DELAY_BUF_LEN];            //!< Circular buffer for feed forward signal
    bool                        is_valid[REF_VFF_DELAY_BUF_LEN];            //!< Circular buffer for the feed forward signal validity
    uint32_t                    buf_index;                                  //!< Free running index of the newest sample in circular buffers
};

//! REF_static : structure of variable structures that are 2D arrays in the application memory.
//!
//! This structure is instantiated in ref_mgr as a way to simplify the access to interesting libref variables
//! by the application.
//!
//! The order of the fields is important to refMgrVarPointerFunc()


struct REF_static_vars
{
    struct FG_error             fg_error;                                   //!< Dummy static structures to support refMgrVar macros
    struct REF_cyc_status       cyc_status;                                 //!< Dummy static structures to support refMgrVar macros
    struct REF_armed            ref_armed;                                  //!< Dummy static structures to support refMgrVar macros
};

//! Cached values for this period with PC STATE on

struct REF_pc_on_cache
{
    uint32_t                    load_select;                                //!< Copy of actual load_select
    enum REG_actuation          actuation;                                  //!< Copy of libreg PC ACTUATION parameter
    cc_float                    max_v_ref;                                  //!< Max voltage reference for LOAD_SELECT
    cc_float                    min_v_ref;                                  //!< Min voltage reference for LOAD_SELECT
    cc_float                    max_i_ref;                                  //!< Max current reference for LOAD_SELECT
    cc_float                    min_i_ref;                                  //!< Min current reference for LOAD_SELECT
    cc_float                    max_b_ref;                                  //!< Max field reference for LOAD_SELECT
    cc_float                    min_b_ref;                                  //!< Min field reference for LOAD_SELECT
    cc_float                    max_ref;                                    //!< Max reference for LOAD_SELECT and MODE_REG_MODE_CYC
    cc_float                    min_ref;                                    //!< Min reference for LOAD_SELECT and MODE_REG_MODE_CYC
};

//! Cached values for this iteration

struct REF_iter_cache
{
    enum REG_mode               reg_mode;                                   //!< Copy of actual reg_mode
    enum REG_mode               mode_reg_mode;                              //!< Copy of requested reg_mode
    enum REF_pc_state           pc_state;                                   //!< Copy of the power converter state
    enum REF_state              ref_mode;                                   //!< Copy of requested ref state
};

//! REF_mgr structure is the principal libref structure

struct REF_mgr
{
    struct POLSWITCH_mgr      * polswitch_mgr;                              //!< Pointer to the polarity switch manager structure
    struct REG_mgr            * reg_mgr;                                    //!< Pointer to regulation manager structure

    void                      * mutex;                                      //!< Pointer to mutex to protect refArm from refEvent functions (NULL if not required)

    struct REF_pars             pars;                                       //!< libref non-cyclic parameters
    union  REF_fg_pars          u;                                          //!< libref function generation (cyclic) parameter pointers and meta data
    bool                        not_sub_sel_fg_types[FG_NUM_FUNCS];         //!< True for function types that do not support sub_sel
    bool                        not_cyc_sel_fg_types[FG_NUM_FUNCS];         //!< True for function types that do not support cyc_sel

    struct REF_pc_on_cache      pc_on_cache;                                //!< Cached values for this period with PC STATE on
    struct REF_iter_cache       iter_cache;                                 //!< Cached values for this iteration

    uint32_t                    num_cyc_sels;                               //!< Number of cycle selectors (includes cyc_sel zero)
    uint32_t                    max_table_points;                           //!< Maximum number of points in a table
    enum REG_rst_source         reg_rst_source;                             //!< Selector between operational or test RST parameters

    uint32_t                    ref_to_tc_limit;                            //!< TO_OFF to TO_CYCLING limit (from refMgrInit)
    uint32_t                    faults_in_off;                              //!< Set to ref_mgr.latched_faults when ref_state is REF_OFF, zero otherwise
    uint32_t                    latched_faults;                             //!< Latched faults bit mask
    uint32_t                    unlatched_faults;                           //!< Unlatched faults bit mask
    uint32_t                    warnings;                                   //!< Warnings bit mask

    uint32_t                    iteration_index;                            //!< Regulation iteration index returned by regMgrMeasSetRT() for the active regulation mode
    struct CC_ns_time           iter_time;                                  //!< Time of the current iteration (s, ns)
    cc_double                   iter_time_s;                                //!< Time of the current iteration (s) as a double

    struct REF_func_mgr         fg;                                         //!< Variables related to function generation

    struct REF_rt_ref           rt_ref;                                     //!< Real-time reference control

    uint32_t                    ref_state_counter;                          //!< Free-running counter incremented after each execution of the reference state machine
    enum REF_state              ref_state;                                  //!< Reference state enum
    RefState                  * ref_state_func;                             //!< Pointer to reference state function
    uint32_t                    ref_state_mask;                             //!< Reference state bit mask (1 bit only will be active)
    struct CC_us_time           to_off_time;                                //!< Time of last transition to TO_OFF reference state (s, us)

    bool                        reg_complete;                               //!< Flag from regMgrRegRT to signal that the actuation is ready to use
    bool                        post_func;                                  //!< Post function flag is set one iteration after the function ends
    bool                        enable_reg_err;                             //!< True to enabled regulation error calculation and limits checking
    bool                        force_openloop;                             //!< True to force open loop
    bool                        reg_meas_latched;                           //!< True if regulation measurement has been latched
    bool                        prev_meas_latch[2];                         //!< Previous meas latch parameter for refMgrLatchUnfltrMeasRT

    struct REF_fsm              fsm;                                        //!< Finite state machine
    struct REF_ilc              ilc;                                        //!< Iterative Learning Controller variables
    struct REF_zero_values      zero_values;                                //!< Variables initialized to zero.
    struct REF_default_ptrs     ref_defaults[REG_NUM_LOADS][REG_NUM_MODES]; //!< 2D array with default value pointers for [load_sel][reg_mode_sel]
    struct REF_limits           ref_limits  [REG_NUM_MODES];                //!< Ref limits pointers for [reg_mode]
    struct REF_static_vars      static_vars;                                //!< Dummy static structures to support refMgrVar macros
    cc_float                  * ref_direct  [REG_NUM_MODES];                //!< Pointers to the DIRECT reference parameters for each regulation mode

    struct FG_error           * fg_error;                                   //!< Pointer to 2D array of fg_error       [sub_sel][cyc_sel]
    struct REF_cyc_status     * cyc_status;                                 //!< Pointer to 2D array of cyc_status     [sub_sel][cyc_sel]
    struct REF_armed          * ref_armed;                                  //!< Pointer to 2D array of ref_armed      [sub_sel][cyc_sel]
    struct FG_point           * armed_table_function;                       //!< Pointer to 3D array of table_function [sub_sel][cyc_sel][point_idx]

    struct REF_event_mgr        event_mgr;
    struct REF_ramp_mgr         ramp_mgr;                                   //!< Ramp manager variables
    struct REF_pre_func_arm     pre_func_arm;                               //!< Pre-function arming variables for current and field regulation
    struct REF_pre_func_mgr     pre_func_mgr;                               //!< Pre-function manager variables for current and field regulation
    struct REF_cyc_status_mgr   cyc_status_mgr;                             //!< Cycle Status manager variables
    struct REF_vfeedfwd_delay   vfeedfwd_delay;                            //!< Voltage feed forward signal delay

    cc_float                    ref_fg_unlimited;                           //!< Function generation reference
    cc_float                    ref_fg;                                     //!< Function generation reference after possible limitation by regMgrRegulate()
    cc_float                    ref_rt;                                     //!< Real-time reference
    cc_float                    ref_now;                                    //!< ref_fg + ref_rt
    cc_float                    ref_actuation;                              //!< Actuation reference taking into account the polarity switch
    cc_float                    ref_dac;                                    //!< Reference for DAC taking into account the polarity switch

    cc_float                  * test_plep_initial_rate;                     //!< Pointer for testing the initial_rate of a PLEP
    cc_float                  * test_plep_final_rate;                       //!< Pointer for testing the final_rate of a PLEP

    union CC_debug              debug;                                      //!< Debug data union
};

#ifdef __cplusplus
extern "C" {
#endif

//! Transform sub_sel according to MODE REF_SUB_SEL.
//!
//! @param[in]      ref_mgr              Pointer to reference manager structure.
//! @param[in]      sub_sel              Cycle selector.
//! @retval         sub_sel              if MODE REF_SUB_SEL is ENABLED.
//! @retval         0                    if MODE REF_SUB_SEL is DISABLED.

static inline uint32_t refMgrSubSel(struct REF_mgr * const ref_mgr, uint32_t const sub_sel)
{
    return   refMgrParValue(ref_mgr, MODE_REF_SUB_SEL) == CC_ENABLED
           ? sub_sel
           : 0;
}


//! Transform cyc_sel according to MODE REF_CYC_SEL.
//!
//! @param[in]      ref_mgr              Pointer to reference manager structure.
//! @param[in]      cyc_sel              Cycle selector.
//! @retval         cyc_sel              if MODE REF_CYC_SEL is ENABLED.
//! @retval         0                    if MODE REF_CYC_SEL is DISABLED.

static inline uint32_t refMgrCycSel(struct REF_mgr * const ref_mgr, uint32_t const cyc_sel)
{
    return   refMgrParValue(ref_mgr, MODE_REF_CYC_SEL) == CC_ENABLED
           ? cyc_sel
           : 0;
}

#ifdef __cplusplus
}
#endif

// EOF
