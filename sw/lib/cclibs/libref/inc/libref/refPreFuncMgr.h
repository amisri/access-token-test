//! @file  refPreFuncMgr.h
//! @brief Header file for pre-function manager functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Pre-function structures

struct REF_pre_func_meta
{
    struct CC_ns_time               abs_end_time;                                   //!< Absolute time of the end of the function
    cc_float                        final_ref;                                      //!< Final reference of the function
    cc_float                        final_rate;                                     //!< Final rate of change of the function
};

struct REF_pre_func_mgr
{
    int32_t                         seg_index;                                      //!< Segment index (0..(num_segs-1) for pre-func segments)
    bool                            seg_finished;                                   //!< True when segment ramp is finished
    bool                            seq_finished;                                   //!< True when the final segment of the sequence is finished
    uint32_t                        num_samples;                                    //!< Number of samples of meas squared accumulated
    cc_float                        sum_ref_squared;                                //!< Reference squared accumulator
    cc_float                        next_seg_time;                                  //!< time_adv to start the next pre-function segment
    struct REF_pre_func_meta        prev_func_meta;                                 //!< Important meta data from the previous function
    struct REF_pre_func_reg_mode    b;                                              //!< Pre-function variables for field regulation
    struct REF_pre_func_reg_mode    i;                                              //!< Pre-function variables for current regulation
};

#ifdef __cplusplus
extern "C" {
#endif

//! Start a new pre-function sequence.
//!
//! This is called from refStateCyclingRT() when a cycle starts with enough time to run a pre-function.
//! It starts the pre-function of the new cycle, which is already the active cycle.
//!
//! @param[in,out]  ref_mgr          Pointer to REF_mgr structure
//! @retval         true             Prepared to start pre-function
//! @retval         false            No time for pre-function

bool refPreFuncMgrStartRT(struct REF_mgr * ref_mgr);


//! Terminate the pre-function if it hasn't finished.
//!
//! This will terminate the pre-function if it is still active, allowing the main function to take over.
//!
//! @param[in,out]  ref_mgr          Pointer to REF_mgr structure

void refPreFuncMgrEndRT(struct REF_mgr * ref_mgr);


//! Run the pre-function.
//!
//! This function should be called on each regulation iteration by refStateCyclingRT()
//! during the pre-function phase. It will start the pre-function segments at the required times.
//!
//! @param[in] ref_mgr       Pointer to reference manager structure

void refPreFuncMgrRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
