//! @file  refStateRunning.h
//! @brief Converter Control Reference Manager library: RUNNING reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef REF_STATE_RUNNING_H
#define REF_STATE_RUNNING_H

#include "libref.h"

//! RUNNING state structure

struct REF_state_running
{
    char	unused;		    //!< To suppress compiler warnings
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in RUNNING state.
//!
//! This is called on entry into the RUNNING reference state to prepare operation of the refStateRunningRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateRunningRT     Pointer to function for RUNNING state.

RefState * refStateRunningInitRT(struct REF_mgr * const ref_mgr);



//! Operate RUNNING state.
//!
//! This is called every iteration of the reference state machine when the state is RUNNING.
//! This includes the first iteration, following the call to refStateRunningInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_RUNNING     Index of the RUNNING state.

enum REF_state refStateRunningRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

#endif

// EOF
