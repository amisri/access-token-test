//! @file      refConsts.h
//! @brief     Reference management library public constants header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Public Constants

#define LIBREF                                                                  //!< Signal that libref is included

#define REF_VFF_DELAY_BUF_LEN               16                                  //!< Feed forward voltage signal delay buffer length (must be a power of 2)

#define REF_ILC_MAX_L_ORDER                 12                                  //!< Max order of ILC L function
#define REF_ILC_L_FUNC_LEN                  (REF_ILC_MAX_L_ORDER*2+1)           //!< Length in elements for ILC L function

#define REF_ILC_MAX_Q_ORDER                 12                                  //!< Max order of ILC Q function
#define REF_ILC_Q_PAR_LEN                   (REF_ILC_MAX_Q_ORDER+1)             //!< Length in elements for ILC Q parameter - defines half the Q function which is symmetrical
#define REF_ILC_Q_FUNC_LEN                  (REF_ILC_MAX_Q_ORDER*2+1)           //!< Length in elements for the full ILC Q function - must be <= REF_ILC_UNFLTR_REF_LEN

#define REF_ILC_ERR_LEN                     32                                  //!< ILC error circular buffer len - next power of two >= REF_ILC_MAX_L_FUNC_LEN
#define REF_ILC_UNFLTR_REF_LEN              32                                  //!< Unfiltered ILC ref circular buffer len - next power of two >= REF_ILC_MAX_Q_FUNC_LEN

#define REF_START_PRE_FUNC_IMMEDIATELY     -1.0E6F                              //!< Start segment immediately (used for the first segment only)

//! Fault bit indexes

enum REF_fault_bit
{
    REF_B_MEAS_FAULT_BIT     ,
    REF_B_MEAS_TRIP_BIT      ,
    REF_B_ERR_FAULT_BIT      ,
    REF_B_RST_FAULT_BIT      ,
    REF_I_MEAS_FAULT_BIT     ,
    REF_I_MEAS_TRIP_BIT      ,
    REF_I_ERR_FAULT_BIT      ,
    REF_I_RST_FAULT_BIT      ,
    REF_I_RMS_FAULT_BIT      ,
    REF_I_RMS_LOAD_FAULT_BIT ,
    REF_V_MEAS_FAULT_BIT     ,
    REF_V_MEAS_TRIP_BIT      ,
    REF_V_ERR_FAULT_BIT      ,
    REF_V_RATE_RMS_FAULT_BIT ,
    REF_I_CAPA_FAULT_BIT     ,
    REF_NUM_FAULT_BITS
};

//! Fault bit masks

enum REF_fault_bit_mask
{
    REF_B_MEAS_FAULT_BIT_MASK     = (1 << REF_B_MEAS_FAULT_BIT     ) ,
    REF_B_MEAS_TRIP_BIT_MASK      = (1 << REF_B_MEAS_TRIP_BIT      ) ,
    REF_B_ERR_FAULT_BIT_MASK      = (1 << REF_B_ERR_FAULT_BIT      ) ,
    REF_B_RST_FAULT_BIT_MASK      = (1 << REF_B_RST_FAULT_BIT      ) ,
    REF_I_MEAS_FAULT_BIT_MASK     = (1 << REF_I_MEAS_FAULT_BIT     ) ,
    REF_I_MEAS_TRIP_BIT_MASK      = (1 << REF_I_MEAS_TRIP_BIT      ) ,
    REF_I_ERR_FAULT_BIT_MASK      = (1 << REF_I_ERR_FAULT_BIT      ) ,
    REF_I_RST_FAULT_BIT_MASK      = (1 << REF_I_RST_FAULT_BIT      ) ,
    REF_I_RMS_FAULT_BIT_MASK      = (1 << REF_I_RMS_FAULT_BIT      ) ,
    REF_I_RMS_LOAD_FAULT_BIT_MASK = (1 << REF_I_RMS_LOAD_FAULT_BIT ) ,
    REF_V_MEAS_FAULT_BIT_MASK     = (1 << REF_V_MEAS_FAULT_BIT     ) ,
    REF_V_MEAS_TRIP_BIT_MASK      = (1 << REF_V_MEAS_TRIP_BIT      ) ,
    REF_V_ERR_FAULT_BIT_MASK      = (1 << REF_V_ERR_FAULT_BIT      ) ,
    REF_V_RATE_RMS_FAULT_BIT_MASK = (1 << REF_V_RATE_RMS_FAULT_BIT ) ,
    REF_I_CAPA_FAULT_BIT_MASK     = (1 << REF_I_CAPA_FAULT_BIT     )
};

//! Warning bit indexes

enum REF_warning_bit
{
    REF_REF_CLIPPED_BIT         ,
    REF_REF_RATE_CLIPPED_BIT    ,
    REF_B_REF_CLIPPED_BIT       ,
    REF_B_REF_RATE_CLIPPED_BIT  ,
    REF_B_ERR_WARNING_BIT       ,
    REF_B_RST_WARNING_BIT       ,
    REF_I_REF_CLIPPED_BIT       ,
    REF_I_REF_RATE_CLIPPED_BIT  ,
    REF_I_ERR_WARNING_BIT       ,
    REF_I_RST_WARNING_BIT       ,
    REF_V_REF_CLIPPED_BIT       ,
    REF_V_REF_RATE_CLIPPED_BIT  ,
    REF_V_ERR_WARNING_BIT       ,
    REF_DYN_ECO_ARM_WARNING_BIT ,
    REF_DYN_ECO_RUN_WARNING_BIT ,
    REF_CYCLING_WARNING_BIT     ,
    REF_I_RMS_WARNING_BIT       ,
    REF_I_RMS_LOAD_WARNING_BIT  ,
    REF_ILC_WARNING_BIT         ,
    REF_NUM_WARNING_BITS
};

//! Warning bit masks

enum REF_warning_bit_mask
{
    REF_REF_CLIPPED_BIT_MASK         = (1 << REF_REF_CLIPPED_BIT         ) ,
    REF_REF_RATE_CLIPPED_BIT_MASK    = (1 << REF_REF_RATE_CLIPPED_BIT    ) ,
    REF_B_REF_CLIPPED_BIT_MASK       = (1 << REF_B_REF_CLIPPED_BIT       ) ,
    REF_B_REF_RATE_CLIPPED_BIT_MASK  = (1 << REF_B_REF_RATE_CLIPPED_BIT  ) ,
    REF_B_ERR_WARNING_BIT_MASK       = (1 << REF_B_ERR_WARNING_BIT       ) ,
    REF_B_RST_WARNING_BIT_MASK       = (1 << REF_B_RST_WARNING_BIT       ) ,
    REF_I_REF_CLIPPED_BIT_MASK       = (1 << REF_I_REF_CLIPPED_BIT       ) ,
    REF_I_REF_RATE_CLIPPED_BIT_MASK  = (1 << REF_I_REF_RATE_CLIPPED_BIT  ) ,
    REF_I_ERR_WARNING_BIT_MASK       = (1 << REF_I_ERR_WARNING_BIT       ) ,
    REF_I_RST_WARNING_BIT_MASK       = (1 << REF_I_RST_WARNING_BIT       ) ,
    REF_V_REF_CLIPPED_BIT_MASK       = (1 << REF_V_REF_CLIPPED_BIT       ) ,
    REF_V_REF_RATE_CLIPPED_BIT_MASK  = (1 << REF_V_REF_RATE_CLIPPED_BIT  ) ,
    REF_V_ERR_WARNING_BIT_MASK       = (1 << REF_V_ERR_WARNING_BIT       ) ,
    REF_DYN_ECO_ARM_WARNING_BIT_MASK = (1 << REF_DYN_ECO_ARM_WARNING_BIT ) ,
    REF_DYN_ECO_RUN_WARNING_BIT_MASK = (1 << REF_DYN_ECO_RUN_WARNING_BIT ) ,
    REF_CYCLING_WARNING_BIT_MASK     = (1 << REF_CYCLING_WARNING_BIT     ) ,
    REF_I_RMS_WARNING_BIT_MASK       = (1 << REF_I_RMS_WARNING_BIT       ) ,
    REF_I_RMS_LOAD_WARNING_BIT_MASK  = (1 << REF_I_RMS_LOAD_WARNING_BIT  ) ,
    REF_ILC_WARNING_BIT_MASK         = (1 << REF_ILC_WARNING_BIT         )
};


//! Cycling status bit masks

enum REF_cyc_status_bit
{
    REF_CYC_REG_OK_BIT          ,
    REF_CYC_REG_WRN_BIT         ,
    REF_CYC_REG_FLT_BIT         ,
    REF_CYC_ABORTED_BIT         ,
    REF_CYC_WAS_OVERRUN_BIT     ,
    REF_CYC_NO_PRE_FUNC_BIT     ,
    REF_CYC_PRE_FUNC_WRN_BIT    ,
    REF_CYC_DYN_ECO_ARM_WRN_BIT ,
    REF_CYC_DYN_ECO_RUN_WRN_BIT ,
    REF_CYC_INCOMPLETE_WRN_BIT  ,
    REF_CYC_ECO_DYN_BIT         ,
    REF_CYC_ECO_ONCE_BIT        ,
    REF_CYC_NUM_STATUSES
};

enum REF_cyc_status_bit_mask
{
    REF_CYC_REG_OK_BIT_MASK          = (1 << REF_CYC_REG_OK_BIT         ) ,
    REF_CYC_REG_WRN_BIT_MASK         = (1 << REF_CYC_REG_WRN_BIT        ) ,
    REF_CYC_REG_FLT_BIT_MASK         = (1 << REF_CYC_REG_FLT_BIT        ) ,
    REF_CYC_ABORTED_BIT_MASK         = (1 << REF_CYC_ABORTED_BIT        ) ,
    REF_CYC_WAS_OVERRUN_BIT_MASK     = (1 << REF_CYC_WAS_OVERRUN_BIT    ) ,
    REF_CYC_NO_PRE_FUNC_BIT_MASK     = (1 << REF_CYC_NO_PRE_FUNC_BIT    ) ,
    REF_CYC_PRE_FUNC_WRN_BIT_MASK    = (1 << REF_CYC_PRE_FUNC_WRN_BIT   ) ,
    REF_CYC_DYN_ECO_ARM_WRN_BIT_MASK = (1 << REF_CYC_DYN_ECO_ARM_WRN_BIT) ,
    REF_CYC_DYN_ECO_RUN_WRN_BIT_MASK = (1 << REF_CYC_DYN_ECO_RUN_WRN_BIT) ,
    REF_CYC_INCOMPLETE_WRN_BIT_MASK  = (1 << REF_CYC_INCOMPLETE_WRN_BIT ) ,
    REF_CYC_ECO_DYN_BIT_MASK         = (1 << REF_CYC_ECO_DYN_BIT        ) ,
    REF_CYC_ECO_ONCE_BIT_MASK        = (1 << REF_CYC_ECO_ONCE_BIT       ) ,
};

//! Power converter state enums

enum REF_pc_state
{
    REF_PC_OFF   ,
    REF_PC_ON    ,
    REF_PC_FAULT ,
    REF_PC_NUM_STATES
};

//! Pre-function mode enums - the order is important to the pre_func_arm.prepare_seq array in refPreFuncArm.c

enum REF_pre_func_mode
{
    REF_PRE_FUNC_RAMP            ,
    REF_PRE_FUNC_MINRMS          ,
    REF_PRE_FUNC_OPENLOOP_MINRMS ,
    REF_PRE_FUNC_MAGCYCLE        ,
    REF_PRE_FUNC_UPMINMAX        ,
    REF_PRE_FUNC_DOWNMAXMIN      ,
    REF_PRE_FUNC_NUM_MODES
};

//! Post-function mode enums

enum REF_post_func_mode
{
    REF_POST_FUNC_MINRMS          ,
    REF_POST_FUNC_OPENLOOP_MINRMS ,
    REF_POST_FUNC_HOLD            ,
    REF_POST_FUNC_NUM_MODES
};

//! Reference state enums

enum REF_state
{
    REF_OFF           ,
    REF_POL_SWITCHING ,
    REF_TO_OFF        ,
    REF_TO_STANDBY    ,
    REF_TO_CYCLING    ,
    REF_TO_IDLE       ,
    REF_DIRECT        ,
    REF_STANDBY       ,
    REF_CYCLING       ,
    REF_DYN_ECO       ,
    REF_FULL_ECO      ,
    REF_PAUSED        ,
    REF_IDLE          ,
    REF_ARMED         ,
    REF_RUNNING       ,
    REF_NUM_STATES
};

//! Reference state bit masks

enum REF_state_bit_mask
{
    REF_OFF_MASK           = (1 << REF_OFF          ) ,
    REF_POL_SWITCHING_MASK = (1 << REF_POL_SWITCHING) ,
    REF_TO_OFF_MASK        = (1 << REF_TO_OFF       ) ,
    REF_TO_STANDBY_MASK    = (1 << REF_TO_STANDBY   ) ,
    REF_TO_CYCLING_MASK    = (1 << REF_TO_CYCLING   ) ,
    REF_TO_IDLE_MASK       = (1 << REF_TO_IDLE      ) ,
    REF_DIRECT_MASK        = (1 << REF_DIRECT       ) ,
    REF_STANDBY_MASK       = (1 << REF_STANDBY      ) ,
    REF_CYCLING_MASK       = (1 << REF_CYCLING      ) ,
    REF_DYN_ECO_MASK       = (1 << REF_DYN_ECO      ) ,
    REF_FULL_ECO_MASK      = (1 << REF_FULL_ECO     ) ,
    REF_PAUSED_MASK        = (1 << REF_PAUSED       ) ,
    REF_IDLE_MASK          = (1 << REF_IDLE         ) ,
    REF_ARMED_MASK         = (1 << REF_ARMED        ) ,
    REF_RUNNING_MASK       = (1 << REF_RUNNING      )
};

//! ILC state enums

enum REF_ilc_state
{
    REF_ILC_DISABLED   ,
    REF_ILC_STANDBY    ,
    REF_ILC_TO_CYCLING ,
    REF_ILC_CYCLING    ,
    REF_ILC_FAILED     ,
    REF_ILC_NUM_STATES
};

//! ILC background status enums

enum REF_ilc_status
{
    REF_ILC_CYC_STATES_BIT            ,
    REF_ILC_RMS_ERR_FAILED_BIT        ,
    REF_ILC_RMS_V_REF_RATE_FAILED_BIT ,
    REF_ILC_L_FUNC_LEN_EVEN_BIT       ,
    REF_ILC_REG_PERIOD_INVALID_BIT    ,
    REF_ILC_NUM_STATUS
};

//! ILC background status bit masks

enum REF_ilc_status_bit_mask
{
    REF_ILC_CYC_STATES_BIT_MASK            = (1 << REF_ILC_CYC_STATES_BIT            ) ,
    REF_ILC_RMS_ERR_FAILED_BIT_MASK        = (1 << REF_ILC_RMS_ERR_FAILED_BIT        ) ,
    REF_ILC_RMS_V_REF_RATE_FAILED_BIT_MASK = (1 << REF_ILC_RMS_V_REF_RATE_FAILED_BIT ) ,
    REF_ILC_L_FUNC_LEN_EVEN_BIT_MASK       = (1 << REF_ILC_L_FUNC_LEN_EVEN_BIT       ) ,
    REF_ILC_REG_PERIOD_INVALID_BIT_MASK    = (1 << REF_ILC_REG_PERIOD_INVALID_BIT    ) ,
};

// EOF
