//! @file  refRampMgr.h
//! @brief Generation of an arbitrary number of ramps using the ramp function from libfg
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REF_START_RAMP_NOW          0.0             //!< Use as ramp start time to start ramp immediately - this will set the event time
#define REF_DONT_USE_ACC_FOR_DEC    false           //!< Flag to use the default deceleration as the ramp deceleration
#define REF_USE_ACC_FOR_DEC         true            //!< Flag to use the default acceleration as the ramp deceleration

//! Ramp manager structure

struct REF_ramp_mgr
{
    enum FG_status  status;                         //!< Ramp manager status: FG_DURING, FG_POST_FUNC
    cc_float        start_time;                     //!< Ramp start time: REF_START_RAMP_NOW or positive will set event time. Time is adjusted for ref advance.
    cc_float        initial_rate;                   //!< Initial ramp rate of change
    cc_float        initial_ref;                    //!< Initial ramp reference
    cc_float        final_ref;                      //!< Final ramp reference (might require polarity switch change)
    cc_float        ramp_ref;                       //!< Current ramp reference
    bool            new_ramp_ref;                   //!< True if ramp reference has changed
    bool            use_acc_for_dec;                //!< True if the default acceleration should also be used as the deceleration
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! @brief Initializes ramp manager with a new set of arguments
//!
//! This function must be called before refRampMgrRT on the same iteration. It initializes internal variables of
//! the ramp manager which then can be used by the real-time function to calculate a new ramp.
//!
//! @param[in]  ref_mgr          Pointer to REF_mgr structure
//! @param[in]  start_time       Time of start of the first ramp with respect to REF_mgr.func_time
//! @param[in]  initial_rate     Initial rate of change of the reference
//! @param[in]  initial_ref      Initial reference
//! @param[in]  final_ref        Final reference to be reached
//! @param[in]  use_acc_for_dec  Use default acceleration for the deceleration when set to true

void refRampMgrInitRT(struct REF_mgr         * ref_mgr,
                      cc_float                 start_time,
                      cc_float                 initial_rate,
                      cc_float                 initial_ref,
                      cc_float                 final_ref,
                      bool                     use_acc_for_dec);


//! @brief Real-time function of ramp manager
//!
//! This function should be called on each regulation iteration. It's responsible for checking the actual
//! reference value, function generation status and polarity switch state. Based on that it may trigger ramp
//! recalculation and change of polarity switch. In order to work, a number of arguments has to be passed to it
//! by the initialization function.
//!
//! @param[in] ref_mgr       Pointer to reference manager structure
//!
//! @retval FG_DURING_FUNC   Indicates that ramp manager is still running, the final reference has not been reached yet
//! @retval FG_POST_FUNC     Indicates that the final reference has been reached

enum FG_status refRampMgrRT(struct REF_mgr * const ref_mgr);



void refRampMgrTerminateRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
