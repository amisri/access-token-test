//! @file  refRestore.h
//! @brief Converter Control Reference Manager library parameter restoring functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

#ifdef __cplusplus
extern "C" {
#endif

//! Restore parameters for an armed function.
//!
//! This function will restore the parameters for the armed function for the specified sub_sel and cyc_sel,
//! if a function is armed. It keeps track of which parameters were restored in the fg_pars_idx array.
//! If the user parameter matches the armed parameter, then there is nothing to do for that parameter.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     sub_sel           Sub-device selector.
//! @param[in]     cyc_sel           Cycle selector.
//! @param[in,out] fg_pars_idx       Optional pointer to array of REF_FG_PAR_MAX_PER_GROUP elements to receive zero-terminated

void refRestore(struct REF_mgr      * ref_mgr,
                uint32_t              sub_sel,
                uint32_t              cyc_sel,
                enum REF_fg_par_idx * fg_pars_idx);

#ifdef __cplusplus
}
#endif

// EOF
