//! @file  refStateOff.h
//! @brief Converter Control Reference Manager library: OFF reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef REF_STATE_OFF_H
#define REF_STATE_OFF_H

#include "libref.h"

//! Constants

#define REF_OFF_TIMER_RESET     0                               //!< Reset value for start_delay and init_rst timers
#define REF_OFF_TIMER_READY     1                               //!< Terminal value for start_delay and init_rst down-counter
#define REF_PC_START_DELAY      2.0                             //!< Delay in seconds after PC STATE = ON before initialization of RST

//! OFF state structure

struct REF_state_off
{
    uint32_t                start_delay_timer;                  //!< Timer down-counter to all the system to settle after power-up before initialization of RST
    uint32_t                init_rst_timer;                     //!< Timer down-counter to initialize the I/B RST history before leaving OFF state
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in OFF state.
//!
//! This is called on entry into the OFF reference state to prepare operation of the refStateOffRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateOffRT     Pointer to function for OFF state.

RefState * refStateOffInitRT(struct REF_mgr * const ref_mgr);



//! Operate OFF state.
//!
//! This is called every iteration of the reference state machine when the state is OFF.
//! This includes the first iteration, following the call to refStateOffInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_OFF     Index of the OFF state.

enum REF_state refStateOffRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

#endif

// EOF
