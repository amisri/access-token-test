//! @file  inc/libref/refFgType.h
//!
//! @brief Converter Control Reference Manager library : Function Generator Parameter default fg_type header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Static array of default FG_TYPE to arm if a given FG Parameter is set

static enum FG_type const transaction_fg_type[] = 
{
    FG_NONE   ,      //  0  TRANSACTION_LAST_FG_PAR_INDEX   Non-transactional: FG parameter index that was last set during a transaction
    FG_NONE   ,      //  1  CTRL_PLAY                       Non-transactional: Control if function should be played. When DISABLED a STANDBY function will be played instead.
    FG_NONE   ,      //  2  CTRL_DYN_ECO_END_TIME           Non-transactional: Dynamic economy function end time. This is time when the function generator returns to playing the armed function.
    FG_NONE   ,      //  3  REF_FG_TYPE                     Function type.
    FG_RAMP   ,      //  4  RAMP_INITIAL_REF                Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    FG_RAMP   ,      //  5  RAMP_FINAL_REF                  Final reference for a RAMP.
    FG_RAMP   ,      //  6  RAMP_ACCELERATION               RAMP acceleration.
    FG_RAMP   ,      //  7  RAMP_LINEAR_RATE                Maximum linear rate during the RAMP.
    FG_RAMP   ,      //  8  RAMP_DECELERATION               RAMP deceleration.
    FG_PULSE  ,      //  9  PULSE_REF                       PULSE reference.
    FG_PULSE  ,      // 10  PULSE_DURATION                  PULSE duration.
    FG_PLEP   ,      // 11  PLEP_INITIAL_REF                Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    FG_PLEP   ,      // 12  PLEP_FINAL_REF                  Final reference for a PLEP.
    FG_PLEP   ,      // 13  PLEP_ACCELERATION               PLEP acceleration.
    FG_PLEP   ,      // 14  PLEP_LINEAR_RATE                Maximum linear rate during the PLEP.
    FG_PLEP   ,      // 15  PLEP_EXP_TC                     PLEP exponential segment time constant.
    FG_PLEP   ,      // 16  PLEP_EXP_FINAL                  PLEP exponential segment asymptote (zero for automatic).
    FG_PPPL   ,      // 17  PPPL_INITIAL_REF                Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    FG_PPPL   ,      // 18  PPPL_ACCELERATION1              Array of initial PPPL accelerations.
    FG_PPPL   ,      // 19  PPPL_ACCELERATION2              Array of second PPPL accelerations.
    FG_PPPL   ,      // 20  PPPL_ACCELERATION3              Array of third PPPL accelerations.
    FG_PPPL   ,      // 21  PPPL_RATE2                      Array of rates at the start of the second PPPL accelerations.
    FG_PPPL   ,      // 22  PPPL_RATE4                      Array of rates at the start of the linear PPPL segments.
    FG_PPPL   ,      // 23  PPPL_REF4                       Array of references at the start of the linear PPPL segments.
    FG_PPPL   ,      // 24  PPPL_DURATION4                  Array of durations of the fourth linear linear PPPL segments.
    FG_NONE   ,      // 25  PPPL_ACCELERATION1_NUM_ELS      Number of elements in the PPPL ACCELERATION1 array.
    FG_NONE   ,      // 26  PPPL_ACCELERATION2_NUM_ELS      Number of elements in the PPPL ACCELERATION2 array.
    FG_NONE   ,      // 27  PPPL_ACCELERATION3_NUM_ELS      Number of elements in the PPPL ACCELERATION3 array.
    FG_NONE   ,      // 28  PPPL_RATE2_NUM_ELS              Number of elements in the PPPL RATE2 array.
    FG_NONE   ,      // 29  PPPL_RATE4_NUM_ELS              Number of elements in the PPPL RATE4 array.
    FG_NONE   ,      // 30  PPPL_REF4_NUM_ELS               Number of elements in the PPPL REF4 array.
    FG_NONE   ,      // 31  PPPL_DURATION4_NUM_ELS          Number of elements in the PPPL DURATION4 array.
    FG_CUBEXP ,      // 32  CUBEXP_REF                      Array of start/end references for CUBEXP.
    FG_CUBEXP ,      // 33  CUBEXP_RATE                     Array of start/end rate of change for CUBEXP.
    FG_CUBEXP ,      // 34  CUBEXP_TIME                     Array of start/end times for CUBEXP.
    FG_NONE   ,      // 35  CUBEXP_REF_NUM_ELS              Number of elements in the CUEXP REF array.
    FG_NONE   ,      // 36  CUBEXP_RATE_NUM_ELS             Number of elements in the CUEXP RATE array.
    FG_NONE   ,      // 37  CUBEXP_TIME_NUM_ELS             Number of elements in the CUEXP TIME array.
    FG_TABLE  ,      // 38  TABLE_FUNCTION                  TABLE function points array.
    FG_NONE   ,      // 39  TABLE_FUNCTION_NUM_ELS          Number of points in the TABLE function.
    FG_CTRIM  ,      // 40  TRIM_INITIAL_REF                Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    FG_CTRIM  ,      // 41  TRIM_FINAL_REF                  Final reference for a TRIM.
    FG_CTRIM  ,      // 42  TRIM_DURATION                   Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    FG_SINE   ,      // 43  TEST_INITIAL_REF                Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    FG_SINE   ,      // 44  TEST_AMPLITUDE_PP               TEST function peak-peak amplitude.
    FG_SINE   ,      // 45  TEST_NUM_PERIODS                Number of TEST function periods to play.
    FG_SINE   ,      // 46  TEST_PERIOD_ITERS               Sampling period in function generator iterations for TEST function.
    FG_SINE   ,      // 47  TEST_PERIOD                     TEST function period.
    FG_SINE   ,      // 48  TEST_WINDOW                     Control of TEST function half-period sine window.
    FG_SINE   ,      // 49  TEST_EXP_DECAY                  Control of TEST function exponential amplitude delay.
    FG_PRBS   ,      // 50  PRBS_INITIAL_REF                Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    FG_PRBS   ,      // 51  PRBS_AMPLITUDE_PP               PRBS peak-peak amplitude.
    FG_PRBS   ,      // 52  PRBS_PERIOD_ITERS               PRBS period in function generator iterations.
    FG_PRBS   ,      // 53  PRBS_NUM_SEQUENCES              Number of PRBS sequences to play.
    FG_PRBS   ,      // 54  PRBS_K                          PRBS K factor. Sequence length is 2^K - 1 periods.
};

// EOF
