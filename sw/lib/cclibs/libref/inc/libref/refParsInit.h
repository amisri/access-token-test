//! @file  inc/libref/refParsInit.h
//!
//! @brief Converter Control Reference Manager library : Parameter initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

static void refParsInit(struct REF_mgr *ref_mgr)
{
    // Initialise the size_of_element for each function generation parameter

    ref_mgr->u.fg_pars.transaction_last_fg_par_index.meta.size_of_element = sizeof(enum REF_fg_par_idx);
    ref_mgr->u.fg_pars.ctrl_play.meta.size_of_element                     = sizeof(enum CC_enabled_disabled);
    ref_mgr->u.fg_pars.ctrl_dyn_eco_end_time.meta.size_of_element         = sizeof(cc_float);
    ref_mgr->u.fg_pars.ref_fg_type.meta.size_of_element                   = sizeof(enum FG_type);
    ref_mgr->u.fg_pars.ramp_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.ramp_final_ref.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.ramp_acceleration.meta.size_of_element             = sizeof(cc_float);
    ref_mgr->u.fg_pars.ramp_linear_rate.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.ramp_deceleration.meta.size_of_element             = sizeof(cc_float);
    ref_mgr->u.fg_pars.pulse_ref.meta.size_of_element                     = sizeof(cc_float);
    ref_mgr->u.fg_pars.pulse_duration.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_final_ref.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_acceleration.meta.size_of_element             = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_linear_rate.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_exp_tc.meta.size_of_element                   = sizeof(cc_float);
    ref_mgr->u.fg_pars.plep_exp_final.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_acceleration1.meta.size_of_element            = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_acceleration2.meta.size_of_element            = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_acceleration3.meta.size_of_element            = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_rate2.meta.size_of_element                    = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_rate4.meta.size_of_element                    = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_ref4.meta.size_of_element                     = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_duration4.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.pppl_acceleration1_num_els.meta.size_of_element    = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_acceleration2_num_els.meta.size_of_element    = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_acceleration3_num_els.meta.size_of_element    = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_rate2_num_els.meta.size_of_element            = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_rate4_num_els.meta.size_of_element            = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_ref4_num_els.meta.size_of_element             = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.pppl_duration4_num_els.meta.size_of_element        = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.cubexp_ref.meta.size_of_element                    = sizeof(cc_float);
    ref_mgr->u.fg_pars.cubexp_rate.meta.size_of_element                   = sizeof(cc_float);
    ref_mgr->u.fg_pars.cubexp_time.meta.size_of_element                   = sizeof(cc_float);
    ref_mgr->u.fg_pars.cubexp_ref_num_els.meta.size_of_element            = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.cubexp_rate_num_els.meta.size_of_element           = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.cubexp_time_num_els.meta.size_of_element           = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.table_function.meta.size_of_element                = sizeof(struct FG_point);
    ref_mgr->u.fg_pars.table_function_num_els.meta.size_of_element        = sizeof(uintptr_t);
    ref_mgr->u.fg_pars.trim_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.trim_final_ref.meta.size_of_element                = sizeof(cc_float);
    ref_mgr->u.fg_pars.trim_duration.meta.size_of_element                 = sizeof(cc_float);
    ref_mgr->u.fg_pars.test_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.test_amplitude_pp.meta.size_of_element             = sizeof(cc_float);
    ref_mgr->u.fg_pars.test_num_periods.meta.size_of_element              = sizeof(uint32_t);
    ref_mgr->u.fg_pars.test_period_iters.meta.size_of_element             = sizeof(uint32_t);
    ref_mgr->u.fg_pars.test_period.meta.size_of_element                   = sizeof(cc_float);
    ref_mgr->u.fg_pars.test_window.meta.size_of_element                   = sizeof(enum CC_enabled_disabled);
    ref_mgr->u.fg_pars.test_exp_decay.meta.size_of_element                = sizeof(enum CC_enabled_disabled);
    ref_mgr->u.fg_pars.prbs_initial_ref.meta.size_of_element              = sizeof(cc_float);
    ref_mgr->u.fg_pars.prbs_amplitude_pp.meta.size_of_element             = sizeof(cc_float);
    ref_mgr->u.fg_pars.prbs_period_iters.meta.size_of_element             = sizeof(uint32_t);
    ref_mgr->u.fg_pars.prbs_num_sequences.meta.size_of_element            = sizeof(uint32_t);
    ref_mgr->u.fg_pars.prbs_k.meta.size_of_element                        = sizeof(uint32_t);
}

// EOF
