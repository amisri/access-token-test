//! @file  refRt.h
//! @brief Converter Control Reference Manager library reference real-time functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"


#ifdef __cplusplus
extern "C" {
#endif

//! This function sets the ref_mgr latched and unlatched faults.
//!
//! @param[in,out] ref_mgr          Pointer to reference manager structure.
//! @param[in]     latch_faults     True to latch faults, false to clear faults latch

void refRtRegFaultsRT(struct REF_mgr * ref_mgr, bool latch_faults);


//! This function controls the use of the real time reference.
//!
//! The RT_REF can be used as a delta_ref in IDLE/ARMED/RUNNING or CYCLING states,
//! or directly as the reference in DIRECT state. To be used in either case:
//!   * MODE RT_REF must be ENABLED,
//!   * the supplied ref_state must equal MODE PC_ON (i.e. the default ref state)
//!   * the actual reg_mode must equal MODE REG_MODE_CYC (i.e. the default reg mode).
//! If the use of the rt_ref is activated or deactivated, then the function adjusts
//! ref_fg and ref_fg_unlimited when possible, to avoid a discontinuity in the reference.
//!
//! @param[out] ref_mgr             Pointer to reference manager structure.
//! @param[in]  ref_state           Reference state (OFF to disable use of real time reference).

void refRtControlRealTimeRefRT(struct REF_mgr * ref_mgr, enum REF_state ref_state);


//! This function performs the reference generation and regulation (on regulation iterations).
//!
//! This function should be called by the converter control application every iteration. It should
//! be called after the measurements have been acquired.
//!
//! @param[in,out] ref_mgr          Pointer to reference manager structure.
//! @param[in]     iter_time        Unix time of this iteration withi nanosecond resolution.

void refRtRegulationRT(struct REF_mgr * ref_mgr, struct CC_ns_time iter_time);


//! This function runs the state machine, ILC and simulation.
//!
//! This function should be called by the converter control application every iteration. It should
//! be called after refRtRegulationRT() has run.
//!
//! @param[in,out] ref_mgr          Pointer to reference manager structure.

void refRtStateRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
