//! @file  refStateCycling.h
//! @brief Converter Control Reference Manager library: CYCLING reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! CYCLING state structure

struct REF_state_cycling
{
    enum FG_status          status;                     //!< Cycling function generator status
    uint32_t                counter;                    //!< Counter that is reset to 1 when new cycle starts and to 0 when not in CYCLING
    bool                    cycle_overrun;              //!< Flag: Cycle was overrun by the next cycle
    bool                    warning;                    //!< Flag: Cycling warning combines pre-function and function warnings
    cc_float                final_rate;                 //!< Final rate for the active function
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in CYCLING state.
//!
//! This is called on entry into the CYCLING reference state to prepare operation of the refStateCyclingRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateCyclingRT     Pointer to function for CYCLING state.

RefState * refStateCyclingInitRT(struct REF_mgr * ref_mgr);


//! Operate CYCLING state.
//!
//! This is called every iteration of the reference state machine when the state is CYCLING.
//! This includes the first iteration, following the call to refStateCyclingInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_CYCLING     Index of the CYCLING state.

enum REF_state refStateCyclingRT(struct REF_mgr * ref_mgr);


//! Check if it is time to start the main function
//!
//! @param[in]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval false      Time is before the start time of the main function
//! @retval true       Time is equal or later than the start time of the main function

bool refStateCyclingCheckStartMainFunctionRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
