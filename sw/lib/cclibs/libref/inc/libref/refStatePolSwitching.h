//! @file  refStatePolSwitching.h
//! @brief Converter Control Reference Manager library: POL_SWITCHING reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef REF_STATE_POL_SWITCHING_H
#define REF_STATE_POL_SWITCHING_H

#include "libref.h"

//! POL_SWITCHING state structure

struct REF_state_pol_switching
{
    bool abandon_cycle;    //!< Flag that forces transition to TO_CYCLING
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in POL_SWITCHING state.
//!
//! This is called on entry into the POL_SWITCHING reference state to prepare operation of the refStatePolSwitchingRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStatePolSwitchingRT     Pointer to function for POL_SWITCHING state.

RefState * refStatePolSwitchingInitRT(struct REF_mgr * const ref_mgr);



//! Operate POL_SWITCHING state.
//!
//! This is called every iteration of the reference state machine when the state is POL_SWITCHING.
//! This includes the first iteration, following the call to refStatePolSwitchingInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_POL_SWITCHING     Index of the POL_SWITCHING state.

enum REF_state refStatePolSwitchingRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

#endif

// EOF
