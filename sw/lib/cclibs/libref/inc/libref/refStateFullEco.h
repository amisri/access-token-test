//! @file  refStateFullEco.h
//! @brief Converter Control Reference Manager library: FULL_ECO reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! FULL_ECO state structure

struct REF_state_full_eco
{
    bool        run;                //!< Set to true to enter full economy state until ECONOMY FULL is DISABLED
    bool        once;               //!< Set to true to enter full economy state until this flag is false
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Test if entry to FULL_ECO is requested.
//!
//! @param[in]  ref_mgr    Pointer to reference manager structure.
//!
//! @retval     true       Entry to FULL_ECO is requested
//! @retval     false      Entry to FULL_ECO is not requested

bool refStateFullEcoRunRT(struct REF_mgr const * ref_mgr);


//! Initialize operation in FULL_ECO state.
//!
//! This is called on entry into the FULL_ECO reference state to prepare operation of the refStateFullEcoRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateFullEcoRT     Pointer to function for FULL_ECO state.

RefState * refStateFullEcoInitRT(struct REF_mgr * ref_mgr);


//! Operate FULL_ECO state.
//!
//! This is called every iteration of the reference state machine when the state is FULL_ECO.
//! This includes the first iteration, following the call to refStateFullEcoInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_FULL_ECO     Index of the FULL_ECO state.

enum REF_state refStateFullEcoRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
