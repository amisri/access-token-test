//! @file  inc/libref/refParsLinkStructs.h
//!
//! @brief Converter Control Reference Manager library : Parameter initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Static inline functions to link function generation parameter structures

static inline void refMgrInitTransactionPars(struct REF_mgr * const ref_mgr,
                                             struct REF_transaction_pars * const transaction,
                                             uint32_t const sub_sel_step,
                                             uint32_t const cyc_sel_step)
{
    // Link to TRANSACTION function generation parameters

    refMgrFgParInitPointer   (ref_mgr, transaction_last_fg_par_index, transaction->last_fg_par_index);
    refMgrFgParInitSubSelStep(ref_mgr, transaction_last_fg_par_index, sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, transaction_last_fg_par_index, cyc_sel_step);
}


static inline void refMgrInitCtrlPars(struct REF_mgr * const ref_mgr,
                                      struct REF_ctrl_pars * const ctrl,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to CTRL function generation parameters

    refMgrFgParInitPointer   (ref_mgr, ctrl_play                    , ctrl->play);
    refMgrFgParInitSubSelStep(ref_mgr, ctrl_play                    , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ctrl_play                    , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, ctrl_dyn_eco_end_time        , ctrl->dyn_eco_end_time);
    refMgrFgParInitSubSelStep(ref_mgr, ctrl_dyn_eco_end_time        , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ctrl_dyn_eco_end_time        , cyc_sel_step);
}


static inline void refMgrInitRefPars(struct REF_mgr * const ref_mgr,
                                     struct REF_ref_pars * const ref,
                                     uint32_t const sub_sel_step,
                                     uint32_t const cyc_sel_step)
{
    // Link to REF function generation parameters

    refMgrFgParInitPointer   (ref_mgr, ref_fg_type                  , ref->fg_type);
    refMgrFgParInitSubSelStep(ref_mgr, ref_fg_type                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ref_fg_type                  , cyc_sel_step);
}


static inline void refMgrInitRampPars(struct REF_mgr * const ref_mgr,
                                      struct REF_ramp_pars * const ramp,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to RAMP function generation parameters

    refMgrFgParInitPointer   (ref_mgr, ramp_initial_ref             , ramp->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, ramp_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ramp_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, ramp_final_ref               , ramp->final_ref);
    refMgrFgParInitSubSelStep(ref_mgr, ramp_final_ref               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ramp_final_ref               , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, ramp_acceleration            , ramp->acceleration);
    refMgrFgParInitSubSelStep(ref_mgr, ramp_acceleration            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ramp_acceleration            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, ramp_linear_rate             , ramp->linear_rate);
    refMgrFgParInitSubSelStep(ref_mgr, ramp_linear_rate             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ramp_linear_rate             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, ramp_deceleration            , ramp->deceleration);
    refMgrFgParInitSubSelStep(ref_mgr, ramp_deceleration            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, ramp_deceleration            , cyc_sel_step);
}


static inline void refMgrInitPulsePars(struct REF_mgr * const ref_mgr,
                                       struct REF_pulse_pars * const pulse,
                                       uint32_t const sub_sel_step,
                                       uint32_t const cyc_sel_step)
{
    // Link to PULSE function generation parameters

    refMgrFgParInitPointer   (ref_mgr, pulse_ref                    , pulse->ref);
    refMgrFgParInitSubSelStep(ref_mgr, pulse_ref                    , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pulse_ref                    , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pulse_duration               , pulse->duration);
    refMgrFgParInitSubSelStep(ref_mgr, pulse_duration               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pulse_duration               , cyc_sel_step);
}


static inline void refMgrInitPlepPars(struct REF_mgr * const ref_mgr,
                                      struct REF_plep_pars * const plep,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to PLEP function generation parameters

    refMgrFgParInitPointer   (ref_mgr, plep_initial_ref             , plep->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, plep_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, plep_final_ref               , plep->final_ref);
    refMgrFgParInitSubSelStep(ref_mgr, plep_final_ref               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_final_ref               , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, plep_acceleration            , plep->acceleration);
    refMgrFgParInitSubSelStep(ref_mgr, plep_acceleration            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_acceleration            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, plep_linear_rate             , plep->linear_rate);
    refMgrFgParInitSubSelStep(ref_mgr, plep_linear_rate             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_linear_rate             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, plep_exp_tc                  , plep->exp_tc);
    refMgrFgParInitSubSelStep(ref_mgr, plep_exp_tc                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_exp_tc                  , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, plep_exp_final               , plep->exp_final);
    refMgrFgParInitSubSelStep(ref_mgr, plep_exp_final               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, plep_exp_final               , cyc_sel_step);
}


static inline void refMgrInitPpplPars(struct REF_mgr * const ref_mgr,
                                      struct REF_pppl_pars * const pppl,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to PPPL function generation parameters

    refMgrFgParInitPointer   (ref_mgr, pppl_initial_ref             , pppl->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration1           , pppl->acceleration1);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration1           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration1           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration2           , pppl->acceleration2);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration2           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration2           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration3           , pppl->acceleration3);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration3           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration3           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_rate2                   , pppl->rate2);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_rate2                   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_rate2                   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_rate4                   , pppl->rate4);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_rate4                   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_rate4                   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_ref4                    , pppl->ref4);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_ref4                    , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_ref4                    , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_duration4               , pppl->duration4);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_duration4               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_duration4               , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration1_num_els   , pppl->acceleration1_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration1_num_els   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration1_num_els   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration2_num_els   , pppl->acceleration2_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration2_num_els   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration2_num_els   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_acceleration3_num_els   , pppl->acceleration3_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_acceleration3_num_els   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_acceleration3_num_els   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_rate2_num_els           , pppl->rate2_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_rate2_num_els           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_rate2_num_els           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_rate4_num_els           , pppl->rate4_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_rate4_num_els           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_rate4_num_els           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_ref4_num_els            , pppl->ref4_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_ref4_num_els            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_ref4_num_els            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, pppl_duration4_num_els       , pppl->duration4_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, pppl_duration4_num_els       , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, pppl_duration4_num_els       , cyc_sel_step);
}


static inline void refMgrInitCubexpPars(struct REF_mgr * const ref_mgr,
                                        struct REF_cubexp_pars * const cubexp,
                                        uint32_t const sub_sel_step,
                                        uint32_t const cyc_sel_step)
{
    // Link to CUBEXP function generation parameters

    refMgrFgParInitPointer   (ref_mgr, cubexp_ref                   , cubexp->ref);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_ref                   , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_ref                   , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, cubexp_rate                  , cubexp->rate);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_rate                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_rate                  , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, cubexp_time                  , cubexp->time);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_time                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_time                  , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, cubexp_ref_num_els           , cubexp->ref_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_ref_num_els           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_ref_num_els           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, cubexp_rate_num_els          , cubexp->rate_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_rate_num_els          , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_rate_num_els          , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, cubexp_time_num_els          , cubexp->time_num_els);
    refMgrFgParInitSubSelStep(ref_mgr, cubexp_time_num_els          , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, cubexp_time_num_els          , cyc_sel_step);
}


static inline void refMgrInitTrimPars(struct REF_mgr * const ref_mgr,
                                      struct REF_trim_pars * const trim,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to TRIM function generation parameters

    refMgrFgParInitPointer   (ref_mgr, trim_initial_ref             , trim->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, trim_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, trim_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, trim_final_ref               , trim->final_ref);
    refMgrFgParInitSubSelStep(ref_mgr, trim_final_ref               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, trim_final_ref               , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, trim_duration                , trim->duration);
    refMgrFgParInitSubSelStep(ref_mgr, trim_duration                , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, trim_duration                , cyc_sel_step);
}


static inline void refMgrInitTestPars(struct REF_mgr * const ref_mgr,
                                      struct REF_test_pars * const test,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to TEST function generation parameters

    refMgrFgParInitPointer   (ref_mgr, test_initial_ref             , test->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, test_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_amplitude_pp            , test->amplitude_pp);
    refMgrFgParInitSubSelStep(ref_mgr, test_amplitude_pp            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_amplitude_pp            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_num_periods             , test->num_periods);
    refMgrFgParInitSubSelStep(ref_mgr, test_num_periods             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_num_periods             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_period_iters            , test->period_iters);
    refMgrFgParInitSubSelStep(ref_mgr, test_period_iters            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_period_iters            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_period                  , test->period);
    refMgrFgParInitSubSelStep(ref_mgr, test_period                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_period                  , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_window                  , test->window);
    refMgrFgParInitSubSelStep(ref_mgr, test_window                  , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_window                  , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, test_exp_decay               , test->exp_decay);
    refMgrFgParInitSubSelStep(ref_mgr, test_exp_decay               , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, test_exp_decay               , cyc_sel_step);
}


static inline void refMgrInitPrbsPars(struct REF_mgr * const ref_mgr,
                                      struct REF_prbs_pars * const prbs,
                                      uint32_t const sub_sel_step,
                                      uint32_t const cyc_sel_step)
{
    // Link to PRBS function generation parameters

    refMgrFgParInitPointer   (ref_mgr, prbs_initial_ref             , prbs->initial_ref);
    refMgrFgParInitSubSelStep(ref_mgr, prbs_initial_ref             , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, prbs_initial_ref             , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, prbs_amplitude_pp            , prbs->amplitude_pp);
    refMgrFgParInitSubSelStep(ref_mgr, prbs_amplitude_pp            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, prbs_amplitude_pp            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, prbs_period_iters            , prbs->period_iters);
    refMgrFgParInitSubSelStep(ref_mgr, prbs_period_iters            , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, prbs_period_iters            , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, prbs_num_sequences           , prbs->num_sequences);
    refMgrFgParInitSubSelStep(ref_mgr, prbs_num_sequences           , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, prbs_num_sequences           , cyc_sel_step);

    refMgrFgParInitPointer   (ref_mgr, prbs_k                       , prbs->k);
    refMgrFgParInitSubSelStep(ref_mgr, prbs_k                       , sub_sel_step);
    refMgrFgParInitCycSelStep(ref_mgr, prbs_k                       , cyc_sel_step);
}

// EOF
