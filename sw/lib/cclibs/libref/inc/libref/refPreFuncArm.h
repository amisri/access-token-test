//! @file  refPreFuncArm.h
//! @brief Header file for pre-function arming functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REF_PRE_FUNC_MAX_NUM_SEGS           9                                       //!< Maximum segments in a pre-function sequence
#define REF_MINMAX_PLATEAU_INDEX            0                                       //!< Index in DEFAULTS PLATEAU_DURATION parameter for the MIN and MAX plateau duration
#define REF_FINAL_PLATEAU_INDEX             1                                       //!< Index in DEFAULTS PLATEAU_DURATION parameter for the final plateau duration

//! Pre-function segment types - the order is important to the final_ref_index array in refPreFuncArm.c

enum REF_pre_func_seg_type
{
    // Segments from a default reference to another default reference

    REF_PRE_FUNC_NEGMINRMS_TO_MIN       ,
    REF_PRE_FUNC_POSMINRMS_TO_MIN       ,
    REF_PRE_FUNC_MAX_TO_MIN             ,
    REF_PRE_FUNC_MIN_TO_NEGMINRMS       ,
    REF_PRE_FUNC_POSMINRMS_TO_NEGMINRMS ,
    REF_PRE_FUNC_MAX_TO_NEGMINRMS       ,
    REF_PRE_FUNC_MIN_TO_POSMINRMS       ,
    REF_PRE_FUNC_NEGMINRMS_TO_POSMINRMS ,
    REF_PRE_FUNC_MAX_TO_POSMINRMS       ,
    REF_PRE_FUNC_MIN_TO_MAX             ,
    REF_PRE_FUNC_NEGMINRMS_TO_MAX       ,
    REF_PRE_FUNC_POSMINRMS_TO_MAX       ,
    REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES  ,

    // Segments to/from a user reference - the order is important

    REF_PRE_FUNC_REF1_TO_REF1 = REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES ,
    REF_PRE_FUNC_REF1_TO_MIN            ,
    REF_PRE_FUNC_REF1_TO_NEGMINRMS      ,
    REF_PRE_FUNC_REF1_TO_POSMINRMS      ,
    REF_PRE_FUNC_REF1_TO_MAX            ,
    REF_PRE_FUNC_XXX_TO_REF2            ,
    REF_PRE_FUNC_NUM_SEG_TYPES          ,
};

//! Default values indexes

enum REF_pre_func_defaults
{
    REF_PRE_FUNC_DEFAULT_ACCELERATION ,
    REF_PRE_FUNC_DEFAULT_DECELERATION ,
    REF_PRE_FUNC_DEFAULT_LINEAR_RATE  ,
    REF_PRE_FUNC_DEFAULT_POS_MIN_RMS  ,
    REF_PRE_FUNC_DEFAULT_NEG_MIN_RMS  ,
    REF_PRE_FUNC_DEFAULT_PRE_FUNC_MAX ,
    REF_PRE_FUNC_DEFAULT_PRE_FUNC_MIN ,
    REF_PRE_FUNC_NUM_DEFAULTS         ,
};

//! Pre-function structures

struct REF_pre_func_seg                                                             //!< Pre-function segment structure
{
    enum REF_pre_func_seg_type      type;                                           //!< Segment type
    cc_float                        start_time;                                     //!< Segment start time relative to the main function event
    cc_float                        final_ref;                                      //!< Final reference for the segment
};

struct REF_pre_func_seq                                                             //!< Pre-function sequence structure
{
    uint32_t                        num_segs;                                       //!< Pre-function length in segments
    enum REF_pre_func_mode          mode;                                           //!< Pre-function mode
    cc_float                        minmax_plateau_duration;                        //!< Pre-function min & max plateau duration
    cc_float                        final_plateau_duration;                         //!< Pre-function final plateau duration (before the function begins)
    cc_float                        dec_delta_ref_factor;                           //!< 1/(2.deceleration) - used to calculate delta_ref for switch to RAMP
    cc_float                        rms;                                            //!< Estimated RMS for the whole sequence
    struct REF_pre_func_seg         seg[REF_PRE_FUNC_MAX_NUM_SEGS];                 //!< Pre-function segments
};

struct REF_pre_func_dur_rms                                                         //!< Structure for calculated and measured duration or RMS
{
    cc_float                        calculated[REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES]; //!< Calculated values (duration or RMS)
    cc_float                        measured  [REF_PRE_FUNC_NUM_DEFAULT_SEG_TYPES]; //!< Measured values   (duration or RMS)
};

struct REF_pre_func_reg_mode                                                        //!< Pre-function data per regulation mode (field or current)
{
    cc_float                        defaults[REF_PRE_FUNC_NUM_DEFAULTS];            //!< Copy of default values for the reg_mode
    struct REF_pre_func_dur_rms     duration;                                       //!< Calculated and measured durations for default segment types
    struct REF_pre_func_dur_rms     rms;                                            //!< Calculated and measured RMSs for default segment types
};

struct REF_pre_func_arm                                                             //!< Structure for pre-function arming variables
{
    cc_float                        reg_period;                                     //!< Regulation period for the pre-function being armed
    cc_float                        initial_ref;                                    //!< Initial reference of the first segment
    cc_float                        initial_rate;                                   //!< Initial rate of change of the reference of the first segment
    cc_float                        final_ref;                                      //!< Final reference of the last segment
    cc_float                        duration_available;                             //!< Time available for the pre-function
    bool                            bipolar_refs;                                   //!< True if initial and final pre-function references have opposite signs
    bool                            bipolar_minmax;                                 //!< True if min and max defaults have opposite signs
    bool                            auto_pol_switch;                                //!< True if there is an automatic polarity switch
    bool                            use_pol_switch;                                 //!< True if bipolar min/max and there is an automatic polarity switch
    enum REG_mode                   reg_mode;                                       //!< Regulation mode for the sequence
    struct REF_pre_func_reg_mode  * pre_func_reg_mode;                              //!< Pre-function reg_mode structure
};

//! Structure declaration so that it's available for debugging and testing in ccrt


#ifdef __cplusplus
extern "C" {
#endif

//! Save important function meta data for the next call to refPreFuncArm
//!
//! This function is called after calling refPreFuncArm() to save important meta data for the next
//! function to play. This may be useful when this function becomes active and the following call to refPreFuncArm()
//! needs to know this meta data.
//!
//! @param[in]  ref_mgr          Pointer to REF_mgr structure
//! @param[in]  abs_end_time     Absolute time of the end of the function
//! @param[in]  final_ref        Final reference
//! @param[in]  final_rate       Final rate

void refPreFuncSaveMeta(struct REF_mgr  * ref_mgr,
                        struct CC_ns_time abs_end_time,
                        cc_float          final_ref,
                        cc_float          final_rate);


//! Arms the pre-function of the next cycle
//!
//! This function will prepare the pre-function sequence to connect the actual reference and rate of change to
//! the initial reference at the start of the function at the event time. It will use the pre-function mode for
//! function to arm.
//!
//! @param[in]  ref_mgr               Pointer to REF_mgr structure
//! @param[in]  next_event            Pointer to next event structure to hold the pre-function
//! @param[in]  func_to_arm           Pointer to function to arm
//! @param[in]  duration_available    Estimate duration available for the pre-function
//! @param[in]  prev_func_meta_valid  True if pre_func_mgr.prev_func_meta is valid

void refPreFuncArm(struct REF_mgr   * ref_mgr,
                   struct REF_event * next_event,
                   struct REF_func  * func_to_arm,
                   cc_float           duration_available,
                   bool               prev_func_meta_valid);

#ifdef __cplusplus
}
#endif

// EOF
