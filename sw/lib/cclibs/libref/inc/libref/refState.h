//! @file  refState.h
//! @brief Converter Control Reference Manager library reference state machine functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! States in which test RST coefficients can be used

#define REF_STATES_RST_SOURCE_ENABLED ( REF_CYCLING_MASK    | REF_DYN_ECO_MASK | REF_PAUSED_MASK )

//! States in which cycling is active

#define REF_STATES_CYCLING            ( REF_CYCLING_MASK    | REF_PAUSED_MASK  | REF_POL_SWITCHING_MASK | REF_DYN_ECO_MASK | REF_FULL_ECO_MASK )

//! States in which dynamic economy warnings can be reported

#define REF_STATES_DYN_ECO            ( REF_TO_CYCLING_MASK | REF_CYCLING_MASK | REF_DYN_ECO_MASK | REF_PAUSED_MASK )

//! States where the regulation error is calculated and checked

#define REF_STATES_REG_ERR_ENABLED    ( REF_DIRECT_MASK     | REF_IDLE_MASK    | REF_ARMED_MASK   | REF_RUNNING_MASK )

//! State and transition function typedefs

typedef enum REF_state RefState     (struct REF_mgr       * const ref_mgr);     //!< Type for reference state machine state function
typedef RefState     * RefStateInit (struct REF_mgr       * const ref_mgr);     //!< Type for reference state function state initialization function
typedef RefStateInit * RefTransition(struct REF_mgr const * const ref_mgr);     //!< Type for reference state machine transition condition function

//! Include state function headers files

#include "refStateOff.h"
#include "refStatePolSwitching.h"
#include "refStateToOff.h"
#include "refStateToStandby.h"
#include "refStateToCycling.h"
#include "refStateToIdle.h"
#include "refStateDirect.h"
#include "refStateStandby.h"
#include "refStateCycling.h"
#include "refStateDynEco.h"
#include "refStateFullEco.h"
#include "refStatePaused.h"
#include "refStateIdle.h"
#include "refStateArmed.h"
#include "refStateRunning.h"


//! Reference Finite State Machine (FSM) structure with variables needed by the ref_state machine

struct REF_fsm
{
    union  FG_pars                  fg_pars       ;         //!< FG parameters for automatic reference changes

    struct REF_state_off            off           ;         //!< OFF           state variables
    struct REF_state_pol_switching  pol_switching ;         //!< POL_SWITCHING state variables
    struct REF_state_to_off         to_off        ;         //!< TO_OFF        state variables
    struct REF_state_to_standby     to_standby    ;         //!< TO_STANDBY    state variables
    struct REF_state_to_cycling     to_cycling    ;         //!< TO_CYCLING    state variables
    struct REF_state_to_idle        to_idle       ;         //!< TO_IDLE       state variables
    struct REF_state_direct         direct        ;         //!< DIRECT        state variables
    struct REF_state_standby        standby       ;         //!< STANDBY       state variables
    struct REF_state_cycling        cycling       ;         //!< CYCLING       state variables
    struct REF_state_dyn_eco        dyn_eco       ;         //!< DYN_ECO       state variables
    struct REF_state_full_eco       full_eco      ;         //!< FULL_ECO      state variables
    struct REF_state_paused         paused        ;         //!< PAUSED        state variables
    struct REF_state_idle           idle          ;         //!< IDLE          state variables
    struct REF_state_armed          armed         ;         //!< ARMED         state variables
    struct REF_state_running        running       ;         //!< RUNNING       state variables
};

#ifdef __cplusplus
extern "C" {
#endif

//! Execute reference state machine.
//!
//! This function drives the reference state machine. Note: It is called at the regulation period.
//! The finite state machine is based on a Petri net model, but with limitations. Transitions
//! only have one output arc and the net only has one token, so the state of the system can be
//! represented by a single enumerated variable. See https://en.wikipedia.org/wiki/Petri_net.
//!
//! @param[in,out] ref_mgr              Pointer to reference manager structure.

void refStateRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
