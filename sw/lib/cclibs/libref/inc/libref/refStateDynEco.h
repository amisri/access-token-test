//! @file  refStateDynEco.h
//! @brief Converter Control Reference Manager library: DYN_ECO reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! DYN_ECO state structure

struct REF_state_dyn_eco
{
    struct CC_ns_time plep_abs_time;                      //!< Absolute time of start of the dynamic dyn_eco PLEP
    struct CC_ns_time event_abs_time;                     //!< Saved event abs_time_s so that it can be restored when running the PLEP
    bool              arm_warning;                        //!< Dynamic economy PLEP arming failed warning flag
    bool              run_warning;                        //!< Dynamic economy PLEP running failed warning flag
    bool              plep_running;                       //!< Dynamic economy PELP is running flag
    bool              plep_complete;                      //!< Dynamic economy PLEP is complete flag
    bool              ramp_to_final_running;              //!< Dynamic economy ramp to final reference is running flag
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Reset DynEco states
//!
//! This is called when a cycle starts to reset the dyn_eco run warning flag after two cycles
//! i.e., the run warning is extended to be sure that it is visible. It also clears the
//! dyn_eco plep complete flag as it is part of the CYtoDE transition condition.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.

void refStateDynEcoResetRT(struct REF_mgr * const ref_mgr);


//! Initialize operation in DYN_ECO state.
//!
//! This is called on entry into the DYN_ECO reference state to prepare operation of the refStateDynEcoRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateDynEcoRT     Pointer to function for DYN_ECO state.

RefState * refStateDynEcoInitRT(struct REF_mgr * const ref_mgr);


//! Operate DYN_ECO state.
//!
//! This is called every iteration of the reference state machine when the state is DYN_ECO.
//! This includes the first iteration, following the call to refStateDynEcoInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_DYN_ECO     Index of the DYN_ECO state.

enum REF_state refStateDynEcoRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
