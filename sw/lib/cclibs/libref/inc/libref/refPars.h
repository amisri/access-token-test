//! @file  inc/libref/refPars.h
//!
//! @brief Converter Control Reference Manager library : Parameter header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libref/refFgParIdx.h"

// Non-cyclic parameter

#define refMgrParValue(REF_MGR,PAR_NAME)                (REF_MGR)->REF_PAR_ ## PAR_NAME[0]
#define refMgrParValueByIndex(REF_MGR,PAR_NAME,INDEX)   (REF_MGR)->REF_PAR_ ## PAR_NAME[INDEX]
#define refMgrParPointer(REF_MGR,PAR_NAME)            (&(REF_MGR)->REF_PAR_ ## PAR_NAME[0])

#define REF_PAR_MODE_REF                  pars.mode_ref                  // Requested reference state
#define REF_PAR_MODE_REF_CYC_SEL          pars.mode_ref_cyc_sel          // Enabled: Use different settings for each cyc_sel. Disabled: Use slot 0 for all cyc_sels.
#define REF_PAR_MODE_REF_SUB_SEL          pars.mode_ref_sub_sel          // Enabled: Use different settings for each sub_sel. Disabled: Use sub_sel 0 for all sub_sels.
#define REF_PAR_MODE_TEST_CYC_SEL         pars.mode_test_cyc_sel         // cyc_sel to use the test regulator
#define REF_PAR_MODE_TEST_REF_CYC_SEL     pars.mode_test_ref_cyc_sel     // cyc_sel to use as the reference cyc_sel when cyc_sel equals TEST_CYC_SEL
#define REF_PAR_MODE_TEST_REG_MODE        pars.mode_test_reg_mode        // Regulation mode for arming reference functions for the Test User Cycles
#define REF_PAR_MODE_EVENT_OFFSET_US      pars.mode_event_offset_us      // Event time offset for CYCLING
#define REF_PAR_MODE_REG_MODE             pars.mode_reg_mode             // Regulation mode for non-CYCLING states (DIRECT IDLE etc...)
#define REF_PAR_MODE_REG_MODE_CYC         pars.mode_reg_mode_cyc         // Regulation mode for arming reference functions for CYCLING
#define REF_PAR_MODE_PRE_FUNC             pars.mode_pre_func             // Pre-function policy
#define REF_PAR_MODE_POST_FUNC            pars.mode_post_func            // Post-function policy
#define REF_PAR_MODE_SIM_MEAS             pars.mode_sim_meas             // Enabled: Use voltage source and load simulation to simulate measurements in libreg. Disabled: Use real signals.
#define REF_PAR_MODE_RT_REF               pars.mode_rt_ref               // Enabled: Use real-time ref in compatible states and reg_modes. Disabled: Ignore real-time ref.
#define REF_PAR_MODE_ECONOMY              pars.mode_economy              // Enabled: Use ECONOMY flags to enter ECONOMY state from CYCLING. Disabled: Ignore ECONOMY flags.
#define REF_PAR_MODE_USE_ARM_NOW          pars.mode_use_arm_now          // Enabled: Use newly armed reference immediately when possible. Disabled:Use newly armed reference on next cycle.
#define REF_PAR_MODE_TO_OFF_FAULTS        pars.mode_to_off_faults        // Bit mask of faults that should trigger transition to TO_OFF state rather than OFF.
#define REF_PAR_MODE_ILC                  pars.mode_ilc                  // Enabled: Use ILC when in CYCLING. Disabled: Do not use ILC.
#define REF_PAR_MODE_HARMONICS            pars.mode_harmonics            // Enabled: Activate harmonics generation if V_AC is present. Disabled: Do not use harmonics generation.
#define REF_PAR_REF_RUN                   pars.ref_run                   // Run time
#define REF_PAR_ILC_START_TIME            pars.ilc_start_time            // ILC start time in seconds (fg_time_adv).
#define REF_PAR_ILC_STOP_REF              pars.ilc_stop_ref              // Reference threshold below which the ILC calculation will stop.
#define REF_PAR_ILC_L_ORDER               pars.ilc_l_order               // Zero:Use L_FUNC array. Not zero:Defines order of unit L-function.
#define REF_PAR_ILC_L_FUNC                pars.ilc_l_func                // L-function array.
#define REF_PAR_ILC_L_FUNC_NUM_ELS        pars.ilc_l_func_num_els        // Pointer to the number of elements in the L_FUNC array.
#define REF_PAR_ILC_Q_FUNC                pars.ilc_q_func                // FIR Q filter function.
#define REF_PAR_ILC_Q_FUNC_NUM_ELS        pars.ilc_q_func_num_els        // Pointer to number of elements in the Q_FUNC array.
#define REF_PAR_ILC_LOG_CYC_SEL           pars.ilc_log_cyc_sel           // Cycle selector for the cycles to log in the ILC_ERR log.
#define REF_PAR_ECONOMY_DYNAMIC           pars.economy_dynamic           // Enabled: Trigger immediate transition to ECONOMY state if in CYCLING and MODE_ECONOMY is ENABLED. Reset automatically.
#define REF_PAR_ECONOMY_FULL              pars.economy_full              // Enabled: Trigger transition to ECONOMY state if in CYCLING and MODE_ECONOMY is ENABLED and function has finished. Disabled: Trigger transition back to CYCLING from ECONOMY.
#define REF_PAR_DEFAULT_V_ACCELERATION    pars.default_v_acceleration    // Default accelerations for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_V_DECELERATION    pars.default_v_deceleration    // Default decelerations for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_V_LINEAR_RATE     pars.default_v_linear_rate     // Default linear rates for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_V_PRE_FUNC_MAX    pars.default_v_pre_func_max    // Pre-function maximum voltages for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_V_PRE_FUNC_MIN    pars.default_v_pre_func_min    // Pre-function minimum voltages for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_V_MINRMS          pars.default_v_minrms          // Pre-function minimum RMS voltages for voltage regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_ACCELERATION    pars.default_i_acceleration    // Default accelerations for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_DECELERATION    pars.default_i_deceleration    // Default decelerations for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_LINEAR_RATE     pars.default_i_linear_rate     // Default linear rates for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_PRE_FUNC_MAX    pars.default_i_pre_func_max    // Pre-function maximum current for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_PRE_FUNC_MIN    pars.default_i_pre_func_min    // Pre-function minimum current for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_I_MINRMS          pars.default_i_minrms          // Pre-function minimum RMS current for current regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_ACCELERATION    pars.default_b_acceleration    // Default accelerations for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_DECELERATION    pars.default_b_deceleration    // Default decelerations for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_LINEAR_RATE     pars.default_b_linear_rate     // Default linear rates for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_PRE_FUNC_MAX    pars.default_b_pre_func_max    // Pre-function maximum field for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_PRE_FUNC_MIN    pars.default_b_pre_func_min    // Pre-function minimum field for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_B_MINRMS          pars.default_b_minrms          // Pre-function minimum RMS field for field regulation mode for each libreg load select
#define REF_PAR_DEFAULT_PLATEAU_DURATION  pars.default_plateau_duration  // Pre-function plateau durations: [1] final plateau before function starts [0] all other plateaus
#define REF_PAR_DIRECT_B_REF              pars.direct_b_ref              // Field reference for DIRECT state
#define REF_PAR_DIRECT_I_REF              pars.direct_i_ref              // Current reference for DIRECT state
#define REF_PAR_DIRECT_V_REF              pars.direct_v_ref              // Voltage reference for DIRECT state
#define REF_PAR_DIRECT_DEGAUSS_AMP_PP     pars.direct_degauss_amp_pp     // Degauss peak-peak amplitude in DIRECT state
#define REF_PAR_DIRECT_DEGAUSS_PERIOD     pars.direct_degauss_period     // Degauss period in DIRECT state
#define REF_PAR_RT_REF                    pars.rt_ref                    // Real-time reference to use in compatible states and regulation modes if MODE_RT_REF is ENABLED
#define REF_PAR_RT_NUM_STEPS              pars.rt_num_steps              // Number of steps for interpolation of real-time ref
#define REF_PAR_PC_STATE                  pars.pc_state                  // Power converter state (ON or not ON)
#define REF_PAR_DAC_CLAMP                 pars.dac_clamp                 // DAC voltage when PC_STATE is not ON
#define REF_PAR_VFEEDFWD_GAIN             pars.vfeedfwd_gain             // Voltage feedforward gain
#define REF_PAR_VFEEDFWD_DELAY_PERIODS    pars.vfeedfwd_delay_periods    // Voltage feedforward delay in feedforward sample periods

struct REF_pars
{
    enum REF_state            mode_ref                 [1];
    enum CC_enabled_disabled  mode_ref_cyc_sel         [1];
    enum CC_enabled_disabled  mode_ref_sub_sel         [1];
    uint32_t                  mode_test_cyc_sel        [1];
    uint32_t                  mode_test_ref_cyc_sel    [1];
    enum REG_mode             mode_test_reg_mode       [1];
    int32_t                   mode_event_offset_us     [1];
    enum REG_mode             mode_reg_mode            [1];
    enum REG_mode             mode_reg_mode_cyc        [1];
    enum REF_pre_func_mode    mode_pre_func            [1];
    enum REF_post_func_mode   mode_post_func           [1];
    enum CC_enabled_disabled  mode_sim_meas            [1];
    enum CC_enabled_disabled  mode_rt_ref              [1];
    enum CC_enabled_disabled  mode_economy             [1];
    enum CC_enabled_disabled  mode_use_arm_now         [1];
    uint32_t                  mode_to_off_faults       [1];
    enum CC_enabled_disabled  mode_ilc                 [1];
    enum CC_enabled_disabled  mode_harmonics           [1];
    struct CC_us_time         ref_run                  [1];
    cc_float                  ilc_start_time           [1];
    cc_float                  ilc_stop_ref             [1];
    uint32_t                  ilc_l_order              [1];
    cc_float                  ilc_l_func               [REF_ILC_L_FUNC_LEN];
    uintptr_t *               ilc_l_func_num_els       [1];
    cc_float                  ilc_q_func               [REF_ILC_Q_FUNC_LEN];
    uintptr_t *               ilc_q_func_num_els       [1];
    uint32_t                  ilc_log_cyc_sel          [1];
    enum CC_enabled_disabled  economy_dynamic          [1];
    enum CC_enabled_disabled  economy_full             [1];
    cc_float                  default_v_acceleration   [REG_NUM_LOADS];
    cc_float                  default_v_deceleration   [REG_NUM_LOADS];
    cc_float                  default_v_linear_rate    [REG_NUM_LOADS];
    cc_float                  default_v_pre_func_max   [REG_NUM_LOADS];
    cc_float                  default_v_pre_func_min   [REG_NUM_LOADS];
    cc_float                  default_v_minrms         [REG_NUM_LOADS];
    cc_float                  default_i_acceleration   [REG_NUM_LOADS];
    cc_float                  default_i_deceleration   [REG_NUM_LOADS];
    cc_float                  default_i_linear_rate    [REG_NUM_LOADS];
    cc_float                  default_i_pre_func_max   [REG_NUM_LOADS];
    cc_float                  default_i_pre_func_min   [REG_NUM_LOADS];
    cc_float                  default_i_minrms         [REG_NUM_LOADS];
    cc_float                  default_b_acceleration   [REG_NUM_LOADS];
    cc_float                  default_b_deceleration   [REG_NUM_LOADS];
    cc_float                  default_b_linear_rate    [REG_NUM_LOADS];
    cc_float                  default_b_pre_func_max   [REG_NUM_LOADS];
    cc_float                  default_b_pre_func_min   [REG_NUM_LOADS];
    cc_float                  default_b_minrms         [REG_NUM_LOADS];
    cc_float                  default_plateau_duration [2];
    cc_float                  direct_b_ref             [1];
    cc_float                  direct_i_ref             [1];
    cc_float                  direct_v_ref             [1];
    cc_float                  direct_degauss_amp_pp    [1];
    cc_float                  direct_degauss_period    [1];
    cc_float                  rt_ref                   [1];
    uint32_t                  rt_num_steps             [1];
    enum REF_pc_state         pc_state                 [1];
    cc_float                  dac_clamp                [1];
    cc_float                  vfeedfwd_gain            [1];
    cc_float                  vfeedfwd_delay_periods   [1];
};

// Function generator parameter

#define REF_FG_PAR_NOT_USED            NULL

#define refMgrFgParInitPointer(REF_MGR,FG_PAR_NAME,VALUE_P)               (REF_MGR)->u.fg_pars.FG_PAR_NAME.value=VALUE_P
#define refMgrFgParInitCycSelStep(REF_MGR,FG_PAR_NAME,CYC_SEL_STEP)       (REF_MGR)->u.fg_pars.FG_PAR_NAME.meta.cyc_sel_step=CYC_SEL_STEP
#define refMgrFgParInitSubSelStep(REF_MGR,FG_PAR_NAME,SUB_SEL_STEP)       (REF_MGR)->u.fg_pars.FG_PAR_NAME.meta.sub_sel_step=SUB_SEL_STEP

#define refMgrFgParValue(REF_MGR,FG_PAR_NAME)                             (REF_MGR)->u.fg_pars.REF_FG_PAR_ ## FG_PAR_NAME ## _STRUCT.value[0]
#define refMgrFgParValueSubCyc(REF_MGR,SUB_SEL,CYC_SEL,FG_PAR_NAME)      *(REF_FG_PAR_ ## FG_PAR_NAME ## _TYPE *)(refMgrFgParPointerSubCycFunc(REF_MGR, SUB_SEL, CYC_SEL, REF_FG_PAR_ ## FG_PAR_NAME))
#define refMgrFgParPointerSubCyc(REF_MGR,SUB_SEL,CYC_SEL,FG_PAR_NAME)     (REF_FG_PAR_ ## FG_PAR_NAME ## _TYPE *)(refMgrFgParPointerSubCycFunc(REF_MGR, SUB_SEL, CYC_SEL, REF_FG_PAR_ ## FG_PAR_NAME))

// Function Generator Parameter types

#define REF_FG_PAR_TRANSACTION_LAST_FG_PAR_INDEX_TYPE      enum REF_fg_par_idx
#define REF_FG_PAR_CTRL_PLAY_TYPE                          enum CC_enabled_disabled
#define REF_FG_PAR_CTRL_DYN_ECO_END_TIME_TYPE              cc_float
#define REF_FG_PAR_REF_FG_TYPE_TYPE                        enum FG_type
#define REF_FG_PAR_RAMP_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_RAMP_FINAL_REF_TYPE                     cc_float
#define REF_FG_PAR_RAMP_ACCELERATION_TYPE                  cc_float
#define REF_FG_PAR_RAMP_LINEAR_RATE_TYPE                   cc_float
#define REF_FG_PAR_RAMP_DECELERATION_TYPE                  cc_float
#define REF_FG_PAR_PULSE_REF_TYPE                          cc_float
#define REF_FG_PAR_PULSE_DURATION_TYPE                     cc_float
#define REF_FG_PAR_PLEP_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_PLEP_FINAL_REF_TYPE                     cc_float
#define REF_FG_PAR_PLEP_ACCELERATION_TYPE                  cc_float
#define REF_FG_PAR_PLEP_LINEAR_RATE_TYPE                   cc_float
#define REF_FG_PAR_PLEP_EXP_TC_TYPE                        cc_float
#define REF_FG_PAR_PLEP_EXP_FINAL_TYPE                     cc_float
#define REF_FG_PAR_PPPL_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_PPPL_ACCELERATION1_TYPE                 cc_float
#define REF_FG_PAR_PPPL_ACCELERATION2_TYPE                 cc_float
#define REF_FG_PAR_PPPL_ACCELERATION3_TYPE                 cc_float
#define REF_FG_PAR_PPPL_RATE2_TYPE                         cc_float
#define REF_FG_PAR_PPPL_RATE4_TYPE                         cc_float
#define REF_FG_PAR_PPPL_REF4_TYPE                          cc_float
#define REF_FG_PAR_PPPL_DURATION4_TYPE                     cc_float
#define REF_FG_PAR_PPPL_ACCELERATION1_NUM_ELS_TYPE         uintptr_t
#define REF_FG_PAR_PPPL_ACCELERATION2_NUM_ELS_TYPE         uintptr_t
#define REF_FG_PAR_PPPL_ACCELERATION3_NUM_ELS_TYPE         uintptr_t
#define REF_FG_PAR_PPPL_RATE2_NUM_ELS_TYPE                 uintptr_t
#define REF_FG_PAR_PPPL_RATE4_NUM_ELS_TYPE                 uintptr_t
#define REF_FG_PAR_PPPL_REF4_NUM_ELS_TYPE                  uintptr_t
#define REF_FG_PAR_PPPL_DURATION4_NUM_ELS_TYPE             uintptr_t
#define REF_FG_PAR_CUBEXP_REF_TYPE                         cc_float
#define REF_FG_PAR_CUBEXP_RATE_TYPE                        cc_float
#define REF_FG_PAR_CUBEXP_TIME_TYPE                        cc_float
#define REF_FG_PAR_CUBEXP_REF_NUM_ELS_TYPE                 uintptr_t
#define REF_FG_PAR_CUBEXP_RATE_NUM_ELS_TYPE                uintptr_t
#define REF_FG_PAR_CUBEXP_TIME_NUM_ELS_TYPE                uintptr_t
#define REF_FG_PAR_TABLE_FUNCTION_TYPE                     struct FG_point
#define REF_FG_PAR_TABLE_FUNCTION_NUM_ELS_TYPE             uintptr_t
#define REF_FG_PAR_TRIM_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_TRIM_FINAL_REF_TYPE                     cc_float
#define REF_FG_PAR_TRIM_DURATION_TYPE                      cc_float
#define REF_FG_PAR_TEST_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_TEST_AMPLITUDE_PP_TYPE                  cc_float
#define REF_FG_PAR_TEST_NUM_PERIODS_TYPE                   uint32_t
#define REF_FG_PAR_TEST_PERIOD_ITERS_TYPE                  uint32_t
#define REF_FG_PAR_TEST_PERIOD_TYPE                        cc_float
#define REF_FG_PAR_TEST_WINDOW_TYPE                        enum CC_enabled_disabled
#define REF_FG_PAR_TEST_EXP_DECAY_TYPE                     enum CC_enabled_disabled
#define REF_FG_PAR_PRBS_INITIAL_REF_TYPE                   cc_float
#define REF_FG_PAR_PRBS_AMPLITUDE_PP_TYPE                  cc_float
#define REF_FG_PAR_PRBS_PERIOD_ITERS_TYPE                  uint32_t
#define REF_FG_PAR_PRBS_NUM_SEQUENCES_TYPE                 uint32_t
#define REF_FG_PAR_PRBS_K_TYPE                             uint32_t

// Function generator Parameter variables

#define REF_FG_PAR_TRANSACTION_LAST_FG_PAR_INDEX_STRUCT    transaction_last_fg_par_index
#define REF_FG_PAR_CTRL_PLAY_STRUCT                        ctrl_play
#define REF_FG_PAR_CTRL_DYN_ECO_END_TIME_STRUCT            ctrl_dyn_eco_end_time
#define REF_FG_PAR_REF_FG_TYPE_STRUCT                      ref_fg_type
#define REF_FG_PAR_RAMP_INITIAL_REF_STRUCT                 ramp_initial_ref
#define REF_FG_PAR_RAMP_FINAL_REF_STRUCT                   ramp_final_ref
#define REF_FG_PAR_RAMP_ACCELERATION_STRUCT                ramp_acceleration
#define REF_FG_PAR_RAMP_LINEAR_RATE_STRUCT                 ramp_linear_rate
#define REF_FG_PAR_RAMP_DECELERATION_STRUCT                ramp_deceleration
#define REF_FG_PAR_PULSE_REF_STRUCT                        pulse_ref
#define REF_FG_PAR_PULSE_DURATION_STRUCT                   pulse_duration
#define REF_FG_PAR_PLEP_INITIAL_REF_STRUCT                 plep_initial_ref
#define REF_FG_PAR_PLEP_FINAL_REF_STRUCT                   plep_final_ref
#define REF_FG_PAR_PLEP_ACCELERATION_STRUCT                plep_acceleration
#define REF_FG_PAR_PLEP_LINEAR_RATE_STRUCT                 plep_linear_rate
#define REF_FG_PAR_PLEP_EXP_TC_STRUCT                      plep_exp_tc
#define REF_FG_PAR_PLEP_EXP_FINAL_STRUCT                   plep_exp_final
#define REF_FG_PAR_PPPL_INITIAL_REF_STRUCT                 pppl_initial_ref
#define REF_FG_PAR_PPPL_ACCELERATION1_STRUCT               pppl_acceleration1
#define REF_FG_PAR_PPPL_ACCELERATION2_STRUCT               pppl_acceleration2
#define REF_FG_PAR_PPPL_ACCELERATION3_STRUCT               pppl_acceleration3
#define REF_FG_PAR_PPPL_RATE2_STRUCT                       pppl_rate2
#define REF_FG_PAR_PPPL_RATE4_STRUCT                       pppl_rate4
#define REF_FG_PAR_PPPL_REF4_STRUCT                        pppl_ref4
#define REF_FG_PAR_PPPL_DURATION4_STRUCT                   pppl_duration4
#define REF_FG_PAR_PPPL_ACCELERATION1_NUM_ELS_STRUCT       pppl_acceleration1_num_els
#define REF_FG_PAR_PPPL_ACCELERATION2_NUM_ELS_STRUCT       pppl_acceleration2_num_els
#define REF_FG_PAR_PPPL_ACCELERATION3_NUM_ELS_STRUCT       pppl_acceleration3_num_els
#define REF_FG_PAR_PPPL_RATE2_NUM_ELS_STRUCT               pppl_rate2_num_els
#define REF_FG_PAR_PPPL_RATE4_NUM_ELS_STRUCT               pppl_rate4_num_els
#define REF_FG_PAR_PPPL_REF4_NUM_ELS_STRUCT                pppl_ref4_num_els
#define REF_FG_PAR_PPPL_DURATION4_NUM_ELS_STRUCT           pppl_duration4_num_els
#define REF_FG_PAR_CUBEXP_REF_STRUCT                       cubexp_ref
#define REF_FG_PAR_CUBEXP_RATE_STRUCT                      cubexp_rate
#define REF_FG_PAR_CUBEXP_TIME_STRUCT                      cubexp_time
#define REF_FG_PAR_CUBEXP_REF_NUM_ELS_STRUCT               cubexp_ref_num_els
#define REF_FG_PAR_CUBEXP_RATE_NUM_ELS_STRUCT              cubexp_rate_num_els
#define REF_FG_PAR_CUBEXP_TIME_NUM_ELS_STRUCT              cubexp_time_num_els
#define REF_FG_PAR_TABLE_FUNCTION_STRUCT                   table_function
#define REF_FG_PAR_TABLE_FUNCTION_NUM_ELS_STRUCT           table_function_num_els
#define REF_FG_PAR_TRIM_INITIAL_REF_STRUCT                 trim_initial_ref
#define REF_FG_PAR_TRIM_FINAL_REF_STRUCT                   trim_final_ref
#define REF_FG_PAR_TRIM_DURATION_STRUCT                    trim_duration
#define REF_FG_PAR_TEST_INITIAL_REF_STRUCT                 test_initial_ref
#define REF_FG_PAR_TEST_AMPLITUDE_PP_STRUCT                test_amplitude_pp
#define REF_FG_PAR_TEST_NUM_PERIODS_STRUCT                 test_num_periods
#define REF_FG_PAR_TEST_PERIOD_ITERS_STRUCT                test_period_iters
#define REF_FG_PAR_TEST_PERIOD_STRUCT                      test_period
#define REF_FG_PAR_TEST_WINDOW_STRUCT                      test_window
#define REF_FG_PAR_TEST_EXP_DECAY_STRUCT                   test_exp_decay
#define REF_FG_PAR_PRBS_INITIAL_REF_STRUCT                 prbs_initial_ref
#define REF_FG_PAR_PRBS_AMPLITUDE_PP_STRUCT                prbs_amplitude_pp
#define REF_FG_PAR_PRBS_PERIOD_ITERS_STRUCT                prbs_period_iters
#define REF_FG_PAR_PRBS_NUM_SEQUENCES_STRUCT               prbs_num_sequences
#define REF_FG_PAR_PRBS_K_STRUCT                           prbs_k

// Function Generation Parameter structures and union

struct REF_fg_par_meta
{
    uint32_t                 size_of_element;
    uint32_t                 cyc_sel_step;
    uint32_t                 sub_sel_step;
};

struct REF_fg_par
{
    char                   * value;
    struct REF_fg_par_meta   meta;
};

union REF_fg_pars
{
    struct REF_fg_par        fg_par[REF_NUM_FG_PARS];
    struct
    {
        struct  //   1. TRANSACTION LAST_FG_PAR_INDEX
        {
            enum REF_fg_par_idx      * value;
            struct REF_fg_par_meta     meta;
        } transaction_last_fg_par_index;

        struct  //   2. CTRL PLAY
        {
            enum CC_enabled_disabled * value;
            struct REF_fg_par_meta     meta;
        } ctrl_play;

        struct  //   3. CTRL DYN_ECO_END_TIME
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ctrl_dyn_eco_end_time;

        struct  //   4. REF FG_TYPE
        {
            enum FG_type             * value;
            struct REF_fg_par_meta     meta;
        } ref_fg_type;

        struct  //   5. RAMP INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ramp_initial_ref;

        struct  //   6. RAMP FINAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ramp_final_ref;

        struct  //   7. RAMP ACCELERATION
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ramp_acceleration;

        struct  //   8. RAMP LINEAR_RATE
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ramp_linear_rate;

        struct  //   9. RAMP DECELERATION
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } ramp_deceleration;

        struct  //  10. PULSE REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pulse_ref;

        struct  //  11. PULSE DURATION
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pulse_duration;

        struct  //  12. PLEP INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_initial_ref;

        struct  //  13. PLEP FINAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_final_ref;

        struct  //  14. PLEP ACCELERATION
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_acceleration;

        struct  //  15. PLEP LINEAR_RATE
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_linear_rate;

        struct  //  16. PLEP EXP_TC
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_exp_tc;

        struct  //  17. PLEP EXP_FINAL
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } plep_exp_final;

        struct  //  18. PPPL INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_initial_ref;

        struct  //  19. PPPL ACCELERATION1
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration1;

        struct  //  20. PPPL ACCELERATION2
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration2;

        struct  //  21. PPPL ACCELERATION3
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration3;

        struct  //  22. PPPL RATE2
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_rate2;

        struct  //  23. PPPL RATE4
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_rate4;

        struct  //  24. PPPL REF4
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_ref4;

        struct  //  25. PPPL DURATION4
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } pppl_duration4;

        struct  //  26. PPPL ACCELERATION1_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration1_num_els;

        struct  //  27. PPPL ACCELERATION2_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration2_num_els;

        struct  //  28. PPPL ACCELERATION3_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_acceleration3_num_els;

        struct  //  29. PPPL RATE2_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_rate2_num_els;

        struct  //  30. PPPL RATE4_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_rate4_num_els;

        struct  //  31. PPPL REF4_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_ref4_num_els;

        struct  //  32. PPPL DURATION4_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } pppl_duration4_num_els;

        struct  //  33. CUBEXP REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } cubexp_ref;

        struct  //  34. CUBEXP RATE
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } cubexp_rate;

        struct  //  35. CUBEXP TIME
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } cubexp_time;

        struct  //  36. CUBEXP REF_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } cubexp_ref_num_els;

        struct  //  37. CUBEXP RATE_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } cubexp_rate_num_els;

        struct  //  38. CUBEXP TIME_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } cubexp_time_num_els;

        struct  //  39. TABLE FUNCTION
        {
            struct FG_point          * value;
            struct REF_fg_par_meta     meta;
        } table_function;

        struct  //  40. TABLE FUNCTION_NUM_ELS
        {
            uintptr_t                * value;
            struct REF_fg_par_meta     meta;
        } table_function_num_els;

        struct  //  41. TRIM INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } trim_initial_ref;

        struct  //  42. TRIM FINAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } trim_final_ref;

        struct  //  43. TRIM DURATION
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } trim_duration;

        struct  //  44. TEST INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } test_initial_ref;

        struct  //  45. TEST AMPLITUDE_PP
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } test_amplitude_pp;

        struct  //  46. TEST NUM_PERIODS
        {
            uint32_t                 * value;
            struct REF_fg_par_meta     meta;
        } test_num_periods;

        struct  //  47. TEST PERIOD_ITERS
        {
            uint32_t                 * value;
            struct REF_fg_par_meta     meta;
        } test_period_iters;

        struct  //  48. TEST PERIOD
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } test_period;

        struct  //  49. TEST WINDOW
        {
            enum CC_enabled_disabled * value;
            struct REF_fg_par_meta     meta;
        } test_window;

        struct  //  50. TEST EXP_DECAY
        {
            enum CC_enabled_disabled * value;
            struct REF_fg_par_meta     meta;
        } test_exp_decay;

        struct  //  51. PRBS INITIAL_REF
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } prbs_initial_ref;

        struct  //  52. PRBS AMPLITUDE_PP
        {
            cc_float                 * value;
            struct REF_fg_par_meta     meta;
        } prbs_amplitude_pp;

        struct  //  53. PRBS PERIOD_ITERS
        {
            uint32_t                 * value;
            struct REF_fg_par_meta     meta;
        } prbs_period_iters;

        struct  //  54. PRBS NUM_SEQUENCES
        {
            uint32_t                 * value;
            struct REF_fg_par_meta     meta;
        } prbs_num_sequences;

        struct  //  55. PRBS K
        {
            uint32_t                 * value;
            struct REF_fg_par_meta     meta;
        } prbs_k;

    } fg_pars;
};

// Enums for the parameters for each type of reference function except TABLE

    // Enum for TRANSACTION parameters

    enum REF_transaction
    {
        TRANSACTION_LAST_FG_PAR_INDEX ,
        TRANSACTION_NUM_PARS
    };

    // Enum for CTRL parameters

    enum REF_ctrl
    {
        CTRL_PLAY                     ,
        CTRL_DYN_ECO_END_TIME         ,
        CTRL_NUM_PARS
    };

    // Enum for REF parameters

    enum REF_ref
    {
        REF_FG_TYPE                   ,
        REF_NUM_PARS
    };

    // Enum for RAMP parameters

    enum REF_ramp
    {
        RAMP_INITIAL_REF              ,
        RAMP_FINAL_REF                ,
        RAMP_ACCELERATION             ,
        RAMP_LINEAR_RATE              ,
        RAMP_DECELERATION             ,
        RAMP_NUM_PARS
    };

    // Enum for PULSE parameters

    enum REF_pulse
    {
        PULSE_REF                     ,
        PULSE_DURATION                ,
        PULSE_NUM_PARS
    };

    // Enum for PLEP parameters

    enum REF_plep
    {
        PLEP_INITIAL_REF              ,
        PLEP_FINAL_REF                ,
        PLEP_ACCELERATION             ,
        PLEP_LINEAR_RATE              ,
        PLEP_EXP_TC                   ,
        PLEP_EXP_FINAL                ,
        PLEP_NUM_PARS
    };

    // Enum for PPPL parameters

    enum REF_pppl
    {
        PPPL_INITIAL_REF              ,
        PPPL_ACCELERATION1            ,
        PPPL_ACCELERATION2            ,
        PPPL_ACCELERATION3            ,
        PPPL_RATE2                    ,
        PPPL_RATE4                    ,
        PPPL_REF4                     ,
        PPPL_DURATION4                ,
        PPPL_ACCELERATION1_NUM_ELS    ,
        PPPL_ACCELERATION2_NUM_ELS    ,
        PPPL_ACCELERATION3_NUM_ELS    ,
        PPPL_RATE2_NUM_ELS            ,
        PPPL_RATE4_NUM_ELS            ,
        PPPL_REF4_NUM_ELS             ,
        PPPL_DURATION4_NUM_ELS        ,
        PPPL_NUM_PARS
    };

    // Enum for CUBEXP parameters

    enum REF_cubexp
    {
        CUBEXP_REF                    ,
        CUBEXP_RATE                   ,
        CUBEXP_TIME                   ,
        CUBEXP_REF_NUM_ELS            ,
        CUBEXP_RATE_NUM_ELS           ,
        CUBEXP_TIME_NUM_ELS           ,
        CUBEXP_NUM_PARS
    };

    // Enum for TRIM parameters

    enum REF_trim
    {
        TRIM_INITIAL_REF              ,
        TRIM_FINAL_REF                ,
        TRIM_DURATION                 ,
        TRIM_NUM_PARS
    };

    // Enum for TEST parameters

    enum REF_test
    {
        TEST_INITIAL_REF              ,
        TEST_AMPLITUDE_PP             ,
        TEST_NUM_PERIODS              ,
        TEST_PERIOD_ITERS             ,
        TEST_PERIOD                   ,
        TEST_WINDOW                   ,
        TEST_EXP_DECAY                ,
        TEST_NUM_PARS
    };

    // Enum for PRBS parameters

    enum REF_prbs
    {
        PRBS_INITIAL_REF              ,
        PRBS_AMPLITUDE_PP             ,
        PRBS_PERIOD_ITERS             ,
        PRBS_NUM_SEQUENCES            ,
        PRBS_K                        ,
        PRBS_NUM_PARS
    };

// Structures for the parameters for each type of reference function except TABLE

    // Structure for TRANSACTION parameters

    struct REF_transaction_pars
    {
        enum REF_fg_par_idx       last_fg_par_index [1];
    };

    // Structure for CTRL parameters

    struct REF_ctrl_pars
    {
        enum CC_enabled_disabled  play                     [1];
        cc_float                  dyn_eco_end_time         [1];
    };

    // Structure for REF parameters

    struct REF_ref_pars
    {
        enum FG_type              fg_type                   [1];
    };

    // Structure for RAMP parameters

    struct REF_ramp_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  final_ref                [1];
        cc_float                  acceleration             [1];
        cc_float                  linear_rate              [1];
        cc_float                  deceleration             [1];
    };

    // Structure for PULSE parameters

    struct REF_pulse_pars
    {
        cc_float                  ref                     [1];
        cc_float                  duration                [1];
    };

    // Structure for PLEP parameters

    struct REF_plep_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  final_ref                [1];
        cc_float                  acceleration             [1];
        cc_float                  linear_rate              [1];
        cc_float                  exp_tc                   [1];
        cc_float                  exp_final                [1];
    };

    // Structure for PPPL parameters

    struct REF_pppl_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  acceleration1            [FG_MAX_PPPLS];
        cc_float                  acceleration2            [FG_MAX_PPPLS];
        cc_float                  acceleration3            [FG_MAX_PPPLS];
        cc_float                  rate2                    [FG_MAX_PPPLS];
        cc_float                  rate4                    [FG_MAX_PPPLS];
        cc_float                  ref4                     [FG_MAX_PPPLS];
        cc_float                  duration4                [FG_MAX_PPPLS];
        uintptr_t                 acceleration1_num_els    [1];
        uintptr_t                 acceleration2_num_els    [1];
        uintptr_t                 acceleration3_num_els    [1];
        uintptr_t                 rate2_num_els            [1];
        uintptr_t                 rate4_num_els            [1];
        uintptr_t                 ref4_num_els             [1];
        uintptr_t                 duration4_num_els        [1];
    };

    // Structure for CUBEXP parameters

    struct REF_cubexp_pars
    {
        cc_float                  ref                    [FG_CUBEXP_MAX_POINTS];
        cc_float                  rate                   [FG_CUBEXP_MAX_POINTS];
        cc_float                  time                   [FG_CUBEXP_MAX_POINTS];
        uintptr_t                 ref_num_els            [1];
        uintptr_t                 rate_num_els           [1];
        uintptr_t                 time_num_els           [1];
    };

    // Structure for TRIM parameters

    struct REF_trim_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  final_ref                [1];
        cc_float                  duration                 [1];
    };

    // Structure for TEST parameters

    struct REF_test_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  amplitude_pp             [1];
        uint32_t                  num_periods              [1];
        uint32_t                  period_iters             [1];
        cc_float                  period                   [1];
        enum CC_enabled_disabled  window                   [1];
        enum CC_enabled_disabled  exp_decay                [1];
    };

    // Structure for PRBS parameters

    struct REF_prbs_pars
    {
        cc_float                  initial_ref              [1];
        cc_float                  amplitude_pp             [1];
        uint32_t                  period_iters             [1];
        uint32_t                  num_sequences            [1];
        uint32_t                  k                        [1];
    };

// EOF
