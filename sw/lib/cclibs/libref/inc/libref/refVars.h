//! @file  inc/libref/refVars.h
//!
//! @brief Converter Control Reference Manager library : Variables header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef REFVARS_H
#define REFVARS_H

//! Use refMgrVarValue() with a pointer to the reg_mgr structure.

#define refMgrVarValue(REF_MGR, VAR_NAME)    ((REF_MGR)->REF_VAR_ ## VAR_NAME)
#define refMgrVarPointer(REF_MGR, VAR_NAME)  (&(REF_MGR)->REF_VAR_ ## VAR_NAME)

#define REF_VAR_REF_ITERATION_INDEX              iteration_index                                      // uint32_t              Iteration index from libreg for the regulated signal (B|I|V)
#define REF_VAR_REF_FG_TIME                      fg.time_fp32                                         // cc_float              Function time (time relative to event time)
#define REF_VAR_REF_FG_TIME_ADV                  fg.time_adv_fp32                                     // cc_float              Function time (time relative to event time) advanced by ref advance
#define REF_VAR_REF_FG_TYPE                      fg.pars->meta.type                                   // enum FG_type          Function type of the currently running function
#define REF_VAR_REF_FG_STATUS                    fg.status                                            // enum FG_status        Function generator status
#define REF_VAR_REF_RAMP_MGR                     ramp_mgr.status                                      // enum FG_status        Ramp manager generator status
#define REF_VAR_REF_STATE                        ref_state                                            // enum REF_state        Reference State Machine
#define REF_VAR_REF_STATE_MASK                   ref_state_mask                                       // uint32_t              Reference State Machine as a bit mask (only 1 bit is ever set)
#define REF_VAR_REF_STATE_COUNTER                ref_state_counter                                    // uint32_t              Counter incremented after each execution of ref state machine
#define REF_VAR_REF_FG_UNLIMITED                 ref_fg_unlimited                                     // cc_float              Function generator reference value
#define REF_VAR_REF_TO_OFF_TIME                  to_off_time                                          // struct CC_us_time     Time (s.us) of last transition to TO_OFF reference state
#define REF_VAR_REF_TO_OFF_TIME_S                to_off_time.secs.abs                                 // uint32_t              Time (s) of last transition to TO_OFF reference state
#define REF_VAR_REF_FG                           ref_fg                                               // cc_float              Function generator reference value after limitations from libreg
#define REF_VAR_REF_RT                           ref_rt                                               // cc_float              Real-time reference value
#define REF_VAR_REF_VFEEDFWD                     vfeedfwd_delay.latest.signal                         // cc_float              V feed forward signal (after scaling with VFEEDFWD GAIN)
#define REF_VAR_REF_NOW                          ref_now                                              // cc_float              Actual reference (REF_FG + REF_RT)
#define REF_VAR_REF_ACTUATION                    ref_actuation                                        // cc_float              Actuation reference taking into account the polarity switch
#define REF_VAR_REF_DAC                          ref_dac                                              // cc_float              DAC reference taking into account the polarity switch
#define REF_VAR_REF_NUM_INVALID_RT_REF           rt_ref.num_invalid                                   // uint32_t              Counter of invalid rt_ref values
#define REF_VAR_REF_FAULTS                       latched_faults                                       // uint32_t              Bit mask of latched faults (REF_XXX_BIT_MASK)
#define REF_VAR_REF_FAULTS_IN_OFF                faults_in_off                                        // uint32_t              Equals REF FAULTS when REF STATE is OFF and zero otherwise
#define REF_VAR_REF_WARNINGS                     warnings                                             // uint32_t              Bit mask of latched warnings (REF_XXX_BIT_MASK)
#define REF_VAR_REF_DIRECT_STATE                 fsm.direct.state                                     // enum REF_direct_state REF_DIRECT_IDLE or REF_DIRECT_RUNNING
#define REF_VAR_REF_CYCLING_STATUS               fsm.cycling.status                                   // enum FG_status        Cycling status
#define REF_VAR_REF_CYCLING_COUNTER              fsm.cycling.counter                                  // uint32_t              Period counter in CYCLING that is reset to 1 when cycle starts and 0 when not in CYCLING
#define REF_VAR_REF_V_MAX                        pc_on_cache.max_v_ref                                // cc_float              Max voltage reference for LOAD_SELECT
#define REF_VAR_REF_V_MIN                        pc_on_cache.min_v_ref                                // cc_float              Min voltage reference for LOAD_SELECT
#define REF_VAR_REF_I_MAX                        pc_on_cache.max_i_ref                                // cc_float              Max current reference for LOAD_SELECT
#define REF_VAR_REF_I_MIN                        pc_on_cache.min_i_ref                                // cc_float              Min current reference for LOAD_SELECT
#define REF_VAR_REF_B_MAX                        pc_on_cache.max_b_ref                                // cc_float              Max field reference for LOAD_SELECT
#define REF_VAR_REF_B_MIN                        pc_on_cache.min_b_ref                                // cc_float              Min field reference for LOAD_SELECT
#define REF_VAR_REF_MAX                          pc_on_cache.max_ref                                  // cc_float              Max reference for LOAD_SELECT and MODE_REG_MODE_CYC
#define REF_VAR_REF_MIN                          pc_on_cache.min_ref                                  // cc_float              Min reference for LOAD_SELECT and MODE_REG_MODE_CYC
#define REF_VAR_FSM_TO_OFF_COMPLETED             fsm.to_off.completed                                 // bool                  True when the TO_OFF ramp down has finished
#define REF_VAR_ILC_STATE                        ilc.state                                            // uint32_t              ILC state machine
#define REF_VAR_ILC_STATUS                       ilc.status                                           // uint32_t              ILC background status flags
#define REF_VAR_ILC_RT_STATUS                    ilc.rt_status                                        // uint32_t              ILC real-time status flags
#define REF_VAR_ILC_FAILED_CYC_SEL               ilc.failed_cyc_sel                                   // uint32_t              Cycle sel if failed due to RMS error
#define REF_VAR_ILC_REF                          ilc.logging.ref                                      // cc_float              Most recent ILC reference value for logging
#define REF_VAR_ILC_ERR                          ilc.logging.err                                      // cc_float              Most recent ILC error value for logging
#define REF_VAR_ILC_RMS_ERR                      ilc.logging.rms_err                                  // cc_float              Most recent cycle RMS ILC error value for logging
#define REF_VAR_ILC_INITIAL_RMS_ERR              ilc.logging.initial_rms_err                          // cc_float              First cycle RMS ILC error value for logging
#define REF_VAR_ILC_RMS_V_REF_RATE               ilc.logging.rms_v_ref_rate                           // cc_float              RMS v_ref_rate for logging
#define REF_VAR_ILC_RMS_V_REF_RATE_LIMIT         ilc.logging.rms_v_ref_rate_limit                     // cc_float              Limit on RMS v_ref_rate for logging
#define REF_VAR_ILC_LOG_TIME_STAMP               ilc.logging.time_stamp                               // struct CC_ns_time     Time stamp for the latest sample of the cycle RMS err signals
#define REF_VAR_ILC_Q_FUNC                       ilc.q.function                                       // cc_float *            Q filter function
#define REF_VAR_ILC_MAX_SAMPLES                  ilc.max_samples                                      // uint32_t              Max ILC ref length – used for tests by ccrt only
#define REF_VAR_EVENT_ACTIVE                     fg.event                                             // struct REF_event      Active event structure
#define REF_VAR_EVENT_SUB_SEL                    fg.event.sub_sel                                     // uint32_t              Active event sub-device selector
#define REF_VAR_EVENT_CYC_SEL                    fg.event.cyc_sel                                     // uint32_t              Active event cycle selector
#define REF_VAR_EVENT_REF_CYC_SEL                fg.event.ref_cyc_sel                                 // uint32_t              Active event reference cycle selector
#define REF_VAR_FLAG_REG_COMPLETE_ITER           reg_complete                                         // bool                  True if regulation was completed on this iteration
#define REF_VAR_FLAG_POST_FUNC                   post_func                                            // bool                  True if reference manager is post function
#define REF_VAR_FLAG_EXT_EVT_RCVD                event_mgr.ext_evt_rcvd                               // uint8_t               True on iteration when external timing event is received (Run;StartFunc;Abort;Pause;Unpause;Coast;Recover)
#define REF_VAR_FLAG_EXT_EVT_OK                  event_mgr.ext_evt_ok                                 // uint8_t               True on iteration when external timing event is accepted
#define REF_VAR_FLAG_FG_INTERNAL_EVT             fg.internal_evt                                      // uint8_t               True on iteration when next internal event becomes active
#define REF_VAR_FLAG_FG_START_FUNC               fg.start_func                                        // uint8_t               True on iteration when refEventStartFunc is called for start_func
#define REF_VAR_FLAG_FG_USE_ARM_NOW              fg.use_arm_now                                       // uint8_t               True on iteration when refEventStartFunc is called for use_arm_now
#define REF_VAR_FLAG_ENABLE_REG_ERR              enable_reg_err                                       // bool                  True if regulation error limits are to be checked
#define REF_VAR_FLAG_FORCE_OPENLOOP              force_openloop                                       // bool                  True if libref wants to force libreg to use open loop regulation
#define REF_VAR_FLAG_CYCLING_WARNING             fsm.cycling.warning                                  // bool                  True if a pre-function or function warning was seen within the last two calls to reset warnings
#define REF_VAR_FLAG_RT_REF_DIRECT               rt_ref.direct                                        // bool                  True if the real-time ref is in use as a direct ref in DIRECT state
#define REF_VAR_FLAG_RT_REF_DELTA                rt_ref.delta                                         // bool                  True if the real-time ref is in use as a delta_ref in CYCLING or IDLE states
#define REF_VAR_FLAG_RT_REF_IN_USE               rt_ref.in_use                                        // bool                  True if either RT_REF_DIRECT or RT_REF_DELTA is true
#define REF_VAR_FLAG_ECO_ONCE                    fsm.full_eco.once                                    // bool                  True if next cycle should be suppressed in FULL_ECO
#define REF_VAR_FLAG_ILC_FAILED                  ilc.failed                                           // bool                  True if the ILC state is FAILED
#define REF_VAR_FLAG_VFEEDFWD_VALID              vfeedfwd_delay.latest.is_valid                       // bool                  True if the V feed forward signal is valid
#define REF_VAR_PRE_FUNC_B_CALC_DURATION         pre_func_mgr.b.duration.calculated                   // cc_float              Calculated durations for field pre-function segments
#define REF_VAR_PRE_FUNC_B_MEAS_DURATION         pre_func_mgr.b.duration.measured                     // cc_float              Measured durations for field pre-function segments
#define REF_VAR_PRE_FUNC_I_CALC_DURATION         pre_func_mgr.i.duration.calculated                   // cc_float              Calculated durations for current pre-function segments
#define REF_VAR_PRE_FUNC_I_MEAS_DURATION         pre_func_mgr.i.duration.measured                     // cc_float              Measured durations for current pre-function segments
#define REF_VAR_PRE_FUNC_B_CALC_RMS              pre_func_mgr.b.rms.calculated                        // cc_float              Calculated RMS for field pre-function segments
#define REF_VAR_PRE_FUNC_B_MEAS_RMS              pre_func_mgr.b.rms.measured                          // cc_float              Measured RMS for field pre-function segments
#define REF_VAR_PRE_FUNC_I_CALC_RMS              pre_func_mgr.i.rms.calculated                        // cc_float              Calculated RMS for current pre-function segments
#define REF_VAR_PRE_FUNC_I_MEAS_RMS              pre_func_mgr.i.rms.measured                          // cc_float              Measured RMS for current pre-function segments
#define REF_VAR_REFARMED_REG_MODE                static_vars.ref_armed.reg_mode                       // enum REG_mode         Regulation mode for this armed reference function
#define REF_VAR_REFARMED_FG_TYPE                 static_vars.ref_armed.fg_pars.meta.type              // enum FG_type          Function type for this armed reference function
#define REF_VAR_REFARMED_POLARITY                static_vars.ref_armed.fg_pars.meta.polarity          // enum FG_func_pol      Function polarity for this armed reference function
#define REF_VAR_REFARMED_LIMITS_INVERTED         static_vars.ref_armed.fg_pars.meta.limits_inverted   // bool                  Limits inverted flag for this armed reference function
#define REF_VAR_REFARMED_START_TIME              static_vars.ref_armed.fg_pars.meta.time.start        // cc_float              Start time for this armed reference function
#define REF_VAR_REFARMED_END_TIME                static_vars.ref_armed.fg_pars.meta.time.end          // cc_float              End time for this armed reference function
#define REF_VAR_REFARMED_DURATION                static_vars.ref_armed.fg_pars.meta.time.duration     // cc_float              Duration for this armed reference function
#define REF_VAR_REFARMED_INITIAL_REF             static_vars.ref_armed.fg_pars.meta.range.initial_ref // cc_float              Initial reference for this armed reference function
#define REF_VAR_REFARMED_MIN_REF                 static_vars.ref_armed.fg_pars.meta.range.min_ref     // cc_float              Minimum reference for this armed reference function
#define REF_VAR_REFARMED_MAX_REF                 static_vars.ref_armed.fg_pars.meta.range.max_ref     // cc_float              Maximum reference for this armed reference function
#define REF_VAR_REFARMED_FINAL_REF               static_vars.ref_armed.fg_pars.meta.range.final_ref   // cc_float              Final reference for this armed reference function
#define REF_VAR_REFARMED_FINAL_RATE              static_vars.ref_armed.fg_pars.meta.range.final_rate  // cc_float              Final rate for this armed reference function
#define REF_VAR_REFARMED_MIN_LIMIT               static_vars.ref_armed.fg_pars.meta.limits.min        // cc_float              Minimum limit forthis armed reference function
#define REF_VAR_REFARMED_MAX_LIMIT               static_vars.ref_armed.fg_pars.meta.limits.max        // cc_float              Maximum limit for this armed reference function
#define REF_VAR_REFARMED_RATE_LIMIT              static_vars.ref_armed.fg_pars.meta.limits.rate       // cc_float              Rate limit for this armed reference function
#define REF_VAR_REFARMED_PLEP_RMS                static_vars.ref_armed.fg_pars.plep.rms               // cc_float              Calculated RMS for PLEP function
#define REF_VAR_REFARMED_CUBEXP_MAX_REF          static_vars.ref_armed.fg_pars.cubexp.max_ref         // cc_float              Calculated max ref for CUBEXP segments
#define REF_VAR_REFARMED_CUBEXP_MIN_REF          static_vars.ref_armed.fg_pars.cubexp.min_ref         // cc_float              Calculated min ref for CUBEXP segments
#define REF_VAR_REFARMED_CUBEXP_MAX_RATE         static_vars.ref_armed.fg_pars.cubexp.max_rate        // cc_float              Calculated max rate for CUBEXP segments
#define REF_VAR_CYCSTATUS_COUNTER                static_vars.cyc_status.counter                       // uint32_t              Count of times this function was played
#define REF_VAR_CYCSTATUS_STATUS                 static_vars.cyc_status.data.status_bit_mask          // uint32_t              Cycle status bit mask
#define REF_VAR_CYCSTATUS_FG_TYPE                static_vars.cyc_status.data.fg_type                  // enum FG_type          Functio type index for last of this cycle
#define REF_VAR_CYCSTATUS_REG_MODE               static_vars.cyc_status.data.reg_mode                 // enum REG_mode         Reg mode for last play of this cycle
#define REF_VAR_CYCSTATUS_EVENT_TIME             static_vars.cyc_status.data.event_us_time            // struct CC_us_time     Event time for last play of this cycle
#define REF_VAR_CYCSTATUS_MAX_ABS_REG_ERR        static_vars.cyc_status.data.max_abs_reg_err          // cc_float              Max absolute regulation error during function
#define REF_VAR_CYCSTATUS_PRE_FUNC_PLATEAU_ERR   static_vars.cyc_status.data.pre_func_plateau_err     // cc_float              Pre-function plateau duration error
#define REF_VAR_CYCSTATUS_ILC_RMS_ERR            static_vars.cyc_status.ilc.rms_err                   // cc_float              RMS ILC error
#define REF_VAR_CYCSTATUS_ILC_INITIAL_RMS_ERR    static_vars.cyc_status.ilc.initial_rms_err           // cc_float              RMS ILC error for the first cycle when ILC started calculating
#define REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE     static_vars.cyc_status.ilc.rms_v_ref_rate            // cc_float              RMS v_ref_rate
#define REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE_LIM static_vars.cyc_status.ilc.rms_v_ref_rate_limit      // cc_float              Limit on RMS v_ref_rate
#define REF_VAR_FGERROR_ERRNO                    static_vars.fg_error.fg_errno                        // enum FG_errno         Error number from attempt to arm reference function
#define REF_VAR_FGERROR_FG_TYPE                  static_vars.fg_error.fg_type                         // enum FG_type          Function type index from attempt to arm reference function
#define REF_VAR_FGERROR_INDEX                    static_vars.fg_error.index                           // uint32_t              Error index from attempt to arm reference function
#define REF_VAR_FGERROR_DATA                     static_vars.fg_error.data                            // cc_float              Error data from attempt to arm reference function

#endif // REFVARS_H

// EOF
