//! @file  refStateToOff.h
//! @brief Converter Control Reference Manager library: TO_OFF reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! TO_OFF state structure

struct REF_state_to_off
{
    uint32_t    to_tc_transition_counter;       //!< TO_OFF to TO_CYCLING transition counter
    bool        completed;                      //!< True when the ramp down has finished
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in TO_OFF state.
//!
//! This is called on entry into the TO_OFF reference state to prepare operation of the refStateToOffRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateToOffRT     Pointer to function for TO_OFF state.

RefState * refStateToOffInitRT(struct REF_mgr * const ref_mgr);


//! Operate TO_OFF state.
//!
//! This is called every iteration of the reference state machine when the state is TO_OFF.
//! This includes the first iteration, following the call to refStateToOffInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_TO_OFF     Index of the TO_OFF state.

enum REF_state refStateToOffRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
