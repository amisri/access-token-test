//! @file  refEvent.h
//! @brief Converter Control Reference Manager library Event functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"


#ifdef __cplusplus
extern "C" {
#endif

//! Prepares function part of event of given type and marks it as pending
//!
//! If event is of type 'start func' or 'use arm now' and CTRL PLAY parameter is disabled for the specified cyc sel/sub sel,
//! the function will arm NONE with the duration of armed function.
//!
//! Note that this function is for internal use and shouldn't be called by the application.
//!
//! @param[in,out]  ref_mgr      Pointer to ref manager structure
//! @param[in]      sub_sel      Sub-device selector of the new event
//! @param[in]      cyc_sel      Cycle selector of the new event
//! @param[in]      ref_cyc_sel  Cycle selector to use for the reference with cyc_sel equals the test cyc_sel
//! @param[in]      event_type   Type of the new event
//!
//! @return         Peak-peak range of the armed reference

cc_float refEventPrepareNextFunc(struct REF_mgr    * ref_mgr,
                                 uint32_t            sub_sel,
                                 uint32_t            cyc_sel,
                                 uint32_t            ref_cyc_sel,
                                 enum REF_event_type event_type);


//! Records a start function event
//!
//! The function will try to prepare a start func event with the given time at which the reference function, armed for
//! the specified cyc sel and sub sel, should be run.
//!
//! Event won't be recorded when:
//! * there's already a pending start func event
//! * ref mode isn't CYCLING or ref state isn't TO_CYCLING, CYCLING, POL_SWITCHING or ECONOMY
//! * there isn't any function armed for the chosen cyc sel and sub sel
//!
//! If CTRL PLAY parameter is disabled for the specified cyc sel/sub sel, the function will arm NONE with the duration
//! of armed function.
//!
//! @param[in]  ref_mgr        Pointer to reference manager structure
//! @param[in]  event_us_time  Pointer to the absolute UTC time of the event with microsecond resolution
//! @param[in]  sub_sel        Sub-device selector of the event
//! @param[in]  cyc_sel        Cycle selector of the event
//! @param[in]  economy        True to suppress the cycle (ramp to STANDBY level)
//!
//! @retval  true   If event was registered
//! @retval  false  If event was discarded

bool refEventStartFunc(struct REF_mgr    * ref_mgr,
                       struct CC_us_time * event_us_time,
                       uint32_t            sub_sel,
                       uint32_t            cyc_sel,
                       bool                economy);

//! Specify the time of a run event
//!
//! This allows the application to define the time when the armed reference function should be run.
//! This applies only to ARMED reference state.  The state will change to RUNNING as soon as this is called in ARMED state.
//! The event time must be set in the REF_RUN parameter before calling this function. If the time in REF_RUN is in the
//! past, the event time will be set to 1s in the future. The actual event time will be stored back in REF_RUN,
//! provided the event is accepted. If the event was not registered then REF_RUN will not be modified.
//!
//! @param[in]  ref_mgr     Pointer to reference manager structure
//!
//! @retval  true   If event was registered
//! @retval  false  If event was not registered because ref was not armed

bool refEventRun(struct REF_mgr * ref_mgr);


//! Aborts running function immediately
//!
//! This allows the application to abort a RUNNING function immediately. If the reference is changing,
//! the reference state will change to TO_IDLE, otherwise it will go directly to IDLE.
//!
//! @param[in]  ref_mgr  Pointer to reference manager structure
//!
//! @retval  true   If abort is accepted,
//! @retval  false  If the abort request can't be accepted in the current state

bool refEventAbort(struct REF_mgr * ref_mgr);


//! Records pause event
//!
//! The event will be discarded when:
//! * there's already a pause event pending
//! * ref mode isn't CYCLING or ref state isn't POL_SWITCHING, TO_CYCLING, CYCLING or PAUSED
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventPause(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);


//! Records unpause event
//!
//! The event will be discarded when:
//! * there's already an unpause event pending
//! * ref mode isn't CYCLING or ref state isn't POL_SWITCHING, TO_CYCLING, CYCLING or PAUSED
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventUnpause(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);


//! Records coast event
//!
//! The event will be discarded when:
//! * there's already a coast event pending
//! * ref mode isn't already CYCLING
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventCoast(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);


//! Records recover (from coast) event
//!
//! The event will be discarded when:
//! * there's already a recover event pending
//! * ref mode is already CYCLING or ref state isn't CYCLING
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventRecover(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);


//! Records start harmonics event
//!
//! The event will be discarded when:
//! * there's already a start harmonics event pending
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventStartHarmonics(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);


//! Records stop harmonics event
//!
//! The event will be discarded when:
//! * there's already a stop harmonics event pending
//!
//! @param[in,out]  ref_mgr         Pointer to reference manager structure
//! @param[in]      event_us_time   Time at which the event should become active
//!
//! @retval  true   Event was registered
//! @retval  false  Event was discarded

bool refEventStopHarmonics(struct REF_mgr * ref_mgr, struct CC_us_time * event_us_time);

#ifdef __cplusplus
}
#endif

// EOF
