//! @file  inc/libref/refFgPars.h
//!
//! @brief Converter Control Reference Manager library : Function Generator Parameters header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Declare typedefs for pointers to parameter setting functions

typedef void (*refFgParsSetParsFunc)(struct REF_mgr  * const,
                                     uint32_t          const,
                                     cc_float  const * const,
                                     struct FG_error * const);

// Parameter setting functions

void refFgParsSetParsRamp(struct REF_mgr * ref_mgr, uint32_t num_pars, cc_float const * par_values, struct FG_error * fg_error);
void refFgParsSetParsPlep(struct REF_mgr * ref_mgr, uint32_t num_pars, cc_float const * par_values, struct FG_error * fg_error);
void refFgParsSetParsTrim(struct REF_mgr * ref_mgr, uint32_t num_pars, cc_float const * par_values, struct FG_error * fg_error);
void refFgParsSetParsTest(struct REF_mgr * ref_mgr, uint32_t num_pars, cc_float const * par_values, struct FG_error * fg_error);
void refFgParsSetParsPrbs(struct REF_mgr * ref_mgr, uint32_t num_pars, cc_float const * par_values, struct FG_error * fg_error);

// EOF
