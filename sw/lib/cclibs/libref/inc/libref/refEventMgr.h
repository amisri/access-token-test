//! @file  refEvent.h
//! @brief Converter Control Reference Manager library Event functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"


//! Status of the active reference event in CYCLING operational mode

enum REF_active_status
{
    REF_ACTIVE_NOT_UPDATED,
    REF_ACTIVE_FUNC_UPDATED,
    REF_ACTIVE_UPDATED,
    REF_ACTIVE_EMPTY,
};

//! Reference event structure
//!
//! The structure contains the reference event time and event meta data.

struct REF_event
{
    struct CC_ns_time       abs_time;                   //!< Absolute event time (s, us)
    struct CC_ns_time       activation_time;            //!< Absolute event time including ref_advance and function start time
    uint32_t                sub_sel;                    //!< Sub-device selector
    uint32_t                cyc_sel;                    //!< Cycle selector
    uint32_t                ref_cyc_sel;                //!< Cycle selector for the reference function
    enum REG_rst_source     rst_source;                 //!< RST source selector (OP or TEST)
    struct REF_pre_func_seq * pre_func;                 //!< Pointer to active pre-function sequence: pre_func_seq or pre_func_ramp
    struct REF_pre_func_seq pre_func_ramp;              //!< RAMP pre-function sequence
    struct REF_pre_func_seq pre_func_seq;               //!< Non-RAMP pre-function sequence
};

//! Reference event function structure
//!
//! The structure contains all information associated with the reference
//! function of the reference event.

struct REF_func
{
    uint32_t                sub_sel;                    //!< Sub-device selector
    uint32_t                cyc_sel;                    //!< Cycle selector
    uint32_t                ref_cyc_sel;                //!< Reference cycle selector
    enum REF_event_type     event_type;                 //!< Type of ref run event
    struct REF_armed        ref_armed;                  //!< Pointer to active in next_and_active[] or NULL
    struct FG_point       * table_function;             //!< Pointer to table_function[point_idx]
    cc_float                dyn_eco_plep_start_time;    //!< Time in function when the dynamic economy PLEP must start
    union FG_pars           dyn_eco_plep;               //!< PLEP parameters for dynamic economy function
};

//! Container for reference event and function

struct REF_container
{
    struct REF_event      * event;                      //!< Convenience pointer to the event structure
    struct REF_func       * func;                       //!< Convenience pointer to the function structure
    uint32_t                event_idx;                  //!< Index of the event in the buffer array
    uint32_t                func_idx;                   //!< Index of the function in the buffer array
    bool                    discard_func;               //!< Helper flag
    enum REF_thread         owner;                      //!< Owner of the structure, allowed to edit it
};

//! Action event structure for PAUSE, UNPAUSE, TO_IDLE and TO_CYCLING events

struct REF_action_event
{
    struct CC_ns_time       time;                       //!< Time at which the event should be processed
    enum REF_event_type     type;                       //!< Event type
    bool                    is_pending;                 //!< True indicates that the event is waiting for processing
    bool                    is_active;                  //!< True means that the event should be processed
};

//! Main structure for event management
//!
//! The structure contains structures and buffers for all libref events

struct REF_event_mgr
{
    struct REF_event_mgr_buffers
    {
        struct REF_event    events[2];                  //!< Buffer for reference event structures
        struct REF_func     funcs[3];                   //!< Buffer for reference function structures
    } buffers;

    struct REF_container    active;                     //!< Contains active function and event
    struct REF_container    next_start_func;            //!< Contains pending 'start func' or 'run' function and event
    struct REF_container    next_use_arm_now;           //!< Contains pending 'use arm now' function and event

    struct REF_action_event pause_event;                //!< Pause event structure
    struct REF_action_event unpause_event;              //!< Unpause event structure
    struct REF_action_event internal_pause_event;       //!< Internal pause event structure (used by TABLE pause points)
    struct REF_action_event coast_event;                //!< Coast event structure
    struct REF_action_event recover_event;              //!< Recover (from coast) event structure
    struct REF_action_event start_harmonics_event;      //!< Start harmonics event structure
    struct REF_action_event stop_harmonics_event;       //!< Stop harmonics event structure

    bool                    coast;                      //!< Coast active flag
    bool                    abort;                      //!< Abort event flag
    uint8_t                 ext_evt_rcvd;               //!< True when an external event is received (Run,StartFunc,Abort,Pause,Unpause,Coast,Recover)
    uint8_t                 ext_evt_ok;                 //!< True when an external event is accepted
};


// Static inline functions

static inline void refEventMgrShiftFlagsRT(struct REF_event_mgr * const event_mgr)
{
    // Shift external event flags - this is an efficient way of extending the flag to ensure that it can be logged

    event_mgr->ext_evt_rcvd >>= 1;
    event_mgr->ext_evt_ok   >>= 1;
}


#ifdef __cplusplus
extern "C" {
#endif

//! Links ref_mgr to the space allocated by the application for the running table functions.
//! The space for three function is needed, one is active, and two are to allow the next functions
//! to be prepared, one for start function events and the other use_arm_now events.
//!
//! @param[in,out]  event_mgr                 Pointer to event manager structure
//! @param[in]      running_table_function    Pointer to point array for the three running table functions (active, start_func, use_arm_now)
//! @param[in]      max_table_points          Max number of table points per function

void refEventMgrInit(struct REF_event_mgr * event_mgr,
                     struct FG_point      * running_table_function,
                     uint32_t               max_table_points);


//! Registers function part of the next reference event
//!
//! @param[in,out]  event_mgr   Pointer to the event manager structure
//! @param[in]      next_func   Pointer to the filled function buffer
//! @param[in]      event_type  Type of the event

void refEventMgrRegisterFuncBuffer(struct REF_event_mgr * const event_mgr,
                                   struct REF_func      * const next_func,
                                   enum REF_event_type    const event_type);


//! Marks next event as pending
//!
//! This function should be called after calling refEventMgrRegisterNextEvent
//! and refEventMgrRegisterFuncBuffer
//!
//! @param[in,out]  event_mgr   Pointer to the event manager structure
//! @param[in,out]  event_type  Type of the event

void refEventMgrRegisterNextFunc(struct REF_event_mgr * event_mgr,
                                 enum REF_event_type    event_type);


//! Discards active reference event
//!
//! The function clears pointers to the event and the function parts of the
//! event, but it doesn't alter the data in the underlying buffers.
//!
//! @param[in,out]  event_mgr   Pointer to the event manager structure

void refEventMgrClearActiveRT(struct REF_event_mgr * event_mgr);


//! Updates active event structure
//!
//! The function has to be called in combination with refEventMakeNextFuncActiveRT and it must be called first!
//!
//! @param[in,out]  event_mgr  Pointer to event manager structure
//!
//! @return  Pointer to the newly activated event part of the reference event

struct REF_event * refEventMgrActivateNextEventRT(struct REF_event_mgr * event_mgr);


//! Updates active function structure
//!
//! The function can be called alone or in combination with refEventMakeNextEventActiveRT, when it must be
//! called second!
//!
//! @param[in,out]  event_mgr   Pointer to event management structure
//! @param[in]      event_type  Pending event type from which the new function should be taken.
//!                             Can be REF_EVENT_START_FUNC or REF_EVENT_USE_ARM_NOW.
//!
//! @return  Pointer to the newly activated function part of the reference event

struct REF_func * refEventMgrActivateNextFuncRT(struct REF_event_mgr * event_mgr, enum REF_event_type event_type);


//! Checks if there's a pending event of given type
//!
//! This function should be used only by the background thread!
//!
//! @param[in,out]  event_mgr   Pointer to event manager structure
//! @param[in]      event_type  Type of event. Can be REF_EVENT_START_FUNC or REF_EVENT_USE_ARM_NOW.

bool refEventMgrNextIsPending(struct REF_event_mgr const * event_mgr, enum REF_event_type event_type);


//! Checks if there's a pending event of given type
//!
//! This function should be used only by the real-time thread!
//!
//! @param[in,out]  event_mgr   Pointer to event manager structure
//! @param[in]      event_type  Type of event. Can be REF_EVENT_START_FUNC or REF_EVENT_USE_ARM_NOW.

bool refEventMgrNextIsPendingRT(const struct REF_event_mgr * event_mgr, enum REF_event_type event_type);


//! Processes reference events in CYCLING operational mode
//!
//! This function should be called every iteration in every state belonging to the CYCLING
//! operational mode - TO_CYCLING, CYCLING, DYN_ECO, FULL_ECO, PAUSED, POL_SWITCHING.
//!
//! @param[in,out]  event_mgr          Pointer to event manager structure
//! @param[in]      iter_time          Current iteration (absolute) time
//! @param[in]      clear_active       Flag directing the event manager to clear the active event and function.
//! @param[in]      allow_use_arm_now  The flag enables or disables updating of the active func parameters
//!                                    with pending 'use armed now' events.
//!
//! @return  Status of the active event
//!
//! @retval  REF_ACTIVE_NOT_UPDATED   The active event hasn't been altered in any way
//! @retval  REF_ACTIVE_FUNC_UPDATED  The function part of the active event was updated with
//!                                   newer function from 'use arm now' event
//! @retval  REF_ACTIVE_UPDATED       The event and function part of the active event were updated
//! @retval  REF_ACTIVE_EMPTY         There's no active event at the moment

enum REF_active_status refEventMgrProcessCyclingEventsRT(struct REF_event_mgr * event_mgr,
                                                         struct CC_ns_time     iter_time,
                                                         bool                  clear_active,
                                                         bool                  allow_use_arm_now);


//! Discards stale events
//!
//! @param[in,out]  event_mgr       Pointer to event manager structure
//! @param[in]      ref_mode        Current reference mode
//! @param[in]      ref_state_mask  Bitmask with current ref state bit set

void refEventMgrResetStaleEventsRT(struct REF_event_mgr * event_mgr, enum REF_state ref_mode, uint32_t ref_state_mask);


//! Activates pending action events when the time arrives
//!
//! @param[in,out]  event_mgr  Pointer to event manager structure
//! @param[in]      time       Current iteration (absolute) time

void refEventMgrActivateActionEventsRT(struct REF_event_mgr * event_mgr, struct CC_ns_time time);


//! Activates internal pause event immediately
//!
//! @param[in,out]  event_mgr     Pointer to event manager structure
//! @param[in]      event_time    Time of the event

void refEventMgrTriggerInternalPauseEventRT(struct REF_event_mgr * event_mgr, struct CC_ns_time event_time);


//! Checks if an event can be recorded in current ref mode and state
//!
//! @param[in]  event_type      Type of event
//! @param[in]  ref_mode        Current ref mode
//! @param[in]  ref_state_mask  Bit mask of the current ref state
//!
//! @retval  true   Event can be accepted
//! @retval  false  Event should be discarded

bool refEventMgrRefStateIsValidRT(enum REF_event_type event_type, enum REF_state ref_mode, uint32_t ref_state_mask);


//! Returns pointer to the next free reference function buffer
//!
//! @param[in]  event_mgr   Pointer to the event manager structure
//! @param[in]  event_type  Type of the event for which the buffer should be returned
//!
//! @return  Pointer to the next free function buffer

struct REF_func * refEventMgrGetNextFuncBuffer(struct REF_event_mgr * event_mgr, enum REF_event_type event_type);


//! Returns pointer to the next free reference event buffer
//!
//! @param[in]  event_mgr  Pointer to the event manager structure
//!
//! @return  Pointer to the next free event buffer

struct REF_event * refEventMgrGetNextEventBuffer(struct REF_event_mgr * event_mgr);


//! Returns pointer to the next reference event container
//!
//! @param[in]  event_mgr   Pointer to the event manager structure
//! @param[in]  event_type  Type of the event for which the container should be returned
//!
//! @return  Pointer to the reference event container

struct REF_container * refEventMgrGetNextPointerRT(struct REF_event_mgr * const event_mgr, enum REF_event_type event_type);

//! Registers event part of the next reference event
//!
//! @param[in,out]  event_mgr   Pointer to the event manager structure
//! @param[in]      next_event  Pointer to the filled event buffer

void refEventMgrRegisterNextEvent(struct REF_event_mgr * event_mgr, struct REF_event * next_event);

//! Set external event received flags
//!
//! When status is true, both ext_evt_rcvd and ext_evt_ok are set, otherwise, only ext_evt_rcvd is set.
//!
//! @param[in,out]  event_mgr   Pointer to the event manager structure
//! @param[in]      status      True if event was accepted, false otherwise
//!
//! @returns        status

bool refEventMgrSetExtEvtFlags(struct REF_event_mgr * event_mgr, bool status);

#ifdef __cplusplus
}
#endif

// EOF
