//! @file  refIlc.h
//! @brief Converter Control Reference Manager library Iterative Learning Controller header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! Constants

#define REF_ILC_CYCLING_STATES      (REF_TO_CYCLING_MASK|REF_CYCLING_MASK|REF_PAUSED_MASK|REF_POL_SWITCHING_MASK|REF_DYN_ECO_MASK|REF_FULL_ECO_MASK)

//! Macros

// The cycle data store needs space for the array of struct REF_ilc_cyc_data and the ILC ref buffers. This can all be in slow memory.

// IMPORTANT: The cycle data store is for users 1 - MAX_CYC_SEL. It does not waste the first slot on cyc_sel 0,
//            so there is an offset of 1 between the array index and the cyc_sel.
//            The macro defines the length in units of struct REF_ilc_cyc_data even though most of it will be used
//            for the ref buffers. This is to comply with the compiler aliasing rules.

#define REF_ILC_CYC_DATA_STORE_NUM_ELS(ILC_MAX_SAMPLES,MAX_CYC_SEL) \
        (MAX_CYC_SEL + (ILC_MAX_SAMPLES * MAX_CYC_SEL * sizeof(cc_float) + sizeof(struct REF_ilc_cyc_data) - 1) \
                     / sizeof(struct REF_ilc_cyc_data))


//! ILC Reset options - the order is important: FULL must be greater than PARTIAL

enum REF_ilc_reset
{
    REF_ILC_RESET_NONE    ,
    REF_ILC_RESET_PARTIAL ,
    REF_ILC_RESET_FULL    ,
};

//! ILC real-time status bit indexes

enum REF_ilc_rt_status_bit
{
    REF_ILC_CYCLING_BIT       ,
    REF_ILC_ERR_BIT           ,
    REF_ILC_L_CALC_BIT        ,
    REF_ILC_Q_CALC_BIT        ,
    REF_ILC_REF_RUN_BIT       ,
    REF_ILC_REF_ARM_BIT       ,
    REF_NUM_ILC_RT_STATUS_BITS
};

//! ILC real-time status bit masks

enum REF_ilc_rt_status_bit_mask
{
    REF_ILC_CYCLING_BIT_MASK     = (1 << REF_ILC_CYCLING_BIT   ) ,
    REF_ILC_ERR_BIT_MASK         = (1 << REF_ILC_ERR_BIT       ) ,
    REF_ILC_L_CALC_BIT_MASK      = (1 << REF_ILC_L_CALC_BIT    ) ,
    REF_ILC_Q_CALC_BIT_MASK      = (1 << REF_ILC_Q_CALC_BIT    ) ,
    REF_ILC_REF_RUN_BIT_MASK     = (1 << REF_ILC_REF_RUN_BIT   ) ,
    REF_ILC_REF_ARM_BIT_MASK     = (1 << REF_ILC_REF_ARM_BIT   ) ,
};

//! ILC ERR/L/Q CALC and REF_ILC run control structure

struct REF_ilc_control
{
    uint32_t                  rt_status_bit_mask;                   //!< Real-time status bit mask: REF_ILC_XXXX_BIT_MASK
    struct CC_ns_time         start_time;                           //!< Start time
    uint32_t                  prev_start_flag;                      //!< Previous start flag shifted into bit 1 (0 or 2)
    uint32_t                  index;                                //!< Array index
};

//! ILC cycle data - pad to 64-bit boundary if necessary

struct REF_ilc_cyc_data
{
    cc_float                * ref_buf;                              //!< Pointer to buffer for ILC ref
    uint32_t                  ref_num_els;                          //!< Number of elements in ref_buf
    int32_t                   event_phase_ns;                       //!< Event phase (ns) for the previous execution of this cycle
    uint32_t                  downshifter;                          //!< Down shift to reset minimum RMS v_ref_rate
    cc_float                  min_rms_v_ref_rate;                   //!< Minimum RMS v_ref_rate - three times this is used as the upper limit for RMS v_ref_rate
    cc_float                  initial_rms_err;                      //!< Initial RMS ILC error - uses as the upper limit for RMS ILC error
};

//! ILC event structure

struct REF_ilc_event
{
    uint32_t                  cyc_sel;                              //!< Next cycle selector
    struct CC_ns_time         time;                                 //!< Next start function event time
};

//! ILC error and RMS calculations structure

struct REF_ilc_err
{
    cc_float                  err_buf[REF_ILC_ERR_LEN];             //!< Circular buffer for the ILC error
    uint32_t                  index;                                //!< Error buffer free-running index
    uint32_t                  iter_counter;                         //!< RMS iteration counter
    cc_float                  acc_ilc_err_2;                        //!< Accumulator for ILC error squared
    cc_float                  acc_v_ref_rate_2;                     //!< Accumulator for V_REF_RATE squared
};

//! ILC L function structure

struct REF_ilc_l_func
{
    struct REF_ilc_control    control;                              //!< ILC L calculation control structure
    cc_float                  function[REF_ILC_L_FUNC_LEN];         //!< ILC L Function
    uint32_t                  num_els;                              //!< ILC L Function number of elements (always odd)
    uint32_t                  order;                                //!< Copy of ILC_L_ORDER to detect changes
};

//! ILC Q function structure

struct REF_ilc_q_func
{
    struct REF_ilc_control    control;                              //!< ILC Q calculation control structure
    cc_float                  ref_unfltr[REF_ILC_UNFLTR_REF_LEN];   //!< Circular buffer for the unfiltered ILC ref
    cc_float                  function[REF_ILC_Q_FUNC_LEN];         //!< ILC Q Function
    uint32_t                  num_els;                              //!< ILC Q Function number of elements (always odd)
    uint32_t                  order;                                //!< ILC Q Function order
};

//! ILC ref structure

struct REF_ilc_ref
{
    cc_float                * buf;                                  //!< Pointer to  buffer for ILC ref
    uint32_t                  num_els;                              //!< ILC ref number of elements
    struct REF_ilc_control    control;                              //!< Control structure for the use of REF_ILC
    uint32_t                  run_extended;                         //!< Down shifter to extend REF_RUN to gate the ILC error calculation
    bool                      armed;                                //!< True if num_els > 0 - allows REF_ILC_REF_ILC_BIT_MASK to be set in rt_status
    bool                      running;                              //!< True when REF_ILC is or will be running (this is used to know when the ref advance should be disabled)
};


//! ILC cycle status structure (per cyc_sel)

struct REF_ilc_cyc_status
{
    cc_float                  rms_err;                              //!< RMS ILC error
    cc_float                  initial_rms_err;                      //!< Initial RMS ILC error (from first cycle with ILC calculating)
    cc_float                  rms_v_ref_rate;                       //!< RMS V_REF rate
    cc_float                  rms_v_ref_rate_limit;                 //!< Limit on RMS v_ref_rate, set to three times the minimum value of RMS V_REF rate
};

//! ILC logging structure

struct REF_ilc_logging
{
    struct CC_ns_time         time_stamp;                           //!< Time stamp of new sample to be recorded (cleared to zero to acknowledge)
    cc_float                  rms_err;                              //!< RMS ILC error for most recent log_cyc_sel cycle
    cc_float                  initial_rms_err;                      //!< RMS ILC error for the first log_cyc_sel cycle
    cc_float                  rms_v_ref_rate;                       //!< RMS V_REF rate for the most recent log_cyc_sel cycle
    cc_float                  rms_v_ref_rate_limit;                 //!< The upper limit for RMS V_REF rate is set to three times the minimum value seen

    cc_float                  ref;                                  //!< Most recent ILC_REF value for logging
    cc_float                  err;                                  //!< Most recent ILC_ERR value for logging
};


//! Main structure for the Iterative Learning Controller

struct REF_ilc
{
    struct REF_ilc_cyc_data * cyc_data_store;                       //!< Pointer to array of cycle data storage structures which can be in slow memory.

    struct CC_ns_time         time_stamp;                           //!< Event timestamp for the active cycle

    uint32_t                  max_cyc_sel;                          //!< Maximum cyc_sel
    uint32_t                  max_samples;                          //!< Length of ref buffers in cyc_data_store

    uint32_t                  state;                                //!< ILC state (DISABLED, READY, RUNNING, FAILED)
    uint32_t                  status;                               //!< ILC background status bit mask
    uint32_t                  rt_status;                            //!< ILC real-time status bit mask
    uint32_t                  cyc_sel;                              //!< Active cycle selector
    uint32_t                  log_cyc_sel;                          //!< Cycle selector to be logged in ILC_ERR cycle log
    uint32_t                  failed_cyc_sel;                       //!< Cycle selector for cycle that failed due to the RMS error
    uint32_t                  openloop_downshifter;                 //!< Down shifter to gate REF_ILC after period of open loop
    uint32_t                  openloop_downshifter_input;           //!< Input bit mask for open loop down shifter
    int32_t                   event_phase_ns;                       //!< Event phase (ns) for the current cycle
    cc_float                  ref_range;                            //!< Reference range (max-min) for the cycle
    cc_float                  start_time;                           //!< ILC start time - used to detect changes
    cc_float                  stop_ref;                             //!< Stop reference level to deactivate the ILC calculation
    enum REF_ilc_reset        delayed_reset_type;                   //!< Delayed reset is requested
    bool                      enabled;                              //!< Flag to enable ILC on this cycle (ref_range != 0)
    bool                      prev_ref_stop;                        //!< Previous ref stop flag : ref_adv < stop_ref
    bool                      to_cycling;                           //!< Flag to request transition to TO_CYCLING
    bool                      reset;                                //!< Flag to trigger a full ILC reset by passing through DISABLED and back to STANDBY
    bool                      failed;                               //!< Flag to tell refRtRegWarningsRT() that the ILC state is FAILED

    struct REF_ilc_event      event;                                //!< Parameters cached by refIlcEvent()
    struct REF_ilc_err        err;                                  //!< ILC error and RMS structure
    struct REF_ilc_l_func     l;                                    //!< ILC L function structure
    struct REF_ilc_q_func     q;                                    //!< ILC Q function structure
    struct REF_ilc_ref        ref;                                  //!< ILC ref structure
    struct REF_ilc_logging    logging;                              //!< Logging data
    struct REF_ilc_cyc_status cyc_status;                           //!< Cycle status data
};


//! Static inline functions to access the ILC data store
//! These functions take care of the offset of 1 between the cyc_sel and the data store array index

static inline cc_float * refIlcCycDataRefBuf(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return ilc->cyc_data_store[cyc_sel - 1].ref_buf;
}



static inline uint32_t * refIlcCycDataRefNumEls(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return &ilc->cyc_data_store[cyc_sel - 1].ref_num_els;
}



static inline int32_t * refIlcCycDataEventPhase(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return &ilc->cyc_data_store[cyc_sel - 1].event_phase_ns;
}



static inline uint32_t * refIlcCycDataDownShifter(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return &ilc->cyc_data_store[cyc_sel - 1].downshifter;
}



static inline cc_float * refIlcCycDataMinRmsVrefRate(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return &ilc->cyc_data_store[cyc_sel - 1].min_rms_v_ref_rate;
}



static inline cc_float * refIlcCycDataInitialRmsErr(struct REF_ilc * const ilc, uint32_t cyc_sel)
{
    return &ilc->cyc_data_store[cyc_sel - 1].initial_rms_err;
}

// Static inline functions for libref

//! Test if the ILC returning the calculated reference now.
//!
//! This function is used to tell libref when to remove the reference advance from the calculation of fg_time_adv.
//! It reports the time window in which the reference is or will be returned with a
//! non-zero value.
//!
//! @param[in,out] ilc              Pointer to the ILC structure.
//!
//! @retval        false            Outside the time window for returning the reference.
//! @retval        true             ILC reference is being returned, or will be on the next cycle at this time.

static inline bool refIlcRefRunningRT(struct REF_ilc * const ilc)
{
    return (ilc->rt_status & (REF_ILC_REF_RUN_BIT_MASK|REF_ILC_Q_CALC_BIT_MASK)) != 0;
}


//! Test if the ILC is actively cycling.
//!
//! @param[in,out] ilc              Pointer to the ILC structure.
//!
//! @retval        false            The ILC is not cycling.
//! @retval        true             The ILC is cycling so refIlcRT() should be called on regulation iterations.

static inline bool refIlcCyclingRT(struct REF_ilc * const ilc)
{
    return (ilc->rt_status & REF_ILC_CYCLING_BIT_MASK) != 0;
}


//! Stop ILC for the current cycle.
//!
//! This function is called by refStateCyclingRT() when the current cycle ends. The next cyc_sel may be known or unknown
//! at the time. The function sets the STOP condition flag which enables the transition CYtoTC provided an ILC cycle is active.
//! The ILC cycle is active evening if the ILC is disabled due to the reference range (max-min) being zero.
//!
//! @param[in,out] ilc              Pointer to the ILC structure.

static inline void refIlcStopRT(struct REF_ilc * const ilc)
{
    // Ignore any stop requests unless ILC was disabled on this cycle or at least one ILC stage is running

    ilc->to_cycling =    ilc->enabled == false
                      || (ilc->rt_status & (REF_ILC_L_CALC_BIT_MASK|REF_ILC_Q_CALC_BIT_MASK|REF_ILC_REF_RUN_BIT_MASK)) != 0;
}

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the pointer to Iterative Learning Controller working buffer.
//!
//! The ILC needs a cycle data storage area that can be in slow memory. The number of elements
//! in cyc_data_store can be known using the macro REF_ILC_CYC_DATA_STORE_NUM_ELS(ILC_MAX_SAMPLES,MAX_CYC_SEL).
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     cyc_data_store    Pointer to the array of cyc_data_store structures in slow memory.
//! @param[in]     max_samples       Length in elements of the ILC ref buffer.
//! @param[in]     max_cyc_sel       Maximum cycle selector.

void refIlcInit(struct REF_mgr          * ref_mgr,
                struct REF_ilc_cyc_data * cyc_data_store,
                uint32_t                  max_samples,
                uint32_t                  max_cyc_sel);



//! Trigger an ILC reset
//!
//! All data in the ILC cycle data store will be reset for all cycle selectors on entry into the STANDBY ILC state.
//! This function will trigger this from CYCLING or FAILED states by setting a flag that enables the
//! transition XXtoDS. This will cause the state to change to DISABLED and then STANDBY.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.

void refIlcReset(struct REF_mgr  * ref_mgr);


//! Register cycle event
//!
//! This function is called by refEventStartFunc() when the next function is known.
//! If the ILC is not disabled and sub_sel is zero then the function caches the event time
//! and cycle selector for use by refIlcState(). If ref_range is zero, the ILC will not
//! run during the cycle.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     sub_sel           Sub-device selector - must be zero for the ILC to be enabled.
//! @param[in]     cyc_sel           Cycle selector (1..max_cyc_sel).
//! @param[in]     event_time        Start function event time.
//! @param[in]     ref_range         Reference range (max-min) for the cycle.

void refIlcEvent(struct REF_mgr   * ref_mgr,
                 uint32_t           sub_sel,
                 uint32_t           cyc_sel,
                 struct CC_ns_time  event_time,
                 cc_float           ref_range);


//! Initialize an ILC control structure for calculating or running the ILC.
//!
//! This helper function set the fields of the REF_ilc_control structure.
//!
//! @param[in,out] control             Pointer to ILC control structure.
//! @param[in]     start_time          Start time for the activity.
//! @param[in]     rt_status_bit_mask  Real-time status bit mask for this ILC stage: REF_ILC_XXXX_BIT_MASK

void refIlcControlInit(struct REF_ilc_control * control,
                       struct CC_ns_time        start_time,
                       uint32_t                 rt_status_bit_mask);


//! Reset all, part or none of the ILC cycle data for one or all cycle selectors
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     cyc_sel           Zero: reset buffers for all cycle_selectors. Non-zero: reset just that cyc_sel.
//! @param[in]     reset_type        Type of reset requested

void refIlcResetData(struct REF_mgr   * ref_mgr,
                     uint32_t           cyc_sel,
                     enum REF_ilc_reset reset_type);


//! Reset length of the ILC reference for the specified cyc_sel.
//!
//! If a reference function type changes, the learned ILC reference should be reset, to force the re-learning
//! of the ILC reference. This function does this by setting the number of elements in the ILC REF parameter to zero.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     sub_sel           Sub-device selector - must be zero.
//! @param[in]     cyc_sel           Cycle selector (1..max_cyc_sel).
//! @param[in]     reset_type        Reset type

void refIlcResetRef(struct REF_mgr   * ref_mgr,
                    uint32_t           sub_sel,
                    uint32_t           cyc_sel,
                    enum REF_ilc_reset reset_type);


//! Reset length of the ILC reference for the specified cyc_sel.
//!
//! If a reference function type changes, the learned ILC reference should be reset, to force the re-learning
//! of the ILC reference. This function does this by setting the number of elements in the ILC REF parameter to zero.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.

void refIlcSetMinVRefRate(struct REF_mgr * ref_mgr);


//! Set the background status flags used by the ILC state machine.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.

void refIlcSetStatus(struct REF_mgr * ref_mgr);


//! Set the active L function.
//!
//! The L function can either come from the ILC_L_FUNC array if ILC_L_ORDER is zero or
//! can be set to a unit function (value 1, length 1) if ILC_L_ORDER is greater than zero.
//!
//! If the L function changes, the cycle data for cyc_sel will be reset.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     cyc_sel           Cycle selector (1..max_cyc_sel).
//!
//! @returns       L function order

int32_t refIlcSetLfunc(struct REF_mgr * ref_mgr,
                       uint32_t         cyc_sel);


//! Set the active Q function.
//!
//! The Q function comes from reflecting the ILC_Q_FUNC array. If the length of the
//! array is zero then the Q filter is disabled. If the length is one, then a first-order
//! filter is generated from Q[0] by setting Q[1] = (1 - Q[0]) / 2.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     cyc_sel           Cycle selector (1..max_cyc_sel).
//!
//! @returns       Q function order

int32_t refIlcSetQfunc(struct REF_mgr * ref_mgr,
                       uint32_t         cyc_sel);


//! Link to the saved ILC reference for cyc_sel and re-sample if the event time
//! phase (within the regulation period) has changed, to minimise the error in the following
//! cycles.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     cyc_sel           Cycle selector (1..max_cyc_sel).
//! @param[in]     reg_period_ns     Regulation period in nanoseconds.
//!
//! @returns       Initial rt_status bit mask

uint32_t refIlcGetRef(struct REF_mgr * ref_mgr,
                      uint32_t         cyc_sel,
                      uint32_t         reg_period_ns);


//! Calculate the ILC.
//!
//! This function is called by refRtIterationPostRegRT() on regulation iterations.
//! It will update the ILC reference during the ILC CALC window.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     ref_adv           Advanced reference.
//! @param[in]     reg_meas          Regulated measurement.
//! @param[in]     v_ref_rate        Voltage reference rate of change.

void refIlcRT(struct REF_mgr * ref_mgr,
              cc_float         ref_adv,
              cc_float         reg_meas,
              cc_float         v_ref_rate);


//! Return the ILC reference for the current regulation iteration.
//!
//! This function is called from refRtRegulationRT() to get the ILC reference value computed
//! on the previous cycle of the same cycle selector.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//!
//! @returns       ILC reference or zero

cc_float refIlcRefRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
