//! @file  inc/libref/refFgParIdx.h
//!
//! @brief Converter Control Reference Manager library : Function Generator Parameter Indexes header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define REF_FG_PAR_MAX_ARM_PAR_VALUES     7
#define REF_FG_PAR_MAX_PER_GROUP         11

// Function Generator Parameter indices enum

enum REF_fg_par_idx  
{
    REF_FG_PAR_TRANSACTION_LAST_FG_PAR_INDEX ,      //  0  Non-transactional: FG parameter index that was last set during a transaction
    REF_FG_PAR_CTRL_PLAY                     ,      //  1  Non-transactional: Control if function should be played. When DISABLED a STANDBY function will be played instead.
    REF_FG_PAR_CTRL_DYN_ECO_END_TIME         ,      //  2  Non-transactional: Dynamic economy function end time. This is time when the function generator returns to playing the armed function.
    REF_FG_PAR_REF_FG_TYPE                   ,      //  3  Function type.
    REF_FG_PAR_RAMP_INITIAL_REF              ,      //  4  Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_RAMP_FINAL_REF                ,      //  5  Final reference for a RAMP.
    REF_FG_PAR_RAMP_ACCELERATION             ,      //  6  RAMP acceleration.
    REF_FG_PAR_RAMP_LINEAR_RATE              ,      //  7  Maximum linear rate during the RAMP.
    REF_FG_PAR_RAMP_DECELERATION             ,      //  8  RAMP deceleration.
    REF_FG_PAR_PULSE_REF                     ,      //  9  PULSE reference.
    REF_FG_PAR_PULSE_DURATION                ,      // 10  PULSE duration.
    REF_FG_PAR_PLEP_INITIAL_REF              ,      // 11  Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_PLEP_FINAL_REF                ,      // 12  Final reference for a PLEP.
    REF_FG_PAR_PLEP_ACCELERATION             ,      // 13  PLEP acceleration.
    REF_FG_PAR_PLEP_LINEAR_RATE              ,      // 14  Maximum linear rate during the PLEP.
    REF_FG_PAR_PLEP_EXP_TC                   ,      // 15  PLEP exponential segment time constant.
    REF_FG_PAR_PLEP_EXP_FINAL                ,      // 16  PLEP exponential segment asymptote (zero for automatic).
    REF_FG_PAR_PPPL_INITIAL_REF              ,      // 17  Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_PPPL_ACCELERATION1            ,      // 18  Array of initial PPPL accelerations.
    REF_FG_PAR_PPPL_ACCELERATION2            ,      // 19  Array of second PPPL accelerations.
    REF_FG_PAR_PPPL_ACCELERATION3            ,      // 20  Array of third PPPL accelerations.
    REF_FG_PAR_PPPL_RATE2                    ,      // 21  Array of rates at the start of the second PPPL accelerations.
    REF_FG_PAR_PPPL_RATE4                    ,      // 22  Array of rates at the start of the linear PPPL segments.
    REF_FG_PAR_PPPL_REF4                     ,      // 23  Array of references at the start of the linear PPPL segments.
    REF_FG_PAR_PPPL_DURATION4                ,      // 24  Array of durations of the fourth linear linear PPPL segments.
    REF_FG_PAR_PPPL_ACCELERATION1_NUM_ELS    ,      // 25  Number of elements in the PPPL ACCELERATION1 array.
    REF_FG_PAR_PPPL_ACCELERATION2_NUM_ELS    ,      // 26  Number of elements in the PPPL ACCELERATION2 array.
    REF_FG_PAR_PPPL_ACCELERATION3_NUM_ELS    ,      // 27  Number of elements in the PPPL ACCELERATION3 array.
    REF_FG_PAR_PPPL_RATE2_NUM_ELS            ,      // 28  Number of elements in the PPPL RATE2 array.
    REF_FG_PAR_PPPL_RATE4_NUM_ELS            ,      // 29  Number of elements in the PPPL RATE4 array.
    REF_FG_PAR_PPPL_REF4_NUM_ELS             ,      // 30  Number of elements in the PPPL REF4 array.
    REF_FG_PAR_PPPL_DURATION4_NUM_ELS        ,      // 31  Number of elements in the PPPL DURATION4 array.
    REF_FG_PAR_CUBEXP_REF                    ,      // 32  Array of start/end references for CUBEXP.
    REF_FG_PAR_CUBEXP_RATE                   ,      // 33  Array of start/end rate of change for CUBEXP.
    REF_FG_PAR_CUBEXP_TIME                   ,      // 34  Array of start/end times for CUBEXP.
    REF_FG_PAR_CUBEXP_REF_NUM_ELS            ,      // 35  Number of elements in the CUEXP REF array.
    REF_FG_PAR_CUBEXP_RATE_NUM_ELS           ,      // 36  Number of elements in the CUEXP RATE array.
    REF_FG_PAR_CUBEXP_TIME_NUM_ELS           ,      // 37  Number of elements in the CUEXP TIME array.
    REF_FG_PAR_TABLE_FUNCTION                ,      // 38  TABLE function points array.
    REF_FG_PAR_TABLE_FUNCTION_NUM_ELS        ,      // 39  Number of points in the TABLE function.
    REF_FG_PAR_TRIM_INITIAL_REF              ,      // 40  Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_TRIM_FINAL_REF                ,      // 41  Final reference for a TRIM.
    REF_FG_PAR_TRIM_DURATION                 ,      // 42  Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    REF_FG_PAR_TEST_INITIAL_REF              ,      // 43  Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_TEST_AMPLITUDE_PP             ,      // 44  TEST function peak-peak amplitude.
    REF_FG_PAR_TEST_NUM_PERIODS              ,      // 45  Number of TEST function periods to play.
    REF_FG_PAR_TEST_PERIOD_ITERS             ,      // 46  Sampling period in function generator iterations for TEST function.
    REF_FG_PAR_TEST_PERIOD                   ,      // 47  TEST function period.
    REF_FG_PAR_TEST_WINDOW                   ,      // 48  Control of TEST function half-period sine window.
    REF_FG_PAR_TEST_EXP_DECAY                ,      // 49  Control of TEST function exponential amplitude delay.
    REF_FG_PAR_PRBS_INITIAL_REF              ,      // 50  Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    REF_FG_PAR_PRBS_AMPLITUDE_PP             ,      // 51  PRBS peak-peak amplitude.
    REF_FG_PAR_PRBS_PERIOD_ITERS             ,      // 52  PRBS period in function generator iterations.
    REF_FG_PAR_PRBS_NUM_SEQUENCES            ,      // 53  Number of PRBS sequences to play.
    REF_FG_PAR_PRBS_K                        ,      // 54  PRBS K factor. Sequence length is 2^K - 1 periods.
    REF_NUM_FG_PARS,
    REF_FG_PAR_NULL,
    REF_FG_FIRST_TRANSACTIONAL_PAR = REF_FG_PAR_REF_FG_TYPE
};

// EOF
