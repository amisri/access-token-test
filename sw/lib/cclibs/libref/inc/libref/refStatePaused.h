//! @file  refStatePaused.h
//! @brief Converter Control Reference Manager library: PAUSED reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef REF_STATE_PAUSED_H
#define REF_STATE_PAUSED_H

#include "libref.h"

//! Copies of ref manager variables needed to resume paused function after exiting PAUSED state

struct REF_paused_cache
{
    struct REF_event            fg_event;                   //!< Copy of active run function event
};

//! PAUSED state variables structure

struct REF_state_paused
{
    struct REF_paused_cache     cache;                      //!< Cached variables needed for restoration of CYCLING state
    struct CC_ns_time           fg_time_adv;
    cc_float                    ref;
    bool                        ramp_running;
    uint32_t                    init_iter_counter;
};

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize operation in PAUSED state.
//!
//! This is called on entry into the PAUSED reference state to prepare operation of the refStatePausedRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStatePausedRT     Pointer to function for PAUSED state.

RefState * refStatePausedInitRT(struct REF_mgr * const ref_mgr);



//! Operate PAUSED state.
//!
//! This is called every iteration of the reference state machine when the state is PAUSED.
//! This includes the first iteration, following the call to refStatePausedInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_PAUSED     Index of the PAUSED state.

enum REF_state refStatePausedRT(struct REF_mgr * const ref_mgr);

#ifdef __cplusplus
}
#endif

#endif

// EOF
