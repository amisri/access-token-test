//! @file  refCycStatusMgr.h
//! @brief Converter Control Reference Manager library Cycle Status functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once


//! REF_cyc_status structure records information about the last time a cycle for [sub_sel][cyc_sel] was played.
//!
//! The fields are updated at different moments during the cycle as indicated in this table:
//!
//! |         Field        |   NEXT   |  EVENT  | FUNCTION| FINISH  | From BG |
//! |----------------------|----------|---------|---------|---------|---------|
//! | counter              |          |         |         |   YES   |         |
//! | status_bit_mask      |   YES    |   YES   |         |   YES   |         |
//! | fg_type              |   YES    |         |         |         |         |
//! | reg_mode             |   YES    |         |         |         |         |
//! | event_us_time        |   YES    |         |         |         |         |
//! | max_abs_reg_err      |  RESET   |         |   YES   |         |         |
//! | pre_func_plateau_err |  RESET   |   YES   |         |         |         |
//! | ilc                  |  COPIED  |         |         |         |   YES   |
//!
//! Note: counter for sub_sel=cyc_sel=0 will count every cycle played

struct REF_cyc_data
{
    uint32_t                    status_bit_mask;                //!< Cycle status bit mask
    enum FG_type                fg_type;                        //!< Function generator type
    enum REG_mode               reg_mode;                       //!< Regulation mode for last play of this cycle
    struct CC_us_time           event_us_time;                  //!< Absolute event time (s, us) for last play of this cycle
    cc_float                    max_abs_reg_err;                //!< Max absolute regulation error during function
    cc_float                    pre_func_plateau_err;           //!< Pre-function plateau arrival time error (s)
};

struct REF_cyc_status
{
    uint32_t                    counter;                        //!< Count of times this [sub_sel][cyc_sel] was played (Note: [0][0] counts every cycle)
    struct REF_cyc_data         data;                           //!< General cycle data
    struct REF_ilc_cyc_status   ilc;                            //!< ILC cycle data
};

//! Cycle status manager structure

struct REF_cyc_status_mgr
{
    uint32_t                    active_idx;                     //!< Index in cyc_status_buffers[] and archive[] for active
    struct REF_cyc_status     * archive[2];                     //!< Pointers to cycle status archive structure for the active and next
    struct REF_cyc_data         cyc_data_buffers[2];            //!< Active and Next cycle data buffers
    struct REF_ilc_cyc_status * ilc;                            //!< Pointer to ILC cycle data structure to be filled by background after cyc_status has been archived
    cc_float                    err_warning_limit[2];           //!< Active and Next regulation error warning threshold
    cc_float                    err_fault_limit[2];             //!< Active and Next regulation error fault threshold
    bool                        prev_in_cycling_state;          //!< Previous in_cycling_state flag to detect transition away from CYCLING states
};

//! Contains variables related to function generation

#ifdef __cplusplus
extern "C" {
#endif

//! Check REF STATE for transition away from a "CYCLING" state.
//!
//! When a transition away from CYCLING is detected, the function calls refCycStatusMgrFinish().
//!
//! @param[in,out]  ref_mgr              Pointer to reference manager structure.
//!
//! @retval         false             Ref state did not just exit from CYCLING.
//! @retval         true              Ref state just exited from CYCLING.

bool refCycStatusMgrRT(struct REF_mgr * ref_mgr);


//! Prepare the next cyc_status buffer when start function event is processed in the background.
//!
//! @param[in,out]  ref_mgr              Pointer to reference manager structure.

void refCycStatusMgrNext(struct REF_mgr * ref_mgr);


//! Active the next cyc_status buffer when the cycle starts.
//!
//! @param[in,out]  cyc_status_mgr    Pointer to cycle status manager structure.

void refCycStatusMgrActivateRT(struct REF_cyc_status_mgr * cyc_status_mgr);


//! Complete and save the active cyc_status buffer at the end of a cycle and transfer the
//! contents to the cyc_status buffer for the sub_sel and cyc_sel. This is also called
//! with finish_early set to true if the REF state changes so that it is no longer in a cycling state.
//!
//! @param[in,out]  ref_mgr           Pointer to reference manager structure.
//! @param[in]      finish_early      True to set INCOMPLETE in cyc_status status_bit_mask
//!
//! @retval         false             No warnings from the previous cycle.
//! @retval         true              Warnings active from the previous cycle.

bool refCycStatusMgrFinishRT(struct REF_mgr * ref_mgr,
                             bool             finish_early);


//! Set bits in the active cyc_status status_bit_mask
//!
//! @param[in,out]  cyc_status_mgr    Pointer to cycle status manager structure.
//! @param[in]      status_bit_mask   Status bit mask to OR into the active cyc_status status_bit_mask

void refCycStatusMgrSetStatusBitMaskRT(struct REF_cyc_status_mgr * cyc_status_mgr,
                                       uint32_t                    status_bit_mask);


//! Save the pre-function last plateau time error (%) in the active cyc_status.
//!
//! @param[in,out]  cyc_status_mgr         Pointer to cycle status manager structure.
//! @param[in]      pre_func_plateau_err   Pre-function final plateau time error (%) to save in the active cyc_status status_bit_mask.

void refCycStatusMgrSetPreFuncPlateaurErrRT(struct REF_cyc_status_mgr * cyc_status_mgr,
                                            cc_float                    pre_func_plateau_err);


//! Save the max absolute regulation error during the function in the active cyc_status.
//!
//! @param[in,out]  cyc_status_mgr         Pointer to cycle status manager structure.
//! @param[in]      max_abs_reg_err        Max absolute regulation error during the function.

void refCycStatusMgrSetMaxAbsRegErrRT(struct REF_cyc_status_mgr * cyc_status_mgr,
                                      cc_float                    max_abs_reg_err);


//! Save the ILC cycle status data.
//!
//! This is special because it is completed by the background processing after the cycle has finished.
//!
//! @param[in,out]  ref_mgr                Pointer to reference manager structure.
//! @param[in]      ilc                    Pointer to ILC cycle data

void refCycStatusMgrSetIlc(struct REF_mgr            * ref_mgr,
                           struct REF_ilc_cyc_status * ilc);

#ifdef __cplusplus
}

#endif
// EOF
