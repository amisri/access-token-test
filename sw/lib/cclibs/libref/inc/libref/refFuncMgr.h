//! @file  refFuncMgr.h
//! @brief Converter Control Reference Manager library function generation functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

//! Contains variables related to function generation

struct REF_func_mgr
{
    union FG_pars            * pars;             //!< Pointer to active function generation parameters
    struct CC_ns_time          time;             //!< Function generation time
    cc_float                   time_fp32;        //!< Function generation time as a float for logging
    struct CC_ns_time          time_adv;         //!< Function generation time with ref advance
    cc_float                   time_adv_fp32;    //!< Function generation time with ref advance as a float for logging
    cc_double                  time_adv_fp64;    //!< Function generation time with ref advance as a double for libfg
    struct REF_event           event;            //!< Active function generation fg_event
    enum FG_status             status;           //!< Function generation status
    uint8_t                    internal_evt;     //!< Internal event: active function generation event update flag (2-bit down-shifter to extend to two iterations)
    uint8_t                    start_func;       //!< Start Function event flag (2-bit down-shifter to extend to two iterations)
    uint8_t                    use_arm_now;      //!< Use Arm Now event flag (2-bit down-shifter to extend to two iterations)
};

// Static inline functions

static inline void refFuncMgrSetFlagEventUpdated(struct REF_func_mgr * const fg)
{
    fg->internal_evt = 3;
}



static inline void refFuncMgrSetFlagStartFunc(struct REF_func_mgr * const fg)
{
    fg->start_func = 3;
}



static inline void refFuncMgrSetFlagUseArmNow(struct REF_func_mgr * const fg)
{
    fg->use_arm_now = 3;
}


#ifdef __cplusplus
extern "C" {
#endif

//! Updates function generation event structure
//!
//! @param[out]  fg               Pointer to structure to update
//! @param[in]   new_event        Pointer to the new event or NULL

void refFuncMgrUpdateEventRT(struct REF_func_mgr       * fg,
                             struct REF_event    const * new_event);


//! Updates only the time of function generation event structure
//!
//! @param[out]  fg                   Pointer to structure to update
//! @param[in]   new_fg_event_time    New event time structure (s,us) passed by value

void refFuncMgrUpdateEventTimeRT(struct REF_func_mgr * fg,
                                 struct CC_ns_time     new_fg_event_time);


//! Updates function generation parameters
//!
//! @param[out]  fg           Pointer to structure to update
//! @param[in]   new_fg_pars  New function generation parameters

void refFuncMgrUpdateParsRT(struct REF_func_mgr * fg,
                            union  FG_pars      * new_fg_pars);


//! Resets function generation event time and arms NONE using provided parameters
//!
//! @param[out]     fg         Pointer to structure to update
//! @param[in,out]  fg_pars    Pointer to function parameters than will be used to arm NONE

void refFuncMgrResetRT(struct REF_func_mgr * fg,
                       union  FG_pars      * fg_pars);


//! Calculates function time and function time with ref advance
//!
//! Generally, the function time is calculated with respect to the active
//! function generation event if it's present. We have to make an exception for
//! PAUSED, however, where the function time may be frozen, unless we are connecting
//! two reference levels with the ramp manager.
//!
//! @param[in,out]  ref_mgr  Pointer to reference manager structure

void refFuncMgrSetFuncTimeRT(struct REF_mgr * ref_mgr);


//! Sets fg reference from function generator or external ref
//!
//! @param[in,out]  ref_mgr  Pointer to reference manager structure

void refFuncMgrSetRefRT(struct REF_mgr * ref_mgr);


//! Set or shift right the function manager flags that are logged
//!
//! This allows flags to be stretched in time, to ensure that they will be captured in the log.
//!
//! @param[in,out]  ref_mgr  Pointer to reference manager structure

void refFuncMgrShiftFlagsRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
