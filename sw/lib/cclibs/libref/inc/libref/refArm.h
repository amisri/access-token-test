//! @file  refArm.h
//! @brief Converter Control Reference Manager library reference arming functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! Declare parameter arming structures

struct REF_armed
{
    enum  REG_mode         reg_mode;                                //!< Regulation mode
    union FG_pars          fg_pars;                                 //!< Function generation parameters
};


#ifdef __cplusplus
extern "C" {
#endif

//! Initialize the parameter group masks in ref_mgr.
//!
//! The parameters used to arm reference functions are divided into groups, based on the libfg initialization
//! functions, e.g. PLEP, TABLE, TRIM, TEST. Libref supports a 2D array of these parameter
//! with dimensions [sub_sel] and [cyc_sel]. The application can choose to instantiate these dimensions
//! or not for each parameter and can inform libref using the sub_sel_step and cyc_sel_step.
//! This function will check the sub_sel_step and cyc_sel_step for each parameter in each group and
//! will create a mask indicating if the application supports the [sub_sel] and/or [cyc_sel]
//! dimensions. This is checked by refArm() when arming a reference function and allows it to
//! reject invalid sub_sel and/or cyc_sel values.
//!
//! @param[in]  ref_mgr              Pointer to reference manager structure.

void refArmInitParGroupMasks(struct REF_mgr * ref_mgr);


//! Clip reference.
//!
//! This function will clip ref if the active regulation mode is unipolar and the reference is
//! below +/-lim_ref->standby.
//!
//! @param[in]  ref_mgr              Pointer to reference manager structure.
//! @param[in]  ref                  Reference value.
//!
//! @retval     clipped_ref          ref clipped to +/-lim_ref->standby if limits are unipolar.

cc_float refArmClipRefRT(const struct REF_mgr * ref_mgr, cc_float ref);


//! Arm reference function.
//!
//! This function should be called to attempt to arm the reference for the specified sub-device and cycle selectors.
//! The function type should be defined in the parameter REF FUNC_TYPE and the regulation mode if sub_sel and cyc_sel are both zero
//! should be in MODE REG_MODE, otherwise it will be taken from MODE REG_MODE_CYC.
//! The parameters defining the function will depend upon the type of function.
//! The function will put status information in ref_mgr->fg_error[sub_sel][cyc_sel] and it returns the fg_errno.
//!
//! In case of success, the armed function parameters will be in ref_mgr->armed[sub_sel][cyc_sel]. This will start
//! with the meta data.
//!
//! If a TABLE function is successfully armed and MODE USE_ARM_NOW is enabled, then then a USE_ARM_NOW event will be generated.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.
//! @param[in]     sub_sel           Sub-device selector.
//! @param[in]     cyc_sel           Cycle selector.
//! @param[in]     test_arm          ENABLED:Test arm, DISABLED:Real arm.
//! @param[in]     num_pars          Number of parameters in par_values.
//! @param[in]     par_values        Optional parameter values array (NaN means use the default value).
//! @param[in,out] fg_pars_idx       Optional pointer to array of REF_FG_PAR_MAX_PER_GROUP elements to receive zero-terminated
//!                                  list of parameter indexes that were used to arm or restore the function.
//!
//! @retval     fg_errno             FG error number.

enum FG_errno refArm(struct REF_mgr         * ref_mgr,
                     uint32_t                 sub_sel,
                     uint32_t                 cyc_sel,
                     enum CC_enabled_disabled test_arm,
                     uint32_t                 num_pars,
                     cc_float         const * par_values,
                     enum REF_fg_par_idx      fg_pars_idx[REF_FG_PAR_MAX_PER_GROUP]);


//! Disarm non-cycling reference function.
//!
//! This function will do nothing unless the reference state is ARMED.
//! When ARMED, it will disarm the non-cyclic reference function.
//!
//! @param[in,out] ref_mgr           Pointer to reference manager structure.

void refArmDisarm(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
