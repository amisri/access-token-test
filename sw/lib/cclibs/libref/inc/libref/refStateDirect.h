//! @file  refStateDirect.h
//! @brief Converter Control Reference Manager library: DIRECT reference state functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

//! DIRECT state

enum REF_direct_state
{
    REF_DIRECT_IDLE    ,
    REF_DIRECT_RUNNING ,
    REF_NUM_DIRECT_STATES
};

//! DIRECT state structure

struct REF_state_direct
{
    cc_float                 prev_clipped_final_ref;
    cc_float                 direct_ref;
    cc_float                 prev_direct_ref;
    cc_float                 pre_func_ref;
    uint32_t                 pre_func_ramp_index;
    enum REF_pre_func_mode   pre_func_type;
    enum FG_status           degauss;
    enum REF_direct_state    state;
    enum REG_mode            prev_mode_reg_mode;
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Set REF DIRECT_STATE variable.
//!
//! REF DIRECT_STATE will be REF_DIRECT_RAMPING when REF_STATE is DIRECT and the ramp manager is ramping,
//! otherwise it will be set to REF_IDLE.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.

void refStateDirectSetRT(struct REF_mgr * ref_mgr);


//! Initialize operation in DIRECT state.
//!
//! This is called on entry into the DIRECT reference state to prepare operation of the refStateDirectRT() function.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval refStateDirectRT     Pointer to function for DIRECT state.

RefState * refStateDirectInitRT(struct REF_mgr * ref_mgr);


//! Operate DIRECT state.
//!
//! This is called every iteration of the reference state machine when the state is DIRECT.
//! This includes the first iteration, following the call to refStateDirectInitRT(), during
//! which ref_mgr->ref_state still has the index of the previous state.
//!
//! @param[in,out]     ref_mgr              Pointer to reference manager structure.
//!
//! @retval REF_DIRECT     Index of the DIRECT state.

enum REF_state refStateDirectRT(struct REF_mgr * ref_mgr);

#ifdef __cplusplus
}
#endif

// EOF
