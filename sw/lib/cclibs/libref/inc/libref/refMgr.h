//! @file  refMgr.h
//! @brief Converter Control Reference Manager library ref_mgr functions header file
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libref.
//!
//! libref is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include "libref.h"

// Constants

#define REF_TIME_NOT_SET            0xFFFFFFFF      //!< Constant to signal the event time is not set

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize libref
//!
//! The function should be called to initialize the ref_mgr structure. The application should
//! know the number of cycle selectors (num_cyc_sels) and the number of sub-devices (num_sub_sels) that need to be supported.
//! The library does not need to know num_sub_sels but it will be needed by the application to dimension the fg_error
//! and ref_armed arrays. The application is free to limit tables to sub_sel zero, which will reduce the size of the
//! table_function array.
//!
//! @param[in,out]  ref_mgr                     Pointer to reference manager structure.
//! @param[in]      polswitch_mgr               Pointer to polarity switch manager structure.
//! @param[in]      reg_mgr                     Pointer to regulation manager structure.
//! @param[in]      mutex                       Pointer to optional mutex to protect refArm and refEvent functions.
//!                                             If set, reg_mgr->mutex_locked and reg_mgr->mutex_unlocked must be set.
//! @param[in]      fg_error                    Pointer to memory to hold FG_error structures. It should be at least
//!                                             sizeof(struct FG_error) * num_cyc_sels * num_sub_sels bytes in length.
//! @param[in]      cyc_status                  Pointer to memory to hold REF_cyc_status structures. It should be
//!                                             sizeof(struct REF_cyc_status) * num_cyc_sels * num_sub_sels bytes in length.
//! @param[in]      ref_armed                   Pointer to memory to hold REF_armed structures. It should be
//!                                             sizeof(struct REF_armed) * num_cyc_sels * num_sub_sels bytes in length.
//! @param[in]      armed_table_function        Pointer to memory to hold armed table_function arrays. It should be
//!                                             REF_ARMED_TABLE_FUNCTION_LEN(NUM_CYC_SELS, NUM_SUB_SELS, NUM_TABLE_POINTS) bytes in length.
//! @param[in]      running_table_function      Pointer to memory to hold triple-buffered running table_function arrays. It should be
//!                                             REF_RUNNING_TABLE_FUNCTION_LEN(NUM_POINTS) bytes in length.
//! @param[in]      ilc_cyc_data_store          Pointer to the array of ILC cyc_data_store structures in slow memory.
//! @param[in]      num_cyc_sels                Number of cycle selectors to support. cyc_sel zero will be used for IDLE/ARMED/RUNNING.
//! @param[in]      max_table_points            Maximum number of points in table functions.
//! @param[in]      max_ilc_samples             Length in elements of the ILC ref buffer.
//! @param[in]      ref_to_tc_limit             Limit on consecutive TO_OFF to TO_CYCLING transitions.
//!
//! @retval         0                           All parameter pointers are set and libreg successfully initialized.
//! @retval        -1                           mutex is defined but either reg_mgr->mutex_lock or reg_mgr->mutex_unlock are NULL
//! @retval         par_num                     Number of parameter if its pointer is still NULL (1-N). Consult refPars.h to identify the parameter.

int32_t refMgrInit(struct REF_mgr          * ref_mgr,
                   struct POLSWITCH_mgr    * polswitch_mgr,
                   struct REG_mgr          * reg_mgr,
                   void                    * mutex,
                   struct FG_error         * fg_error,
                   struct REF_cyc_status   * cyc_status,
                   struct REF_armed        * ref_armed,
                   struct FG_point         * armed_table_function,
                   struct FG_point         * running_table_function,
                   struct REF_ilc_cyc_data * ilc_cyc_data_store,
                   uint32_t                  num_cyc_sels,
                   uint32_t                  max_table_points,
                   uint32_t                  max_ilc_samples,
                   uint32_t                  ref_to_tc_limit);


//! Reset libref faults
//!
//! The function be called to clear the libref latched faults.
//!
//! @param[in,out]  ref_mgr              Pointer to reference manager structure.

void refMgrResetFaultsRT(struct REF_mgr * ref_mgr);


//! Latch the unfiltered measurement used for regulation.
//!
//! This function will latch the unfiltered measurement used for regulation when the latch flag transitions from false to true.
//! The values are latched in the location provided by the calling function.
//!
//! @param[in,out]  ref_mgr           Pointer to the reference manager structure.
//! @param[in]      meas              Pointer to the float variable to use to latch the regulated unfiltered measurement.
//! @param[in]      meas_index        0 for first measurement and 1 for the second measurement (>1 is treated as 1).
//! @param[in]      latch             Set to true when regulated measurement should be latched.
//!
//! @retval         false             The measurement was not latched on this call to the function.
//! @retval         true              The measurement was latched on this call to the function.

bool refMgrLatchUnfltrMeasRT(struct REF_mgr * ref_mgr,
                             cc_float       * meas,
                             uint32_t         meas_index,
                             bool             latch);


//! Set the regulation mode.
//!
//! This function does nothing if the requested mode matches the actual mode, or if the
//! requested mode (FIELD or CURRENT) is not enabled (based on LIMITS POS and NEG).
//!
//! Otherwise, the regulation mode is changed and all the relevant regulation variables are adjusted.
//!
//! The reference is adjusted and is available in *reg_mgr->ref.
//!
//! This is a Real-Time function. Note that reading and resetting the flags is not an atomic
//! operation, so it is assumed that this function will be called from one thread only.
//!
//! @param[in,out] ref_mgr              Pointer to reference manager structure.
//! @param[in]     reg_mode             Regulation mode to set (REG_NONE, REG_VOLTAGE, REG_CURRENT or REG_FIELD).
//! @param[in]     use_average_v_ref    If reg_mode is set to REG_VOLTAGE then this flag controls how the voltage reference
//!                                     is initialized. If true, v_ref will be set to the average v_ref in the RST history.
//!                                     If false, it will be set to the most recent v_ref.

cc_float refMgrModeSetRT(struct REF_mgr * ref_mgr, enum REG_mode reg_mode, bool use_average_v_ref);


//! Return the initial reference rate for a ramp.
//!
//! The function will always return zero if the reg_mode is VOLTAGE.
//! For CURRENT and FIELD, it will return the actual reference clipped to the reference rate limit.
//! This protects against a overly large value of the rate following an instability.
//!
//! @param[in,out]  ref_mgr              Pointer to reference manager structure.
//!
//! @return Initial rate for a ramp based on the actual rate

cc_float refMgrInitialRate(struct REF_mgr * ref_mgr);


//! Set feed forward voltage signal. The signal is passed through a gain and a delay before being used.
//! The function should be called when each new sample is available, giving the timestamp for the sample.
//!
//! This is a Real-Time function.
//!
//! @param[in,out] ref_mgr                   Pointer to the reference manager structure.
//! @param[in]     vfeedfwd                  VFEEDFWD signal and validity.
//! @param[in]     timestamp                 UTC time stamp in seconds for the sample.

void refMgrSetVfeedfwdRT(struct REF_mgr * ref_mgr, struct CC_meas_signal vfeedfwd, struct CC_ns_time timestamp);


//! Get interpolated feed forward voltage signal. The time comes from ref_mgr and is the time of the current iteration.
//!
//! This is a Real-Time function.
//!
//! @param[in]     ref_mgr                   Pointer to the reference manager structure.
//! @param[out]    v_ff                      Pointer to V_FF signal structure to receive the interpolated VFEEDFWD signal.

void refMgrGetVffRT(struct REF_mgr * ref_mgr, struct CC_meas_signal * v_ff);


//! Calculate Pointer to libref variable.
//!
//! Libref manages three 2D arrays of structures with dimensions [sub_sel][cyc_sel]:
//!
//! ref_mgr->fg_error
//! ref_mgr->ref_status
//! ref_mgr->ref_armed
//!
//! The memory for these arrays is provided by the application. The number of cyc_sel elements isn't known at compile time
//! so this function is needed to calculate the address of a variable for the specified sub_sel and cyc_sel. If the
//! variable is not in these structure arrays then the function offers an optional service to take into account the
//! sub_sel_step and cyc_sel_step, and will return the pointer with the offset
//!      sub_sel * sub_sel_step + cyc_sel * cyc_sel_step.
//!
//! This is a convenience for the calling application and is not strictly needed by the library.
//!
//! @param[in]      ref_mgr              Pointer to reference manager structure.
//! @param[in]      base_address         Pointer to base address for variable.
//! @param[in]      sub_sel              Sub-device selector.
//! @param[in]      cyc_sel              Cycle selector.
//! @param[in]      sub_sel_step         Sub-device selector step in bytes.
//! @param[in]      cyc_sel_step         Cycle selector step in bytes.
//! @param[out]     var_pointer          Pointer to pointer to variable
//!
//! @retval         true     if variable is in fg_error, ref_status or ref_armed.
//! @retval         false    otherwise.

bool refMgrVarPointerFunc(struct REF_mgr * ref_mgr,
                          uint8_t        * base_address,
                          uint32_t         sub_sel,
                          uint32_t         cyc_sel,
                          uint32_t         sub_sel_step,
                          uint32_t         cyc_sel_step,
                          uint8_t       ** var_pointer);


//! Return pointer to function generation parameter element in a 2D array with dimensions [sub_sel][cyc_sel]
//!
//! This function is in fact a helper for the function macros refMgrFgParPointerSubCyc() and
//! refMgrFgParValueSubCyc(), which are defined in refPars.h
//!
//! @param[in]      ref_mgr              Pointer to reference manager structure.
//! @param[in]      sub_sel              Sub-device selector.
//! @param[in]      cyc_sel              Cycle selector.
//! @param[in]      fg_par_idx           Function Generation parameter index as defined in refPars.h
//!
//! @return         Pointer to the parameter

char * refMgrFgParPointerSubCycFunc(struct REF_mgr    * ref_mgr,
                                    uint32_t            sub_sel,
                                    uint32_t            cyc_sel,
                                    enum REF_fg_par_idx fg_par_idx);

#ifdef __cplusplus
}
#endif

// EOF
