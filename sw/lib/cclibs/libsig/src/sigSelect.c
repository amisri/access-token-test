//! @file  sigSelect.c
//! @brief Converter Control Signals library signal selection functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libsig.h"

// Constants

#define SIG_SEL_DIFF_FLT_ITERS          3           //!< Number of iterations before a diff fault causes fault flag to be set


//! Combine the mean and peak_to_peak values for two averages
//!
//! @param[in]    average_select           Pointer to average structure for the selector
//! @param[in]    average_a                Pointer to average structure for transducer A
//! @param[in]    average_b                Pointer to average structure for transducer B

static inline void sigSelectAverageAB(struct SIG_meas_average       * const average_select,
                                      struct SIG_meas_average const * const average_a,
                                      struct SIG_meas_average const * const average_b)
{
    // Set to the mean and peak_to_peak from two averages

    average_select->mean         = 0.5F * (average_a->mean         + average_b->mean        );
    average_select->peak_to_peak = 0.5F * (average_a->peak_to_peak + average_b->peak_to_peak);
}


// NON-RT   sigSelect

void sigSelect(struct SIG_mgr * const sig_mgr, struct SIG_select * const select, bool const start_average2)
{
    struct SIG_transducer const * const transducer_a = &sig_mgr->transducers[select->transducer_a_index];
    struct SIG_transducer const * const transducer_b = &sig_mgr->transducers[select->transducer_b_index];

    // Act according to the actual selector from the previous call to sigSelectRT()

    switch(select->actual_selector)
    {
        case SIG_A:

            select->meas.average1 = transducer_a->meas.average1;
            select->meas.average2 = transducer_a->meas.average2;
            break;

        case SIG_B:

            select->meas.average1 = transducer_b->meas.average1;
            select->meas.average2 = transducer_b->meas.average2;
            break;

        case SIG_AB:

            // Take the mean of average1 mean and peak_to_peak from the two transducers

            sigSelectAverageAB(&select->meas.average1, &transducer_a->meas.average1, &transducer_b->meas.average1);

            // Take the mean of average2 mean and peak_to_peak from the two transducers when it has been recalculated

            if(start_average2 == true)
            {
                sigSelectAverageAB(&select->meas.average2, &transducer_a->meas.average2, &transducer_b->meas.average2);
            }

            break;

        case SIG_NONE:

            // Neither signal is available: reset average1 and average2 mean and peak_to_peak

            select->meas.average1.mean         = 0.0F;
            select->meas.average1.peak_to_peak = 0.0F;
            select->meas.average2.mean         = 0.0F;
            select->meas.average2.peak_to_peak = 0.0F;
            break;

        default: break;
    }

    // Set diff_fault limit from the warn limit if the diff_fault_warn_ratio is non-zero

    if(select->diff_fault_warn_ratio > 0.0F)
    {
        select->diff_fault_limit = select->diff_warn_limit * select->diff_fault_warn_ratio;
    }
}



void sigSelectRT(struct SIG_mgr * const sig_mgr, struct SIG_select * const select)
{
    uint32_t          sig_warnings = 0;
    cc_float          abs_diff     = 0.0F;
    struct SIG_status status       = { .faults   = 0,
                                       .warnings = 0 };

    struct SIG_transducer const * transducer_a = &sig_mgr->transducers[select->transducer_a_index];
    struct SIG_transducer const * transducer_b = &sig_mgr->transducers[select->transducer_b_index];

    bool const meas_a_is_valid = transducer_a->meas.unfiltered.is_valid;
    bool const meas_b_is_valid = transducer_b->meas.unfiltered.is_valid;

    // Set actual selector to user selector by default

    enum SIG_selector actual_selector = select->selector;

    switch(select->selector)
    {
        case SIG_A:

            if(transducer_a->meas.status.faults == SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK)
            {
                status.faults = SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK;
            }
            else if(meas_a_is_valid == true)
            {
                sig_warnings = transducer_a->meas.status.warnings;

                if(transducer_a->adc_calibrating_flag == true)
                {
                    actual_selector = SIG_NONE;
                }
            }
            else
            {
                status.faults = SIG_SELECT_SIG_FLT_BIT_MASK;
            }

            break;

        case SIG_B:

            if(transducer_b->meas.status.faults == SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK)
            {
                status.faults = SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK;
            }
            else if(meas_b_is_valid == true)
            {
                sig_warnings = transducer_b->meas.status.warnings;

                if(transducer_b->adc_calibrating_flag == true)
                {
                    actual_selector = SIG_NONE;
                }
            }
            else
            {
                status.faults = SIG_SELECT_SIG_FLT_BIT_MASK;
            }

            break;

        case SIG_AB:

            if(   transducer_a->meas.status.faults == SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK
               && transducer_b->meas.status.faults == SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK)
            {
                status.faults = SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK;
            }
            else if(meas_a_is_valid == false && meas_b_is_valid == false)
            {
                status.faults = SIG_SELECT_SIG_FLT_BIT_MASK;
            }
             else
            {
                // At least one signal is valid (but both may be calibrating)

                bool sig_a_not_avl = (meas_a_is_valid == false || transducer_a->adc_calibrating_flag == true);
                bool sig_b_not_avl = (meas_b_is_valid == false || transducer_b->adc_calibrating_flag == true);

                //  Merge the warnings from the input signals

                sig_warnings = transducer_a->meas.status.warnings | transducer_b->meas.status.warnings;

                if(sig_a_not_avl == true && sig_b_not_avl == true)
                {
                    // Neither signal is available so freeze last measurement

                    actual_selector = SIG_NONE;
                }
                else if(sig_a_not_avl == true)
                {
                    // Channel A is not available so use channel B

                    actual_selector = SIG_B;

                    if(meas_a_is_valid == false)
                    {
                        status.warnings = SIG_SELECT_SIG_INVALID_WARN_BIT_MASK;
                    }

                }
                else if(sig_b_not_avl == true)
                {
                    // Channel B is not available so use channel A

                    actual_selector = SIG_A;

                    if(meas_b_is_valid == false)
                    {
                        status.warnings = SIG_SELECT_SIG_INVALID_WARN_BIT_MASK;
                    }
                }
            }

            break;

        default:

            // The user selector should never be anything other than SIG_A, SIG_B or SIG_AB

            CC_ASSERT(false);
            break;
    }

    // Save the updated actual_selector for use by the background function sigSelect()

    select->actual_selector = actual_selector;

    // Act according to the actual selector

    switch(actual_selector)
    {
        case SIG_A:

            select->meas.unfiltered = transducer_a->meas.unfiltered;
            select->meas.filtered   = transducer_a->meas.filtered;
            break;

        case SIG_B:

            select->meas.unfiltered = transducer_b->meas.unfiltered;
            select->meas.filtered   = transducer_b->meas.filtered;
            break;

        case SIG_AB:

            // Measured signal is average of signals from Transducers A and B

            select->meas.unfiltered.signal = 0.5F * (transducer_a->meas.unfiltered.signal + transducer_b->meas.unfiltered.signal);
            select->meas.filtered.signal   = 0.5F * (transducer_a->meas.filtered.signal   + transducer_b->meas.filtered.signal  );

            // Calculate difference between signals and check against fault and warning limits

            abs_diff = fabsf(transducer_a->meas.unfiltered.signal - transducer_b->meas.unfiltered.signal);

            if(status.faults == 0)
            {
                // SIG_FLT is not active - both channel must be valid

                if(select->diff_fault_limit > 0.0F && abs_diff > select->diff_fault_limit)
                {
                    // Diff limit exceeded so immediately mark signal as invalid

                    select->meas.unfiltered.is_valid = false;
                    select->meas.filtered.is_valid   = false;

                    // Delay setting DIFF_FLT in case it's just a short glitch

                    if(select->diff_fault_counter < SIG_SEL_DIFF_FLT_ITERS)
                    {
                        select->diff_fault_counter++;
                        status.warnings |= SIG_SELECT_DIFF_WARN_BIT_MASK;
                    }
                    else
                    {
                        status.faults   = SIG_SELECT_DIFF_FLT_BIT_MASK;
                        status.warnings = 0;
                    }
                }
                else
                {
                    // No diff fault so mark output signal as valid and reset diff fault counter

                    select->meas.unfiltered.is_valid = true;
                    select->diff_fault_counter       = 0;

                    if(select->diff_warn_limit > 0.0F && abs_diff > select->diff_warn_limit)
                    {
                        // Difference exceeds warning level to set warning flag

                        status.warnings |= SIG_SELECT_DIFF_WARN_BIT_MASK;
                    }

                    // Set filter validity based on the filtered transducer signal validity because
                    // it will be invalid for the length of the FIR filter after the unfiltered validity becomes good again

                    select->meas.filtered.is_valid = transducer_a->meas.filtered.is_valid && transducer_b->meas.filtered.is_valid;
                }
            }
            else
            {
                // SIG_FLT is active so mark the selector's measurement as invalid

                select->meas.unfiltered.is_valid = false;
                select->meas.filtered.is_valid   = false;
            }

            break;

        case SIG_NONE:

            // No input signals available so freeze last valid measurement

            break;

        default: break;
    }

    // Convert absolute difference to milli units and store as signed 16-bit integer

    select->abs_diff       = abs_diff;
    select->milli_abs_diff = ccMilliShort(abs_diff);

    // If any input signal warnings are present then set the SIG_WARN warning

    if(sig_warnings != 0)
    {
        status.warnings |= SIG_SELECT_SIG_WARN_WARN_BIT_MASK;
    }

    select->meas.status = status;
}

// EOF
