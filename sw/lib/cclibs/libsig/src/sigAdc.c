//! @file  sigAdc.c
//! @brief Converter Control Signals library ADC functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <limits.h>

#include "libsig.h"

// ADC related constants

static uint32_t const SIG_ADC_CIRC_BUF_MASK        = (SIG_ADC_CIRC_BUF_LEN-1);      //!< ADC circular buffer length mask
static uint32_t const SIG_ADC_CIRC_BUF_EMPTY       = UINT32_MAX;                    //!< readout_sample_index when empty
static int32_t  const SIG_ADC_RAW_OFFSET_THRESHOLD = 100000;                        //!< Limit for identification of cal level


// NON-RT   sigAdcOff

void sigAdcOff(struct SIG_adc * const adc)
{
    // ADC is off so reset ADC status, measurements and calibrating counter

    adc->meas.status.faults         = 0;
    adc->meas.status.warnings       = 0;

    adc->meas.unfiltered.signal     = 0.0F;
    adc->meas.unfiltered.is_valid   = false;

    adc->meas.filtered.signal       = 0.0F;
    adc->meas.filtered.is_valid     = false;

    adc->meas.average1.mean         = 0.0F;
    adc->meas.average1.peak_to_peak = 0.0F;

    adc->meas.average2.mean         = 0.0F;
    adc->meas.average2.peak_to_peak = 0.0F;

    adc->calibrating_counter = 0;
}


// NON-RT   sigAdcGetCircBuf

bool sigAdcGetCircBuf(struct SIG_adc * const adc, struct SIG_adc_raw * const adc_raw)
{
    uint32_t readout_sample_index = adc->circ_buf.readout_sample_index;
    uint32_t last_sample_index    = adc->circ_buf.last_sample_index;

    // If FIFO has overrun, it will be empty

    if(readout_sample_index == SIG_ADC_CIRC_BUF_EMPTY)
    {
        // Reset FIFO readout and return no data

        adc->circ_buf.readout_sample_index = last_sample_index;

        return false;
    }

    // If no records are pending then return false

    if(readout_sample_index == last_sample_index)
    {
        return false;
    }

    // Return next sample

    readout_sample_index = (readout_sample_index + 1) & SIG_ADC_CIRC_BUF_MASK;

    *adc_raw = adc->circ_buf.buffer[readout_sample_index];

    adc->circ_buf.readout_sample_index = readout_sample_index;

    return true;
}


// NON-RT   sigAdcCalAverage

bool sigAdcCalAverage(struct SIG_adc * const adc, int32_t * const average_raw)
{
    cc_double                  accummulator    = 0.0;
    struct SIG_adc_num_samples num_samples     = { 0, 0 };
    uint32_t                   cal_num_samples = adc->cal_num_samples_p == NULL ? adc->cal_num_samples : *adc->cal_num_samples_p;

    // Calculate the average of the most recent average1 values from the circular buffer

    uint32_t cal_average_index = adc->circ_buf.last_sample_index;

    while(num_samples.valid < cal_num_samples && num_samples.invalid <= adc->max_invalid_samples)
    {
        uint32_t period_num_valid_samples =  adc->circ_buf.buffer[cal_average_index].num_samples.valid;

        accummulator        += (cc_double)adc->circ_buf.buffer[cal_average_index].meas * (cc_double)period_num_valid_samples;
        num_samples.valid   += period_num_valid_samples;
        num_samples.invalid += adc->circ_buf.buffer[cal_average_index].num_samples.invalid;

        cal_average_index   = (cal_average_index - 1) & SIG_ADC_CIRC_BUF_MASK;
    }

    // Calculate average only if the number of invalid samples is below the limit

    if(num_samples.valid > 0 && num_samples.invalid <= adc->max_invalid_samples)
    {
        *average_raw = (int32_t)(accummulator / (cc_double)num_samples.valid);
        return true;
    }

    // Return false if a fault detected

    return false;
}


// NON-RT   sigAdcCalcCalErr

static void sigAdcCalcCalErr(struct SIG_mgr   * const sig_mgr,
                             struct SIG_adc   * const adc,
                             enum SIG_cal_level const cal_level,
                             int32_t                  cal_average_raw)
{
    struct SIG_cal * const adc_cal = &adc->cal;

    if(cal_level == SIG_CAL_OFFSET)
    {
        // Calculate offset error normalized to T0

        adc_cal->event.s.offset_ppm = (cc_float)cal_average_raw * 1.0E6F / (cc_float)adc->nominal_gain
                                    - sigCalRefErrorPpm(sig_mgr, adc_cal, SIG_CAL_OFFSET)
                                    - sigTempErrorPpm  (sig_mgr, adc_cal, SIG_CAL_OFFSET);
    }
    else
    {
        // Calculate positive or negative gain error normalized to T0

        cc_double vref_err_ppm = sigCalRefErrorPpm(sig_mgr, adc_cal, cal_level);
        cc_double offset_ppm   = adc_cal->event.s.offset_ppm + sigTempErrorPpm(sig_mgr, adc_cal, SIG_CAL_OFFSET);
        cc_double average_gain = (cc_double)cal_average_raw / (cc_double)adc->nominal_gain;

        if(cal_level == SIG_CAL_POSITIVE)
        {
            adc_cal->event.s.gain_err_pos_ppm = (( average_gain - 1.0) * 1.0E6 - offset_ppm - vref_err_ppm) / (1.0 + vref_err_ppm * 1.0E-6)
                                              - sigTempErrorPpm(sig_mgr, adc_cal, SIG_CAL_POSITIVE);
        }
        else
        {
            adc_cal->event.s.gain_err_neg_ppm = ((-average_gain - 1.0) * 1.0E6 + offset_ppm - vref_err_ppm) / (1.0 + vref_err_ppm * 1.0E-6)
                                              - sigTempErrorPpm(sig_mgr, adc_cal, SIG_CAL_NEGATIVE);
        }
    }
}


// NON-RT   sigAdcCal

enum SIG_cal_level sigAdcCal(struct SIG_mgr * const sig_mgr, struct SIG_adc * const adc)
{
    int32_t average_raw;
    enum SIG_cal_level cal_level;
    struct SIG_cal * const adc_cal = &adc->cal;

    // If calibration average from circular buffer is invalid then return immediately

    if(sigAdcCalAverage(adc, &average_raw) == false)
    {
        return SIG_CAL_FAULT;
    }

    // Identify calibration level

    if(average_raw > SIG_ADC_RAW_OFFSET_THRESHOLD)
    {
        cal_level = SIG_CAL_POSITIVE;
    }
    else if(average_raw < -SIG_ADC_RAW_OFFSET_THRESHOLD)
    {
        cal_level = SIG_CAL_NEGATIVE;
    }
    else
    {
        cal_level = SIG_CAL_OFFSET;
    }

    // Save average raw value for this calibration level - this is stored for all levels for debugging. Only the CAL_OFFSET
    // value is actually used in libsig for the adc->nominal_gain calculation below.

    adc->cal_average_raw[cal_level] = average_raw;

    // Check if nominal gain is unset

    if(adc->nominal_gain == 0)
    {
        // If calibrating positive or negative gain then we can calculate the nominal gain, otherwise do nothing

        if(cal_level == SIG_CAL_OFFSET)
        {
            return SIG_CAL_OFFSET;
        }

        adc->nominal_gain = (uint32_t)(fabs((cc_double)(average_raw - adc->cal_average_raw[SIG_CAL_OFFSET]))
                                       / (1.0F + sigCalRefErrorPpm(sig_mgr, adc_cal, cal_level)
                                               + sigTempErrorPpm  (sig_mgr, adc_cal, cal_level)));

        // Re-calculate offset error normalized to T0, because nominal gain has changed

        sigAdcCalcCalErr(sig_mgr, adc, SIG_CAL_OFFSET, adc->cal_average_raw[SIG_CAL_OFFSET]);
    }

    // Calculate calibration error normalized to T0

    sigAdcCalcCalErr(sig_mgr, adc, cal_level, average_raw);

    // Save date/time stamp

    sigCalEventSetUnixtime(&adc_cal->event.s, adc->average1.raw.timestamp.secs.abs);

    // Save temperature if available

    if(adc->cal.temp_filter_index != SIG_NOT_IN_USE)
    {
        adc_cal->event.s.temp_c = sig_mgr->temp_filters[adc_cal->temp_filter_index].temp_c;
    }

    return cal_level;
}


// NON-RT   sigAdcAverage

static void sigAdcAverage(struct SIG_adc_average  * const adc_average,
                          struct SIG_cal_factors  * const cal_factors,
                          struct SIG_meas_average * const meas_average)
{
    uint32_t const num_valid_samples = adc_average->sum.num_samples.valid;

    // Calculate average if valid samples are available

    if(num_valid_samples > 0)
    {
        adc_average->raw.meas         = (int32_t)(adc_average->sum.raw / (cc_float)num_valid_samples);
        adc_average->raw.peak_to_peak = adc_average->sum.max - adc_average->sum.min;
    }
    else
    {
        adc_average->raw.meas         = 0;
        adc_average->raw.peak_to_peak = 0;
    }

    // Convert raw average and peak_to_peak to volts using calibration factors

    // For the peak_to_peak, the positive gain factor is used all the time. This is an acceptable simplification
    // because the peak-peak value depends on noise and doesn't have to be highly precise, and the positive and
    // negative gains are always very similar.


    meas_average->mean         = sigAdcRawToVoltsRT(cal_factors, adc_average->raw.meas);
    meas_average->peak_to_peak = (cc_float)adc_average->raw.peak_to_peak * cal_factors->gain_positive;
}


// NON-RT   sigAdc

void sigAdc(struct SIG_mgr * const sig_mgr,
            struct SIG_adc * const adc,
            bool             const start_average2)
{
    struct SIG_cal * const cal = &adc->cal;

    // Process average1 and compute measurement average and peak-peak in volts for period 1

    sigAdcAverage(&adc->average1, cal->active, &adc->meas.average1);

    // Record new avereage1 in the circular buffer

    adc->average1.raw.num_samples = adc->average1.sum.num_samples;

    uint32_t last_sample_index = (adc->circ_buf.last_sample_index + 1) & SIG_ADC_CIRC_BUF_MASK;

    adc->circ_buf.buffer[last_sample_index] = adc->average1.raw;

    adc->circ_buf.last_sample_index = last_sample_index;

    if(last_sample_index == adc->circ_buf.readout_sample_index)
    {
        adc->circ_buf.readout_sample_index = SIG_ADC_CIRC_BUF_EMPTY;
    }

    // Accumulate new average1 in average 2 sum and track min/max

    adc->average2.sum.raw += adc->average1.sum.raw;

    adc->average2.sum.num_samples.valid   += adc->average1.sum.num_samples.valid;
    adc->average2.sum.num_samples.invalid += adc->average1.sum.num_samples.invalid;

    if(adc->average1.sum.max > adc->average2.sum.max)
    {
        adc->average2.sum.max = adc->average1.sum.max;
    }

    if(adc->average1.sum.min < adc->average2.sum.min)
    {
        adc->average2.sum.min = adc->average1.sum.min;
    }

    // Restart average 2 when required

    if(start_average2)
    {
        // Process average2 and compute measurement average and peak-peak in volts for period 2

        sigAdcAverage(&adc->average2, cal->active, &adc->meas.average2);

        // Reset avereage2 sum

        adc->average2.sum.num_samples.valid   = 0;
        adc->average2.sum.num_samples.invalid = 0;

        adc->average2.sum.raw = 0.0;
        adc->average2.sum.max = INT32_MIN;
        adc->average2.sum.min = INT32_MAX;
    }

    // Calculate next calibration to be used during the next average1 period

    struct SIG_cal_factors * const next = cal->next;
    union  SIG_cal_event cal_event;

    // Only calculate calibration factors if the nominal gain has been set

    if(adc->nominal_gain > 0)
    {
        // Check calibration errors against limits and set next->status if calibration or temperature faults/warnings are detected

        sigCalCheckLimits(sig_mgr, cal, &cal_event, SIG_ADC_CAL_FLT_BIT_MASK, SIG_ADC_CAL_WARN_BIT_MASK, SIG_ADC_TEMP_WARN_BIT_MASK);

        // Calculate next ADC calibration factors using temperature if provided

        cc_float nominal_volt_gain = SIG_CAL_V_NOMINAL / (cc_float)adc->nominal_gain;

        next->offset.raw = (int32_t)((cc_float)adc->nominal_gain * 1.0E-6F * (cal_event.s.offset_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_OFFSET)));

        next->gain_positive = nominal_volt_gain / (1.0E-6F * (cal_event.s.gain_err_pos_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_POSITIVE)) + 1.0F);
        next->gain_negative = nominal_volt_gain / (1.0E-6F * (cal_event.s.gain_err_neg_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_NEGATIVE)) + 1.0F);

        // The gain factors should always be positive

        CC_ASSERT(next->gain_positive > 0.0F);
        CC_ASSERT(next->gain_negative > 0.0F);
    }
    else
    {
       next->status.faults |= SIG_ADC_CAL_FLT_BIT_MASK;
       next->offset.raw     = 0;
       next->gain_positive  = 0.0F;
       next->gain_negative  = 0.0F;
    }
}



void sigAdcRT(struct SIG_mgr          * const sig_mgr,
              struct SIG_adc          * const adc,
              bool                      const start_average1,
              struct CC_ns_time         const timestamp,
              uint32_t                  const faults_mask)
{
    int32_t           raw    = adc->raw;
    struct SIG_status status = { 0, 0 };

    // Respond to the ADC calibrating flag

    if(adc->calibrating_flag)
    {
        // Set to 2 to request two start_average1 periods before the calibration period is considered to be over.
        // This guarantees at least one average1 period to recover from the effects of the calibration.

        adc->calibrating_counter = 2;
    }

    //Process new raw ADC sample according to application's fault flag

    uint32_t stuck_limit = adc->stuck.limit_p == NULL ? adc->stuck.limit : *adc->stuck.limit_p;

    if(adc->fault_flag == true)
    {
        // Application reports that the raw ADC sample is invalid

        status.faults |= SIG_ADC_FLT_BIT_MASK;
        raw = adc->stuck.prev_raw;
        adc->stuck.counter = 0;
    }
    else if(stuck_limit != 0)
    {
        // Application reports that the raw ADC sample is valid, but check if the value is stuck

        if(raw == adc->stuck.prev_raw)
        {
            uint32_t counter = adc->stuck.counter + 1;

            if(counter >= stuck_limit)
            {
                status.faults |= SIG_ADC_STUCK_FLT_BIT_MASK;
            }
            else
            {
                adc->stuck.counter = counter;
            }
        }
        else
        {
            adc->stuck.counter = 0;
        }
    }

    // If no fault is present then the sample is valid

    if(status.faults == 0)
    {
        adc->stuck.prev_raw = raw;

        // Accumulate valid raw samples in average1 and track the min/max

        adc->average1.accumulator.raw += (cc_double)raw;
        adc->average1.accumulator.num_samples.valid++;

        if(raw > adc->average1.accumulator.max)
        {
            adc->average1.accumulator.max = raw;
        }
        else if(raw < adc->average1.accumulator.min)
        {
            adc->average1.accumulator.min = raw;
        }
    }
    else
    {
        adc->average1.accumulator.num_samples.invalid++;
    }

    // Restart average 1 when required and switch to next calibration factors

    if(start_average1 == true)
    {
        // Time stamp average1 with the time of the last sample

        adc->average1.raw.timestamp = timestamp;

        // Save current state of the average1 accumulator into sum

        adc->average1.sum = adc->average1.accumulator;

        // Reset average1 accumulator

        adc->average1.accumulator.num_samples.valid   = 0;
        adc->average1.accumulator.num_samples.invalid = 0;

        adc->average1.accumulator.raw = 0.0;
        adc->average1.accumulator.max = INT32_MIN;
        adc->average1.accumulator.min = INT32_MAX;
    }

    // FIR filter stage 1

    uint32_t fir_index = adc->fir_filter.index1;

    adc->fir_filter.accumulator1   += (raw - adc->fir_filter.buf1[fir_index]);
    adc->fir_filter.buf1[fir_index] =  raw;

    if(++fir_index >= SIG_ADC_FIR1_LEN)
    {
        fir_index = 0;
    }

    adc->fir_filter.index1 = fir_index;

    // FIR filter stage 2

    fir_index = adc->fir_filter.index2;

    adc->fir_filter.accumulator2   += (adc->fir_filter.accumulator1 - adc->fir_filter.buf2[fir_index]);
    adc->fir_filter.buf2[fir_index] =  adc->fir_filter.accumulator1;

    if(++fir_index >= SIG_ADC_FIR2_LEN)
    {
        fir_index = 0;
    }

    adc->fir_filter.index2 = fir_index;

    // Combine ADC faults and warnings with the active ADC calibration faults and warnings

    status.warnings |= adc->cal.active->status.warnings;
    status.faults   |= adc->cal.active->status.faults;

    // Combine ADC faults with the faults mask passed from from sigMgrRT

    status.faults   |= faults_mask;

    // Combine application ADC faults

    status.faults   |= adc->meas.status.faults & (SIG_ADC_APP_0_FLT_BIT_MASK|SIG_ADC_APP_1_FLT_BIT_MASK|SIG_ADC_APP_2_FLT_BIT_MASK|SIG_ADC_APP_3_FLT_BIT_MASK);

    adc->meas.status = status;

    // Convert raw value to volts and set validity based on faults mask

    adc->meas.unfiltered.signal   = sigAdcRawToVoltsRT(adc->cal.active, raw);
    adc->meas.unfiltered.is_valid = (status.faults == 0);

    // Invalidity 'flag' for filtered measurement can be thought of as FIFO holding
    // negated is_valid flags of all samples in the filter buffers
    // The code below pushes !is_valid of the newest sample to the FIFO and then
    // drops the oldest flag

    adc->fir_filter.invalidity_fifo_bitmask  |= adc->meas.unfiltered.is_valid ? 0 : (1 << (SIG_ADC_FIR1_LEN + SIG_ADC_FIR2_LEN - 1));
    adc->fir_filter.invalidity_fifo_bitmask >>= 1;

    // Convert filtered raw value to volts and set validity based on validity FIFO

    adc->meas.filtered.signal   = sigAdcRawToVoltsRT(adc->cal.active, adc->fir_filter.accumulator2 / (SIG_ADC_FIR1_LEN*SIG_ADC_FIR2_LEN));
    adc->meas.filtered.is_valid = (adc->fir_filter.invalidity_fifo_bitmask == 0);

    // Make sure that is_valid integer is big enough to hold flags for all samples in the filter buffer

    CC_STATIC_ASSERT((SIG_ADC_FIR1_LEN + SIG_ADC_FIR2_LEN - 1) < sizeof(adc->fir_filter.invalidity_fifo_bitmask) * CHAR_BIT,
                     filtered_meas_is_valid_flag_too_small);

    // Switch to next calibration factors for the new average1 period

    if(start_average1 == true)
    {
        struct SIG_cal_factors *temp = adc->cal.active;
        adc->cal.active = adc->cal.next;
        adc->cal.next   = temp;

        // Down count the calibrating counter to extend the calibrating time

        if(adc->calibrating_counter > 0)
        {
            adc->calibrating_counter--;
        }
    }
}



cc_float sigAdcRawToVoltsRT(struct SIG_cal_factors * const cal_factors, int32_t raw)
{
    raw -= cal_factors->offset.raw;

    return (cc_float)raw * (raw < 0 ? cal_factors->gain_negative : cal_factors->gain_positive);
}

// EOF
