//! @file  sigTransducer.c
//! @brief Converter Control Signals library Transducer functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libsig.h"

// Transducer related constants

static const cc_float SIG_TRANSDUCER_VADC_OFFSET_THRESHOLD = 1.0F;          //!< Limit for identification of cal level in volts


// NON-RT   sigTransducerCal

enum SIG_cal_level sigTransducerCal(struct SIG_mgr * const sig_mgr, struct SIG_transducer * const transducer)
{
    int32_t                 average_raw;
    cc_float                average_v_adc;
    enum SIG_cal_level      cal_level;
    struct SIG_adc * const  adc            =  transducer->adc;
    struct SIG_cal * const  transducer_cal = &transducer->cal;

    // If calibration average from ADC circular buffer is invalid then return immediately

    if(   adc == NULL
       || transducer->nominal_gain == 0.0F
       || sigAdcCalAverage(transducer->adc, &average_raw) == false)
    {
        return SIG_CAL_FAULT;
    }

    average_v_adc = sigAdcRawToVoltsRT(adc->cal.active, average_raw);

    // Identify calibration level

    if(average_v_adc > SIG_TRANSDUCER_VADC_OFFSET_THRESHOLD)
    {
        cal_level = SIG_CAL_POSITIVE;
    }
    else if(average_v_adc < -SIG_TRANSDUCER_VADC_OFFSET_THRESHOLD)
    {
        cal_level = SIG_CAL_NEGATIVE;
    }
    else
    {
        cal_level = SIG_CAL_OFFSET;

        // Calculate offset error normalized to T0

        transducer_cal->event.s.offset_ppm = (average_v_adc / SIG_CAL_V_NOMINAL) * 1.0E6F
                                           - sigCalRefErrorPpm(sig_mgr, transducer_cal, SIG_CAL_OFFSET) // External Cal. Ref uses event.f[SIG_CAL_OFFSET]
                                           - sigTempErrorPpm  (sig_mgr, transducer_cal, SIG_CAL_OFFSET);
    }

    // If calibrating a gain error

    if(cal_level != SIG_CAL_OFFSET)
    {
        // Calculate the offset error at the temperature of the transducer

        cc_double offset_ppm = transducer_cal->event.s.offset_ppm + sigTempErrorPpm(sig_mgr, transducer_cal, SIG_CAL_OFFSET);

        // Calculate positive or negative gain error normalized to T0

        cc_float const gain_err_ppm = (transducer->ignore_gain_err_during_cal == NULL || *transducer->ignore_gain_err_during_cal == CC_DISABLED) ?
                                       transducer->gain_err_ppm : 0.0F;

        if(cal_level == SIG_CAL_POSITIVE)
        {
            transducer_cal->event.s.gain_err_pos_ppm = ( average_v_adc / SIG_CAL_V_NOMINAL - 1.0F) * 1.0E6F
                                                     - offset_ppm
                                                     - gain_err_ppm
                                                     - sigCalRefErrorPpm(sig_mgr, transducer_cal, SIG_CAL_OFFSET) // External Cal. Ref uses event.f[SIG_CAL_OFFSET]
                                                     - sigTempErrorPpm  (sig_mgr, transducer_cal, SIG_CAL_POSITIVE);
        }
        else
        {
            transducer_cal->event.s.gain_err_neg_ppm = (-average_v_adc / SIG_CAL_V_NOMINAL - 1.0F) * 1.0E6F
                                                     + offset_ppm
                                                     - gain_err_ppm
                                                     - sigCalRefErrorPpm(sig_mgr, transducer_cal, SIG_CAL_OFFSET) // External Cal. Ref uses event.f[SIG_CAL_OFFSET]
                                                     - sigTempErrorPpm  (sig_mgr, transducer_cal, SIG_CAL_NEGATIVE);
        }
    }

    // Save date/time stamp

    sigCalEventSetUnixtime(&transducer_cal->event.s, adc->average1.raw.timestamp.secs.abs);

    // Save temperature if available

    if(transducer->cal.temp_filter_index != SIG_NOT_IN_USE)
    {
        transducer_cal->event.s.temp_c = sig_mgr->temp_filters[transducer_cal->temp_filter_index].temp_c;
    }

    return cal_level;
}


// NON-RT   sigTransducerAvereage

static void sigTransducerAvereage(struct SIG_meas_average * const adc_average,
                                  struct SIG_cal_factors  * const cal_factors,
                                  struct SIG_meas_average * const transducer_average,
                                  cc_float                  const invert_factor)
{
    // Convert adc average and peak_to_peak voltages to transducer units using calibration factors

    // For the peak_to_peak, the condition for the choice of positive or negative gain factor is slightly simplified by
    // ignoring the offset (compare to sigTransducerVadcToPhysUnitsRT()). This is acceptable because the peak-peak value
    // depends on noise and doesn't have to be highly precise, and the positive and negative gains are always very similar.


    transducer_average->mean         = invert_factor * sigTransducerVadcToPhysUnitsRT(cal_factors, adc_average->mean);
    transducer_average->peak_to_peak = adc_average->peak_to_peak * (adc_average->mean < 0 ? cal_factors->gain_negative : cal_factors->gain_positive);
}


// NON-RT   sigTransducer

void sigTransducer(struct SIG_mgr * const sig_mgr, struct SIG_transducer * const transducer, bool const start_average2)
{
    struct SIG_adc * const adc =  transducer->adc;
    struct SIG_cal * const cal = &transducer->cal;

    // Prepare invert_if_polswitch_neg flag for sigTransducerRT()

    transducer->invert_if_polswitch_neg = (transducer->position == SIG_TRANSDUCER_POSITION_CONVERTER);

    // If the transducer is using external measurement, we don't need calibration factors nor we can calculate the averages

    if(adc == NULL)
    {
        transducer->meas.status.warnings = 0;

        transducer->meas.average1.mean         = 0.0F;
        transducer->meas.average1.peak_to_peak = 0.0F;

        transducer->meas.average2.mean         = 0.0F;
        transducer->meas.average2.peak_to_peak = 0.0F;

        return;
    }

    // Convert ADC average1 measurement and peak-peak to transducer units

    sigTransducerAvereage(&adc->meas.average1, cal->active, &transducer->meas.average1, transducer->invert_factor);

    // Converter ADC average2 measurement and peak-peak to transducer units if avereage2 was just recomputed

    if(start_average2 == true)
    {
        sigTransducerAvereage(&adc->meas.average2, cal->active, &transducer->meas.average2, transducer->invert_factor);
    }

    // Calculate next calibration provided previous cal factors were switched by RT thread

    struct SIG_cal_factors * const next = cal->next;
    union  SIG_cal_event           cal_event;

    // Check calibration errors against limits and set next->status if calibration or temperature faults/warnings are detected

    sigCalCheckLimits(sig_mgr, cal, &cal_event, SIG_TRANSDUCER_CAL_FLT_BIT_MASK, SIG_TRANSDUCER_CAL_WARN_BIT_MASK, SIG_TRANSDUCER_TEMP_WARN_BIT_MASK);

    // Calculate next ADC calibration factors using temperature if provided

    cc_float gain_primary_turns_gain = transducer->nominal_gain;
    uint32_t primary_turns           = transducer->primary_turns_p == NULL ? transducer->primary_turns : *transducer->primary_turns_p;

    if(primary_turns > 1)
    {
        gain_primary_turns_gain /= (cc_float)primary_turns;
    }

    next->offset.v = 1.0E-6F * (cal_event.s.offset_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_OFFSET )) * SIG_CAL_V_NOMINAL;

    next->gain_positive = gain_primary_turns_gain / (1.0E-6F * (cal_event.s.gain_err_pos_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_POSITIVE) + transducer->gain_err_ppm) + 1.0F);
    next->gain_negative = gain_primary_turns_gain / (1.0E-6F * (cal_event.s.gain_err_neg_ppm + sigTempErrorPpm(sig_mgr, cal, SIG_CAL_NEGATIVE) + transducer->gain_err_ppm) + 1.0F);

    // The gains should not be zero

    if(next->gain_positive == 0.0F)
    {
        next->gain_positive = 1.0F;
    }

    if(next->gain_negative == 0.0F)
    {
        next->gain_negative = 1.0F;
    }
}



void sigTransducerRT(struct SIG_mgr        * const sig_mgr,
                     struct SIG_transducer * const transducer,
                     bool                    const start_average1)
{
    struct SIG_adc const * const adc = transducer->adc;

    // Set invert factor based on polswitch state and invert_if_polswitch_neg flag from sigTransducer()

    transducer->invert_factor = (   transducer->position == SIG_TRANSDUCER_POSITION_INVERTED
                                 || (   transducer->invert_if_polswitch_neg == true
                                     && sig_mgr->polswitch_negative == true)
                              ? -1.0F
                              :  1.0F);

    // If there's no ADC to feed the transducer, we will use external measurement

    if(adc == NULL)
    {
        // Set filtered and unfiltered measurements to the same value

        cc_float const value    = transducer->invert_factor * transducer->ext_meas.meas.signal;
        bool     const is_valid = transducer->ext_meas.meas.is_valid;

        transducer->meas.unfiltered.signal   = value;
        transducer->meas.filtered.signal     = value;
        transducer->meas.unfiltered.is_valid = is_valid;
        transducer->meas.filtered.is_valid   = is_valid;

        // Set transducer fault if external measurement is invalid

        transducer->meas.status.faults = (is_valid ? 0 : SIG_TRANSDUCER_EXT_FLT_BIT_MASK);

        return;
    }

    // Set transducer faults and warnings, starting with transducer calibration faults and warning

    struct SIG_status status = transducer->cal.active->status;

    if(adc->meas.status.faults != 0)
    {
        status.faults |= SIG_TRANSDUCER_ADC_FLT_BIT_MASK;
    }

    if(adc->meas.status.warnings != 0)
    {
        status.warnings |= SIG_TRANSDUCER_ADC_WARN_BIT_MASK;
    }

    if(transducer->fault_flag == true)
    {
        status.faults |= SIG_TRANSDUCER_FLT_BIT_MASK;
    }

    transducer->meas.status = status;

    // Set the adc_calibrating_flag when the extended calibrating counter is not zero and only
    // update the measured values when not calibrating

    if((transducer->adc_calibrating_flag = (adc->calibrating_counter > 0)) == false)
    {
        // Convert unfiltered ADC voltage to physical units and set validity based on faults bit mask

        transducer->meas.unfiltered.signal   = transducer->invert_factor * sigTransducerVadcToPhysUnitsRT(transducer->cal.active, adc->meas.unfiltered.signal);
        transducer->meas.unfiltered.is_valid = (status.faults == 0);

        // Convert Filtered ADC voltage to physical units

        transducer->meas.filtered.signal   = transducer->invert_factor * sigTransducerVadcToPhysUnitsRT(transducer->cal.active, adc->meas.filtered.signal);
        transducer->meas.filtered.is_valid = (adc->meas.filtered.is_valid && status.faults == 0);
    }

    // Switch transducer calibration factors at the start of each average1 period

    if(start_average1 == true)
    {
        struct SIG_cal_factors * temp = transducer->cal.active;
        transducer->cal.active        = transducer->cal.next;
        transducer->cal.next          = temp;
    }
}



cc_float sigTransducerVadcToPhysUnitsRT(struct SIG_cal_factors * const cal_factors, cc_float v_adc)
{
    v_adc -= cal_factors->offset.v;

    return v_adc * (v_adc < 0.0F ? cal_factors->gain_negative : cal_factors->gain_positive);
}

// EOF
