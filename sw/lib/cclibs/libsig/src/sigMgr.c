//! @file  sigMgr.c
//! @brief Converter Control Signals library management functions.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libreg.
//!
//! libreg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libsig.h"


// NON-RT   sigMgr

void sigMgr(struct SIG_mgr * const sig_mgr, bool const start_average2)
{
    // Background processing for all temperature filters

    struct SIG_temp_filter * temp_filter = sig_mgr->temp_filters;
    uint32_t                 temp_filter_index;

    for(temp_filter_index = 0 ; temp_filter_index < sig_mgr->num_temp_filters ; temp_filter_index++)
    {
        sigTempFilter(temp_filter++);
    }

    // Background processing for all ADCs

    uint32_t   const num_adcs        = sig_mgr->num_adcs;
    uint32_t   const num_transducers = sig_mgr->num_transducers;
    struct SIG_adc * adc = sig_mgr->adcs;
    uint32_t         transducer_in_use_bit_mask = 0;
    uint32_t         adc_index;
    uint32_t         transducer_index;

    for(adc_index = 0 ; adc_index < num_adcs ; adc_index++, adc++)
    {
        transducer_index = adc->transducer_index;

        // Only process the ADC if it is linked to a valid transducer.

        if(transducer_index < num_transducers)
        {
            sigAdc(sig_mgr, adc, start_average2);

            transducer_in_use_bit_mask |= (1 << adc->transducer_index);
        }
        else if(adc->calibrating_flag)
        {
            // ADC is not in use but is being calibrated, so activate the processing of the ADC

            sigAdc(sig_mgr, adc, start_average2);
        }
        else
        {
            // ADC is not linked to a transducer, so reset the ADC status and measurement

            sigAdcOff(adc);
        }
    }

    // Run all active transducers

    struct SIG_transducer * transducer = sig_mgr->transducers;

    for(transducer_index = 0; transducer_index < num_transducers; transducer_index++)
    {
        // If the transducer is linked to an ADC or it uses external measurements

        if(   (transducer_in_use_bit_mask & 1) == 1
           || transducer->ext_meas.is_enabled == true)
        {
            sigTransducer(sig_mgr, transducer, start_average2);
        }

        transducer_in_use_bit_mask >>= 1;
        transducer++;
    }

    // Run all signal selectors

    struct SIG_select * select = sig_mgr->selects;
    uint32_t            select_index;

    for(select_index = 0 ; select_index < sig_mgr->num_selects ; select_index++, select++)
    {
        sigSelect(sig_mgr, select, start_average2);
    }
}



void sigMgrRT(struct SIG_mgr * const sig_mgr, bool const start_average1, struct CC_ns_time const timestamp)
{
    // Real-time processing for all ADCs and their associated transducers and selectors

    uint32_t const          num_adcs        = sig_mgr->num_adcs;
    uint32_t const          num_transducers = sig_mgr->num_transducers;
    struct SIG_adc        * adc             = sig_mgr->adcs;
    struct SIG_transducer * transducer;
    uint32_t                adc_index;
    uint32_t                transducer_index;
    uint32_t                transducer_has_adc_bit_mask = 0;

    for(adc_index = 0 ; adc_index < num_adcs ; adc_index++, adc++)
    {
        transducer_index = adc->transducer_index;

        // Only process the ADC if it is linked to a valid transducer.

        if(transducer_index < num_transducers)
        {
            // ADC is linked with a transducer

            uint32_t const transducer_bit_mask = (1 << transducer_index);

            transducer = &sig_mgr->transducers[transducer_index];

            if(   (transducer_has_adc_bit_mask & transducer_bit_mask) == 0
               && transducer->ext_meas.is_enabled == false)
            {
                // This is the first reference to this transducer by an ADC so process the ADC

                sigAdcRT(sig_mgr, adc, start_average1, timestamp, 0);

                transducer_has_adc_bit_mask |= transducer_bit_mask;
                transducer->adc = adc;
            }
            else
            {
                // Another ADC already claims this transducer so set the "TRANSDUCER IN USE" fault on this ADC

                sigAdcRT(sig_mgr, adc, start_average1, timestamp, SIG_ADC_XDCR_IN_USE_FLT_BIT_MASK);
            }
        }
        else if(adc->calibrating_flag)
        {
            // ADC is not in use but is being calibrated, so activate the processing of the ADC

            sigAdcRT(sig_mgr, adc, start_average1, timestamp, 0);
        }
    }

    // Cache polswitch negative flag

    sig_mgr->polswitch_negative = (sig_mgr->polswitch_mgr != NULL && polswitchIsNegative(sig_mgr->polswitch_mgr) == true);

    // Mark all unused transducer signals as "not available" and process external measurements

    transducer = sig_mgr->transducers;

    for(transducer_index = 0 ; transducer_index < num_transducers ; transducer_index++)
    {
        // Process all used transducers

        if(   (transducer_has_adc_bit_mask & 1) == 1
           || transducer->ext_meas.is_enabled == true)
        {
            if(transducer->ext_meas.is_enabled == true)
            {
                transducer->adc = NULL;
            }

            sigTransducerRT(sig_mgr, transducer, start_average1);
        }
        else
        {
            // If transducer is not in use then set NOT_AVL fault and clear warnings

            transducer->meas.status.faults       = SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK;
            transducer->meas.status.warnings     = 0;
            transducer->meas.unfiltered.signal   = 0.0F;
            transducer->meas.filtered.signal     = 0.0F;
            transducer->meas.unfiltered.is_valid = false;
            transducer->meas.filtered.is_valid   = false;
            transducer->adc                      = NULL;
        }

        transducer_has_adc_bit_mask >>= 1;
        transducer++;
    }

    // Run all signal selectors

    struct SIG_select * select      = sig_mgr->selects;
    uint32_t const      num_selects = sig_mgr->num_selects;

    uint32_t select_index;

    for(select_index = 0 ; select_index < num_selects ; select_index++)
    {
        sigSelectRT(sig_mgr, select++);
    }
}


// NON-RT   sigMgrFast

void sigMgrFast(struct SIG_mgr * const sig_mgr)
{
    // Fast processing for ADCs and their associated transducers and selectors

    uint32_t              const   num_transducers = sig_mgr->num_transducers;
    struct SIG_transducer       * transducer      = sig_mgr->transducers;
    uint32_t                      transducer_index;

    // Process transducers with fast acquisition measurements

    for(transducer_index = 0 ; transducer_index < num_transducers ; transducer_index++, transducer++)
    {
        if(transducer->fast_flag == true)
        {
            if (transducer->adc != NULL)
            {
                cc_float const raw_adc_fast = sigAdcRawToVoltsRT(transducer->adc->cal.active, *transducer->fast_raw);

                transducer->fast_meas = transducer->invert_factor * sigTransducerVadcToPhysUnitsRT(transducer->cal.active, raw_adc_fast);
            }
            else
            {
                transducer->fast_meas = 0.0F;
            }
        }
    }

    // Run all fast signal selectors

    uint32_t          const   num_selects = sig_mgr->num_selects;
    struct SIG_select       * select      = sig_mgr->selects;
    uint32_t                  select_index;

    for(select_index = 0 ; select_index < num_selects ; select_index++, select++)
    {
        if(select->fast_flag == true)
        {
            struct SIG_transducer const * transducer_a = &sig_mgr->transducers[select->transducer_a_index];
            struct SIG_transducer const * transducer_b = &sig_mgr->transducers[select->transducer_b_index];

            switch(select->actual_selector)
            {
                case SIG_A:

                    select->fast_meas = transducer_a->fast_meas;
                    break;

                case SIG_B:

                    select->fast_meas = transducer_b->fast_meas;
                    break;

                case SIG_AB:

                    // Measured signal is average of A and B

                    select->fast_meas = 0.5F * (transducer_a->fast_meas + transducer_b->fast_meas);
                    break;

                default:

                    // No input signals available to freeze last valid measurement

                    break;
            }
        }
    }
}

// EOF
