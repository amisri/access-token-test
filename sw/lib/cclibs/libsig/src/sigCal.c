//! @file  sigCal.c
//! @brief Converter Control Signals library generic calibration functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libsig.h"


// NON-RT   sigCalInit

void sigCalInit(struct SIG_cal * const cal)
{
    cal->active = &cal->factors[0];
    cal->next   = &cal->factors[1];
}


// NON-RT   sigCalRefErrorPpm

cc_float sigCalRefErrorPpm(struct SIG_mgr     * const sig_mgr,
                           struct SIG_cal     * const cal,
                           enum   SIG_cal_level const cal_level)
{
    cc_float ref_err_ppm = 0.0F;

    // If calibration reference is in use then extract the error for the given level

    if(cal->cal_ref_index != SIG_NOT_IN_USE)
    {
        struct SIG_cal_ref * const cal_ref = &sig_mgr->cal_refs[cal->cal_ref_index];

        ref_err_ppm = cal_ref->event.f[cal_level];

        // If the temperature of the calibration reference is controlled, then apply the temperature coefficient to the
        // errors that are normalized (by the user) to 40C. If the calibration reference must be operated at a different
        // temperature, then the temperature coefficient is applied to the difference from 40C.

        if(cal_ref->temp_c != NULL)
        {
            ref_err_ppm += (*cal_ref->temp_c - SIG_TEMP_CAL_REF_T0) * cal_ref->tc[cal_level];
        }
    }

    return ref_err_ppm;
}


// NON-RT   sigCalCheckLimits

void sigCalCheckLimits(struct SIG_mgr       * const sig_mgr,
                       struct SIG_cal       * const cal,
                       union  SIG_cal_event * const cal_event,
                       uint32_t               const cal_flt_bit_mask,
                       uint32_t               const cal_warn_bit_mask,
                       uint32_t               const temp_warn_bit_mask)
{
    struct SIG_status status = { 0, 0 };

    // Copy cal event by default

    *cal_event = cal->event;

    // Check calibration limits if provided

    if(cal->cal_limit_index != SIG_NOT_IN_USE)
    {
        struct SIG_cal_limit * const limit = &sig_mgr->cal_limits[cal->cal_limit_index];

        cc_float offset_ppm_error = fabsf(cal->event.s.offset_ppm - limit->nominal_offset_ppm);
        cc_float gain_err_pos_ppm = fabsf(cal->event.s.gain_err_pos_ppm);
        cc_float gain_err_neg_ppm = fabsf(cal->event.s.gain_err_neg_ppm);

        // Check fault limits

        if(   offset_ppm_error > limit->offset_fault_ppm
           || gain_err_pos_ppm > limit->gain_err_fault_ppm
           || gain_err_neg_ppm > limit->gain_err_fault_ppm)
        {
            // If calibration exceeds a fault limit then reset to nominal values

            cal_event->s.offset_ppm       = limit->nominal_offset_ppm;
            cal_event->s.gain_err_pos_ppm = 0.0F;
            cal_event->s.gain_err_neg_ppm = 0.0F;

            status.faults = cal_flt_bit_mask;
        }
        else
        {
             // Check warning limits

            if(   offset_ppm_error > limit->offset_warning_ppm
               || gain_err_pos_ppm > limit->gain_err_warning_ppm
               || gain_err_neg_ppm > limit->gain_err_warning_ppm)
            {
                status.warnings = cal_warn_bit_mask;
            }
        }
    }

    // Check temperature for warnings

    if(cal->temp_filter_index != SIG_NOT_IN_USE)
    {
        struct SIG_temp_filter * const temp_filter = &sig_mgr->temp_filters[cal->temp_filter_index];

        if(temp_filter->invalid_flag == true || temp_filter->warning_flag == true)
        {
            status.warnings |= temp_warn_bit_mask;
        }
    }

    // Store status in next factors structure

    cal->next->status = status;
}

// EOF
