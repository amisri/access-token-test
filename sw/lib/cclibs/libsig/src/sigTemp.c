//! @file  sigTemp.c
//! @brief Converter Control Signals library Temperature functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "stddef.h"

#include "libsig.h"

// Signal temp constants

static uint32_t const SIG_TEMP_INVALID_COUNT = 3;               //!< Number of consecutive invalid temperatures threshold


// NON-RT   sigTempInit

void sigTempInit(struct SIG_temp_filter * const temp_filter)
{
    // Don't set last_temp_c so it remains zero and indicates first call to sigTempMeas()

    temp_filter->temp_c            = SIG_TEMP_CAL_T0;
    temp_filter->last_valid_temp_c = SIG_TEMP_CAL_T0;
}


// NON-RT   sigTempMeas

void sigTempMeas(struct SIG_temp_filter * const temp_filter, cc_float measured_temp_c)
{
    // If this is the first call, last_temp_c will still be zero

    if(temp_filter->last_temp_c == 0.0F)
    {
        if(   measured_temp_c >= temp_filter->invalid.min_c
           && measured_temp_c <= temp_filter->invalid.max_c)
        {
            // Measurement is valid so jump the filtered value immediately to the measured value

            temp_filter->temp_c            = measured_temp_c;
            temp_filter->last_temp_c       = measured_temp_c;
            temp_filter->last_valid_temp_c = measured_temp_c;

            temp_filter->invalid_flag = false;
        }
        else
        {
            // Initial measurement is invalid so don't use it and set the invalid flag

            temp_filter->invalid_flag = true;
        }

        return;
    }

    // Check the new measurement against the valid range and max valid step size

    if(   measured_temp_c < temp_filter->invalid.min_c
       || measured_temp_c > temp_filter->invalid.max_c
       || fabsf(measured_temp_c - temp_filter->last_temp_c) > temp_filter->max_delta_temp_c)
    {
        uint32_t invalid_counter = temp_filter->invalid_counter + 1;

        if(invalid_counter >= SIG_TEMP_INVALID_COUNT)
        {
            temp_filter->invalid_flag = true;
        }
        else
        {
            temp_filter->invalid_counter = invalid_counter;
        }
    }

    // Measurement is valid

    else
    {
        temp_filter->last_valid_temp_c = measured_temp_c;
        temp_filter->invalid_flag      = false;
        temp_filter->invalid_counter   = 0;
    }

    // Keep last measurement for rate test

    temp_filter->last_temp_c = measured_temp_c;
}


// NON-RT   sigTempFilter

void sigTempFilter(struct SIG_temp_filter * const temp_filter)
{
    // Filter last valid temperature

    cc_float const time_constant_s =  temp_filter->time_constant_s_p == NULL
                                   ?  temp_filter->time_constant_s
                                   : *temp_filter->time_constant_s_p;

    if(time_constant_s > 0.0F)
    {
        cc_float filter_factor = temp_filter->filter_period_s / (time_constant_s + 0.5F * temp_filter->filter_period_s);

        temp_filter->temp_c += filter_factor * (temp_filter->last_valid_temp_c - temp_filter->temp_c);
    }
    else
    {
        temp_filter->temp_c = temp_filter->last_valid_temp_c;
    }

    // Check temperature against warning limits, with hysteresis to avoid toggling

    cc_float const warning_max_c =  temp_filter->warning.max_c_p == NULL
                                 ?  temp_filter->warning.max_c
                                 : *temp_filter->warning.max_c_p;

    if(temp_filter->warning_flag == false)
    {
        if(temp_filter->temp_c > warning_max_c ||
           temp_filter->temp_c < temp_filter->warning.min_c)
        {
            temp_filter->warning_flag = true;
        }
    }
    else
    {
        if(temp_filter->temp_c < (warning_max_c              - temp_filter->max_delta_temp_c) &&
           temp_filter->temp_c > (temp_filter->warning.min_c + temp_filter->max_delta_temp_c))
        {
            temp_filter->warning_flag = false;
        }
    }
}


// NON-RT   sigTempErrorPpm

cc_float sigTempErrorPpm(struct SIG_mgr * const sig_mgr, struct SIG_cal * const cal, enum SIG_cal_level const cal_level)
{
    if(cal->temp_filter_index == SIG_NOT_IN_USE)
    {
        return 0.0F;
    }

    cc_float temp_c = sig_mgr->temp_filters[cal->temp_filter_index].temp_c;
    cc_float tc     = cal->tc [cal_level];
    cc_float dtc    = cal->dtc[cal_level];

    // if second order temperature coefficient defined then use parabolic temperature compensation function

    if(dtc != 0.0F)
    {
        return   (temp_c - SIG_TEMP_CAL_T0)
               * (tc + dtc * (SIG_TEMP_CAL_T2 - temp_c)
               * (1.0F / ((SIG_TEMP_CAL_T1 - SIG_TEMP_CAL_T0) * (SIG_TEMP_CAL_T2 - SIG_TEMP_CAL_T1))));
    }

    // else use linear temperature compensation

    return (temp_c - SIG_TEMP_CAL_T0) * tc;
}

// EOF
