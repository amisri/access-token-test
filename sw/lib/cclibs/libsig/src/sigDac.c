//! @file  sigDac.c
//! @brief Converter Control Signals library DAC functions
//!
//! These functions support bipolar DACs only.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "libsig.h"


// NON-RT   sigDacInit

void sigDacInit(struct SIG_dac * const dac, uint32_t const resolution, cc_float const v_nominal, cc_float const max_v_error)
{
    // Save parameters

    dac->v_nominal   = v_nominal;
    dac->max_v_error = max_v_error;

    // Calculate DAC raw value range from the resolution - this assumes the DAC is bipolar

    dac->min_raw_dac = -(1 << (resolution-1));
    dac->max_raw_dac = -(dac->min_raw_dac + 1);

    // Calibrate using nominal full scale voltage

    sigDacCal(dac, 0, 0.0F);
    sigDacCal(dac, dac->max_raw_dac,  v_nominal);
    sigDacCal(dac, dac->min_raw_dac, -v_nominal);
}


// NON-RT   sigDacCal

enum SIG_cal_level sigDacCal(struct SIG_dac * const dac, int32_t const raw_dac, cc_float const v_dac_meas)
{
    bool fault_flag = false;
    enum SIG_cal_level cal_level;

    // Set calibration level based on the raw DAC value

    if(raw_dac == 0)
    {
        cal_level = SIG_CAL_OFFSET;
    }
    else if(raw_dac > 0)
    {
        cal_level = SIG_CAL_POSITIVE;

        if(v_dac_meas > dac->cal_v_dac_meas[SIG_CAL_OFFSET])
        {
            // Calculate the DAC scaling for positive raw_dac values (will always be positive)

            dac->pos_raw_per_volt = (cc_float)raw_dac / (v_dac_meas - dac->cal_v_dac_meas[SIG_CAL_OFFSET]);
        }
        else
        {
            fault_flag = true;
        }
    }
    else
    {
        cal_level = SIG_CAL_NEGATIVE;

        if(v_dac_meas < dac->cal_v_dac_meas[SIG_CAL_OFFSET])
        {
            // Calculate the DAC scaling for negative raw_dac values (will always be positive)

            dac->neg_raw_per_volt = (cc_float)raw_dac / (v_dac_meas - dac->cal_v_dac_meas[SIG_CAL_OFFSET]);
        }
        else
        {
            fault_flag = true;
        }
    }

    // Save measured DAC voltage for the calibration level

    dac->cal_v_dac_meas[cal_level] = v_dac_meas;

    // Calculate the expected DAC voltage and check if the measured voltage is within the tolerance

    cc_float v_dac_expected = dac->v_nominal * (cc_float)raw_dac / (cc_float)dac->max_raw_dac;

    if(fabsf(v_dac_meas - v_dac_expected) > dac->max_v_error)
    {
        fault_flag = true;
    }

    // Set or clear DAC calibration fault bit

    if(fault_flag)
    {
        dac->cal_fault_bit_mask |= (1 << cal_level);
    }
    else
    {
        dac->cal_fault_bit_mask &= ~(1 << cal_level);
    }

    // Return actual calibration level if there was no fault

    return fault_flag ? SIG_CAL_FAULT : cal_level;
}



int32_t sigDacRT(struct SIG_dac const * const dac, cc_float v_dac, bool * clipped_flag)
{
    int32_t      raw_dac;
    bool         dummy;

    // Use dummy bool if clipped_flag is NULL

    if(clipped_flag == NULL)
    {
        clipped_flag = &dummy;
    }

    // Calculate raw DAC value based on calibration

    v_dac -= dac->cal_v_dac_meas[SIG_CAL_OFFSET];

    if(v_dac >= 0.0F)
    {
        raw_dac = (int32_t)(v_dac * dac->pos_raw_per_volt);
    }
    else
    {
        raw_dac = (int32_t)(v_dac * dac->neg_raw_per_volt);
    }

    // Clip raw value to raw DAC range

    if(raw_dac > dac->max_raw_dac)
    {
        raw_dac = dac->max_raw_dac;

        *clipped_flag = true;
    }
    else if(raw_dac < dac->min_raw_dac)
    {
        raw_dac = dac->min_raw_dac;

        *clipped_flag = true;
    }
    else
    {
        *clipped_flag = false;
    }

    return raw_dac;
}



cc_float sigDacInvertRT(struct SIG_dac const * const dac, int32_t raw_dac)
{
    cc_float v_dac;

    // Calculate DAC voltage from the raw DAC value based on calibration

    if(raw_dac >= 0.0F)
    {
        v_dac = (cc_float)raw_dac / dac->pos_raw_per_volt;
    }
    else
    {
        v_dac = (cc_float)raw_dac / dac->neg_raw_per_volt;
    }

    v_dac += dac->cal_v_dac_meas[SIG_CAL_OFFSET];

    return v_dac;
}

// EOF
