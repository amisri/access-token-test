//! @file sigCal.h
//! @brief Converter Control Signals library generic calibration functions
//!
//! Functions for manipulating generic calibration data (for ADC or transducers)
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Calibration constants

#define SIG_CAL_EVENT_LEN                   6           //!< Number of floats in struct SIG_cal_event

static const cc_float SIG_TEMP_CAL_REF_T0 = 40.0F;      //!< Reference voltage calibration temperature
static const cc_float SIG_CAL_V_NOMINAL   = 10.0F;      //!< Nominal calibration voltage

// Macros

#define sigCalEventGetUnixtime(cal_event_s)             ((uint32_t)(cal_event_s)->date_days * 86400 + (uint32_t)(cal_event_s)->time_s)
#define sigCalEventSetUnixtime(cal_event_s,unix_time)   (cal_event_s)->date_days=(cc_float)(unix_time/86400),(cal_event_s)->time_s=(cc_float)(unix_time%86400)

// Global calibration structures

//! Calibration event data
//!
//! Note: All the fields in the SIG_cal_event structure are floats so that they can be represented as a single
//! float array.  This is not ideal but simplifies the storage of the calibration data in a database.
//! It means that unix_time must be broken into two floats since one float has
//! insufficient resolution to hold a 32-bit integer.


struct SIG_cal_event_fields                             //!< Calibration event for ADC or transducer - ppm values are normalised to temp T0
{
    cc_float                    offset_ppm;             //!< ADC: Raw offset in ppm of nominal gain at T0. DCCT: Voltage offset in ppm of SIG_CAL_V_NOMINAL at T0.
    cc_float                    gain_err_pos_ppm;       //!< Gain error in ppm of nominal gain for positive values at T0
    cc_float                    gain_err_neg_ppm;       //!< Gain error in ppm of nominal gain for negative values at T0
    cc_float                    temp_c;                 //!< Temperature when calibrated
    cc_float                    date_days;              //!< Calibration date in days since 1970
    cc_float                    time_s;                 //!< Calibration time in seconds since midnight
};

union SIG_cal_event
{
    struct SIG_cal_event_fields s;                      //!< Struct for cal_event elements
    cc_float                    f[SIG_CAL_EVENT_LEN];   //!< Array for cal_event elements
};

struct SIG_cal_limit                                    //!< Calibration error limits for ADCs or DCCTs
{
    cc_float                    nominal_offset_ppm;     //!< Nominal offset (ppm) in case the interface has a bias
    cc_float                    offset_warning_ppm;     //!< Limits are nominal +/- warning level (ppm)
    cc_float                    offset_fault_ppm;       //!< Limits are nominal +/- fault level (ppm)
    cc_float                    gain_err_warning_ppm;   //!< Limits are +/- warning level (ppm)
    cc_float                    gain_err_fault_ppm;     //!< Limits are +/- fault level (ppm)
};

struct SIG_cal_factors                                  //!< ADC or transducer calibration factors for a particular temperature
{
    union
    {
        int32_t                 raw;                    //!< Offset for ADC calibration in raw units
        cc_float                v;                      //!< Offset for transducer calibration in volts
    } offset;

    cc_float                    gain_positive;          //!< Gain for positive values. ADC: volts/raw, transducers: trasducer_units/volts
    cc_float                    gain_negative;          //!< Gain for negative values. ADC: volts/raw, transducers: trasducer_units/volts
    struct SIG_status           status;                 //!< Calibration faults and warnings bit masks
};

struct SIG_cal                                          //!< ADC or transducer calibration structure
{
    struct SIG_cal_factors    * active;                 //!< Pointer to active calibration factors
    struct SIG_cal_factors    * next;                   //!< Pointer to next calibration factors
    struct SIG_cal_factors      factors[2];             //!< Active and next calibration factors
    union  SIG_cal_event        event;                  //!< Latest calibration event
    cc_float                    tc [SIG_NUM_CALS];      //!< Linear temperature coefficients (ppm/C)
    cc_float                    dtc[SIG_NUM_CALS];      //!< Second order temperature coefficients (ppm @ SIG_TEMP_CAL_T1)
    uint32_t                    temp_filter_index;      //!< Initialize to temperature filter index (or SIG_NOT_IN_USE)
    uint32_t                    cal_ref_index;          //!< Initialize to calibration references index (or SIG_NOT_IN_USE)
    uint32_t                    cal_limit_index;        //!< Initialize to calibration limit index (or SIG_NOT_IN_USE)
};

struct SIG_cal_ref                                      //!< Calibration reference structure
{
    union  SIG_cal_event        event;                  //!< Latest calibration errors event
    cc_float                  * temp_c;                 //!< Pointer to operating temperature of the voltage reference
    cc_float                    tc [SIG_NUM_CALS];      //!< Linear temperature coefficients (ppm/C)
};

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize active/next pointers to double buffered calibration factors
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]  cal           Pointer to calibration structure to initialize.

void sigCalInit(struct SIG_cal * cal);


//! Calculates the error for a calibration reference
//!
//!  This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in]      sig_mgr       Pointer to signal manager structure.
//! @param[in,out]  cal           Pointer to calibration structure.
//! @param[in]      cal_level     Calibration level (OFFSET, POSITIVE, NEGATIVE).
//!
//! @return         Calibration error in ppm

cc_float sigCalRefErrorPpm(struct SIG_mgr * sig_mgr, struct SIG_cal * cal, enum SIG_cal_level cal_level);


//! Check the calibration errors and temperature for a calibration event
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in]      sig_mgr              Pointer to signal manager structure.
//! @param[in,out]  cal                  Pointer to calibration structure.
//! @param[in]      cal_event            Pointer to calibration event structure.
//! @param[in]      cal_flt_bit_mask     Fault bit mask to set if the calibration errors exceed fault limits.
//! @param[in]      cal_warn_bit_mask    Warning bit mask to set if the calibration errors exceed warning limits.
//! @param[in]      temp_warn_bit_mask   Temperature warning bit mask to set if the calibration event temperature exceed warning limits.

void sigCalCheckLimits(struct SIG_mgr       * sig_mgr,
                       struct SIG_cal       * cal,
                       union  SIG_cal_event * cal_event,
                       uint32_t               cal_flt_bit_mask,
                       uint32_t               cal_warn_bit_mask,
                       uint32_t               temp_warn_bit_mask);

#ifdef __cplusplus
}
#endif

// EOF
