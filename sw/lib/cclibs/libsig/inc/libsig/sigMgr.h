//! @file  sigMgr.h
//! @brief Converter Control Signals library management functions.
//!
//! The functions provided by sigMgr.c combine all the elements needed to manage the
//! measurement signals for power converter control.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Forward reference to structures that will be declared later

struct SIG_temp_filter;
struct SIG_cal_ref;
struct SIG_adc;
struct SIG_transducer;
struct SIG_select;

// Global structures

struct SIG_mgr
{
    uint32_t                        num_temp_filters;
    uint32_t                        num_cal_refs;
    uint32_t                        num_cal_limits;
    uint32_t                        num_adcs;
    uint32_t                        num_transducers;
    uint32_t                        num_selects;

    struct SIG_temp_filter        * temp_filters;
    struct SIG_cal_ref            * cal_refs;
    struct SIG_cal_limit          * cal_limits;
    struct SIG_adc                * adcs;
    struct SIG_transducer         * transducers;
    struct SIG_select             * selects;

    struct POLSWITCH_mgr          * polswitch_mgr;
    bool                            polswitch_negative;

    union CC_debug                  debug;
};

// Global function declarations

#ifdef __cplusplus
extern "C" {
#endif

//! Background signal manager function
//!
//! This function should be called by the application in the background, once per average1 period, soon
//! after the start of each average1 period. This is typically 200ms. The function processes the
//! temperature filters and the calculates the calibration factors for the ADCs and transducers,
//! taking into account the temperatures.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out] sig_mgr                Pointer to SIG_mgr structure to initialize
//! @param[in]     start_average2         True on iterations that start an average2 period (typically 1s)

void sigMgr(struct SIG_mgr * sig_mgr, bool start_average2);


//! This real-time function should be called every iteration from the real-time thread/interrupt.
//!
//! It will process the status and values of all ADCs, using the calibration factors from sigMgr to convert
//! the raw ADC values into ADC voltages, and from there to the transducer value. It also processes
//! signal selectors.
//!
//! This is real-time function.
//!
//! @param[in,out] sig_mgr                Pointer to SIG_mgr structure to initialize
//! @param[in]     start_average1         True on iterations that start an average1 period (typically 200ms)
//! @param[in]     timestamp              Iteration time stamp (s, ns)

void sigMgrRT(struct SIG_mgr * sig_mgr, bool start_average1, const struct CC_ns_time timestamp);


//! This process will process the values of the ADCs containing fast acquisition, using the calibration
//! factors from sigMgr to convert the raw ADC values into ADC voltages, and from there to the transducer
//! value. It also processes signal selectors. It does not process the status or produce filtered values
//! as this is already done by sigMgrRT
//!
//! @param[in,out] sig_mgr                Pointer to SIG_mgr structure to initialize

void sigMgrFast(struct SIG_mgr * sig_mgr);


#ifdef __cplusplus
}
#endif

// EOF
