//! @file sigTransducer.h
//! @brief Converter Control Signals library transducer functions
//!
//! Functions for scaling and calibration of transducers.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// The transducer measurement is inverted when certain conditions are met, as indicated in this table.
// Two variables are concerned:
//
//  transducer->position            Position or orientation of the transducer.
//                                   - If the polarity switch is present then it defines the position of the transducer
//                                     relative to the polarity switch (LOAD or CONVERTER).
//                                   - If the polarity switch is not present then it defines
//                                     the orientation of the transducer (NORMAL or INVERTED).
//
//  sig_mgr->polswitch_is_negative  True if polarity switch exists and is currently in the negative position.
//
//                          +-----------------------+
//                          | POLSWITCH_IS_NEGATIVE |
//                          +-----------+-----------+
//                          |   FALSE   |   TRUE    |
// +------------+-----------+-----------+-----------+
// |            | CONVERTER | No change |  INVERT   |
// |            +-----------+-----------+-----------+
// | TRANSDUCER |   LOAD    | No change | No change |
// |            +-----------+-----------+-----------+
// |  POSITION  |   NORMAL  | No change | No change |
// |            +-----------+-----------+-----------+
// |            | INVERTED  |   INVERT  |   INVERT  |
// +------------+-----------+-----------+-----------+

struct SIG_ext_meas
{
    bool                  is_enabled;
    struct CC_meas_signal meas;
};

// Global transducer structure for DCCT and Probes (Hall probe, Voltage probe, Current probe)

struct SIG_transducer
{
    struct SIG_adc *             adc;                       //!< Pointer to ADC measuring the transducer signal
    bool                         fault_flag;                //!< Transducer fault flag

    bool                         adc_calibrating_flag;      //!< ADC is calibrating flag
    bool                         invert_if_polswitch_neg;   //!< Invert polarity if polswitch is negative flag

    bool                         fast_flag;                 //!< Transducer with a fast measurement

    cc_float                     invert_factor;             //!< +/-1 to invert or not the signal
    cc_float                     nominal_gain;              //!< Nominal gain (physical_units/volt)
    cc_float                     gain_err_ppm;              //!< Error in nominal gain in ppm
    enum CC_enabled_disabled   * ignore_gain_err_during_cal;//!< Pointer to flag: if true, gain_err_ppm is ignored when calibrating
    uint32_t                   * primary_turns_p;           //!< Pointer to number of primary turns for DCCTs (NULL to use primary_turns)
    uint32_t                     primary_turns;             //!< Number of primary turns for DCCTs if primary_turns_p == NULL

    enum SIG_transducer_position position;                  //!< Transducer position with respect to a polarity switch

    struct SIG_cal               cal;                       //!< Transducer scaling and calibration structure

    struct SIG_meas_signal       meas;                      //!< Resulting unfiltered measurement and status
    struct SIG_ext_meas          ext_meas;                  //!< Optional external signal

    int32_t                    * fast_raw;                  //!< Fast raw ADC value
    cc_float                     fast_meas;                 //!< Fast measurement
};

#ifdef __cplusplus
extern "C" {
#endif

//! Calibrates the transducer structure. The calibration should be done with three calls to this function:
//! * offset calibration
//! * positive gain calibration
//! * negative gain calibration
//! The offset calibration has to be the first call.
//!
//! The application is responsible for filling up the ADC calibration buffer before each call to this function.
//! Number of samples required to fill the buffer is defined by SIG_ADC_CAL_NUM_SAMPLES macro. The function then
//! is able to figure out which level (offset, positive/negative gain) it's calibrating. Also note that
//! the ADC should be calibrated before transducer calibration.
//!
//! If calibration of any level fails then the function returns SIG_CAL_FAULT. When the calibration succeeds
//! then it returns an enum value corresponding to the calibrated level.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in,out]  sig_mgr      Pointer to signal manager structure.
//! @param[out]     transducer   Pointer to transducer structure to calibrate.
//!
//! @return  Calibrated level or fault

enum SIG_cal_level sigTransducerCal(struct SIG_mgr * sig_mgr, struct SIG_transducer * transducer);


//! Background processing for the transducer
//!
//! This is called from SigMgr(), which is called by the application at intervals of period1.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]  sig_mgr           Pointer to signal manager structure.
//! @param[in,out]  transducer        Pointer to transducer structure.
//! @param[in]      start_average2    Set to true on iterations when the average2 should start

void sigTransducer(struct SIG_mgr * sig_mgr, struct SIG_transducer * transducer, bool start_average2);


//! Real-time processing for the transducer measurement
//!
//! @param[in,out]  sig_mgr           Pointer to signal manager structure
//! @param[in,out]  transducer        Pointer to transducer structure
//! @param[in]      start_average1    Set to true on an iteration that starts an average1 period
//!

void sigTransducerRT(struct SIG_mgr * sig_mgr, struct SIG_transducer * transducer, bool start_average1);


//! Can be used to inject external measurement to the transducer
//!
//! This should be called before calling sigTransducerRT (so before calling sigMgrRT)!
//!
//! @param[out]  transducer           Pointer to transducer structure
//! @param[in]   is_ext_meas_enabled  Enables/disables use of external measurement by the transducer
//! @param[in]   is_valid             External measurement validity flag
//! @param[in]   value                External measurement value

static inline void sigTransducerSetExtMeasRT(struct SIG_transducer * const transducer,
                                             bool                    const is_ext_meas_enabled,
                                             bool                    const is_valid,
                                             cc_float                const value)
{
    transducer->ext_meas.is_enabled = is_ext_meas_enabled;

    transducer->ext_meas.meas.is_valid = is_valid;
    transducer->ext_meas.meas.signal   = value;
}


//! Convert an ADC voltage to the transducer value using the supplied transducer calibration
//!
//! @param[in]    cal_factors            Pointer to transducer calibration factors
//! @param[out]   v_adc                  Voltage measured by the ADC
//!
//! @return       Transducer value

cc_float sigTransducerVadcToPhysUnitsRT(struct SIG_cal_factors * cal_factors, cc_float v_adc);

//! Return whether the transducer source is an ADC
//!
//! @param[out]  transducer           Pointer to transducer structure
//!
//! @retval      true                 The transducer source is an ADC
//! @retval      false                The transducer source is not and ADC

static inline bool sigTransducerSourceIsAdc(struct SIG_transducer * const transducer)
{
    return (transducer->adc != NULL);
}

#ifdef __cplusplus
}
#endif

// EOF
