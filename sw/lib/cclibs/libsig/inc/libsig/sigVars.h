//! @file  inc/libsig/sigVars.h
//!
//! @brief Converter Control Signals library : Converter Control Signals library variables header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Use sigMgrVar macros with a pointer to the sig structure.

#define sigVarValue(SIG, STRUCT_TYPE, STRUCT_NAME, VAR_NAME)         ((SIG)->STRUCT_TYPE.named.STRUCT_NAME.SIG_VAR_ ## VAR_NAME)
#define sigVarPointer(SIG, STRUCT_TYPE, STRUCT_NAME, VAR_NAME)      (&(SIG)->STRUCT_TYPE.named.STRUCT_NAME.SIG_VAR_ ## VAR_NAME)
#define sigVarValueByIdx(SIG, STRUCT_TYPE, IDX, VAR_NAME)            ((SIG)->STRUCT_TYPE.array[IDX].SIG_VAR_ ## VAR_NAME)

#define SIG_VAR_TEMP_FILTER_PERIOD                    filter_period_s                  // cc_float                     Temperature filter period
#define SIG_VAR_TEMP_FILTER_TIME_CONSTANT             time_constant_s                  // cc_float                     Temperature filter time constant
#define SIG_VAR_TEMP_FILTER_WARNING_MIN_C             warning.min_c                    // cc_float                     Minimum temperature warning threshold
#define SIG_VAR_TEMP_FILTER_WARNING_MAX_C             warning.max_c                    // cc_float                     Maximum temperature warning threshold
#define SIG_VAR_TEMP_FILTER_TEMP_C                    temp_c                           // cc_float                     Filtered temperature
#define SIG_VAR_TEMP_FILTER_INVALID                   invalid_flag                     // bool                         Invalid temperature flag
#define SIG_VAR_TEMP_FILTER_WARNING                   warning_flag                     // bool                         Temperature warning flag
#define SIG_VAR_CAL_REF_INT_CAL_EVENT                 event.f                          // cc_float *                   Internal calibration reference calibration event
#define SIG_VAR_CAL_REF_INT_CAL_EVENT_TEMP_C          event.s.temp_c                   // cc_float                     Internal calibration reference calibration event temperature
#define SIG_VAR_CAL_REF_INT_CAL_TEMP_C                temp_c                           // cc_float *                   Pointer to internal calibration reference temperature
#define SIG_VAR_CAL_REF_INT_CAL_TC                    tc                               // cc_float *                   Internal calibration references temperature coefficients
#define SIG_VAR_CAL_REF_EXT_CAL_ERR                   event.s.offset_ppm               // cc_float                     External calibration reference error
#define SIG_VAR_CAL_LIMIT_NOMINAL_OFFSET_PPM          nominal_offset_ppm               // cc_float                     Nominal offset (ppm) in case the interface has a bias
#define SIG_VAR_CAL_LIMIT_OFFSET_WARN_PPM             offset_warning_ppm               // cc_float                     Limits are nominal +/- warning level (ppm)
#define SIG_VAR_CAL_LIMIT_OFFSET_FAULT_PPM            offset_fault_ppm                 // cc_float                     Limits are nominal +/- fault level (ppm)
#define SIG_VAR_CAL_LIMIT_GAIN_ERR_WARN_PPM           gain_err_warning_ppm             // cc_float                     Limits are +/- warning level (ppm)
#define SIG_VAR_CAL_LIMIT_GAIN_ERR_FAULT_PPM          gain_err_fault_ppm               // cc_float                     Limits are +/- fault level (ppm)
#define SIG_VAR_ADC_RAW                               raw                              // int32_t                      Raw value read from the ADC
#define SIG_VAR_ADC_FAULT_FLAG                        fault_flag                       // bool                         ADC fault flag
#define SIG_VAR_ADC_CALIBRATING_FLAG                  calibrating_flag                 // bool                         ADC is being calibrated or used to calibrate something else (e.g. the DAC)
#define SIG_VAR_ADC_CALIBRATING_COUNTER               calibrating_counter              // int32_t                      Down counter to extend effect of calibrating flag by at least one average1 period
#define SIG_VAR_ADC_NOMINAL_GAIN                      nominal_gain                     // int32_t                      ADC nominal gain (delta_Raw for delta Vnominal)
#define SIG_VAR_ADC_MAX_INVALID_SAMPLES               max_invalid_samples              // uint32_t                     Max invalid samples allowed during calibration period
#define SIG_VAR_ADC_STUCK_LIMIT_POINTER               stuck.limit_p                    // uint32_t *                   Pointer to stuck limit (NULL to use STUCK_LIMIT)
#define SIG_VAR_ADC_STUCK_LIMIT                       stuck.limit                      // uint32_t                     Max repeated samples before ADC is considered to be stuck when STUCK_LIMIT_POINTER = NULL
#define SIG_VAR_ADC_CAL_NUM_SAMPLES_POINTER           cal_num_samples_p                // uint32_t *                   Pointer to number samples to average when calibration (NULL to use cal_num_samples)
#define SIG_VAR_ADC_CAL_NUM_SAMPLES                   cal_num_samples                  // uint32_t                     Number of samples to average when calibrating
#define SIG_VAR_ADC_CAL_AVERAGE_RAW                   cal_average_raw                  // int32_t *                    Array of raw ADC averages used for most recent calibration
#define SIG_VAR_ADC_CAL_EVENT                         cal.event.f                      // cc_float *                   ADC calibration event (errors timestamp temperature)
#define SIG_VAR_ADC_CAL_TC                            cal.tc                           // cc_float *                   Linear temperature coefficients
#define SIG_VAR_ADC_CAL_DTC                           cal.dtc                          // cc_float *                   Second order temperature coefficients
#define SIG_VAR_ADC_CAL_LIMIT_INDEX                   cal.cal_limit_index              // uint32_t                     Index of calibration limit structure for this ADC
#define SIG_VAR_ADC_AVE1_RAW_MEAS                     average1.raw.meas                // int32_t                      Average1 raw measurement
#define SIG_VAR_ADC_AVE1_RAW_PP                       average1.raw.peak_to_peak        // int32_t                      Average1 raw peak to peak
#define SIG_VAR_ADC_AVE1_NUM_VALID_SAMPLES            average1.raw.num_samples.valid   // uint32_t                     Average1 number of valid samples
#define SIG_VAR_ADC_AVE1_NUM_INVALID_SAMPLES          average1.raw.num_samples.invalid // uint32_t                     Average1 number of invalid samples
#define SIG_VAR_ADC_AVE2_RAW_MEAS                     average2.raw.meas                // int32_t                      Average2 raw measurement
#define SIG_VAR_ADC_AVE2_RAW_PP                       average2.raw.peak_to_peak        // int32_t                      Average2 raw peak to peak
#define SIG_VAR_ADC_AVE2_NUM_VALID_SAMPLES            average2.raw.num_samples.valid   // uint32_t                     Average2 number of valid samples
#define SIG_VAR_ADC_AVE2_NUM_INVALID_SAMPLES          average2.raw.num_samples.invalid // uint32_t                     Average2 number of invalid samples
#define SIG_VAR_ADC_TRANSDUCER_INDEX                  transducer_index                 // uint32_t                     Index of transducer using this ADCs signal
#define SIG_VAR_ADC_STATUS                            meas.status                      // struct SIG_status            Measurement status bit masks
#define SIG_VAR_ADC_FAULTS                            meas.status.faults               // uint32_t                     Measurement faults bit mask
#define SIG_VAR_ADC_WARNINGS                          meas.status.warnings             // uint32_t                     Measurement warnings bit mask
#define SIG_VAR_ADC_MEAS_SIGNAL                       meas.unfiltered                  // struct CC_meas_signal        Libreg measurement structure (signal and validity)
#define SIG_VAR_ADC_MEAS_IS_VALID                     meas.unfiltered.is_valid         // bool                         Measured signal validity
#define SIG_VAR_ADC_MEAS_UNFILTERED                   meas.unfiltered.signal           // cc_float                     Unfiltered signal
#define SIG_VAR_ADC_MEAS_FILTERED_IS_VALID            meas.filtered.is_valid           // bool                         Filtered measured signal validity
#define SIG_VAR_ADC_MEAS_FILTERED                     meas.filtered.signal             // cc_float                     Filtered signal
#define SIG_VAR_ADC_MEAS_AVE1_MEAN                    meas.average1.mean               // cc_float                     Average1 mean
#define SIG_VAR_ADC_MEAS_AVE1_PP                      meas.average1.peak_to_peak       // cc_float                     Average1 peak to peak
#define SIG_VAR_ADC_MEAS_AVE2_MEAN                    meas.average2.mean               // cc_float                     Average2 mean
#define SIG_VAR_ADC_MEAS_AVE2_PP                      meas.average2.peak_to_peak       // cc_float                     Average2 peak to peak
#define SIG_VAR_TRANSDUCER_FAULT_FLAG                 fault_flag                       // bool                         Transducer fault flag
#define SIG_VAR_TRANSDUCER_ADC_CALIBRATING_FLAG       adc_calibrating_flag             // bool                         ADC calibrating flag (extended by at least one average1 period)
#define SIG_VAR_TRANSDUCER_NOMINAL_GAIN               nominal_gain                     // cc_float                     Transducer nominal gain (physical units/volt)
#define SIG_VAR_TRANSDUCER_GAIN_ERR_PPM               gain_err_ppm                     // enum REG_status              Gain error
#define SIG_VAR_TRANSDUCER_IGNORE_GAIN_ERR_DURING_CAL ignore_gain_err_during_cal       // enum CC_enabled_disabled *   Pointer to flag: if true then gain_err_ppm is ignored during calibration
#define SIG_VAR_TRANSDUCER_PRIMARY_TURNS_POINTER      primary_turns_p                  // uint32_t *                   Point to primary turns for a DCCT (NULL to use primary_turns)
#define SIG_VAR_TRANSDUCER_PRIMARY_TURNS              primary_turns                    // uint32_t                     Primary turns for a DCCT (0 if unused)
#define SIG_VAR_TRANSDUCER_POSITION                   position                         // enum SIG_transducer_position Transducer position with respect to a polarity switch
#define SIG_VAR_TRANSDUCER_CAL_EVENT                  cal.event.f                      // cc_float *                   Transducer calibration event
#define SIG_VAR_TRANSDUCER_CAL_TC                     cal.tc                           // cc_float *                   Linear temperature coefficients
#define SIG_VAR_TRANSDUCER_CAL_DTC                    cal.dtc                          // cc_float *                   Second order temperature coefficients
#define SIG_VAR_TRANSDUCER_STATUS                     meas.status                      // struct SIG_status            Measurement status bit masks
#define SIG_VAR_TRANSDUCER_FAULTS                     meas.status.faults               // uint32_t                     Measurement faults bit mask
#define SIG_VAR_TRANSDUCER_WARNINGS                   meas.status.warnings             // uint32_t                     Measurement warnings bit mask
#define SIG_VAR_TRANSDUCER_MEAS_SIGNAL                meas.unfiltered                  // struct CC_meas_signal        Libreg measurement structure (signal and validity)
#define SIG_VAR_TRANSDUCER_MEAS_IS_VALID              meas.unfiltered.is_valid         // bool                         Measured signal validity
#define SIG_VAR_TRANSDUCER_MEAS_UNFILTERED            meas.unfiltered.signal           // cc_float                     Unfiltered signal
#define SIG_VAR_TRANSDUCER_MEAS_FILTERED_IS_VALID     meas.filtered.is_valid           // bool                         Filtered measured signal validity
#define SIG_VAR_TRANSDUCER_MEAS_FILTERED              meas.filtered.signal             // cc_float                     Filtered signal
#define SIG_VAR_TRANSDUCER_MEAS_AVE1_MEAN             meas.average1.mean               // cc_float                     Average1 mean
#define SIG_VAR_TRANSDUCER_MEAS_AVE1_PP               meas.average1.peak_to_peak       // cc_float                     Average1 peak to peak
#define SIG_VAR_TRANSDUCER_MEAS_AVE2_MEAN             meas.average2.mean               // cc_float                     Average2 mean
#define SIG_VAR_TRANSDUCER_MEAS_AVE2_PP               meas.average2.peak_to_peak       // cc_float                     Average2 peak to peak
#define SIG_VAR_TRANSDUCER_FAST_MEAS                  fast_meas                        // cc_float                     Fast measurement
#define SIG_VAR_TRANSDUCER_EXT_MEAS_IS_ENABLED        ext_meas.is_enabled              // bool                         External signal control
#define SIG_VAR_TRANSDUCER_EXT_MEAS_SIGNAL            ext_meas.meas.signal             // cc_float                     External signal
#define SIG_VAR_TRANSDUCER_EXT_MEAS_IS_VALID          ext_meas.meas.is_valid           // bool                         External signal validity
#define SIG_VAR_SELECT_SELECTOR                       selector                         // enum SIG_selector            User signal selector
#define SIG_VAR_SELECT_ACTUAL_SELECTOR                actual_selector                  // enum SIG_selector            Actual signal selector
#define SIG_VAR_SELECT_DIFF_WARN_LIMIT                diff_warn_limit                  // cc_float                     Signal difference warning limit
#define SIG_VAR_SELECT_DIFF_FAULT_LIMIT               diff_fault_limit                 // cc_float                     Signal difference fault limit
#define SIG_VAR_SELECT_DIFF_FAULT_WARN_RATIO          diff_fault_warn_ratio            // cc_float                     Diff fault limit to warn limit ration (0 if not in use)
#define SIG_VAR_SELECT_ABS_DIFF                       abs_diff                         // cc_float                     Absolute difference between input signals
#define SIG_VAR_SELECT_MILLI_ABS_DIFF                 milli_abs_diff                   // int16_t                      Clipped signed short integer of abs_diff x 1000
#define SIG_VAR_SELECT_STATUS                         meas.status                      // struct SIG_status            Measurement status bit masks
#define SIG_VAR_SELECT_FAULTS                         meas.status.faults               // uint32_t                     Measurement faults bit mask
#define SIG_VAR_SELECT_WARNINGS                       meas.status.warnings             // uint32_t                     Measurement warnings bit mask
#define SIG_VAR_SELECT_MEAS_SIGNAL                    meas.unfiltered                  // struct CC_meas_signal        Libreg measurement structure (signal and validity)
#define SIG_VAR_SELECT_MEAS_IS_VALID                  meas.unfiltered.is_valid         // bool                         Measured signal validity
#define SIG_VAR_SELECT_MEAS_UNFILTERED                meas.unfiltered.signal           // cc_float                     Unfiltered signal
#define SIG_VAR_SELECT_MEAS_FILTERED_IS_VALID         meas.filtered.is_valid           // bool                         Filtered measured signal validity
#define SIG_VAR_SELECT_MEAS_FILTERED                  meas.filtered.signal             // cc_float                     Filtered signal
#define SIG_VAR_SELECT_MEAS_AVE1_MEAN                 meas.average1.mean               // cc_float                     Average1 mean
#define SIG_VAR_SELECT_MEAS_AVE1_PP                   meas.average1.peak_to_peak       // cc_float                     Average1 peak to peak
#define SIG_VAR_SELECT_MEAS_AVE2_MEAN                 meas.average2.mean               // cc_float                     Average2 mean
#define SIG_VAR_SELECT_MEAS_AVE2_PP                   meas.average2.peak_to_peak       // cc_float                     Average2 peak to peak
#define SIG_VAR_SELECT_FAST_MEAS                      fast_meas                        // cc_float                     Fast measurement
#define SIG_VAR_DAC_MIN_RAW                           min_raw_dac                      // int32_t                      Minimum raw value
#define SIG_VAR_DAC_MAX_RAW                           max_raw_dac                      // int32_t                      Maximum raw value

// EOF
