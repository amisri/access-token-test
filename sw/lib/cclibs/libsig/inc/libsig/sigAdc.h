//! @file sigAdc.h
//! @brief Converter Control Signals library ADC functions
//!
//! Functions for scaling and calibration of ADCs.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global ADC constants

// Notes on the FIR filters:
//
// The FIR1 and FIR2 stages are fixed with the objective of providing an anti-aliasing filter before decimating
// by one decade. The noise will be attenuated by about a factor 9, or -19dB. The filter delay will be
// (SIG_ADC_FIR1_LEN + SIG_ADC_FIR2_LEN) / 2 - 1 = 8 periods.
//
// The FIR1 and FIR2 stages accumulate the raw ADC value in signed 32 bit integers. This imposes a limit
// on the maximum value of the raw ADC value of 2^31 / (SIG_ADC_FIR1_LEN * SIG_ADC_FIR2_LEN) which is about
// 26M. So the ADC must not emit great than a signed 25 bit value. This should not be a significant limitation.


#define SIG_ADC_FIR1_LEN        10                              //!< ADC FIR filter stage 1 length
#define SIG_ADC_FIR2_LEN        8                               //!< ADC FIR filter stage 2 length
#define SIG_ADC_CIRC_BUF_LEN    64                              //!< ADC circular buffer length (must be power of 2)

 // Global ADC structures

struct SIG_adc_stuck
{
    int32_t                     prev_raw;                       //!< Previous raw value
    uint32_t                    counter;                        //!< Signal stuck counter
    uint32_t                  * limit_p;                        //!< Pointer to signal stuck limit (NULL to use limit)
    uint32_t                    limit;                          //!< Signal stuck limit if limit_p == NULL
};

struct SIG_adc_num_samples
{
    uint32_t                    valid;                          //!< Number of valid samples
    uint32_t                    invalid;                        //!< Number of invalid samples
};

struct SIG_adc_raw
{
    struct CC_ns_time           timestamp;                      //!< Timestamp of the first sample of the period (unix time + ns time)
    struct SIG_adc_num_samples  num_samples;                    //!< Valid and invalid samples counters for the averaging period
    int32_t                     meas;                           //!< Raw ADC measurement averaged over previous accumulation period
    int32_t                     peak_to_peak;                   //!< Raw ADC peak-peak during previous accumulation period
};

struct SIG_adc_acc
{
    struct SIG_adc_num_samples  num_samples;                    //!< Valid and invalid samples counters for the accumulation period
    int32_t                     min;                            //!< Minimum raw ADC value
    int32_t                     max;                            //!< Maximum raw ADC value
    cc_double                   raw;                            //!< Accumulator/sum of raw signal
};

struct SIG_adc_average
{
    struct SIG_adc_acc          accumulator;                    //!< Raw value accumulator
    struct SIG_adc_acc          sum;                            //!< Result of previous accumulation
    struct SIG_adc_raw          raw;                            //!< Average and peak-peak raw from previous accumulation
};

struct SIG_adc_circ_buf
{
    uint32_t                    last_sample_index;              //!< Index to most recently added sample
    uint32_t                    readout_sample_index;           //!< Readout index

    struct SIG_adc_raw          buffer[SIG_ADC_CIRC_BUF_LEN];   //!< Circular buffer for calibration and FIFO log
};

struct SIG_adc_fir_filter
{
    uint32_t                    index1;                         //!< FIR1 index
    int32_t                     accumulator1;                   //!< FIR1 accumulator
    int32_t                     buf1[SIG_ADC_FIR1_LEN];         //!< FIR1 buffer
    uint32_t                    index2;                         //!< FIR2 index
    int32_t                     accumulator2;                   //!< FIR2 accumulator
    int32_t                     buf2[SIG_ADC_FIR2_LEN];         //!< FIR2 buffer
    uint32_t                    invalidity_fifo_bitmask;        //!< FIFO containing validity of samples pushed to the filter buffers
};

struct SIG_adc
{
    int32_t                     raw;                            //!< Raw ADC value

    bool                        fault_flag;                     //!< ADC fault flag
    bool                        calibrating_flag;               //!< ADC is being calibrated, or is being used to calibrate something else (e.g. the DAC)

    uint32_t                    calibrating_counter;            //!< Down counter to extend calibrating flag by at least one average1 period
    uint32_t                    nominal_gain;                   //!< Nominal ADC gain in raw (per nominal voltage)
    uint32_t                    max_invalid_samples;            //!< Max invalid samples allowed per average
    uint32_t                  * cal_num_samples_p;              //!< Pointer to number of samples needed for calibration average (NULL to set cal_num_samples)
    uint32_t                    cal_num_samples;                //!< Number of samples needed for calibration average if cal_num_samples_p == NULL
    int32_t                     cal_average_raw[SIG_NUM_CALS];  //!< Average raw used for most recent ADC calibration
    struct SIG_cal              cal;                            //!< ADC scaling and calibration structure
    struct SIG_adc_average      average1;                       //!< Average at rate 1 (real-time)
    struct SIG_adc_average      average2;                       //!< Average at rate 2 (background)
    struct SIG_adc_circ_buf     circ_buf;                       //!< Circular buffer for calibration and FIFO log
    struct SIG_adc_stuck        stuck;                          //!< Signal stuck detection
    struct SIG_adc_fir_filter   fir_filter;                     //!< FIR filter

    struct SIG_meas_signal      meas;                           //!< Resulting measurement and status

    uint32_t                    transducer_index;               //!< Transducer (SIG_NOT_IN_USE, 0, 1, 2, ...)
};

#ifdef __cplusplus
extern "C" {
#endif

//! Reset ADC status, measurement and calibration counter.
//!
//! This is called when the ADC transducer index is set to NOT_IN_USE
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[out]   adc                  Pointer to adc structure.

void sigAdcOff(struct SIG_adc * adc);


//! Read one record from ADC's circular buffer.
//!
//! If the FIFO overruns, it is cleared and a first call to this function is needed to reset it.
//! Subsequent calls will return records if they are pending.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[out]   adc                  Pointer to adc structure.
//! @param[out]   adc_raw              Pointer adc_raw struct to receive record if available
//!
//! @retval       true   if record is being returned in *adc_raw
//! @retval       false  if no records available.

bool sigAdcGetCircBuf(struct SIG_adc     * adc,
                      struct SIG_adc_raw * adc_raw);


//! This will use the circular buffer of average1 values to return the average raw ADC measurement
//! for
//!
//! Notes
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in]   adc                  Pointer to adc structure.
//! @param[out]  average_raw          Pointer to variable to receive the average raw ADC measurement
//!
//! @retval       true   if record is being returned in *adc_raw
//! @retval       false  if no records available.

bool sigAdcCalAverage(struct SIG_adc * adc,
                      int32_t        * average_raw);


//! Calibrates the ADC structure. The calibration should be done with three calls to this function:
//! * offset calibration
//! * positive gain calibration
//! * negative gain calibration
//! The offset calibration has to be the first call.
//!
//! The application is responsible for filling up the ADC calibration buffer before each call to this function.
//! Number of samples required to fill the buffer is defined by SIG_ADC_CAL_NUM_SAMPLES macro. The function then
//! is able to figure out which level (offset, positive/negative gain) it's calibrating.
//!
//! If calibration of any level fails then the function returns SIG_CAL_FAULT. When the calibration succeeds
//! then it returns an enum value corresponding to the calibrated level.
//!
//! This is a background function: do not call from the real-time thread or interrupt.
//!
//! @param[in, out] sig_mgr      Pointer to signal manager structure.
//! @param[out]     adc          Pointer to ADC structure to calibrate.
//!
//! @return  Calibrated level or fault

enum SIG_cal_level sigAdcCal(struct SIG_mgr * sig_mgr,
                             struct SIG_adc * adc);


//! Process ADCs in the background
//!
//! This is called from SigMgr(), which is called by the application at intervals of period1.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in, out] sig_mgr           Pointer to signal manager structure.
//! @param[in, out] adc               Pointer to ADC structure.
//! @param[in]      start_average2    Set to true on an iteration that starts an average2 period

void sigAdc(struct SIG_mgr * sig_mgr,
            struct SIG_adc * adc,
            bool             start_average2);


//! Real-time processing for the ADC measurement
//!
//! This is called from SigMgrRT().
//!
//! @param[in, out] sig_mgr           Pointer to signal manager structure.
//! @param[in, out] adc               Pointer to ADC structure.
//! @param[in]      start_average1    Set to true on an iteration that starts an average1 period
//! @param[in]      timestamp         Time stamp for this iteration (s, ns)
//! @param[in]      faults_mask       ADC faults mask to merge into ADC faults

void sigAdcRT(struct SIG_mgr          * sig_mgr,
              struct SIG_adc          * adc,
              bool                      start_average1,
              const struct CC_ns_time   timestamp,
              uint32_t                  faults_mask);


//! Convert a raw ADC measurement into volts using the supplied ADC calibration
//!
//! @param[in]    cal_factors            Pointer to the ADC calibration factors
//! @param[out]   raw                    Raw ADC measurement
//!
//! @return       Voltage measured by the ADC

cc_float sigAdcRawToVoltsRT(struct SIG_cal_factors * cal_factors,
                            int32_t                  raw);

#ifdef __cplusplus
}
#endif

// EOF
