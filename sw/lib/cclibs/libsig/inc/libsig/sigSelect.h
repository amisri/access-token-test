//! @file sigSelect.h
//! @brief Converter Control Signals library signal selection functions
//!
//! Functions for selecting and checking two measurements of the same quantity.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Global signal select structures

struct SIG_select
{
    enum SIG_selector               selector;               //!< User signal selector parameter
    enum SIG_selector               actual_selector;        //!< Actual signal selector (based on selector and the validity of the input signals)
    uint32_t                        transducer_a_index;     //!< Initialize to transducer A index
    uint32_t                        transducer_b_index;     //!< Initialize to transducer B index
    uint32_t                        diff_fault_counter;     //!< Iteration counter for difference fault exceeded
    cc_float                        diff_warn_limit;        //!< Difference warning limit (0 if not in use)
    cc_float                        diff_fault_limit;       //!< Difference fault limit (0 if not in use)
    cc_float                        diff_fault_warn_ratio;  //!< Fault to warning ratio to set fault limit from the warn limit (0 if not in use)
    struct SIG_meas_signal          meas;                   //!< Resulting measured signal and status
    cc_float                        fast_meas;              //!< Fast measurement
    cc_float                        abs_diff;               //!< Absolute difference as a float
    int16_t                         milli_abs_diff;         //!< Absolute difference as a signed short integer with milli units
    bool                            fast_flag;              //!< Selector with a fast measurement
};

#ifdef __cplusplus
extern "C" {
#endif

//! Background processing for signal selector.
//!
//! This is called from sigMgr(), which is called at the average1 period. It will propagate the
//! average1 and average2 for the input transducers, according to the actual selector.
//!
//! @param[in]    sig_mgr               Pointer to signal manager structure
//! @param[in]    select                Pointer to signal selector structure
//! @param[in]    start_average2        True if average2 period started on this iteration

void sigSelect(struct SIG_mgr * sig_mgr, struct SIG_select * select, bool start_average2);


//! Real-time processing for signal selector.
//!
//! This is called from sigMgrRT(). It will respect the user selector parameter (AB, A or B).
//!
//! When AB is selected, the validity of the input transducer signals will be monitored and
//! if a signal becomes invalid, the selector will automatically switch to the other signal.
//! When both signals are valid, the difference between them is computed and checked against
//! warning and fault limits.
//!
//! The funct
//!
//! @param[in]    sig_mgr               Pointer to signal manager structure
//! @param[in]    select                Pointer to signal selector structure

void sigSelectRT(struct SIG_mgr * sig_mgr, struct SIG_select *select);

#ifdef __cplusplus
}
#endif

// EOF
