//! @file  sigConsts.h
//! @brief Converter Control Signals library public constants header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

//! Generic libsig constants

enum
{
    SIG_NOT_IN_USE = 99                                 //!< Entity not in use index
};

//! ADC fault bits - four bits are reserved for use by the application

enum SIG_adc_fault_bit
{
    SIG_ADC_FLT_BIT              ,
    SIG_ADC_STUCK_FLT_BIT        ,
    SIG_ADC_CAL_FLT_BIT          ,
    SIG_ADC_XDCR_IN_USE_FLT_BIT  ,
    SIG_ADC_APP_0_FLT_BIT        ,
    SIG_ADC_APP_1_FLT_BIT        ,
    SIG_ADC_APP_2_FLT_BIT        ,
    SIG_ADC_APP_3_FLT_BIT        ,
    SIG_ADC_NUM_FAULT_BITS
};

enum SIG_adc_fault_bit_mask
{
    SIG_ADC_FLT_BIT_MASK             = (1<<SIG_ADC_FLT_BIT            ) ,
    SIG_ADC_STUCK_FLT_BIT_MASK       = (1<<SIG_ADC_STUCK_FLT_BIT      ) ,
    SIG_ADC_CAL_FLT_BIT_MASK         = (1<<SIG_ADC_CAL_FLT_BIT        ) ,
    SIG_ADC_XDCR_IN_USE_FLT_BIT_MASK = (1<<SIG_ADC_XDCR_IN_USE_FLT_BIT) ,
    SIG_ADC_APP_0_FLT_BIT_MASK       = (1<<SIG_ADC_APP_0_FLT_BIT      ) ,
    SIG_ADC_APP_1_FLT_BIT_MASK       = (1<<SIG_ADC_APP_1_FLT_BIT      ) ,
    SIG_ADC_APP_2_FLT_BIT_MASK       = (1<<SIG_ADC_APP_2_FLT_BIT      ) ,
    SIG_ADC_APP_3_FLT_BIT_MASK       = (1<<SIG_ADC_APP_3_FLT_BIT      )
};

//! ADC warning bits - four bits are reserved for use by the application

enum SIG_adc_warning_bit
{
    SIG_ADC_TEMP_WARN_BIT        ,
    SIG_ADC_CAL_WARN_BIT         ,
    SIG_ADC_RSVD_0_WARN_BIT      ,
    SIG_ADC_RSVD_1_WARN_BIT      ,
    SIG_ADC_NUM_WARNING_BITS
};

enum SIG_adc_warning_bit_mask
{
    SIG_ADC_TEMP_WARN_BIT_MASK   = (1<<SIG_ADC_TEMP_WARN_BIT) ,
    SIG_ADC_CAL_WARN_BIT_MASK    = (1<<SIG_ADC_CAL_WARN_BIT )
};

//! Transducer fault bits - four bits are reserved for use by the application

enum SIG_transducer_fault_bit
{
    SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT ,
    SIG_TRANSDUCER_ADC_FLT_BIT         ,
    SIG_TRANSDUCER_FLT_BIT             ,
    SIG_TRANSDUCER_CAL_FLT_BIT         ,
    SIG_TRANSDUCER_EXT_FLT_BIT         ,
    SIG_TRANSDUCER_APP_0_FLT_BIT       ,
    SIG_TRANSDUCER_APP_1_FLT_BIT       ,
    SIG_TRANSDUCER_APP_2_FLT_BIT       ,
    SIG_TRANSDUCER_APP_3_FLT_BIT       ,
    SIG_TRANSDUCER_NUM_FAULT_BITS
};

enum SIG_transducer_fault_bit_mask
{
    SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK = (1<<SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT ) ,
    SIG_TRANSDUCER_ADC_FLT_BIT_MASK         = (1<<SIG_TRANSDUCER_ADC_FLT_BIT         ) ,
    SIG_TRANSDUCER_FLT_BIT_MASK             = (1<<SIG_TRANSDUCER_FLT_BIT             ) ,
    SIG_TRANSDUCER_CAL_FLT_BIT_MASK         = (1<<SIG_TRANSDUCER_CAL_FLT_BIT         ) ,
    SIG_TRANSDUCER_EXT_FLT_BIT_MASK         = (1<<SIG_TRANSDUCER_EXT_FLT_BIT         )
};

//! Transducer warning bits - four bits are reserved for use by the application

enum SIG_transducer_warning_bit
{
    SIG_TRANSDUCER_ADC_WARN_BIT        ,
    SIG_TRANSDUCER_TEMP_WARN_BIT       ,
    SIG_TRANSDUCER_CAL_WARN_BIT        ,
    SIG_TRANSDUCER_APP_0_WARN_BIT      ,
    SIG_TRANSDUCER_APP_1_WARN_BIT      ,
    SIG_TRANSDUCER_APP_2_WARN_BIT      ,
    SIG_TRANSDUCER_APP_3_WARN_BIT      ,
    SIG_TRANSDUCER_APP_4_WARN_BIT      ,
    SIG_TRANSDUCER_APP_5_WARN_BIT      ,
    SIG_TRANSDUCER_NUM_WARN_BITS
};

enum SIG_transducer_warning_bit_mask
{
    SIG_TRANSDUCER_ADC_WARN_BIT_MASK  = (1<<SIG_TRANSDUCER_ADC_WARN_BIT ) ,
    SIG_TRANSDUCER_TEMP_WARN_BIT_MASK = (1<<SIG_TRANSDUCER_TEMP_WARN_BIT) ,
    SIG_TRANSDUCER_CAL_WARN_BIT_MASK  = (1<<SIG_TRANSDUCER_CAL_WARN_BIT )
};

//! Transducer position constants

enum SIG_transducer_position
{
    SIG_TRANSDUCER_POSITION_CONVERTER ,
    SIG_TRANSDUCER_POSITION_LOAD      ,
    SIG_TRANSDUCER_POSITION_NORMAL    ,
    SIG_TRANSDUCER_POSITION_INVERTED  ,
    SIG_TRANSDUCER_NUM_POSITIONS
};

//! Signal selector fault bits

enum SIG_select_fault_bit
{
    SIG_SELECT_SIG_FLT_BIT                ,             //!< Output signal is invalid
    SIG_SELECT_DIFF_FLT_BIT               ,
    SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT ,
    SIG_SELECT_NUM_FAULT_BITS
};

enum SIG_select_fault_bit_mask
{
    SIG_SELECT_SIG_FLT_BIT_MASK                = (1<<SIG_SELECT_SIG_FLT_BIT               ) ,
    SIG_SELECT_DIFF_FLT_BIT_MASK               = (1<<SIG_SELECT_DIFF_FLT_BIT              ) ,
    SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK = (1<<SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT) ,
};

//! Signal selector warning bits

enum SIG_select_warning_bit
{
    SIG_SELECT_SIG_INVALID_WARN_BIT   ,                 //!< One signal is invalid and is being ignored
    SIG_SELECT_SIG_WARN_WARN_BIT      ,                 //!< A contributing signal has warnings
    SIG_SELECT_DIFF_WARN_BIT          ,                 //!< The signal difference exceeds the warning threshold
    SIG_SELECT_NUM_WARN_BITS
};

enum SIG_select_warning_bit_mask
{
    SIG_SELECT_SIG_INVALID_WARN_BIT_MASK = (1<<SIG_SELECT_SIG_INVALID_WARN_BIT) ,
    SIG_SELECT_SIG_WARN_WARN_BIT_MASK    = (1<<SIG_SELECT_SIG_WARN_WARN_BIT   ) ,
    SIG_SELECT_DIFF_WARN_BIT_MASK        = (1<<SIG_SELECT_DIFF_WARN_BIT       )
};

//! Signal selector control constants

enum SIG_selector
{
    SIG_A             ,                                 //!< Use channel A
    SIG_B             ,                                 //!< Use channel B
    SIG_AB            ,                                 //!< Use average of channels A & B if both valid
    SIG_NONE          ,                                 //!< Use neither channel (actual_selector only)
    SIG_NUM_SELECTORS
};

//! Signal calibration level constants

enum SIG_cal_level
{
    SIG_CAL_OFFSET     ,                                //!< Offset calibration
    SIG_CAL_POSITIVE   ,                                //!< Calibration of positive level
    SIG_CAL_NEGATIVE   ,                                //!< Calibration of negative level
    SIG_NUM_CALS       ,                                //!< Number of calibration levels
    SIG_CAL_FAULT      ,                                //!< Indicates calibration fault
    SIG_CAL_NONE                                        //!< To be used by the application
};

// EOF
