//! @file sigTemp.h
//! @brief Converter Control Signals library temperature functions
//!
//! Functions for filtering the temperature measurement and using it for calibration compensation
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Temperature related constants

static const cc_float SIG_TEMP_CAL_T0 = 23.0;         //!< T0 calibration temperature: All errors are normalized to T0
static const cc_float SIG_TEMP_CAL_T1 = 28.0;         //!< T1 calibration temperature: Second order DTC applies at T1
static const cc_float SIG_TEMP_CAL_T2 = 33.0;         //!< T2 calibration temperature: Second order effect is zero at T0 and T2

// Forward reference to structures that will be declared later

struct SIG_cal;

// Global temperature measurement filter and validation structure

struct SIG_temp_min_max
{
    cc_float                    min_c;                          //!< Minimum (C)
    cc_float                    max_c;                          //!< Maximum (C)
    cc_float                  * max_c_p;                        //!< Pointer to maximum (C)
};

struct SIG_temp_filter                                          //!< Temperature filter structure
{
    cc_float                    last_temp_c;                    //!< Last measured temperature
    cc_float                    last_valid_temp_c;              //!< Last valid temperature
    cc_float                    temp_c;                         //!< Filtered temperature
    cc_float                    filter_period_s;                //!< Filter period in seconds
    cc_float                  * time_constant_s_p;              //!< Pointer to filter time constant (s) (NULL to use time_constant_s)
    cc_float                    time_constant_s;                //!< Filter time constant (s) if time_constant_s_p == NULL
    struct SIG_temp_min_max     warning;                        //!< Warning thresholds
    struct SIG_temp_min_max     invalid;                        //!< Invalid measurement thresholds
    cc_float                    max_delta_temp_c;               //!< Maximum valid temperature step
    uint32_t                    invalid_counter;                //!< Invalid measurement counter
    bool                        invalid_flag;                   //!< Invalid measurement flag
    bool                        warning_flag;                   //!< Temperature warning flag
};

#ifdef __cplusplus
extern "C" {
#endif

//! Initialize temperature filter to default value
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]    temp_filter         Pointer to temperature filter structure to initialize.

void sigTempInit(struct SIG_temp_filter * temp_filter);


//! Pass a temperature measurement to libsig
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]    temp_filter         Pointer to temperature filter structure to initialize.
//! @param[in]        measured_temp_c     New temperature measurement in degrees C

void sigTempMeas(struct SIG_temp_filter * temp_filter, cc_float measured_temp_c);


//! Run one iteration of the first-order temperature filter.
//!
//! The temperature input comes from the most recent call to sigTempMeas()
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]    temp_filter         Pointer to temperature filter structure.

void sigTempFilter(struct SIG_temp_filter * temp_filter);


//! Calculate the temperature error using first or second order model.
//!
//! This is a background function: do not call from the real-time thread or interrupt
//!
//! @param[in,out]  sig_mgr               Pointer to SIG_mgr structure to initialize
//! @param[in,out]  cal                   Pointer to calibration structure.
//! @param[in]      cal_level             Calibration level (OFFSET, POSITIVE, NEGATIVE).
//!
//! @return         Temperature error in ppm

cc_float sigTempErrorPpm(struct SIG_mgr * sig_mgr, struct SIG_cal * cal, enum SIG_cal_level cal_level);

#ifdef __cplusplus
}
#endif

// EOF
