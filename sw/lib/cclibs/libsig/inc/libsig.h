//! @file  libsig.h
//! @brief Converter Control Signals library header file
//!
//! The converter control signals library provides support for:
//!
//! 1. ADCs
//! 2. Transducers such as DCCTs, hall probes, voltage and current probes
//! 3. DACs
//! 4. Calibration voltage references
//! 5. Temperature filters
//! 5. Calibration limits (for transducers or ADCs)
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "cclibs.h"

// Signal status structure

struct SIG_status
{
    uint32_t                    faults;                 //!< Calibration fault bit mask
    uint32_t                    warnings;               //!< Calibration warning bit mask
};

// Measured signal structures

struct SIG_meas_average
{
    cc_float                    mean;                   //!< Average measurement during given period
    cc_float                    peak_to_peak;           //!< Measurement peak-peak during given period
};

struct SIG_meas_signal
{
    struct SIG_status           status;                 //!< Measurement faults and warnings bit masks
    struct CC_meas_signal       unfiltered;             //!< Unfiltered measurement and validity
    struct CC_meas_signal       filtered;               //!< Filtered measurement and validity
    struct SIG_meas_average     average1;               //!< Average and peak-peak over period 1
    struct SIG_meas_average     average2;               //!< Average and peak-peak over period 2
};

// Include all libsig header files

#include "libpolswitch.h"
#include "libsig/sigMgr.h"
#include "libsig/sigTemp.h"
#include "libsig/sigCal.h"
#include "libsig/sigAdc.h"
#include "libsig/sigTransducer.h"
#include "libsig/sigSelect.h"
#include "libsig/sigDac.h"
#include "libsig/sigVars.h"

// EOF
