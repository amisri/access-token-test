# libsig/scripts/write_h.awk
#
# Converter Control Signals library : Structures header files writer
#
# Libsig manages five entities in interlocked structures. This program converts
# a csv file defining the relationship between the entities into header files
# that declare and initialize the structures that represent the entities.
# It writes sigStructs.h and sigStructsInit.h.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libsig.
#
# libsig is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

    # Get output path from command line

    if(ARGC < 3)
    {
        printf "usage: write_h.awk input_csv_file1 input_csv_file2 ... output_path\n"
        exit 1
    }

    ARGC--

    gsub(/\/+$/, "", ARGV[ARGC]) # remove trailing slashes from output_path

    output_path = ARGV[ARGC]

    # Read sig CSV file(s) and then generate sigStructs header files.

    ReadCSV()

    WriteSigStructs(output_path "/sigStructs.h")

    WriteSigStructsInit(output_path "/sigStructsInit.h")
}



function ReadCSV()
{
    # Set field separater to comma to read csv file

    FS = ","

    # Identify all possible libsig entities

    struct_types["temp_filter"] = 1
    struct_types["cal_ref"]     = 1
    struct_types["cal_limit"]   = 1
    struct_types["adc"]         = 1
    struct_types["transducer"]  = 1
    struct_types["select"]      = 1

    # Identify the columns in the csv file

    struct_column   = 1
    field_column    = 2
    type_column     = 3
    value_column    = 4

    # Read structure definitions from input CSV file

    num_struct_types = 0

    for(i = 1 ; i < ARGC ; i++)
    {
        filename = ARGV[i]

        # Read next file to the end

        line_num = 0

        while((getline < filename) > 0)
        {
            line_num++

            # Skip blank lines or comment lines

            if($0 == "" || $struct_column == "#") continue

            # Save contents

            if($struct_column != "")
            {
                struct_type = $struct_column

                if(!(struct_type in struct_types))
                {
                    # Invalid structure type

                    Error(filename, line_num, "Invalid struct type (" struct_type ")\n")
                }

                if(!(struct_type in num_structs))
                {
                    # New structure type

                    struct_types[num_struct_types++] = struct_type

                    num_structs[struct_type] = 0
                }

                struct_idx  = num_structs[struct_type]++

                struct_name = $field_column

                struct_index[struct_type, struct_name] = struct_idx

                struct_names[struct_type, struct_idx] = struct_name

                struct_num_fields[struct_type, struct_name] = 0

            }
            else
            {
                field_name  = $field_column
                field_type  = $type_column
                field_value = $value_column
                field_idx   = struct_num_fields[struct_type, struct_name]

                if(field_type == "") continue

                if(field_type == "const")
                {
                    field_names [struct_type, struct_name, field_idx] = field_name
                    field_values[struct_type, struct_name, field_idx] = field_value
                }
                else if(field_type in num_structs)
                {
                    if(struct_index[field_type, field_value] == "")
                    {
                        Error(filename, line_num, "Unknown struct name (" field_value ") for " struct_type "->" struct_name ":" field_name "->" field_type "\n")
                    }

                    field_names [struct_type, struct_name, field_idx] = field_name
                    field_values[struct_type, struct_name, field_idx] = struct_index[field_type, field_value]
                }
                else
                {
                    Error(filename, line_num, "Unknown struct type (" field_type ") for " struct_type "->" struct_name "\n")
                }

                struct_num_fields[struct_type, struct_name]++
            }
        }

        close(filename)
    }
}



function WriteSigStructs(of,  n, struct_type_idx, struct_type, struct_type_uc, struct_idx)
{
    WriteGnuLicense("libsig", "Signals", "Structures header file", of)

    print "#pragma once\n"                                                                                                                   > of

    print "#include \"libsig.h\"\n"                                                                                                          > of

    print "// Signals enums\n"                                                                                                               > of


    for(struct_type_idx = 0 ; struct_type_idx < num_struct_types ; struct_type_idx++)
    {
        struct_type = struct_types[struct_type_idx]

        n = num_structs[struct_type]

        printf "enum SIG_%ss_enum\n{\n",struct_type                                                                                          > of

        struct_type_uc = toupper(struct_type)

        for(struct_idx = 0 ; struct_idx < n ; struct_idx++)
        {
            printf "    SIG_%s_%s,\n", struct_type_uc, toupper(struct_names[struct_type, struct_idx])                                        > of
        }
        printf "    SIG_NUM_%sS,\n", struct_type_uc                                                                                          > of

        print "};\n"                                                                                                                         > of
    }

    print "// Signals structures\n"                                                                                                          > of
    print "struct SIG_struct"                                                                                                                > of
    print "{"                                                                                                                                > of

    for(struct_type_idx = 0 ; struct_type_idx < num_struct_types ; struct_type_idx++)
    {
        struct_type = struct_types[struct_type_idx]

        n = num_structs[struct_type]

        printf "    union SIG_%ss_union\n    {\n",struct_type                                                                                > of
        printf "        struct SIG_%-*sarray[%d];\n\n", 30, struct_type, n                                                                   > of
        printf "        struct SIG_%ss\n        {\n", struct_type                                                                            > of

        for(struct_idx = 0 ; struct_idx < n ; struct_idx++)
        {
            printf "            struct SIG_%-*s%s;\n", 26, struct_type, struct_names[struct_type, struct_idx]                                > of
        }

        printf "        } named;\n    } %s;\n\n", struct_type                                                                                > of
    }

    print "    struct SIG_mgr                               mgr;"                                                                            > of
    print "};\n"                                                                                                                             > of
    print "// EOF"                                                                                                                           > of

    close(of)
}



function WriteSigStructsInit(of,  n, f, struct_type_uc, struct_type_idx, struct_type, struct_idx, field_idx)
{
    WriteGnuLicense("libsig", "Signals", "Structures initialization header file", of)

    print "// Default values to use if pointers are NULL\n"                                                                                  > of

    print "// Initialize signals structures per device "                                                                                     > of
    print "//"                                                                                                                               > of
    print "// @param[in]    sig                     Pointer to signals structure to initialize"                                              > of
    print "// @param[in]    polswitch_mgr           Pointer to polarity switch structure"                                                    > of
    print "// @param[in]    device_index            Device index allow initialization of multiple devices (NULL if not required)\n"          > of

    print "static void sigStructsInitDevice(struct SIG_struct    * const sig,"                                                               > of
    print "                                 struct POLSWITCH_mgr * const polswitch_mgr,"                                                     > of
    print "                                 uint32_t               const device_index)"                                                      > of
    print "{"                                                                                                                                > of
    print "    // Reset sig structure\n"                                                                                                     > of

    print "    memset(sig, 0, sizeof(struct SIG_struct));\n"                                                                                 > of

    print "    // Initialize sig_mgr structure\n"                                                                                            > of

    for(struct_type_idx = 0 ; struct_type_idx < num_struct_types ; struct_type_idx++)
    {
        struct_type = struct_types[struct_type_idx]

        printf "    sig->mgr.%-21s = &sig->%s.array[0];\n", struct_type "s", struct_type                                                     > of
    }

    printf "    sig->mgr.num_temp_filters      = %u;\n", num_structs["temp_filter"]                                                          > of
    printf "    sig->mgr.num_cal_refs          = %u;\n", num_structs["cal_ref"]                                                              > of
    printf "    sig->mgr.num_cal_limits        = %u;\n", num_structs["cal_limits"]                                                           > of
    printf "    sig->mgr.num_adcs              = %u;\n", num_structs["adc"]                                                                  > of
    printf "    sig->mgr.num_transducers       = %u;\n", num_structs["transducer"]                                                           > of
    printf "    sig->mgr.num_selects           = %u;\n", num_structs["select"]                                                               > of

    print  "    sig->mgr.polswitch_mgr         = polswitch_mgr;\n"                                                                           > of

    print  "\n    // Initialize temperature filters\n"                                                                                       > of

    print  "    uint32_t i;\n"                                                                                                               > of

    printf "    for(i = 0 ; i < %u ; i++)\n", num_structs["temp_filter"]                                                                     > of
    print  "    {"                                                                                                                           > of
    print  "        sigTempInit(&sig->mgr.temp_filters[i]);"                                                                                 > of
    print  "    }"                                                                                                                           > of

    print  "\n    // Initialize ADC calibration pointers\n"                                                                                  > of

    printf "    for(i = 0 ; i < %u ; i++)\n", num_structs["adc"]                                                                             > of
    print  "    {"                                                                                                                           > of
    print  "        sigCalInit(&sig->mgr.adcs[i].cal);"                                                                                      > of
    print  "    }"                                                                                                                           > of

    if (num_structs["transducer"] > 0)
    {
    print  "\n    // Initialize transducer calibration pointers\n"                                                                           > of

    printf "    for(i = 0 ; i < %u ; i++)\n", num_structs["transducer"]                                                                      > of
    print  "    {"                                                                                                                           > of
    print  "        sigCalInit(&sig->mgr.transducers[i].cal);"                                                                               > of
    print  "    }"                                                                                                                           > of
    }

    print  "\n    // Initialize sig structure elements\n"                                                                                    > of

    for(struct_type_idx = 0 ; struct_type_idx < num_struct_types ; struct_type_idx++)
    {
        struct_type = struct_types[struct_type_idx]

        n = num_structs[struct_type]

        for(struct_idx = 0 ; struct_idx < n ; struct_idx++)
        {
            struct_name = struct_names[struct_type, struct_idx]

            f = struct_num_fields[struct_type, struct_name]

            for(field_idx = 0 ; field_idx < f ; field_idx++)
            {
                printf "    sig->%s.named.%s.%s = %s;\n",
                    struct_type,
                    struct_name,
                    field_names [struct_type, struct_name, field_idx],
                    field_values[struct_type, struct_name, field_idx]                                                                        > of
            }
        }
    }

    print "}\n"                                                                                                                              > of

    # Generate static inline for mono-device classes

    print "// Initialize signals structure"                                                                                                  > of
    print "//"                                                                                                                               > of
    print "// Wrapper around sigStructsInitDevice() to initialize applications with one device"                                              > of
    print "//"                                                                                                                               > of
    print "// @param[in]    sig                     Pointer to signals structure to initialize"                                              > of
    print "// @param[in]    polswitch_mgr           Pointer to polarity switch structure\n"                                                  > of

    print "static inline void sigStructsInit(struct SIG_struct    * const sig,"                                                              > of
    print "                                  struct POLSWITCH_mgr * const polswitch_mgr)"                                                    > of
    print "{"                                                                                                                                > of
    print "    sigStructsInitDevice(sig, polswitch_mgr, 0);"                                                                                 > of
    print "}\n\n// EOF"                                                                                                                      > of

    close(of)
}

# EOF
