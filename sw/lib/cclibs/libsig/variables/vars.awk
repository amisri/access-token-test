#!/usr/bin/awk -f
#
# vars.awk
# Converter Control Signals library read-only variables header file generator
#
# All libsig variables that might be interesting to an application are
# identified in vars.csv. This allows this script to create a
# header file with a macro and constants that allow the application developer
# easy read-only access the variables.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of libsig.
#
# libsig is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

# Set field separater to comma to read csv file

    FS = ","

# Identify the columns in the csv file

    var_group_column   = 1
    var_name_column    = 2
    var_type_column    = 3
    var_sig_mgr_column = 4
    var_comment_column = 5

# Prepare to calculate maximum var_id length and var_type length

    max_var_id_len      = 0
    max_var_type_len    = 0
    max_var_sig_mgr_len = 0

# Read and discard heading line from stdin

    getline

# Read variable definitions from stdin

    n_vars   = 0
    line_num = 1

    while(getline > 0)
    {
        line_num++

        # Skip blank lines

        if($var_group_column == "") continue

        # Stop if non-blank lines do not have the correct number of colums

        if($var_comment_column == "")
        {
            Error("stdin", line_num, "Missing data")
        }

        # Save contents

        var_id     [n_vars] = $var_group_column "_" $var_name_column
        var_type   [n_vars] = $var_type_column
        var_sig_mgr[n_vars] = $var_sig_mgr_column
        var_comment[n_vars] = $var_comment_column

        var_id_len      = length(var_id  [n_vars])
        var_type_len    = length(var_type[n_vars])
        var_ref_mgr_len = length(var_sig_mgr[n_vars])

        if(var_id_len > max_var_id_len)
        {
            max_var_id_len = var_id_len
        }

        if(var_type_len > max_var_type_len)
        {
            max_var_type_len = var_type_len
        }

        if(var_ref_mgr_len > max_var_sig_mgr_len)
        {
            max_var_sig_mgr_len = var_ref_mgr_len
        }

        n_vars++
    }

# Generate variable header file

    of = "inc/libsig/sigVars.h"

    WriteGnuLicense("libsig", "Signals", "Converter Control Signals library variables header file", of)

    print "#pragma once\n" > of

    print "// Use sigMgrVar macros with a pointer to the sig structure.\n" > of

    print "#define sigVarValue(SIG, STRUCT_TYPE, STRUCT_NAME, VAR_NAME)         ((SIG)->STRUCT_TYPE.named.STRUCT_NAME.SIG_VAR_ ## VAR_NAME)" > of
    print "#define sigVarPointer(SIG, STRUCT_TYPE, STRUCT_NAME, VAR_NAME)      (&(SIG)->STRUCT_TYPE.named.STRUCT_NAME.SIG_VAR_ ## VAR_NAME)" > of
    print "#define sigVarValueByIdx(SIG, STRUCT_TYPE, IDX, VAR_NAME)            ((SIG)->STRUCT_TYPE.array[IDX].SIG_VAR_ ## VAR_NAME)\n" > of

    for(i=0 ; i < n_vars ; i++)
    {
        printf "#define SIG_VAR_%-*s %-*s // %-*s %s\n",
                max_var_id_len,      var_id[i],
                max_var_sig_mgr_len, var_sig_mgr[i],
                max_var_type_len,    var_type[i],
                var_comment[i] > of
    }

    print "\n// EOF" > of

    close(of)
}
# EOF
