#!/usr/bin/awk -f
#
# convcon.awk
#
# The script will convert /* */ comments in a C/C++ source or header file to // comments.
# It will preserve Doxygen comments that use the ! character.

BEGIN {

# Get output path from command line

    if(ARGC < 2)
    {
        printf "usage: convcom source_file...\n"
        exit 1
    }

    spaces = "                                                      "

    for(i = 1 ; i < ARGC ; i++)
    {
        input_file = ARGV[i]
        print "Converting " input_file
        of = input_file ".new"

        com_flag     = 0
        end_com_flag = 0
        doxy_flag    = ""

        while((getline < input_file) > 0)
        {
            // Trim trailing whitespace

            gsub(/[ \t]+$/, "", $0)

            if($1 == "/*" && NF == 1)
            {
                com_flag  = 1
                doxy_flag = ""
            }
            else if($1 == "/*!")
            {
                com_flag  = 1
                doxy_flag = "!"
            }
            else if($1 == "*/")
            {
                com_flag     = 0
                doxy_flag    = ""
                end_com_flag = 1
            }
            else if (end_com_flag == 1)
            {
                printf "\n" > of

                if($0 != "")
                {
                    print $0 > of
                }

                end_com_flag = 0
            }
            else if($1 == "*" && com_flag == 1)
            {
                p = index($0,"*")

                if(p > 2) printf substr(spaces,1, p-2) > of

                printf "//%s%s\n", doxy_flag, substr($0, p+1) > of
            }
            else
            {
                print $0 > of
            }
        }

        close(of)
        close(input_file)

        system("mv " of " " input_file)
    }
}

# EOF
