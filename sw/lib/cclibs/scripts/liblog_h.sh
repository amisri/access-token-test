#! /bin/bash

# liblog_h.sh - Generate include files for liblog

if [ $# -ne 2 ]; then
echo "Usage: $0  liblog_csv_file  output_path_for_header_files";
exit 1;
fi

scripts_path=`dirname $0`
gnu_license_h=$scripts_path/gnu_license_h.awk
read_csv=$scripts_path/../liblog/scripts/read_csv.awk
write_h=$scripts_path/../liblog/scripts/write_h.awk

awk -f $write_h -f $gnu_license_h -f $read_csv $@

exit

# EOF
