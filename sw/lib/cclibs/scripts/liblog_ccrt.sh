#! /bin/bash

# liblog_ccrt.sh - Generate liblog related files for ccrt

if [ $# -ne 2 ]; then
echo "Usage: $0  liblog_csv_file  output_path_for_ccrt_files";
exit 1;
fi

scripts_path=`dirname $0`
gnu_license_h=$scripts_path/gnu_license_h.awk
read_csv=$scripts_path/../liblog/scripts/read_csv.awk
write_ccrt=$scripts_path/../liblog/scripts/write_ccrt.awk

awk -f $write_ccrt -f $gnu_license_h -f $read_csv $@

exit

# EOF
