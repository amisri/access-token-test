#! /bin/bash
#
# libsig_h.sh - Generate include files for libsig

if [ $# -ne 2 ]; then
echo "Usage: $0  signals_csv_file  output_path_for_header_files";
exit 1;
fi

scripts_path=`dirname $0`
gnu_license_h=$scripts_path/gnu_license_h.awk
write_h=$scripts_path/../libsig/scripts/write_h.awk

awk -f $write_h -f $gnu_license_h $@

exit

# EOF
