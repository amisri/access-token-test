# cclibs/scripts/write_gnu_license_h.awk
#
# Converter Control Library Script : GNU license writer for C header files
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of cclibs.
#
# libsig is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function WriteGnuLicense(lib_short_name, lib_full_name, brief, file_name)
{
    print "//! @file  " file_name                                                                             > file_name
    print "//!"                                                                                               > file_name
    print "//! @brief Converter Control " lib_full_name " library : " brief                                   > file_name
    print "//!"                                                                                               > file_name
    print "//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>"                    > file_name
    print "//!"                                                                                               > file_name
    print "//! <h4> Contact </h4>"                                                                            > file_name
    print "//!"                                                                                               > file_name
    print "//! cclibs-devs@cern.ch"                                                                           > file_name
    print "//!"                                                                                               > file_name
    print "//! <h4> Copyright </h4>"                                                                          > file_name
    print "//!"                                                                                               > file_name
    print "//! Copyright CERN 2017. This project is released under the GNU Lesser General"                    > file_name
    print "//! Public License version 3."                                                                     > file_name
    print "//!"                                                                                               > file_name
    print "//! <h4> License </h4>"                                                                            > file_name
    print "//!"                                                                                               > file_name
    print "//! This file is part of " lib_short_name "."                                                      > file_name
    print "//!"                                                                                               > file_name
    print "//! " lib_short_name " is free software: you can redistribute it and/or modify it under the"       > file_name
    print "//! terms of the GNU Lesser General Public License as published by the Free"                       > file_name
    print "//! Software Foundation, either version 3 of the License, or (at your option)"                     > file_name
    print "//! any later version."                                                                            > file_name
    print "//!"                                                                                               > file_name
    print "//! This program is distributed in the hope that it will be useful, but WITHOUT"                   > file_name
    print "//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or"                         > file_name
    print "//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License"                   > file_name
    print "//! for more details."                                                                             > file_name
    print "//!"                                                                                               > file_name
    print "//! You should have received a copy of the GNU Lesser General Public License"                      > file_name
    print "//! along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"                       > file_name
}



function Error(filename, line_num, err_msg)
{
    if(filename == "")
    {
        printf "Error : %s\n", err_msg > "/dev/stderr"
    }
    else
    {
        printf "Error in %s line %u : %s\n", filename, line_num, err_msg > "/dev/stderr"
    }

    exit 1
}

# EOF
