#!/usr/bin/awk -f
#
# code_section.awk
#
# This script scans files for the "NON-RT" marker and constructs the code section
# pragmas for the FGC3.1 platform to move non-real-time functions into the large
# but slow external memory.
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of ccrt
#
# ccrt is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

# Get source files to scan from the command line

    if(ARGC < 3)
    {
        printf "usage: code_section.awk lib_name source_files...\n"
        exit 1
    }

    lib_name = ARGV[1]

    printf "//! @file   %s_alloc.c\n",lib_name
    printf "//! @brief  Allocates non-real-time %s functions into the ext_code section that is is large slow external RAM\n\n",lib_name

    print  "//  DO NOT EDIT - this file is GENERATED AUTOMATICALLY by cclibs/scripts/code_section.awk"

    for(i = 2 ; i < ARGC ; i++)
    {
        ReadSourceFile(lib_name, ARGV[i])
    }

    print "\n// EOF"
}



function ReadSourceFile(lib_name, input_file,  f, num_functions, functions, function_names)
{
    num_functions = 0

    while((getline < input_file) > 0)
    {
        if($2 == "NON-RT")
        {
            if($3 in function_names)
            {
                printf "code_section.awk: Error: %s is repeated in %s\n",$3,input_file > "/dev/stderr"
                exit -1
            }

            function_names[$3] = 1
            functions[num_functions++] = $3
        }
    }

    close(input_file)

    if(num_functions > 0)
    {
        printf "\n#include <lib/cclibs/%s/%s>\n\n", lib_name, input_file

        for(f = 0 ; f < num_functions ; f++)
        {
            printf "#pragma CODE_SECTION(%-40s \".ext_code\")\n", functions[f] ","
        }
    }
}

# EOF
