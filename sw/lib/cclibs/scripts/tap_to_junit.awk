#!/usr/bin/awk -f
#
# tap_to_junit.awk
#
# The script will convert a TAP format test results file into junit format
# This script require gawk rather than awk because of the call to strftime().
#
# Contact
#
# cclibs-devs@cern.ch
#
# Copyright
#
# Copyright CERN 2017. This project is released under the GNU Lesser General
# Public License version 3.
#
# License
#
# This file is part of ccrt
#
# ccrt is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {

# Get output path from command line

    if(ARGC < 3 || ARGC > 4)
    {
        printf "usage: tap_to_junit.awk  converter tap_file [report_file]\n"
        exit 1
    }

    input_file = ARGV[2]
    report_file = ARGV[3]

    # Prepare test name

    test_name = ARGV[1] ":"  input_file

    sub(/\/ccrt\.tap/, "", test_name)
    sub(/results\//, "", test_name)

    # Prepare output file name from input file name (.tap -> .xml)

    of = input_file

    sub(/\.tap/, ".xml", of)

    # Read input file

    not_ok     = 0
    num_tests  = 0

    while((getline < input_file) > 0)
    {
        n = split($0, parts, "|")

        if($1 == "not")
        {
            not_ok++
            line[++num_tests] = substr(parts[2],2)
            ok[num_tests] = 0
        }
        else if($1 == "ok")
        {
            line[++num_tests] = substr(parts[2],2)
            ok[num_tests] = 1
        }
    }

# Generate JUnit data

    timestamp = strftime("%Y-%m-%d-%H:%M:%S")

    printf "<?xml version='1.0' encoding='UTF-8' ?>\n" > of
    printf "<testsuites>\n" > of
    printf "  <testsuite id='%s' name='%s' tests='%u' failures='%u' time='%.3f'>\n", timestamp, test_name, num_tests, not_ok, num_tests * 1.0E-3 > of

    for(i = 1 ; i <= num_tests ; i++)
    {
        printf "    <testcase classname='%s' name='%s' time='0.001'>\n", test_name, line[i] > of

        if(ok[i] == 0)
        {
            print "      <failure message='not ok' type='WARNING'>" > of

            report_file_included = 0

            if(report_file != "")
            {
                report_file_included = WriteReportFile(report_file, line[i], of)
            }

            # If report file is not defined or is empty then simply embed the testcase data line

            if(report_file_included == 0)
            {
                print line[i] > of
            }

            print "      </failure>" > of
        }

        print "    </testcase>" > of
    }

    print "  </testsuite>"  > of
    print "</testsuites>"  > of

    close(of)

    exit not_ok
}



function WriteReportFile(report_file, trigger, of,  write_flag, written_flag)
{
    write_flag = 0
    written_flag = 0

    # Write only the section including and following the trigger line

    while((getline < report_file) > 0)
    {
        if($0 == trigger) write_flag = 1
        else if(NF == 0)  write_flag = 0

        if(write_flag == 1)
        {
            written_flag = 1

            gsub(/</,"\\&lt;")
            gsub(/>/,"\\&gt;")

            print $0 > of
        }
    }

    close(report_file)

    return written_flag
}

# EOF
