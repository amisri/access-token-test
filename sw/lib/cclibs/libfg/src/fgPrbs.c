//! @file  fgPrbs.c
//! @brief Generate random binary sequence (PRBS)
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

// Linear feedback shift register masks for maximal LSRFs of orders k = 3 - 30
// See https://en.wikipedia.org/wiki/Linear-feedback_shift_register#Galois_LFSRs
// We use the

static uint32_t const lfsr_masks[] =
{                  //  k. LFSR feedback polynomial
    0x00000006,    //  3. x^3  + x^2  + 1
    0x0000000C,    //  4. x^4  + x^3  + 1
    0x00000014,    //  5. x^5  + x^3  + 1
    0x00000030,    //  6. x^6  + x^5  + 1
    0x00000060,    //  7. x^7  + x^6  + 1
    0x000000B8,    //  8. x^8  + x^6  + x^5  + x^4  + 1
    0x00000110,    //  9. x^9  + x^5  + 1
    0x00000240,    // 10. x^10 + x^7  + 1
    0x00000500,    // 11. x^11 + x^9  + 1
    0x00000829,    // 12. x^12 + x^6  + x^4  + x^1  + 1
    0x0000100D,    // 13. x^13 + x^4  + x^3  + x^1  + 1
    0x00002015,    // 14. x^14 + x^5  + x^3  + x^1  + 1
    0x00006000,    // 15. x^15 + x^14 + 1
    0x0000D008,    // 16. x^16 + x^15 + x^13 + x^4  + 1
    0x00012000,    // 17. x^17 + x^14 + 1
    0x00020400,    // 18. x^18 + x^11 + 1
    0x00040023,    // 19. x^19 + x^6  + x^2  + x^1  + 1
    0x00090000,    // 20. x^20 + x^17 + 1
    0x00140000,    // 21. x^21 + x^19 + 1
    0x00300000,    // 22. x^22 + x^21 + 1
    0x00420000,    // 23. x^23 + x^18 + 1
    0x00E10000,    // 24. x^24 + x^23 + x^22 + x^17 + 1
    0x01200000,    // 25. x^25 + x^22 + 1
    0x02000023,    // 26. x^26 + x^6  + x^2  + x^1  + 1
    0x04000013,    // 27. x^27 + x^5  + x^2  + x^1  + 1
    0x09000000,    // 28. x^28 + x^25 + 1
    0x14000000,    // 29. x^29 + x^27 + 1
    0x20000029,    // 30. x^30 + x^6  + x^4  + x^1  + 1
    0x48000000,    // 31. x^31 + x^28 + 1
};


// NON-RT   fgPrbsArm

void fgPrbsArm(struct FG_limits const * const limits,
               bool                     const polswitch_auto,
               bool                     const invert_limits,
               bool                     const test_arm,
               cc_float                 const iter_period,
               cc_float                 const initial_ref,
               cc_float                 const amplitude_pp,
               uint32_t                 const period_iters,
               uint32_t                 const num_sequences,
               uint32_t                 const k,
               union  FG_pars         * const pars,
               struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_prbs  prbs_pars;              // Local PRBS pars - copied to user *pars only if there are no errors

    fgResetMetaRT(FG_PRBS, initial_ref, &prbs_pars.meta, fg_error);

    // Check if period_iters is invalid

    if(period_iters == 0 || period_iters > 10000)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_PRBS,                 // fg_type
                0,                       // error index
                (cc_float)period_iters,  // error data 0
                0.0F,                    // error data 1
                1.0E4F,                  // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Check if number of sequences is invalid

    if(num_sequences == 0 || num_sequences > 1000)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_PRBS,                 // fg_type
                1,                       // error index
                (cc_float)num_sequences, // error data 0
                0.0F,                    // error data 1
                1000.0F,                 // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Check if sequence order (k) is invalid

    if(k < 3 || k > 30)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_PRBS,                 // fg_type
                2,                       // error index
                (cc_float)k,             // error data 0
                3.0F,                    // error data 1
                30.0F,                   // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Check if total duration is too long

    cc_float period   = iter_period * (cc_float)period_iters;
    cc_float duration = period * (cc_float)((1 << k) - 1) * (cc_float)num_sequences;

    if(duration > 1.08E6F)
    {
        fgError(FG_INVALID_TIME,         // fg_errno
                FG_PRBS,                 // fg_type
                0,                       // error index
                duration,                // error data 0
                0.0F,                    // error data 1
                1.0E6F,                  // error data 2
                period,                  // error data 3
                fg_error);

        return;
    }

    // Prepare parameter structure

    prbs_pars.period_iters     = period_iters;
    prbs_pars.period_counter   = 0;
    prbs_pars.num_sequences    = num_sequences;
    prbs_pars.sequence_counter = 0;
    prbs_pars.lfsr             = 1;
    prbs_pars.lfsr_mask        = lfsr_masks[k - 3];     // lfsr_masks has masks from k = 3 - 31

    // Calculate min and max levels for the PSBR waveform

    cc_float amplitude = 0.5F * amplitude_pp;

    fgSetMinMaxRT(initial_ref + amplitude, &prbs_pars.meta);
    fgSetMinMaxRT(initial_ref - amplitude, &prbs_pars.meta);

    // Complete meta data - note that limits are not checked for PRBS functions.

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                0.0F,               // start_time
                duration,
                initial_ref,
                0.0F,               // final_rate
                limits,
                &prbs_pars.meta);

    // Check the segments against the limits if provided

    if(limits != NULL)
    {
        if(   fgCheckRef(prbs_pars.meta.range.max_ref, 0.0F, 0, &prbs_pars.meta, fg_error) != FG_OK
           || fgCheckRef(prbs_pars.meta.range.min_ref, 0.0F, 0, &prbs_pars.meta, fg_error) != FG_OK)
        {
            return;
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgPrbsRestorePars() to use in future

        prbs_pars.input.initial_ref   = initial_ref;
        prbs_pars.input.amplitude_pp  = amplitude_pp;
        prbs_pars.input.period_iters  = period_iters;
        prbs_pars.input.num_sequences = num_sequences;
        prbs_pars.input.k             = k;

        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &prbs_pars, sizeof(prbs_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = FG_PRBS;
    }
}


// NON-RT   fgPrbsRestorePars

uint32_t fgPrbsRestorePars(union FG_pars * const pars,
                           cc_float      * const initial_ref,
                           cc_float      * const amplitude_pp,
                           uint32_t      * const period_iters,
                           uint32_t      * const num_sequences,
                           uint32_t      * const k)
{
    return   fgRestorePar(&pars->prbs.input.initial_ref  , initial_ref  , sizeof(*initial_ref  ), 0)
           | fgRestorePar(&pars->prbs.input.amplitude_pp , amplitude_pp , sizeof(*amplitude_pp ), 1)
           | fgRestorePar(&pars->prbs.input.period_iters , period_iters , sizeof(*period_iters ), 2)
           | fgRestorePar(&pars->prbs.input.num_sequences, num_sequences, sizeof(*num_sequences), 3)
           | fgRestorePar(&pars->prbs.input.k            , k            , sizeof(*k            ), 4);
}



enum FG_status fgPrbsRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    struct FG_prbs * const prbs_pars = &pars->prbs;

    // Pre-function - PRBS functions always start at time 0.0

    if((cc_float)*function_time < 0.0)
    {
        *ref = pars->meta.range.initial_ref;

        return FG_PRE_FUNC;
    }

    // Generate PRBS function at the specified period in iterations using the Galois LSRF algorithm.

    if(prbs_pars->period_counter == 0)
    {
        // Post-function

        if(prbs_pars->sequence_counter >= prbs_pars->num_sequences)
        {
            *ref = pars->meta.range.final_ref;

            return FG_POST_FUNC;
        }

        // Reset the period down-counter to the period length in iterations

        prbs_pars->period_counter = prbs_pars->period_iters;

        // Take the least significant bit before shifting the shift register

        uint32_t lsb = prbs_pars->lfsr & 1;

        // Shift right the register and then flip the bits from the polynomial mask only if the lsb was 1

        prbs_pars->lfsr = (prbs_pars->lfsr >> 1) ^ (prbs_pars->lfsr_mask & (-lsb));

        // If lfsr has returned to the initial value of 1 then the sequence has finished

        prbs_pars->sequence_counter += (prbs_pars->lfsr == 1);

        // Set reference to max or min value based on the least significant bit

        *ref = (lsb == 0 ? prbs_pars->meta.range.min_ref : prbs_pars->meta.range.max_ref);
    }

    prbs_pars->period_counter--;

    return FG_DURING_FUNC;
}

// EOF
