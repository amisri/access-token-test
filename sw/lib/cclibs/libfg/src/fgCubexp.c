//! @file  fgCubexp.c
//! @brief Generate chained cubexp functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

// #define CCPRINTF


//! Calculate cubic reference from the coefficients (a3, a2, a1, a0) for the specified segment time.
//! This is the time relative to the end of the cubic segment.
//!
//! @param[in]     seg_time     Time relative to the end of the segment
//! @param[in]     coeffs       Pointer to cubic coefficients
//!
//! @returns       Cubic reference at the specified segment time

static inline cc_float fgCubexpCubicRT(cc_double                 const seg_time,
                                       struct FG_cubexp_coeffs * const coeffs)
{
    return coeffs->a0 + (coeffs->a1 + (coeffs->a2 +  coeffs->a3 * seg_time) * seg_time) * seg_time;
}


//! Calculate exponential reference from the coefficients (a3, a2, a0) for the specified segment time.
//! This is the time relative to the end of the exponential segment.
//!
//! @param[in]     seg_time     Time relative to the end of the segment
//! @param[in]     coeffs       Pointer to exponential coefficients
//!
//! @returns       Exponential reference at the specified segment time

static inline cc_float fgCubexpExponentialRT(cc_double                 const seg_time,
                                             struct FG_cubexp_coeffs * const coeffs)
{
    return coeffs->a0 + coeffs->a2 * (1.0F - exp(coeffs->a3 * seg_time));
}


//! NON-RT   fgCubexpMinMax
//!
//! Checks the inflection point of a cubic segment to report the min/max values in the meta data for the function.
//!
//! @param[in,out] meta         Pointer to the function's meta struct.
//! @param[in]     t_min_max    Time of the point of inflection where the reference is a max or min value
//! @param[in]     t0           Time of start of the segment relative to the end (always negative)
//! @param[in]     coeffs       Pointer to cubic coefficients
//! @param[in]     max_ref      Pointer to the max ref value for the segment
//! @param[in]     min_ref      Point to the min ref value for the segment

static void fgCubexpMinMax(struct FG_meta          * const meta,
                           cc_float                  const t_min_max,
                           cc_float                  const t0,
                           struct FG_cubexp_coeffs * const coeffs,
                           cc_float                * const max_ref,
                           cc_float                * const min_ref)
{
    if(t_min_max > t0 && t_min_max < 0.0F)
    {
        // Inflection point is during the segment so calculate the min or max reference and check it

        cc_float const min_max_ref = fgCubexpCubicRT(t_min_max, coeffs);

        fgSetMinMaxRT(min_max_ref, meta);

        if(min_max_ref > *max_ref)
        {
            *max_ref = min_max_ref;
        }

        if(min_max_ref < *min_ref)
        {
            *min_ref = min_max_ref;
        }
    }
}


//! NON-RT   fgCubexpArmCubic
//!
//! Calculate coefficients for the specified cubic segment and calculate and check the min/max reference and max rate.
//!
//! @param[in,out] cubexp_pars  Pointer to the cubexp parameter structure
//! @param[in]     ref          Array of references at the points
//! @param[in]     rate         Array of rates at the points
//! @param[in]     t0           Time of start point relative to the end point (always negative)
//! @param[in]     seg_idx      Segment index (0..N)

static void fgCubexpArmCubic(struct FG_cubexp * const cubexp_pars,
                             cc_float           const ref [FG_CUBEXP_MAX_POINTS],
                             cc_float           const rate[FG_CUBEXP_MAX_POINTS],
                             cc_float           const t0,
                             uint32_t           const seg_idx)
{
    uint32_t const seg_idx1 = seg_idx + 1;

    // Calculate the cubic coefficients that pass through the start/end points with the specified rates

    cc_float const dref = ref[seg_idx1] - ref[seg_idx];
    cc_float const a0   = cubexp_pars->coeffs[seg_idx].a0 =  ref [seg_idx1];
    cc_float const a1   = cubexp_pars->coeffs[seg_idx].a1 =  rate[seg_idx1];
    cc_float const a2   = cubexp_pars->coeffs[seg_idx].a2 = -(3.0F * dref + t0 * (rate[seg_idx] + 2.0F * rate[seg_idx1])) / (t0 * t0);
    cc_float const a3   = cubexp_pars->coeffs[seg_idx].a3 =  (2.0F * dref + t0 * (rate[seg_idx] +        rate[seg_idx1])) / (t0 * t0 * t0);

    // Default max/min ref and max rate is set from the end of the segment

    cc_float max_ref  = a0 > ref[seg_idx] ? a0 : ref[seg_idx];
    cc_float min_ref  = a0 < ref[seg_idx] ? a0 : ref[seg_idx];
    cc_float max_rate = fabsf(a1) > fabsf(rate[seg_idx]) ? a1 : rate[seg_idx];

    // Check end-of-segment reference for min/max

    fgSetMinMaxRT(a0, &cubexp_pars->meta);

    // Search for the points of inflection and max_rate unless segment is linear

    if(a3 != 0.0F)
    {
        // Segment is cubic

        cc_float const det = a2 * a2 - 3.0F * a1 * a3;

        if(det >= 0.0F)
        {
            // cubic has point(s) of inflection - check if they lie within the segment

            cc_float const d = 1.0F / (3.0F * a3);
            cc_float const b = sqrtf(det);

            fgCubexpMinMax(&cubexp_pars->meta,
                           d * (-a2 + b),
                           t0,
                           &cubexp_pars->coeffs[seg_idx],
                           &max_ref,
                           &min_ref);

            fgCubexpMinMax(&cubexp_pars->meta,
                           d * (-a2 - b),
                           t0,
                           &cubexp_pars->coeffs[seg_idx],
                           &max_ref,
                           &min_ref);
        }

        // Check if max rate of cubic is greater than the start or end rate

        cc_float const t_max_rate = -a2 / (3.0F * a3);

        if(t_max_rate > t0 && t_max_rate < 0.0F)
        {
            // Max rate time lies within the segment so update the max rate

            max_rate = a1 + (2.0F * a2 + 3.0F * a3 * t_max_rate) * t_max_rate;
        }
    }
    else if(a2 != 0.0F)
    {
        // Segment is quadratic

        cc_float const t_min_max = -a1 / (2.0F * a2);

        if(t_min_max > t0 && t_min_max < 0.0F)
        {
            // Point of inflection lies within the segment so update the min/max in meta data

            fgSetMinMaxRT(a0 + (a1 + a2 * t_min_max) * t_min_max, &cubexp_pars->meta);
        }
    }

    // Save min/max ref and max rate for each segment

    cubexp_pars->max_ref [seg_idx] = max_ref;
    cubexp_pars->min_ref [seg_idx] = min_ref;
    cubexp_pars->max_rate[seg_idx] = max_rate;
}


//! NON-RT   fgCubexpArmExponential
//!
//! Calculate coefficients for the specified exponential segment and calculate and check the min/max reference and max rate.
//!
//! @param[in,out] cubexp_pars  Pointer to the cubexp parameter structure
//! @param[in]     ref          Array of references at the points
//! @param[in]     rate         Array of rates at the points
//! @param[in]     t1           Time of end point relative to the start point (always positive)
//! @param[in]     seg_idx      Segment index (0..N)
//! @param[in,out] fg_error     Pointer to fg_error structure

static void fgCubexpArmExponential(struct FG_cubexp * const cubexp_pars,
                                   cc_float           const ref [FG_CUBEXP_MAX_POINTS],
                                   cc_float                 rate[FG_CUBEXP_MAX_POINTS],
                                   cc_float           const t1,
                                   uint32_t           const seg_idx,
                                   struct FG_error  * const fg_error)
{
    // Check that initial rate is not zero

    cc_float const rate0 = rate[seg_idx];

    ccprintf("seg_idx=%u  rate0=%.8E\n", seg_idx, rate0);

    if(rate0 == 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_CUBEXP,               // fg_type
                100 + seg_idx,           // error index
                0.0F,                    // error data 0
                0.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    cubexp_pars->max_rate[seg_idx] = rate0;

    // Check that final ref is feasible for an exponential

    uint32_t const seg_idx1 = seg_idx + 1;

    cc_float const ref0 = ref[seg_idx];
    cc_float const ref1 = ref[seg_idx1];

    cc_float const dref = ref1 - ref0;

    ccprintf("ref0=%.8E  ref1=%.8E   dref=%.8E\n", ref0, ref1, dref);

    if(dref == 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_CUBEXP,               // fg_type
                200 + seg_idx,           // error index
                ref0,                    // error data 0
                ref1,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Update min/max reference in meta data

    fgSetMinMaxRT(ref0, &cubexp_pars->meta);
    fgSetMinMaxRT(ref1, &cubexp_pars->meta);

    cc_float const max_dref = 0.99F * t1 * rate0;

    cc_float max_ref1 = ref0;
    cc_float min_ref1 = ref0;

    if(dref > 0.0F)
    {
        cubexp_pars->max_ref [seg_idx] = ref1;
        cubexp_pars->min_ref [seg_idx] = ref0;

        max_ref1 += max_dref;
    }
    else
    {
        cubexp_pars->max_ref [seg_idx] = ref0;
        cubexp_pars->min_ref [seg_idx] = ref1;

        min_ref1 += max_dref;
    }

    ccprintf("ref1=%.8E   min_ref1=%.8E   max_ref1=%.8E\n", ref1, min_ref1, max_ref1);

    if(ref1 < min_ref1 || ref1 > max_ref1)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_CUBEXP,               // fg_type
                300 + seg_idx,           // error index
                ref1,                    // error data 0
                min_ref1,                // error data 1
                max_ref1,                // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Calculate exponential time constant using iterative method

    cc_float const tau0 = dref / rate0;
    cc_float       tau  = tau0;

    cc_float dtau;

    do
    {
        cc_float const e       = expf(-t1 / tau);
        cc_float const f       = tau * (1 - e) - tau0;
        cc_float const df_dtau = 1.0F - e * (1 + t1 / tau);

        dtau = -f / df_dtau;

        ccprintf("tau=%.8f  dtau=%.8f   tau1=%.8f   dtau/tau=%.3E\n",
                  tau, dtau, tau+dtau, fabsf(dtau / (tau+dtau)));

        tau += dtau;

    }
    while(fabsf(dtau / tau) > 1.0E-4F);

    // Calculate the asymptote for the exponential

    cc_float const ref_inf = ref0 + rate0 * tau;

    ccprintf("ref_inf=%.8E ref1=%.8E   ref_ing==ref1=%u\n", ref_inf, ref1, (ref_inf == ref1));

    if(ref_inf == ref1)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_CUBEXP,               // fg_type
                400 + seg_idx,           // error index
                ref_inf,                 // error data 0
                ref1,                    // error data 1
                ref0,                    // error data 2
                rate0 * tau,             // error data 3
                fg_error);

        return;
    }

    // Calculate the other exponential segment coefficients from the time constant

    cubexp_pars->coeffs[seg_idx].a3 = -1.0F / tau;
    cubexp_pars->coeffs[seg_idx].a2 = ref_inf - ref1;
    cubexp_pars->coeffs[seg_idx].a1 = rate[seg_idx1] = (ref_inf - ref1) / tau;
    cubexp_pars->coeffs[seg_idx].a0 = ref1;

    ccprintf("ref_inf=%.8E a0=%.8E  a1=%.8E   a2=%.8E   a3=%.8E\n"
            , ref_inf
            , cubexp_pars->coeffs[seg_idx].a0
            , cubexp_pars->coeffs[seg_idx].a1
            , cubexp_pars->coeffs[seg_idx].a2
            , cubexp_pars->coeffs[seg_idx].a3
            );
}


// NON-RT   fgCubexpArm

void fgCubexpArm(struct FG_limits const * const limits,
                 bool                     const polswitch_auto,
                 bool                     const invert_limits,
                 bool                     const test_arm,
                 cc_float                 const ref [FG_CUBEXP_MAX_POINTS],
                 cc_float                       rate[FG_CUBEXP_MAX_POINTS],
                 cc_float                 const time[FG_CUBEXP_MAX_POINTS],
                 uint32_t                 const num_points,
                 union  FG_pars         * const pars,
                 struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_cubexp cubexp_pars;      // Local CUBEXP pars - copied to user *pars only if there are no errors

    fgResetMetaRT(FG_CUBEXP, ref[0], &cubexp_pars.meta, fg_error);

    // Check that number of CUBEXPs is valid

    if(num_points < 2 || num_points > FG_CUBEXP_MAX_POINTS)
    {
        fgError(FG_BAD_ARRAY_LEN,        // fg_errno
                FG_CUBEXP,               // fg_type
                1,                       // error index
                (cc_float)num_points,    // error data 0
                2.0F,                    // error data 1
                FG_CUBEXP_MAX_POINTS,    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    cubexp_pars.num_segs = num_points - 1;

    // Check that initial rate is zero

    if(rate[0] != 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_CUBEXP,               // fg_type
                0,                       // error index
                rate[0],                 // error data 0
                0.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Save rate array in input structure now because it is modified for exponential segments

    size_t const size_of_array = FG_CUBEXP_MAX_POINTS * sizeof(cc_float);

    memcpy(cubexp_pars.input.rate, rate, size_of_array);

    // Process each segment

    uint32_t seg_idx;

    for(seg_idx = 0 ; seg_idx < cubexp_pars.num_segs ; seg_idx++)
    {
        uint32_t const seg_idx1 = seg_idx + 1;

        cc_float       t0 = time[seg_idx];
        cc_float const t1 = time[seg_idx1];

        // Check that times are always increasing

        if(t1 <= t0)
        {
            fgError(FG_INVALID_TIME,         // fg_errno
                    FG_CUBEXP,               // fg_type
                    seg_idx,                 // error index
                    t1,                      // error data 0
                    t0,                      // error data 1
                    0.0F,                    // error data 2
                    0.0F,                    // error data 3
                    fg_error);

            return;
        }

        cubexp_pars.seg_time[seg_idx] = t1;

        // Determine the segment type : use exponential if the rate exceeds the rate limit

        cubexp_pars.exp_seg[seg_idx] = (   limits != NULL
                                        && limits->rate > 0.0F
                                        && fabsf(rate[seg_idx1]) > limits->rate);

        // Calculate the coefficients for the segment

        if(cubexp_pars.exp_seg[seg_idx] == false)
        {
            fgCubexpArmCubic(&cubexp_pars, ref, rate, t0 - t1, seg_idx);
        }
        else
        {
            fgCubexpArmExponential(&cubexp_pars, ref, rate, t1 - t0, seg_idx, fg_error);

            if(fg_error->fg_errno != FG_OK)
            {
                return;
            }
        }
    }

    cubexp_pars.seg_idx = 0;

    // Complete meta data

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                time[0],
                time[num_points-1],
                ref [num_points-1],
                rate[num_points-1],
                limits,
                &cubexp_pars.meta);

    // Check the segments against the limits if provided

    if(limits != NULL)
    {
        // Check first point which always has a rate of zero

        if(fgCheckRef(ref[0], 0.0F, 0, &cubexp_pars.meta, fg_error) != FG_OK)
        {
            return;
        }

        // Check min and max references and max rate for all the segments

        for(seg_idx = 0 ; seg_idx < cubexp_pars.num_segs ; seg_idx++)
        {
            if(   fgCheckRef(cubexp_pars.max_ref[seg_idx], cubexp_pars.max_rate[seg_idx], seg_idx+1, &cubexp_pars.meta, fg_error) != FG_OK
               || fgCheckRef(cubexp_pars.min_ref[seg_idx], 0.0F,                          seg_idx+1, &cubexp_pars.meta, fg_error) != FG_OK)
            {
                return;
            }
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgCubexpRestorePars() to use in future

        cubexp_pars.input.num_points = num_points;


        memcpy(cubexp_pars.input.ref , ref , size_of_array);
        memcpy(cubexp_pars.input.time, time, size_of_array);

        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &cubexp_pars, sizeof(cubexp_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = FG_CUBEXP;
    }
}


// NON-RT   fgCubexpRestorePars

uint32_t fgCubexpRestorePars(union FG_pars * const pars,
                             cc_float              ref [FG_CUBEXP_MAX_POINTS],
                             cc_float              rate[FG_CUBEXP_MAX_POINTS],
                             cc_float              time[FG_CUBEXP_MAX_POINTS],
                             uintptr_t     * const ref_num_els,
                             uintptr_t     * const rate_num_els,
                             uintptr_t     * const time_num_els)
{
    // Restore num_els parameters but exclude them from the returned bit mask of restored parameters

    uint32_t const num_points = pars->cubexp.input.num_points;

    fgRestorePar(&num_points, ref_num_els , sizeof(num_points), 0);
    fgRestorePar(&num_points, rate_num_els, sizeof(num_points), 0);
    fgRestorePar(&num_points, time_num_els, sizeof(num_points), 0);

    size_t const size_of_array = FG_CUBEXP_MAX_POINTS * sizeof(cc_float);

    return   fgRestorePar(pars->cubexp.input.ref , ref , size_of_array, 0)
           | fgRestorePar(pars->cubexp.input.rate, rate, size_of_array, 1)
           | fgRestorePar(pars->cubexp.input.time, time, size_of_array, 2);
}



enum FG_status fgCubexpRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    struct FG_cubexp * const cubexp_pars = &pars->cubexp;
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;

    // If during pre-function then return initial reference and PRE_FUNC

    if(func_time_fp32 < 0.0)
    {
        cubexp_pars->seg_idx = 0;
        *ref = cubexp_pars->meta.range.initial_ref;

        return FG_PRE_FUNC;
    }

    // Scan through the CUBEXP segments to find segment containing the current time

    while(func_time_fp32 > cubexp_pars->seg_time[cubexp_pars->seg_idx])
    {
        // If function complete then continue linear ramp from the last reference and return POST_FUNC

        if(++cubexp_pars->seg_idx >= cubexp_pars->num_segs)
        {
            cubexp_pars->seg_idx = cubexp_pars->num_segs - 1;

            *ref = cubexp_pars->coeffs[cubexp_pars->seg_idx].a0
                 + cubexp_pars->coeffs[cubexp_pars->seg_idx].a1 * (func_time_fp64 - cubexp_pars->seg_time[cubexp_pars->seg_idx]);

            return FG_POST_FUNC;
        }
    }

    // While time before start of segment - backtrack to the previous segment

    while(cubexp_pars->seg_idx > 0 && func_time_fp32 < cubexp_pars->seg_time[cubexp_pars->seg_idx - 1])
    {
        cubexp_pars->seg_idx--;
    }

    // During function : compute reference using cubic or exponential function

    uint32_t  const seg_idx  = cubexp_pars->seg_idx;
    cc_double const seg_time = func_time_fp64 - cubexp_pars->seg_time[seg_idx];

    *ref = cubexp_pars->exp_seg[seg_idx] == false
         ? fgCubexpCubicRT      (seg_time, &cubexp_pars->coeffs[seg_idx])
         : fgCubexpExponentialRT(seg_time, &cubexp_pars->coeffs[seg_idx]);

    return FG_DURING_FUNC;
}

// EOF
