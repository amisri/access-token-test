//! @file  fgNone.c
//! @brief Generate unchanging reference function
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"



void fgNoneArmRT(union  FG_pars  * const pars,
                 struct FG_error * const fg_error)
{
    // Reset meta & error structures

    fgResetMetaRT(FG_NONE, 0.0F, &pars->meta, fg_error);
}



enum FG_status fgNoneRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    // NONE does not modify the reference

    return FG_POST_FUNC;
}

// EOF
