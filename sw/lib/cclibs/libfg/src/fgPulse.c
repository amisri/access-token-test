//! @file  fgPulse.c
//! @brief Generate linear pulse function
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"


// NON-RT   fgPulseArm

void fgPulseArm(struct FG_limits const * const limits,
                bool                     const polswitch_auto,
                bool                     const invert_limits,
                bool                     const test_arm,
                cc_float                 const ref,
                cc_float                 const duration,
                union  FG_pars         * const pars,
                struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_pulse pulse_pars;                  // Local PULSE pars - copied to user *pars only if there are no errors

    fgResetMetaRT(FG_PULSE, ref, &pulse_pars.meta, fg_error);

    // Check that duration is valid if limits are provided

    if(limits != NULL && duration <= 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_PULSE,                // fg_type
                0,                       // error index
                duration,                // error data 0
                0.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Prepare function limits and complete the meta data

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                0.0F,              // start_time
                duration,
                ref,
                0.0F,              // final_rate
                limits,
                &pulse_pars.meta);

    // Check limits at the beginning, middle and end if supplied

    if(limits != NULL)
    {
        if(fgCheckRef(ref, 0.0F, 0, &pulse_pars.meta, fg_error) != FG_OK)
        {
            return;
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgPulseRestorePulse() to use in future

        pulse_pars.input.ref      = ref;
        pulse_pars.input.duration = duration;

        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &pulse_pars, sizeof(pulse_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = FG_PULSE;
    }
}


// NON-RT   fgPulseRestorePars

uint32_t fgPulseRestorePars(union FG_pars * const pars,
                            cc_float      * const ref,
                            cc_float      * const duration)
{
    return   fgRestorePar(&pars->pulse.input.ref     , ref     , sizeof(*ref     ), 0)
           | fgRestorePar(&pars->pulse.input.duration, duration, sizeof(*duration), 1);
}



enum FG_status fgPulseRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    *ref = pars->meta.range.initial_ref;

    // Return status according to function time

    cc_float const func_time_fp32 = (cc_float)*function_time;

    if(func_time_fp32 < 0.0)
    {
        return FG_PRE_FUNC;
    }

    if(func_time_fp32 < pars->meta.time.end)
    {
        return FG_DURING_FUNC;
    }

    return FG_POST_FUNC;
}

// EOF
