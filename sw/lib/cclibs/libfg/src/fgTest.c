//! @file  fgTest.c
//! @brief Generate test functions (STEPS, SQUARE, SINE or COSINE)
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"


void fgTestArmRT(struct FG_limits const * const limits,
                 bool                     const polswitch_auto,
                 bool                     const invert_limits,
                 bool                     const test_arm,
                 enum FG_type             const fg_type,
                 cc_float                 const initial_ref,
                 cc_float                 const amplitude_pp,
                 uint32_t                 const num_periods,
                 uint32_t                 const period_iters,
                 cc_float                 const period,
                 enum CC_enabled_disabled const window_enabled,
                 enum CC_enabled_disabled const exp_decay_enabled,
                 union  FG_pars         * const pars,
                 struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_test  test_pars;              // Local TEST pars - copied to user *pars only if there are no errors

    fgResetMetaRT(fg_type, initial_ref, &test_pars.meta, fg_error);

    // Check if number of cycles is zero

    if(num_periods == 0)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                fg_type,                 // fg_type
                0,                       // error index
                0.0F,                    // error data 0
                0.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Check if period is zero or negative

    if(period <= 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                fg_type,                 // fg_type
                1,                       // error index
                period,                  // error data 0
                0.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Check if total duration is too long

    cc_float const duration = (cc_float)num_periods * period;

    if(duration > 1.0E6F)
    {
        fgError(FG_INVALID_TIME,         // fg_errno
                fg_type,                 // fg_type
                0,                       // error index
                duration,                // error data 0
                1.0E6F,                  // error data 1
                (cc_float)num_periods,   // error data 2
                period,                  // error data 3
                fg_error);

        return;
    }

    // Prepare parameter structure

    test_pars.num_periods    = num_periods;
    test_pars.iter_idx       = 0;
    test_pars.period_iters   = period_iters;
    test_pars.half_period    = 0.5F * period;
    test_pars.frequency      = 1.0F / period;
    test_pars.offset         = initial_ref;
    test_pars.amplitude      = amplitude_pp;
    test_pars.fg_type        = fg_type;
    test_pars.window_enabled = false;
    test_pars.exp_decay      = 0.0F;

    cc_float final_ref  = initial_ref;
    cc_float window[2];                     // Max/min window scaling for sine or cosine

    // Calculate amplitude related parameters

    switch(fg_type)
    {
        case FG_STEPS:

            final_ref = test_pars.meta.range.initial_ref + test_pars.amplitude;
            test_pars.amplitude /= (cc_float)test_pars.num_periods;

            fgSetMinMaxRT(final_ref, &test_pars.meta);
            break;

        case FG_SQUARE:

            // Square wave is created from 2 x half cycles

            test_pars.num_periods *= 2;
            test_pars.frequency   *= 2.0F;

            fgSetMinMaxRT(initial_ref + test_pars.amplitude, &test_pars.meta);
            break;

        case FG_SINE:
        case FG_COSINE:

            test_pars.amplitude *= 0.5F;
            test_pars.frequency *= 6.283185307F;

            window[0] =  1.0F;
            window[1] = -1.0F;

            // Handle the window

            if(window_enabled == CC_ENABLED)
            {
                test_pars.window_enabled = true;

                // Prepare exponential decay factor if enabled

                if(exp_decay_enabled == CC_ENABLED)
                {
                    // The factor of -5.0F is chosen to reduce the oscillation to 1% by the end of the function

                    test_pars.exp_decay = -5.0F / duration;
                }

                // Adjust range scaling if number of cycles is 1
                // This does not take into account the exponential decay because
                // it is too complex to solve analytically for any number of cycles.

                if(test_pars.num_periods == 1)
                {
                    if(fg_type == FG_SINE)              // Windowed SINE
                    {
                        window[0] =  0.649519053F;           // +3 . sqrt(3) / 8
                        window[1] = -0.649519053F;           // -3 . sqrt(3) / 8
                    }
                    else                                // Windowed COSINE
                    {
                        window[0] =  1.0F / 8.0F;            // +1/8
                        window[1] = -1.0F;                   // -1
                    }
                }
            }

            fgSetMinMaxRT(initial_ref + test_pars.amplitude * window[0], &test_pars.meta);
            fgSetMinMaxRT(initial_ref + test_pars.amplitude * window[1], &test_pars.meta);
            break;

        case FG_OFFCOS:

            fgSetMinMaxRT(initial_ref                      , &test_pars.meta);
            fgSetMinMaxRT(initial_ref + test_pars.amplitude, &test_pars.meta);

            test_pars.frequency *= 6.283185307F;
            test_pars.amplitude *= 0.5F;
            test_pars.offset    += test_pars.amplitude;
            break;

        default: // Invalid function type requested

            fgError(FG_BAD_PARAMETER,    // fg_errno
                    fg_type,             // fg_type
                    2,                   // error index
                    0.0F,                // error data 0
                    0.0F,                // error data 1
                    0.0F,                // error data 2
                    0.0F,                // error data 3
                    fg_error);

            return;
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgTestRestoreInput() to use in future

        test_pars.input.initial_ref       = initial_ref;
        test_pars.input.amplitude_pp      = amplitude_pp;
        test_pars.input.num_periods       = num_periods;
        test_pars.input.period_iters      = period_iters;
        test_pars.input.period            = period;
        test_pars.input.window_enabled    = window_enabled;
        test_pars.input.exp_decay_enabled = exp_decay_enabled;

        // Complete meta data - note that limits are not checked for test functions.

        fgSetMetaRT(polswitch_auto,
                    invert_limits,
                    0.0F,                // start_time
                    duration,
                    final_ref,
                    0.0F,                // final_rate
                    limits,
                    &test_pars.meta);

        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &test_pars, sizeof(test_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = fg_type;
    }
}


// NON-RT   fgTestRestorePars

uint32_t fgTestRestorePars(union FG_pars            * const pars,
                           cc_float                 * const initial_ref,
                           cc_float                 * const amplitude_pp,
                           uint32_t                 * const num_periods,
                           uint32_t                 * const period_iters,
                           cc_float                 * const period,
                           enum CC_enabled_disabled * const window_enabled,
                           enum CC_enabled_disabled * const exp_decay_enabled)
{
    return   fgRestorePar(&pars->test.input.initial_ref      , initial_ref      , sizeof(*initial_ref      ), 0)
           | fgRestorePar(&pars->test.input.amplitude_pp     , amplitude_pp     , sizeof(*amplitude_pp     ), 1)
           | fgRestorePar(&pars->test.input.num_periods      , num_periods      , sizeof(*num_periods      ), 2)
           | fgRestorePar(&pars->test.input.period_iters     , period_iters     , sizeof(*period_iters     ), 3)
           | fgRestorePar(&pars->test.input.period           , period           , sizeof(*period           ), 4)
           | fgRestorePar(&pars->test.input.window_enabled   , window_enabled   , sizeof(*window_enabled   ), 5)
           | fgRestorePar(&pars->test.input.exp_decay_enabled, exp_decay_enabled, sizeof(*exp_decay_enabled), 6);
}



enum FG_status fgTestRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;

    // Return if pre-function - test functions always start at time 0.0

    if(func_time_fp32 < 0.0)
    {
        *ref = pars->meta.range.initial_ref;

        return FG_PRE_FUNC;
    }

    // Return if not on a sampling iteration

    struct FG_test * const test_pars = &pars->test;

    if(test_pars->period_iters > 1 && (test_pars->iter_idx++ % test_pars->period_iters) != 0)
    {
        return FG_DURING_FUNC;
    }

    // Return if post-function

    if(func_time_fp32 >= pars->meta.time.end)
    {
        *ref = pars->meta.range.final_ref;

        return FG_POST_FUNC;
    }

    // During function

    uint32_t period_idx;
    cc_float delta_ref;
    cc_double radians;
    cc_double cos_rads = 0.0;

    switch(test_pars->fg_type)
    {
        case FG_STEPS:

            // Calculate period index and clip to number of cycles in case of floating point errors

            period_idx = 1 + (uint32_t)(test_pars->frequency * func_time_fp64);

            if(period_idx > test_pars->num_periods)
            {
                period_idx = test_pars->num_periods;
            }

            delta_ref = test_pars->amplitude * (cc_float)period_idx;

            break;

        case FG_SQUARE:

            // Calculate period index and clip to number of cycles in case of floating point errors

            period_idx = 1 + (uint32_t)(test_pars->frequency * func_time_fp64);

            if(period_idx > test_pars->num_periods)
            {
                period_idx = test_pars->num_periods;
            }

            delta_ref = (period_idx & 0x1) != 0
                      ? test_pars->amplitude
                      : 0.0F;

            break;

        case FG_SINE:

            radians   = test_pars->frequency * func_time_fp64;
            delta_ref = test_pars->amplitude * sin(radians);

            break;

        case FG_COSINE:

            radians   = test_pars->frequency * func_time_fp64;
            cos_rads  = cos(radians);
            delta_ref = test_pars->amplitude * cos_rads;

            break;

        case FG_OFFCOS:

            delta_ref = -test_pars->amplitude * cos(test_pars->frequency * func_time_fp64);

            break;

        default: // Invalid function type requested

            return FG_POST_FUNC;
    }

    // If window is enabled (SINE or COSINE only)

    if(test_pars->window_enabled)
    {
        if(   func_time_fp32 < test_pars->half_period
           || (test_pars->meta.time.duration - func_time_fp32) < test_pars->half_period)
        {
            // Apply Cosine window - if type is COSINE then we already calculated cosf(radians)

           delta_ref *= 0.5 * (1.0 - (test_pars->fg_type == FG_SINE ? cosf(radians) : cos_rads));
        }

        if(test_pars->exp_decay != 0.0F)
        {
            // Apply exponential decay

            delta_ref *= expf(func_time_fp32 * test_pars->exp_decay);
        }
    }

    // Add the offset

    *ref = test_pars->offset + delta_ref;

    return FG_DURING_FUNC;
}

// EOF
