//! @file  fgTable.c
//! @brief Generate linearly interpolated table functions.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

// NON-RT   fgTableArm

void fgTableArm(struct FG_limits const * const limits,
                bool                     const polswitch_auto,
                bool                     const invert_limits,
                bool                     const test_arm,
                struct FG_point  const * const points,
                struct FG_point        * const armed_points,
                uint32_t                 const num_points,
                cc_float                       min_time_step,
                union  FG_pars         * const pars,
                struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_meta  meta;           // Local TABLE pars - copied to user *pars only if there are no errors

    // Unlike other functions, we do not need a local copy of struct fg_table.  We just need a local
    // copy of meta because the function doesn't need to write anything other than the meta data
    // to pars until the limits have been checked.

    fgResetMetaRT(FG_TABLE, points[0].ref, &meta, fg_error);

    // Verify that there are at least two points

    if(num_points < 2)
    {
        fgError(FG_BAD_ARRAY_LEN,        // fg_errno
                FG_TABLE,                // fg_type
                1,                       // error index
                (cc_float)num_points,    // error data 0
                2.0F,                    // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Adjust min time step to avoid rounding errs

    min_time_step *= 0.8F;

    // Check time vector and calculate min/max for table

    uint32_t point_idx;
    uint32_t prev_point_idx;
    uint32_t last_point_idx = num_points - 1;

    for(point_idx = 1, prev_point_idx = 0 ; point_idx < num_points ; point_idx++, prev_point_idx++)
    {
        if(points[point_idx].time <= (points[prev_point_idx].time + min_time_step))
        {
            // Time step is less than or equal to the minimum - allow a single repeated point (auto-pause)

            if(   points[point_idx].time != (points[prev_point_idx].time)
               || (   point_idx < last_point_idx
                   && points[point_idx].time == points[point_idx + 1].time))
            {
                fgError(FG_INVALID_TIME,                               // fg_errno
                        FG_TABLE,                                      // fg_type
                        point_idx,                                     // error index
                        points[point_idx].time,                        // error data 0
                        points[prev_point_idx].time,                   // error data 1
                        min_time_step,                                 // error data 2
                        0.0F,                                          // error data 3
                        fg_error);

                return;
            }
        }

        fgSetMinMaxRT(points[point_idx].ref, &meta);
    }

    // Complete meta data

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                points[0].time,
                points[last_point_idx].time,
                points[last_point_idx].ref,
                0.0F,                       // final_rate
                limits,
                &meta);

    // Check reference function limits if provided

    if(limits != NULL)
    {
        // Check the first point assuming zero rate of change

        if(fgCheckRef(points[0].ref, 0.0F, 0, &meta, fg_error) != FG_OK)
        {
            return;
        }

        // Check each point using rate of change arriving at the function

        for(point_idx = 1 ; point_idx < num_points ; point_idx++)
        {
            cc_float seg_rate = 0.0F;

            if(points[point_idx].time != (points[point_idx - 1].time))
            {
                seg_rate = (points[point_idx].ref  - points[point_idx - 1].ref) /
                           (points[point_idx].time - points[point_idx - 1].time);
            }

            if(fgCheckRef(points[point_idx].ref, seg_rate, point_idx, &meta, fg_error) != FG_OK)
            {
                fg_error->data[3] = points[point_idx].time;         // error data 3 - record time for the failed point
                return;
            }
        }
    }

    // Return if test arming

    if(test_arm == true)
    {
        return;
    }

    // Prepare table parameters

    struct FG_table * const table_pars = &pars->table;

    table_pars->meta         = meta;
    table_pars->num_points   = num_points;
    table_pars->seg_idx      = 0;
    table_pars->prev_seg_idx = 0;
    table_pars->allow_pause  = false;

    // Transfer table point data if armed_function pointer is supplied

    if(armed_points == NULL)
    {
        table_pars->points = points;
    }
    else
    {
        table_pars->points = armed_points;
        memcpy(armed_points, points, num_points * sizeof(points[0]));
    }

    // Set the function type last to validate the parameter structure

    pars->meta.type = FG_TABLE;
}


// NON-RT   fgTableRestorePars

uint32_t fgTableRestorePars(union FG_pars   * const pars,
                            uint32_t          const max_points,
                            struct FG_point * const points,
                            uintptr_t       * const num_points)
{
    // Restore num_points but exclude it from the returned bit mask of restored parameters

    fgRestorePar(&pars->table.num_points, num_points, sizeof(*num_points), 0);

    return fgRestorePar(pars->table.points, points, max_points * sizeof(*points), 0);
}



enum FG_status fgTableRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    struct FG_table * const table_pars = &pars->table;
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;
    struct FG_point const * const points = pars->table.points;

    enum FG_status status;

    // Pre-function coast

    if(func_time_fp32 < pars->meta.time.start)
    {
         *ref = pars->meta.range.initial_ref;

         status = FG_PRE_FUNC;
    }

    // Post-function coast

    else if(func_time_fp32 >= pars->meta.time.end)
    {
         *ref = pars->meta.range.final_ref;

         status = FG_POST_FUNC;
    }

    // During function

    else
    {
        // Scan forward through table to find segment containing the current time - checking for pause points when allowed

        while(func_time_fp32 >= points[table_pars->seg_idx].time)
        {
            // A segment is identified by the index of the point at the end of the segment

            uint32_t const prev_seg_idx = table_pars->seg_idx++;

            if(table_pars->allow_pause &&
               points[table_pars->seg_idx].time == points[prev_seg_idx].time)
            {
                // Pause point - return the point's time and reference

                *ref           = points[prev_seg_idx].ref;
                *function_time = points[prev_seg_idx].time;

                return FG_PAUSED;
            }
        }

        // If time before start of segment than back up through the table

        while(func_time_fp32 < points[table_pars->seg_idx - 1].time)
        {
            table_pars->seg_idx--;
        }

        // If time is in a new segment, calculate the rate

        uint32_t const seg_idx = table_pars->seg_idx;

        if(seg_idx != table_pars->prev_seg_idx)
        {
            table_pars->prev_seg_idx = seg_idx;
            table_pars->seg_rate     = (points[seg_idx].ref  - points[seg_idx - 1].ref) /
                                       (points[seg_idx].time - points[seg_idx - 1].time);
        }

        // Calculate reference using segment gradient

        *ref = points[seg_idx].ref - (points[seg_idx].time - func_time_fp64) * table_pars->seg_rate;

        // Enable pause points

        table_pars->allow_pause = true;

        return FG_DURING_FUNC;
    }

    // When pre- or post-function, reset seg_idx to the start of the table and clear the allow_pause flag

    table_pars->seg_idx      = 0;
    table_pars->prev_seg_idx = 0;
    table_pars->allow_pause  = false;

    return status;
}



void fgTableReplaceParsRT(union FG_pars * const next_pars, union FG_pars * const active_pars)
{
    // Clip the active segment index to the number of points in the next table

    uint32_t seg_idx;

    if(active_pars->table.seg_idx >= next_pars->table.num_points)
    {
        seg_idx = next_pars->table.num_points - 1;
    }
    else
    {
        seg_idx = active_pars->table.seg_idx;
    }

    // Check if seg_idx is the first point of a pause point pair (times are equal)

    struct FG_point const * const points = next_pars->table.points;

    uint32_t const next_idx = seg_idx + 1;

    if(next_idx < next_pars->table.num_points)
    {
        if(points[seg_idx].time == points[next_idx].time)
        {
            // First pause point - advance to end point of the pair

            seg_idx = next_idx;
        }
    }

    // Save the new seg_idx for the next table

    next_pars->table.seg_idx = seg_idx;
}



void fgTableResetParsRT(union FG_pars * const pars)
{
    pars->table.allow_pause  = false;
    pars->table.seg_idx      = 0;
    pars->table.prev_seg_idx = 0;
}

// EOF
