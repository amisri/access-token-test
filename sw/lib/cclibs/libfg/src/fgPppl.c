//! @file  fgPppl.c
//! @brief Generate Parabola-Parabola-Parabola-Linear (PPPL) functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

//! NON-RT   fgPpplMinMax
//!
//! Checks a parabolic segment against the min/max values in the meta data for the function.
//! If the parabola has it's point of inflexion during the segment, then the reference at the point
//! of inflexion (the min/max for the parabola) will be checked, as well as the reference at the end
//! of the segment.
//!
//! @param[in,out] meta         Pointer function meta struct.
//! @param[in]     a0           reference at the end of the segment (when time = 0)
//! @param[in]     a1           rate of change at the end of the segment (when time = 0)
//! @param[in]     a2           acceleration during segment
//! @param[in]     delta_time   Duration of segment (note that time runs from -delta_time to 0)

static void fgPpplMinMax(struct FG_meta * const meta,
                         cc_float         const a0,
                         cc_float         const a1,
                         cc_float         const a2,
                         cc_float         const delta_time)
{
    // Check end-of-segment reference for min/max

    fgSetMinMaxRT(a0, meta);

    // If acceleration isn't zero (i.e. segment isn't linear)

    if(a2 != 0.0F)
    {
        cc_float inflexion_time;        // Time of parabola's inflexion
        cc_float ref_min_max;           // Reference at inflexion time (min or max of parabola)

        // If inflexion time lies within the segment then check reference at inflexion time for min/max

        inflexion_time = -0.5F * a1 / a2;

        if(inflexion_time < 0.0F && inflexion_time > -delta_time)
        {
            ref_min_max = a0 + (a1 + a2 * inflexion_time) * inflexion_time;

            fgSetMinMaxRT(ref_min_max, meta);
        }
    }
}


// NON-RT   fgPpplArm

void fgPpplArm(struct FG_limits const * const limits,
               bool                     const polswitch_auto,
               bool                     const invert_limits,
               bool                     const test_arm,
               cc_float                 const initial_ref,
               cc_float                 const acceleration1[FG_MAX_PPPLS],
               cc_float                 const acceleration2[FG_MAX_PPPLS],
               cc_float                 const acceleration3[FG_MAX_PPPLS],
               cc_float                 const rate2        [FG_MAX_PPPLS],
               cc_float                 const rate4        [FG_MAX_PPPLS],
               cc_float                 const ref4         [FG_MAX_PPPLS],
               cc_float                 const duration4    [FG_MAX_PPPLS],
               uint32_t                 const num_pppls,
               union  FG_pars         * const pars,
               struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_pppl  pppl_pars;      // Local PPPL pars - copied to user *pars only if there are no errors

    fgResetMetaRT(FG_PPPL, initial_ref, &pppl_pars.meta, fg_error);

    // Check that number of PPPLs is valid

    if(num_pppls == 0 || num_pppls > FG_MAX_PPPLS)
    {
        fgError(FG_BAD_ARRAY_LEN,        // fg_errno
                FG_PPPL,                 // fg_type
                1,                       // error index
                (cc_float)num_pppls,     // error data 0
                FG_MAX_PPPLS,            // error data 1
                0.0F,                    // error data 2
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Prepare to process all PPPLs

    uint32_t seg_idx = 0;                           // Segment index (0-(4*FG_MAX_PPPLS-1))
    cc_float time    = 0.0F;                        // End of segment times

    cc_float * const seg_time = &pppl_pars.seg_time[0];
    cc_float * const seg_a0   = &pppl_pars.seg_a0  [0];
    cc_float * const seg_a1   = &pppl_pars.seg_a1  [0];
    cc_float * const seg_a2   = &pppl_pars.seg_a2  [0];

    cc_float delta_time  [FG_PPPL_NUM_SEGS];        // Segment durations
    cc_float r           [FG_PPPL_NUM_SEGS];        // Reference at start of each segment
    cc_float rate        [FG_PPPL_NUM_SEGS];        // Rate of change of at start of each segment
    cc_float acceleration[FG_PPPL_NUM_SEGS];        // Acceleration of each segment
    cc_float rate3_squared;                         // Rate squared at start of third parabola

    pppl_pars.seg_idx = 0;
    r[0]    = initial_ref;
    rate[0] = 0.0F;

    // For all PPPLs

    uint32_t pppl_idx;

    for(pppl_idx=0 ; pppl_idx < num_pppls ; pppl_idx++)
    {
        uint32_t  i;

        r[3]    = ref4 [pppl_idx];
        rate[3] = rate4[pppl_idx];
        rate[1] = rate2[pppl_idx];
        acceleration[0] = acceleration1[pppl_idx];
        acceleration[1] = acceleration2[pppl_idx];
        acceleration[2] = acceleration3[pppl_idx];

        // TEST 1: If accelerations or rates are invalid - error.index = 1xx

        if(   acceleration[0] == 0.0F
           || acceleration[2] == 0.0F
           || acceleration[2] == acceleration[1]
           || rate[1]         == rate[0])
        {
            fgError(FG_BAD_PARAMETER,    // fg_errno
                    FG_PPPL,             // fg_type
                    100 + pppl_idx,      // error index
                    rate[0],             // error data 0
                    rate[1],             // error data 1
                    acceleration[0],     // error data 2
                    acceleration[2],     // error data 3
                    fg_error);

            return;
        }

        delta_time[3] = duration4[pppl_idx];
        delta_time[0] = (rate[1] - rate[0]) / acceleration[0];

        r[1] = r[0] + 0.5F * delta_time[0] * (rate[0] + rate[1]);

        rate3_squared = (   2.0F * acceleration[1] * acceleration[2] * (r[3] - r[1])
                         +  rate[1] * rate[1] * acceleration[2]
                         -  rate[3] * rate[3] * acceleration[1])
                      / (acceleration[2] - acceleration[1]);

         // TEST 2: If rate3 squared is negative - error.index = 2xx

        if(rate3_squared < 0.0F)
        {
            fgError(FG_BAD_PARAMETER,    // fg_errno
                    FG_PPPL,             // fg_type
                    200 + pppl_idx,      // error index
                    rate3_squared,       // error data 0
                    r[1],                // error data 1
                    r[3],                // error data 2
                    delta_time[0],       // error data 3
                    fg_error);

            return;
        }

        rate[2] = sqrtf(rate3_squared) * (acceleration[2] > 0.0F ? -1.0F : 1.0F);

        // TEST 3: If denominator of delta_time[1] is zero - error.index = 3xx

        if((rate[1] + rate[2]) == 0.0F)
        {
            fgError(FG_BAD_PARAMETER,    // fg_errno
                    FG_PPPL,             // fg_type
                    300 + pppl_idx,      // error index
                    rate[1],             // error data 0
                    rate[2],             // error data 1
                    rate[1] + rate[2],   // error data 2
                    rate3_squared,       // error data 3
                    fg_error);

            return;
        }

        delta_time[2] = (rate[3] - rate[2]) / acceleration[2];
        delta_time[1] = (2.0F * (r[3] - r[1]) - delta_time[2] * (rate[2] + rate[3])) / (rate[1] + rate[2]);

        if(delta_time[1] >= 0.0F)
        {
            // Second parabola is included (PPPL) so calculate reference at start of third parabola

            r[2] = r[1] + 0.5F * delta_time[1] * (rate[1] + rate[2]);
        }
        else
        {
            // Second parabola is not included (PPL) so re-compute the rate at start of the "third" parabola

            rate3_squared = (  2.0F * acceleration[0] * acceleration[2] * (r[3] - r[0])
                             + rate[0] * rate[0] * acceleration[2]
                             - rate[3] * rate[3] * acceleration[0])
                          / (acceleration[2] - acceleration[0]);

            // TEST 4: If rate3 squared is negative - error.index = 4xx

            if(rate3_squared < 0.0F)
            {
                fgError(FG_BAD_PARAMETER,    // fg_errno
                        FG_PPPL,             // fg_type
                        400 + pppl_idx,      // error index
                        rate3_squared,       // error data 0
                        r[0],                // error data 1
                        r[3],                // error data 2
                        delta_time[1],       // error data 3
                        fg_error);

                return;
            }

            rate[2] = sqrtf(rate3_squared) * (acceleration[2] > 0.0F ? -1.0F : 1.0F);

            // TEST 5: If denominator of delta_time[0] is zero - error.index = 5xx

            if((rate[0] + rate[2]) == 0.0F)
            {
                fgError(FG_BAD_PARAMETER,    // fg_errno
                        FG_PPPL,             // fg_type
                        500 + pppl_idx,      // error index
                        rate[0],             // error data 0
                        rate[2],             // error data 1
                        rate[0] + rate[2],   // error data 2
                        rate3_squared,       // error data 3
                        fg_error);

                return;
            }

            delta_time[2] = (rate[3] - rate[2]) / acceleration[2];
            delta_time[0] = (2.0F * (r[3] - r[0]) - delta_time[2] * (rate[2] + rate[3])) / (rate[0] + rate[2]);
            r[2]          = r[0] + 0.5F *delta_time[0] * (rate[0] + rate[2]);
            delta_time[1] = 0.0F;
            rate[1]       = rate[2];
            r[1]          = r[2];
        }

        // TEST 6: If any segments have negative duration - error.index = 0xx

        if(delta_time[0] < 0.0F || delta_time[1] < 0.0F || delta_time[2] < 0.0F)
        {
            fgError(FG_INVALID_TIME,     // fg_errno
                    FG_PPPL,             // fg_type
                    100 + pppl_idx,      // error index
                    delta_time[0],       // error data 0
                    delta_time[1],       // error data 1
                    delta_time[2],       // error data 2
                    0.0F,                // error data 3
                    fg_error);

            return;
        }

        // Save coefficients for three parabolic segments

        for(i = 0 ; i < 3 ; i++)
        {
            time += delta_time[i];
            seg_time[seg_idx] = time;
            seg_a0  [seg_idx] = r[i+1];
            seg_a1  [seg_idx] = rate[i+1];
            seg_a2  [seg_idx] = 0.5F * acceleration[i];

            fgPpplMinMax(&pppl_pars.meta, seg_a0[seg_idx], seg_a1[seg_idx], seg_a2[seg_idx], delta_time[i]);
            seg_idx++;
        }

        // Save coefficients for the linear segment

        time += delta_time[3];
        seg_time[seg_idx] = time;
        seg_a0  [seg_idx] = r[3] + rate[3] * delta_time[3];
        seg_a1  [seg_idx] = rate[3];
        seg_a2  [seg_idx] = 0.0F;

        fgSetMinMaxRT(seg_a0[seg_idx], &pppl_pars.meta);

        // Pass final rate and reference to the start of the next PPPL

        r[0]    = seg_a0[seg_idx];
        rate[0] = rate[3];

        seg_idx++;
    }

    uint32_t const num_segs = seg_idx;

    pppl_pars.num_segs = num_segs;
    pppl_pars.seg_idx  = 0;

    // Complete meta data

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                0.0F,                   // start_time
                seg_time[num_segs-1],
                seg_a0  [num_segs-1],
                seg_a1  [num_segs-1],
                limits,
                &pppl_pars.meta);

    // Check the segments against the limits if provided

    if(limits != NULL)
    {
        for(seg_idx=0 ; seg_idx < num_segs ; seg_idx++)
        {
            if(fgCheckRef(seg_a0[seg_idx], seg_a1[seg_idx], seg_idx+1, &pppl_pars.meta, fg_error) != FG_OK)
            {
                return;
            }
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgPpplRestorePars() to use in future

        pppl_pars.input.initial_ref = initial_ref;
        pppl_pars.input.num_pppls   = num_pppls;

        size_t const size_of_array = FG_MAX_PPPLS * sizeof(cc_float);

        memcpy(pppl_pars.input.acceleration1, acceleration1, size_of_array);
        memcpy(pppl_pars.input.acceleration2, acceleration2, size_of_array);
        memcpy(pppl_pars.input.acceleration3, acceleration3, size_of_array);
        memcpy(pppl_pars.input.rate2        , rate2        , size_of_array);
        memcpy(pppl_pars.input.rate4        , rate4        , size_of_array);
        memcpy(pppl_pars.input.ref4         , ref4         , size_of_array);
        memcpy(pppl_pars.input.duration4    , duration4    , size_of_array);

        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &pppl_pars, sizeof(pppl_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = FG_PPPL;
    }
}


// NON-RT   fgPpplRestorePars

uint32_t fgPpplRestorePars(union FG_pars * const pars,
                           cc_float      * const initial_ref,
                           cc_float              acceleration1[FG_MAX_PPPLS],
                           cc_float              acceleration2[FG_MAX_PPPLS],
                           cc_float              acceleration3[FG_MAX_PPPLS],
                           cc_float              rate2        [FG_MAX_PPPLS],
                           cc_float              rate4        [FG_MAX_PPPLS],
                           cc_float              ref4         [FG_MAX_PPPLS],
                           cc_float              duration4    [FG_MAX_PPPLS],
                           uintptr_t     * const acceleration1_num_els,
                           uintptr_t     * const acceleration2_num_els,
                           uintptr_t     * const acceleration3_num_els,
                           uintptr_t     * const rate2_num_els,
                           uintptr_t     * const rate4_num_els,
                           uintptr_t     * const ref4_num_els,
                           uintptr_t     * const duration4_num_els)
{
    // Restore num_els parameters but exclude them from the returned bit mask of restored parameters

    uint32_t num_pppls = pars->pppl.input.num_pppls;

    fgRestorePar(&num_pppls, acceleration1_num_els, sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, acceleration2_num_els, sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, acceleration3_num_els, sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, rate2_num_els        , sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, rate4_num_els        , sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, ref4_num_els         , sizeof(num_pppls), 0);
    fgRestorePar(&num_pppls, duration4_num_els    , sizeof(num_pppls), 0);

    size_t const size_of_array = FG_MAX_PPPLS * sizeof(cc_float);

    return   fgRestorePar(&pars->pppl.input.initial_ref , initial_ref  , sizeof(*initial_ref), 0)
           | fgRestorePar(pars->pppl.input.acceleration1, acceleration1, size_of_array       , 1)
           | fgRestorePar(pars->pppl.input.acceleration2, acceleration2, size_of_array       , 2)
           | fgRestorePar(pars->pppl.input.acceleration3, acceleration3, size_of_array       , 3)
           | fgRestorePar(pars->pppl.input.rate2        , rate2        , size_of_array       , 4)
           | fgRestorePar(pars->pppl.input.rate4        , rate4        , size_of_array       , 5)
           | fgRestorePar(pars->pppl.input.ref4         , ref4         , size_of_array       , 6)
           | fgRestorePar(pars->pppl.input.duration4    , duration4    , size_of_array       , 7);
}



enum FG_status fgPpplRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    struct FG_pppl * const pppl_pars = &pars->pppl;
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;

    // If pre-function

    if(func_time_fp32 < 0.0)
    {
        pppl_pars->seg_idx = 0;
        *ref = pppl_pars->meta.range.initial_ref;

        return FG_PRE_FUNC;
    }

    // Scan through the PPPL segments to find segment containing the current time

    cc_double seg_time;

    while(func_time_fp32 > pppl_pars->seg_time[pppl_pars->seg_idx])
    {
        // If function complete the coast from last reference and return POST_FUNC

        if(++pppl_pars->seg_idx >= pppl_pars->num_segs)
        {
            pppl_pars->seg_idx = pppl_pars->num_segs - 1;
            seg_time           = func_time_fp64 - pppl_pars->seg_time[pppl_pars->seg_idx];
            *ref               = pppl_pars->seg_a0[pppl_pars->seg_idx] + pppl_pars->seg_a1[pppl_pars->seg_idx] * seg_time;

            return FG_POST_FUNC;
        }
    }

    // While time before start of segment - backtrack to the previous segment

    while(pppl_pars->seg_idx > 0 && func_time_fp32 < pppl_pars->seg_time[pppl_pars->seg_idx - 1])
    {
        pppl_pars->seg_idx--;
    }

    // seg_time is time within the segment

    uint32_t const seg_idx = pppl_pars->seg_idx;

    seg_time = func_time_fp64 - pppl_pars->seg_time[seg_idx];

    *ref = pppl_pars->seg_a0[seg_idx] +
          (pppl_pars->seg_a1[seg_idx] + pppl_pars->seg_a2[seg_idx] * seg_time) * seg_time;

    return FG_DURING_FUNC;
}

// EOF
