//! @file  fgPlep.c
//! @brief Generate Parabola-Linear-Exponential-Parabola (PLEP) function
//!
//! The PLEP function is special for two reasons:
//!
//!  1. It can end with a non-zero final rate of change. When used, it becomes a PLEPP.
//!
//!  2. It calculates the RMS of the function (excluding the optional final accelerating
//!     parabola).
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

//! NON-RT   fgPlepIntegrateParSquared
//!
//! Returns the integral of the square of the parabola p(t) from t = 0 to t = et where
//!
//!        p = r + 1/2 . a . t^2
//!
//! Integrate (r + 0.5 * a * t * t)^2 using https://www.integral-calculator.com/
//!
//! It's possible to calculate the integral in the range from t1 to t2 by calling
//! this function twice, for t1 and t2, and taking the difference.
//!
//! @param[in]    r       Reference at time zero
//! @param[in]    a       Acceleration
//! @param[in]    et      End time for the parabola
//!
//! @returns      integrated reference squared for a parabolic segment

static cc_float fgPlepIntegrateParSquared(cc_float const r,
                                          cc_float const a,
                                          cc_float const et)
{
    cc_float const et2 = et * et;

    return (1.0F / 60.0F) * et * ((3.0F * a * a * et2 * et2) + (20.0F * a * r * et2) + (60.0F * r * r));
}


//! NON-RT   fgPlepArm

void fgPlepArm(struct FG_limits const * const limits,
               bool                     const polswitch_auto,
               bool                     const invert_limits,
               bool                     const test_arm,
               cc_float                       initial_rate,
               cc_float                 const final_rate,
               cc_float                       initial_ref,
               cc_float                       final_ref,
               cc_float                 const acceleration,
               cc_float                 const deceleration,
               cc_float                 const linear_rate,
               cc_float                 const exp_tc,
               cc_float                 const exp_final,
               union  FG_pars         * const pars,
               struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_plep  plep_pars;

    fgResetMetaRT(FG_PLEP, initial_ref, &plep_pars.meta, fg_error);

    // Check that some of the parameters are valid

    if(acceleration <= 0.0F || deceleration <= 0.0F || linear_rate < 0.0F || exp_tc < 0.0F)
    {
        fgError(FG_BAD_PARAMETER,  // fg_errno
                FG_PLEP,           // fg_type
                0,                 // error index
                acceleration,      // error data 0
                deceleration,      // error data 1
                linear_rate,       // error data 2
                exp_tc,            // error data 3
                fg_error);

        return;
    }

    // Save input parameters for fgPlepRestorePars() to use in future

    if(test_arm == false)
    {
        plep_pars.input.initial_ref  = initial_ref;
        plep_pars.input.final_ref    = final_ref;
        plep_pars.input.acceleration = acceleration;
        plep_pars.input.linear_rate  = linear_rate;
        plep_pars.input.exp_tc       = exp_tc;
        plep_pars.input.exp_final    = exp_final;
    }

    // Calculate final accelerating parabola if specified

    cc_float delta_time[FG_PLEP_NUM_SEGS+1] = { 0.0F };

    plep_pars.seg_ref[5] = final_ref;

    if(final_rate != 0.0F)
    {
        // Rate rate is not zero so adjust final ref to be the min or max value for the parabola

        plep_pars.final_acc = acceleration * (final_rate >= 0.0F ? 1.0F : -1.0F);
        delta_time[5]       = final_rate / plep_pars.final_acc;
        final_ref          -= 0.5F * final_rate * delta_time[5];
    }
    else
    {
        // No final parabolic acceleration

        plep_pars.final_acc = 0.0F;
    }

    plep_pars.seg_ref[4] = final_ref;

    // Determine if the PLEP is ascending or descending

    cc_float       delta_ref      = final_ref - initial_ref;
    bool     const null_pelp      = (delta_ref == 0.0F && initial_rate == 0.0F);
    bool     const exp_tc_flag    = (null_pelp == false && exp_tc > 0.0F);
    bool     const lin_flag       = (null_pelp == false && linear_rate > 0.0F);
    bool     const ascending_flag = (delta_ref >= 0.0F);
    cc_float const direction      = ascending_flag == true ? 1.0F : -1.0F;

    plep_pars.acceleration = acceleration * direction;
    plep_pars.deceleration = deceleration * direction * -1.0F;
    plep_pars.linear_rate  = linear_rate  * direction;
    plep_pars.final_rate   = final_rate;

    // Set exp_final based on exp_tc and exp_final - use the exp_final_auto if exp_final is zero

    plep_pars.exp_final = exp_tc_flag == false
                        ? 0.0F
                        : exp_final;

    // Prepare the max initial rate for a P-P

    cc_float const max_initial_rate_pp = sqrtf(-2.0F * plep_pars.deceleration * delta_ref);

    cc_float const inv_acc = 1.0F / plep_pars.acceleration;
    cc_float       inv_dec = 1.0F / plep_pars.deceleration;

    if(exp_tc_flag == true)
    {
        // Check validity of exp_final compared to final_ref

        if(   (ascending_flag == true  && plep_pars.exp_final <= final_ref)
           || (ascending_flag == false && plep_pars.exp_final >= final_ref))
        {
            fgError(FG_BAD_PARAMETER,           // fg_errno
                    FG_PLEP,                    // fg_type
                    1,                          // error index
                    (cc_float)ascending_flag,   // error data 0
                    plep_pars.exp_final,        // error data 1
                    final_ref,                  // error data 2
                    delta_ref,                  // error data 3
                    fg_error);

            return;
        }

        // Cache the inverted exponential time constant

        plep_pars.inv_exp_tc = -1.0F / exp_tc;
    }

    // Adjust inital_ref based on initial_rate

    if(initial_rate != 0.0F)
    {
        // Clip initial rate to the linear rate limit, if specified

        if(   lin_flag == true
           && (   (ascending_flag == true  && initial_rate > plep_pars.linear_rate)
               || (ascending_flag == false && initial_rate < plep_pars.linear_rate)))
        {
            initial_rate = plep_pars.linear_rate;
        }

        // Clip initial rate to the exponential decay rate at the given initial reference, if exponential time constant is specified

        if(exp_tc_flag == true)
        {
            cc_float const max_exp_rate = (initial_ref - plep_pars.exp_final) * plep_pars.inv_exp_tc;

            if(   (ascending_flag == true  && initial_rate > max_exp_rate)
               || (ascending_flag == false && initial_rate < max_exp_rate))
            {
                initial_rate = max_exp_rate;
            }
        }

        // Initial rate is not zero so adjust initial ref to be the min or max value for the parabola

        delta_time[0] = -initial_rate * inv_acc;
        initial_ref  +=  0.5F * initial_rate * delta_time[0];
        delta_ref      = final_ref - initial_ref;
    }

    plep_pars.seg_ref[0] = initial_ref;

    // Define variables used below before the gotos

    uint32_t i;
    cc_float ref_time = 0.0F;

    // Prepare for linear section if required

    cc_float delta_time1_pl;

    if(lin_flag == true)
    {
        delta_time1_pl = plep_pars.linear_rate * inv_acc;
    }
    else
    {
        delta_time1_pl = 1.0E30F;
    }

    // Prepare for exponential section if required

    cc_float delta_time1_pe;

    if(exp_tc_flag == true)
    {
        // Set time constant parameter to generate exponential

        delta_time1_pe = sqrtf(exp_tc * exp_tc + 2.0F * inv_acc * (plep_pars.exp_final - initial_ref)) - exp_tc;
    }
    else
    {
        plep_pars.inv_exp_tc = 0.0F;
        plep_pars.ref_exp    = 0.0F;
        delta_time1_pe       = 1.0E30F;
    }

    // Identify which type of PLEP it is: 1.Inverted P-P, 2.P-P, 3.P-E-P, 4.P-L-P, 5.P-L-E-P

    cc_float const delta_time1_pp = sqrtf(2.0F * delta_ref / (plep_pars.acceleration * (1.0F - plep_pars.acceleration * inv_dec)));

    // Delta times between the start of the first parabola and either the start of the second
    // parabola (P-P), the exponential section (P-E-P) or the start of the linear segment (P-L-P, P-L-E-P cases)

    bool const par_b4_lin = (delta_time1_pp < delta_time1_pl);
    bool const par_b4_exp = (delta_time1_pp < delta_time1_pe);
    bool const exp_b4_lin = (delta_time1_pe < delta_time1_pl);
    bool const inv_pp     = (ascending_flag == true  && initial_rate >  max_initial_rate_pp)
                         || (ascending_flag == false && initial_rate < -max_initial_rate_pp);

    bool include_lin = false;
    bool include_exp = false;

    // Case 1 : Inverted P-P

    if(inv_pp == true)
    {
        // Both accelerating and deceleration parabolas will use the deceleration

        plep_pars.acceleration =  plep_pars.deceleration;
        plep_pars.deceleration = -plep_pars.deceleration;
        inv_dec                = -inv_dec;

        delta_time[0] = initial_rate * inv_dec;

        plep_pars.seg_ref[0] = plep_pars.input.initial_ref + 0.5F * initial_rate * initial_rate * inv_dec;

        initial_ref = plep_pars.seg_ref[0];

        cc_float const dt1_squared = inv_dec * (plep_pars.seg_ref[0] - plep_pars.seg_ref[4]);

        if(dt1_squared > 0.0F)   // Protect sqrtf() against negative values due to float rounding errors
        {
            delta_time[1] = sqrtf(dt1_squared);
        }

        delta_time[4] = delta_time[1];

        plep_pars.seg_ref[1] = plep_pars.seg_ref[2] = plep_pars.seg_ref[3] = 0.5F * (plep_pars.seg_ref[4] - plep_pars.seg_ref[0]);

        goto end;
    }

    // Case 2 : P-P

    if(par_b4_lin == true && par_b4_exp == true)
    {
        delta_time[1] = delta_time1_pp;
        delta_time[4] = -plep_pars.acceleration * inv_dec * delta_time1_pp;

        plep_pars.seg_ref[1] = plep_pars.seg_ref[2] = plep_pars.seg_ref[3] = initial_ref + 0.5F * plep_pars.acceleration * delta_time1_pp * delta_time1_pp;

        goto end;
    }

    // Case 3 : P-E-P

    if(exp_b4_lin == true)
    {
        delta_time[1] = delta_time1_pe;

        plep_pars.seg_ref[1] = plep_pars.seg_ref[2] = initial_ref + 0.5F * plep_pars.acceleration * delta_time1_pe * delta_time1_pe;

        exp_par:        // From Case 4 below : P-L-E-P

        include_exp          = true;
        plep_pars.ref_exp    = plep_pars.seg_ref[2] - plep_pars.exp_final;
        delta_time[4]        = exp_tc - sqrtf(exp_tc * exp_tc + 2.0F * inv_dec * (plep_pars.exp_final - final_ref));
        plep_pars.seg_ref[3] = final_ref + 0.5F * plep_pars.deceleration * delta_time[4] * delta_time[4];

        // Use log() instead of logf() for now, because gcc7.1 and gcc7.3 have different results for logf(), but the same for log()

        delta_time[3] = -exp_tc * log((plep_pars.seg_ref[3] - plep_pars.exp_final) / plep_pars.ref_exp);

        goto end;
    }

    // Case 4, 5 : P-L-P or P-L-E-P

    include_lin   = true;
    delta_time[1] = delta_time1_pl;

    plep_pars.seg_ref[1] = initial_ref + 0.5F * plep_pars.acceleration * delta_time1_pl * delta_time1_pl;
    plep_pars.seg_ref[3] = final_ref + 0.5F * inv_dec * plep_pars.linear_rate * plep_pars.linear_rate;

    if(exp_tc_flag == true)
    {
        plep_pars.seg_ref[2] = plep_pars.exp_final - plep_pars.linear_rate * exp_tc;

        // plep_pars.seg_ref[2] is reference where linear and exponential would join
        // plep_pars.seg_ref[3] is reference where linear and parabolic would join

        if((ascending_flag == true  && plep_pars.seg_ref[2] < plep_pars.seg_ref[3]) ||
           (ascending_flag == false && plep_pars.seg_ref[2] > plep_pars.seg_ref[3]))
        {
            // Case 5 : P-L-E-P

            delta_time[2] = (plep_pars.seg_ref[2] - plep_pars.seg_ref[1]) / plep_pars.linear_rate;

            goto exp_par;       // Jump to part of Case 2: P-E-P above, to compute the final E-P
        }
    }

    // Case 4 : P-L-P

    plep_pars.seg_ref[2] = plep_pars.seg_ref[3];
    delta_time[2]        = (plep_pars.seg_ref[2] - plep_pars.seg_ref[1]) / plep_pars.linear_rate;
    delta_time[4]        = -plep_pars.linear_rate * inv_dec;

    end:        // From goto end;

    // Calculate segment times and min/max range
    //
    // Segments are numbered:
    //
    //  [0] - Min/max of initial parabola.
    //  [1] - Initial parabola.
    //  [2] - Linear ramp.
    //  [3] - Exponential deceleration.
    //  [4] - Parabolic deceleration.
    //  [5] - Final parabolic acceleration.
    //
    // The following arrays are referenced to these segments:
    //
    //   delta_time - duration of the segment
    //   seg_time   - time at the end of the segment
    //   seg_ref    - reference at the end of the segment
    //   seg_rate   - rate of change of reference at the end of the segment

    for(i = 0 ; i <= FG_PLEP_NUM_SEGS ; i++)
    {
        ref_time             += delta_time[i];
        plep_pars.seg_time[i] = ref_time;
    }

    // Peak initial value will be seg_ref[0] if delta_time[0] is negative, otherwise it will be the user's initial_ref

    fgSetMinMaxRT(delta_time[0] >= 0.0F ? plep_pars.seg_ref[0] : plep_pars.input.initial_ref, &plep_pars.meta);
    fgSetMinMaxRT(plep_pars.seg_ref[4], &plep_pars.meta);
    fgSetMinMaxRT(plep_pars.seg_ref[5], &plep_pars.meta);

    // Complete meta data - fgSetMinMax() must set min/max range before calling fgSetMeta

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                0.0F,                    // start_time
                plep_pars.seg_time[5],
                plep_pars.seg_ref[5],
                final_rate,
                limits,
                &plep_pars.meta);

    // Check limits for all the segments if limits are supplied

    if(limits != NULL)
    {
        /*
         * Only calculate end of segment rates of change that need to be checked.
         * The rates for segments 2, 3, and 4 cannot be greater than for segment 1
         * (parabolic acceleration) so they are not calculated.
         */

        cc_float seg_rate[FG_PLEP_NUM_SEGS+1] = { 0.0F };  // Rate at the end of the segments (used for limits checking only)

        seg_rate[1] = plep_pars.acceleration * delta_time[1];
        seg_rate[5] = final_rate;

        for(i = 0 ; i <= FG_PLEP_NUM_SEGS ; i++)
        {
            if(fgCheckRef(plep_pars.seg_ref[i], seg_rate[i], i, &plep_pars.meta, fg_error) != FG_OK)
            {
                return;
            }
        }
    }

    // Success - store exp_final in fg_error data

    fg_error->data[0] = exp_final;

    // Return if test arming

    if(test_arm == true)
    {
        return;
    }

    if(plep_pars.seg_time[4] > 0.0F)
    {
        // Calculate integrated reference squared for the parabolas (excluding optional final accelerating parabola)
        // If the initial_rate is not zero, then we need to integrate just the part of the parabola that will be played.

        cc_float const integrated_pars_squared = fgPlepIntegrateParSquared(initial_ref, plep_pars.acceleration,  delta_time[1])
                                               - fgPlepIntegrateParSquared(initial_ref, plep_pars.acceleration, -delta_time[0])
                                               + fgPlepIntegrateParSquared(final_ref,   plep_pars.deceleration,  delta_time[4]);

        // Calculate the integrated reference squared for the optional linear and exponential sections

        cc_float integrated_lin_squared = 0.0F;
        cc_float integrated_exp_squared = 0.0F;

        if(inv_pp == false)
        {
            // Calculate the integrated reference squared for the linear section

            // Integrate (r_1 + l*t)^2 using https://www.integral-calculator.com/

            if(include_lin == true)
            {
                cc_float const t  = delta_time[2];
                cc_float const r1 = plep_pars.seg_ref[1];
                cc_float const l  = plep_pars.linear_rate;

                integrated_lin_squared = t * (r1 * r1  +  r1 * l * t  +  l * l * t * t / 3.0F);
            }

            // Calculate the integrated reference squared for the exponential

            // Integrate ((r_2 - ef) * exp( -t / t_c ) + ef)^2 using https://www.integral-calculator.com/

            if(include_exp == true)
            {
                cc_float const t        = delta_time[3];
                cc_float const r2       = plep_pars.seg_ref[2];
                cc_float const ef       = plep_pars.exp_final;
                cc_float const ef_r2    = ef - r2;
                cc_float const exp_t_tc = expf(t * plep_pars.inv_exp_tc);

                integrated_exp_squared = 2.0F * ef_r2 * exp_tc * exp_t_tc * ef  +  t * ef * ef
                                       - 0.5F * ef_r2 * exp_tc * (exp_t_tc * exp_t_tc * ef_r2 + (3.0F * ef + r2));
            }
        }

        // Calculate the RMS reference for the full P-L-E-P from the integrated reference squared

        plep_pars.rms = sqrtf((integrated_pars_squared + integrated_lin_squared + integrated_exp_squared) / plep_pars.seg_time[4]);
    }
    else
    {
        // Null PLEP - set RMS to zero

        plep_pars.rms = 0.0F;
    }

    // Copy valid set of parameters to user's pars structure

    memcpy(pars, &plep_pars, sizeof(plep_pars));

    // Set the function type last to validate the parameter structure

    pars->meta.type = FG_PLEP;
}


//! NON-RT   fgPlepRestorePars

uint32_t fgPlepRestorePars(union FG_pars * const pars,
                           cc_float      * const initial_ref,
                           cc_float      * const final_ref,
                           cc_float      * const acceleration,
                           cc_float      * const linear_rate,
                           cc_float      * const exp_tc,
                           cc_float      * const exp_final)
{
    return   fgRestorePar(&pars->plep.input.initial_ref , initial_ref , sizeof(*initial_ref ), 0)
           | fgRestorePar(&pars->plep.input.final_ref   , final_ref   , sizeof(*final_ref   ), 1)
           | fgRestorePar(&pars->plep.input.acceleration, acceleration, sizeof(*acceleration), 2)
           | fgRestorePar(&pars->plep.input.linear_rate , linear_rate , sizeof(*linear_rate ), 3)
           | fgRestorePar(&pars->plep.input.exp_tc      , exp_tc      , sizeof(*exp_tc      ), 4)
           | fgRestorePar(&pars->plep.input.exp_final   , exp_final   , sizeof(*exp_final   ), 5);
}



enum FG_status fgPlepRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;
    cc_double seg_time;

    // Pre-function

    if(func_time_fp32 < 0.0)
    {
        *ref = pars->plep.input.initial_ref;

        return FG_PRE_FUNC;
    }

    // Segment 1: Parabolic acceleration

    else if(func_time_fp32 <= pars->plep.seg_time[1])
    {
        // seg_time is relative to the min or max of the first parabola

        seg_time = func_time_fp64 - pars->plep.seg_time[0];

        *ref = pars->plep.seg_ref[0] + 0.5 * pars->plep.acceleration * seg_time * seg_time;

        return FG_DURING_FUNC;
    }

    // Segment 2: Linear ramp

    else if(func_time_fp32 <= pars->plep.seg_time[2])
    {
        // seg_time is relative to start of linear segment

        seg_time = func_time_fp64 - pars->plep.seg_time[1];

        *ref = pars->plep.seg_ref[1] + pars->plep.linear_rate * seg_time;

        return FG_DURING_FUNC;
    }

    // Segment 3: Exponential deceleration

    else if(func_time_fp32 <= pars->plep.seg_time[3])
    {
        // seg_time is relative to start of exponential segment

        seg_time = func_time_fp64 - pars->plep.seg_time[2];

        *ref = pars->plep.ref_exp * exp(pars->plep.inv_exp_tc * seg_time) + pars->plep.exp_final;

        return FG_DURING_FUNC;
    }

    // Segment 4: Parabolic deceleration

    else if(func_time_fp32 < pars->plep.seg_time[4])
    {
        // seg_time is relative to end of the decelerating parabola (seg_time is negative)

        seg_time = func_time_fp64 - pars->plep.seg_time[4];

        *ref = pars->plep.seg_ref[4] + 0.5 * pars->plep.deceleration * seg_time * seg_time;

        return FG_DURING_FUNC;
    }

    // Segment 5: Parabolic acceleration

    else if(func_time_fp32 < pars->plep.seg_time[5])
    {
        // seg_time is relative to start of the final accelerating parabola

        seg_time = func_time_fp64 - pars->plep.seg_time[4];

        *ref = pars->plep.seg_ref[4] + 0.5 * pars->plep.final_acc * seg_time * seg_time;

        return FG_DURING_FUNC;
    }

    // Post-function: Continue linear ramp using final_rate

    seg_time = func_time_fp64 - pars->plep.seg_time[5];

    *ref = pars->plep.seg_ref[5] + pars->plep.final_rate * seg_time;

    return FG_POST_FUNC;
}

// EOF
