//! @file  fgTrim.c
//! @brief Generate linear and cubic trim functions
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"


// NON-RT   fgTrimArm

void fgTrimArm(struct FG_limits const * const limits,
               bool                     const polswitch_auto,
               bool                     const invert_limits,
               bool                     const test_arm,
               enum FG_type             const fg_type,
               cc_float                 const initial_ref,
               cc_float                 const final_ref,
               cc_float                 const linear_rate,
               cc_float                       duration,
               union  FG_pars         * const pars,
               struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_trim  trim_pars;        // Local TRIM pars - copied to user *pars only if there are no errors

    struct FG_meta * const trim_meta = &trim_pars.meta;

    fgResetMetaRT(fg_type, initial_ref, &trim_pars.meta, fg_error);

    // Save input parameters for fgTestRestoreInput() to use in future

    if(test_arm == false)
    {
        trim_pars.input.initial_ref = initial_ref;
        trim_pars.input.final_ref   = final_ref;
        trim_pars.input.linear_rate = linear_rate;
        trim_pars.input.duration    = duration;
    }

    // Assess if ramp is rising or falling

    cc_float const delta_ref = final_ref - initial_ref;

    if(delta_ref < 0.0F)
    {
        trim_meta->range.min_ref = final_ref;
        trim_meta->range.max_ref = initial_ref;
    }
    else
    {
        trim_meta->range.min_ref = initial_ref;
        trim_meta->range.max_ref = final_ref;
    }

    // Prepare cubic factors according to trim type

    switch(fg_type)
    {
        case FG_CTRIM:

            trim_pars.a = 1.0F;
            trim_pars.c = 1.5F;
            break;

        case FG_LTRIM:

            trim_pars.a = 0.0F;
            trim_pars.c = 1.0F;
            break;

        default:

            fgError(FG_BAD_PARAMETER,    // fg_errno
                    fg_type,             // fg_type
                    1,                   // error index
                    0.0F,                // error data 0
                    0.0F,                // error data 1
                    0.0F,                // error data 2
                    0.0F,                // error data 3
                    fg_error);

            return;
    }

    // Calculate or check duration and complete cubic factors

    if(duration < 1.0E-6F)
    {
        // If duration is zero then limits must be supplied

        if(linear_rate <= 0.0F)
        {
            fgError(FG_BAD_PARAMETER,    // fg_errno
                    fg_type,             // fg_type
                    0,                   // error index
                    duration,            // error data 0
                    linear_rate,         // error data 1
                    0.0F,                // error data 2
                    0.0F,                // error data 3
                    fg_error);

            return;
        }

        // Calculate duration based on linear_rate parameter

        duration = fabsf(trim_pars.c * delta_ref / linear_rate);
    }

    trim_pars.a *= -2.0F * delta_ref / (duration * duration * duration);
    trim_pars.c *= delta_ref / duration;

    // Calculate offsets

    trim_pars.time_offset = 0.5F * duration;
    trim_pars.ref_offset  = 0.5F * (initial_ref + final_ref);

    // Complete meta data

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                0.0F,                   // start_time
                duration,
                final_ref,
                0.0F,                   // final_rate
                limits,
                trim_meta);

    // Check limits at the beginning, middle and end if supplied

    if(limits != NULL)
    {
        if(fgCheckRef(initial_ref,          0.0F,        0, trim_meta, fg_error) != FG_OK ||
           fgCheckRef(trim_pars.ref_offset, trim_pars.c, 1, trim_meta, fg_error) != FG_OK ||
           fgCheckRef(final_ref,            0.0F,        2, trim_meta, fg_error) != FG_OK)
        {
            return;
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Copy valid set of parameters to user's pars structure

        memcpy(pars, &trim_pars, sizeof(trim_pars));

        // Set the function type last to validate the parameter structure

        pars->meta.type = fg_type;
    }
}


// NON-RT   fgTrimRestorePars

uint32_t fgTrimRestorePars(union FG_pars * const pars,
                           cc_float      * const initial_ref,
                           cc_float      * const final_ref,
                           cc_float      * const duration)
{
    return   fgRestorePar(&pars->trim.input.initial_ref, initial_ref, sizeof(*initial_ref), 0)
           | fgRestorePar(&pars->trim.input.final_ref  , final_ref  , sizeof(*final_ref  ), 1)
           | fgRestorePar(&pars->trim.input.duration   , duration   , sizeof(*duration   ), 2);
}



enum FG_status fgTrimRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    cc_double const func_time_fp64 = *function_time;
    cc_float const func_time_fp32 = (cc_float)func_time_fp64;

    // Pre-function - trim functions always start at time 0.0

    if(func_time_fp32 < 0.0)
    {
        *ref = pars->meta.range.initial_ref;

        return FG_PRE_FUNC;
    }

    // Post-function

    if(func_time_fp32 >= pars->meta.time.end)
    {
        *ref = pars->meta.range.final_ref;

        return FG_POST_FUNC;
    }

    // Function - linear or cubic segment

    cc_double const seg_time = func_time_fp64 - pars->trim.time_offset;

    *ref = pars->trim.ref_offset + seg_time * (pars->trim.a * seg_time * seg_time + pars->trim.c);

    return FG_DURING_FUNC;
}

// EOF
