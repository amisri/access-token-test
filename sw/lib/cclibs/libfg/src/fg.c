//! @file  fg.c
//! @brief Function Generation library shared functions source file.
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"

// Global variables

FG_FuncRT fg_func_rt[] =
{
    fgNoneRT   ,
    fgZeroRT   ,
    fgRampRT   ,
    fgPulseRT  ,
    fgPlepRT   ,
    fgPpplRT   ,
    fgCubexpRT ,
    fgTableRT  ,
    fgTrimRT   ,
    fgTrimRT   ,
    fgTestRT   ,
    fgTestRT   ,
    fgTestRT   ,
    fgTestRT   ,
    fgTestRT   ,
    fgPrbsRT
};

CC_STATIC_ASSERT(CC_ARRAY_LEN(fg_func_rt) == FG_NUM_FUNCS, mismatch_between_fg_func_rt_array_len_and_number_of_function_types);

char const * const fg_func_names[] =
{
    "NONE"    ,
    "ZERO"    ,
    "RAMP"    ,
    "PULSE"   ,
    "PLEP"    ,
    "PPPL"    ,
    "CUBEXP"  ,
    "TABLE"   ,
    "LTRIM"   ,
    "CTRIM"   ,
    "STEPS"   ,
    "SQUARE"  ,
    "SINE"    ,
    "COSINE"  ,
    "OFFCOS"  ,
    "PRBS"
};

CC_STATIC_ASSERT(CC_ARRAY_LEN(fg_func_names) == FG_NUM_FUNCS, mismatch_between_fg_func_names_array_len_and_number_of_function_types);


void fgResetMetaRT(enum FG_type      const fg_type,
                   cc_float          const initial_ref,
                   struct FG_meta  * const meta,
                   struct FG_error *       fg_error)
{
    // Set the function type

    meta->type = fg_type;

    // Reset the other fields in meta structure

    meta->polarity          = FG_FUNC_POL_ZERO;
    meta->limits_inverted   = false;

    meta->time.start        = 0.0F;
    meta->time.end          = 0.0F;
    meta->time.duration     = 0.0F;

    meta->range.initial_ref = initial_ref;
    meta->range.final_ref   = initial_ref;
    meta->range.min_ref     = initial_ref;
    meta->range.max_ref     = initial_ref;
    meta->range.final_rate  = 0.0F;

    meta->limits.min        = 0.0F;
    meta->limits.max        = 0.0F;
    meta->limits.rate       = 0.0F;

    // Reset fg_error structure

    if(fg_error != NULL)
    {
        fgError(FG_OK, fg_type, 0, 0.0F, 0.0F, 0.0F, 0.0F, fg_error);
    }
}

// NON-RT   fgError

void fgError(enum FG_errno     const fg_errno,
             enum FG_type      const fg_type,
             uint32_t          const index,
             cc_float          const data0,
             cc_float          const data1,
             cc_float          const data2,
             cc_float          const data3,
             struct FG_error * const fg_error)
{
    fg_error->fg_errno = fg_errno;
    fg_error->fg_type  = fg_type;
    fg_error->index    = index;
    fg_error->data[0]  = data0;
    fg_error->data[1]  = data1;
    fg_error->data[2]  = data2;
    fg_error->data[3]  = data3;
}



void fgSetMinMaxRT(cc_float const ref, struct FG_meta * const meta)
{
    if(ref > meta->range.max_ref)
    {
        meta->range.max_ref = ref;
    }
    else if(ref < meta->range.min_ref)
    {
        meta->range.min_ref = ref;
    }
}



void fgSetMetaRT(bool                     const polswitch_auto,
                 bool                     const invert_limits,
                 cc_float                 const start_time,
                 cc_float                 const end_time,
                 cc_float                 const final_ref,
                 cc_float                 const final_rate,
                 struct FG_limits const * const limits,
                 struct FG_meta         * const meta)
{
    // Calculate function duration from start and end times

    meta->time.start    = start_time;
    meta->time.end      = end_time;
    meta->time.duration = end_time - start_time;

    // Store final ref and rate

    meta->range.final_ref  = final_ref;
    meta->range.final_rate = final_rate;

    // Set meta polarity flag - min/max range must already be set by calls to fgSetMinMax()

    if(meta->range.max_ref > 0.0F)
    {
        meta->polarity |= FG_FUNC_POL_POSITIVE;
    }

    if(meta->range.min_ref < 0.0F)
    {
        meta->polarity |= FG_FUNC_POL_NEGATIVE;
    }

    // Save the polarity switch automatic flag in the meta data

    meta->polswitch_auto = polswitch_auto;

    // Set limits inversion control based on the polarity switch control (auto/manual),
    // the switch state (invert_limits) and function polarity (meta->polarity)

    meta->limits_inverted = (polswitch_auto == false && invert_limits  == true) ||
                            (polswitch_auto == true  && meta->polarity == FG_FUNC_POL_NEGATIVE);

    // Initialize limits if provided, adjusting by a small clip limit factor to avoid rounding errors

    if(limits != NULL)
    {
        // Set min/max limits based on limits inversion control

        if(meta->limits_inverted)
        {
            // Invert limits - only ever required for unipolar converters so limits->neg will be zero

            meta->limits.max = -(1.0F - FG_CLIP_LIMIT_FACTOR) * limits->standby;
            meta->limits.min = -(1.0F + FG_CLIP_LIMIT_FACTOR) * limits->pos;
        }
        else // Limits do not need to be inverted
        {
            meta->limits.max = (1.0F + FG_CLIP_LIMIT_FACTOR) * limits->pos;

            meta->limits.min = limits->neg < 0.0F
                             ? (1.0F + FG_CLIP_LIMIT_FACTOR) * limits->neg
                             : (1.0F - FG_CLIP_LIMIT_FACTOR) * limits->standby;
        }

        // Set rate limit if it is positive

        if(limits->rate > 0.0F)
        {
            meta->limits.rate = (1.0F + FG_CLIP_LIMIT_FACTOR) * limits->rate;
        }
    }
}

// NON-RT   fgCheckRef

enum FG_errno fgCheckRef(cc_float          const ref,
                         cc_float          const rate,
                         uint32_t          const error_idx,
                         struct FG_meta  * const meta,
                         struct FG_error * const fg_error)
{
    // Check if reference exceeds min/max limits

    if(ref > meta->limits.max || ref < meta->limits.min)
    {
        fg_error->fg_errno = FG_OUT_OF_LIMITS;
        fg_error->index    = error_idx;
        fg_error->data[0]  = ref;
        fg_error->data[1]  = meta->limits.min;
        fg_error->data[2]  = meta->limits.max;
    }

    // Check if rate of change of reference exceeds limit if rate limit was provided

    else if(meta->limits.rate > 0.0F && fabsf(rate) > meta->limits.rate)
    {
        fg_error->fg_errno = FG_OUT_OF_RATE_LIMITS;
        fg_error->index    = error_idx;
        fg_error->data[0]  = fabsf(rate);
        fg_error->data[1]  = meta->limits.rate;
    }

    // else report OK

    else
    {
        fg_error->fg_errno = FG_OK;
    }

    return fg_error->fg_errno;
}

// NON-RT   fgRestorePar

uint32_t fgRestorePar(void const * const armed_par,
                      void       * const user_par,
                      size_t       const size_of_par,
                      uint32_t     const bit_index)
{
    if(memcmp(user_par, armed_par, size_of_par) != 0)
    {
        // Armed parameter is different so restore the user parameter to match the armed parameter

        memcpy(user_par, armed_par, size_of_par);

        return (1 << bit_index);
    }

    return 0;
}

// EOF
