//! @file  fgRamp.c
//! @brief Generate fast ramp based on Parabola-Parabola function
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "libfg.h"


// NON-RT   fgRampArm

void fgRampArm(struct FG_limits const * const limits,
               bool                     const polswitch_auto,
               bool                     const invert_limits,
               bool                     const test_arm,
               cc_float                 const initial_ref,
               cc_float                 const final_ref,
               cc_float                 const acceleration,
               cc_float                 const linear_rate,
               cc_float                 const deceleration,
               union  FG_pars         * const pars,
               struct FG_error        * const fg_error)
{
    // Reset meta & error structures

    struct FG_ramp  ramp_pars;      // Local RAMP pars - copied to user *pars if there are no errors

    struct FG_meta * const ramp_meta = &ramp_pars.meta;

    fgResetMetaRT(FG_RAMP, initial_ref, ramp_meta, fg_error);

    // Check that parameters are valid

    if(acceleration <= 0.0F || linear_rate < 0.0F || deceleration <= 0.0F)
    {
        fgError(FG_BAD_PARAMETER,        // fg_errno
                FG_RAMP,                 // fg_type
                0,                       // error index
                acceleration,            // error data 0
                linear_rate,             // error data 2
                deceleration,            // error data 1
                0.0F,                    // error data 3
                fg_error);

        return;
    }

    // Set terminal_delta_ref using 1 ppm of pos limit, if supplied, otherwise use 1E-4

    cc_float const terminal_delta_ref = (limits != NULL)
                                      ? limits->pos * 1.0E-6F
                                      : 1.0E-4F;

    // Calculate ramp parameters always with zero initial ramp rate

    fgRampCalcRT(polswitch_auto,
                 invert_limits,
                 0.0F,            // start_time is always zero when armed by the user
                 0.0F,            // initial_rate is always zero when armed by the user
                 initial_ref,
                 final_ref,
                 acceleration,
                 linear_rate,
                 deceleration,
                 terminal_delta_ref,
                 &ramp_pars);

    // Check limits if supplied

    if(limits != NULL)
    {
        // Prepare function limits in meta structure because limits is not available to fgRampCalc()

        fgSetMetaRT(polswitch_auto,
                    invert_limits,
                    ramp_meta->time.start,
                    ramp_meta->time.end,
                    ramp_meta->range.final_ref,
                    ramp_meta->range.final_rate,
                    limits,
                    &ramp_pars.meta);

        // Check limits at the start of the parabolic acceleration (segment 1)

        if(fgCheckRef(initial_ref, 0.0F, 0, ramp_meta, fg_error) != FG_OK ||
           fgCheckRef(final_ref,   0.0F, 1, ramp_meta, fg_error) != FG_OK)
        {
            return;
        }
    }

    // Only update pars when not doing a test arm

    if(test_arm == false)
    {
        // Save input parameters for fgRampRestorePars() to use in future

        ramp_pars.input.initial_ref  = initial_ref;
        ramp_pars.input.final_ref    = final_ref;
        ramp_pars.input.acceleration = acceleration;
        ramp_pars.input.linear_rate  = linear_rate;
        ramp_pars.input.deceleration = deceleration;

        // Copy valid set of parameters to user's pars structure and ensure that type is set from NONE to RAMP last

        pars->meta.type = FG_NONE;

        memcpy(pars, &ramp_pars, sizeof(ramp_pars));

        pars->meta.type = FG_RAMP;
    }
}


// NON-RT   fgRampRestorePars

uint32_t fgRampRestorePars(union FG_pars * const pars,
                           cc_float      * const initial_ref,
                           cc_float      * const final_ref,
                           cc_float      * const acceleration,
                           cc_float      * const linear_rate,
                           cc_float      * const deceleration)
{
    return   fgRestorePar(&pars->ramp.input.initial_ref , initial_ref , sizeof(*initial_ref ), 0)
           | fgRestorePar(&pars->ramp.input.final_ref   , final_ref   , sizeof(*final_ref   ), 1)
           | fgRestorePar(&pars->ramp.input.acceleration, acceleration, sizeof(*acceleration), 2)
           | fgRestorePar(&pars->ramp.input.linear_rate , linear_rate , sizeof(*linear_rate ), 3)
           | fgRestorePar(&pars->ramp.input.deceleration, deceleration, sizeof(*deceleration), 4);
}



void fgRampCalcRT(bool             const polswitch_auto,
                  bool             const invert_limits,
                  cc_float         const start_time,
                  cc_float         const initial_rate,
                  cc_float         const initial_ref,
                  cc_float         const final_ref,
                  cc_float         const acceleration,
                  cc_float         const linear_rate,
                  cc_float         const deceleration,
                  cc_float         const terminal_delta_ref,
                  struct FG_ramp * const pars)
{
    // Reset meta structure but without the error structure as there is no error checking in fgRampCalc()

    fgResetMetaRT(FG_RAMP, initial_ref, &pars->meta, NULL);

    // Prepare variables assuming ascending (positive) ramp

    pars->meta.type         =  FG_RAMP;
    pars->prev_time         =  start_time;
    pars->is_ramp_positive  =  true;
    pars->acceleration      =  acceleration;
    pars->deceleration      =  deceleration;
    pars->linear_rate       =  linear_rate;
    pars->linear_rate_limit =  fabsf(initial_rate);
    pars->terminal_ref      =  0.0F;
    pars->prev_ramp_ref     =  pars->prev_returned_ref = initial_ref;

    cc_float delta_ref = final_ref - initial_ref;

    cc_float const overshoot_rate_limit = sqrtf(2.0F * pars->deceleration * fabsf(delta_ref));

    // Set up accelerations according to ramp direction and possible overshoot

    if(   (delta_ref == 0.0F && initial_rate >= 0.0F)
       || (delta_ref <  0.0F && initial_rate >= -overshoot_rate_limit))
    {
        // Descending (final_ref <= initial_ref) ramp without undershoot

        pars->is_ramp_positive   =  false;
        pars->acceleration       = -pars->acceleration;
    }
    else if(   (delta_ref == 0.0F && initial_rate < 0.0F)
            || (delta_ref >  0.0F && initial_rate <= overshoot_rate_limit))
    {
        // Ascending (final_ref > initial_ref) ramp without overshoot

        pars->is_ramp_positive =  true;
        pars->deceleration     = -pars->deceleration;
    }
    else if(delta_ref > 0.0F && initial_rate > overshoot_rate_limit)
    {
        // Descending (final_ref < initial_ref) ramp with overshoot

        pars->is_ramp_positive =  false;
        pars->acceleration     = -pars->deceleration;
    }
    else
    {
        // Ascending (final_ref > initial_ref) ramp with undershoot

        pars->is_ramp_positive =  true;
        pars->acceleration     =  pars->deceleration;
        pars->deceleration     = -pars->deceleration;
    }

    // Set terminal reference based on the direction at the end of the ramp

    if(terminal_delta_ref > 0.0F)
    {
        pars->terminal_ref = final_ref + (pars->is_ramp_positive == false
                                       ?  terminal_delta_ref
                                       : -terminal_delta_ref);
    }

    // Set time_shift and ref0 and delta_ref to take into account the initial rate of change

    pars->time_shift  = -initial_rate / pars->acceleration;
    pars->is_pre_ramp = (pars->time_shift > 0.0F);

    cc_float const ref0 = initial_ref + 0.5F * initial_rate * pars->time_shift;

    delta_ref = final_ref - ref0;

    // Calculate ramp parameters

    cc_float const seg_ratio = pars->deceleration / (pars->deceleration - pars->acceleration);

    pars->time[0] = 0.0F;
    pars->time[2] = sqrtf(2.0F * delta_ref / (seg_ratio * pars->acceleration));
    pars->time[1] = pars->time[2] * seg_ratio;

    pars->ref[0]  = ref0;
    pars->ref[1]  = ref0 + delta_ref * seg_ratio;
    pars->ref[2]  = final_ref;

    // Set min/max

    fgSetMinMaxRT(initial_ref, &pars->meta);
    fgSetMinMaxRT(final_ref,   &pars->meta);

    // If time_shift is positive then include point of inflexion of first parabola in min/max check

    if(pars->time_shift > 0.0F)
    {
        fgSetMinMaxRT(ref0, &pars->meta);
    }

    // Complete meta data without limits

    fgSetMetaRT(polswitch_auto,
                invert_limits,
                start_time,
                start_time + pars->time[2] + pars->time_shift,        // end of function time
                final_ref,
                0.0F,                                                 // final_rate is always zero for RAMP
                NULL,                                                 // No limits available in fgRampCalc()
                &pars->meta);
}



enum FG_status fgRampRT(union FG_pars * const pars, cc_double * const function_time, cc_float * const ref)
{
    enum FG_status  status = FG_DURING_FUNC;                    // Set default return status
    cc_float const func_time_fp32 = *function_time;
    uint32_t time_shift_alg = 0;                                // Time shift adjustment algorithm index
    cc_float r;

    struct FG_ramp * const ramp_pars = &pars->ramp;

    // Pre-function coast

    if(func_time_fp32 < pars->meta.time.start)
    {
        r = pars->meta.range.initial_ref;

        status = FG_PRE_FUNC;
    }
    else
    {
        // If reference received from previous iteration was changed, and isn't blocked

        if(*ref != ramp_pars->prev_ramp_ref && *ref != ramp_pars->prev_returned_ref)
        {
            // Identify time shift adjustment algorithm according to ramp direction

            if(ramp_pars->is_ramp_positive)
            {
                // Positive (rising) ramp

                if(*ref > ramp_pars->ref[0])
                {
                    if(ramp_pars->is_pre_ramp)
                    {
                         time_shift_alg = 1;
                    }
                    else if(*ref <= ramp_pars->ref[1])
                    {
                         time_shift_alg = 2;
                    }
                    else if(*ref <= ramp_pars->ref[2])
                    {
                         time_shift_alg = 3;
                    }
                }
            }
            else
            {
                // Negative (falling) ramp

                if(*ref < ramp_pars->ref[0])
                {
                    if(ramp_pars->is_pre_ramp)
                    {
                         time_shift_alg = 1;
                    }
                    else if(*ref >= ramp_pars->ref[1])
                    {
                         time_shift_alg = 2;
                    }
                    else if(*ref >= ramp_pars->ref[2])
                    {
                         time_shift_alg = 3;
                    }
                }
            }

            // Adjust time shift using appropriate algorithm

            switch(time_shift_alg)
            {
                case 1: ramp_pars->time_shift = ramp_pars->prev_time - ramp_pars->meta.time.start +
                                                sqrtf(2.0F * (*ref - ramp_pars->ref[0]) / ramp_pars->acceleration);
                        break;

                case 2: ramp_pars->time_shift = ramp_pars->prev_time - ramp_pars->meta.time.start -
                                                sqrtf(2.0F * (*ref - ramp_pars->ref[0]) / ramp_pars->acceleration);
                        break;

                case 3: ramp_pars->time_shift = ramp_pars->prev_time - ramp_pars->meta.time.start -
                                               (ramp_pars->time[2] - sqrtf(2.0F * (*ref - ramp_pars->ref[2]) / ramp_pars->deceleration)); // deceleration always +ve
                        break;

                default:break;
            }
        }

        // Calculate new ref_time including delay and time_shift

        cc_double ref_time = func_time_fp32 - ramp_pars->meta.time.start - ramp_pars->time_shift;

        // Parabolic acceleration

        if(ref_time < ramp_pars->time[1])
        {
            r = ramp_pars->ref[0] + 0.5 * ramp_pars->acceleration * ref_time * ref_time;

            // Reset is_pre_ramp once the main part of the ramp is started

            if(ref_time >= 0.0F)
            {
                ramp_pars->is_pre_ramp = false;
            }
        }

        // Parabolic deceleration

        else if(ref_time < ramp_pars->time[2])
        {
            // ref_time is relative to end of parabola (negative)

            ref_time -= ramp_pars->time[2];

            r = ramp_pars->ref[2] + 0.5 * ramp_pars->deceleration * ref_time * ref_time;
        }

        // Coast

        else
        {
            r = ramp_pars->ref[2];

            // If the terminal_ref is defined then only end of function once the
            // returned reference crosses the terminal reference threshold

            if(    ramp_pars->terminal_ref == 0.0F
               || (ramp_pars->is_ramp_positive == false && *ref <= ramp_pars->terminal_ref)
               || (ramp_pars->is_ramp_positive == true  && *ref >= ramp_pars->terminal_ref))
            {
                status = FG_POST_FUNC;
            }
        }

        // Keep ramp reference for next iteration (before rate limiter)

        ramp_pars->prev_ramp_ref = r;

        // Apply rate limit if active

        cc_float ref_rate_limit;

        cc_float const period = func_time_fp32 - ramp_pars->prev_time;

        if(ramp_pars->linear_rate > 0.0F && period > 0.0F)
        {
            if(ramp_pars->linear_rate_limit < ramp_pars->linear_rate)
            {
                ramp_pars->linear_rate_limit = ramp_pars->linear_rate;
            }

            if(r > *ref)
            {
                // Positive rate of change

                ref_rate_limit = *ref + ramp_pars->linear_rate_limit * period;

                if(r > ref_rate_limit)
                {
                    r = ref_rate_limit;
                }
            }
            else if(r < *ref)
            {
                // Negative rate of change

                ref_rate_limit = *ref - ramp_pars->linear_rate_limit * period;

                if(r < ref_rate_limit)
                {
                    r = ref_rate_limit;
                }
            }

            // Adjust linear rate limit if greater than user value, respecting acceleration

            if(ramp_pars->linear_rate_limit > ramp_pars->linear_rate)
            {
                ramp_pars->linear_rate_limit -= period * fabsf(ref_time < ramp_pars->time[1]
                                                               ? ramp_pars->acceleration
                                                               : ramp_pars->deceleration);
            }
        }
    }

    // Keep returned reference and time for next iteration

    ramp_pars->prev_returned_ref = *ref;
    ramp_pars->prev_time = func_time_fp32;

    // Return new reference after rate limit

    *ref = r;

    return status;
}

// EOF
