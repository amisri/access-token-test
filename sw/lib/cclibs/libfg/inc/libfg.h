//! @file      libfg.h
//! @brief     Function Generation library top-level header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! \mainpage CERN Converter Control Library: Function Generation
//!
//! Libfg generates the converter control reference (field, current or voltage)
//! as a pre-programmed function of time. It is managed by libref and is not
//! directly called by application code. It currently implements the following
//! types of function:
//!
//! |    |   File    | Type    | Function                                                        |
//! |----|-----------|---------|-----------------------------------------------------------------|
//! |  0 | fgNone.c  | NONE    | Leaves reference value unchanged                                |
//! |  1 | fgZero.c  | ZERO    | Zero reference                                                  |
//! |  2 | fgRamp.c  | RAMP    | Parabola-parabola function with time shift                      |
//! |  3 | fgPulse.c | PULSE   | Fixed reference for given duration                              |
//! |  4 | fgPlep.c  | PLEP    | Parabola-linear-exponential-parabola                            |
//! |  5 | fgPppl.c  | PPPL    | Sequence of parabola-parabola-parabola-linear segments          |
//! |  6 | fgPppl.c  | CUBEXP  | Sequence of cubic segments                                      |
//! |  7 | fgTable.c | TABLE   | Linearly interpolated table                                     |
//! |  8 | fgTrim.c  | LTRIM   | Linear trim                                                     |
//! |  9 | fgTrim.c  | CTRIM   | Cubic trim                                                      |
//! | 10 | fgTest.c  | STEPS   | Sequence of steps                                               |
//! | 11 | fgTest.c  | SQUARE  | Offset square wave                                              |
//! | 12 | fgTest.c  | SINE    | Centered sine wave with optional window and exponential decay   |
//! | 13 | fgTest.c  | COSINE  | Centered cosine wave with optional window and exponential decay |
//! | 14 | fgTest.c  | OFFCOS  | Offset cosine wave                                              |
//! | 15 | fgPrbs.c  | PRBS    | Pseudo-random binary sequence                                   |

#pragma once

#include "cclibs.h"
#include "libfg/fgConsts.h"

// Enums

//! Libfg reference limits structure

struct FG_limits
{
    cc_float            pos;                    //!< Positive reference limit
    cc_float            standby;                //!< Standby absolute reference limit
    cc_float            neg;                    //!< Negative reference limit
    cc_float            rate;                   //!< Rate of change limit
};

//! Libfg function error structure

struct FG_error
{
    enum FG_errno       fg_errno;               //!< Function error number
    enum FG_type        fg_type;                //!< Function type
    uint32_t            index;                  //!< Error index (e.g. table point or PPPL segment)
    cc_float            data[FG_ERR_DATA_LEN];  //!< Error debug data - helps to diagnose error
};

//! Forward declaration of union

union FG_pars;

//! Typedef for pointer to real-time reference generation function

typedef enum FG_status (*FG_FuncRT) (union FG_pars *, cc_double *, cc_float *);

//! Declare array of pointers to real-time FG functions - it is defined in fg.c

extern FG_FuncRT fg_func_rt[];

//! Libfg function meta data structure
//!
//! Most reference functions start at time zero and end with a zero rate of change,
//! but there are exceptions, as indicated in the table.
//!
//! |    |   File    |  Type   |meta.time.start|  meta.range.final_rate |
//! |----|-----------|---------|---------------|------------------------|
//! |  0 | fgNone.c  | NONE    | 0.0           | 0.0                    |
//! |  1 | fgZero.c  | ZERO    | 0.0           | 0.0                    |
//! |  2 | fgRamp.c  | RAMP    | start_time    | 0.0                    |
//! |  3 | fgPulse.c | PULSE   | 0.0           | 0.0                    |
//! |  4 | fgPlep.c  | PLEP    | 0.0           | final_rate             |
//! |  5 | fgPppl.c  | PPPL    | 0.0           | rate4[rate4_num_els-1] |
//! |  6 | fgCubExp.c| CUBEXP  | time[0]       | rate[rate_num_els-1]   |
//! |  7 | fgTable.c | TABLE   | time[0]       | 0.0                    |
//! |  8 | fgTrim.c  | LTRIM   | 0.0           | 0.0                    |
//! |  9 | fgTrim.c  | CTRIM   | 0.0           | 0.0                    |
//! | 10 | fgTest.c  | STEPS   | 0.0           | 0.0                    |
//! | 11 | fgTest.c  | SQUARE  | 0.0           | 0.0                    |
//! | 12 | fgTest.c  | SINE    | 0.0           | 0.0                    |
//! | 13 | fgTest.c  | COSINE  | 0.0           | 0.0                    |
//! | 14 | fgTest.c  | OFFCOS  | 0.0           | 0.0                    |
//! | 15 | fgPrbs.c  | PRBS    | 0.0           | 0.0                    |
//!
//! Function time is divided into three phases:
//!
//!     1. PRE_FUNC    :                    func_time <  meta.time.start
//!     2. DURING_FUNC : meta.time.start <= func_time <  meta.time.end
//!     3. POST_FUNC   :                    func_time >= meta.time.end

struct FG_meta
{
    enum FG_type        type;                   //!< Function type
    enum FG_func_pol    polarity;               //!< Function polarity
    bool                limits_inverted;        //!< Function was checked against inverted limits
    bool                polswitch_auto;         //!< Polarity switch is automatic

    struct FG_meta_time
    {
        cc_float        start;                  //!< Time of start of function
        cc_float        end;                    //!< Time of end of function
        cc_float        duration;               //!< Function duration (end - start)
    } time;

    struct FG_meta_range
    {
        cc_float        initial_ref;            //!< Reference value at start of function
        cc_float        min_ref;                //!< Minimum value of the function
        cc_float        max_ref;                //!< Maximum value of the function
        cc_float        final_ref;              //!< Reference value at the end of the function
        cc_float        final_rate;             //!< Rate of change of reference value at the end of the function
    } range;

    struct FG_meta_limits
    {
        cc_float        min;                    //!< Minimum function limit
        cc_float        max;                    //!< Maximum function limit
        cc_float        rate;                   //!< Maximum absolute rate of change limit
    } limits;
};

//! Global variables

extern char const * const fg_func_names[];

//! Include all libfg header files

#include "libfg/fgNone.h"
#include "libfg/fgZero.h"
#include "libfg/fgRamp.h"
#include "libfg/fgPulse.h"
#include "libfg/fgPlep.h"
#include "libfg/fgPppl.h"
#include "libfg/fgCubexp.h"
#include "libfg/fgTable.h"
#include "libfg/fgTrim.h"
#include "libfg/fgTest.h"
#include "libfg/fgPrbs.h"

//! Libfg union of meta and all function parameter structs (which all start with struct meta).

union FG_pars
{
   struct FG_meta      meta;
   struct FG_none      none;
   struct FG_zero      zero;
   struct FG_ramp      ramp;
   struct FG_pulse     pulse;
   struct FG_plep      plep;
   struct FG_pppl      pppl;
   struct FG_cubexp    cubexp;
   struct FG_table     table;
   struct FG_trim      trim;
   struct FG_test      test;
   struct FG_prbs      prbs;
};

//! FG macro to call real-time function generation function

#define fgFuncRT(fg_pars, function_time, ref)  fg_func_rt[(fg_pars)->meta.type](fg_pars, function_time, ref)

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Report a libfg error.
//!
//! If an error with parameters is detected by a libfg initialization function,
//! this function is used to record the cause of the error, an error index and
//! up to four floats of debug data that can help in the analysis of the error.
//!
//! @param[in]  fg_errno      Libfg error number.
//! @param[in]  fg_type       Function type index.
//! @param[in]  index         Error index.
//! @param[in]  data0         Error data 0.
//! @param[in]  data1         Error data 1.
//! @param[in]  data2         Error data 2.
//! @param[in]  data3         Error data 3.
//! @param[out] error         Pointer to fg_error structure used to return detailed error codes.

void fgError(enum FG_errno     fg_errno,
             enum FG_type      fg_type,
             uint32_t          index,
             cc_float          data0,
             cc_float          data1,
             cc_float          data2,
             cc_float          data3,
             struct FG_error * error);

#ifdef __cplusplus
}
#endif

// EOF
