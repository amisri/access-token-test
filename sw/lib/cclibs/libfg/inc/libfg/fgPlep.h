//! @file    fgPlep.h
//! @brief   Generate Parabola-Linear-Exponential-Parabola (PLEP) functions
//!
//! A PLEP can have up to five segments: Parabola-Linear-Exponential-Parabola-Parabola.
//!
//! The PLEP function is special because the initial and final reference can have a
//! non-zero rate of change. If the final rate of change is not zero, this adds a fifth parabolic
//! segment. This can be an extension of the fourth parabola, or it can have the
//! opposite acceleration.
//!
//! The initial rate of change is provided as a service to libref for the pre-function
//! segment estimation. It is not expected to be used in operation.
//!
//! For further details, see <a href="../pdf/PLEP.pdf">PLEP.pdf</a>.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

// Constants

#define FG_PLEP_NUM_SEGS        5                   //!< Number of segments: P-L-E-P-P = 5

//! PLEP function input parameters - saved so that they can be restored if arming fails.

struct FG_plep_input
{
    cc_float       initial_ref;                     //!< Initial reference.
    cc_float       final_ref;                       //!< Final reference.
    cc_float       acceleration;                    //!< Acceleration.
    cc_float       linear_rate;                     //!< Maximum linear rate.
    cc_float       exp_tc;                          //!< Exponential section time constant.
    cc_float       exp_final;                       //!< Final value of exponential section.
};

//! PLEP function parameters

struct FG_plep
{
    struct FG_meta meta;                            //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_plep_input input;                     //!< Input parameters.

    cc_float       rms;                             //!< Estimated RMS for the PLEP.
    cc_float       acceleration;                    //!< Parabolic acceleration (signed).
    cc_float       deceleration;                    //!< Parabolic deceleration (signed).
    cc_float       final_acc;                       //!< Final parabolic acceleration (signed).
    cc_float       linear_rate;                     //!< Linear rate of change (signed).
    cc_float       final_rate;                      //!< Final linear rate of change (signed).
    cc_float       ref_exp;                         //!< Initial reference for exponential segment - exp_final.
    cc_float       inv_exp_tc;                      //!< -1 / Time constant for exponential segment.
    cc_float       exp_final;                       //!< Asymptote for exponential segment.
    cc_float       seg_ref [FG_PLEP_NUM_SEGS+1];    //!< Start/End of segment references.
    cc_float       seg_time[FG_PLEP_NUM_SEGS+1];    //!< Start/End of segment times.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm a PLEP function.
//!
//! NOTE: The deceleration parameter is provided to support the pre-function algorithm in libref.
//! It is not mapped to a libref parameter, so it is not stored in the FG_plep_input structure, and
//! is not restored by fgPlepRestorePars.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     initial_rate      Initial rate of change (signed).
//! @param[in]     final_rate        Final rate of change (signed).
//! @param[in]     initial_ref       Initial reference value.
//! @param[in]     final_ref         Final reference value.
//! @param[in]     acceleration      Acceleration (must be positive).
//! @param[in]     deceleration      Deceleration (must be positive).
//! @param[in]     linear_rate       Maximum linear rate (must be positive or zero to disable linear section).
//! @param[in]     exp_tc            Time constant for exponential section (zero to disable exponential section).
//! @param[in]     exp_final         Final reference for exponential section.
//!                                  The actual value of exp_final is returned in *exp_final_auto.
//! @param[out]    pars              Pointer to fg_pars union containing plep parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgPlepArm(const struct FG_limits * limits,
               bool                      polswitch_auto,
               bool                      invert_limits,
               bool                      test_arm,
               cc_float                  initial_rate,
               cc_float                  final_rate,
               cc_float                  initial_ref,
               cc_float                  final_ref,
               cc_float                  acceleration,
               cc_float                  deceleration,
               cc_float                  linear_rate,
               cc_float                  exp_tc,
               cc_float                  exp_final,
               union  FG_pars          * pars,
               struct FG_error         * fg_error);


//! Restore PLEP input parameters from armed PLEP parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed PLEP parameter structure.
//! @param[out]    initial_ref       Pointer to variable to restore with the initial reference value.
//! @param[out]    final_ref         Pointer to variable to restore with the final reference value.
//! @param[out]    acceleration      Pointer to variable to restore with the acceleration.
//! @param[out]    linear_rate       Pointer to variable to restore with the linear rate.
//! @param[out]    exp_tc            Pointer to variable to restore with the time constant for exponential section.
//! @param[out]    exp_final         Pointer to variable to restore with the final reference for exponential section.
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgPlepRestorePars(union FG_pars * pars,
                           cc_float      * initial_ref,
                           cc_float      * final_ref,
                           cc_float      * acceleration,
                           cc_float      * linear_rate,
                           cc_float      * exp_tc,
                           cc_float      * exp_final);


//! Real-time function to generate a PLEP reference.
//!
//! Derive the reference for the previously-initialized PLEP function at the given
//! function time. The PLEP will start at time 0. The PLEP can have a non-zero
//! final rate of change.
//!
//! @param[in]     pars              Pointer to fg_pars union containing plep parameters structure.
//!                                  This contains the coordinates of the transition points between the
//!                                  segments of the PLEP function:
//!                                  <ul>
//!                                  <li>pars->plep.seg_time[0], pars->plep.seg_ref[0]: Min or Max of the first parabola</li>
//!                                  <li>pars->plep.seg_time[1], pars->plep.seg_ref[1]: End of the first parabola</li>
//!                                  <li>pars->plep.seg_time[2], pars->plep.seg_ref[2]: End of the linear segment</li>
//!                                  <li>pars->plep.seg_time[3], pars->plep.seg_ref[3]: End of the exponential segments</li>
//!                                  <li>pars->plep.seg_time[4], pars->plep.seg_ref[4]: End of the second (decelerating) parabola</li>
//!                                  <li>pars->plep.seg_time[5], pars->plep.seg_ref[5]: End of the third (accelerating) parabola and end of the PLEP function</li>
//!                                  </ul>
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to the reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end).

enum FG_status fgPlepRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
