//! @file    fgRamp.h
//! @brief   Generate fast ramp based on Parabola-Parabola function.
//!
//! RAMP is a special function within libfg in two ways:
//!
//! 1. It can start with a non-zero start time and rate of change (by calling fgRampCalc())
//! 2. It can responds to rate of change limits in the calling application.
//!
//! The first feature allows the RAMP function to take over from any running function.
//!
//! The second feature allow a RAMP to go as fast as possible, with limits applied
//! by the calling application, while still arriving smoothly at the final value.
//! Note that the function duration returned in FG_meta meta.time.duration will be wrong if the
//! reference is limited at any time during the generation of the function.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

// Constants

#define FG_RAMP_NUM_SEGS        2               //!< Number of RAMP segments: Parabola-Parabola = 2

//! RAMP function input parameters - saved so that they can be restored if arming fails.

struct FG_ramp_input
{
    cc_float       initial_ref;                 //!< Initial reference.
    cc_float       final_ref;                   //!< Final reference.
    cc_float       acceleration;                //!< Acceleration.
    cc_float       linear_rate;                 //!< Maximum linear rate.
    cc_float       deceleration;                //!< Deceleration.
};

//! RAMP function meta data, input parameters and real-time parameters.

struct FG_ramp
{
    struct FG_meta meta;                        //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_ramp_input input;                 //!< Input parameters.

    cc_float       acceleration;                //!< Parabolic acceleration.
    cc_float       linear_rate;                 //!< User linear rate.
    cc_float       deceleration;                //!< Parabolic deceleration.
    cc_float       linear_rate_limit;           //!< Actual linear rate limit.
    cc_float       ref [FG_RAMP_NUM_SEGS+1];    //!< End of segment references.
    cc_float       time[FG_RAMP_NUM_SEGS+1];    //!< End of segment times.
    cc_float       prev_ramp_ref;               //!< Function ref from previous iteration.
    cc_float       prev_returned_ref;           //!< Returned ref from previous iteration.
    cc_float       terminal_ref;                //!< Threshold on returned_ref to end the ramp.
    cc_float       time_shift;                  //!< Time shift due to limits applied by the application.
    cc_float       prev_time;                   //!< Time from previous iteration.
    bool           is_ramp_positive;            //!< Positive ramp flag.
    bool           is_pre_ramp;                 //!< Pre-ramp flag. True if before point of inflexion of 1st parabola.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm RAMP function and check limits.
//!
//! This function uses fgRampCalc() to prepare the Ramp parameters.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     initial_ref       Initial reference value (at start_time).
//! @param[in]     final_ref         Final reference value.
//! @param[in]     acceleration      Acceleration of the 1st parabolic segment. Absolute value is used.
//! @param[in]     linear_rate       Maximum linear rate. Absolute value is used.
//! @param[in]     deceleration      Deceleration of the 2nd parabolic segment. Absolute value is used.
//! @param[out]    pars              Pointer to fg_pars union containing ramp parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgRampArm(const struct FG_limits * limits,
               bool                     polswitch_auto,
               bool                     invert_limits,
               bool                     test_arm,
               cc_float                 initial_ref,
               cc_float                 final_ref,
               cc_float                 acceleration,
               cc_float                 linear_rate,
               cc_float                 deceleration,
               union  FG_pars         * pars,
               struct FG_error        * fg_error);


//! Restore RAMP input parameters from armed RAMP parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed RAMP parameter structure.
//! @param[out]    initial_ref       Pointer to variable to restore with the Initial reference value (at start_time).
//! @param[out]    final_ref         Pointer to variable to restore with the Final reference value.
//! @param[out]    acceleration      Pointer to variable to restore with the Maximum linear rate.
//! @param[out]    linear_rate       Pointer to variable to restore with the linear rate limit.
//! @param[out]    deceleration      Pointer to variable to restore with the Deceleration of the 2nd parabolic segment.
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgRampRestorePars(union FG_pars * pars,
                           cc_float      * initial_ref,
                           cc_float      * final_ref,
                           cc_float      * acceleration,
                           cc_float      * linear_rate,
                           cc_float      * deceleration);


//! Arm RAMP function without checking limits.
//!
//! This function is used by fgRampArm(). It does no limit checking. It is a separate function so
//! a real-time thread can initialize a ramp, for example, to take over from another running function.
//! To allow this, the function accepts a start_time and initial rate of change.
//!
//! @param[in]     polswitch_auto      True if polarity switch can be changed automatically.
//! @param[in]     invert_limits       True if polarity switch mode and state requires limits to be inverted
//! @param[in]     start_time          Time of the start of the function.
//! @param[in]     initial_rate        Initial rate of change.
//! @param[in]     initial_ref         Initial reference value.
//! @param[in]     final_ref           Final reference value.
//! @param[in]     acceleration        Acceleration of the 1st parabolic segment. Absolute value is used.
//! @param[in]     linear_rate         Maximum linear rate. Absolute value is used.
//! @param[in]     deceleration        Deceleration of the 2nd parabolic segment. Absolute value is used.
//! @param[in]     terminal_delta_ref  Threshold on |returned_ref - final_ref| to end the ramp.
//! @param[out]    pars                Pointer to ramp function parameters.
//!                                    The coordinates of the transition points between
//!                                    the segments of the ramp function are:
//!                                    <ul>
//!                                    <li>pars->time[0], pars->ref[0]: Max/min of first (accelerating) parabola</li>
//!                                    <li>pars->time[1], pars->ref[1]: Connection between accelerating and decelerating parabolas</li>
//!                                    <li>pars->time[2], pars->ref[2]: End of the second (decelerating) parabola, also end of the ramp function</li>
//!                                    </ul>

void fgRampCalcRT(bool             polswitch_auto,
                  bool             invert_limits,
                  cc_float         start_time,
                  cc_float         initial_rate,
                  cc_float         initial_ref,
                  cc_float         final_ref,
                  cc_float         acceleration,
                  cc_float         linear_rate,
                  cc_float         deceleration,
                  cc_float         terminal_delta_ref,
                  struct FG_ramp * pars);


//! Real-time function to generate a RAMP reference.
//!
//! Derive the reference for the previously-initialized Ramp function at the given time.
//!
//! Note: Unlike the other libfg functions, TIME MUST NOT GO BACKWARDS
//! because *ref is both an input and an output.
//!
//! @param[in]     pars              Pointer to fg_pars union containing ramp parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[in,out] ref               Pointer to the reference value. If the application needed to clip
//!                                  the reference, fgRampRT() will take this into account.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgRampRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
