//! @file    fgPrivate.h
//! @brief   Function Generation library private header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define FG_CLIP_LIMIT_FACTOR    0.001           //!< Scale factor for user limits

// External functions

#ifdef __cplusplus
extern "C" {
#endif

//! Reset all the meta data fields.
//!
//! This is a private function, used by the libfg initialization functions.
//!
//! When a function is initialized, the meta data structure is filled with a summary of the function's
//! characteristics, including the delay, duration, min/max and start and end values.
//! In addition, an error structure is filled in to help understand why a function
//! failed to arm.
//!
//! @param[in]     fg_type           Type of function
//! @param[in]     initial_ref       Initial value for the start, end, min and max.
//! @param[out]    meta              Pointer to fg_meta data structure to initialize.
//! @param[out]    fg_error          Pointer to fg_error structure to initialize.

void fgResetMetaRT(enum FG_type      fg_type,
                   cc_float          initial_ref,
                   struct FG_meta  * meta,
                   struct FG_error * fg_error);


//! Set the meta min and max fields
//!
//! This is a private function, used by the libfg initialization functions. It is used to
//! record the min/max range of the reference before calling fgSetMeta.
//!
//! @param[in]     ref       Value for reference to check against meta min and max.
//! @param[in,out] meta      Pointer to fg_meta structure.

void fgSetMinMaxRT(cc_float ref, struct FG_meta * meta);


//! Set the function's meta polarity and limits (if provided)
//!
//! This is a private function, used by libfg initialization functions. This function needs
//! the min/max range of the reference to have been set already by calls to fgSetMinMax.
//!
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     start_time        Time of start of function.
//! @param[in]     end_time          Time of end of function.
//! @param[in]     final_ref         Reference at end of function.
//! @param[in]     final_rate        Rate of change of reference at end of function.
//! @param[in]     limits            Pointer to fg_limits structure (can be NULL).
//! @param[in,out] meta              Pointer to fg_meta structure.

void fgSetMetaRT(bool                     polswitch_auto,
                 bool                     invert_limits,
                 cc_float                 start_time,
                 cc_float                 end_time,
                 cc_float                 final_ref,
                 cc_float                 final_rate,
                 const struct FG_limits * limits,
                 struct FG_meta         * meta);


//! Check function reference value, rate and acceleration against the supplied limits.
//!
//! This is a private function, used by libfg initialization functions.
//!
//! When a function is initialized, this is called to check the function value,
//! rate and acceleration (in that order) against the limits in meta (set by fgSetFuncPolarity).
//! The function returns the status of the first limit that is exceeded and the return value
//! indicates the type of error (REF or RATE). The error::data array
//! provides extra information about limit that was exceeded:
//!
//! |ERROR.FG_ERRNO & RETURN VALUE|ERROR.DATA[0]|ERROR.DATA[1]|ERROR.DATA[2]|
//! |-----------------------------|-------------|-------------|-------------|
//! |FG_OK                        |unchanged    |unchanged    |unchanged    |
//! |FG_OUT_OF_LIMITS             |ref          |min          |max          |
//! |FG_OUT_OF_RATE_LIMITS        |fabs(rate)   |rate_limit   |0.0          |
//!
//! @param[in]     ref               Reference value.
//! @param[in]     rate              Rate of change of reference.
//! @param[in]     error_idx         Error index to save if limit exceeded.
//! @param[in]     meta              Pointer to fg_meta structure containing limits.
//! @param[out]    fg_error          Pointer to fg_error structure used to return detailed error codes.
//!
//! @retval FG_OK on success.
//! @retval FG_OUT_OF_LIMITS if function value is out of limits.
//! @retval FG_OUT_OF_RATE_LIMITS if function rate of change is out of limits.

enum FG_errno fgCheckRef(cc_float          ref,
                         cc_float          rate,
                         uint32_t          error_idx,
                         struct FG_meta  * meta,
                         struct FG_error * fg_error);


//! Restore user parameter to matched armed parameter
//!
//! This is a private function, used by libfg initialization functions.
//!
//! This function tests to see if the user parameter is different from the armed
//! parameter, and only if it is does it over write the user parameter with the
//! armed parameter. In this case it returns a bit mask with the bit defined
//! by bit index
//!
//! @param[in]     armed_par         Pointer to the armed parameter value.
//! @param[in]     user_par          Pointer to the user parameter value.
//! @param[in]     size_of_par       Size in bytes of the parameter value.
//! @param[in]     bit_index         Bit to return set to 1 if the user parameter is restored from the armed parameter.
//!
//! @returns       bit_mask with the bit identified by bit_index set to 1 if the user parameter is restored.

uint32_t fgRestorePar(void const * armed_par,
                      void       * user_par,
                      size_t       size_of_par,
                      uint32_t     bit_index);

#ifdef __cplusplus
}
#endif

// EOF
