//! @file    fgCubexp.h
//! @brief   Generate chained cubexp functions
//!
//! The CUBEXP function allows a series of points to be linked by cubexp.
//! The reference and rate of change at each point must be specified.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! Constants

#define FG_CUBEXP_MAX_POINTS    15                           //!< Max number start/end points for cubexp segments
#define FG_CUBEXP_MAX_SEGS      (FG_CUBEXP_MAX_POINTS-1)     //!< Max number of cubexp segments

//! CUBEXP function input parameters - saved so that they can be restored if arming fails.

struct FG_cubexp_input
{

    cc_float                ref [FG_CUBEXP_MAX_POINTS];      //!< Reference at start/end of each cubexp segment.
    cc_float                rate[FG_CUBEXP_MAX_POINTS];      //!< Rate at the start/end of each cubexp segment.
    cc_float                time[FG_CUBEXP_MAX_POINTS];      //!< Time at the start/end of each cubexp segment.
    uint32_t                num_points;                      //!< Number of points
};

//!         Cubic segment function                    Exponential segment function
//!
//!    ref = a3.t^3 + a2.t^2 + a1.t + a0              ref = a0 + a2.(1 - exp(a3.t))
//!
//! where t is the segment time, which is zero at the end of the segment.

struct FG_cubexp_coeffs
{
    cc_float                a0;                              //!< Cubexp: Coeff for static term.    Exp: ref1
    cc_float                a1;                              //!< Cubexp: Coeff for linear term.    Exp: rate1
    cc_float                a2;                              //!< Cubexp: Coeff for quadratic term. Exp: ref_inf - ref1
    cc_float                a3;                              //!< Cubexp: Coeff for cubic term.     Exp: -1/tau
};

struct FG_cubexp
{
    struct FG_meta          meta;                            //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_cubexp_input  input;                           //!< Input parameters.

    uint32_t                seg_idx;                         //!< Current segment index.
    uint32_t                num_segs;                        //!< Total number of segments
    bool                    exp_seg [FG_CUBEXP_MAX_SEGS];    //!< True for exponential segment, false for cubic segment
    cc_float                seg_time[FG_CUBEXP_MAX_SEGS];    //!< Times of the end of each segment relative to the start of the function.
    cc_float                max_ref [FG_CUBEXP_MAX_SEGS];    //!< Max reference for each segment.
    cc_float                min_ref [FG_CUBEXP_MAX_SEGS];    //!< Min reference for each segment.
    cc_float                max_rate[FG_CUBEXP_MAX_SEGS];    //!< Max rate for each segment.
    struct FG_cubexp_coeffs coeffs  [FG_CUBEXP_MAX_SEGS];    //!< Cubic coefficients for the segments
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm CUBEXP function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     ref               Start/end reference of cubic segments.
//! @param[in]     rate              Start/end rate of cubic segments.
//! @param[in]     time              Start/end times of cubic segments.
//! @param[in]     num_points        Number of start/end points defined by the parameter arrays.
//! @param[out]    pars              Pointer to fg_pars union containing cubexp parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgCubexpArm(struct FG_limits const * limits,
                 bool                     polswitch_auto,
                 bool                     invert_limits,
                 bool                     test_arm,
                 cc_float         const   ref [FG_CUBEXP_MAX_POINTS],
                 cc_float                 rate[FG_CUBEXP_MAX_POINTS],
                 cc_float         const   time[FG_CUBEXP_MAX_POINTS],
                 uint32_t                 num_points,
                 union  FG_pars         * pars,
                 struct FG_error        * fg_error);


//! Restore CUBEXP input parameters from armed CUBEXP parameters if changed.
//!
//! Note: The number of elements parameters are not included in the bit mask returned.
//!
//! @param[in]     pars            Pointer to fg_pars union containing armed CUBEXP parameter structure.
//! @param[out]    ref             Start/end reference of cubic segments.
//! @param[out]    rate            Start/end rate of cubic segments.
//! @param[out]    time            Start/end times of cubic segments.
//! @param[out]    ref_num_els     Number of element in ref array.
//! @param[out]    rate_num_els    Number of element in rate array.
//! @param[out]    time_num_els    Number of element in time array.
//!
//! @returns       bit_mask with three bits representing the three parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.
//!                The num_els parameters are not included.

uint32_t fgCubexpRestorePars(union FG_pars * pars,
                             cc_float        ref [FG_CUBEXP_MAX_POINTS],
                             cc_float        rate[FG_CUBEXP_MAX_POINTS],
                             cc_float        time[FG_CUBEXP_MAX_POINTS],
                             uintptr_t     * ref_num_els,
                             uintptr_t     * rate_num_els,
                             uintptr_t     * time_num_els);


//! Real-time function to generate a CUBEXP reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing cubexp parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgCubexpRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
