//! @file    fgTest.h
//! @brief   Generate test functions (STEPS, SQUARE, SINE or COSINE)
//!
//! Four types of test function are supported in the test function family:
//!
//! <ul>
//! <li> STEPS:  The reference is stepped up or down from its initial value to
//!              the initial value + amplitude_pp in equal-sized steps. </li>
//! <li> SQUARE: Creates a square wave of peak-peak amplitude amplitude_pp
//!              offset from the initial value. </li>
//! <li> SINE:   Creates a sine wave of amplitude amplitude_pp at
//!              each period. If use_window != 0, the curve is
//!              smoothed at the beginning of the first and end of the last period. </li>
//! <li> COSINE: creates a cosine wave with the same behaviour as SINE. </li>
//! </ul>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! TEST function input parameters - saved so that they can be restored if arming fails.

struct FG_test_input
{
    cc_float                 initial_ref;       //!< Initial reference value.
    cc_float                 amplitude_pp;      //!< Peak-to-peak amplitude.
    uint32_t                 num_periods;       //!< Number of waveform periods or steps.
    uint32_t                 period_iters;      //!< Sampling period in iterations
    cc_float                 period;            //!< Test function period in seconds.
    enum CC_enabled_disabled window_enabled;    //!< Set true to use window function (for sine & cosine only).
    enum CC_enabled_disabled exp_decay_enabled; //!< Set true to use exponential decay when window is enabled.
};

//! TEST function parameters

struct FG_test
{
    struct FG_meta           meta;              //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_test_input     input;             //!< Input parameters.

    enum FG_type             fg_type;           //!< Type of test function.
    bool                     window_enabled;    //!< Window control: true to use window for sine & cosine.
    uint32_t                 num_periods;       //!< Number of waveform periods or steps.
    uint32_t                 period_iters;      //!< Number of waveform periods or steps.
    uint32_t                 iter_idx;          //!< Iteration index.
    cc_float                 frequency;         //!< 1 / period.
    cc_float                 half_period;       //!< period / 2.
    cc_float                 offset;            //!< Reference amplitude.
    cc_float                 amplitude;         //!< Reference amplitude.
    cc_float                 exp_decay;         //!< Exp decay factor (0 if disabled)
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm TEST function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure - no limits are checked for test functions.
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     fg_type           Type of test function (FG_STEPS, FG_SQUARE, FG_SINE, FG_COSINE).
//! @param[in]     initial_ref       Initial reference value.
//! @param[in]     amplitude_pp      Peak-to-peak amplitude.
//! @param[in]     num_periods       Number of waveform periods or steps.
//! @param[in]     period_iters      Sampling period in iterations.
//! @param[in]     period            Period.
//! @param[in]     window_enabled    Set to CC_ENABLED to use window function (for sine & cosine only).
//! @param[in]     exp_decay_enabled Set to CC_ENABLED to use exponential decay when window is enabled.
//! @param[out]    pars              Pointer to fg_pars union containing test parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgTestArmRT(const struct FG_limits * limits,
                 bool                     polswitch_auto,
                 bool                     invert_limits,
                 bool                     test_arm,
                 enum FG_type             fg_type,
                 cc_float                 initial_ref,
                 cc_float                 amplitude_pp,
                 uint32_t                 num_periods,
                 uint32_t                 period_iters,
                 cc_float                 period,
                 enum CC_enabled_disabled window_enabled,
                 enum CC_enabled_disabled exp_decay_enabled,
                 union  FG_pars         * pars,
                 struct FG_error        * fg_error);


//! Restore TEST input parameters from armed TEST parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed TEST parameter structure.
//! @param[out]    initial_ref       Pointer to variable to restore with the Initial reference value.
//! @param[out]    amplitude_pp      Pointer to variable to restore with the Peak-to-peak amplitude.
//! @param[out]    num_periods       Pointer to variable to restore with the Number of periods/steps.
//! @param[out]    period_iters      Pointer to variable to restore with the Sampling period in function generator iterations.
//! @param[out]    period            Pointer to variable to restore with the Period.
//! @param[out]    window_enabled    Pointer to variable to restore with the Window function control.
//! @param[out]    exp_decay_enabled Pointer to variable to restore with the Exponential decay control.
//!
//! @returns       bit_mask with seven bits representing the seven parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgTestRestorePars(union FG_pars            * pars,
                           cc_float                 * initial_ref,
                           cc_float                 * amplitude_pp,
                           uint32_t                 * num_periods,
                           uint32_t                 * period_iters,
                           cc_float                 * period,
                           enum CC_enabled_disabled * window_enabled,
                           enum CC_enabled_disabled * exp_decay_enabled);


//! Real-time function to generate a SINE, COSINE, SQUARE or STEPS reference.
//!
//! The function is evaluated at the specified period in iterations.
//!
//! @param[in]     pars              Pointer to fg_pars union containing test parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to new reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgTestRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
