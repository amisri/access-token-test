//! @file    fgPrbs.h
//! @brief   Pseudo random binary sequence functions
//!
//! The length of a PSBR-k sequence is 2^k - 1 periods. The libfg PRBS function allows
//! up to 1000 complete PRBS-k sequences to be generated, where k is between 3 and 31 .
//! The PRBS waveform will be centered about the initial reference.
//!
//! See https://en.wikipedia.org/wiki/Pseudorandom_binary_sequence
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! PRBS function input parameters - saved so that they can be restored if arming fails.

struct FG_prbs_input
{
    cc_float            initial_ref;        //!< Initial reference.
    cc_float            amplitude_pp;       //!< Peak-to-peak amplitude.
    uint32_t            period_iters;       //!< PSRB period in function generator iterations.
    uint32_t            num_sequences;      //!< Number of sequences to generate (1-1000).
    uint32_t            k;                  //!< PSBR sequence order (3-31) - the sequence length will be 2^k - 1 periods.
};

//! PRBS function parameters

struct FG_prbs
{
    struct FG_meta      meta;               //!< Meta data for the armed function - this must be the first field in the struct.

    struct FG_prbs_input input;             //!< Input parameters.

    uint32_t            period_iters;       //!< Period in iterations.
    uint32_t            period_counter;     //!< Period counter.
    uint32_t            num_sequences;      //!< Number of PRBS sequences to generate.
    uint32_t            sequence_counter;   //!< Sequence counter.
    uint32_t            lfsr;               //!< Linear feedback shift register.
    uint32_t            lfsr_mask;          //!< Linear feedback shift register mask.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm PRBS function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure - no limits are checked for prbs functions.
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     initial_ref       Initial reference value.
//! @param[in]     amplitude_pp      Peak-to-peak amplitude.
//! @param[in]     iter_period       Iteration period in seconds.
//! @param[in]     period_iters      PSRB period in iterations.
//! @param[in]     num_sequences     Number of sequences to generate (1-1000).
//! @param[in]     k                 PSBR sequence order (3-31) - the sequence length will be 2^k - 1 periods.
//! @param[out]    pars              Pointer to fg_pars union containing prbs parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgPrbsArm(const struct FG_limits * limits,
               bool                     polswitch_auto,
               bool                     invert_limits,
               bool                     test_arm,
               cc_float                 iter_period,
               cc_float                 initial_ref,
               cc_float                 amplitude_pp,
               uint32_t                 period_iters,
               uint32_t                 num_sequences,
               uint32_t                 k,
               union  FG_pars         * pars,
               struct FG_error        * fg_error);


//! Restore PRBS input parameters from armed PRBS parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed PRBS parameter structure.
//! @param[out]    initial_ref       Pointer to variable to restore with the initial reference value.
//! @param[out]    amplitude_pp      Pointer to variable to restore with the Peak-to-peak amplitude.
//! @param[out]    period_iters      Pointer to variable to restore with the PSRB period in iterations.
//! @param[out]    num_sequences     Pointer to variable to restore with the Number of sequences to generate (1-1000).
//! @param[out]    k                 Pointer to variable to restore with the PSBR sequence order (3-31).
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgPrbsRestorePars(union FG_pars * pars,
                           cc_float      * initial_ref,
                           cc_float      * amplitude_pp,
                           uint32_t      * period_iters,
                           uint32_t      * num_sequences,
                           uint32_t      * k);


//! Real-time function to generate a PRBS reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing PRBS parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to new reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgPrbsRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
