//! @file    fgTrim.h
//! @brief   Generate linear and cubic trim functions
//!
//! Two types of trim function are supported, linear and cubic. It is possible to
//! define the duration of the trim, or to go as fast as possible while respecting
//! the limits. For more details, see <a href="../pdf/TRIMS.pdf">TRIMS.pdf</a>.
//!
//! <h4>CTRIM or Cubic Trim</h4>
//!
//! The Cubic Trim reference function is ref = a.t^3 + c.t
//!
//! \image html  Cubic_Trim.png
//!
//! <h4>LTRIM or Linear Trim</h4>
//!
//! The Linear Trim reference function is ref = c.t
//!
//! \image html  Linear_Trim.png
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! TRIM function input parameters - saved so that they can be restored if arming fails.

struct FG_trim_input
{
    cc_float            initial_ref;    //!< Initial reference value.
    cc_float            final_ref;      //!< Final reference value.
    cc_float            linear_rate;    //!< Linear rate to use if duration is not specified.
    cc_float            duration;       //!< Function duration (if <= 0 then use linear_rate to define duration).
};

//! TRIM function parameters.
//!
//! ref = a.t^3 + c.t + ref_offset,
//! where t is the time since the start of the function.

struct FG_trim
{
    struct FG_meta      meta;           //!< Meta data for the armed function - this must be the first in the struct

    struct FG_trim_input input;         //!< Input parameters.

    cc_float            time_offset;    //!< Time base offset for cubic.
    cc_float            ref_offset;     //!< Reference offset.
    cc_float            a;              //!< Coefficient for cubic term.
    cc_float            c;              //!< Coefficient for constant term.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm TRIM function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     fg_type           Type of trim function (FG_LTRIM or FG_CTRIM).
//! @param[in]     initial_ref       Initial reference value.
//! @param[in]     final_ref         Final reference value.
//! @param[in]     linear_rate       Linear rate to use if duration is not specified.
//! @param[in]     duration          Function duration (if <= 0 then use linear_rate to define duration).
//! @param[out]    pars              Pointer to fg_pars union containing trim parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgTrimArm(const struct FG_limits * limits,
               bool                     polswitch_auto,
               bool                     invert_limits,
               bool                     test_arm,
               enum FG_type             fg_type,
               cc_float                 initial_ref,
               cc_float                 final_ref,
               cc_float                 linear_rate,
               cc_float                 duration,
               union  FG_pars         * pars,
               struct FG_error        * fg_error);


//! Restore TRIM input parameters from armed TRIM parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed TRIM parameter structure.
//! @param[out]    initial_ref       Pointer to variable to restore with the Initial reference value.
//! @param[out]    final_ref         Pointer to variable to restore with the Final reference value.
//! @param[out]    duration          Pointer to variable to restore with the Function duration.
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgTrimRestorePars(union FG_pars * pars,
                           cc_float      * initial_ref,
                           cc_float      * final_ref,
                           cc_float      * duration);


//! Real-time function to generate a CTRIM or LTRIM reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing trim parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgTrimRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
