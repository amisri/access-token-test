//! @file    fgTable.h
//! @brief   Generate linearly interpolated table functions.
//!
//! This function is provided with a list of reference values and times that the
//! reference values take effect, i.e. it calculates the reference by linear interpolation
//! between the points provided.
//!
//! \image html  Interpolation_error.png
//!
//! If the table is approximating a parabola with acceleration a,
//! then the maximum error during the segment between two points will be:
//!
//! Max_Error E = a.T^2/8
//!
//! The table supports automatic pause points. If a point is repeated, fgTableRT will
//! return FG_PAUSED when the time arrives at, or pass the time of the repeated point.
//! This allows libref to implement PAUSE/RESUME behavior.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! Point struct contains a pair of time and reference values

struct FG_point
{
    cc_float                time;               //!< Time since start of the table (first point must have time 0.0)
    cc_float                ref;                //!< Reference value at the specified time
};

//! Table function parameters

struct FG_table
{
    struct FG_meta          meta;                //!< Meta data for the armed function - this must be the first in the struct
    uint32_t                seg_idx;             //!< Segment index - identifies point at the end of the segment
    uint32_t                prev_seg_idx;        //!< Previous segment index for which the rate of change was calculated.
    uint32_t                num_points;          //!< Number of points in table.
    const struct FG_point * points;              //!< Pointer to table function points array.
    cc_float                seg_rate;            //!< Rate of change of reference for segment fg_table::prev_seg_idx
    bool                    allow_pause;         //!< True if pause points will be respected
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm TABLE function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     points            Pointer to array of table points.
//! @param[in]     armed_points      Pointer to array of armed table points (set to NULL if points array to be used).
//! @param[in]     num_points        Number of elements in time array.
//! @param[in]     min_time_step     Minimum time between points in the table.
//! @param[out]    pars              Pointer to fg_pars union containing table parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgTableArm(const struct FG_limits * limits,
                bool                     polswitch_auto,
                bool                     invert_limits,
                bool                     test_arm,
                const struct FG_point  * points,
                struct FG_point        * armed_points,
                uint32_t                 num_points,
                cc_float                 min_time_step,
                union  FG_pars         * pars,
                struct FG_error        * fg_error);


//! Restore TABLE input parameters from armed TABLE parameters if changed.
//!
//! Note: The number of points parameter is not included in the bit mask returned.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed TABLE parameter structure.
//! @param[in]     max_points        Maximum number of points - used to ensure excess points are zeroed.
//! @param[out]    points            Pointer to array to restore with the armed points.
//! @param[out]    num_points        Pointer to variable to restore with the number of points.
//!
//! @retval true  if input parameters were restored from armed parameters.
//! @retval false if input parameters were not restored because a TABLE is not armed.
//!                The num_points parameters is not included.

uint32_t fgTableRestorePars(union FG_pars   * pars,
                            uint32_t          max_points,
                            struct FG_point * points,
                            uintptr_t       * num_points);


//! Real-time function to generate a TABLE reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing pulse parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to reference value.
//!
//! @retval FG_PRE_FUNC    function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC function time is during the function.
//! @retval FG_POST_FUNC   function time is after the end of the function (i.e. func_time >= pars->meta.time.end)
//! @retval FG_PAUSED      function time arrived at, or passed a pause point in the table.

enum FG_status fgTableRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);


//! Real-time function to replace an active table with the next table.
//!
//! If a table is replaced while it is running, then the number of points might be different
//! and the time series might be different, which will require fgTableRT to search for the new
//! segment corresponding to the function time. However, the new table is unlikely to be very
//! different from the active table, so preserving the segment index is likely to reduce the
//! search time. This function will transfer over the segment index from the active to the next
//! parameters, protecting against the length of the next table and possible pause points.
//!
//! @param[in]     next_pars         Pointer to the next table pars
//! @param[in]     active_pars       Pointer to the active table pars

void fgTableReplaceParsRT(union FG_pars * next_pars, union FG_pars * active_pars);


//! Real-time function that can reset a table so that it can be played again.
//!
//! fgTableRT can handle time jumping from the end of the table back to the start, however,
//! this can take a long time as it searches for time linearly. This could cause a
//! an overrun in the real-time processing, so this function can be called to
//! reset the segment index and allow_pause flag.
//!
//! @param[in]     pars              Pointer to the table pars

void fgTableResetParsRT(union FG_pars * pars);

#ifdef __cplusplus
}
#endif

// EOF
