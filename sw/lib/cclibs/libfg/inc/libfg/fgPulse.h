//! @file    fgPulse.h
//! @brief   Generate linear pulse functions
//!
//! A pulse is simply a linear section starting at time 0 for the
//! specified duration. If the linear section is a ramp, then the function
//! will generation a parabolic sectioning before time 0 in order to
//! accelerate to the linear rate of the ramp.
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! PULSE function input parameters - saved so that they can be restored if arming fails.

struct FG_pulse_input
{
    cc_float       ref;                 //!< Pulse reference.
    cc_float       duration;            //!< Pulse duration.
};

//! PULSE function parameters

struct FG_pulse
{
    struct FG_meta meta;                //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_pulse_input input;        //!< Input parameters.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm PULSE function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    Set true if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted.
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     ref               Pulse reference value.
//! @param[in]     duration          Pulse duration.
//! @param[out]    pars              Pointer to fg_pars union containing pulse parameter struct.
//! @param[out]    fg_error          Pointer to error information.

void fgPulseArm(const struct FG_limits * limits,
                bool                     polswitch_auto,
                bool                     invert_limits,
                bool                     test_arm,
                cc_float                 ref,
                cc_float                 duration,
                union  FG_pars         * pars,
                struct FG_error        * fg_error);


//! Restore PULSE input parameters from armed PULSE parameters if changed.
//!
//! @param[in]     pars              Pointer to fg_pars union containing armed PULSE parameter structure.
//! @param[out]    ref               Pointer to variable to restore with the pulse reference value.
//! @param[out]    duration          Pointer to variable to restore with the Pulse duration.
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.

uint32_t fgPulseRestorePars(union FG_pars * pars,
                            cc_float      * ref,
                            cc_float      * duration);


//! Real-time function to generate a PULSE reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing pulse parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgPulseRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
