//! @file    fgPppl.h
//! @brief   Generate Parabola-Parabola-Parabola-Linear (PPPL) functions
//!
//! The PPPL function allows a series of plateaus to be linked by smooth
//! parabolic accelerations and decelerations.
//!
//! <h4>PPPL Reference Definition</h4>
//!
//! <ul>
//! <li>Seven parameters define each PPPL section from four segments</li>
//! <li>Up to eight PPPL sections can be chained together</li>
//! <li>If ACCELERATION2 is zero, it becomes a PLPL</li>
//! <li>Ramps can be up or down</li>
//! </ul>
//!
//! \image html  PPPL_parameters.png
//! \image latex PPPL_parameters.png "PPPL Reference Definition" width=\textwidth
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

// Constants

#define FG_MAX_PPPLS      8                              //!< Max number of PPPL sections that can be chained together
#define FG_PPPL_NUM_SEGS  4                              //!< Number of segments in each PPPL section (P-P-P-L = 4)
#define FG_MAX_PPPL_SEGS  FG_PPPL_NUM_SEGS*FG_MAX_PPPLS  //!< Max number of PPPL segments

//! PPPL function input parameters - saved so that they can be restored if arming fails.

struct FG_pppl_input
{
    cc_float       initial_ref;                     //!< Initial reference.
    cc_float       acceleration1[FG_MAX_PPPLS];     //!< Accelerations of first (parabolic) segments.
    cc_float       acceleration2[FG_MAX_PPPLS];     //!< Accelerations of second (parabolic) segments.
    cc_float       acceleration3[FG_MAX_PPPLS];     //!< Accelerations of third  (parabolic) segments.
    cc_float       rate2        [FG_MAX_PPPLS];     //!< Rates of change at start of second (parabolic) segments.
    cc_float       rate4        [FG_MAX_PPPLS];     //!< Rates of change of fourth (linear) segments.
    cc_float       ref4         [FG_MAX_PPPLS];     //!< References at start of fourth (linear) segments.
    cc_float       duration4    [FG_MAX_PPPLS];     //!< Durations of fourth (linear) segments.
    uint32_t       num_pppls;                       //!< Number of PPPLs
};

//! PPPL function parameters.
//!
//!    ref = seg_a2.t^2 + seg_a1.t + seg_a0
//!
//! where t is the time in the segment which is always negative, since t=0 corresponds to the time at the end of the segment.

struct FG_pppl
{
    struct FG_meta meta;                            //!< Meta data for the armed function - this must be the first in the struct.

    struct FG_pppl_input input;                     //!< Input parameters.

    uint32_t       seg_idx;                         //!< Current segment index.
    uint32_t       num_segs;                        //!< Total number of segments (4 x Number of PPPLs).
    cc_float       seg_time[FG_MAX_PPPL_SEGS];      //!< Times of the end of each segment relative to the start of the function.
    cc_float       seg_a0  [FG_MAX_PPPL_SEGS];      //!< Coefficient for static term.
    cc_float       seg_a1  [FG_MAX_PPPL_SEGS];      //!< Coefficient for linear term.
    cc_float       seg_a2  [FG_MAX_PPPL_SEGS];      //!< Coefficient for quadratic term.
};

#ifdef __cplusplus
extern "C" {
#endif

// External functions

//! Arm PPPL function.
//!
//! @param[in]     limits            Pointer to fgc_limits structure (or NULL if no limits checking required).
//! @param[in]     polswitch_auto    True if polarity switch can be changed automatically.
//! @param[in]     invert_limits     True if polarity switch mode and state requires limits to be inverted
//! @param[in]     test_arm          True if test arming - pars will not be modified.
//! @param[in]     initial_ref       Initial reference value.
//! @param[in]     acceleration1     Accelerations of first (parabolic) segments.
//! @param[in]     acceleration2     Accelerations of second (parabolic) segments.
//! @param[in]     acceleration3     Accelerations of third  (parabolic) segments.
//! @param[in]     rate2             Rates of change at start of second (parabolic) segments.
//! @param[in]     rate4             Rates of change of fourth (linear) segments.
//! @param[in]     ref4              References at start of fourth (linear) segments.
//! @param[in]     duration4         Durations of fourth (linear) segments.
//! @param[in]     num_pppls         Number of PPPLs defined by the parameter arrays.
//! @param[out]    pars              Pointer to fg_pars union containing pppl parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgPpplArm(struct FG_limits const * limits,
               bool                     polswitch_auto,
               bool                     invert_limits,
               bool                     test_arm,
               cc_float                 initial_ref,
               cc_float           const acceleration1[FG_MAX_PPPLS],
               cc_float           const acceleration2[FG_MAX_PPPLS],
               cc_float           const acceleration3[FG_MAX_PPPLS],
               cc_float           const rate2        [FG_MAX_PPPLS],
               cc_float           const rate4        [FG_MAX_PPPLS],
               cc_float           const ref4         [FG_MAX_PPPLS],
               cc_float           const duration4    [FG_MAX_PPPLS],
               uint32_t                 num_pppls,
               union  FG_pars         * pars,
               struct FG_error        * fg_error);


//! Restore PPPL input parameters from armed PPPL parameters if changed.
//!
//! Note: The number of elements parameters are not included in the bit mask returned.
//!
//! @param[in]     pars                    Pointer to fg_pars union containing armed PPPL parameter structure.
//! @param[out]    initial_ref             Pointer to variable to restore with the initial reference value.
//! @param[out]    acceleration1           Accelerations of first  (parabolic) segments.
//! @param[out]    acceleration2           Accelerations of second (parabolic) segments.
//! @param[out]    acceleration3           Accelerations of third  (parabolic) segments.
//! @param[out]    rate2                   Rates of change at start of second (parabolic) segments.
//! @param[out]    rate4                   Rates of change of fourth (linear) segments.
//! @param[out]    ref4                    References at start of fourth (linear) segments.
//! @param[out]    duration4               Durations of fourth (linear) segments.
//! @param[out]    acceleration1_num_els   Number of element in acceleration1 array.
//! @param[out]    acceleration2_num_els   Number of element in acceleration2 array.
//! @param[out]    acceleration3_num_els   Number of element in acceleration3 array.
//! @param[out]    rate2_num_els           Number of element in rate2         array.
//! @param[out]    rate4_num_els           Number of element in rate4         array.
//! @param[out]    ref4_num_els            Number of element in ref4          array.
//! @param[out]    duration4_num_els       Number of element in duration4     array.
//!
//! @returns       bit_mask with five bits representing the five parameters in order.
//!                If the bit is 1 it indicates that this parameter was restored from pars.
//!                The num_els parameters are not included.

uint32_t fgPpplRestorePars(union FG_pars * pars,
                           cc_float      * initial_ref,
                           cc_float        acceleration1[FG_MAX_PPPLS],
                           cc_float        acceleration2[FG_MAX_PPPLS],
                           cc_float        acceleration3[FG_MAX_PPPLS],
                           cc_float        rate2        [FG_MAX_PPPLS],
                           cc_float        rate4        [FG_MAX_PPPLS],
                           cc_float        ref4         [FG_MAX_PPPLS],
                           cc_float        duration4    [FG_MAX_PPPLS],
                           uintptr_t     * acceleration1_num_els,
                           uintptr_t     * acceleration2_num_els,
                           uintptr_t     * acceleration3_num_els,
                           uintptr_t     * rate2_num_els,
                           uintptr_t     * rate4_num_els,
                           uintptr_t     * ref4_num_els,
                           uintptr_t     * duration4_num_els);


//! Real-time function to generate a PPPL reference.
//!
//! @param[in]     pars              Pointer to fg_pars union containing pppl parameter struct.
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to reference value.
//!
//! @retval FG_PRE_FUNC    if function time is before the start of the function (i.e. func_time < 0).
//! @retval FG_DURING_FUNC if function time is during the function.
//! @retval FG_POST_FUNC   if function time is after the end of the function (i.e. func_time >= pars->meta.time.end)

enum FG_status fgPpplRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
