//! @file    fgNone.h
//! @brief   None function will leave reference unchanged
//!
//! <h4>Contact</h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4>Copyright</h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4>License</h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libfg/fgPrivate.h"

//! NONE function parameters

struct FG_none
{
    struct FG_meta meta;           //!< Meta data for the armed function - this must be the first in the struct.
};

#ifdef __cplusplus
extern "C" {
#endif

//! Arm NONE function.
//!
//! @param[out]    pars              Pointer to fg_pars union containing none parameter structure.
//! @param[out]    fg_error          Pointer to error information.

void fgNoneArmRT(union FG_pars * pars, struct FG_error * fg_error);


//! Real-time function to generate a static reference.
//!
//! This function leaves *ref unchanged.
//!
//! @param[in]     pars              Pointer to fg_pars union
//! @param[in]     function_time     Pointer to time within the function.
//! @param[out]    ref               Pointer to the reference value - ignored.
//!
//! @retval FG_POST_FUNC indicates that the function has always finished.

enum FG_status fgNoneRT(union FG_pars * pars, cc_double * function_time, cc_float * ref);

#ifdef __cplusplus
}
#endif

// EOF
