//! @file      fgConsts.h
//! @brief     Function Generation library public constants header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libfg.
//!
//! libfg is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Constants

#define FG_ERR_DATA_LEN         4               //!< Length of FG_error data array

// Enums

//! Returned status from real-time reference generation functions

enum FG_status
{
    FG_POST_FUNC   ,                            //!< Time is after the end of the function
    FG_DURING_FUNC ,                            //!< Time is during the function
    FG_PRE_FUNC    ,                            //!< Time is before the function
    FG_PAUSED      ,                            //!< Time is paused at the start of a function within a function list
    FG_NUM_STATUSES                             //!< Number of FG_status constants
};

//! Reference function polarity bit mask constants.

enum FG_func_pol                                // Note: POL_BOTH = POL_POSITIVE | POL_NEGATIVE
{
    FG_FUNC_POL_ZERO     = 0x0,                 //!< Function is entirely zero
    FG_FUNC_POL_POSITIVE = 0x1,                 //!< Function is entirely positive
    FG_FUNC_POL_NEGATIVE = 0x2,                 //!< Function is entirely negative
    FG_FUNC_POL_BOTH     = 0x3,                 //!< Function is both positive and negative
    FG_FUNC_NUM_POLARITIES
};

//! Libfg function types

enum FG_type
{
    FG_NONE      ,                              //!< Leaves reference value unchanged
    FG_ZERO      ,                              //!< Zero reference
    FG_RAMP      ,                              //!< Parabola-parabola function with time shift
    FG_PULSE     ,                              //!< Fixed reference for given duration
    FG_PLEP      ,                              //!< Parabola-linear-exponential-parabola
    FG_PPPL      ,                              //!< Sequence of parabola-parabola-parabola-linear segments
    FG_CUBEXP    ,                              //!< Sequence of cubic or exponential segments
    FG_TABLE     ,                              //!< Linearly interpolated table
    FG_LTRIM     ,                              //!< Linear trim
    FG_CTRIM     ,                              //!< Cubic trim
    FG_STEPS     ,                              //!< Sequence of steps
    FG_SQUARE    ,                              //!< Offset square wave
    FG_SINE      ,                              //!< Centered sine wave with optional window and exponential decay
    FG_COSINE    ,                              //!< Centered cosine wave with optional window and exponential decay
    FG_OFFCOS    ,                              //!< Offset cosine wave
    FG_PRBS      ,                              //!< Pseudo-random binary sequence
    FG_NUM_FUNCS                                //!< Number of types of reference function
};

//! Libfg error numbers - some additional libref statuses are included to simplify error reporting by libref.

enum FG_errno
{
    FG_OK                 ,                     //!< libfg : No error
    FG_BAD_ARRAY_LEN      ,                     //!< libfg : Array length is invalid
    FG_BAD_PARAMETER      ,                     //!< libfg : Parameter is invalid
    FG_INVALID_TIME       ,                     //!< libfg : Time is invalid
    FG_OUT_OF_LIMITS      ,                     //!< libfg : Reference is out of limits
    FG_OUT_OF_RATE_LIMITS ,                     //!< libfg : Rate of change of reference is out of limits
    FG_INIT_REF_MISMATCH  ,                     //!< libref: Initial reference mismatches actual reference
    FG_INVALID_REG_MODE   ,                     //!< Libref: Regulation mode is invalid
    FG_INVALID_REF_STATE  ,                     //!< Libref: Reference state is invalid
    FG_INVALID_SUB_SEL    ,                     //!< Libref: Sub-device selector is invalid
    FG_INVALID_CYC_SEL    ,                     //!< Libref: Cycle selector is invalid
    FG_NUM_ERRORS                               //!< Number of FG errors
};

// EOF
