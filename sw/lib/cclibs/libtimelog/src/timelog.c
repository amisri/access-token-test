//! @file  timelog.c
//! @brief Converter Control Cycle Logging library initialization functions
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libtimelog.
//!
//! libtimelog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdint.h>

#include "libtimelog.h"


void timelogInit(struct TIMELOG_device       * timelog_device,
                 TIMELOG_event_type_callback * str_event_type_function)
{
    memset(timelog_device, 0, sizeof(struct TIMELOG_device));

    timelog_device->str_event_type_function = str_event_type_function;
}



void timelogStore(struct TIMELOG_device     * timelog_device,
                  struct CC_us_time   const * time_stamp,
                  uint32_t            const   delay_us,
                  uint8_t             const   sub_sel,
                  uint8_t             const   cyc_sel,
                  uint8_t             const   type)
{
    struct TIMELOG_record * record = &timelog_device->records[timelog_device->head_index];

    // Cancel this record by reseting the unix_time time stamp

    record->time_stamp.secs.abs = 0;

    // Add the record

    record->delay_us      = delay_us;
    record->sub_sel       = sub_sel;
    record->cyc_sel       = cyc_sel;
    record->type          = type;
    record->time_stamp.us = time_stamp->us;

    CC_MEMORY_BARRIER;

    // Store the Unix time part of the time stamp last to validate the rest of the record

    record->time_stamp.secs.abs = time_stamp->secs.abs;

    // Only advance head_index after the boundary check to prevent timelogRead() from
    // using an invalid index

    uint16_t head_index = timelog_device->head_index + 1;

    timelog_device->head_index = head_index >= TIMELOG_NUM_RECORDS ? 0 : head_index;
}



void timelogRead(struct TIMELOG_device     * timelog_device,
                 uint32_t             const  record_index,
                 char                        read_buf[TIMELOG_READ_BUF_LEN])
{
    // Initialize or advance record index to the next record

    if (record_index >= TIMELOG_NUM_RECORDS)
    {
        return;
    }

    uint32_t                buf_index = (timelog_device->head_index + record_index) % TIMELOG_NUM_RECORDS;
    struct TIMELOG_record * record    = &timelog_device->records[buf_index];
    char                  * buf       = read_buf;

    // Make sure the record is valid

    if (record->time_stamp.secs.abs != 0)
    {
        buf += snprintf(buf, TIMELOG_READ_BUF_LEN, "%10u.%06u %2u|%2u %10u %19s",
                                                    (unsigned int)record->time_stamp.secs.abs,
                                                    (unsigned int)record->time_stamp.us,
                                                    (unsigned int)record->sub_sel,
                                                    (unsigned int)record->cyc_sel,
                                                    (unsigned int)record->delay_us,
                                                    timelog_device->str_event_type_function(record->type));
    }

    *buf = '\0';
}


// EOF
