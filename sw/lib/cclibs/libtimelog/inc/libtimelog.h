//! @file  libtimelog.h
//! @brief Converter Control Cycle Logging library header file
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libtimelog.
//!
//! libtimelog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <stdint.h>

#include "cclibs.h"


// Constants

//! The read event FGC log format is:
//!
//! {time_stamp.s}.{time_stamp.us} {sub_sel}|{cyc_sel} {delay} {event_type}\0
//!
//!     time_stamp.s     : 10
//!     decimal point    :  1
//!     time_stamp.us    :  6
//!     sub_sel          :  2
//!     vertical line    :  1
//!     cyc_sel          :  2
//!     delay            : 10
//!     event_type       : 19
//!     space delimiters :  3
//!     Null termination :  1
//!     TOTAL CHARACTERS : 55

#define TIMELOG_READ_BUF_LEN         55
#define TIMELOG_NUM_RECORDS          64


// typedefs

typedef char * TIMELOG_event_type_callback(uint32_t const type);  //!< Type for function type string callback


//!< Time log record

struct TIMELOG_record
{
    struct CC_us_time     time_stamp;                          //!< Timestamp
    uint32_t              delay_us;                            //!< Delay in microseconds
    uint8_t               sub_sel;                             //!< Sub-device selector
    uint8_t               cyc_sel;                             //!< Cycle selector
    uint8_t               type;                                //!< Event type
};


//!< Cycle log data for a device

struct TIMELOG_device
{
    TIMELOG_event_type_callback * str_event_type_function;     //!< Function type string callback
    uint16_t                      head_index;                  //!< Buffer head index
    struct TIMELOG_record         records[TIMELOG_NUM_RECORDS]; //!< Timing records
};


#ifdef __cplusplus
extern "C" {
#endif


//! Initialize time log device structure
//!
//! @param[in]    timelog_device            Pointer to time log structure to initialize
//! @param[in]    str_event_type_function   Pointer to function for string event type

void timelogInit(struct TIMELOG_device       * timelog_device,
                 TIMELOG_event_type_callback * str_event_type_function);


//! Store one record in the time log
//!
//! This funciton is not thread-safe
//!
//! @param[in]    timelog_device    Pointer to time log structure
//! @param[in]    time_stamp        Event time stamp
//! @param[in]    delay_us          Elay in microseconds
//! @param[in]    sub_sel           Sub-device selector
//! @param[in]    cyc_sel           Cycle selector
//! @param[in]    type              Event type

void timelogStore(struct TIMELOG_device     * timelog_device,
                  struct CC_us_time   const * time_stamp,
                  uint32_t                    delay_us,
                  uint8_t                     sub_sel,
                  uint8_t                     cyc_sel,
                  uint8_t                     type);


//! This function will copy the time log record identified by record_index to the destination buffer.
//! This function is not thread safe.
//!
//! @param[in]      timelog_device  Pointer to the time log device structure
//! @param[in]      record_index    Record_index to copy
//! @param[in]      read_buf        Pointer to destination buffer

void timelogRead(struct TIMELOG_device * timelog_device,
                 uint32_t                record_index,
                 char                    read_buf[TIMELOG_READ_BUF_LEN]);


#ifdef __cplusplus
}
#endif

// EOF