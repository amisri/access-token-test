#!/bin/bash
# Retrieve what systems use a DIM and what variant

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
	echo 'Usage: '$0' <HCRCode> (<VariantNum>)'
	exit
fi
git_root=$(git rev-parse --show-toplevel)
# xml_path=../../../def/src
xml_path=$git_root/def/src


# remove digits from the HCRCode
hcrcode=`echo $1 | sed 's/[0-9]//g'`

# build dim_name ignoring first two letters of HCRCode and converting to lowercase
dim_name=`echo ${hcrcode:2} | awk '{print tolower($0)}'`;

# find all variants by 'globbing' for filename pattern ${dim_name}_a* and then extracting the numeric part
# since 2022, there are XML + (auto-generated) YAML files for each, so we also need to de-duplicate the results
dim_variants=`ls $xml_path/dim_types/"$dim_name"_a* | grep -oE "[0-9]+" | sort -n | uniq`;

if [ $# -eq 1 ]; then
	# single argument passed (HCRCode)

	for file in $dim_variants;
	do
		echo "$(tput bold) Variant $file used by: $(tput sgr0)";
		# of course this is completely broken, you cannot expect to grep over XML and get sensible results
		matches=( $(grep -lr 'type="'$hcrcode'..."\s\+position="A"\s\+variant="'$file'"' $xml_path/*) )
		for match in "${matches[@]}"; do
			basename $match | awk -F "." '{print $1}';
		done
	done
elif [ $# -eq 2 ]; then
	# two arguments passed (HCRCode, VariantNum)

	echo
	echo 'The variant' $2 'of the DIM' $hcrcode 'is used by:';
	grep -lr 'type="'$hcrcode'..."\s\+position="A"\s\+variant="'$2'"' $xml_path/*;
	variantUses=`grep -lr 'type="'$hcrcode'..."\s\+position="A"\s\+variant="'$2'"' $xml_path/* | wc | awk '{print $1}'`;

	# are there multiple system types using this variant?
	if [ $variantUses -gt 1 ]; then
		total=1;
		variant=0;
		while [ $total -ne 0 ]; do
			let variant=variant+1;
			total=`grep -lr 'type="'$hcrcode'..."\s\+position="A"\s\+variant="'$variant'"' $xml_path/* | wc -l`;
		done
		echo
		echo 'Variant '$variant' can be (re)used.';
	else
		dim_used=`grep -lr 'type="'$hcrcode'..."\s\+position="A"\s\+variant="'$2'"' $xml_path/* | xargs basename | awk -F "." '{print toupper($1)}'`;
		echo
		echo 'The variant' $2 'of the DIM' $1 'is only used by '$dim_used', you can modify.'
	fi
fi
