#!/bin/bash
# Find what targets run instances of a given converter type

# Return what FECs run a converter type
function wtfec(){
#       get all the occurrences of the converter RP*** in the namefile
        grep -rnisI "$1" /user/pclhc/etc/fgcd/name | \
#       split line by ':' and get only the 2nd value, which correspoponds to the FEC name
        awk -F':' '{ print $2 }'| \
#       get rid of repeated gateways
        uniq
}

# Get FECs and targets where a converter is deployed
function wtarget(){
        IFS=$'\n'
        source ~pclhc/bin/python/ccs_venv/bin/activate
        for fec in $(wtfec $1)
        do
                target=$(ccs.deployment.deployment_targets_for_host $fec)
                echo "$fec --> $target"
        done
        deactivate
}

# Do not run if we are sourcing the script
if [ "source" != "$0" ]; then
	# Check number of args
	if [ "$#" -lt "1" ]; then
		echo "Usage: wtarget <CONVERTER_TYPE>"
	else
		wtarget $1
	fi
fi

#EOF
