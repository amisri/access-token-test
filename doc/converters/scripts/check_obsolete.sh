#!/bin/bash
#
# Filename: check_obsolete.sh
#
# Purpose:  Get obsolete or unused FGC systems by checking which ones are not present in the name file
#

git_root=$(git rev-parse --show-toplevel)

systems_path=$git_root/def/src/systems/
deployed_systems_file=/user/pclhc/etc/fgcd/name

#get system names from the generated XMLs in the repo, removing the file extension
systems=$(ls -1 $systems_path | awk -F'.xml' '{ print $1}')

#parse the class and system from the name file, sorting alphabetically and removing repetitions
deployed_systems=$(awk -F':' '{ print $3 ":" $4}' $deployed_systems_file | awk -F'.' '{ print $1 }' | sort | uniq)

for system in $systems
do
  # check if system is not deployed
  if [[ $deployed_systems != *"$system"* ]]; then

    # extract the attribute platform from the tag system inside the system xml
    platform=$(xmllint --xpath 'string(//system/@platform)' $systems_path/$system.xml)

    echo "$system --> $platform"
  fi

done

#EOF
