#!/bin/bash
# Find variants with the same definitions or find available variants

git_root=$(git rev-parse --show-toplevel)
dims_path=$git_root/def/src/dim_types


function printVariants(){
        # exit if we have other than 2 input arguments
        if [ "$#" != "2" ]; then return; fi

        current_card=$(getShortHCRCode $1)
        current_variant=$(getVariant $1)
        local variants=( $2 )

        if [ "${#variants[@]}" -eq "0" ];
        then
	# there were no matches
                if [ "$current_variant" == "0" ]; then
                	available_variant=$(getAvailableVariant $(getLongHCRCode $current_card) )
                        echo "$current_card[$current_variant] No matches! You can assign variant $available_variant."

                	# Delete the dim files with variant 0
        		rm $dims_path/${current_card}_?0.xml
		fi

        # we found matching variants
	else
		if [ "$current_variant" == "0" ]; then
		 	echo "$current_card[$current_variant] DIMs can be assigned the variants: ${variants[@]}. Please change and recompile."

			# Delete the dim files with variant 0
        		rm $dims_path/${current_card}_?0.xml
		else
			echo "$current_card[$current_variant] DIMs match definitions with the variants: ${variants[@]}"
		fi
        fi
}

function getAvailableVariant(){
	
	old_pwd=$(pwd)
	cd $git_root/doc/converters/scripts
	
	prev_line=""
	declare -i v=0
	
	while IFS= read -r line; do
		# line_with_hidden_characters=$(echo "$line"| cat -v | grep --color=always "[0-9]")
		
		if [[ $line == *Variant*  ]]; then
			if [[ $prev_line == *Variant* && $v > 0 ]]; then
				echo $v
				return
			fi
		
			# Using grep to extract the numbers after the word Variant
			# Using this method because line has hidden characters
			#
			# -o  ==>  option for printing only the matching part of the line
			# -P  ==>  use perl-regexp
			# \K  ==>  do not print that comes before \K (zero-width look-behind assertion)
			# \d  ==>  match digits	
			
			v=$(echo "$line"| grep -oP "Variant\s+\K\d+")


		elif [[ $line == *No\ such\ file\ or\ directory* ]]; then
			echo 1
			return
		fi
		prev_line=$line
	done < <(./check_variant.sh $1 2>/dev/null)
	
	v+=1
	echo $v
	cd $old_pwd
	unset IFS
}

function getShortHCRCode(){
	# build shord hcr code from the DIM filename
	basename $1 | awk -F'_' '{ print $1 }'
}

function getLongHCRCode(){
	current_card=$1
	# build long and CAPITAL HCR code drom the small one
	echo "HC$current_card" | awk '{print toupper($0)}'
}

function getVariant(){
        basename $1 | tr -d -c 0-9
}

function getBusOrder(){
	if [[ $# != 1 ]];
	then
		echo "z"
	else
		# remove digits, then split by '_' and '.' and select the letter in pos 2 (a,b,c,d...) that corresponds to the BUS order
		printf '%s' $(basename $1) | sed 's/[0-9]//g' | awk -F'[_.]' '{print $2}'
	fi
}

function newCard(){
	# if we do not have 2 input args always return true
	if [[ $# != 2 ]]; then
		echo true
		return
	fi

	# We are on a new CARD DIM if the HCRCode is different than the previous
	if [[ $(getShortHCRCode $1) != $(getShortHCRCode $2) ]];
	then
		echo true

	# We might still be on a new card with the same HCRCode
	else
		# We are on a new CARD if the bus order is smaller than the previous
		if [[ $(getBusOrder $1) < $(getBusOrder $2) ]];
		then
			echo true
		else
			echo false
		fi
	fi
}

function intersect(){
	local array1=$1"[@]";
	local array2=$2"[@]";
	comm -12 <(printf '%s\n' "${!array1}" | LC_ALL=C sort) <(printf '%s\n' "${!array2}" | LC_ALL=C sort);
}

function equalDIMS(){
	file_1=$dims_path/$(basename $1)
	# pattern corresponds to the filename with the numbers replaced by '*'
	search_pattern=$( basename $file_1 | sed 's/[0-9]/*/g' )

	# Get the list of dims of same type and digital bank (letter a/b/c...)
        dims_of_same_type=$( ls $dims_path/$search_pattern )

	unset IFS
	for file_2 in ${dims_of_same_type[@]} ; do

		# do not compare if filenames are the same (same file)
		if [ "$file_1" == "$file_2" ]; then
			# echo $( getVariant $file_2 )
			continue
		fi

		# diff files but skip line starting with '<dim_type', which is the line containing the variant number
		diff_res=$(diff -I '^<dim_type' $file_1 $file_2 )

		# if the diff result is empty we have a match and print the variant number
		if [ -z "$diff_res" ]; then
			echo $( getVariant $file_2 )
		fi
	done
}


current_card=""
equal_variants=()
prev_dim_file=""


# iterate the received list of DIM files
for dim_file in $@; do

	# Check if we are on a new CARD DIM by checking if the current bus order is smaller than the previous
	if $(newCard $dim_file $prev_dim_file)
	then
		# Print previous DIM matches
		printVariants "$prev_dim_file" "${equal_variants[@]}"

		# Check if the new DIM matches any of the existing variants
		equal_variants=$(equalDIMS $dim_file)
	else
		# find matching DIMs with the current HCRCode
	        new_matches=$(equalDIMS $dim_file)
		equal_variants=$(intersect "equal_variants" "new_matches")
	fi

	prev_dim_file=$dim_file
done

# Print last batch
printVariants "$prev_dim_file" "${equal_variants[@]}"
