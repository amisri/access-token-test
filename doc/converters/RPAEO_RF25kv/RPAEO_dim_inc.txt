

<!-- RPAEO  -->

<dim name="ST_FLTS"	  type="HCRAIRL003" position="A" variant="13" dim_idx="0" bus_addr="0x06" />
<dim name="ST_WRNS"       type="HCRAIRL003" position="B" variant="13" dim_idx="1" bus_addr="0x07" />
<dim name="ST_STS"        type="HCRAIRL003" position="C" variant="13" dim_idx="2" bus_addr="0x08" />

<dim name="REG_FLTS"      type="HCRAIRO003" position="A" variant="4" dim_idx="3" bus_addr="0x04" />
<dim name="REG_WRNS"      type="HCRAIRO003" position="B" variant="4" dim_idx="4" bus_addr="0x05" />

<dim name="ANAINTK_FLTS1" type="HCRAKZY005" position="A" variant="7" dim_idx="5" bus_addr="0x02" />
<dim name="ANAINTK_FLTS2" type="HCRAKZY005" position="B" variant="7" dim_idx="6" bus_addr="0x03" />

<dim name="DIGINTK_FLTS1" type="HCRAIRM003" position="A" variant="7" dim_idx="8" bus_addr="0x00" />
<dim name="DIGINTK_FLTS2" type="HCRAIRM003" position="B" variant="7" dim_idx="9" bus_addr="0x01" />
