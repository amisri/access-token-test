fakezone:
  doc: |
    The FGC3 has 2 banks of 5 supplemental inputs/outputs.  The direction of each bank can be
    controlled if the type of backplane indicates that the bank can be an output, otherwise it
    is forced to be an input.  When a bank is selected to be an output, each output can be driven
    as a level or with a pulse generator based on a specified value "time till event".
    When used as an input the level can be read and the time of a first transition can be
    recorded.
  children:
  
      # FGC3 DIG SUP memmap definition
  
      # =====================================================================================================
  - zone:
      name: DIR
      offset: 0B
      size: 2B
      type: volatile uint16_t
      consts: near
      comment: SUP I/O direction control register
      doc: |
        This register contains the output permit for the SUP I/O bank and the direction control
        bit (input or output) which will be respected if the output permit is active.
        To activate the "OUT_PERMIT" set the bit; to put the bank in output set the "OUT_CTRL" bit.
      children:
      - zone:
          name: OUT_PERMIT
          offset: 0b
          size: 1b
          consts: mask16
          access: RW
          comment: SUP I/O bank output permit bit
          doc: |
            Set the bit to permit this SUP_IO bank to be driven as outputs. In this case the OUT_CTRL bit can be also set to make the bank an
            output.  When this bit is 0, OUT_CTRL will be forced to zero and the bank will always be an input.
      - zone:
          name: OUT_CTRL
          offset: 8b
          size: 1b
          consts: mask16
          comment: SUP bank output control bit
          doc: |
            If the backplane type allows this SUP I/O bank to be driven (i.e. OUT_PERMIT set = 1)
            then this bit will control the transceiver direction, otherwise it will be forced to
            zero and the bank will always be an input.
            1 for OUTPUT, 0 for INPUT.
  - zone:
      name: PORT
      offset: 2B
      size: 2B
      type: volatile uint16_t
      consts: near
      access: R
      comment: SUP I/O port register
      doc: |
        This register reports the state of the port's inputs (after optional inversion).  This works even
        if the direction is output.
      children:
      - zone:
          name: INP4
          offset: 12b
          size: 1b
          access: R
          comment: SUP I/O channel 4 input status bit
      - zone:
          name: INP3
          offset: 11b
          size: 1b
          access: R
          comment: SUP I/O channel 3 input status bit
      - zone:
          name: INP2
          offset: 10b
          size: 1b
          access: R
          comment: SUP I/O channel 2 input status bit
      - zone:
          name: INP1
          offset: 9b
          size: 1b
          access: R
          comment: SUP I/O channel 1 input status bit
      - zone:
          name: INP0
          offset: 8b
          size: 1b
          access: R
          comment: SUP I/O channel 0 input status bit
  - zone:
      name: DATA
      offset: 4B
      size: 2B
      type: volatile uint16_t
      consts: near
      access: R
      comment: SUP I/O output data register
      doc: |
        This register reports the state of the port's direct output register. This cannot be set directly
        but rather the bits can be set or cleared using the SET and RESET registers.
      children:
      - zone:
          name: OUT4
          offset: 12b
          size: 1b
          access: R
          consts: mask16
          comment: SUP I/O channel 4 output data bit
      - zone:
          name: OUT3
          offset: 11b
          size: 1b
          access: R
          consts: mask16
          comment: SUP I/O channel 3 output data bit
      - zone:
          name: OUT2
          offset: 10b
          size: 1b
          access: R
          consts: mask16
          comment: SUP I/O channel 2 output data bit
      - zone:
          name: OUT1
          offset: 9b
          size: 1b
          access: R
          consts: mask16
          comment: SUP I/O channel 1 output data bit
      - zone:
          name: OUT0
          offset: 8b
          size: 1b
          access: R
          consts: mask16
          comment: SUP I/O channel 0 output data bit
  - zone:
      name: RESET
      offset: 4B
      size: 2B
      type: volatile uint16_t
      consts: near
      access: W
      comment: SUP I/O output data reset register
      doc: |
        Writing a bit to 1 in this register will reset the equivalent bit in the DATA register.
      children:
      - zone:
          name: RESET4
          offset: 4b
          size: 1b
          access: W
          comment: SUP I/O channel 4 output data reset bit
      - zone:
          name: RESET3
          offset: 3b
          size: 1b
          access: W
          comment: SUP I/O channel 3 output data reset bit
      - zone:
          name: RESET2
          offset: 2b
          size: 1b
          access: W
          comment: SUP I/O channel 2 output data reset bit
      - zone:
          name: RESET1
          offset: 1b
          size: 1b
          access: W
          comment: SUP I/O channel 1 output data reset bit
      - zone:
          name: RESET0
          offset: 0b
          size: 1b
          access: W
          comment: SUP I/O channel 0 output data reset bit
  - zone:
      name: SET
      offset: 4B
      size: 2B
      type: volatile uint16_t
      consts: near
      access: W
      comment: SUP I/O output data set register
      doc: |
        Writing a bit to 1 in this byte will set the equivalent bit in the DATA register.
      children:
      - zone:
          name: SET4
          offset: 12b
          size: 1b
          access: W
          comment: SUP I/O channel 4 output data set bit
      - zone:
          name: SET3
          offset: 11b
          size: 1b
          access: W
          comment: SUP I/O channel 3 output data set bit
      - zone:
          name: SET2
          offset: 10b
          size: 1b
          access: W
          comment: SUP I/O channel 2 output data set bit
      - zone:
          name: SET1
          offset: 9b
          size: 1b
          access: W
          comment: SUP I/O channel 1 output data set bit
      - zone:
          name: SET0
          offset: 8b
          size: 1b
          access: W
          comment: SUP I/O channel 0 output data set bit
  - zone:
      name: ARM
      offset: 6B
      size: 2B
      type: volatile uint16_t
      consts: near
      comment: 'SUP A: I/O pulse generator arm register'
      doc: |
        <p><b>This register is only implemented for DIG_SUP_A channels 0-3.</b></p>
        <p>This register contains the SUP I/O pulse generator arming bits. When a bit is set the
        PULSE_ETIM_US and PULSE_WIDTH_US registers for the associated channel are locked.
        Attempts to write these registers when armed will be silently ignored.</p>
        <p>Once armed, the PULSE_ETIM_US register will be compared to the time to event and if
        equal the pulse will start. It will remain active for the
        time given in PULSE_WIDTH_US. When the pulse ends the arm bit will be reset to zero
        automatically, unlocking write access to the registers.</p>
        <p>Setting the arm bit to zero will terminate the pulse if it is in progress and
        will unlock write access to the registers.</p>
      children:
      - zone:
          name: OUT3
          offset: 11b
          size: 1b
          comment: SUP I/O channel 3 pulse generator arm bit
      - zone:
          name: OUT2
          offset: 10b
          size: 1b
          comment: SUP I/O channel 2 pulse generator arm bit
      - zone:
          name: OUT1
          offset: 9b
          size: 1b
          comment: SUP I/O channel 1 pulse generator arm bit
      - zone:
          name: OUT0
          offset: 8b
          size: 1b
          comment: SUP I/O channel 0 pulse generator arm bit
  
      # =================================================================================================
  - zone:
      name: CHAN_CTRL
      offset: 16B
      size: 10B
      type: volatile uint16_t
      consts: array
      comment: SUP I/O channel control and status registers
      doc: |
        These registers control the configuration and report the status of the SUP I/O channels.
      children:
      - zone:
          name: RISING_EDGE
          offset: 0b
          size: 1b
          type: volatile uint32_t
          access: R
          consts: mask16
          comment: First rising edge detected flag
          doc: |
            <p><b>This bit is only implemented for DIG_SUP_B channels 0-3.</b></p>
            This bit will be set if a rising edge (after optional inversion) of more than 1.5us is detected.
            The RISE_ETIM_US register will latch the current time to event at the moment of the rising edge.
            This bit will be reset when the RISE_ETIM_US register is read.
      - zone:
          name: FALLING_EDGE
          offset: 1b
          size: 1b
          type: volatile uint32_t
          access: R
          consts: mask16
          comment: First falling edge detected flag
          doc: |
            <p><b>This bit is only implemented for DIG_SUP_B channels 0-3.</b></p>
            This bit will be set if a falling edge (after optional inversion) of more than 1.5us is detected.
            The FALL_ETIM_US register will latch the current time to event at the moment of the falling edge.
            This bit will be reset when the FALL_ETIM_US register is read.
      - zone:
          name: REPEATED_RISE
          offset: 2b
          size: 1b
          type: volatile uint32_t
          access: R
          consts: mask16
          comment: Repeated rising edge detected flag
          doc: |
            <p><b>This bit is only implemented for DIG_SUP_B channels 0-3.</b></p>
            This bit will be set if more than one rising edge (after optional inversion) of more than 1.5us
            is detected. The RISE_ETIM_US register will latch the time to event of the first rising edge,
            so the timing of repeated rising edges will not be recorded.
            It will be reset when RISE_ETIM_US register is read.
      - zone:
          name: REPEATED_FALL
          offset: 3b
          size: 1b
          type: volatile uint32_t
          access: R
          consts: mask16
          comment: Repeated falling edge detected flag
          doc: |
            <p><b>This bit is only implemented for DIG_SUP_B channels 0-3.</b></p>
            This bit will be set if more than one falling edge (after optional inversion) of more than 1.5us
            is detected. The FALL_ETIM_US register will latch the time to event of the first falling edge,
            so the timing of repeated falling edges will not be recorded.
            It will be reset when FALL_ETIM_US register is read.
      - zone:
          name: INVERT
          offset: 15b
          size: 1b
          type: volatile uint16_t
          consts: mask16
          comment: Invert channel control bit
          doc: |
            Setting this bit will invert the input and output for the channel.
      - zone:
          name: OUTMPX
          offset: 8b
          size: 4b
          type: volatile uint16_t
          consts: mask16
          comment: Output multiplexor control
          doc: |
            These bits define the source of the signal when the channel is being used as
            an output:
            <ul>
            <li>0 : Data register bit value</li>
            <li>1 : Pulse generator (SUP_A 0-3 only)</li>
            <li>2 : Serial link transmission (SUP_A 0 only)</li>
            <li>3 : Pulse at start of cycle (C0&amp;C1) (SUP_A 0-3 only)</li>
            <li>4 : 50 Hz clock (SUP_A 0-3 only)</li>
            <li>5 : 1 kHz clock (SUP_A 0-3 only)</li>
            <li>6 : 1 kHz sync (SUP_A 0-3 only)</li>
            <li>7 : 1 MHz clock (SUP_A 0-3 only)</li>
            <li>8 : 10 kHz clock (SUP_A 0-3 only)</li>
            <li>9-15 : Reserved</li>
            </ul>
            When the channel is used as an input then these bits have no effect.
  - zone:
      name: RISE_ETIM_US
      offset: 32B
      size: 16B
      type: volatile uint32_t
      consts: array
      access: R
      comment: SUP I/O input channel rising edge event time latch registers
      doc: |
        <p><b>This register is only implemented for DIG_SUP_B channels 0-3.</b></p>
        <p>These registers will latch the time to event in microseconds for each channel.
        Only the time of the first rising edge
        of the channel input (after optional inversion) since the register was last read
        will be recorded, provided the pulse width is at least 1.5us.</p>
        <p>Two flags are included in the CHAN_CTRL
        register to help the program interpret the contents of this register. The RISING_EDGE bit
        will be set when the first rising edge is detected and the REPEATED_RISE bit will be set if a
        second edge is detected.  The CHAN_CTRL register should be read before reading this register
        because reading this register will reset both the RISING_EDGE and REPEATED_RISE bits.</p>
  - zone:
      name: FALL_ETIM_US
      offset: 48B
      size: 16B
      type: volatile uint32_t
      consts: array
      access: R
      comment: SUP I/O input channel falling edge event time latch registers
      doc: |
        <p><b>This register is only implemented for DIG_SUP_B channels 0-3.</b></p>
        <p>These registers will latch the time to event in microseconds for each channel.
        Only the time of the first falling edge
        of the channel input (after optional inversion) since the register was last read
        will be recorded, provided the pulse width is at least 1.5us.</p>
        <p>Two flags are included in the CHAN_CTRL
        register to help the program interpret the contents of this register. The FALLING_EDGE bit
        will be set when the first rising edge is detected and the REPEATED_FALL bit will be set if a
        second edge is detected.  The CHAN_CTRL register should be read before reading this register
        because reading this register will reset both the FALLING_EDGE and REPEATED_FALL bits.</p>
  - zone:
      name: PULSE_ETIM_US
      offset: 64B
      size: 16B
      type: volatile uint32_t
      consts: array
      comment: SUP I/O output channel pulse event time registers
      doc: |
        <p><b>This register is only implemented for DIG_SUP_A channels 0-3.</b></p>
        These registers define for each channel the time till the event in microseconds
        when the output pulse should start, when armed. The register
        can only be written when not armed.
  - zone:
      name: PULSE_WIDTH_US
      offset: 80B
      size: 16B
      type: volatile uint32_t
      consts: array
      comment: SUP I/O output channel pulse width registers
      doc: |
        <p><b>This register is only implemented for DIG_SUP_A channels 0-3.</b></p>
        These registers define the output pulse width in microseconds for each channel.
        The register for a channel can only be written when the channel is not armed.
  
      # EOF
# EOF
