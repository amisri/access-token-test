# # FGC Codes memmap definition
## 
##         qak     01/03/2005	Page created
# # PG0 =======================================================================================
zone:
  name: CODES
  offset: '0x20000B'
  size: 384K
  comment: HC16 Code zones
  doc: |
    The HC16 has several resident codes in its memory map.
    The layout of the flash pages in the HC16 memory map is very flexible with 32 memory modes
    (of which 18 are unique).
  children:
  - zone:
      name: NAMEDB
      offset: '0x1A000B'
      size: 8K
      access: R
      consts: 32bit
      comment: NameDB Code in page 3
      doc: |
        The Name Database and is written to the BB5 flash sector.
      children:
      - zone:
          name: INFO
          offset: '0x01FE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: NameDB Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: BOOTPROG23
      offset: '0x00000B'
      size: 128K
      access: R
      consts: 32bit sizewords sizebytes
      comment: Boot program Code in pages 2-3
      doc: |
        The boot program and is written to the BA0-1 and BB0-1 flash sectors.
        	When the boot is running, the BA0-1 or BB0-1 flash sectors are mapped to pages 2-3.
      children:
      - zone:
          name: INFO
          offset: '0x1FFE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: Boot program Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: BOOTPROG45
      offset: '0x20000B'
      size: 128K
      consts: 32bit
      comment: Boot program Code in pages 4-5
      doc: |
        The boot program which is written to the BA0-1 and BB0-1 flash sectors.
      children:
      - zone:
          name: INFO
          offset: '0x1FFE2B'
          size: 30B
          consts: 32bit
          comment: Boot program Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: DSPPROG
      offset: '0x00000B'
      access: R
      size: 128K
      comment: DSP Code (top half of Main Program) in pages 2-3
      doc: |
        The Main HC16 program and the DSP program.
        	When the main program is running, it is possible to map the top half
        	of C1 into pages 2-3.  This contains the DSP program and allows the
        	C32 memory to be prepared with the DSP program, via the dual port window.
      children:
      - zone:
          name: P20PG0
          offset: '0x00000B'
          size: 64K
          access: R
          consts: 32bit sizewords
          comment: C32 DspProg first page
      - zone:
          name: P20PG1
          offset: '0x10000B'
          size: 64K
          access: R
          consts: 32bit sizewords
          comment: C32 DspProg second page
  - zone:
      name: MAINPROG
      offset: '0x20000B'
      size: 256K
      consts: 32bit
      comment: MainProgs Code in pages 4-7
      doc: |
        The Main HC16 program and the DSP program.
        	The Code is only ever stored in the MP flash which is mapped in pages 4-7
        	when the boot is running.  When the main program is running, only the
        	first half of the code is mapped into pages 4-5.
      children:
      - zone:
          name: MCUPROG
          offset: '0x0000B'
          size: 128K
          access: R
          comment: HC16 MainProg
          children:
          - zone:
              name: START
              offset: 1K
              size: 127K
              access: R
              consts: 32bit
              comment: Entry point to main HC16 program
      - zone:
          name: INFO
          offset: '0x3FFE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: Main Program Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: DIMDB
      offset: '0x00000B'
      size: 64K
      comment: DIMDB Code in page 2
      doc: |
        The DIMs database Code is stored in the flash sector BB_2 which can be mapped in page 2
        	when the main program is running.
      children:
      - zone:
          name: DIMDB
          offset: '0x0000B'
          size: 64K
          access: R
          consts: 32bit
          comment: DIMDs Database
          doc: |-
            The DimDB code.
            	
      - zone:
          name: INFO
          offset: '0xFFE2B'
          size: 30B
          access: R
          comment: DIMDB Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: COMPDB
      offset: '0x10000B'
      size: 32K
      comment: CompDB Code in page 3
      doc: |
        The Components database code is stored in the flash sector BB_3 which can be mapped in page 3
        	when the main program is running.
      children:
      - zone:
          name: COMPDB
          offset: '0x0000B'
          size: 32K
          access: R
          consts: 32bit
          comment: Components Database)
          doc: |-
            The CompDB code.
            	
      - zone:
          name: INFO
          offset: '0x07FE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: CompDB Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: UNUSED
      offset: '0x18000B'
      size: 8K
      comment: Unused Code in page 3
      doc: |
        Flash sector BB_4 is available for a future 8KB code which can be mapped in page 3
        	when the main program is running.
      children:
      - zone:
          name: UNUSED
          offset: '0x0000B'
          size: 8K
          access: R
          consts: 32bit
          comment: Unused code
          doc: |-
            This flash sector is unused.
            	
      - zone:
          name: INFO
          offset: '0x01FE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: Unused Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: SYSDB
      offset: '0x1C000B'
      size: 16K
      comment: SysDB Code in page 3
      doc: |
        The Systems database code is stored in the flash sector BB_6 which can be mapped in page 3
        	when the main program is running.
      children:
      - zone:
          name: SYSDB
          offset: '0x0000B'
          size: 16K
          access: R
          consts: 32bit
          comment: Systems Database
          doc: |-
            The SysDB code.
            	
      - zone:
          name: INFO
          offset: '0x03FE2B'
          size: 30B
          access: R
          consts: 32bit
          comment: SysDB Code information
          children:
          - include: shared/memmaps/codeinfo.yaml
          
              # Code information zone
  - zone:
      name: FLASH
      offset: '0x20000B'
      size: 256K
      comment: Flash sectors
      doc: |
        The three flash memories (BA, BB, MP) can be  mapped in pages 4-7
        	(BA is restricted to two pages only 4-5).  The AM29F200T memories
        	have seven sectors that can be individually erased.
      children:
      - zone:
          name: SA0
          offset: '0x00000B'
          size: 64K
          consts: 32bit
          comment: Flash sector 0
      - zone:
          name: SA1
          offset: '0x10000B'
          size: 64K
          consts: 32bit
          comment: Flash sector 1
      - zone:
          name: SA2
          offset: '0x20000B'
          size: 64K
          consts: 32bit
          comment: Flash sector 2
      - zone:
          name: SA3
          offset: '0x30000B'
          size: 32K
          consts: 32bit
          comment: Flash sector 3
      - zone:
          name: SA4
          offset: '0x38000B'
          size: 8K
          consts: 32bit
          comment: Flash sector 4
      - zone:
          name: SA5
          offset: '0x3A000B'
          size: 8K
          consts: 32bit
          comment: Flash sector 5
      - zone:
          name: SA6
          offset: '0x3C000B'
          size: 16K
          consts: 32bit
          comment: Flash sector 6
# EOF
