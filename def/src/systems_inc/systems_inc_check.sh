#! /usr/bin/bash

systems=../systems/*.xml

# Check for number of uses of each dims_inc file in the systems files

for f in *.xml
do
    read -r first_line < $f
    printf '%-20s %3u  %s\n' $f $(grep systems_inc/$f $systems | wc -l) "$first_line"

done

# EOF
