#! /usr/bin/bash

systems=../systems/*.xml

# Check for number of uses of each dims_inc file in the systems files

for f in *.xml
do
    echo -n $f

    grep -n dims_inc/$f $systems | wc -l

done

# EOF
