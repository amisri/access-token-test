properties:
  name: adc.yaml
  children:
  - property:
      name: ADC
      type: PARENT
      title: Analogue to Digital Converter properties
      group: CALIBRATION
      get: Parent
      set: Reset
      doc: |
        This property contains the properties related to the ADC channels. As explained in
        the documentation for property {PROP ADC.FILTER}, on the FGC2, the two following commands are equivalent:
        <PRE>
        S ADC RESET
        S {PROP ADC.FILTER} RESET
        </PRE>
      children:
          # ADC.EXTERNAL properties =======================================================================
      - include:
          name: adc_external
          file: inc/adc/adc_external.yaml

          # ADC.INTERNAL properties =======================================================================
      - include:
          name: adc_internal
          file: inc/adc/adc_internal.yaml

          # ADC.FILTER properties =========================================================================
      - include:
          name: adc_filter
          file: inc/adc/adc_filter.yaml

          # ADC.ANALOG_BUS properties =====================================================================
      - include:
          name: adc_analog_bus
          file: inc/adc/adc_analog_bus.yaml

          # ADC debug properties ==========================================================================
      - property:
          name: RAW_200MS
          title: Raw 200ms measurements
          type: INT32S
          get: Integer
          numels: FGC_N_ADCS
          doc: |
            This property contains the 200ms average of the raw ADC filter output.
          51:
            value: dpcls.dsp.adc.raw_200ms
          53:
            value: dpcls.dsp.adc.raw_200ms
          62:
            value: dpcom.dsp.adc.raw_200ms
          63:
            value: dpcom.dsp.adc.raw_200ms
          64:
            value: dpcom.dsp.adc.raw_200ms
          123:
            value: equipdev.device[0].sig_struct.adc.array[0].average1.raw.meas
            step: sizeof(struct SIG_adc)
          92:
            value: equipdev.device[0].sig_struct.adc.array[0].average1.raw.meas
            step: sizeof(struct SIG_adc)
          94:
            value: equipdev.device[0].sig_struct.adc.array[0].average1.raw.meas
            step: sizeof(struct SIG_adc)
      - property:
          name: RAW_1S
          title: Raw 1s measurements
          type: INT32S
          get: Integer
          numels: FGC_N_ADCS
          doc: |
            This property contains the 1s average of the raw ADC filter output,
            aligned on UTC second boundaries.
          51:
            value: dpcls.dsp.adc.raw_1s
          53:
            value: dpcls.dsp.adc.raw_1s
          62:
            value: dpcom.dsp.adc.raw_1s
          63:
            value: dpcom.dsp.adc.raw_1s
          64:
            value: dpcom.dsp.adc.raw_1s
          123:
            value: equipdev.device[0].sig_struct.adc.array[0].average2.raw.meas
            step: sizeof(struct SIG_adc)
          92:
            value: equipdev.device[0].sig_struct.adc.array[0].average2.raw.meas
            step: sizeof(struct SIG_adc)
          94:
            value: equipdev.device[0].sig_struct.adc.array[0].average2.raw.meas
            step: sizeof(struct SIG_adc)
      - property:
          name: VOLTS_200MS
          title: 200ms measurements in volts
          type: FLOAT
          get: Float
          numels: FGC_N_ADCS
          doc: |
            This property contains the 200ms averages of the ADC,
            calibrated in volts.
          51:
            value: dpcls.dsp.adc.volts_200ms
          53:
            value: dpcls.dsp.adc.volts_200ms
          62:
            value: dpcom.dsp.adc.volts_200ms
          63:
            value: dpcom.dsp.adc.volts_200ms
          64:
            value: dpcom.dsp.adc.volts_200ms
          123:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average1.mean
            step: sizeof(struct SIG_adc)
          92:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average1.mean
            step: sizeof(struct SIG_adc)
          94:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average1.mean
            step: sizeof(struct SIG_adc)
      - property:
          name: VOLTS_1S
          title: 1s measurements in volts
          type: FLOAT
          get: Float
          numels: FGC_N_ADCS
          doc: |
            This property contains the 1s averages of the ADCs,
            aligned on UTC second boundaries, calibrated in volts.
          51:
            value: dpcls.dsp.adc.volts_1s
          53:
            value: dpcls.dsp.adc.volts_1s
          62:
            value: dpcom.dsp.adc.volts_1s
          63:
            value: dpcom.dsp.adc.volts_1s
          64:
            value: dpcom.dsp.adc.volts_1s
          123:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.mean
            step: sizeof(struct SIG_adc)
          92:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.mean
            step: sizeof(struct SIG_adc)
          94:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.mean
            step: sizeof(struct SIG_adc)
      - property:
          name: PP_VOLTS_1S
          title: Raw Peak-Peak 1s measurements
          type: FLOAT
          get: Float
          numels: FGC_N_ADCS
          doc: |
            This property contains the peak-peak voltage during 1s
            for the unfiltered ADC voltages, aligned on UTC second boundaries.
          51:
            value: dpcls.dsp.adc.pp_volts_1s
          53:
            value: dpcls.dsp.adc.pp_volts_1s
          62:
            value: dpcom.dsp.adc.pp_volts_1s
          63:
            value: dpcom.dsp.adc.pp_volts_1s
          64:
            value: dpcom.dsp.adc.pp_volts_1s
          123:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.peak_to_peak
            step: sizeof(struct SIG_adc)
          92:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.peak_to_peak
            step: sizeof(struct SIG_adc)
          94:
            value: equipdev.device[0].sig_struct.adc.array[0].meas.average2.peak_to_peak
            step: sizeof(struct SIG_adc)
      - property:
          name: SIGNALS_200MS
          title: Average mean (200 ms) of the ADC measurements
          type: FLOAT
          get: Float
          numels: FGC_N_ADCS
          doc: |
            This property contains the 200 ms average mean of the transducer measurements
            connected to the four internal ADCs and the two external ADCs if present.
          63:
            value: dpcom.dsp.adc.cal_meas_200ms
      - property:
          name: SIGNALS_1S
          title: Average mean (1 s) of the ADC measurements
          type: FLOAT
          get: Float
          numels: FGC_N_ADCS
          doc: |
            This property contains the 1 s average mean of the transducer measurements
            connected to the four internal ADCs and the two external ADCs if present.
          63:
            value: dpcom.dsp.adc.cal_meas_1s
      - property:
          name: AMPS_200MS
          title: 200ms measurements in amps
          type: FLOAT
          get: Float
          numels: 2
          doc: |
            This property contains the 200ms averages of the ADC inputs IA and IB,
            calibrated in amps.
          51:
            value: dpcls.dsp.adc.amps_200ms
          53:
            value: dpcls.dsp.adc.amps_200ms
          62:
            value: dpcom.dsp.adc.amps_200ms
          63:
            value: dpcom.dsp.adc.amps_200ms
          64:
            value: dpcom.dsp.adc.amps_200ms
          123:
            value: equipdev.device[0].sig_struct.transducer.array[0].meas.average2.mean
            step: sizeof(struct SIG_transducer)
          92:
            value: equipdev.device[0].sig_struct.transducer.array[0].meas.average2.mean
            step: sizeof(struct SIG_transducer)
      - property:
          name: FIFO
          title: ADC fifo buffer
          type: FIFOADC
          flags: HIDE
          get: Fifo
          set: Fifo
          numels: FGC_ADC_FIFO_LEN
          doc: |
            This property contains the ADC fifo log (128 samples in length).
            This log will accumulate the
            raw 200ms average ADC values for both channels, and the raw peak-peak values
            for the same periods.  Each entry in the log starts with the timestamp and
            is followed by the ADC values for channels A and B, and then the peak-peak values for
            channels A and B.  Setting the property will flush the log.  If the log fills up,
            old samples will be overwritten and the readout index will not be changed.  This
            means that the first reading after the fifo has filled up will return an unknown
            number of samples.
          51:
            value: dpcls.dsp.fifo.adc
          53:
            value: dpcls.dsp.fifo.adc
