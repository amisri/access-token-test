properties:
  name: v_probe.yaml
  children:
  - property:
      name: V_PROBE
      type: PARENT
      get: Parent
      title: Voltage probe properties
      group: CONFIG
      doc: |
        Properties related to the voltage probes.
      children:
      - property:
          name: CAPA
          type: PARENT
          get: Parent
          title: Capacitor voltage properties
          doc: |
            Properties associated with the capacitor voltage probe.
          children:
          - property:
              name: GAIN
              title: Capacitor voltage measurement nominal gain (v_capa/v_adc)
              type: FLOAT
              flags: NON_VOLATILE CONFIG
              get: Float
              set: Float
              maxels: 1
              from_spare_converter: 1
              62:
                dsp: fgc
                value: sig_struct.transducer.named.v_capa.nominal_gain
              63:
                dsp: fgc
                value: sig_struct.transducer.named.v_capa.nominal_gain
              doc: |
                This property contains the nominal gain for the capacitor voltage measurement.
                The value should be capacitor voltage divided by the ADC voltage.
          - property:
              name: POSITION
              title: Capacitor volage probe position
              type: INT32U
              flags: NON_VOLATILE CONFIG
              get: Integer
              set: Integer
              setif: RegStateNone
              symlist: TRANSDUCER_POSITION
              maxels: 1
              from_spare_converter: 1
              62:
                dsp: fgc
                value: sig_struct.transducer.named.v_capa.position
              63:
                dsp: fgc
                value: sig_struct.transducer.named.v_capa.position
              doc: |
                This property contains the position or orientation of the voltage probe.
                If a polarity switch is present, it can identify whether the probe is located
                on the converter side or on the load side of the switch. If no switch
                is present, it can define the orientation of the probe.
                The sign of the measured value returned by the probe will depend upon this and
                the state of the polarity switch, if present.
      - property:
          name: LOAD
          type: PARENT
          get: Parent
          title: Load voltage properties
          doc: |
            Properties associated with the load voltage probe.
          children:
          - property:
              name: GAIN
              title: Load voltage measurement nominal gain (v_load/v_adc)
              type: FLOAT
              flags: NON_VOLATILE CONFIG
              get: Float
              set: Float
              maxels: 1
              from_spare_converter: 1
              62:
                dsp: fgc
                value: sig_struct.transducer.named.v_meas.nominal_gain
              63:
                dsp: fgc
                value: sig_struct.transducer.named.v_meas.nominal_gain
              123:
                flags: NON_VOLATILE
                value: equipdev.device[0].sig_struct.transducer.named.v_meas.nominal_gain
# 2023-05-30: CONFIG flag removed for FGClite until YETS 2023/24
              92:
                flags: NON_VOLATILE
                value: equipdev.device[0].sig_struct.transducer.named.v_meas.nominal_gain
              94:
                value: equipdev.device[0].sig_struct.transducer.named.v_meas.nominal_gain
              doc: |
                This property contains the nominal gain for the load voltage measurement.
                The value should be the load voltage divided by the ADC voltage.
          - property:
              name: POSITION
              title: Volage probe position
              type: INT32U
              flags: NON_VOLATILE CONFIG
              get: Integer
              set: Integer
              setif: RegStateNone
              symlist: TRANSDUCER_POSITION
              maxels: 1
              from_spare_converter: 1
              62:
                dsp: fgc
                value: sig_struct.transducer.named.v_meas.position
              63:
                dsp: fgc
                value: sig_struct.transducer.named.v_meas.position
              doc: |
                This property contains the position or orientation of the voltage probe.
                If a polarity switch is present, it can identify whether the probe is located
                on the converter side or on the load side of the switch. If no switch
                is present, it can define the orientation of the probe.
                The sign of the measured value returned by the probe will depend upon this and
                the state of the polarity switch, if present.
# EOF
