# # REF.CYC =========================================================================================
properties:
  name: ref_cyc.yaml
  children:
  - property:
      name: CYC
      type: PARENT
      flags: HIDE
      title: Cycling checks
      get: Parent
      doc: |
        This property contains properties that provide diagnostic information about
        failures of cycling checks.
      children:
          # Cycle Status from Libref v2
      - property:
          name: COUNTER
          title: Counter
          type: INT32U
          get: Integer
          numels: 1
          63:
            dsp: fgc
            flags: PPM GET_ZERO
            value: ref_mgr.REF_VAR_CYCSTATUS_COUNTER
          94:
            flags: PPM GET_ZERO
            value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_COUNTER
          doc: |
            This property contains the Count of times this function was played of this cycle.
      - property:
          name: STATUS
          title: Cycle status
          operational: 1
          type: INT32U
          get: Integer
          class_symlist: CYC_STATUS
          numels: 1
          51:
            dsp: fgc
            value: property.log.cyc.status
          53:
            dsp: fgc
            flags: PPM
            value: property.log.cyc.status
          62:
            flags: PPM BIT_MASK
            value: pulse_log.status
          63:
            dsp: fgc
            flags: PPM BIT_MASK
            value: ref_mgr.REF_VAR_CYCSTATUS_STATUS
          94:
            flags: PPM BIT_MASK
            value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_STATUS
          doc: |
            This property contains the status for the last play of this cycle.
      - property:
          name: MAX_ABS_ERR
          title: Maximum absolute error
          operational: 1
          type: FLOAT
          get: Float
          numels: 1
          63:
            dsp: fgc
            flags: PPM
            value: ref_mgr.REF_VAR_CYCSTATUS_MAX_ABS_REG_ERR
          94:
            flags: PPM
            value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_MAX_ABS_REG_ERR
          51:
            dsp: fgc
            value: property.log.cyc.max_abs_err
          53:
            dsp: fgc
            value: property.log.cyc.max_abs_err
          doc: |
            This property contains the maximum absolute regulation error during function
            for the last play of this cycle.
      - property:
          name: REG_MODE
          title: Regulation mode
          type: INT32U
          get: Integer
          class_symlist: REG
          numels: 1
          63:
            dsp: fgc
            flags: PPM
            value: ref_mgr.REF_VAR_CYCSTATUS_REG_MODE
          94:
            flags: PPM
            value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_REG_MODE
          doc: |
            This property contains the regulation mode for the last play of this cycle.
      - property:
          name: EVENT_TIME
          title: Event time
          type: ABSTIME
          get: AbsTime
          numels: 1
          63:
            dsp: fgc
            flags: PPM
            value: ref_mgr.REF_VAR_CYCSTATUS_EVENT_TIME
          94:
            flags: PPM
            value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_EVENT_TIME
          doc: |
            This property contains the event time for thel ast play of this cycle.
      - property:
          name: PRE_FUNC_ERR
          title: Pre-function plateau arrival time error (s)
          type: FLOAT
          get: Float
          numels: 1
          63:
            dsp: fgc
            flags: PPM
            value: ref_mgr.REF_VAR_CYCSTATUS_PRE_FUNC_PLATEAU_ERR
          doc: |
            <p>This property contains the pre-function plateau arrival time error in seconds
            for the last play of this cycle. If the reference arrives at the plateau level early,
            the value will be negative. If it arrives late it will be positive.</p>
            <p>If there wasn't time to finish the pre-function,
            PRE_FUNC_WRN will be set in {PROP REF.CYC.STATUS} and this property will be zero.</p>
          # Iterative Learning Controller properties (Libref v2 only)
      - property:
          name: ILC
          type: PARENT
          flags: HIDE
          title: Iterative learning control data
          get: Parent
          doc: |
            This property contains properties that provide diagnostic information about
            operation of the iterative learning controller.
          children:
          - property:
              name: RMS_ERR
              title: ILC RMS error
              type: FLOAT
              get: Float
              numels: 1
              63:
                dsp: fgc
                flags: PPM
                value: ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_ERR
              94:
                flags: PPM
                value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_ERR
              doc: |
                This property contains the RMS ILC error for the given cycle.
          - property:
              name: INITIAL_RMS_ERR
              title: ILC RMS error for the first cycle after starting
              type: FLOAT
              get: Float
              numels: 1
              63:
                dsp: fgc
                flags: PPM
                value: ref_mgr.REF_VAR_CYCSTATUS_ILC_INITIAL_RMS_ERR
              94:
                flags: PPM
                value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_ILC_INITIAL_RMS_ERR
              doc: |
                This property contains the RMS ILC error for the given user for the first cycle after
                the ILC was enabled. This level is used to check if the ILC is diverging. If the
                RMS ILC error exceeds this value then RMS_ERR_FAIL will be set in {PROP REF.ILC.STATUS} and
                {PROP REF.ILC.STATE} will transition to FAILED and {PROP REF.ILC.FAILED_USER}
                will report the user concerned.
          - property:
              name: RMS_V_RATE
              title: RMS V_REF rate
              type: FLOAT
              get: Float
              numels: 1
              63:
                dsp: fgc
                flags: PPM
                value: ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE
              94:
                flags: PPM
                value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE
              doc: |
                This property contains the RMS V_REF rate for the part of the cycle where the ILC is being calculated.
                {PROP REF.CYC.ILC.RMS_V_RATE_LIM} will be set to three times the minimum value of this property seen
                since the ILC was last enabled. If the value exceeds the limit then V_RATE_FAIL will be set in {PROP REF.ILC.STATUS} and
                {PROP REF.ILC.STATE} will transition to FAILED and {PROP REF.ILC.FAILED_USER}
                will report the user concerned.
          - property:
              name: RMS_V_RATE_LIM
              title: RMS V_REF rate limit
              type: FLOAT
              get: Float
              numels: 1
              63:
                dsp: fgc
                flags: PPM
                value: ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE_LIM
              94:
                flags: PPM
                value: equipdev.device[0].ref_mgr.REF_VAR_CYCSTATUS_ILC_RMS_V_REF_RATE_LIM
              doc: |
                This property contains the limit for {PROP REF.CYC.ILC.RMS_V_RATE}. It is set to
                three times the minimum value of {PROP REF.CYC.ILC.RMS_V_RATE} seen since the ILC was last enabled.
                It is used as the upper limit for {PROP REF.CYC.ILC.RMS_V_RATE}
                and if the limit is exceeded then V_RATE_FAIL will be set in {PROP REF.ILC.STATUS} and
                {PROP REF.ILC.STATE} will transition to FAILED and {PROP REF.ILC.FAILED_USER}
                will report the user concerned.
          # Cycle check warnings from Class 53 (POPS)
      - property:
          name: WARNING
          type: PARENT
          flags: HIDE
          title: Failed cycling check data
          get: Parent
          doc: |
            This contains PPM properties that provide diagnostic information about
            failures of cycling checks.
          children:
          - property:
              name: CHK
              title: Failed cycling check
              operational: 1
              type: INT32U
              flags: PPM
              get: Integer
              symlist: CYC_WRN
              numels: 1
              51:
                dsp: fgc
                value: ppm[0].cycling.warning.chk
              53:
                dsp: fgc
                value: ppm[0].cycling.warning.chk
              doc: |
                This property contains the name of the most recent
                failed cycling check for each user.
          - property:
              name: DATA
              title: Failed cycling check data
              operational: 1
              flags: PPM
              type: FLOAT
              get: Float
              numels: FGC_N_CYC_CHK_DATA
              51:
                dsp: fgc
                value: ppm[0].cycling.warning.data
              53:
                dsp: fgc
                value: ppm[0].cycling.warning.data
              doc: |
                This property contains data to help understand why the
                most recent cycling check failed.
          # Cycle check faults from Class 53 (POPS)
      - property:
          name: FAULT
          type: PARENT
          title: Fault that caused cycling to stop
          get: Parent
          doc: |
            This property contains properties that report information about the last failed
            cycling check that caused {PROP STATE.PC} to return from CYCLING to ON_STANDBY.
          children:
          - property:
              name: USER
              title: User when last cycling check failed
              operational: 1
              type: INT32U
              get: Integer
              numels: 1
              51:
                value: dpcls.dsp.cyc.fault.user
              53:
                value: dpcls.dsp.cyc.fault.user
              doc: |
                This property contains the user index that was active the last time a PPM check failed
                that stopped cycling. {PROP REF.CYC.FAULT.CHK} will indicate which check failed.
          - property:
              name: CHK
              title: Last failed PPM check
              operational: 1
              type: INT32U
              get: Integer
              numels: 1
              symlist: CYC_FLT
              51:
                value: dpcls.dsp.cyc.fault.chk
              53:
                value: dpcls.dsp.cyc.fault.chk
              doc: |
                This property contains the last failed PPM check that caused {PROP STATE.PC} to
                switch from CYCLING to ON_STANDBY.  Some checks do not cause cycling to stop
                but the check status is still visible in the PPM property called
                {PROP REF.CYC.WARNING.CHK}.  Additional data about the
                check that failed is in the PPM property {PROP REF.CYC.FAULT.DATA}.
          - property:
              name: DATA
              title: Failed cycling check data
              operational: 1
              type: FLOAT
              get: Float
              numels: FGC_N_CYC_CHK_DATA
              51:
                dsp: fgc
                value: property.ref.cyc.fault.data
              53:
                dsp: fgc
                value: property.ref.cyc.fault.data
              doc: |
                This property contains data to help understand why cycling failed.
# EOF
