# REF.START =======================================================================================
properties:
  name: ref_start.yaml
  children:
  - property:
      name: START
      type: PARENT
      flags: HIDE
      get: Parent
      title: POPS open-loop voltage reference start-up properties
      doc: |
        These PPM properties concern the open-loop start of every field regulation cycle in POPS.
        Every field regulation cycle begins with the application of {PROP REF.START.VREF} volts at
        {PROP REF.START.TIME} for a duration of {PROP REF.START.DURATION}.  At the end of this
        period the software will attempt to close the regulation loop and
        ramp to the injection plateau defined by {PROP REF.FIRST_PLATEAU.REF},
        {PROP REF.FIRST_PLATEAU.TIME} and {PROP REF.FIRST_PLATEAU.DURATION}.
      children:
      - property:
          name: VREF
          title: Open-loop start-up voltage
          operational: 1
          type: FLOAT
          flags: PPM
          get: Float
          set: Float
          numels: 1
          limits: float
          min: '0.0'
          max: '1.0E+4'
          doc: |
            This property defines the open-loop voltage reference to apply at time {PROP REF.START.TIME}.
          53:
            dsp: fgc
            value: ppm[0].cycling.ref_start.vref
      - property:
          name: TIME
          title: Open-loop start-up time (s)
          operational: 1
          flags: PPM
          type: FLOAT
          limits: float
          min: '0.001'
          max: '0.120'
          get: Float
          set: Float
          numels: 1
          doc: |
            This property contains time in the cycle when the open-loop voltage reference defined in
            {PROP REF.START.VREF} should be applied.
          53:
            dsp: fgc
            value: ppm[0].cycling.ref_start.time
      - property:
          name: DURATION
          title: Open-loop start-up duration (s)
          operational: 1
          flags: PPM
          type: FLOAT
          limits: float
          min: '0.010'
          max: '0.100'
          get: Float
          set: Float
          numels: 1
          doc: |
            This property contains the duration during which the open-loop voltage {PROP REF.START.VREF}
            will be applied.  At the end of this time the software will attempt to
            start regulating the current or the field (according to the regulation mode
            {PROP REF.FUNC.REG_MODE}).
          53:
            dsp: fgc
            value: ppm[0].cycling.ref_start.duration
# EOF
