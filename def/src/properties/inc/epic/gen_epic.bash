#!/bin/bash

INPUT="device_in.yaml"

cd `dirname $0`

SCRIPT=`basename $0`

for DEVICE in 1 2 3 4 5 6 7 8
do
    OUTPUT="device_$DEVICE.yaml"
    let INTERNAL_INDEX=$DEVICE-1

    echo "# Generated from $INPUT using $SCRIPT - DO NOT EDIT!" > $OUTPUT
    sed 's/DEVICE_n/DEVICE_'$DEVICE'/g; s/DEV_INDEX/'$INTERNAL_INDEX'/' $INPUT >> $OUTPUT
done

# EOF
