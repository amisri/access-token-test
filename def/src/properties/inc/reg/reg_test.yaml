# # ============================= REF.TEST Properties =======================================
properties:
  name: reg_test.yaml
  children:
  - property:
      name: TEST
      type: PARENT
      title: Regulator and regulation mode test properties
      flags: HIDE
      get: Parent
      doc: |
        <p>These properties allow a <b>Test Cycle</b> to be defined. The user of the <b>Test Cycle</b> is set
        in {PROP REG.TEST.USER}. This is provided by the accelerator operators as one cycle per supercycle that
        has no beam during a period of a few hours. During each <b>Test Cycle</b>, the function that will be
        played is identified as the <b>Test Ref User</b>, which must be set in {PROP REG.TEST.REF_USER}.</p>
        <p><i>IMPORTANT NOTE: There is a restriction with the <b>Test Cycle</b>. It is not possible to change
        the regulation period while running the converter so this means that the value of the REG.B/I.PERIOD_ITERS property
        for {PROP LOAD.TEST_SELECT} is not used. The regulation period will continue to be the value for {PROP LOAD.SELECT},
        so it is only possible to test a new regulator with the same period as the operational regulator.
        All the other properties that influence the preparation of the RST will be based on {PROP LOAD.TEST_SELECT}.</i></p>
        <p>This feature makes it possible to try the Test RST regulator and/or play a function armed with
        the <b>Test Regulation Mode</b>. Here are three use cases in more detail:</p>

        <p><b>1. Run one cycle in the super-cycle with a different regulation mode</b>
        - <em>This is only possible if {PROP DEVICE.PPM} is ENABLED.</em></p>

        <p>At the start of the sequence we assume that {PROP REG.TEST.USER} is 0 (i.e. the Test Cycle is deactivated),
        the power converter state is already CYCLING and we do not have the right to stop cycling.</p>

        <p><table class="embedded">
        <tr><th>Step</th><th width="25%">Command</th><th>Notes</th></tr>
        <tr><td class="alt2">1</td>
        <td>Set {PROP LOAD.TEST_SELECT} to equal {PROP LOAD.SELECT}</td>
        <td>This will deactivate testing the RST regulator.</td>
        </tr>
        <tr><td class="alt2">2</td>
        <td>Set {PROP REG.TEST.REF_USER} to 0</td>
        <td><p>This will prepare to use the non-PPM slot (user 0) as the Test Ref User.
        It is possible to use any user that isn't being used operationally,
        but user 0 is the safest choice.</p>
        <p><em>Note: this is only true if {PROP DEVICE.PPM} is ENABLED.
        If the {PROP DEVICE.PPM} is DISABLED, then it is only possible to arm functions for user 0,
        which is needed operationally, so testing a different regulation mode is not possible.</em></p></td>
        </tr>
        <tr><td class="alt2">3</td>
        <td>Set {PROP REG.TEST.REG_MODE} to the desired test regulation mode</td>
        <td>This will define the regulation mode that we want to arm for Test Cycles.</td>
        </tr>
        <tr><td class="alt2">4</td>
        <td>Set the REF function properties for user 0</td>
        <td>This will prepare to arm the required user 0 function with the Test Regulation Mode,
        once {PROP REG.TEST.USER} has been set.</td>
        </tr>
        <tr><td class="alt2">5</td>
        <td>Set {PROP REG.TEST.USER} to the user of the Test Cycle</td>
        <td>This will activate the Test Cycle and will play the function armed in user 0
        (this was set in {PROP REG.TEST.REF_USER} in step 2). Initially, there may be no function
        armed, which means the state will remain in TO_CYCLING during Test Cycles. This is not a problem.</td>
        </tr>
        <tr><td class="alt2">6</td>
        <td>Set {PROP REF.FUNC.TYPE}(0) to the desired function type</td>
        <td>This will arm the desired Test Ref User function with the
        Test Regulation Mode set in {PROP REG.TEST.REG_MODE} in step 3.</td>
        </tr>
        <tr><td class="alt2">7</td>
        <td>Repeat steps 4 and 6 as required</td>
        <td>The Test Ref User function can be re-armed as often as needed until the tests are finished.</td>
        </tr>
        <tr><td class="alt2">8</td>
        <td>Set {PROP REG.TEST.USER} to 0</td>
        <td>This will deactivate the Test Cycle</td>
        </tr>
        </table></p>

        <p><b>2. Test a new externally synthesised RST controller</b></p>

        <p>At the start of the sequence we assume that {PROP REG.TEST.USER} is 0 (i.e. the Test Cycle is deactivated),
        the power converter state is already CYCLING and we do not have the right to stop cycling.</p>
        <p><i>Remember that the period of the test regulator will be the same as the operational regulator.</i></p>

        <p><table class="embedded">
        <tr><th>Step</th><th width="25%">Command</th><th>Notes</th></tr>
        <tr><td class="alt2">1</td>
        <td>Set {PROP LOAD.TEST_SELECT} to <b>not</b> equal {PROP LOAD.SELECT}</td>
        <td>This will prepare to test an RST regulator.</td>
        </tr>
        <tr><td class="alt2">2</td>
        <td>Set {PROP REG.TEST.REF_USER} as required.</td>
        <td>This will define the user of the reference function to play when we activate the Test Cycle.
        For the purposes of this example, I assume it is already armed and we do not need to re-arm it.</td>
        </tr>
        <tr><td class="alt2">3</td>
        <td>Set {PROP REG.B.EXTERNAL_ALG}[{PROP LOAD.TEST_SELECT}] or
        {PROP REG.I.EXTERNAL_ALG}[{PROP LOAD.TEST_SELECT}] to ENABLED</td>
        <td>This will prepare to test the external RST coefficients when we activate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">4</td>
        <td>Set {PROP REG.B.EXTERNAL.TEST}.R/S/T or {PROP REG.I.EXTERNAL.TEST}.R/S/T to the test RST coefficients</td>
        <td>This will prepare the external RST coefficients to test when we activate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">5</td>
        <td>Set {PROP REG.TEST.USER} to the user of the Test Cycle</td>
        <td>This will activate the Test Cycle and will play the function armed in user {PROP REG.TEST.REF_USER} (set in step 2).</td>
        </tr>
        <tr><td class="alt2">6</td>
        <td>Repeat step 4 as required</td>
        <td>The external RST coefficients can be updated as often as needed until the tests are finished.
        It is a good idea to avoid setting them while the Test Cycle is playing, otherwise there may be a glitch in the regulation.</td>
        </tr>
        <tr><td class="alt2">7</td>
        <td>Set {PROP REG.TEST.USER} to 0</td>
        <td>This will deactivate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">8</td>
        <td>Set {PROP LOAD.TEST_SELECT} to equal {PROP LOAD.SELECT}</td>
        <td>This will deactivate testing the RST regulator in case the Test Cycle is activated in future.</td>
        </tr>
        </table></p>

        <p><b>3. Test a new internally synthesised RST controller</b></p>

        <p>At the start of the sequence we assume that {PROP REG.TEST.USER} is 0 (i.e. the Test Cycle is deactivated),
        the power converter state is already CYCLING and we do not have the right to stop cycling.</p>
        <p><i>Remember that the period of the test regulator will be the same as the operational regulator.</i></p>

        <p><table class="embedded">
        <tr><th>Step</th><th width="25%">Command</th><th>Notes</th></tr>
        <tr><td class="alt2">1</td>
        <td>Set {PROP LOAD.TEST_SELECT} to <b>not</b> equal {PROP LOAD.SELECT}</td>
        <td>This will prepare to test an RST regulator.</td>
        </tr>
        <tr><td class="alt2">2</td>
        <td>Set {PROP REG.TEST.REF_USER} as required.</td>
        <td>This will define the user of the reference function to play when we activate the Test Cycle.
        For the purposes of this example, I assume it is already armed and we do not need to re-arm it.</td>
        </tr>
        <tr><td class="alt2">3</td>
        <td>Set {PROP REG.B.EXTERNAL_ALG}[{PROP LOAD.TEST_SELECT}] or
        {PROP REG.I.EXTERNAL_ALG}[{PROP LOAD.TEST_SELECT}] to DISABLED</td>
        <td>This will prepare to test the internally synthesised RST coefficients when we activate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">4</td>
        <td>Set the required properties in {PROP REG.B.INTERNAL}[{PROP LOAD.TEST_SELECT}] or
        {PROP REG.I.INTERNAL}[{PROP LOAD.TEST_SELECT}] and/or {PROP LOAD}[{PROP LOAD.TEST_SELECT}]</td>
        <td>This will prepare the internally synthesised RST coefficients to test when we activate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">5</td>
        <td>Set {PROP REG.TEST.USER} to the user of the Test Cycle</td>
        <td>This will activate the Test Cycle and will play the function armed in user {PROP REG.TEST.REF_USER} (set in step 2).</td>
        </tr>
        <tr><td class="alt2">6</td>
        <td>Repeat step 4 as required</td>
        <td>The internal RST coefficients can be updated as often as needed until the tests are finished.</td>
        </tr>
        <tr><td class="alt2">7</td>
        <td>Set {PROP REG.TEST.USER} to 0</td>
        <td>This will deactivate the Test Cycle.</td>
        </tr>
        <tr><td class="alt2">8</td>
        <td>Set {PROP LOAD.TEST_SELECT} to equal {PROP LOAD.SELECT}</td>
        <td>This will deactivate testing the RST regulator in case the Test Cycle is activated in future.</td>
        </tr>
        </table></p>
      children:
      - property:
          name: USER
          title: User for Test Cycles
          type: INT32U
          get: Integer
          set: Integer
          numels: 1
          limits: int
          min: 0
          max: FGC_MAX_USER
          63:
            dsp: fgc
            value: refMgrParValue(&ref_mgr, MODE_TEST_CYC_SEL)
          94:
            value: refMgrParValue(&equipdev.device[0].ref_mgr, MODE_TEST_CYC_SEL)
          doc: |
            <p>This property defines the Test Cycle for CYCLING states (TO_CYCLING, CYCLING, PAUSE and ECONOMY).
            Setting the value to zero deactivates the Test Cycle. When the Test Cycle is active and the specified user
            plays, it is possible to:</p>
            <ol>
            <li>Use the Test RST coefficients to help optimise the current or field regulator.</li>
            <li>Play a function with the Test Regulation Mode.</li>
            </ol>
            <p>It is possible to do both at the same time, but this is very unlikely to be needed.
            To use the first case, {PROP LOAD.TEST_SELECT} must be different to {PROP LOAD.SELECT}.
            For the second, it is necessary to set the desired Test Regulation Mode in {PROP REG.TEST.REG_MODE}
            and to then arm the function in slot {PROP REG.TEST.REF_USER} after activating the Test Cycle
            by seting this property to a non-zero value. See the use case sequences for {PROP REG.TEST} above for more details.</p>
      - property:
          name: REF_USER
          title: Test Reference User to play during Test Cycles
          type: INT32U
          get: Integer
          set: Integer
          numels: 1
          limits: int
          min: 0
          max: FGC_MAX_USER
          63:
            dsp: fgc
            value: refMgrParValue(&ref_mgr, MODE_TEST_REF_CYC_SEL)
          94:
            value: refMgrParValue(&equipdev.device[0].ref_mgr, MODE_TEST_REF_CYC_SEL)
          doc: |
            <p>This property defines the user of the function that will be played during Test Cycles, which
            are activated by setting {PROP REG.TEST.USER} to a non-zero value. Note that if this property is
            0, the FGC will play the function armed in slot 0. If no function is armed, then the {PROP STATE.PC} will
            remain in TO_CYCLING during the Test Cycles.</p>
      - property:
          name: REG_MODE
          title: Regulation mode when arming a function for the Test Cycle
          type: INT32U
          get: Integer
          set: Integer
          numels: 1
          class_symlist: REG
          63:
            dsp: fgc
            value: refMgrParValue(&ref_mgr, MODE_TEST_REG_MODE)
          94:
            value: refMgrParValue(&equipdev.device[0].ref_mgr, MODE_TEST_REG_MODE)
          doc: |
            <p>This property defines the Test Regulation Mode. This property allows the Test Cycles to play a function armed
            with a regulation mode that is different to {PROP REG.MODE_CYC}.
            The Test Regulation Mode will be used only when the Test Cycle is active and
            when arming the function in the slot identified by {PROP REG.TEST.REF_USER}.</p>
# EOF
