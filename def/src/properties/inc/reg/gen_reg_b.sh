# reg_b.yaml can be derived from reg_i.yaml by substitution

# name: I\s*$                                      ->      name: B
# I_ERR                                            ->      B_ERR
# I_REF                                            ->      B_REF
# reg.i                                            ->      reg.b
# REG.I                                            ->      REG.B
# MEAS.I                                           ->      MEAS.B
# CURRENT                                          ->      FIELD
# Current                                          ->      Field
# current                                          ->      field
# <div class="reg_i_only"> ... </div>              ->
# # START_I_ONLY # ... # END_I_ONLY #              ->

REG_I_YAML="reg_i.yaml"
REG_B_YAML="reg_b.yaml"

cd `dirname $0`

SCRIPT=`basename $0`

echo "# Generated from $REG_I_YAML using $SCRIPT - DO NOT EDIT!" > $REG_B_YAML

sed_cmd='s/name: I\s*$/name: B/g;
         s/I_ERR/B_ERR/g;
         s/I_REF/B_REF/g;
         s/reg\.i/reg.b/g;
         s/REG\.I/REG.B/g;
         s/MEAS\.I/MEAS.B/g;
         s/IREG/BREG/g;
         s/CURRENT/FIELD/g;
         s/Current/Field/g;
         s/current/field/g'

sed "$sed_cmd" $REG_I_YAML >> $REG_B_YAML

# Use perl to substitue the REG_I_ONLY divs and START/END_I_ONLY blocks, because sed
# processes the file line by line and hence cannot not apply a regex over multiple lines

PERL5LIB=../../../../../sw/lib/extern/perl perl <<__ENDPERL__
use File::Slurp;
my \$text = read_file('$REG_B_YAML');
\$text =~ s!\s*<div class="reg_i_only">(\s|\S)*?</div>!!g;
\$text =~ s!\s*# START_I_ONLY #(\s|\S)*?# END_I_ONLY #!!g;
write_file('$REG_B_YAML', \$text);
__ENDPERL__

# EOF
