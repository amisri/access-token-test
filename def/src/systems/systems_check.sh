#! /usr/bin/bash

name=~pclhc/etc/fgcd/name

# Check for number of uses of each dims_inc file in the systems files

for f in *.xml
do
    system=${f%.xml}

    printf '%-20s %3u  %s\n' $system $(grep ":$system\." $name | wc -l) 

done

# EOF
