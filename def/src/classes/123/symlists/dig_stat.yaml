symlist:
  title: Class 92 Digital Status
  doc: |
    The FGClite digital interface has 14 direct digital inputs. This symbol list maps
    the input bits of this register.

    The interlock inputs PC_PERMIT and PWR_FAILURE come from CONTROLLER_STATUS. INTLK_SPARE
    and TIMEOUT (present in FGC2) are not implemented in FGClite.

    VS_RUN is the read-back of the output to the PC and comes from CONVERTER_OUTPUT.

    The other signals are the digital inputs read back from the converter and come from CONVERTER_INPUT.

    This symbol list is used with {PROP DIG.STATUS}.
  children:
  - const:
      name: VS_POWER_ON
      value: '0x0001'
      title: Voltage source input power on
      doc: |
        This is the FGClite status bit POWER_ON, which indicates that the voltage input
        power is on. This may be a circuit breaker or an electronic switch, according to
        the type of converter.
  - const:
      name: VS_READY
      value: '0x0002'
      title: Voltage loop OK
      doc: |
        This is the FGClite status bit VLOOP_OK, which indicates that the voltage source
        has completed its start-up sequence and is ready.
  - const:
      name: VS_FAULT
      value: '0x0004'
      title: Voltage source fault
      doc: |
        This is the FGClite status bit VS_FAULT, which indicates that the voltage source
        cannot operate because an internal fault is present.
  - const:
      name: VS_EXTINTLK
      value: '0x0008'
      title: Voltage source external interlock
      doc: |
        This is the FGClite status bit VS_EXTINTLK, which indicates that the voltage source
        cannot operate because an external interlock signal is active, e.g. lack of water
        cooling.
  - const:
      name: FAST_ABORT
      value: '0x0100'
      title: Fast power abort interlock
      doc: |
        This is the FGClite status bit FASTPA_MEM, which is the latched
        copy of the FAST_PA interlock signal received by the voltage source.
  - const:
      name: PC_DISCH_RQ
      value: '0x0200'
      title: Discharge switch open request
      doc: |
        Power Converter Discharge Request is the SW_OP_REQ status bit, which
        is a readback on the switch open request signal sent from the voltage
        source to the PIC.
  - const:
      name: VS_NO_CABLE
      value: '0x8000'
      title: Cable to voltage source not connected
      doc: |
        This is the inverse of the FGClite status bit CABLE_OK.
        The voltage source cable includes a sense wire to detect if
        it is connected at both ends.
  - const:
      name: DCCT_A_FLT
      value: '0x0020'
      title: Dcct A fault
      doc: |
        This is the inverse of the FGClite status bit DCCTA_OK.
        It indicates that DCCT A is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
  - const:
      name: DCCT_B_FLT
      value: '0x0010'
      title: Dcct B fault
      doc: |
        This is the inverse of the FGClite status bit DCCTB_OK.
        It indicates that DCCT B is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
  - const:
      name: POL_SWI_POS
      value: '0x0040'
      title: Polarity switch positive
      doc: |
        This is the FGClite status bit POLSWPOS, which indicates that the polarity
        switch is in the positive position.
  - const:
      name: POL_SWI_NEG
      value: '0x0080'
      title: Polarity switch negative
      doc: |
        This is the FGClite status bit POLSWNEG, which indicates that the polarity
        switch is in the negative position.
  - const:
      name: VS_RUN
      value: '0x2000'
      title: Readback of VS_RUN command
      doc: |
        This is the FGClite status bit for readback of CMD0 (VS_RUN).
  - const:
      name: PC_PERMIT
      value: '0x0400'
      title: Power converter permit interlock
      doc: |
        This is the FGClite status bit PC_PERMIT, which is the interlock signal
        received from the PIC. If it is inactive (low), the state machine should
        transition to SLOW_ABORT.
  - const:
      name: PWR_FAILURE
      value: '0x1000'
      title: Readback of Powering Failure interlock signal
      doc: |
        This is the FGClite status bit POWERING_FAILURE, which is the readback on the
        power failure signal sent to the PIC.
# EOF
