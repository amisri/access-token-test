use File::Slurp;
use Getopt::Long;

die "Usage: $0 INPUT OUTPUT\n" if @ARGV != 2;
my($input_path, $output_path) = @ARGV;

my $text = read_file($input_path);
my @parts = split('#', $text);

open(FH, '>', $output_path) or die $!;

print FH "# Generated from $input_path via gen_devices.pl; do not edit by hand!\n\n";

print FH "properties:\n";

for my $device (1..8) {
    print FH $text =~ s/DEVICE_n/DEVICE_$device/rg;
}

close(FH);
