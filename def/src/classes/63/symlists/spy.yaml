symlist:
  title: Class 63 Spy Signal Selector
  doc: |
    This symbol list is used with {PROP SPY.MPX} to control which signals can be sent to the Spy interface.
  children:
  
      # This file can be parsed by fgc/def/scripts/spytemplate.awk to generate the contents
      #         of the function spyClassGetSignal() in spy_class.h. These contents need to be
      #         copied and pasted into the C file.
      # 
      #         The source attribute is needed unless the source is "regulationGetVarValue(name)",
      #         which is the default in the spytemplate.awk script.
  
      # INPUT SIGNALS ============================================================================
  - const:
      name: RAW_A
      value: 0
      title: Raw ADC A value
      source: sigVarValue(&sig_struct, adc,        adc_a,     ADC_RAW)
  - const:
      name: RAW_B
      value: 1
      title: Raw ADC B value
      source: sigVarValue(&sig_struct, adc,        adc_b,     ADC_RAW)
  - const:
      name: RAW_C
      value: 2
      title: Raw ADC C value
      source: sigVarValue(&sig_struct, adc,        adc_c,     ADC_RAW)
  - const:
      name: RAW_D
      value: 3
      title: Raw ADC D value
      source: sigVarValue(&sig_struct, adc,        adc_d,     ADC_RAW)
  - const:
      name: V_ADC_A
      value: 4
      title: ADC A voltage
      source: sigVarValue(&sig_struct, adc,        adc_a,     ADC_MEAS_UNFILTERED)
  - const:
      name: V_ADC_B
      value: 5
      title: ADC B voltage
      source: sigVarValue(&sig_struct, adc,        adc_b,     ADC_MEAS_UNFILTERED)
  - const:
      name: V_ADC_C
      value: 6
      title: ADC C voltage
      source: sigVarValue(&sig_struct, adc,        adc_c,     ADC_MEAS_UNFILTERED)
  - const:
      name: V_ADC_D
      value: 7
      title: ADC D voltage
      source: sigVarValue(&sig_struct, adc,        adc_d,     ADC_MEAS_UNFILTERED)
  - const:
      name: I_DCCT_A
      value: 8
      title: DCCT A current measurement
      source: sigVarValue(&sig_struct, transducer, dcct_a,    TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: I_DCCT_B
      value: 9
      title: DCCT B current measurement
      source: sigVarValue(&sig_struct, transducer, dcct_b,    TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: B_PROBE_A
      value: 10
      title: Field probe A measurement
      source: sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: B_PROBE_B
      value: 11
      title: Field probe B measurement
      source: sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: V_MEAS
      value: 12
      title: Measured converter voltage
      source: sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: EXT_REF
      value: 13
      title: External reference
      source: sigVarValue(&sig_struct, transducer, ext_ref,   TRANSDUCER_MEAS_UNFILTERED)
  - const:
      name: DCCT_DIFF
      value: 14
      title: Difference between I_DCCT_A and I_DCCT_B
      source: sigVarValue(&sig_struct, select,     i_meas,    SELECT_ABS_DIFF)
  - const:
      name: V_FLTR_A
      value: 15
      title: Filtered voltage measured on ADC for channel A
      source: sigVarValue(&sig_struct, adc,        adc_a,     ADC_MEAS_FILTERED)
  - const:
      name: V_FLTR_B
      value: 16
      title: Filtered voltage measured on ADC for channel B
      source: sigVarValue(&sig_struct, adc,        adc_b,     ADC_MEAS_FILTERED)
  - const:
      name: V_FLTR_C
      value: 17
      title: Filtered voltage measured on ADC for channel C
      source: sigVarValue(&sig_struct, adc,        adc_c,     ADC_MEAS_FILTERED)
  - const:
      name: V_FLTR_D
      value: 18
      title: Filtered voltage measured on ADC for channel D
      source: sigVarValue(&sig_struct, adc,        adc_d,     ADC_MEAS_FILTERED)
  - const:
      name: DCCT_FLTR_A
      value: 19
      title: Filtered DCCT A current measurement
      source: sigVarValue(&sig_struct, transducer, dcct_a,    TRANSDUCER_MEAS_FILTERED)
  - const:
      name: DCCT_FLTR_B
      value: 20
      title: Filtered DCCT B current measurement
      source: sigVarValue(&sig_struct, transducer, dcct_b,    TRANSDUCER_MEAS_FILTERED)
  - const:
      name: BPROBE_FLTR_A
      value: 21
      title: Filtered field probe A measurement
      source: sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_FILTERED)
  - const:
      name: BPROBE_FLTR_B
      value: 22
      title: Filtered Field probe B measurement
      source: sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_FILTERED)
  - const:
      name: V_MEAS_FLTR
      value: 23
      title: Filtered measured voltage
      source: sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_FILTERED)
  - const:
      name: EXT_REF_FLTR
      value: 24
      title: Filtered external reference
      source: sigVarValue(&sig_struct, transducer, ext_ref,   TRANSDUCER_MEAS_FILTERED)
  
      # FIELD SIGNALS ============================================================================
  - const:
      name: B_MEAS
      value: 25
      title: Unfiltered measured field (G)
      source: regulationGetVarValue(MEAS_B_UNFILTERED)
  - const:
      name: B_MEAS_FLTR
      value: 26
      title: Filtered measured field (G)
      source: regulationGetVarValue(MEAS_B_FILTERED)
  - const:
      name: B_MEAS_EXTR
      value: 27
      title: Extrapolated field (G)
      source: regulationGetVarValue(MEAS_B_EXTRAPOLATED)
  - const:
      name: B_MEAS_RATE
      value: 28
      title: Measured rate of change of field (G/s)
      source: regulationGetVarValue(MEAS_B_RATE)
  - const:
      name: B_REF_DELAYED
      value: 29
      title: Delayed field regulation reference (G)
  - const:
      name: B_ERR
      value: 30
      title: Field regulation error (G)
  - const:
      name: B_MAX_ABS_ERR
      value: 31
      title: Maximum absolute field regulation error (G)
  - const:
      name: B_MEAS_SIM
      value: 32
      title: Simulated measurement of field in the magnet (G)
      source: regulationGetVarValue(SIM_B_MEAS)
  - const:
      name: B_MEAS_REG
      value: 33
      title: Field measurement used for regulation (G)
  - const:
      name: B_REF_ADV
      value: 34
      title: Field reference advanced by the ref advance time (G)
  - const:
      name: B_REF_LIMITED
      value: 35
      title: Advanced field reference after limits (G)
  - const:
      name: B_REF_CLOSED
      value: 36
      title: Closed-loop field reference saved in history (G)
  - const:
      name: B_REF_OPEN
      value: 37
      title: Open-loop field reference saved in the hisory (G)
  - const:
      name: B_TRACK_DELAY
      value: 38
      title: Measured field regulation track delay in regulation periods
      source: regulationGetVarValue(B_TRACK_DELAY_PERIODS)
  - const:
      name: B_RMS_ERR
      value: 39
      title: RMS of field regulation error (G)
  
      # CURRENT SIGNALS ============================================================================
  - const:
      name: I_MEAS
      value: 40
      title: Unfiltered measured current (A)
      source: regulationGetVarValue(MEAS_I_UNFILTERED)
  - const:
      name: I_MEAS_FLTR
      value: 41
      title: Filtered measured current (A)
      source: regulationGetVarValue(MEAS_I_FILTERED)
  - const:
      name: I_MEAS_EXTR
      value: 42
      title: Extrapolated current (A)
      source: regulationGetVarValue(MEAS_I_EXTRAPOLATED)
  - const:
      name: I_MEAS_RATE
      value: 43
      title: Measured rate of change of current (G/s)
      source: regulationGetVarValue(MEAS_I_RATE)
  - const:
      name: I_REF_DELAYED
      value: 44
      title: Delayed current regulation reference (A)
  - const:
      name: I_ERR
      value: 45
      title: Current regulation error (A)
  - const:
      name: I_MAX_ABS_ERR
      value: 46
      title: Maximum absolute current regulation error (A)
  - const:
      name: I_MEAS_SIM
      value: 47
      title: Simulated measurement of current in the magnet (A)
      source: regulationGetVarValue(SIM_I_MEAS)
  - const:
      name: I_MAGNET_SIM
      value: 48
      title: Simulated current in the magnet without delay (A)
      source: regulationGetVarValue(SIM_I_MAGNET)
  - const:
      name: I_RMS
      value: 49
      title: RMS current (A)
  - const:
      name: I_RMS_LOAD
      value: 50
      title: RMS load current (A)
  - const:
      name: I_MEAS_REG
      value: 51
      title: Current measurement used for regulation (A)
  - const:
      name: I_REF_ADV
      value: 52
      title: Current reference advanced by ref advance (A)
  - const:
      name: I_REF_LIMITED
      value: 53
      title: Advanced current reference after limits (A)
  - const:
      name: I_REF_CLOSED
      value: 54
      title: Closed-loop current reference saved in the history (A)
  - const:
      name: I_REF_OPEN
      value: 55
      title: Open-loop current reference saved in the history (A)
  - const:
      name: I_TRACK_DELAY
      value: 56
      title: Measured current regulation track delay in regulation periods
      source: regulationGetVarValue(I_TRACK_DELAY_PERIODS)
  - const:
      name: I_RMS_ERR
      value: 57
      title: RMS current regulation error (A)
  
      # LOAD RELATED MEASUREMENT SIGNALS ============================================================================
  - const:
      name: SAT_FACTOR
      value: 58
      title: Magnet saturation factor
      source: regulationGetVarValue(LOAD_SAT_FACTOR)
  - const:
      name: I_MAG_SAT
      value: 59
      title: Current for magnet saturation model (A)
      source: regulationGetVarValue(LOAD_I_MAG_SAT)
  - const:
      name: MEAS_OHMS
      value: 60
      title: Measured resistence of the load (Ohms)
      source: regulationGetVarValue(LOAD_MEAS_OHMS)
  - const:
      name: MEAS_HENRYS
      value: 61
      title: Measured inductance of the load (H)
      source: regulationGetVarValue(LOAD_MEAS_HENRYS)
  - const:
      name: POWER
      value: 62
      title: Delivered power (W)
      source: regulationGetVarValue(MEAS_POWER)
  
      # VOLTAGE SIGNALS ============================================================================
  - const:
      name: V_REF_REG
      value: 63
      title: Voltage reference from regulator or function generator (V)
  - const:
      name: V_REF_SAT
      value: 64
      title: Voltage reference after magnet saturation compensation (V)
  - const:
      name: V_REF_DECO
      value: 65
      title: Voltage reference after sat comp and decoupling (V)
  - const:
      name: V_REF
      value: 66
      title: Voltage reference after sat comp, decoupling and limits (V)
  - const:
      name: V_REF_VS
      value: 67
      title: Voltage reference for each voltage source (V)
  - const:
      name: V_REF_DAC
      value: 68
      title: Voltage reference to send to DAC (V)
      source: referenceGetVarValue(REF_DAC)
  - const:
      name: V_REF_RATE
      value: 69
      title: Voltage reference rate of change (V/s)
  - const:
      name: V_RATE_RMS
      value: 70
      title: RMS of Voltage reference rate of change (V/s)
  - const:
      name: V_ERR
      value: 71
      title: Voltage error (V_MEAS_REG-V_MEAS_SIM) (V)
  - const:
      name: V_MAX_ABS_ERR
      value: 72
      title: Maximum absolute voltage error (V)
  - const:
      name: V_MEAS_SIM
      value: 73
      title: Simulated voltage measurement (V)
      source: regulationGetVarValue(SIM_V_MEAS)
  
      # FIRING SIGNALS ============================================================================
  - const:
      name: V_MEAS_REG
      value: 74
      title: Voltage measurement for regulation (V)
      source: regulationGetVarValue(MEAS_V_UNFILTERED)
  - const:
      name: V_REG_ERR
      value: 75
      title: Voltage regulation error ()(V)
      source: regulationGetVarValue(VREG_ERR)
  - const:
      name: V_INTEGRATOR
      value: 76
      title: Voltage regulation integrator (V)
      source: regulationGetVarValue(VREG_INTEGRATOR)
  - const:
      name: D_REF
      value: 77
      title: Damping reference (V)
      source: regulationGetVarValue(VFILTER_REF)
  - const:
      name: D_MEAS
      value: 78
      title: Damping measurement (V)
      source: regulationGetVarValue(VFILTER_MEAS)
  - const:
      name: F_REF_REG
      value: 79
      title: Firing reference
      source: regulationGetVarValue(FIRING_REF_REG)
  - const:
      name: F_REF_LIMITED
      value: 80
      title: Firing reference after limits
      source: regulationGetVarValue(FIRING_REF_LIMITED)
  - const:
      name: F_REF
      value: 81
      title: Firing reference after limits and linearisation
      source: regulationGetVarValue(FIRING_REF)
  - const:
      name: I_CAPA
      value: 82
      title: Capacitor current used in damping measurement (partial) (A)
      source: regulationGetVarValue(VFILTER_I_CAPA)
  - const:
      name: I_CAPA_MEAS
      value: 83
      title: Measured capacitor current (partial or full) (A)
      source: regulationGetVarValue(MEAS_I_CAPA_UNFILTERED)
  - const:
      name: I_CAPA_SIM
      value: 84
      title: Simulated capacitor current (partial) (A)
      source: regulationGetVarValue(SIM_I_CAPA_MEAS)
  
      # I_EARTH ============================================================================
  - const:
      name: I_EARTH
      value: 85
      title: Earth current (A)
      source: dpcom.mcu.meas.i_earth[0]
  
      # TEMPERATURE SIGNALS ============================================================================
  - const:
      name: T_FGC_IN
      value: 86
      title: FGC inlet temperature
      source: dpcom.mcu.temp.in
      doc: |
        The FGC inlet temperature. This is the is the temperature used for the
        ADC temperature compensation after filtering with a first order filter
        with time constant {PROP ADC.INTERNAL.TAU_TEMP}.
  - const:
      name: T_FGC_OUT
      value: 87
      title: FGC outlet temperature
      source: dpcom.mcu.temp.out
      doc: |
        FGC outlet temperature.
  - const:
      name: T_DCCT_A
      value: 88
      title: DCCT_A calibration temperature
      source: dpcom.mcu.temp.dcct[0]
      doc: |
        The DCCT A calibration can include compensation for the temperature. This
        	         is the temperature used for the calculations and is derived from the DCCT_A
        	         temperature if available, or the FGC inlet temperature if not.
        	         The measured temperature is filtered with a first order filter with time constant
        	         {PROP DCCT.TAU_TEMP} in order to derive the calibration temperature.
  - const:
      name: T_DCCT_B
      value: 89
      title: DCCT_B calibration temperature
      source: dpcom.mcu.temp.dcct[1]
      doc: |
        The DCCT B calibration can include compensation for the temperature. This
        	         is the temperature used for the calculations and is derived from the DCCT_B
        	         temperature if available, or the FGC inlet temperature if not.
        	         The measured temperature is filtered with a first order filter with time constant
        	         {PROP DCCT.TAU_TEMP} in order to derive the calibration temperature.
  
      # PLL SIGNALS ============================================================================
  - const:
      name: PLL_STATE
      value: 90
      title: Phase locked loop state
      source: dpcom.mcu.pll.state
  - const:
      name: PLL_E
      value: 91
      title: Phase locked loop phase error (s)
      source: dpcom.mcu.pll.error               / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_INTE
      value: 92
      title: Phase locked loop integrator (ppm)
      source: dpcom.mcu.pll.integrator          / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_FILT_INTE
      value: 93
      title: Phase locked loop filtered integrator (ppm)
      source: dpcom.mcu.pll.filtered_integrator / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_DAC
      value: 94
      title: Phase locked loop DAC value (ppm)
      source: dpcom.mcu.pll.dac
  - const:
      name: PLL_ETH_SYNC
      value: 95
      title: Phase locked loop ethernet sync calibration
      source: dpcom.mcu.pll.eth_sync_cal        / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_EXT_E
      value: 96
      title: Phase locked loop external phase error (s)
      source: dpcom.mcu.pll.ext_error           / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_E18_E
      value: 97
      title: Phase locked loop ETH18 phase error (s)
      source: dpcom.mcu.pll.e18_error           / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_E19_E
      value: 98
      title: Phase locked loop ETH19 phase error (s)
      source: dpcom.mcu.pll.e19_error           / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_EXT_A_E
      value: 99
      title: Phase locked loop external average phase error (s)
      source: dpcom.mcu.pll.ext_avg_error       / (float)PLL_TICKS_TO_SECONDS
  - const:
      name: PLL_NET_A_E
      value: 100
      title: Phase locked loop network average phase error (s)
      source: dpcom.mcu.pll.net_avg_error       / (float)PLL_TICKS_TO_SECONDS
  
      # DSP USAGE SIGNALS ============================================================================
  - const:
      name: DSPISR
      value: 101
      title: DSP iteration ISR CPU usage
      source: iter.cpu_usage
# EOF
