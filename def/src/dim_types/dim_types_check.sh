#! /usr/bin/bash

# Create report

xml_files=*.xml

printf '# DIM TYPES REPORT\n\n'

printf ' %-55s%s\n' 'Number of analog signals:' $(grep '<ana_chan' $xml_files | wc -l)
printf ' %-55s%s\n' 'Number of digital signals:' $(grep '<input' $xml_files | wc -l)
printf ' %-55s%s\n' 'Number of fault signals:' $(grep 'fault="1"' $xml_files | wc -l)
printf ' %-55s%s\n' 'Number of status signals:' $(grep 'fault="0"' $xml_files | wc -l)
printf ' %-55s%s\n' 'Number of fault signals with empty one labels:' $(grep 'fault="1".*one=""' $xml_files |  wc -l)
printf ' %-55s%s\n' 'Number of fault signals with non-empty zero labels:' $(grep 'fault="1"' $xml_files | grep -v 'zero=""' |  wc -l)
printf ' %-55s%s\n' 'Number of status signals with empty zero labels:' $(grep 'fault="0".*zero=""' $xml_files |  wc -l)
printf ' %-55s%s\n' 'Number of status signals with empty one labels:' $(grep 'fault="0".*one=""' $xml_files |  wc -l)

printf '\n# List of unique analog signal labels (%u):\n' $(gawk '/^ *<ana_chan/{print}' *.xml | sed 's| *<ana_chan.*\label="\(.*\)".*".*".*".*".*".*".*$|  \1|g' | sort | uniq | wc -l)
gawk '/^ *<ana_chan/{print}' *.xml | sed 's| *<ana_chan.*\label="\(.*\)".*".*".*".*".*".*".*$|  \1|g' | sort | uniq

printf '\n# List of unique digital signal labels (%u):\n' $(gawk '/^ *<input./{print}' *.xml | sed 's| *<input.*\label="\(.*\)".*".*".*".*".*$|  \1|g' | sort | uniq | wc -l)
gawk '/^ *<input./{print}' *.xml | sed 's| *<input.*\label="\(.*\)".*".*".*".*".*$|  \1|g' | sort | uniq

printf '\n# List of unique status signal zero labels (%u):\n' $(gawk '/^ *<input.*fault="0"/{print}' *.xml | sed 's| *<input.*\zero="\(.*\)".*".*".*$|  \1|g' | sort | uniq | wc -l)
gawk '/^ *<input.*fault="0"/{print}' *.xml | sed 's| *<input.*\zero="\(.*\)".*".*".*$|  \1|g' | sort | uniq

printf '\n# List of unique status signal one labels (%u):\n' $(gawk '/^ *<input.*fault="0"/{print}' *.xml | sed 's| *<input.*\one="\(.*\)".*|  \1|g' | sort | uniq | wc -l)
gawk '/^ *<input.*fault="0"/{print}' *.xml | sed 's| *<input.*\one="\(.*\)".*|  \1|g' | sort | uniq

printf '\n# List of unique fault signal one labels:\n'
gawk '/^ *<input.*fault="1"/{print}' *.xml | sed 's| *<input.*\one="\(.*\)".*|  \1|g' | sort | uniq

# Create an index file for each dim type, position and variant used by files in dims_inc

gawk '/dim name/ \
{ \
    sub(/>/," "); \
    gsub(/"/,""); \
    for(i=2;i<=NF;i++) { if(split($i,a,"=")==2) {key[a[1]]=a[2]}}; \
    printf "%s_%s_%s\n",key["type"],key["position"],key["variant"] \
}' ../dims_inc/*.xml | sort | uniq > .dims_inc_index

# Check every dim type to see if it used by dims inc

printf '\n# DIM type reference counts\n\n'

for f in $xml_files
do
    dim_type=$(gawk '/<dim_type/{sub(/>/," ");gsub(/"/,"");for(i=2;i<=NF;i++){if(split($i,a,"=")==2){key[a[1]]=a[2]}};printf "%s_%s_%s\n",key["comp"],key["position"],key["variant"]}' $f)

    printf '%-20s%-30s%s\n' $f $dim_type $(grep -n $dim_type .dims_inc_index | wc -l)

done

rm .dims_inc_index

# EOF
