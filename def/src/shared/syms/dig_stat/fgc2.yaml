consts:
  children:
  - const:
      name: DCCT_A_FLT
      value: '*MCU.DIG_IP1_DCCTAFLT_MASK16'
      title: Dcct A fault
      doc: |
        This signal indicates that DCCT A is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
  - const:
      name: DCCT_B_FLT
      value: '*MCU.DIG_IP1_DCCTBFLT_MASK16'
      title: Dcct B fault
      doc: |
        This signal indicates that DCCT B is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
  - const:
      name: FAST_ABORT
      value: '*MCU.DIG_IP1_FASTABORT_MASK16'
      title: Fast power abort interlock
      doc: |
        This signal is the latched copy of the FAST_PA interlock
        signal that is received by the voltage source.
  - const:
      name: INTLKSPARE
      value: '*MCU.DIG_IP1_INTLKSPARE_MASK16'
      title: Spare interlock input
      doc: |
        The FGC has two opto-coupler inputs for interlock signals.
        One is dedicated to the PC_PERMIT signal and the other is
        spare.  This bit indicates the state of this spare input.
  - const:
      name: PC_DISCH_RQ
      value: '*MCU.DIG_IP1_PCDISCHRQ_MASK16'
      title: Discharge switch open request
      doc: |
        This is a readback on the switch open request signal
        sent from the voltage source to the PIC.
  - const:
      name: PC_PERMIT
      value: '*MCU.DIG_IP1_PCPERMIT_MASK16'
      title: Power converter permit interlock
      doc: |
        This interlock signal is received from the PIC.  If it is
        inactive, the SLOW_ABORT signal is set.
  - const:
      name: POL_SWI_NEG
      value: '*MCU.DIG_IP1_POLSWINEG_MASK16'
      title: Polarity switch negative
      doc: |
        This signal indicates that the polarity switch is
        in the negative position.
  - const:
      name: POL_SWI_POS
      value: '*MCU.DIG_IP1_POLSWIPOS_MASK16'
      title: Polarity switch positive
      doc: |
        This signal indicates that the polarity switch is
        in the positive position.
  - const:
      name: PWR_FAILURE
      value: '*MCU.DIG_IP1_PWRFAILURE_MASK16'
      title: Readback of Powering Failure interlock signal
      doc: |
        This is the readback on the power failure signal, sent
        to the PIC.
  - const:
      name: TIMEOUT
      value: '*MCU.DIG_IP1_TIMEOUT_MASK16'
      title: Digital interface timeout
      doc: |
        This is the readback on the mono-stable watchdog timer (~2s) on the
        digital interface.  This is triggered by the HC16 to allow the
        VS_RUN_CMD to be set.  If the FGC crashes, the interface will
        keep the voltage source running for two seconds, enough time for
        it to restart and re-establish control.  The readback is provided
        for self-test purposes.
  - const:
      name: VS_EXTINTLK
      value: '*MCU.DIG_IP1_VSEXTINTLK_MASK16'
      title: Voltage source external interlock
      doc: |
        This signal indicates that the voltage source cannot
        operate because an external interlock signal is active.
        (E.g. lack of water cooling).
  - const:
      name: VS_FAULT
      value: '*MCU.DIG_IP1_VSFAULT_MASK16'
      title: Voltage source fault
      doc: |
        This signal indicates that the voltage source cannot
        operate because an internal fault is present.
  - const:
      name: VS_NO_CABLE
      value: '*MCU.DIG_IP1_VSNOCABLE_MASK16'
      title: Cable to voltage source not connected
      doc: |
        The voltage source cable includes a sense wire to detect if
        it is connected at both ends.
  - const:
      name: VS_POWER_ON
      value: '*MCU.DIG_IP1_VSPOWERON_MASK16'
      title: Voltage source input power on
      doc: |
        This signal indicates that the voltage input power
        is on.  This may be a circuit breaker or an electronic
        switch, according to the type of converter.
  - const:
      name: VS_READY
      value: '*MCU.DIG_IP1_VSREADY_MASK16'
      title: Voltage loop OK
      doc: |
        This signal indicates that the voltage source has completed
        its start-up sequence and is ready.
  - const:
      name: VS_RUN
      value: '*MCU.DIG_IP1_VSRUN_MASK16'
      title: Readback of VS_RUN command
      doc: |
        This is the readback of the VS_RUN command.
  
      # EOF
# EOF
