consts:
  doc: |
    This symbol list is used to report the status of the attempt to initialise the
    current, field regulation RST coefficients, or Voltage regulation coefficients.
    It is used for both the operational and test regulators. It is used by software
    classes that use version 2 of libreg.
  children:
  - const:
      name: OK
      value: 0
      assert: REG_OK
      title: Regulator is OK
      doc: |
        The regulator coefficients are valid.
  - const:
      name: LOW_MOD_MARGN
      value: 1
      assert: REG_WARNING_LOW_MODULUS_MARGIN
      title: 'Warning: Low modulus margin'
      doc: |
        Warning: Modulus margin less than 0.4. This means that the regulator risks to become
        unstable if the load coefficients in {PROP LOAD} mismatch the reality. You can
        increase the modulus margin by reducing the CLBW and CLBW2 property values, or
        increasing the regulation period.
  - const:
      name: OHMS_PAR_SMAL
      value: 2
      assert: REG_FAULT_OHMS_PAR_TOO_SMALL
      title: 'Fault: The parallel resistance is too small.'
      doc: |
        The parallel resistance is too small. A significant parallel (<1.0E6) is only
        supported if the PURE_DELAY_PERIODS is less than 0.401. The pure delay can be
        reduced by setting MEAS_SELECT to EXTRAPOLATED in {PROP REG.B.INTERNAL}/{PROP REG.B.EXTERNAL}
        or {PROP REG.I.INTERNAL}/{PROP REG.I.EXTERNAL} (according to {PROP REG.STATE}).
  - const:
      name: PURE_DLY_BIG
      value: 3
      assert: REG_FAULT_PURE_DELAY_TOO_LARGE
      title: 'Fault: Pure delay is too big'
      doc: |
        Pure delay is too large (max is 2.4 periods). The internal RST synthesis algorithms
        work with a maximum pure delay of 2.4 regulation periods. The pure delay can be
        reduced by setting MEAS_SELECT to EXTRAPOLATED in {PROP REG.B.INTERNAL}/{PROP REG.B.EXTERNAL}
        or {PROP REG.I.INTERNAL}/{PROP REG.I.EXTERNAL} (according to {PROP REG.STATE}).
  - const:
      name: R0_IS_ZERO
      value: 4
      assert: REG_FAULT_R0_IS_ZERO
      title: 'Fault: R[0] is zero'
      doc: |
        R[0] is zero, which prevents operation of the RST algorithm.
        For this test, "zero" is considered to be in the range +/-1E-7.
  - const:
      name: S0_NOT_POS
      value: 5
      assert: REG_FAULT_S0_NOT_POSITIVE
      title: 'Fault: S[0] is zero (or negative)'
      doc: |
        S[0] is zero (or negative), which prevents operation of the RST algorithm.
        For this test, "zero" is considered to be in the range +/-1E-7.
  - const:
      name: T0_NOT_POS
      value: 6
      assert: REG_FAULT_T0_NOT_POSITIVE
      title: 'Fault: T[0] is zero (or negative)'
      doc: |
        T[0] is zero (or negative), which prevents operation of the RST algorithm.
        For this test, "zero" is considered to be in the range +/-1E-7.
  - const:
      name: S_UNSTBL_ROOT
      value: 7
      assert: REG_FAULT_S_HAS_UNSTABLE_ROOT
      title: 'Fault: Unstable root in S'
      doc: |
        Jury's test indicates that S contains an unstable root.
  - const:
      name: T_UNSTBL_ROOT
      value: 8
      assert: REG_FAULT_T_HAS_UNSTABLE_ROOT
      title: 'Fault: Unstable root in T'
      doc: |
        Jury's test indicates that T contains an unstable root.
  
      # EOF
# EOF
