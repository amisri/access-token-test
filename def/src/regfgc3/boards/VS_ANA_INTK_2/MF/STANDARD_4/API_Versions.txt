--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS Analog INTK 2
--//		     XILINX: XC7A50T-2CSG324C
--//                 Flash:  N25Q128A13ESE40
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: Standard
--//               ........................................
--// AUTHOR: Matteo Di Cosmo
--//*****************************************************************************

*****************************************************************************
API-Version 200
	DATE: 17/04/2018
	AUTHOR: Matteo Di Cosmo

	New structure directory

	
