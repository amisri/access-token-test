<?xml version='1.0'?>

<variant>
    <doc><![CDATA[
        FGC3 to/from CVM Modules for exchange of Data between FGC3 and Converter
        Converter: IGBT with External Timing
    ]]></doc>


    <!-- ====================================================== -->
    <!-- Write Block 0 -->

    <write_block
        number  = "0"
        title   = "External-Internal Timing"
    >
        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB0_Source"
            type            = "uint16_t"
            size_bits       = "4"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                External or FGC3 Sync pulse Number for pulse generated in DIG-SUPB0
                Default: 1
                Formula: Value = 0 to 10. 0: Disabled
            ]]></doc>
        </element>

        <!-- ====================================================== -->
        <element
            name            = "DIG_SUPB0_PassThrough"
            type            = "uint16_t"
            size_bits       = "1"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Set if the Pulse generated in DIG-SUPB0 is a direct copy of the Source Pulse
                Default: 0
                Formula: Value = 0 or 1
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB0_Delay"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Delay of Pulse in DIG-SUPB0 from External Source sync pulse
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB0_Width"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Width of Pulse in DIG-SUPB0, if not in Mode PassThrough
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>




        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB1_Source"
            type            = "uint16_t"
            size_bits       = "4"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                External or FGC3 Sync pulse Number for pulse generated in DIG-SUPB1
                Default: 0
                Formula: Value = 0 to 10. 0: Disabled
            ]]></doc>
        </element>

        <!-- ====================================================== -->
        <element
            name            = "DIG_SUPB1_PassThrough"
            type            = "uint16_t"
            size_bits       = "1"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Set if the Pulse generated in DIG-SUPB1 is a direct copy of the Source Pulse
                Default: 0
                Formula: Value = 0 or 1
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB1_Delay"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Delay of Pulse in DIG-SUPB1 from External Source sync pulse
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB1_Width"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Width of Pulse in DIG-SUPB1, if not in Mode PassThrough
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>


        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB2_Source"
            type            = "uint16_t"
            size_bits       = "4"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                External or FGC3 Sync pulse Number for pulse generated in DIG-SUPB2
                Default: 0
                Formula: Value = 0 to 10. 0: Disabled
            ]]></doc>
        </element>

        <!-- ====================================================== -->
        <element
            name            = "DIG_SUPB2_PassThrough"
            type            = "uint16_t"
            size_bits       = "1"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Set if the Pulse generated in DIG-SUPB2 is a direct copy of the Source Pulse
                Default: 0
                Formula: Value = 0 or 1
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB2_Delay"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Delay of Pulse in DIG-SUPB2 from External Source sync pulse
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "DIG_SUPB2_Width"
            type            = "uint16_t"
            size_bits       = "29"
            default_value   = "0x00000000"
        >
            <doc><![CDATA[
                Width of Pulse in DIG-SUPB2, if not in Mode PassThrough
                Default: 0 usec
                Formula: Time = default_value / 80 MHz
            ]]></doc>
        </element>



        <!-- ==================== End of Elements ================= -->

        <doc><![CDATA[
            Block sending Timing Parameters for DigSupB.
            Change allowed: always
        ]]></doc>
    </write_block>

    <!-- ====================================================== -->
    <!-- Write Block 1 -->

    <write_block
        number  = "1"
        title   = "State Machine parameters"
    >
        <!-- ====================================================== -->

        <element
            name            = "Wait_Initialization"
            type            = "uint16_t"
            size_bits       = "13"
            default_value   = "0x0000000A"
        >
            <doc><![CDATA[
                Delay after Power-ON before accepting Commands.
                Default: 10 msec
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "Wait_MCB_to_ON"
            type            = "uint16_t"
            size_bits       = "13"
            default_value   = "0x000007D0"
        >
            <doc><![CDATA[
                Delay before testing if MCB is ON.
                Default: 2000 msec
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "Wait_MCB_to_OFF"
            type            = "uint16_t"
            size_bits       = "13"
            default_value   = "0x000007D0"
        >
            <doc><![CDATA[
                Delay before testing if MCB is OFF.
                Default: 2000 msec
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "PulseWidth_Reset_Hardware"
            type            = "uint16_t"
            size_bits       = "10"
            default_value   = "0x00000064"
        >
            <doc><![CDATA[
                width of Reset Pulse sent to external hardware.
                Default: 100 msec
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "Wait_PRECH_Status"
            type            = "uint16_t"
            size_bits       = "13"
            default_value   = "0x000001F4"
        >
            <doc><![CDATA[
                Delay before testing if Precharge is ON or OFF.
                Default: 500 msec
            ]]></doc>
        </element>


        <!-- ====================================================== -->

        <element
            name            = "Wait_PRECH_ON_to_MCB_ON"
            type            = "uint16_t"
            size_bits       = "15"
            default_value   = "0x000007D0"
        >
            <doc><![CDATA[
                Delay to wait after Precharge is ON to set MCB to ON.
                Default: 2000 msec
            ]]></doc>
        </element>


        <!-- ====================================================== -->

        <element
            name            = "Wait_MCB_ON_to_PRECH_OFF"
            type            = "uint16_t"
            size_bits       = "15"
            default_value   = "0x000001F4"
        >
            <doc><![CDATA[
                Delay to wait after MCB is ON to set Precharge to OFF.
                Default: 500 msec
            ]]></doc>
        </element>


        <!-- ==================== End of Elements ================= -->

        <doc><![CDATA[
            Block sending the Parameters for the Converter State Machine.
            Change allowed: only when Converter is OFF
        ]]></doc>
    </write_block>



    <!-- ====================================================== -->
    <!-- Definitions for READ Blocks: FGC3 to CVM -->

    <!-- ====================================================== -->
    <!-- Read Block 0 -->

    <read_block
        number  = "0"
        title   = "Converter Status"
    >
        <!-- ====================================================== -->

        <element
            name            = "ConverterStateMachine_state"
            type            = "uint16_t"
            size_bits       = "7"
        >
            <doc><![CDATA[
                Status of the State Machine.
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "ConverterStateMachine_warnings"
            type            = "uint16_t"
            size_bits       = "8"
        >
            <doc><![CDATA[
                Warnings detected by the State Machine controlling the Converter.
            ]]></doc>
        </element>

        <!-- ====================================================== -->

        <element
            name            = "Timing_warnings"
            type            = "uint16_t"
            size_bits       = "8"
        >
            <doc><![CDATA[
                Warnings detected by the Timing State Machine.
            ]]></doc>
        </element>

        <!-- ==================== End of elements ================= -->

        <doc><![CDATA[
            Block transfering the status of the Timing and State Machine parameters.
            Request allowed:
            	- always
        ]]></doc>

    </read_block>
</variant>

<!-- EOF -->
