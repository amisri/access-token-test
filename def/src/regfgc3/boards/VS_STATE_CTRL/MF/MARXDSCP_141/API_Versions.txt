--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS StateControl INTK
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: MARXDSCP_141 Crate Type-10
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************

*****************************************************************************
API-Version 216
	DATE: 03/02/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-2: Eliminated Elements: Pulse_A3_duration/delay. Local Pulses generate signals equivalent to FGC3 (A0/A1/A2)
		
*****************************************************************************
API-Version 215
	DATE: 03/02/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-3: Order of B3 EDM Elements. Text description
		
*****************************************************************************
API-Version 214
	DATE: 28/01/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-3: Changed Frequency/Gain for Element-5
		
*****************************************************************************
API-Version 213
	DATE: 27/01/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-3: Changed Frequency/Gain for Element-1
		
*****************************************************************************
API-Version 212
	DATE: 25/01/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-3: Changed gain for InputPulse_Length_min
		
*****************************************************************************
API-Version 211
	DATE: 25/01/2021
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-3: default Values
		
		
*****************************************************************************
API-Version 210
	DATE: 21/01/2021
	AUTHOR: Gomez Costa J.L.

	Add:
        Block-3 Pulse Timing
		
*****************************************************************************
API-Version 209
	DATE: 04/11/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        Block-2 Local Timing Element Pulse_A0_duration size
        
*****************************************************************************
API-Version 208
	DATE: 11/06/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        Parameters for Pulse_A2_duration
        
*****************************************************************************
API-Version 207
	DATE: 09/06/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        Parameters for CycleStart_period
        
*****************************************************************************
API-Version 206
	DATE: 03/06/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        Local Definitions transferred to VS StateControl
        
*****************************************************************************
API-Version 205
	DATE: 28/01/2020
	AUTHOR: Gomez Costa J.L.

    Modified:
        MinimumTime_OFF2ON: Units seconds
        
*****************************************************************************
API-Version 204
	DATE: 06/12/2019
	AUTHOR: Gomez Costa J.L.

    Eliminated:
        Timeout_ActiveFilter
        
*****************************************************************************
API-Version 203
	DATE: 21/10/2019
	AUTHOR: Gomez Costa J.L.

    Modified:
        MinimumTime_OFF2ON: passed to msec
        
*****************************************************************************
API-Version 202
	DATE: 09/10/2019
	AUTHOR: Gomez Costa J.L.

	Bug:
        Corrected wrong name
        
*****************************************************************************
API-Version 201
	DATE: 09/10/2019
	AUTHOR: Gomez Costa J.L.

	Added:
        VS StateControl WriteBlock-1 Minimum Time OFF-ON
    
*****************************************************************************
API-Version 200
	DATE: 23/05/2019
	AUTHOR: Gomez Costa J.L.

	First Revision

	
