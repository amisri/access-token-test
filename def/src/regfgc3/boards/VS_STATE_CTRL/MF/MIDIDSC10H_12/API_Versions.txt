--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS StateControl INTK
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: MIDIDSC10H_12
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************


*****************************************************************************
API-Version 203
	DATE: 03/11/2020
	AUTHOR: Gomez Costa J.L.

	Bug:
        WriteBlock 0: Element 1 name "EarthFault Mode" -> EarthFaultMode
        
*****************************************************************************
API-Version 202
	DATE: 02/11/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        Variant Name: MidiDiscap 5H
        WriteBlock 0: Element 1
       
    Eliminated:
        - WriteBlock 0: Elements 2 - 7
        
*****************************************************************************
API-Version 201
	DATE: 20/05/2020
	AUTHOR: Gomez Costa J.L.

	Eliminated:
        - ReadBlock 0: ModuleUART_RX_ErrorCounters
        
*****************************************************************************
API-Version 200
	DATE: 19/04/2018
	AUTHOR: Gomez Costa J.L.

	Based on Revision 5

	
