*****************************************************************************
 CERN SY-EPC-CCE ............................................................
 SYSTEM: 	VS_EX_FMC_POF
		     XILINX: XC7A50T-2CSG324C
                 Flash:  N25Q128A13ESE40

 DESCRIPTION :
		Definition of FGC3CVM protocol FGC3-card
		Converter: DownloadBoot

*****************************************************************************

*****************************************************************************
API-Version 200
	DATE: 20/01/2023
	AUTHOR: Gomez Costa J.L.

	First Version

	
