--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS Analog INTK
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: Standard
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************

*****************************************************************************
API-Version 200
	DATE: 09/04/2018
	AUTHOR: Gomez Costa J.L.

	New structure directory
	Based on Version 1 10/05/2017

*****************************************************************************
API-Version 201
	DATE: 25/11/2022
	AUTHOR: Gomez Costa J.L.

	Eliminated:
        All Block definition
	
