--//*****************************************************************************
--// CERN TE-EPC-CCE ............................................................
--// SYSTEM: 	VS RegulationDSP
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: Development
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************

*****************************************************************************
API-Version 301
	DATE: 04/08/2020
	AUTHOR: Gomez Costa J.L.

	Modified:
        - WriteBlock 0, Element names
    
*****************************************************************************
API-Version 300
	DATE: 03/08/2020
	AUTHOR: Gomez Costa J.L.

	Test of New FGC3CVM Tasks

	
