*****************************************************************************
 CERN SY-EPC-CCE ............................................................
 SYSTEM: 	VS RegulationDSP
		     XILINX: XC3S700AN-4FG484C
                 Flash: None

 DESCRIPTION :
		Definition of FGC3CVM protocol FGC3-card
		Converter: MARXDISCAP 141

*****************************************************************************

*****************************************************************************
API-Version 221
	DATE: 14/02/2022
	AUTHOR: Gomez Costa J.L.
	
    Modified:
        - WriteBlock 0: Pulse_Ax_zzz replaced by Pulse_Bx_zzz: to be cohenernt, pulses are from DIG_SUPB
		
