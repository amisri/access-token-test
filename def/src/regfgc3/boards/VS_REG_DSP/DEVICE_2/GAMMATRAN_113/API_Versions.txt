--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS RegulationDSP
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: IGBT_37 Device-2: DSP GAMMATRAN_113
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************

*****************************************************************************
API-Version 200
	DATE: 02/08/2019
	AUTHOR: Gomez Costa J.L.

	Based on Revision 1

	
