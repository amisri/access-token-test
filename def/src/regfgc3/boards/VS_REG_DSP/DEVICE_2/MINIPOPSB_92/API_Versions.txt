--//*****************************************************************************
--// CERN TE-EPC-CC..............................................................
--// SYSTEM: 	VS RegulationDSP
--//		     XILINX: XC3S700AN-4FG484C
--//                 Flash: None
--//
--// DESCRIPTION :
--//		Definition of FGC3CVM protocol FGC3-card
--//		Converter: MINIPOPSB 92 Device-2: DSP
--//               ........................................
--// AUTHOR: Gomez Costa J.L.
--//*****************************************************************************

*****************************************************************************
API-Version 200
	DATE: 05/06/2018
	AUTHOR: Gomez Costa J.L.

	First revision

	
