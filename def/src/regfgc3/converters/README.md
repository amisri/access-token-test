As of 2023-06-20, these are not being updated anymore -- see https://issues.cern.ch/browse/EPCCCS-9579

This is a first step in gradual phase-out of this infrastructure.
