#!/usr/bin/perl -w
#
# Name:     Parser.pm
# Purpose:  Processes XML and returns a useful tree
# Author:   Stephen Page

package Parser;

use Carp;
use Exporter;
use File::Basename;
use strict;
use XML::Parser;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse printnode);

my $current_node;
my $doc_links;
my $doc_splinks;
my $file;
my $node_end_handler;
my $node_start_handler;
my $node_tag;
my $node_tags;
my $root;
my $superhash;

return(1);


# Begin functions

sub printnode($$);
sub populate_hash($$$);

# Function to parse an XML file and return a useful tree
sub parse(%)
{
    my %args                = @_;
    $file                   = $args{file};
    undef($root);
    undef($current_node);
    undef($node_tag);
    $doc_links              = $args{doc_links};
    $doc_splinks            = $args{doc_splinks};
    $node_tags              = $args{node_tags};
    $node_start_handler     = $args{node_start_handler};
    $node_end_handler       = $args{node_end_handler};
    $superhash              = $args{superhash};

    die "Invalid file ($file) passed to Parser::parse\n" unless defined($file) && -f $file;

    my $parser = new XML::Parser(
                                    ErrorContext    => 0,
                                    ParseParamEnt   => 0,
                                );

    $parser->setHandlers(
                            Start       => \&starthandler,
                            Char        => \&charhandler,
                            End         => \&endhandler,
                            ExternEnt   => \&extenthandler,
                        );

    # Parse the XML file (eval so that we don't die by croak on XML error).

    eval{$parser->parsefile($file)};

    # Handle error during parsing

    if($@)
    {
        # Convert $file path to MS-DOS syntax

        $file =~ s#/#\\#g;

        # Make error message one line for easier manipulation

        $@ =~ s/\n//g;

        # Strip out error string

        my $errorstring =  $@;
        $errorstring    =~ s/^(.*)? at line.*/$1/;

        # Strip out error line number

        my $linenum =  $@;
        if($linenum !~ s/.* at line (\d*).*/$1/)
        {
            $linenum = 9999;
        }

        confess "$file($linenum): ERROR $errorstring\n";
    }

    return($root);
}

# Function to handle XML start tags
sub starthandler($$$)
{
    my ($expat, $element, %attributes) = @_;

    if(!defined($node_tags) || defined($node_tags->{$element}))
    {
        if(!defined($root))
        {
            # Set root and current node

            $root = $current_node = \%attributes;

            # If node tags have not been defined, use this tag as a node tag

            if(!defined($node_tags))
            {
                $node_tags->{$element}  = 1;
            }

            $current_node->{node_depth} = 0;
        }
        else
        {
            # Add reference to parent (current_node at this stage)

            $attributes{parent} = $current_node;

            # Set current node

            $current_node = \%attributes;

            # Add reference to this node within parents children

            push(@{$current_node->{parent}->{children}}, \%attributes);

            $current_node->{node_depth} = $current_node->{parent}->{node_depth} + 1;
        }

        # Set node_tag attribute, so that a node knows what it is

        $current_node->{node_tag} = $element;

        # Call node handler if one exists

        if(defined($node_start_handler))
        {
            $node_start_handler->($expat, $element, $current_node, $superhash);
        }
    }
    else
    {
        # Non-node tag matched
        # Convert tag attributes to an attribute of the current node

        my $key;
        for $key (keys(%attributes))
        {
            $current_node->{"${element}_$key"} = $attributes{$key};
        }
    }
}

# Function to handle XML characters
sub charhandler($$)
{
    my ($expat, $data) = @_;

    # Suppress initial whitespace

    $data =~ s/^\s+/\n/g;

    # If characters are not just whitespace, then add them as attributes of current_node

    if($data ne "\n" && !($data =~ /^\s+$/))
    {
        my $element = $expat->current_element;
        if(!defined($node_tags->{$element}))
        {
            if(!defined($current_node->{$element}))
            {
                # Attribute does not yet exist, so create it with value of current data

                $current_node->{$element} = $data;
            }
            else
            {
                # Attribute exists, so append current data to it

                $current_node->{$element} .= $data;
            }
        }
        else
        {
            if(!defined($current_node->{node_text}))
            {
                # node_text attribute does not yet exist, so create it with value of current data

                $current_node->{node_text} = $data;
            }
            else
            {
                # node_text attribute exists, so append current data to it

                $current_node->{node_text} .= $data;
            }
        }
    }
}

# Function to handle XML end tags
sub endhandler($$)
{
    my ($expat, $element) = @_;

    # Add links to documentation

    adddoclinks($current_node);

    # Undefine children if there are zero children

    delete($current_node->{children}) if(defined($current_node->{children}) &&
                                         !scalar(@{$current_node->{children}}));

    if(defined($node_tags->{$element}))
    {
        # Call node handler if one exists

        if(defined($node_end_handler))
        {
            $node_end_handler->($expat, $element, $current_node, $superhash);
        }

        # Current node is now closed, so parent becomes current node

        $current_node = $current_node->{parent};
    }
}

# Function to add links to doc attribute of an node
sub adddoclinks($)
{
    my ($node) = @_;

    # Return immediately if the node has no doc attribute

    return if(!defined($node->{doc}));

    # Insert links into documentation

    # New links

    for my $linktype (keys(%{$doc_links}))
    {
        $node->{doc} =~ s!"\{$linktype (\w+)\}!"$doc_links->{$linktype}\#$1!g;
    }
    for my $linktype (keys(%{$doc_splinks}))
    {
        $node->{doc} =~ s!"\{$linktype (\w+)(.*?)\}!"$doc_splinks->{$linktype}$1.htm\#$1$2!g;
    }

    # Old style links

    for my $linktype (keys(%{$doc_links}))
    {
        $node->{doc} =~ s!{$linktype (\w+)}!<A href=\"$doc_links->{$linktype}\#$1\">$1</A>!g;
    }
    for my $linktype (keys(%{$doc_splinks}))
    {
        $node->{doc} =~ s!{$linktype (\w+)(.*?)}!<A href=\"$doc_splinks->{$linktype}$1.htm\#$1$2\">$1$2</A>!g;
    }
}

# Function to handle XML external entities
sub extenthandler($$$$)
{
    my ($expat, $base, $sysid, $pubid) = @_;

    #my $filename = "../xml/$sysid";
    my $filename = "../src/$sysid";

    open(FILE, "<$filename") or confess "Could not open file $filename\n";

    my $text = join("", <FILE>);

    close(FILE);

    return($text);
}

# Function to print a node and recurse for all of its children
sub printnode($$)
{
    my ($node, $node_identifier) = @_;

    # Print all of the node's contents

    print "\n";
    my $key;
    for $key (sort keys(%$node))
    {
        if($key eq "parent")
        {
            # Print parent identifier unless parent is a pseudoroot

            if(!defined($node->{parent}->{pseudoroot}))
            {
                print "parent = $node->{parent}->{$node_identifier}\n";
            }
        }
        elsif($key eq "children")
        {
            print "children = ";
            my $child;
            for $child (@{$node->{children}})
            {
                print "$child->{$node_identifier} ";
            }
            print "\n";
        }
        else
        {
            print "$key = $node->{$key}\n";
        }
    }

    # Recurse for all of the node's children

    my $child;
    for $child (@{$node->{children}})
    {
        printnode($child, $node_identifier);
    }
}

# Function to populate a hash containing all nodes in a tree indexed to a defined attribute
sub populate_hash($$$)
{
    my ($hash, $root, $index_attribute) = @_;

    my $current_node = $root;

    # Add the node to the hash

    if(defined($current_node->{$index_attribute}) &&
       $current_node->{$index_attribute} ne "pseudoroot")
    {
        $hash->{$current_node->{$index_attribute}} = $current_node;
    }

    # Recurse for all of the node's children

    if(defined($current_node->{children}))
    {
        my $child;
        for $child (@{$current_node->{children}})
        {
            populate_hash($hash, $child, $index_attribute);
        }
    }
}

# EOF
