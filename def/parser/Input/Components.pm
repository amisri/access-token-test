#!/usr/bin/perl -w
#
# Name:     Input/Components.pm
# Purpose:  Provides functions relating to type components
# Author:   Stephen Page

package Input::Components;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "component")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(barcode))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check whether a component with the barcode already exists

        if(defined($superhash->{components}->{$current_node->{barcode}}))
        {
            $expat->xpcroak("Component with barcode $current_node->{barcode} already defined");
        }
        $superhash->{components}->{$current_node->{barcode}} = $current_node;

        # Check that references to properties are valid

        if(defined($current_node->{barcode_prop}))
        {
            my ($prop_a, $prop_b);

            if($current_node->{barcode_prop} =~ /\?/)
            {
                ($prop_a = $current_node->{barcode_prop}) =~ s/\?/A/g;
                ($prop_b = $current_node->{barcode_prop}) =~ s/\?/B/g;

                if(!defined($superhash->{properties}->{$prop_a}) &&
                   !defined($superhash->{properties}->{$prop_b}))
                {
                    $expat->xpcroak("Property is not valid");
                }
            }
            else
            {
                if(!defined($superhash->{properties}->{$current_node->{barcode_prop}}))
                {
                    $expat->xpcroak("Property is not valid");
                }
            }
        }

        if(defined($current_node->{temp_prop}))
        {
            my ($prop_a, $prop_b);

            if($current_node->{temp_prop} =~ /\?/)
            {
                ($prop_a = $current_node->{temp_prop}) =~ s/\?/A/g;
                ($prop_b = $current_node->{temp_prop}) =~ s/\?/B/g;

                if(!defined($superhash->{properties}->{$prop_a}) &&
                   !defined($superhash->{properties}->{$prop_b}))
                {
                    $expat->xpcroak("Property is not valid");
                }
            }
            else
            {
                if(!defined($superhash->{properties}->{$current_node->{temp_prop}}))
                {
                    $expat->xpcroak("Property is not valid");
                }
            }
        }

        # Convert categories into array

        if(defined($current_node->{categories}))
        {
            my @categories              = split(/\s+/, $current_node->{categories});
            $current_node->{categories} = \@categories;
        }
    }
    elsif($current_node->{node_tag} eq "components")
    {
        if(defined($current_node->{parent}))
        {
            $expat->xpcroak("components element must be at the top-level");
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Nothing to be done
}

# EOF
