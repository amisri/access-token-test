#!/usr/bin/perl -w
#
# Name:     Input/Asserts.pm
# Purpose:  Used to parse assert definitions
# Author:   Kevin Kessler

package Input::Asserts;

use Exporter;
use strict;
use Data::Dumper;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    return if($element ne "assert");

    # assert that lhs and rhs attributes are given
    my %attributes = %$current_node;
    for my $attribute (('lhs','rhs')) 
    {
        if(!exists($attributes{$attribute}))
        {
            $expat->xpcroak("Assert tag is missing the '$attribute' attribute\n");
        }
    }

    # check operator attribute
    my @supported_operators = ('==', '!=', '<=', '>=', '<', '>');
    my $operator_regex = '^('. join('|', @supported_operators) .')$';

    # operator attribute is optional and '==' by default
    if(!exists($attributes{operator}))
    {
        $current_node->{operator} = $supported_operators[0];
    }
    # if specified, the value must be one of the supported operators
    elsif($attributes{operator} !~ /$operator_regex/)
    {
        $expat->xpcroak("Invalid value for 'operator' attribute in assert tag. Supported operators: ".join(' ', @supported_operators)."\n");
    }
}

# EOF
