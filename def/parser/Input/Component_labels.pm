#!/usr/bin/perl -w
#
# Name:     Input/Component_labels.pm
# Purpose:  Provides functions to parse component label data
# Author:   Stephen Page

package Input::Component_labels;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "component_label")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(barcode label))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        $superhash->{component_labels}->{$current_node->{barcode}} = $current_node->{label};

        # Check whether a component with the barcode exists

        return if(!defined($superhash->{components}->{$current_node->{barcode}}));
        $superhash->{components}->{$current_node->{barcode}}->{label} = $current_node->{label};
    }
    elsif($current_node->{node_tag} eq "component_labels")
    {
        if(defined($current_node->{parent}))
        {
            $expat->xpcroak("component_labels element must be at the top-level");
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "component_labels")
    {
        # Check that all component types have labels

        for my $component (values(%{$superhash->{components}}))
        {
            if(!defined($component->{label}))
            {
                $expat->xpcroak("No label defined for component type $component->{barcode}");
            }
        }
    }
}

# EOF
