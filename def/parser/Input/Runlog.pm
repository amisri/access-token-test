#!/usr/bin/perl -w
#
# Name:     Input/Runlog.pm
# Purpose:  Provides functions relating to the runlog
# Author:   Stephen Page

package Input::Runlog;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "entry")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(id label length))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Add prefix to id

        $current_node->{id} = "RL_$current_node->{id}";
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "entry")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(doc))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }
    }
}

# EOF
