#!/usr/bin/perl -w
#
# Name:     Input/Symlist.pm
# Purpose:  Provides functions relating to the FGC symbol lists
# Author:   Stephen Page

package Input::Symlist;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Handle different elements

    if($element eq "symlist")
    {
        # Return error if current node is not root node

        if(defined($current_node->{parent}))
        {
            $expat->xpcroak("symlist must be root node");
        }

        # Check that appropriate attributes are defined

        if(!defined($current_node->{consts}))
        {
            $current_node->{consts} = $current_node->{children};
        }
    }
    elsif($element eq "const")
    {
        # Return error if current node is the root node

        if(!defined($current_node->{parent}))
        {
            $expat->xpcroak("const must not be root node");
        }

        # Return error if parent node is not a symlist

        if($current_node->{parent}->{node_tag} ne "symlist")
        {
            $expat->xpcroak("const has wrong parent node type");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(name title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Const is settable unless otherwise specified

        if(!defined($current_node->{settable}))
        {
            $current_node->{settable} = 1;
        }

        # Handle value if defined

        if(defined($current_node->{value}))
        {
            # Convert value to decimal if it is in hex

            if($current_node->{value} =~ /^0x\w+/) # Value is in hex
            {
                $current_node->{value} =~ s/(0x\w+)/hex($1)/e;
            }
        }
    }

    # Set meaning

    if(defined($current_node->{meaning}))
    {
        # Check that meaning is one of the accepted values

        if($current_node->{meaning} ne 'NONE'   &&
           $current_node->{meaning} ne 'OFF'    &&
           $current_node->{meaning} ne 'ON'     &&
           $current_node->{meaning} ne 'ERROR'  &&
           $current_node->{meaning} ne 'WARNING')
        {
            $expat->xpcroak("$current_node->{meaning} is not a valid meaning");
        }
    }
    else
    {
        if(defined($current_node->{parent}))
        {
            $current_node->{meaning} = $current_node->{parent}->{meaning};
        }
        else
        {
            $current_node->{meaning} = 'NONE';
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Handle different elements

    if($element eq "symlist")
    {
        # If symlist has documentation, then all constants within the list should also have documentation
        # and the title will be used if the documentation is not defined.
        # If symlist has no documentation, then neither should the constants within the list

        if(defined($current_node->{doc}))
        {
            # Check that constants all have documentation

            for my $constant (@{$current_node->{consts}})
            {
                if(!defined($constant->{doc}))
                {
                    # If no doc provided then use the title instead

                    $constant->{doc} = $constant->{title};
                }
            }
        }
        else
        {
            # Check that no constants have documentation

            for my $constant (@{$current_node->{consts}})
            {
                if(defined($constant->{doc}))
                {
                    $expat->xpcroak("Constant $constant->{name} may not have doc defined");
                }
            }
        }
    }
    elsif($element eq "const")
    {
        if(!defined($current_node->{parent}->{consts}))
        {
            $current_node->{parent}->{consts} = [];
        }

        push(@{$current_node->{parent}->{consts}}, $current_node);
    }
}

# EOF
