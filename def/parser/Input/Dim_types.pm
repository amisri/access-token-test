#!/usr/bin/perl -w
#
# Name:    Input/Dim_types.pm
# Author:  Stephen Page
# Purpose: Provides functions relating to DIM types
#

package Input::Dim_types;

use strict;
use Exporter;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %start_tag_handlers = (
                            "ana_channel"   => \&start_ana_channel_tag_handler,
                            "dig_bank"      => \&start_dig_bank_tag_handler,
                            "dim_type"      => \&start_dim_type_tag_handler,
                            "input"         => \&start_input_tag_handler,
                         );

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check whether a handler has been defined for the tag

    if(defined($start_tag_handlers{$current_node->{node_tag}}))
    {
        # Handler has been defined, call it

        $start_tag_handlers{$current_node->{node_tag}}($expat, $element, $current_node, $superhash);
    }
    else
    {
        # No handler defined for tag, return error

        $expat->xpcroak("No start tag handler for tag $current_node->{node_tag}");
    }
}

sub start_ana_channel_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that parent is a dim_type element

    if(!defined($current_node->{parent}) ||
       $current_node->{parent}->{node_tag} ne "dim_type")
    {
        $expat->xpcroak("Parent must be dim_type");
    }

    # Check that appropriate attributes are defined

    foreach my $attribute (qw(index label min_0V max_5V units))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check the appropriate length and that have not spaces
    if ($current_node->{label} =~ /\s/)
    {
        $expat->xpcroak("Label $current_node->{label} have at least one white space");
    }

    if (length($current_node->{label}) > 13)
    {
        $expat->xpcroak("Label $current_node->{label} is longer than 13");
    }

    # Capitalise label

    $current_node->{label} = uc($current_node->{label});

    # Convert max and min to gain and offset

    $current_node->{gain}   = ($current_node->{max_5V} - $current_node->{min_0V}) / 4096;
    $current_node->{offset} = $current_node->{min_0V};

    # Add current node to parent's analogue channels

    $current_node->{parent}->{ana_channels}->[$current_node->{index}] = $current_node;
}

sub start_dig_bank_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that parent is a dim_type element

    if(!defined($current_node->{parent}) ||
       $current_node->{parent}->{node_tag} ne "dim_type")
    {
        $expat->xpcroak("Parent must be dim_type");
    }

    # Check that appropriate attributes are defined

    foreach my $attribute (qw(index))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Add current node to parent's digital banks

    $current_node->{parent}->{dig_banks}->[$current_node->{index}] = $current_node;
}

sub start_dim_type_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that this is the root node

    if(defined($current_node->{parent}))
    {
        $expat->xpcroak("Diag type must be root node");
    }

    # Check that appropriate attributes are defined

    foreach my $attribute (qw(comp position variant))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check that type name is a known component barcode

    $current_node->{type} = "$current_node->{comp}-$current_node->{position}$current_node->{variant}";
    $current_node->{comp} = $superhash->{component_labels}->{$current_node->{comp}};
    $expat->xpcroak("Unknown component") if(!defined($current_node->{comp}));

    $superhash->{dim_types}->{"$current_node->{type}"} = $current_node;
}

sub start_input_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that parent is a dig_bank element

    if(!defined($current_node->{parent}) ||
       $current_node->{parent}->{node_tag} ne "dig_bank")
    {
        $expat->xpcroak("Parent must be dig_bank");
    }

    # Check that appropriate attributes are defined

    foreach my $attribute (qw(fault index label one zero))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Set case for label, one and zero

    $current_node->{label}  = uc($current_node->{label});
    $current_node->{one}    = uc($current_node->{one});
    $current_node->{zero}   = lc($current_node->{zero});

    # Add current node to parent's inputs

    $current_node->{parent}->{inputs}->[$current_node->{index}] = $current_node;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
