#!/usr/bin/perl -w
#
# Name:     Input/Function.pm
# Purpose:  Provides functions relating to the FGC functions
# Author:   Stephen Page

package Input::Function;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Check that appropriate attributes are defined

    for my $attribute (qw(name title))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check that types within this node are valid

    if(defined($current_node->{types}))
    {
        my @types;
        for my $type (split(/\s+/, $current_node->{types}))
        {
            if(defined($superhash->{types}->{$type}))
            {
                push(@types, $superhash->{types}->{$type});
            }
            else
            {
                $expat->xpcroak("$type is not a valid type");
            }
        }
        $current_node->{types} = \@types;
    }
    elsif($current_node->{node_tag} eq "get" ||
          $current_node->{node_tag} eq "set")
    {
        # Get and set functions must have types

        $expat->xpcroak("$current_node->{node_tag} function $current_node->{name} has no types");
    }

    # Check that 'get options' within this node are valid

    if(defined($current_node->{getopts}))
    {
        my @getopts;
        for my $getopt (split(/\s+/, $current_node->{getopts}))
        {
            if(defined($superhash->{getoptions}->{$getopt}))
            {
                push(@getopts, $superhash->{getoptions}->{$getopt});
            }
            else
            {
                $expat->xpcroak("$getopt is not a valid get option");
            }
        }
        $current_node->{getopts} = \@getopts;
    }
    elsif($current_node->{node_tag} eq "get")
    {
        # Get functions must have 'get options'

        $expat->xpcroak("Get function $current_node->{name} has no get options");
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Check that doc is defined

    if(!defined($current_node->{doc}))
    {
        $expat->xpcroak("$current_node->{node_tag} function $current_node->{name} has no doc");
    }
}

# EOF
