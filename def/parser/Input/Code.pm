#!/usr/bin/perl -w
#
# Name:     Input/Code.pm
# Purpose:  Provides functions relating to the FGC Codes
# Author:   Stephen Page

package Input::Code;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my $id     = 0;
my %code_names;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{node_tag} eq "codes")
    {
    }
    elsif($current_node->{node_tag} eq "code")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(id name size_kb title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        $current_node->{full_name} = defined($current_node->{class})                ?
                                     "$current_node->{class}_$current_node->{name}" :
                                     $current_node->{name};

        $current_node->{code_dir_name} = sprintf( "C%03d_$current_node->{full_name}", $current_node->{id} );

        # Check for a duplicate code name

        if(defined($code_names{$current_node->{full_name}}))
        {
            $expat->xpcroak("Code name $current_node->{full_name} is duplicated");
        }

        $code_names{$current_node->{full_name}} = 1;

        # Check that ID matches expected

        if($current_node->{id} != $id)
        {
            $expat->xpcroak("Code ID $current_node->{id} does not match expected $id");
        }
        $id++;
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
