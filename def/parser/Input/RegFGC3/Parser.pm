#!/usr/bin/perl -w
#
# Name:     Input/RegFGC3/Parser.pm
# Purpose:  Provides functions to parse RegFGC3 definitions

package Input::RegFGC3::Parser;

use Exporter;
use File::Slurp qw(read_dir);
use File::Spec::Functions qw( catfile );
use Input::RegFGC3::Board;
use Input::RegFGC3::Boards;
use Input::RegFGC3::Converters;
use Input::RegFGC3::Crates;
use Output::Doc::Writer;
use Parser;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);


# Begin functions

sub parse($$$$)
{
    my ($projpath, $superhash, $roots, $regfgc3_dir) = @_;

    # Parse RegFGC3 converter definition

    $superhash->{regfgc3}->{converters} = {};
    my @regfgc3_converter_files         = <$projpath/def/src/regfgc3/converters/*.xml>;

    for my $converter_file (@regfgc3_converter_files)
    {
        Input::RegFGC3::Converters::parse($projpath, $superhash, $roots, $converter_file);
    }


    # Parse RegFGC3 board definition

    $superhash->{regfgc3}->{boards} = {};
    my @regfgc3_board_dirs = grep {-d} glob("$projpath/def/src/regfgc3/boards/*");
   
    for my $board_dir (@regfgc3_board_dirs)
    {
        Input::RegFGC3::Board::parse($projpath, $superhash, $roots, $board_dir);
    }


    # Parse RegFGC3 boards definition

    Input::RegFGC3::Boards::parse($projpath, $superhash, $roots, <$projpath/def/src/regfgc3/boards/boards.xml>);

    print "********** Parsing of RegFGC3 completed successfully **********\n\n";

}

# EOF
