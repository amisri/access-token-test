#!/usr/bin/perl -w
#
# Name:     Input/RegFGC3/Board/Parser.pm
# Purpose:  Provides functions to parse RegFGC3 definitions

package Input::RegFGC3::Boards;

use Exporter;
use Output::Doc::Writer;
use Parser;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);

# Merge hash of hashes, appending hr2 values to hr1, excluding given keys
sub merge_hash_of_hash($$;@)
{
    my ($hr1, $hr2, @exclude_keys) = @_;
    
    while (my($hr2_k,$hr2_v) = each %$hr2) 
    {
        @{$hr1->{$hr2_k}}{keys %$hr2_v} = values %$hr2_v;

        delete(@{$hr1->{$hr2_k}}{@exclude_keys});
        
    }
}


# Begin functions

sub parse($$$$)
{
    my ($projpath, $superhash, $roots, $boards_file) = @_;

#    print "********** Beginning parsing RegFGC3 board $boards_file ... *********\n";

    my $boards = Parser::parse(
                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                    file                => "$boards_file",
                                    node_tags           => { board_list => 1, board_type => 1, barcode => 1},
                                    node_start_handler  => \&Input::RegFGC3::Boards::start_tag_handler,
                                    node_end_handler    => \&Input::RegFGC3::Boards::end_tag_handler,
                                    superhash           => $superhash,
                               );

    my $boards_doc = {};
    Parser::populate_hash($boards_doc, $boards, "name");

    merge_hash_of_hash($superhash->{regfgc3}->{boards}, $boards_doc, ('node_depth', 'node_tag', 'parent'));
    
#    print "********** Parsing of RegFGC3 board $boards_file completed successfully **********\n\n";
}


sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

     # Handle different elements

    if($element eq "board_list")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }
    }
    elsif($element eq "board_type")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(name))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }
        
        # Check the node's parent

        if(!defined($current_node->{parent}) ||
           $current_node->{parent}->{node_tag} ne "board_list")
        {
            $expat->xpcroak("The parent must be a board_list but is $current_node->{parent}->{node_tag}");
        }
        
        # Check that the board type corresponds to a known type defined in regfgc3 board symlist
        # Add reference to this symlist
        
        my @board_type_symlist = @{$superhash->{symlists}->{REGFGC3_BOARDS}->{consts}};
        my ($board_symbol) = grep($_->{symbol} eq $current_node->{name}, @board_type_symlist);
        
        if(!defined($board_symbol))
        {
            confess "ERROR Value for board $current_node->{name} not defined in symlist\n";
        }
        $current_node->{board_symbol} = $board_symbol;
    
    }
    elsif($element eq "barcode")
    {

        # Check that the node's parent

        if(!defined($current_node->{parent}) ||
           $current_node->{parent}->{node_tag} ne "board_type")
        {
            $expat->xpcroak("The parent must be a board_type");
        }
        
    }
    else # Unhandled element
    {
        $expat->xpcroak("Unhandled element \"$element\"");
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    
    if($element eq "board_type")
    {

        for my $barcode (@{$current_node->{children}})
        {
            if (defined($barcode->{node_text}) && $barcode->{node_text} ne "")
            {
                $barcode->{node_text} =~ tr/\n\t//d;
                
                if(!defined($superhash->{components}->{$barcode->{node_text}}))
                {
                    $expat->xpcroak("Component with barcode $barcode->{node_text} does not exist");
                }
                $superhash->{components}->{$barcode->{node_text}}->{regfgc3_def} = $current_node;
            }

        }

    }
}

# EOF
