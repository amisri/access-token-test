#!/usr/bin/perl -w
#
# Name:     Input/RegFGC3/Board/Variant.pm
# Purpose:  Provides functions related to the definition of a RegFGC3 board API

package Input::RegFGC3::Board::Variant;

use Exporter;
use strict;
use Carp;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

     # Handle different elements

    if($element eq "element")
    {
        # Check that appropriate attributes are defined

        foreach my $attribute (qw(name type size_bits))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check that the size in bits is in the valid range

        if($current_node->{size_bits} < 1 ||
           $current_node->{size_bits} > 32)
        {
            $expat->xpcroak("size_bits must be in the range 1-32");
        }

        # Check that the type is valid

        if($current_node->{type} ne "float"     &&
           $current_node->{type} ne "int8_t"    &&
           $current_node->{type} ne "int16_t"   &&
           $current_node->{type} ne "int32_t"   &&
           $current_node->{type} ne "uint8_t"   &&
           $current_node->{type} ne "uint16_t"  &&
           $current_node->{type} ne "uint32_t")
        {
            $expat->xpcroak("Type \"$current_node->{type}\" is not valid");
        }

        # Set element number

        $current_node->{number} = scalar(@{$current_node->{parent}->{elements}});

        if($current_node->{number} > 30)
        {
            $expat->xpcroak("Too many elements");
        }

        # If the element is within a write block, then check that it has a default value

        if($current_node->{parent}->{type} eq "Write")
        {
            # Check that the default value is in hexadecimal format, then decode it

            if($current_node->{default_value} !~ /^0x[0-9A-F]+$/i)
            {
                $expat->xpcroak("Default value must be in hexadecimal");
            }
            $current_node->{default_value} = hex($current_node->{default_value});
        }

        # Check that symlist of this node is valid
        # if not a symlist we expect a scaling_gain + scaling_offset.
        
        if(defined($current_node->{symlist}))
        {
            if(defined($superhash->{symlists}->{$current_node->{symlist}}))
            {
                $current_node->{symlist} = $superhash->{symlists}->{$current_node->{symlist}};
            }
            else
            {
                $expat->xpcroak("$current_node->{symlist} is not a valid symlist");
            }
            
            if(defined($current_node->{scaling_gain}) || defined($current_node->{scaling_offset} ))
            {
                $expat->xpcroak("Parameter $current_node->{name} : symlist and scaling are exclusive");
            }
        }
        elsif(defined($current_node->{scaling_gain}) && defined($current_node->{scaling_offset} ))
        {
            if ($current_node->{scaling_gain} == 0)
            {
                $expat->xpcroak("Parameter $current_node->{name} gain is zero");
            }
        }
        else
        {
            $current_node->{scaling_gain} = 1;
            $current_node->{scaling_offset} = 0;
            $current_node->{unit} = "?";
            
            # ToDo croak instead of setting default values when EPCCCS-2731 is done
#            $expat->xpcroak("Parameter $current_node->{name} is neither a symlist nor has gain/offset/unit");
        }
        
        # if limits min, max, check as for properties

        if(defined($current_node->{limits}))
        {
            $expat->xpcroak("Min limit not defined") if(!defined($current_node->{min}));
            $expat->xpcroak("Max limit not defined") if(!defined($current_node->{max}));
            $expat->xpcroak("Unknown limit type") if(($current_node->{limits} ne "int") && ($current_node->{limits} ne "float"));
        }

        # Add the element to the containing block
        
        $current_node->{parent}->{elements}->[$current_node->{number}] = $current_node;
    }
    elsif($element eq "read_block" || $element eq "write_block")
    {
        # Check that appropriate attributes are defined

        foreach my $attribute (qw(number))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check that the node's parent is a variant

        if(!defined($current_node->{parent}) ||
           $current_node->{parent}->{node_tag} ne "variant")
        {
            $expat->xpcroak("The parent of a write_block must be a variant");
        }

        # Check that the block number is valid

        if($current_node->{number} !~ /^\d+$/ ||
           $current_node->{number} <  0       ||
           $current_node->{number} >  6)
        {
            $expat->xpcroak("Block number must be in the range 0-6");
        }

        # if critical is defined, check 0 or 1. if not defined, set as 0.

        if (!defined($current_node->{critical}))
        {
            $current_node->{critical} = 0;
        }
        elsif ($current_node->{critical} != 0 && $current_node->{critical} != 1)
        {
            $expat->xpcroak("$current_node->{critical} n0t valid for attribute critical (should be 0 or 1)");
        }

        # Initialise element array

        $current_node->{elements} = [];

        # Handle read or write block

        if($element eq "read_block")
        {
            # Set the block type

            $current_node->{type} = "Read";

            # Add the block to the variant

            if(defined($current_node->{parent}->{read_blocks}->[$current_node->{number}]))
            {
                $expat->xpcroak("Duplicate block number $current_node->{number}");
            }
            $current_node->{parent}->{read_blocks}->[$current_node->{number}] = $current_node;
        }
        else # $element eq "write_block"
        {
            # Set the block type

            $current_node->{type} = "Write";
            
            # Add the block to the variant

            if(defined($current_node->{parent}->{write_blocks}->[$current_node->{number}]))
            {
                $expat->xpcroak("Duplicate block number $current_node->{number}");
            }
            $current_node->{parent}->{write_blocks}->[$current_node->{number}] = $current_node;
        }
    }
    elsif($element eq "variant")
    {   
        # Check that the variant is at the top-level

        if(defined($current_node->{parent}))
        {
            $expat->xpcroak("Variant must be the top-level element");
        }

        # Initialise block arrays

        $current_node->{read_blocks}    = [];
        $current_node->{write_blocks}   = [];
    }
    else # Unhandled element
    {
        $expat->xpcroak("Unhandled element \"$element\"");
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
