#!/usr/bin/perl -w
#
# Name:     Input/RegFGC3/Board/Parser.pm
# Purpose:  Provides functions to parse RegFGC3 definitions

package Input::RegFGC3::Board;

use Carp;
use Exporter;
use Input::RegFGC3::Board::Variant;
use Output::Doc::Writer;


use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);


# Begin functions

sub parse($$$$)
{
    my ($projpath, $superhash, $roots, $board_dir) = @_;

    # Derive board type code from file name

    my $board_dir_name =  $board_dir;
    $board_dir_name    =~ s/.*\/(.*)/$1/;

    my @variant_files = grep {-f} glob("$board_dir/*/*/*.xml");

    foreach my $variant_file (@variant_files)
    {
        # Extract data of interest from file
        my ($board, $device, $variant, $xml_file) = ($variant_file =~ /.*\/boards\/(\w+)\/(\w+)\/(\w+)\/(\w+)\.xml/);
        my $api_version = (split('_', $xml_file))[1];
        
        # Check data against symlists
        # Boards
        my @board_type_symlist = @{$superhash->{symlists}->{REGFGC3_BOARDS}->{consts}};
        my ($board_symbol)     =  grep($_->{symbol} eq $board, @board_type_symlist);

        if(!defined($board_symbol))
        {
            confess "ERROR: Value for board $board not defined in symlist regfgc3_boards.xml\n";
        }

        # Devices
        my @devices_symlist = @{$superhash->{symlists}->{REGFGC3_DEVICES}->{consts}};
        my ($device_symbol) = grep($_->{symbol} eq $device, @devices_symlist);

        if(!defined($device_symbol))
        {
            confess "ERROR: Value for device $device not defined in symlist regfgc3_devices.xml\n";
        }

        # Variants
        my @variant_symlist = @{$superhash->{symlists}->{REGFGC3_VARIANTS}->{consts}};
        my ($variant_symbol)  = grep($_->{symbol} eq $variant, @variant_symlist);
        
        if(!defined($variant_symbol))
        {
            confess "ERROR: Value for $variant not defined in symlist regfgc3_variants.xml\n";
        }

        $superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}->{$variant}->{variant_symbol} = $variant_symbol;

        my $variant_obj = Parser::parse(
                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                        file                => $variant_file,
                                        node_tags           => { element => 1, read_block => 1, write_block => 1, variant => 1 },
                                        node_start_handler  => \&Input::RegFGC3::Board::Variant::start_tag_handler,
                                        node_end_handler    => \&Input::RegFGC3::Board::Variant::end_tag_handler,
                                        superhash           => $superhash,
                                   );

        $superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}->{$variant}->{$api_version} = $variant_obj;
    }
}

# EOF
