#!/usr/bin/perl -w
#
# Name:     Input/RegFGC3/Converters.pm
# Purpose:  Provides functions to parse RegFGC3 definitions

package Input::RegFGC3::Converters;

use Carp;
use Exporter;
use Output::Doc::Writer;
use Parser;

use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);


# Begin functions

sub parse ($$$$)
{
    my ($projpath, $superhash, $roots, $converter_file) = @_;

    # Derive converter code from file name

    my $converter_file_name =  $converter_file;
    $converter_file_name    =~ s/.*\/(.*)/$1/;

    # Parse converter
    my $converter = Parser::parse(
                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                    file                => $converter_file,
                                    node_tags           => { crate => 1, board => 1, device => 2 },
                                    node_start_handler  => \&Input::RegFGC3::Converters::start_tag_handler,
                                    node_end_handler    => \&Input::RegFGC3::Converters::end_tag_handler,
                                    superhash           => $superhash,
                               );


    # Check file name is consistent with type in the xml
    my $converter_name = $converter_file;
    $converter_name   =~ s/.*\/(.*).xml/$1/;
    
    if($converter_name ne $converter->{type})
    {
        confess "ERROR File name $converter_name does not match $converter->{type}\n";
    }
    
    # Create hash with boards
    # check that 2 boards of the same type share the same variant
    # merge the boards of same type.
    
    $converter->{boards} = {};

    foreach my $board (@{$converter->{children}})
    {
        if (defined($converter->{boards}->{$board->{name}}))
        {
            push (@{$converter->{boards}->{$board->{name}}->{slots}}, $board->{slot});
        }
        else
        {
            $board->{slots} = ["$board->{slot}"];
            delete ($board->{slot});
            $converter->{boards}->{$board->{name}} = $board;
        }
    }

    delete ($converter->{children});

    # Save the parsed converter in converters hash table
    $superhash->{regfgc3}->{converters}->{$converter->{type}} = $converter;    
}



sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

     # Handle different elements
    if($element eq "board")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(slot name))
        {
            if(!defined($current_node->{$attribute}))
            {
                confess "ERROR: Attribute $attribute not defined in node of type $element";
            }
        }
        
        # Check that the size in bits is in the valid range

        if($current_node->{slot} < 1 || $current_node->{slot} > 31)
        {
            confess "ERROR: slot number of out limits (must be in the range 1-31)";
        }
        
        # Check that the node's parent is a crate

        if(!defined($current_node->{parent}) || $current_node->{parent}->{node_tag} ne "crate")
        {
            confess "ERROR: Parent of node 'board' must be a crate";
        }
        
        # Check that the board type corresponds to a known type defined in regfgc3 board symlist
        # Add reference to this symlist
        my @board_type_symlist = @{$superhash->{symlists}->{REGFGC3_BOARDS}->{consts}};
        my ($board_symbol)     = grep($_->{symbol} eq $current_node->{name}, @board_type_symlist);
        
        if(!defined($board_symbol))
        {
            confess "ERROR: Value for board $current_node->{name} not defined in regfgc3_boards.xml\n";
        }
        $current_node->{board_symbol} = $board_symbol;
        
    }
    elsif($element eq "device")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(name variant))
        {
            if(!defined($current_node->{$attribute}))
            {
                confess "ERROR: Attribute of node 'device' $attribute not defined";
            }
        }

        # Check that the node's parent is a board
        if(!defined($current_node->{parent}) || $current_node->{parent}->{node_tag} ne "board")
        {
            confess "ERROR: Parent of node 'device' must be a board";
        }
        
        # Check device's name validity against symlist
        my @device_symlist  = @{$superhash->{symlists}->{REGFGC3_DEVICES}->{consts}};
        my ($device_symbol) = grep($_->{symbol} eq uc $current_node->{name}, @device_symlist);

        if(!defined($device_symbol))
        {
            confess "ERROR: Value for device $current_node->{name} not defined in symlist regfgc3_devices.xml\n";
        }

        # Check that the variant type corresponds to a known type defined in regfgc3 variant symlist
        # Add reference to this symlist        
        my @variant_symlist = @{$superhash->{symlists}->{REGFGC3_VARIANTS}->{consts}};
        my ($variant_symbol) = grep($_->{symbol} eq $current_node->{variant}, @variant_symlist);
        
        if(!defined($variant_symbol))
        {
            confess "Value for $current_node->{variant} not defined in symlist regfgc3_variants.xml\n";
        }

        $current_node->{variant_symbol} = $variant_symbol;
    }
    elsif($element eq "crate")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(title barcode))
        {
            if(!defined($current_node->{$attribute}))
            {
                confess "ERROR: Attribute in node 'crate' $attribute not defined";
            }
        }

        # check that barcode is valid, extract type from barcode
        
        if ($current_node->{barcode} =~ m/^HC([A-Z]{5})___$/)
        {
            $current_node->{type} = $1;
        }
        else
        {
            confess "ERROR: Invalid barcode $current_node->{barcode} in node of type 'crate'";
        }
        
        # Check that the crate is at the top-level
        if(defined($current_node->{parent}))
        {
            confess "ERROR: converter must be the top-level element";
        }

    }
    else # Unhandled element
    {
        confess "ERROR: Unhandled element \"$element\"";
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
