#!/usr/bin/perl -w
#
# Name:     Input/Class/Parser.pm
# Purpose:  Provides functions to parse class definitions
# Author:   Stephen Page

package Input::Class::Parser;

use Exporter;
use Input::Asserts;
use Input::Class::Consts;
use Input::Class::Main;
use Input::Class::Pubdata;
use Input::Class::Properties;
use Input::Symlist;
use Output::Doc::Writer;
use Parser;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);


# Begin functions

sub parse($$$$)
{
    my ($projpath, $superhash, $roots, $class_dir) = @_;

    # Derive class ID from class directory name

    my $id  = $class_dir;
    $id     =~ s#.*/(.*)#$1#;

    print "********** Beginning parsing class $id **********\n\n";

    # Check that the main definition for the class exists

    if(! -f "$class_dir/main.xml")
    {
        confess "$class_dir\\main.xml(9999): ERROR Main definition file for class with ID $id does not exist\n";
    }

    # Parse the main definition for the class

    my $class_node =    Parser::parse(
                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                        file                => "$class_dir/main.xml",
                                        node_tags           => { area => 1, class => 1, link => 1 },
                                        node_start_handler  => \&Input::Class::Main::start_tag_handler,
                                        node_end_handler    => \&Input::Class::Main::end_tag_handler,
                                        superhash           => $superhash,
                                     );
    $class_node->{id}               = $id;
    $superhash->{classes}->{$id}    = $class_node;

    # Add current class reference to superhash temporarily for reference when parsing

    $superhash->{current_class} = $class_node;

    # Add reference to class in platform node

    $class_node->{platform}->{classes}->{$class_node->{id}} = $class_node;

    # Check that the consts definition for the class exists

    if(! -f "$class_dir/consts.xml")
    {
        $class_dir =~ s#/#\\#g;
        $class_dir =~ s/\n//g;
        confess "$class_dir\\consts.xml(9999): ERROR Consts definition file for class with ID $id does not exist\n";
    }

    # Parse the consts definition for the class

    $class_node->{consts} = Parser::parse(
                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                            file                => "$class_dir/consts.xml",
                                            node_tags           => { consts => 1, assert => 0, asserts => 0 },
                                            node_start_handler  => \&Input::Class::Consts::start_tag_handler,
                                            node_end_handler    => \&Input::Class::Consts::end_tag_handler,
                                            superhash           => $superhash,
                                         );

    # Remove the special attributes from class's consts

    delete($class_node->{consts}->{node_depth});
    delete($class_node->{consts}->{node_tag});

    # Add undefined consts from platform if defined

    if(defined $class_node->{platform}->{consts})
    {
        for my $const (keys(%{$class_node->{platform}->{consts}}))
        {
            if(!defined($class_node->{consts}->{$const}))
            {
                $class_node->{consts}->{$const} = $class_node->{platform}->{consts}->{$const};
            }
        }
    }

    # Parse the asserts definitions for the class

    $class_node->{asserts} = Parser::parse(
                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                            file                => "$class_dir/consts.xml",
                                            node_tags           => { assert => 1, asserts => 1 },
                                            node_start_handler  => \&Input::Asserts::start_tag_handler,
                                            node_end_handler    => \&Input::Asserts::end_tag_handler,
                                            superhash           => $superhash,
                                         );

    $class_node->{asserts} = $class_node->{asserts}->{children};
    for my $assert (@{$class_node->{asserts}}) 
    {
        delete($assert->{node_depth});
        delete($assert->{node_tag});
        delete($assert->{parent});
    }

    # Get a list of symlist definition files

    my @symlist_def_files = <$class_dir/symlists/*.xml>;

    # Parse the symlist definition files

    $class_node->{symlists} = {};

    for my $symlist_def_file (@symlist_def_files)
    {
        my $symlist_name    =  $symlist_def_file;
        $symlist_name       =~ s#.*/(.*)\.xml#$1#;

        # Capitalise symlist name

        $symlist_name =~ s/(\w+)/\U$1/g;

        $class_node->{symlists}->{$symlist_name} = Parser::parse(
                                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                    file                => $symlist_def_file,
                                                                    node_tags           => { symlist => 1, const => 1 },
                                                                    node_start_handler  => \&Input::Symlist::start_tag_handler,
                                                                    node_end_handler    => \&Input::Symlist::end_tag_handler,
                                                                    superhash           => $superhash,
                                                                );

        my $symlist = $class_node->{symlists}->{$symlist_name};

        # Set name of symlist

        $symlist->{name} = $symlist_name;

        if(defined($superhash->{symlists}->{$symlist_name}))
        {
            confess "$class_dir\\symlists\\symlist_name.xml(9999): ERROR Symlist $symlist_name defined both globally and for class $id\n";
        }

        # Add symlist name as prefix of constants in symlist

        for my $const (@{$symlist->{consts}})
        {
            $const->{symbol}    = $const->{name};
            $const->{name}      = "${symlist_name}_$const->{symbol}";
        }
    }

    # Check that the properties definition for the class exists

    if(! -f "$class_dir/properties.xml")
    {
        $class_dir =~ s#/#\\#g;
        $class_dir =~ s/\n//g;
        confess "$class_dir\\properties.xml(9999): ERROR Properties definition file for class with ID $id does not exist\n";
    }

    # Parse the properties definition for the class

    my $class_properties_root = Parser::parse(
                                                doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                file                => "$class_dir/properties.xml",
                                                node_start_handler  => \&Input::Class::Properties::start_tag_handler,
                                                node_end_handler    => \&Input::Class::Properties::end_tag_handler,
                                                superhash           => $superhash,
                                             );
    $class_node->{properties}       = $class_properties_root->{properties};
    $class_node->{property_levels}  = $class_properties_root->{property_levels};

    # Parse the published data map for the class

    if(-f "$class_dir/pubdata.xml")
    {
        $class_node->{pubdataroot} = Parser::parse(
                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                    file                => "$class_dir/pubdata.xml",
                                                    node_start_handler  => \&Input::Class::Pubdata::start_tag_handler,
                                                    node_end_handler    => \&Input::Class::Pubdata::end_tag_handler,
                                                    superhash           => $superhash,
                                                  );

        # Create a flat hash of all map nodes

        $class_node->{pubdata} = {};
        Parser::populate_hash($class_node->{pubdata}, $class_node->{pubdataroot}, "fullname");
    }

    # Construct a hash of all symlists used by this class

    my %class_symlists;
    for my $property (@{$class_node->{properties}})
    {
        if(defined($property->{symlist}))
        {
            $class_symlists{$property->{symlist}->{name}} = $property->{symlist};
        }
        elsif(defined($property->{class_symlist}))
        {
            $class_symlists{$property->{class_symlists}->{$id}->{name}} = $property->{class_symlists}->{$id};
        }
    }
    $class_node->{class_symlists} = \%class_symlists;

    # Construct a hash of all constants within properties in this class

    for my $symlist (values(%class_symlists))
    {
        for my $const (@{$symlist->{consts}})
        {
            # Add const value to global constants if set

            if(defined($const->{value}))
            {
                if(defined($superhash->{consts}->{$const->{name}}))
                {
                    confess "NOFILE.xml(9999): ERROR Constant $const->{symbol} has values in both symlist $symlist->{name} and global consts\n";
                }
                elsif(defined($class_node->{consts}->{$const->{name}}))
                {
                    confess "NOFILE.xml(9999): ERROR Symbol $const->{symbol} has values in both symlist $symlist->{name} and class $id consts\n";
                }

                if($const->{value} =~ /^\*(.*)\.(.*)_(.*?)$/)
                {
                    my $memmap_name     = $1;
                    my $memzone_name    = $2;
                    my $const_type      = $3;

                    # Locate memzone

                    my @memzone_tree    = split("_", $memzone_name);
                    my $memzone         = $class_node->{platform}->{memmaps}->{$memmap_name};

                    my $target_name;
                    while(($target_name = shift(@memzone_tree)))
                    {
                        my $found = 0;
                        for my $child (@{$memzone->{children}})
                        {
                            if($child->{name} eq $target_name)
                            {
                                $memzone    = $child;
                                $found      = 1;
                                last;
                            }
                        }

                        if(!$found)
                        {
                            confess "NOFILE.xml(9999): ERROR Memzone $memzone_name not found for class $id symlist $symlist->{name} symbol $const->{symbol}\n";
                        }
                    }

                    if($const_type eq '16BIT')
                    {
                        # Truncate address to 16 bits

                        my $truncated_address   =  sprintf("%04X",  $memzone->{address} / 8);
                        $truncated_address      =~ s/^.*(.{4,4})$/$1/;

                        $class_node->{consts}->{$const->{name}} = sprintf("0x%s",   $truncated_address);
                    }
                    elsif($const_type eq '32BIT')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%08X", $memzone->{address} / 8);
                    }
                    elsif($const_type eq '24BIT')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%06X", $memzone->{address} / 8);
                    }
                    elsif($const_type eq 'PAGE')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%1X",  $memzone->{address} / 524288);
                    }
                    elsif($const_type eq 'MASK8')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%04X", $memzone->{mask});
                    }
                    elsif($const_type eq 'MASK16')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%04X", $memzone->{mask});
                    }
                    elsif($const_type eq 'MASK32')
                    {
                        $class_node->{consts}->{$const->{name}} = sprintf("0x%04X", $memzone->{mask});
                    }
                    else
                    {
                        confess "NOFILE.xml(9999): ERROR Invalid const type $const_type for symlist $symlist->{name} symbol $const->{symbol}\n";
                    }
                }
                else
                {
                    $class_node->{consts}->{$const->{name}} = $const->{value};
                }
            }

            # Set value symbol table constant to global constant value if defined, otherwise zero

            my $value = defined($superhash->{consts}->{$const->{name}})     ?
                        $superhash->{consts}->{$const->{name}}              :
                        defined($class_node->{consts}->{$const->{name}})    ?
                        $class_node->{consts}->{$const->{name}}             :
                        0;

            $class_node->{symtab_consts}->{$const->{name}} = { %$const, value => $value};
        }
    }

    # Check that all symlist constants are defined

    for my $const (values(%{$class_node->{symtab_consts}}))
    {
        if(!defined($class_node->{consts}->{$const->{name}}) &&
           !defined($superhash->{consts}->{$const->{name}}))
        {
            $class_dir =~ s#/#\\#g;
            $class_dir =~ s/\n//g;
            confess "$class_dir\\consts.xml(1): ERROR Value for const $const->{name} not defined\n";
        }
    }

    # Remove current class's reference from the superhash

    delete($superhash->{current_class});

    print "********** Parsing of class $id completed successfully **********\n\n";
}

# EOF
