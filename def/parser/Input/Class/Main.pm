#!/usr/bin/perl -w
#
# Name:     Input/Class/Main.pm
# Purpose:  Provides functions relating to the FGC classes' main definitions
# Author:   Stephen Page

package Input::Class::Main;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %class_names;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
        # Handle top-level area

        if($current_node->{parent}->{node_tag} eq "class") # Top-level area
        {
            $current_node->{parent}->{sitemap} = $current_node;
        }
        else # low-level area
        {
            # Check that appropriate attributes are defined

            for my $attribute (qw(name))
            {
                if(!defined($current_node->{$attribute}))
                {
                    $expat->xpcroak("$attribute not defined");
                }
            }
        }
    }
    elsif($element eq "class")
    {
        if(defined($current_node->{parent}))
        {
            $expat->xpcroak("class element must be at the top-level");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(name platform title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check for a duplicate class name

        if(defined($class_names{$current_node->{name}}))
        {
            $expat->xpcroak("Class name $current_node->{name} is duplicated");
        }
        else
        {
            $class_names{$current_node->{name}} = 1;
        }

        # Check that platform exists

        if(!defined($superhash->{platforms}->{$current_node->{platform}}))
        {
            $expat->xpcroak("Platform $current_node->{platform} does not exist");
        }

        # Replace platform name with reference to platform

        $current_node->{platform} = $superhash->{platforms}->{$current_node->{platform}};
    }
    elsif($element eq "link")
    {
        if($current_node->{parent}->{node_tag} ne "area")
        {
            $expat->xpcroak("link must be a child of an area");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(name url))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Add link to parent's links

        push(@{$current_node->{parent}->{links}}, $current_node);
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
    }
    elsif($element eq "class")
    {
        # Check that doc is defined

        if(!defined($current_node->{doc}))
        {
            $expat->xpcroak("Class $current_node->{name} has no doc");
        }
    }
    elsif($element eq "link")
    {
        # Remove from parent's children

        pop(@{$current_node->{parent}->{children}});
    }
}

# EOF
