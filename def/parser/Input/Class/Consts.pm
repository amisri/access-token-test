#!/usr/bin/perl -w
#
# Name:     Input/Class/Consts.pm
# Purpose:  Provides functions relating to the FGC class constants
# Author:   Stephen Page

package Input::Class::Consts;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    return if($element eq 'assert' || $element eq 'asserts');

    # Check that the consts are valid for this class (they are contained within a symlist in the class)

    for my $const (keys(%$current_node))
    {
        # Ignore special attributes

        next if($const eq "node_depth");
        next if($const eq "node_tag");
        next if($const eq "parent");

        if($current_node->{$const} =~ /^\*(.*)\.(.*)_(.*?)$/)
        {
            my $memmap_name     = $1;
            my $memzone_name    = $2;
            my $const_type      = $3;

            # Locate memzone

            my @memzone_tree    = split("_", $memzone_name);
            my $memzone         = $superhash->{current_class}->{platform}->{memmaps}->{$memmap_name};

            my $target_name;
            while(($target_name = shift(@memzone_tree)))
            {
                my $found = 0;
                for my $child (@{$memzone->{children}})
                {
                    if($child->{name} eq $target_name)
                    {
                        $memzone    = $child;
                        $found      = 1;
                        last;
                    }
                }
                $expat->xpcroak("Memzone $memzone_name not found") if(!$found);
            }

            if($const_type eq '16BIT')
            {
                # Truncate address to 16 bits

                my $truncated_address   =  sprintf("%04X", $memzone->{address} / 8);
                $truncated_address      =~ s/^.*(.{4,4})$/$1/;

                $current_node->{$const} = sprintf("0x%s", $truncated_address);
            }
            elsif($const_type eq '32BIT')
            {
                $current_node->{$const} = sprintf("0x%08X", $memzone->{address} / 8);
            }
            elsif($const_type eq '24BIT')
            {
                $current_node->{$const} = sprintf("0x%06X", $memzone->{address} / 8);
            }
            elsif($const_type eq 'PAGE')
            {
                $current_node->{$const} = sprintf("0x%1X", $memzone->{address} / 524288);
            }
            elsif($const_type eq 'MASK8')
            {
                $current_node->{$const} = sprintf("0x%04X", $memzone->{mask});
            }
            elsif($const_type eq 'MASK16')
            {
                $current_node->{$const} = sprintf("0x%04X", $memzone->{mask});
            }
            elsif($const_type eq 'MASK32')
            {
                $current_node->{$const} = sprintf("0x%04X", $memzone->{mask});
            }
            else
            {
                $expat->xpcroak("Invalid const type $const_type");
            }
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    return if($element eq 'assert' || $element eq 'asserts');

    delete($current_node->{children}) if(exists($current_node->{children}));

    # Flatten constants

    if(exists($current_node->{parent}))
    {
        for my $const (keys(%$current_node))
        {
            # Ignore special attributes

            next if($const eq "node_depth");
            next if($const eq "node_tag");
            next if($const eq "parent");

            # Convert value to decimal if it is in hex

            if($current_node->{$const} =~ /^0x\w+/) # Value is in hex
            {
                $current_node->{$const} =~ s/(0x\w+)/hex($1)/e;
            }

            # Check that constant is not already defined globally

            if(defined($superhash->{consts}->{$const}))
            {
                $expat->xpcroak("Const $const already defined globally");
            }

            # Check that parent does not already have constant defined

            if(defined($current_node->{parent}->{$const}))
            {
                $expat->xpcroak("Const $const defined in parent and child");
            }

            # Assign constant in parent

            $current_node->{parent}->{$const} = $current_node->{$const};
        }
    }
    else # Top-level node
    {
        for my $const (keys(%$current_node))
        {
            if(defined($superhash->{current_class}->{symtab_consts}->{$const}))
            {
                $superhash->{current_class}->{symtab_consts}->{$const} = $current_node->{$const};
            }
        }
    }
}

# EOF
