#!/usr/bin/perl -w
#
# Name:     Input/Class/Pubdata.pm
# Purpose:  Provides functions relating to the FGC class published data memory map
# Author:   Stephen Page

package Input::Class::Pubdata;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %zone_fullnames;

my $data_position           = 0;
my $next_possible_address   = 0;

my %allowed_types           = (
                                INT8U   => 1,
                                INT8S   => 1,
                                INT16U  => 1,
                                INT16S  => 1,
                                INT32U  => 1,
                                INT32S  => 1,
                                FLOAT   => 1,
                              );

return(1);


# Begin functions

sub start_tag_handler
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(comment name offset size))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Capitalise name

    $current_node->{name} = "\U$current_node->{name}";

    if(defined($current_node->{parent}))
    {
        # Node is not root, so use parent's full name to form this node's name

        if($current_node->{parent}->{fullname} ne "")
        {
            $current_node->{fullname} = "$current_node->{parent}->{fullname}_$current_node->{name}";
        }
        else  # Parent is name-less root node
        {
            $current_node->{fullname} = $current_node->{name};
        }
    }
    else  # Node is the root node
    {
        # The root node's fullname is equal to its name

        $current_node->{fullname} = $current_node->{name};

        # Reset $next_possible_address to zero

        $next_possible_address = 0;

        # Empty %zone_fullnames

        %zone_fullnames = ()
    }

    # Check for a duplicate full name

    if($current_node->{fullname} ne "")
    {
        if(defined($zone_fullnames{$current_node->{fullname}}))
        {
            $expat->xpcroak("Zone name $current_node->{fullname} is duplicated");
        }
        else
        {
            $zone_fullnames{$current_node->{fullname}} = 1;
        }
    }

    # Add reference to class

    $current_node->{class} = $superhash->{current_class};

    # Add memory map name

    $current_node->{mapname} = "pubdata";

    # Convert offset into an integer number of bits

    my %memsizes = (
                        b => 1,         # Bit
                        B => 8,         # Byte
                        K => 8192,      # Kilobyte
                        M => 8388608,   # Megabyte
                        G => 8589934592 # Gigabyte
                   );

    # Convert offset to decimal if it is in hex

    if($current_node->{offset} =~ /^0x\w+/) # Offset is in hex
    {
        $current_node->{offset} =~ s/^(0x\w+)(\D)$/hex($1).$2/e;
    }

    # Check that offset is validly formatted and convert to bits

    if($current_node->{offset} =~ /^\d+\D$/)
    {
        # Get offset in bits

        $current_node->{offset} =~ s/^(\d+)(\D)$/$1/;

        my $units = $2;

        if(defined($memsizes{$units}))
        {
            $current_node->{offset} *= $memsizes{$units};
        }
        else
        {
            $expat->xpcroak("Invalid unit character \'$units\' specified for offset of zone $current_node->{name}");
        }
    }
    else
    {
        $expat->xpcroak("Zone $current_node->{name} has invalidly formatted offset field");
    }

    # Get the address of the start of this zone

    if(!defined($current_node->{parent})) # root node
    {
        $current_node->{address} = $current_node->{offset};
    }
    else # Low-level node
    {
        $current_node->{address} = $current_node->{parent}->{address} + $current_node->{offset};
    }

    # Check that address does not overlap with previous node

    $expat->xpcroak("Zones overlap") if($current_node->{address} < $next_possible_address);

    # Set the byte address for this zone (rounded down)

    $current_node->{byte_address} = int($current_node->{address} / 8);

    # Check that size is validly formatted and get number of bits

    if($current_node->{size} =~ /^\d+\D$/)
    {
        # Get size in bits

        $current_node->{numbits} =  $current_node->{size};
        $current_node->{numbits} =~ s/^(\d+)(\D)$/$1/;

        my $units = $2;

        if(defined($memsizes{$units}))
        {
            $current_node->{numbits} *= $memsizes{$units};
        }
        else
        {
            $expat->xpcroak("Invalid unit character \'$units\' specified for size of zone $current_node->{name}");
        }
    }
    else
    {
        $expat->xpcroak("Zone $current_node->{name} has invalidly formatted size field");
    }

    # Check that this node does not overrun the end of its parent

    if(defined($current_node->{parent}) &&
       ($current_node->{offset} + $current_node->{numbits}) > $current_node->{parent}->{numbits})
    {
        $expat->xpcroak("Zone $current_node->{name} overruns the end of its parent $current_node->{parent}->{name}");
    }
}

sub end_tag_handler
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # If this is a bottom-level node (has no children) and
    # is not also the top-level node (it has a parent), then it must have a valid property attribute

    if(!defined($current_node->{children}) &&
       defined($current_node->{parent}))
    {
        # Check that property is defined

        if(!defined($current_node->{property}))
        {
            $expat->xpcroak("Property not defined");
        }

        # Check that property exists for this class and convert property attribute to a reference to the property

        my $property_found = 0;
        for my $property (@{$superhash->{current_class}->{properties}})
        {
            if($property->{name} eq $current_node->{property})
            {
                $current_node->{property}   = $property;
                $property_found             = 1;
                last;
            }
        }

        # Report error if property does not exist for this class

        if(!$property_found)
        {
            $expat->xpcroak("Property $current_node->{property} does not exist for this class");
        }

        # Check that property is of an allowed type

        if(!defined($allowed_types{$current_node->{property}->{type}->{name}}))
        {
            $expat->xpcroak("Property $current_node->{property}->{name} is not of an allowed type ($current_node->{property}->{type}->{name})");
        }

        # Check that the size of the zone matches that of the property's type

        my $property_numbits = $current_node->{property}->{type}->{size} * 8;

        if($current_node->{numbits} != $property_numbits)
        {
            $expat->xpcroak("Size of zone $current_node->{name} ($current_node->{numbits}) does not match that of property $current_node->{property}->{name} ($property_numbits)");
        }

        # Do not display zone bits by default

        $current_node->{display_bits} = 0 if(!defined($current_node->{display_bits}));

        # Add position within data

        $current_node->{data_position} = $data_position++;
    }
    else
    {
        # Check that property is not defined

        if(defined($current_node->{property}))
        {
            $expat->xpcroak("Zone may not have a property defined");
        }

        # Zone does not directly contain a property, set data position to -1 to indicate this

        $current_node->{data_position} = -1;
    }

    $next_possible_address = $current_node->{address} + $current_node->{numbits};
}

# EOF
