#!/usr/bin/perl -w
#
# Name:     Input/Class/Properties.pm
# Purpose:  Provides functions relating to the FGC class properties
# Author:   Stephen Page

package Input::Class::Properties;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    my $class = $superhash->{current_class};

    # Check that properties within this node are valid

    $current_node->{properties} = [];
    $current_node->{property_levels}->[0] = [];

    if(defined($current_node->{node_text}))
    {
        my %propertyhash;
        my %parsfunctions;

        # Remove newlines from node_text
        
        $current_node->{node_text} =~ s/\n/ /g;

        # Remove initial whitespace from node_text
        
        $current_node->{node_text} =~ s/^\s*//;

        # Validate properties

        for my $property (split(/\s+/, $current_node->{node_text}))
        {
            # Check that property exists within defined properties

            if(!defined($superhash->{properties}->{$property}))
            {
                $expat->xpcroak("$property is not a valid property");
            }
            $property = $superhash->{properties}->{$property};

            # Check whether property has already been added to the class (duplicated in XML)

            if(defined($propertyhash{$property->{name}}))
            {
                $expat->xpcroak("Property $property->{name} is duplicated for class $class->{id}");
            }

            $property->{classes}->{$class->{id}} = $class;

            # Check whether property has a class-specific symlist

            if(defined($property->{class_symlist}))
            {
                # Check whether symlist exists for this class

                my $symlist = $class->{symlists}->{$property->{class_symlist}};
                if(!defined($symlist))
                {
                    $expat->xpcroak("Missing class symlist $property->{class_symlist} required for property $property->{name} for class $class->{id}");
                }
                else
                {
                    $property->{class_symlists}->{$class->{id}} = $symlist;
                }
            }
    
            # Check whether property is for a DSP

            my $dsp = undef;
            if(defined($property->{"dsp_$class->{id}"}))
            {
                $dsp = $property->{"dsp_$class->{id}"};
            }
            elsif(defined($property->{"dsp_$class->{platform}->{name}"}))
            {
                $dsp = $property->{"dsp_$class->{platform}->{name}"};
            }
            
            if(defined($dsp))
            {
                # Add DSP to class's DSPs

                $class->{dsps}->{$dsp}->{name} = $dsp;

                # Add property to DSP's properties

                $class->{dsps}->{$dsp}->{properties}->{$property->{name}} = $property;
                $class->{dsps}->{$dsp}->{property_idxs}->{$property->{name}}
                    = scalar(keys(%{$class->{dsps}->{$dsp}->{properties}})) - 1;
            }

            # Define pars function

            if(defined($property->{"pars_$class->{id}"}))
            {
                $parsfunctions{$property->{"pars_$class->{id}"}} = 1;
            }
            elsif(defined($property->{"pars_$class->{platform}->{name}"}))
            {
                $parsfunctions{$property->{"pars_$class->{platform}->{name}"}} = 1;
            }
            elsif(defined($property->{pars}))
            {
                $parsfunctions{$property->{pars}} = 1;
            }

            # Check for class and platform specific flags

            foreach my $flags_postfix ($class->{id}, $class->{platform}->{name})
            {
                # skip if hash has already been parsed and assigned (platform hash will be called on every class of that platform)
                next if(defined($property->{class_flags}->{$flags_postfix}));

                # continue, as data is still raw
                if(defined($property->{"flags_".$flags_postfix}))
                {
                    my %flags;
                    for my $flag (split(/\s+/, $property->{"flags_".$flags_postfix}))
                    {
                        if(defined($superhash->{flags}->{$flag}))
                        {
                            $flags{$superhash->{flags}->{$flag}->{name}} = $superhash->{flags}->{$flag};
                        }
                        else
                        {
                            $expat->xpcroak("$flag is not a valid flag");
                        }
                    }
                    $property->{class_flags}->{$flags_postfix} = \%flags;
                }

                if(defined($property->{symlist}) ||
                   defined($property->{class_symlist}))
                {   
                    $property->{class_flags}->{$flags_postfix} = {} if(!defined($property->{class_flags}->{$flags_postfix}));
                    $property->{class_flags}->{$flags_postfix}->{SYM_LST} = $superhash->{flags}->{SYM_LST};
                }

                if(defined($property->{limits}))
                {
                    $expat->xpcroak("Min limit not defined") if(!defined($property->{min}));
                    $expat->xpcroak("Max limit not defined") if(!defined($property->{max}));

                    $property->{class_flags}->{$flags_postfix} = {} if(!defined($property->{class_flags}->{$flags_postfix}));
                    $property->{class_flags}->{$flags_postfix}->{LIMITS} = $superhash->{flags}->{LIMITS};
                }
            }

            # operational - per class
            if(defined($property->{"operational_$class->{platform}->{name}"}))
            {
                $expat->xpcroak("Please define operational flag only per class id, not platform");
            }
            if(defined($property->{"operational_$class->{id}"}))
            {
                $property->{class_operational} = {} if(!defined($property->{class_operational}));
                $property->{class_operational}->{$class->{id}} = $property->{"operational_$class->{id}"};
            }

            # Add property and its parents to this class's properties

            my $name;
            for my $symbol (split(/\./, $property->{name}))
            {
                if(!defined($name))
                {
                    $name = $symbol;
                }
                else
                {
                    $name .= ".$symbol";
                }
                
                if(!defined($propertyhash{$name}))
                {
                    $propertyhash{$name} = 1;

                    # Add property to this class's property array

                    push(@{$current_node->{properties}}, $superhash->{properties}->{$name});

                    # Add reference to this class within property's hash of classes

                    $superhash->{properties}->{$name}->{classes}->{$class->{id}} = $class;

                    # Add property to this class's level array

                    push(@{$current_node->{property_levels}->[$superhash->{properties}->{$name}->{node_depth}]},
                         $superhash->{properties}->{$name});
                }
            }
        }

        # Set pars functions for class

        $class->{parsfunctions} = [sort(keys(%parsfunctions))];
    }
}

# EOF
