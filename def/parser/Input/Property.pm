#!/usr/bin/perl -w
#
# Name:     Input/Property.pm
# Purpose:  Provides functions relating to the FGC properties
# Author:   Stephen Page

package Input::Property;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %property_names;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(symbol title type))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check that symbol is properly formatted

    if($current_node->{symbol} !~ /^[A-Z]\w*$/)
    {
        $expat->xpcroak("Symbol $current_node->{symbol} is not validly formatted");
    }

    # Construct the name for this node

    if(defined($current_node->{parent}))
    {
        # Node is not root, so use parent's name to form this node's name

        $current_node->{name} = "$current_node->{parent}->{name}.$current_node->{symbol}";

        push(@{$superhash->{property_levels}->[$current_node->{node_depth}]}, $current_node);
    }
    else
    {
        # Node is the root node, so name is equal to symbol

        $current_node->{name} = $current_node->{symbol};

        push(@{$superhash->{property_levels}->[0]}, $current_node);
    }

    # Check for a duplicate property name

    if(defined($property_names{$current_node->{name}}))
    {
        $expat->xpcroak("Property name $current_node->{name} is duplicated");
    }

    $property_names{$current_node->{name}} = 1;

    # Check that type of this node is valid

    if(defined($superhash->{types}->{$current_node->{type}}))
    {
        $current_node->{type} = $superhash->{types}->{$current_node->{type}};
    }
    else
    {
        $expat->xpcroak("$current_node->{type} is not a valid type");
    }

    # Check extra types (specific for class/property)
    foreach my $extra_type ( grep (/^type_[A-Za-z0-9]+$/, keys(%$current_node)) )
    {
        my $type_class_platform = (split /_/, $extra_type)[1];  
        if(defined($superhash->{types}->{$current_node->{$extra_type}}))
        {
               if(!defined($current_node->{specific_types}))
               {
                   $current_node->{specific_types} = {};
               }
               $current_node->{specific_types}->{$type_class_platform} = $superhash->{types}->{$current_node->{$extra_type}};
        }
        else
        {
            $expat->xpcroak("$extra_type: $current_node->{$extra_type} is not a valid type");
        }
    }


    # Check that flags within this node are valid

    if(defined($current_node->{flags}))
    {
        my %flags;
        for my $flag (split(/\s+/, $current_node->{flags}))
        {
            if(defined($superhash->{flags}->{$flag}))
            {
                $flags{$superhash->{flags}->{$flag}->{name}} = $superhash->{flags}->{$flag};
            }
            else
            {
                $expat->xpcroak("$flag is not a valid flag");
            }
        }
        $current_node->{flags} = \%flags;
    }
    else
    {
        $current_node->{flags} = {};
    }

    # If the property has the CONFIG flag set, check that all conditions are met

    if(defined($current_node->{flags}->{CONFIG}))
    {
        # Check that NON_VOLATILE flag is set

        if(!defined($current_node->{flags}->{NON_VOLATILE}))
        {
            $expat->xpcroak("CONFIG flag is set, but not NON_VOLATILE flag is not");
        }

        # Check that number of elements is 0

        if(defined($current_node->{numels}) && $current_node->{numels} ne '0')
        {
            $expat->xpcroak("CONFIG flag is set, but number of elements is not 0");
        }
    }

    # Check that no DSP flag is set directly

    if(grep { $_->{name} =~ /^DSP_/ } values(%{$current_node->{flags}}))
    {
        $expat->xpcroak("DSP flags may not be set directly");
    }

    # If 'limits' is specified per property, check that it is consistent with the type, since FGC[D] code assumes so

    if (defined($current_node->{limits})         &&
        defined($current_node->{type}->{limits}) &&
        $current_node->{limits} ne $current_node->{type}->{limits})
    {
        print "WARNING: Mismatch of 'limits' in property $current_node->{name}\t";
        print "(property: limits = '$current_node->{limits}', type $current_node->{type}->{name}: limits = \t'$current_node->{type}->{limits}')\n";
    }

    # If limits are not defined then obtain them from the property's type

    if(!defined($current_node->{symlist})       &&
       !defined($current_node->{class_symlist}) &&
        !defined($current_node->{limits}))
    {
        # Check extra types (specific for class/property)
        foreach my $extra_type ( grep (/^type_[A-Za-z0-9]+$/, keys(%$current_node)) )
        {
            my $type_class_platform = (split /_/, $extra_type)[1];
            my $extra_type_val = $current_node->{specific_types}->{$type_class_platform};

            if(defined($extra_type_val->{limits}))
            {
                $current_node->{'limits_'.$type_class_platform} = $extra_type_val->{limits};
                $current_node->{'min_'.$type_class_platform}    = $extra_type_val->{min};
                $current_node->{'max_'.$type_class_platform}    = $extra_type_val->{max};
            }

        }
         
        if(defined($current_node->{type}->{limits}))
        {
            $current_node->{limits} = $current_node->{type}->{limits};
            $current_node->{min}    = $current_node->{type}->{min};
            $current_node->{max}    = $current_node->{type}->{max};
        }
    }

    if(defined($current_node->{symlist}) ||
       defined($current_node->{class_symlist}))
    {
        $current_node->{flags}->{SYM_LST} = $superhash->{flags}->{SYM_LST};
    }

    if(defined($current_node->{limits}))
    {
        $expat->xpcroak("Min limit not defined") if(!defined($current_node->{min}));
        $expat->xpcroak("Max limit not defined") if(!defined($current_node->{max}));

        $current_node->{flags}->{LIMITS} = $superhash->{flags}->{LIMITS};
    }

    # Delete get function if explicitly set to empty string

    if(defined($current_node->{get}) && $current_node->{get} eq "")
    {
        delete($current_node->{get});
    }

    # Check that get functions are valid (there might be different get functions for each class)

    foreach my $get_function_label ( grep (/^get(_[A-Za-z0-9]+)?$/, keys(%$current_node)) )
    {
        my $get_function = $current_node->{$get_function_label};

        if(defined($superhash->{getfuncs}->{$get_function}))
        {
            $current_node->{$get_function_label} = $superhash->{getfuncs}->{$get_function};
        }
        else
        {
            $expat->xpcroak("$current_node->{$get_function_label} is not a valid get function");
        }

        # Check that node type corresponds to one of the get function's types

        my $validtype = 0;
        for my $getfunc_type (@{$current_node->{$get_function_label}->{types}})
        {
            $validtype = 1 if($getfunc_type == $current_node->{type});
        }

        if($validtype == 0)
        {
            $expat->xpcroak("$current_node->{type}->{name} is not a valid type for get function $current_node->{$get_function_label}->{name} in $current_node->{name}");
        }
    }

    # Check that set function of this node is valid

    foreach my $set_function_label ( grep (/^set(_[A-Za-z0-9]+)?$/, keys(%$current_node)) )
    {
        my $set_function = $current_node->{$set_function_label};

        if(defined($superhash->{setfuncs}->{$set_function}))
        {
            $current_node->{$set_function_label} = $superhash->{setfuncs}->{$set_function};
        }
        else
        {
            $expat->xpcroak("$current_node->{$set_function_label} is not a valid set function");
        }

        # Check that node type corresponds to one of the get function's types

        my $validtype = 0;
        for my $setfunc_type (@{$current_node->{$set_function_label}->{types}})
        {
            $validtype = 1 if($setfunc_type == $current_node->{type});
        }

        if($validtype == 0)
        {
            $expat->xpcroak("$current_node->{type}->{name} is not a valid type for set function $current_node->{$set_function_label}->{name} in $current_node->{name}");
        }
    }

    # Check that 'set if' function of this node is valid

    if(defined($current_node->{setif}))
    {
        if(defined($superhash->{setiffuncs}->{$current_node->{setif}}))
        {
            $current_node->{setif} = $superhash->{setiffuncs}->{$current_node->{setif}};
        }
        else
        {
            $expat->xpcroak("$current_node->{setif} is not a valid 'set if' function");
        }
    }

    # Check that symlist of this node is valid

    if(defined($current_node->{symlist}))
    {
        if(defined($current_node->{class_symlist}))
        {
            $expat->xpcroak("symlist and class_symlist are mutually exclusive");
        }

        if(defined($superhash->{symlists}->{$current_node->{symlist}}))
        {
            $current_node->{symlist} = $superhash->{symlists}->{$current_node->{symlist}};
        }
        else
        {
            $expat->xpcroak("$current_node->{symlist} is not a valid symlist");
        }
    }

    # Set property group

    if(defined($current_node->{group}))
    {
        my $group = $superhash->{property_groups}->{$current_node->{group}};
        $expat->xpcroak("$current_node->{group} is not a valid property group") if(!defined($group));
        $current_node->{group} = $group;
    }
    else
    {
        if(defined($current_node->{parent}))
        {
            $current_node->{group} = $current_node->{parent}->{group};
        }
        else
        {
            $current_node->{group} = $superhash->{property_groups}->{DEFAULT};
        }
    }

    # If KT flag is not defined, check parent

    if(!defined($current_node->{kt}))
    {
        if(defined($current_node->{parent}))
        {
            if(defined($current_node->{parent}->{kt}) && $current_node->{parent}->{kt})
            {
                $current_node->{kt} = $current_node->{parent}->{kt};
            }
        }
    }

    # EPCCCS-7439: check operational flag
    # if we reached a leaf node, check that at max one property in the property's hierarchy is marked as operational
    
    if($current_node->{type}->{name} ne 'PARENT')
    {
        my $op_count = defined($current_node->{operational}) && $current_node->{operational} ? 1 : 0;
        my $parent = $current_node->{parent};
        while (defined($parent)) 
        {
            if(defined($parent->{operational}) && $parent->{operational})
            {
                $op_count = $op_count + 1 ;

                if($op_count > 1)
                {
                    $expat->xpcroak("Operational flag is defined at more than one level of the property's hierarchy - $current_node->{name}");
                }
            }

            $parent = $parent->{parent};
        }
    }

    # Set dynamic flag to 0 by default

    $current_node->{dynamic} ||= 0;
}

sub is_reference
{
    return ($_[0] =~ /^\(.*\)\s*&/)
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ensure that type is correctly set

    if(!defined($current_node->{children})) # Child
    {
        if(!defined($current_node->{type}))
        {
            $expat->xpcroak("Property $current_node->{name} has no type");
        }

        if($current_node->{type}->{name} eq 'PARENT')
        {
            $current_node->{children} = [];
        }
    }
    else # Parent
    {
        $current_node->{type} = $superhash->{types}->{PARENT};
    }

    # Set number of elements for parent properties

    if($current_node->{type}->{name} eq 'PARENT')
    {
        if(!@{$current_node->{children}} ||             # Property has no children
           !$current_node->{children}->[0]->{dynamic})  # First child is not dynamic
        {
            if(defined($current_node->{numels}))
            {
                $expat->xpcroak("Number of elements should not be specified for parent properties");
            }

            (my $name = "\L$current_node->{name}") =~ s/\./_/g;
            $current_node->{numels} = @{$current_node->{children}} ? "FGC_N_ELS(prop_$name)" : "0";
        }
        else
        {
            $current_node->{numels} = 0;
        }
    }

    # Ensure that number of elements and maximum number of elements are set

    my $update_node;
    my %numels_attributes = ();

    foreach my $key ( keys %{ $current_node } )
    {
        # Key = numels

        if(substr($key, 0, 6) eq "numels")
        {
            $numels_attributes{$key} = $current_node->{$key};

            my $maxels = "max" . substr $key, 3;

            if(is_reference($current_node->{$key}))
            {
                # Case 1: numels is a reference. Check that INDIRECT_N_ELS flag is set.

                if(!defined($current_node->{flags}->{INDIRECT_N_ELS}))
                {
                    $expat->xpcroak("INDIRECT_N_ELS flag must be set when numels is a reference.");
                }

                # If maxels_{platform}/{class} is not defined, then fail

#                if(!defined($current_node->{$maxels}))
#                {
#                    $expat->xpcroak("$key attibute defined as a reference, but $maxels atribute is not defined.\n");
#                }
            }
            else
            {
                # Case 2: numels is a constant

                if(!defined($current_node->{$maxels}))
                {
                    # If maxels is not defined set it from numels

                    $update_node->{$maxels} = $current_node->{$key};
                }
            }
        }

        # Key = maxels

        elsif(substr($key, 0, 6) eq "maxels")
        {
            my $numels = "num" . substr $key, 3;

            if(!defined($current_node->{$numels}))
            {
                $update_node->{$numels} = 0;
            }
        }
    }

    # If INDIRECT_N_ELS flag is present, check that values of all numels attributes are references

    if(defined($current_node->{flags}->{INDIRECT_N_ELS}))
    {
        if(!%numels_attributes)
        {
            $expat->xpcroak("INDIRECT_N_ELS flag present, but no numels attribute defined\n");
        }

        foreach my $key ( keys %numels_attributes )
        {
            if(!is_reference($numels_attributes{$key}))
            {
                $expat->xpcroak("INDIRECT_N_ELS flag present, but value of $key is not a reference.\n");
            }
        }
    }

    # Set the PPM flag for direct parents of PPM leaf properties

    if($current_node->{type}->{name} ne 'PARENT' && defined($current_node->{flags}->{PPM}))
    {
        $current_node->{parent}->{flags}->{PPM} = $superhash->{flags}->{PPM};
    }

    # Finished iterating, commit the changes to the hash

    foreach my $key ( keys %{ $update_node } )
    {
        ${$current_node}{$key} = ${$update_node}{$key};
    }

    # If numels is still undefined, set it to zero. There are a few cases where numels should be zero:
    # NULL properties and properties which trigger a function rather than reading from a data structure.

    if(!defined($current_node->{numels}))
    {
        $current_node->{numels} = 0;
        $current_node->{maxels} = 0;
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(doc))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }
}

# EOF
