#!/usr/bin/perl -w
#
# Name:     Input/Consts.pm
# Purpose:  Provides functions relating to global constants
# Author:   Stephen Page

package Input::Consts;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    return if($element eq 'assert' || $element eq 'asserts');

    delete($current_node->{children}) if(exists($current_node->{children}));

    # Flatten constants

    if(exists($current_node->{parent}))
    {
        for my $const (keys(%$current_node))
        {
            # Ignore special attributes

            next if($const eq "node_depth");
            next if($const eq "node_tag");
            next if($const eq "parent");

            # Convert value to decimal if it is in hex

            if($current_node->{$const} =~ /^0x\w+/) # Value is in hex
            {
                $current_node->{$const} =~ s/(0x\w+)/hex($1)/e;
            }

            # Check that parent does not already have constant defined

            if(defined($current_node->{parent}->{$const}))
            {
                $expat->xpcroak("Const $const defined in parent and child");
            }

            # Assign constant in parent

            $current_node->{parent}->{$const} = $current_node->{$const};
        }
    }
}

# EOF
