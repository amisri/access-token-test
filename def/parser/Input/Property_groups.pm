#!/usr/bin/perl -w
#
# Name:     Input/Property_groups.pm
# Purpose:  Provides functions relating to property groups
# Author:   Stephen Page

package Input::Property_groups;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my $system;
my $group_idx;

my %start_tag_handlers = (
                            "property_group"    => \&start_property_group_tag_handler,
                            "property_groups"   => \&start_property_groups_tag_handler,
                         );

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check whether a handler has been defined for the tag

    if(defined($start_tag_handlers{$current_node->{node_tag}}))
    {
        # Handler has been defined, call it

        $start_tag_handlers{$current_node->{node_tag}}($expat, $element, $current_node, $superhash);
    }
    else
    {
        # No handler defined for tag, return error

        $expat->xpcroak("No start tag handler for tag $current_node->{node_tag}");
    }
}

sub start_property_group_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if(!defined($current_node->{parent}) ||
       $current_node->{parent}->{node_tag} ne "property_groups")
    {
        $expat->xpcroak("property_group must be a child of property_groups");
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(name title))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Set java to default value of 0 if not defined

    $current_node->{java} = 0 if(!defined($current_node->{java}));
}

sub start_property_groups_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if(defined($current_node->{parent}))
    {
        $expat->xpcroak("property_groups element must be at the top-level");
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
