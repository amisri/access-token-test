#!/usr/bin/perl -w
#
# Name:     Input/Parser.pm
# Purpose:  Provides functions to parse global definitions
# Author:   Stephen Page

package Input::Parser;

use Exporter;
use Input::Component_labels;
use Input::Components;
use Input::Code;
use Input::Consts;
use Input::Dim_names;
use Input::Dim_types;
use Input::Error;
use Input::Flag;
use Input::Function;
use Input::Getoption;
use Input::Property;
use Input::Property_groups;
use Input::Symlist;
use Input::Systems;
use Input::Type;
use Input::Class::Parser;
use Input::Doc::Main;
use Input::Platform::Parser;
use Input::RegFGC3::Parser;
use Input::Runlog;
use Output::Doc::Writer;

use FGC::Names;

use Parser;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

# Create a hash of start tag handlers for definition files

my %start_tag_handlers = (
                           asserts              => \&Input::Asserts::start_tag_handler,
                           codes                => \&Input::Code::start_tag_handler,
                           component_labels     => \&Input::Component_labels::start_tag_handler,
                           components           => \&Input::Components::start_tag_handler,
                           consts               => \&Input::Consts::start_tag_handler,
                           dim_names            => \&Input::Dim_names::start_tag_handler,
                           dim_types            => \&Input::Dim_types::start_tag_handler,
                           errors               => \&Input::Error::start_tag_handler,
                           flags                => \&Input::Flag::start_tag_handler,
                           getoptions           => \&Input::Getoption::start_tag_handler,
                           getfuncs             => \&Input::Function::start_tag_handler,
                           property_groups      => \&Input::Property_groups::start_tag_handler,
                           runlog               => \&Input::Runlog::start_tag_handler,
                           setfuncs             => \&Input::Function::start_tag_handler,
                           setiffuncs           => \&Input::Function::start_tag_handler,
                           sitemap              => \&Input::Doc::Main::start_tag_handler,
                           symlists             => \&Input::Symlist::start_tag_handler,
                           systems              => \&Input::Systems::start_tag_handler,
                           types                => \&Input::Type::start_tag_handler,
                         );

# Create a hash of end tag handlers for definition files

my %end_tag_handlers = (
                           asserts              => \&Input::Asserts::end_tag_handler,
                           codes                => \&Input::Code::end_tag_handler,
                           component_labels     => \&Input::Component_labels::end_tag_handler,
                           components           => \&Input::Components::end_tag_handler,
                           consts               => \&Input::Consts::end_tag_handler,
                           dim_names            => \&Input::Dim_names::end_tag_handler,
                           dim_types            => \&Input::Dim_types::end_tag_handler,
                           errors               => \&Input::Error::end_tag_handler,
                           flags                => \&Input::Flag::end_tag_handler,
                           getoptions           => \&Input::Getoption::end_tag_handler,
                           getfuncs             => \&Input::Function::end_tag_handler,
                           property_groups      => \&Input::Property_groups::end_tag_handler,
                           runlog               => \&Input::Runlog::end_tag_handler,
                           setfuncs             => \&Input::Function::end_tag_handler,
                           setiffuncs           => \&Input::Function::end_tag_handler,
                           sitemap              => \&Input::Doc::Main::end_tag_handler,
                           symlists             => \&Input::Symlist::end_tag_handler,
                           systems              => \&Input::Systems::end_tag_handler,
                           types                => \&Input::Type::end_tag_handler,
                       );

# Create a hash of index attributes for each definition file

my %index_attributes = (
                         codes              => 'full_name',
                         component_labels   => 'barcode',
                         components         => 'barcode',
                         dim_names          => 'name',
                         errors             => 'number',
                         flags              => 'name',
                         getoptions         => 'symbol',
                         getfuncs           => 'name',
                         property_groups    => 'name',
                         setfuncs           => 'name',
                         setiffuncs         => 'name',
                         sitemap            => 'name',
                         symlists           => 'name',
                         types              => 'name',
                       );

return(1);


# Begin functions

sub parse($$$)
{
    my ($projpath, $superhash, $roots) = @_;

    parse_global_defs($projpath, $superhash, $roots);

    # Parse all platforms

    # Get a list of platform directories

    my @platform_dirs = <$projpath/def/src/platforms/*>;

    $superhash->{platforms} = {};

    for my $platform_dir (@platform_dirs)
    {
        Input::Platform::Parser::parse($projpath, $superhash, $roots, $platform_dir);
    }

    # Parse all classes

    # Get a list of class directories

    my @class_dirs = <$projpath/def/src/classes/*>;

    $superhash->{classes} = {};

    for my $class_dir (@class_dirs)
    {
        Input::Class::Parser::parse($projpath, $superhash, $roots, $class_dir);
    }
    
    # Parse RegFGC3

    $superhash->{regfgc3} = {};
    my $regfgc3_dir          = <$projpath/def/src/regfgc3/>;

    Input::RegFGC3::Parser::parse($projpath, $superhash, $roots, $regfgc3_dir);
}

# Function to parse definition files that apply globally (to all classes)
sub parse_global_defs($$$)
{
    my ($projpath, $superhash, $roots) = @_;

    # Create a hash of definition file locations

    my %def_files = (
                      codes             => "$projpath/def/src/codes.xml",
                      component_labels  => "$projpath/def/src/component_labels.xml",
                      components        => "$projpath/def/src/components.xml",
                      consts            => "$projpath/def/src/consts.xml",
                      dim_names         => "$projpath/def/src/dim_names.xml",
                      errors            => "$projpath/def/src/errors.xml",
                      flags             => "$projpath/def/src/flags.xml",
                      getoptions        => "$projpath/def/src/getoptions.xml",
                      getfuncs          => "$projpath/def/src/getfuncs.xml",
                      property_groups   => "$projpath/def/src/property_groups.xml",
                      runlog            => "$projpath/def/src/runlog.xml",
                      setfuncs          => "$projpath/def/src/setfuncs.xml",
                      setiffuncs        => "$projpath/def/src/setiffuncs.xml",
                      sitemap           => "$projpath/def/src/doc/main.xml",
                      types             => "$projpath/def/src/types.xml",
                    );

    my %node_tags = (
                      asserts           => { assert => 1, asserts => 1 },
                      codes             => { code => 1, codes => 1 },
                      component_labels  => { component_label => 1, component_labels => 1 },
                      components        => { component => 1, components => 1 },
                      consts            => { consts => 1, assert => 0, asserts => 0 },
                      dim_names         => { dim_name => 1, dim_names => 1 },
                      dim_types         => { ana_channel => 1, dig_bank => 1, dim_type => 1, input => 1 },
                      property_groups   => { property_group => 1, property_groups => 1 },
                      runlog            => { entry => 1, runlog => 1 },
                      sitemap           => { area => 1, link => 1, main => 1 },
                      systems           => { component => 1, dim => 1, group => 1, log_menu => 1, system => 1 },
                    );

    # Get a list of symlist definition files

    my @symlist_def_files = <$projpath/def/src/symlists/*.xml>;

    # Get a list of property definition files

    my @property_def_files = <$projpath/def/src/properties/*.xml>;

    # Parse the general definition files

    for my $def (qw(codes dim_names errors flags types getoptions getfuncs property_groups setfuncs setiffuncs))
    {
        $roots->{$def} = Parser::parse(
                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                        file                => $def_files{$def},
                                        node_tags           => $node_tags{$def},
                                        node_start_handler  => $start_tag_handlers{$def},
                                        node_end_handler    => $end_tag_handlers{$def},
                                        superhash           => $superhash,
                                      );
        $superhash->{$def} = {};
        Parser::populate_hash($superhash->{$def}, $roots->{$def}, $index_attributes{$def});

    }

    # Parse the runlog definition file

    $superhash->{runlog} = Parser::parse(
                                                doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                file                => $def_files{runlog},
                                                node_tags           => $node_tags{runlog},
                                                node_start_handler  => $start_tag_handlers{runlog},
                                                node_end_handler    => $end_tag_handlers{runlog},
                                                superhash           => $superhash,
                                            );

    # Move runlog entries down one level to move past root

    $superhash->{runlog} = $superhash->{runlog}->{children};

    # Parse sitemap definition file

    $superhash->{sitemap} = Parser::parse(
                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                            file                => $def_files{sitemap},
                                            node_tags           => $node_tags{sitemap},
                                            node_start_handler  => $start_tag_handlers{sitemap},
                                            node_end_handler    => $end_tag_handlers{sitemap},
                                            superhash           => $superhash,
                                         );

    # Parse the global consts definition file

    $superhash->{consts} = Parser::parse(
                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                            file                => $def_files{consts},
                                            node_tags           => $node_tags{consts},
                                            node_start_handler  => $start_tag_handlers{consts},
                                            node_end_handler    => $end_tag_handlers{consts},
                                            superhash           => $superhash,
                                        );

    # Remove the special attributes consts

    delete($superhash->{consts}->{node_depth});
    delete($superhash->{consts}->{node_tag});

    # Parse the global asserts definitions

    $superhash->{asserts} = Parser::parse(
                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                            file                => $def_files{consts},
                                            node_tags           => $node_tags{asserts},
                                            node_start_handler  => $start_tag_handlers{asserts},
                                            node_end_handler    => $end_tag_handlers{asserts},
                                            superhash           => $superhash,
                                         );

    $superhash->{asserts} = $superhash->{asserts}->{children};
    for my $assert (@{$superhash->{asserts}}) 
    {
        delete($assert->{node_depth});
        delete($assert->{node_tag});
        delete($assert->{parent});
    }

    # Parse the symlist definition files

    $superhash->{symlists} = {};

    for my $symlist_def_file (@symlist_def_files)
    {
        my $symlist_name    =  $symlist_def_file;
        $symlist_name       =~ s#.*/(.*)\.xml#$1#;

        # Capitalise symlist name

        $symlist_name =~ s/(\w+)/\U$1/g;

        $superhash->{symlists}->{$symlist_name} = Parser::parse(
                                                                doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                file                => $symlist_def_file,
                                                                node_tags           => { symlist => 1, const => 1 },
                                                                node_start_handler  => $start_tag_handlers{symlists},
                                                                node_end_handler    => $end_tag_handlers{symlists},
                                                                superhash           => $superhash,
                                                               );

        my $symlist = $superhash->{symlists}->{$symlist_name};

        # Set name of symlist

        $symlist->{name} = $symlist_name;

        # Add symlist name as prefix of constants in symlist

        for my $const (@{$symlist->{consts}})
        {
            $const->{symbol}    = $const->{name};
            $const->{name}      = "${symlist_name}_$const->{symbol}";
        }
    }

    # Parse the property definition files

    $superhash->{properties} = {};

    for my $property_def_file (@property_def_files)
    {
        my $toplevel_property_name  =  $property_def_file;
        $toplevel_property_name     =~ s#.*/(.*)\.xml#$1#;

        $roots->{properties} = {};

        $roots->{properties}->{$toplevel_property_name} = Parser::parse(
                                                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                            file                => $property_def_file,
                                                                            node_start_handler  => \&Input::Property::start_tag_handler,
                                                                            node_end_handler    => \&Input::Property::end_tag_handler,
                                                                            superhash           => $superhash,
                                                                       );
        Parser::populate_hash($superhash->{properties}, $roots->{properties}->{$toplevel_property_name}, "name");
    }

    # Parse the general definition files

    for my $def (qw(components component_labels))
    {
        $roots->{$def} = Parser::parse(
                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                        file                => $def_files{$def},
                                        node_tags           => $node_tags{$def},
                                        node_start_handler  => $start_tag_handlers{$def},
                                        node_end_handler    => $end_tag_handlers{$def},
                                        superhash           => $superhash,
                                      );
        $superhash->{$def} = {};
        Parser::populate_hash($superhash->{$def}, $roots->{$def}, $index_attributes{$def});
    }

    # Parse DIM types

    my @dim_type_files = <$projpath/def/src/dim_types/*.xml>;

    for my $dim_type_file (@dim_type_files)
    {
        Parser::parse(
                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                        file                => $dim_type_file,
                        node_tags           => $node_tags{dim_types},
                        node_start_handler  => $start_tag_handlers{dim_types},
                        node_end_handler    => $end_tag_handlers{dim_types},
                        superhash           => $superhash,
                     );
    }

    # Set DIM type indices

    if(defined($superhash->{dim_types}))
    {
        my $dim_type_idx = 0;

        for my $dim_type_name (sort keys (%{$superhash->{dim_types}}))
        {
            $superhash->{dim_types}->{$dim_type_name}->{dim_type_idx} = $dim_type_idx++;
        }
    }
    
    # Parse systems

    my @system_files = <$projpath/def/src/systems/*.xml>;

    for my $system_file (@system_files)
    {
        ($superhash->{current_system_type} = $system_file) =~ s/.*\/(.*).xml/\U$1/;

        Parser::parse(
                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                        file                => $system_file,
                        node_tags           => $node_tags{systems},
                        node_start_handler  => $start_tag_handlers{systems},
                        node_end_handler    => $end_tag_handlers{systems},
                        superhash           => $superhash,
                     );
    }

    # Check that there is a system definition for each instance of FGC2, FGC3 and FGCLite converters
    eval{
      my ($devices, $gateways) = FGC::Names::read();

      my %class_platforms = (        
          51 => ["fgc2"],
          92 => ["fgc2","fgclite"],
          62 => ["fgc3"],
          63 => ["fgc3"],
      );

      for my $device (values(%$devices))
      {
          next if !(grep($_ eq $device->{class}, [keys(%class_platforms)]));

          if(!defined($superhash->{systems}->{$device->{type}}))
          {
              
              ## EPCCCS-4915: Since a lot of FGC2 devices won't pass this test until after the naming cleanup, 
              #               we limit this warning to FGC3 devies by now.
              #
              #               Once the cleanup is done this should be an error and it should be applied 
              #               to FGC2, FGC3 and FGClite.
              
              if( grep($_ eq $device->{class}, [63, 62]))
              {
                  printf( STDERR "WARNING: Type $device->{type} has no XML definition (needed for $device->{name})\n");
              }
          }
          else
          {
              my $system_platform = $superhash->{systems}->{$device->{type}}->{platform};

              if( !(grep($_ eq $system_platform, @{$class_platforms{$device->{class}}}) ) )
              {
                  printf( STDERR "ERROR: $device->{type} has platform ${system_platform} on the xml definition ");
                  printf( STDERR "but device $device->{name} is declared as class $device->{type}\n");
                  die();
              }
          }
      }
    };

    if($@){
      printf( STDERR "WARNING: Unable to read Name File. System definition check for each instance omitted\n");
    }

    # Get a list of tester platforms with pin groups

    my @tester_platforms;
    for my $pin_group_dir (<$projpath/def/src/platforms/*/pin_groups>)
    {
        my $tester_platform =  $pin_group_dir;
        $tester_platform    =~ s!^.*/(.+)/pin_groups$!$1!;

        push(@tester_platforms, $tester_platform);
    }

    # Parse each tester platform

    for my $tester_platform (@tester_platforms)
    {
        $superhash->{tester_platforms}->{$tester_platform} = { name => $tester_platform };

        # Get a list of tester pin group directories

        my @tester_pin_group_def_dirs = <$projpath/def/src/platforms/$tester_platform/pin_groups/*>;

        # Parse the tester pin groups

        $tester_platform                        = $superhash->{tester_platforms}->{$tester_platform};
        $tester_platform->{all_test_functions}  = {};

        # Add temporary current tester platform name to superhash

        $superhash->{current_tester_platform}   = $tester_platform;

        for my $tester_pin_group_def (@tester_pin_group_def_dirs)
        {
            my $pin_group_name  =  $tester_pin_group_def;
            $pin_group_name     =~ s#.*/(.*)#$1#;

            # Check whether a main definition file exists for pin group

            # Check that main attributes file exists

            if(-f "$tester_pin_group_def/main.xml")
            {
                # Parse main attributes

                $tester_platform->{pin_groups}->{$pin_group_name} = Parser::parse(
                                                                                    doc_links   => Output::Doc::Writer::get_docinfo()->{links},
                                                                                    doc_splinks => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                                    file        => "$tester_pin_group_def/main.xml",
                                                                                    superhash   => $superhash
                                                                                 );
            }

            # Check that pins definition file exists for pin group

            die("$tester_pin_group_def/pins.xml does not exist\n") if(! -f "$tester_pin_group_def/pins.xml");

            # Add temporary current pin group name to superhash

            $superhash->{current_pin_group_name} = $pin_group_name;

            $tester_platform->{pin_groups}->{$pin_group_name}->{pins} = Parser::parse(
                                                                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                                        file                => "$tester_pin_group_def/pins.xml",
                                                                                        node_start_handler  => $start_tag_handlers{tester_pins},
                                                                                        node_end_handler    => $end_tag_handlers{tester_pins},
                                                                                        superhash           => $superhash,
                                                                                     );

            # Move pins down one level to move past root

            $tester_platform->{pin_groups}->{$pin_group_name}->{name} = $pin_group_name;
            $tester_platform->{pin_groups}->{$pin_group_name}->{pins} = $tester_platform->{$pin_group_name}->{pins}->{children};

            # Parse pin group's functions if any exist

            if(-f "$tester_pin_group_def/functions.xml")
            {
                $tester_platform->{pin_groups}->{$pin_group_name}->{functionsroot} = Parser::parse(
                                                                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                                                    file                => "$tester_pin_group_def/functions.xml",
                                                                                                    node_tags           => { arg => 1, function => 1 },
                                                                                                    node_start_handler  => $start_tag_handlers{tester_pin_functions},
                                                                                                    node_end_handler    => $end_tag_handlers{tester_pin_functions},
                                                                                                    superhash           => $superhash,
                                                                                                  );

                $tester_platform->{pin_groups}->{$pin_group_name}->{functions} = {};
                Parser::populate_hash($tester_platform->{pin_groups}->{$pin_group_name}->{functions},
                                      $tester_platform->{pin_groups}->{$pin_group_name}->{functionsroot},
                                      "name");

                # Add functions to hash of all functions

                Parser::populate_hash($tester_platform->{all_test_functions},
                                      $tester_platform->{pin_groups}->{$pin_group_name}->{functionsroot},
                                      "name");
            }

            # Remove temporary current pin group name from superhash

            delete($superhash->{current_pin_group_name});
        }

        # Remove temporary current tester platform name from superhash

        delete($superhash->{current_tester_platform_name});
    }
}

# EOF
