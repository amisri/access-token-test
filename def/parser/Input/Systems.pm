#!/usr/bin/perl -w
#
# Name:     Input/Systems.pm
# Purpose:  Provides functions relating to systems
# Author:   Stephen Page

package Input::Systems;
use Data::Dumper; ###########################3
use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my $system;
my $group_idx;

my @bus_addr;
my @dim_names;

my %start_tag_handlers = (
                            "component" => \&start_component_tag_handler,
                            "dim"       => \&start_dim_tag_handler,
                            "group"     => \&start_group_tag_handler,
                            "log_menu"  => \&start_log_menu_tag_handler,
                            "system"    => \&start_system_tag_handler,
                         );

my %end_tag_handlers = (
                            "system"    => \&end_system_tag_handler,
                       );

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check whether a handler has been defined for the tag

    if(defined($start_tag_handlers{$current_node->{node_tag}}))
    {
        # Handler has been defined, call it

        $start_tag_handlers{$current_node->{node_tag}}($expat, $element, $current_node, $superhash);
    }
    else
    {
        # No handler defined for tag, return error

        $expat->xpcroak("No start tag handler for tag $current_node->{node_tag}");
    }
}

sub start_component_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(barcode))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check whether component is of a valid type

    my $component = $superhash->{components}->{$current_node->{barcode}};
    if(!defined($component))
    {
        $expat->xpcroak("Component type $current_node->{barcode} is not defined");
    }

    # Check whether a component of this type is already defined for the system

    if(defined($system->{components}->{$current_node->{barcode}}))
    {
        if(defined($system->{components}->{$current_node->{barcode}}->{required}))
        {
            $system->{components}->{$current_node->{barcode}}->{required} += $current_node->{required};
        }
        else
        {
            $system->{components}->{$current_node->{barcode}}->{required}  = $current_node->{required};
        }
    }
    else
    {
        $system->{components}->{$current_node->{barcode}} = $current_node;
    }

    # Check whether component's parent is a group

    if($current_node->{parent}->{node_tag} eq "group")
    {
        $current_node->{group} = $current_node->{parent}->{group};

        # Check whether another system group is already defined for this component type

        if(defined($component->{sys_groups})                    &&
           defined($component->{sys_groups}->{$system->{type}}) &&
           $component->{sys_groups}->{$system->{type}} != $current_node->{group}->{index})
        {
            $expat->xpcroak("Component $current_node->{barcode} cannot be a member of multiple groups");
        }
        $component->{sys_groups}->{$system->{type}} = $current_node->{group}->{index};
    }
    else # Current node's parent is not a group
    {
        # Component is a member of the individual group for its type

        # Set required count to 1 by default

        if(!defined($current_node->{required}))
        {
            $current_node->{required} = 1;
        }

        # Check whether a group for this component type already exists

        if(defined($system->{groups}->{$current_node->{barcode}}))
        {
            $current_node->{group}              = $system->{groups}->{$current_node->{barcode}};
            $current_node->{group}->{required} += $current_node->{required};
        }
        else # Create new group for component type
        {
            $system->{groups}->{$current_node->{barcode}}   = {};
            $current_node->{group}                          = $system->{groups}->{$current_node->{barcode}};
            $current_node->{group}->{index}                 = $group_idx++;
            $current_node->{group}->{required}              = $current_node->{required};
            $current_node->{group}->{tag}                   = $current_node->{barcode};

            # Check whether another system group is already defined for this component type

            if(defined($component->{sys_groups})                    &&
               defined($component->{sys_groups}->{$system->{type}}) &&
               $component->{sys_groups}->{$system->{type}} != $current_node->{group}->{index})
            {
                $expat->xpcroak("Component $current_node->{barcode} cannot be a member of multiple groups");
            }
            $component->{sys_groups}->{$system->{type}} = $current_node->{group}->{index};
        }
    }

    $current_node->{group}->{barcodes}->{$current_node->{barcode}} = 1;

    # EPCCCS-5685: Prevent duplicated equipment properties by verifying that the system
    # does not use two components (of different groups) that share the same property category
    if(defined($component->{categories}) && scalar(@{$component->{categories}}))
    {
        # track categories used by the system so that they can be validated on system end tag handler
        for my $cat (@{$component->{categories}})
        {
            if(!exists($system->{categories}))
            {
                $system->{categories} = {};
            }
            if(!exists($system->{categories}->{$cat}))
            {
                $system->{categories}->{$cat} =
                {
                    category_name => $cat,
                    category_usages_within_system => 0,
                    groups_using_this_category => {},
                    component_names_using_this_category => [],
                }
            }
            $system->{categories}->{$cat}->{groups_using_this_category}->{$current_node->{group}->{index}} = $current_node->{group};
            push(@{$system->{categories}->{$cat}->{component_names_using_this_category}}, $component->{barcode});
        }
    }
}

sub start_dim_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{parent}->{node_tag} ne "system")
    {
        $expat->xpcroak("dim must be a child of a system");
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(bus_addr dim_idx name position type variant))
    {
        if(!defined($current_node->{$attribute}) || $current_node->{$attribute} eq '')
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check that bus_addr is valid and unique
	$expat->xpcroak("bus_addr is not a valid HEX number") if( not $current_node->{bus_addr} =~ /^(0x)?[0-9a-fA-F]+$/ );
	$expat->xpcroak("bus_addr is not unique") if( grep($_ eq hex($current_node->{bus_addr}), @bus_addr));

    $current_node->{bus_addr} = hex($current_node->{bus_addr});
    push(@bus_addr, $current_node->{bus_addr});

    # Check that DIM name is known and unique for this system

    my $name = $superhash->{dim_names}->{$current_node->{name}};
    $expat->xpcroak("Unknown DIM name") if(!defined($name));
    $expat->xpcroak("DIM names must be unique") if( grep($_ eq $current_node->{name}, @dim_names) );

    $current_node->{name_idx} = $name->{dim_name_idx};
    push(@dim_names, $current_node->{name});

    # Check that DIM type is known

    my $type = $superhash->{dim_types}->{"$current_node->{type}-$current_node->{position}$current_node->{variant}"};
    $expat->xpcroak("Unknown DIM type") if(!defined($type));

    $current_node->{type} = $type;

	# Check that dim_idx is unique
	$expat->xpcroak("dim idx '".$current_node->{dim_idx}."' already declared") if( defined $current_node->{parent}->{dims}->[$current_node->{dim_idx}] );

    $current_node->{parent}->{dims}->[$current_node->{dim_idx}] = $current_node;

}

sub start_group_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(tag))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    if($current_node->{parent}->{node_tag} ne "system")
    {
        $expat->xpcroak("group must be a child of a system");
    }

    # Check whether group with tag already exists

    if(defined($system->{groups}->{$current_node->{tag}}))
    {
        $current_node->{group}              = $system->{groups}->{$current_node->{tag}};
        $current_node->{group}->{required} += $current_node->{required};
    }
    else
    {
        $system->{groups}->{$current_node->{tag}}   = {};
        $current_node->{group}                      = $system->{groups}->{$current_node->{tag}};
        $current_node->{group}->{index}             = $group_idx++;
        $current_node->{group}->{required}          = $current_node->{required};
        $current_node->{group}->{tag}               = $current_node->{tag};
    }
}

sub start_log_menu_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if($current_node->{parent}->{node_tag} ne "system")
    {
        $expat->xpcroak("log_menu must be a child of a system");
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(name prop))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    push(@{$current_node->{parent}->{log_menus}}, $current_node);

    # Set indices

    if(defined($superhash->{log_menu_names}) &&
       defined($superhash->{log_menu_names}->{$current_node->{name}}))
    {
        $current_node->{log_menu_name_idx} = $superhash->{log_menu_names}->{$current_node->{name}};
    }
    else
    {
        $superhash->{log_menu_names}->{$current_node->{name}}   = defined($superhash->{log_menu_names}) ? keys(%{$superhash->{log_menu_names}}) : 0;
        $current_node->{log_menu_name_idx}                      = $superhash->{log_menu_names}->{$current_node->{name}};
    }

    if(defined($superhash->{log_menu_props}) &&
       defined($superhash->{log_menu_props}->{$current_node->{prop}}))
    {
        $current_node->{log_menu_prop_idx} = $superhash->{log_menu_props}->{$current_node->{prop}};
    }
    else
    {
        $superhash->{log_menu_props}->{$current_node->{prop}}   = defined($superhash->{log_menu_props}) ? keys(%{$superhash->{log_menu_props}}) : 0;
        $current_node->{log_menu_prop_idx}                      = $superhash->{log_menu_props}->{$current_node->{prop}};
    }
}

sub start_system_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if(defined($current_node->{parent}))
    {
        $expat->xpcroak("system element must be at the top-level");
    }

    # Check that appropriate attributes are defined

    if(!defined($current_node->{comp}))
    {
        $expat->xpcroak("The attribute comp must be defined");
    }
    elsif(!defined($superhash->{components}->{$current_node->{comp}}))
    {
        $expat->xpcroak("The component $current_node->{comp} is not defined in components.xml");
    }
    else
    {
        $current_node->{label} = $superhash->{component_labels}->{$current_node->{comp}}->{label};
    }

    for my $attr (keys(%{$current_node}))
    {
        # Check if current attribiute is a flag one

        my ( $flag_name ) = ($attr =~ /^flag_(\w+)/i);

        if($flag_name)
        {
            # Create flags subhash if it does not exist yet

            if(!defined($current_node->{flags}))
            {
                # Collection of all flags grouped together

                $current_node->{flags} = {};
            }

            # Move flag to dedicatated subhash

            $current_node->{flags}->{$flag_name} = $current_node->{$attr};
            undef($current_node->{$attr});
        }
    }

    # Reset group index

    $group_idx = 1;

    # Reset dim addr_idx

    @bus_addr = ();
    @dim_names = ();

    $system = $current_node;
    $system->{type} = $superhash->{current_system_type};
    $superhash->{systems}->{$current_node->{type}} = $system;
}


sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    if(defined($end_tag_handlers{$current_node->{node_tag}}))
    {
        # Handler has been defined, call it

        $end_tag_handlers{$current_node->{node_tag}}($expat, $element, $current_node, $superhash);
    }
}

sub end_system_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;


    # EPCCCS-5685: Prevent duplicated equipment properties by verifying that the system
    # does not use two components (of different groups) that share the same property category

    if(!defined($system->{categories}))
    {
        return;
    }
    for my $cat_name (keys %{$system->{categories}})
    {
        my $cat = $system->{categories}->{$cat_name};

        # gather number of usages per category within same system
        for my $group_tag (keys %{$cat->{groups_using_this_category}})
        {
            my $group = $cat->{groups_using_this_category}->{$group_tag};
            $cat->{category_usages_within_system} += $group->{required};
        }

        # categories are not allowed to be used more than twice because of potential property duplication
        if($cat->{category_usages_within_system} > 2)
        {
            $expat->xpcroak(
                "Too many usages of property category '$cat_name' within system type '$system->{type}' (in total required > 2). Used by following components: " . join(',', @{$cat->{component_names_using_this_category}})
            );
        }
        # categories that are used twice, must be of a special kind (containing .?. properties).
        if($cat->{category_usages_within_system} == 2)
        {
            my $allowed_shared_cats_regex = qr/^(LEM|EXADC|BC_EXADC|DCCT_HEAD|DCCT_ELEC|BC_DCCT_ELEC|BC_DCCT_HEAD|ONE_BC_DCCT_CAL|ONE_BC_DCCT_HEAD)$/;
            my $is_allowed_to_be_shared = $cat_name =~ /$allowed_shared_cats_regex/;
            if(!$is_allowed_to_be_shared)
            {
                $expat->xpcroak(
                "The property category '$cat_name' is used twice within system type '$system->{type}'. Used by following components: " . join(',', @{$cat->{component_names_using_this_category}}) . ". Only .?. categories are supported to be used (exactly) twice: $allowed_shared_cats_regex"
                );
            }
        }
    }
}

# EOF
