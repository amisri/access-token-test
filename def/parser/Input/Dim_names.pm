#!/usr/bin/perl -w
#
# Name:     Input/Dim_names.pm
# Purpose:  Provides functions relating to DIM names
# Author:   Stephen Page

package Input::Dim_names;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %start_tag_handlers = (
                            "dim_name"  => \&start_dim_name_tag_handler,
                            "dim_names" => \&start_dim_names_tag_handler,
                         );

my $dim_name_idx;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check whether a handler has been defined for the tag

    if(defined($start_tag_handlers{$current_node->{node_tag}}))
    {
        # Handler has been defined, call it

        $start_tag_handlers{$current_node->{node_tag}}($expat, $element, $current_node, $superhash);
    }
    else
    {
        # No handler defined for tag, return error

        $expat->xpcroak("No start tag handler for tag $current_node->{node_tag}");
    }
}

sub start_dim_name_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that parent is a dim_names element

    if(!defined($current_node->{parent}) ||
       $current_node->{parent}->{node_tag} ne "dim_names")
    {
        $expat->xpcroak("Parent must be dim_names");
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(name))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    $current_node->{dim_name_idx} = $dim_name_idx++;
}

sub start_dim_names_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that this is the root node

    if(defined($current_node->{parent}))
    {
        $expat->xpcroak("Diag type must be root node");
    }

    $dim_name_idx = 0;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
