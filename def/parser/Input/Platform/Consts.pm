#!/usr/bin/perl -w
#
# Name:     Input/Platform/Consts.pm
# Purpose:  Provides functions relating to the platform constants
# Author:   Stephen Page

package Input::Platform::Consts;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
