#!/usr/bin/perl -w
#
# Name:     Input/Platform/Parser.pm
# Purpose:  Provides functions to parse platform definitions
# Author:   Stephen Page

package Input::Platform::Parser;

use Exporter;
use Input::Platform::Boot::Menu;
use Input::Platform::Consts;
use Input::Platform::Hw::Connectors;
use Input::Platform::Hw::Main;
use Input::Platform::Hw::Xilinxes;
use Input::Platform::Main;
use Input::Platform::Memmap;
use Output::Doc::Writer;
use Parser;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(parse);

return(1);


# Begin functions

sub parse($$$$)
{
    my ($projpath, $superhash, $roots, $platform_dir) = @_;

    # Derive platform name from platform directory name

    my $name    =  $platform_dir;
    $name       =~ s#.*/(.*)#$1#;

    print "********** Beginning parsing platform $name **********\n\n";

    # Check that the main definition for the platform exists

    if(! -f "$platform_dir/main.xml")
    {
        confess "$platform_dir\\main.xml(9999): ERROR Main definition file for platform with name $name does not exist\n";
    }

    # Parse the main definition for the platform

    my $platform_node = Parser::parse(
                                        doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                        doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                        file                => "$platform_dir/main.xml",
                                        node_tags           => { area => 1, link => 1, platform => 1 },
                                        node_start_handler  => \&Input::Platform::Main::start_tag_handler,
                                        node_end_handler    => \&Input::Platform::Main::end_tag_handler,
                                        superhash           => $superhash,
                                     );
    $platform_node->{name}              = $name;
    $superhash->{platforms}->{$name}    = $platform_node;

    # Add current platform reference to superhash temporarily for reference when parsing

    $superhash->{current_platform} = $platform_node;

    # If a constants file exists for this platform, parse it

    if(-f "$platform_dir/consts.xml")
    {
        $platform_node->{consts} =  Parser::parse(
                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                    file                => "$platform_dir/consts.xml",
                                                    node_start_handler  => \&Input::Platform::Consts::start_tag_handler,
                                                    node_end_handler    => \&Input::Platform::Consts::end_tag_handler,
                                                    superhash           => $superhash,
                                                 );

        # Remove the node tag from platform's consts

        delete($platform_node->{consts}->{node_tag});
    }

    # Get a list of hardware directories

    my @hardware_dirs = <$platform_dir/hw/*>;

    for my $hardware_dir (@hardware_dirs)
    {
        # Derive hardware name from hardware directory name

        my $hardware    =  $hardware_dir;
        $hardware       =~ s#.*/(.*)#$1#;

        # Add current hardware name to platform temporarily for reference when parsing

        $platform_node->{current_hardware_name} = $hardware;

        # Check that main attributes file exists

        if(! -f "$hardware_dir/main.xml")
        {
            confess "$hardware_dir\\main.xml(9999): ERROR Main definition file for hardware $hardware for platform $name does not exist\n";
        }

        # Parse main attributes

        $platform_node->{hardware}->{$hardware} = Parser::parse(
                                                                doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                file                => "$hardware_dir/main.xml",
                                                                node_start_handler  => \&Input::Platform::Hw::Main::start_tag_handler,
                                                                node_end_handler    => \&Input::Platform::Hw::Main::end_tag_handler,
                                                                superhash           => $superhash,
                                                               );

        # Parse xilinxes file if one exists

        if(-f "$hardware_dir/xilinxes.xml")
        {
            $platform_node->{hardware}->{$hardware}->{xilinxes} = Parser::parse(
                                                                                doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                                doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                                file                => "$hardware_dir/xilinxes.xml",
                                                                                node_tags           => { xilinx => 1, program => 1 , revision => 1 },
                                                                                node_start_handler  => \&Input::Platform::Hw::Xilinxes::start_tag_handler,
                                                                                node_end_handler    => \&Input::Platform::Hw::Xilinxes::end_tag_handler,
                                                                                superhash           => $superhash,
                                                                               );

            # Move xilinxes down one level to move past root

            $platform_node->{hardware}->{$hardware}->{xilinxes} = $platform_node->{hardware}->{$hardware}->{xilinxes}->{children};
        }

        # Parse connectors file if one exists

        if(-f "$hardware_dir/connectors.xml")
        {
            $platform_node->{hardware}->{$hardware}->{connectors} = Parser::parse(
                                                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                                    file                => "$hardware_dir/connectors.xml",
                                                                                    node_tags           => { connector => 1, pin => 1 },
                                                                                    node_start_handler  => \&Input::Platform::Hw::Connectors::start_tag_handler,
                                                                                    node_end_handler    => \&Input::Platform::Hw::Connectors::end_tag_handler,
                                                                                    superhash           => $superhash,
                                                                                 );

            # Move connectors down one level to move past root

            $platform_node->{hardware}->{$hardware}->{connectors} = $platform_node->{hardware}->{$hardware}->{connectors}->{children};
        }
        else
        {
            # Add reference to empty array for connectors

            $platform_node->{hardware}->{$hardware}->{connectors} = [];
        }

        # Remove temporary current hardware name from platform

        delete($platform_node->{current_hardware_name});
    }

    # Convert sub-hardware to references

    if(defined($platform_node->{hardware}))
    {
        for my $hardware (values(%{$platform_node->{hardware}}))
        {
            if(defined($hardware->{subhw}))
            {
                for my $subhw (@{$hardware->{subhw}})
                {
                    if(defined($platform_node->{hardware}->{$subhw}))
                    {
                        $subhw = $platform_node->{hardware}->{$subhw};
                    }
                    else
                    {
                        confess "$platform_dir\\hw\\$hardware->{label}\\main.xml(1): ERROR Sub-hardware $subhw does not exist\n";
                    }
                }
            }
        }
    }

    # Parse boot for device platforms

    if($platform_node->{id} >= 50)
    {
        # Parse boot menu if definition file exists

        if(-f "$platform_dir/boot/menu.xml")
        {
            $platform_node->{boot}->{menu} = Parser::parse(
                                                            doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                            doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                            file                => "$platform_dir/boot/menu.xml",
                                                            node_tags           => { arg => 1, menu => 1, return => 1 },
                                                            node_start_handler  => \&Input::Platform::Boot::Menu::start_tag_handler,
                                                            node_end_handler    => \&Input::Platform::Boot::Menu::end_tag_handler,
                                                            superhash           => $superhash,
                                                          );
        }
    }

    my @memmap_files = <$platform_dir/memmaps/*.xml>;

    for my $memmap_file (@memmap_files)
    {
        (my $memmap_name = $memmap_file) =~ s/.*\/(.*).xml/\U$1/;

        $platform_node->{memmaps}->{$memmap_name} = Parser::parse(
                                                                    doc_links           => Output::Doc::Writer::get_docinfo()->{links},
                                                                    doc_splinks         => Output::Doc::Writer::get_docinfo()->{splinks},
                                                                    file                => $memmap_file,
                                                                    node_start_handler  => \&Input::Platform::Memmap::start_tag_handler,
                                                                    node_end_handler    => \&Input::Platform::Memmap::end_tag_handler,
                                                                    superhash           => $superhash,
                                                                 );
    }
    # Remove temporary current platform reference from superhash

    delete($superhash->{current_platform});

    print "********** Parsing of platform $name completed successfully **********\n\n";
}

# EOF
