#!/usr/bin/perl -w
#
# Name:     Input/Platform/Memmap.pm
# Purpose:  Provides functions relating to the platform memory map
# Author:   Stephen Page

package Input::Platform::Memmap;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my $origin;
my %zone_fullnames;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(comment name offset size))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    if(defined($current_node->{parent}))
    {
        # Node is not root, so use parent's full name to form this node's name

        if($current_node->{parent}->{fullname} ne "")
        {
            $current_node->{fullname} = "$current_node->{parent}->{fullname}_$current_node->{name}";
        }
        else # Parent is nameless root node
        {
            $current_node->{fullname} = $current_node->{name};
        }

        # Default to parent's endianness if endianness not defined

        if(!defined($current_node->{endian}))
        {
            $current_node->{endian} = $current_node->{parent}->{endian};
        }
    }
    else  # Node is the root node
    {
        # The root node's full name is equal to its name

        $current_node->{fullname} = $current_node->{name};

        # Empty %zone_fullnames

        %zone_fullnames = ();

        # Default to big-endian if endianness not defined

        $current_node->{endian} = "big" if(!defined($current_node->{endian}));
    }

    # Check for a duplicate full name

    if(defined($zone_fullnames{$current_node->{fullname}}))
    {
        $expat->xpcroak("Zone name $current_node->{fullname} is duplicated");
    }

    $zone_fullnames{$current_node->{fullname}} = 1;

    # Check that endianness is valid

    if($current_node->{endian} ne "little" && $current_node->{endian} ne "big")
    {
        $expat->xpcroak("Endian must be \"big\" or \"little\"");
    }

    # Check validity of supplied access rule

    if  (
            defined($current_node->{access})    &&
            $current_node->{access} ne 'R'      &&
            $current_node->{access} ne 'W'      &&
            $current_node->{access} ne 'RW'
        )
    {
        $expat->xpcroak("Invalid access rule for zone");
    }

    # Add reference to platform

    $current_node->{platform} = $superhash->{current_platform};

    # Convert offset to an integer number of bits

    my %memsizes = (
                     b => 1,            # Bit
                     B => 8,            # Byte
                     K => 8192,         # Kilobyte
                     M => 8388608,      # Megabyte
                     G => 8589934592,   # Gigabyte
                   );

    # Convert offset to decimal if it is in hex

    if($current_node->{offset} =~ /^0x\w+/) # Offset is in hex
    {
        $current_node->{offset} =~ s/^(0x\w+)(\D)$/hex($1).$2/e;
    }

    # Check that offset is validly formatted and convert to bits

    if($current_node->{offset} =~ /^\d+\D$/)
    {
        # Get offset in bits

        $current_node->{offset} =~ s/^(\d+)(\D)$/$1/;

        my $units = $2;

        if(defined($memsizes{$units}))
        {
            $current_node->{offset} *= $memsizes{$units};
        }
        else
        {
            $expat->xpcroak("Invalid unit character \'$units\' specified for offset of zone $current_node->{name}");
        }
    }
    else
    {
        $expat->xpcroak("Zone $current_node->{name} has invalidly formatted offset field");
    }

    # Get the address of the start of this zone

    if(!defined($current_node->{parent})) # root node
    {
        $current_node->{address} = $current_node->{offset};
    }
    else # Low-level node
    {
        $current_node->{address} = $current_node->{parent}->{address} + $current_node->{offset};
    }

    # Set the byte address for this zone (rounded down)

    $current_node->{byte_address} = int($current_node->{address} / 8);

    # Convert size to decimal if it is in hex

    if($current_node->{size} =~ /^0x\w+/) # Size is in hex
    {
        $current_node->{size} =~ s/^(0x\w+)(\D)$/hex($1).$2/e;
    }

    # Check that size is validly formatted and get number of bits

    if($current_node->{size} =~ /^\d+\D$/)
    {
        # Get size in bits

        $current_node->{numbits} = $current_node->{size};
        $current_node->{numbits} =~ s/^(\d+)(\D)$/$1/;

        my $units = $2;

        if(defined($memsizes{$units}))
        {
            $current_node->{numbits} *= $memsizes{$units};
        }
        else
        {
            $expat->xpcroak("Invalid unit character \'$units\' specified for size of zone $current_node->{name}");
        }
    }
    else
    {
        $expat->xpcroak("Zone $current_node->{name} has invalidly formatted size field");
    }

    # Check that this node does not overrun the end of its parent

    if(defined($current_node->{parent}) &&
       ($current_node->{offset} + $current_node->{numbits}) > $current_node->{parent}->{numbits})
    {
        $expat->xpcroak("Zone $current_node->{name} overruns the end of its parent $current_node->{parent}->{name}");
    }

    # Check whether the zone was set as an origin

    if(defined($current_node->{origin}))
    {
        if(defined($origin))
        {
            $expat->xpcroak("Origin already defined as ".sprintf("0x%08X", $origin));
        }
        $origin = $current_node->{byte_address};
    }

    # Set the origin offset for the zone

    if(defined($origin))
    {
        $current_node->{origin_offset} = $current_node->{byte_address} - $origin;
    }

    # Use hash to store desired constant types

    $current_node->{const_types} = {};

    # Validate requested constants

    if(defined($current_node->{consts}))
    {
        my @consts = split(/\s+/, $current_node->{consts});

        for my $const_type (@consts)
        {
            if  (
                    $const_type ne "16bit"          &&
                    $const_type ne "32bit"          &&
                    $const_type ne "array"          &&
                    $const_type ne "far"            &&
                    $const_type ne "mask8"          &&
                    $const_type ne "mask16"         &&
                    $const_type ne "mask32"         &&
                    $const_type ne "near"           &&
                    $const_type ne "origin_near"    &&
                    $const_type ne "origin_array"   &&
                    $const_type ne "origin_far"     &&
                    $const_type ne "page"           &&
                    $const_type ne "shift"          &&
                    $const_type ne "sizebits"       &&
                    $const_type ne "sizebytes"      &&
                    $const_type ne "sizewords"
                )
            {
                $expat->xpcroak("$const_type is not a valid constant type");
            }

            # Capitalise constant type

            $const_type = "\U$const_type";

            # Flag type as desired

            $current_node->{const_types}->{$const_type} = 1;
        }
    }

    # Check whether type needs to be defined

    if((defined($current_node->{const_types}->{NEAR})         ||
        defined($current_node->{const_types}->{FAR})          ||
        defined($current_node->{const_types}->{ARRAY})        ||
        defined($current_node->{const_types}->{ORIGIN_NEAR})  ||
        defined($current_node->{const_types}->{ORIGIN_FAR})   ||
        defined($current_node->{const_types}->{ORIGIN_ARRAY})) && !defined($current_node->{type}))
    {
        $expat->xpcroak("Zone $current_node->{name} requires type to be defined");
    }

    # Check for address byte alignment when 16BIT, 32BIT, ARRAY, NEAR or FAR constants requested

    if  (
            (
                defined($current_node->{const_types}->{'PAGE'})         ||
                defined($current_node->{const_types}->{'16BIT'})        ||
                defined($current_node->{const_types}->{'32BIT'})        ||
                defined($current_node->{const_types}->{'NEAR'})         ||
                defined($current_node->{const_types}->{'FAR'})          ||
                defined($current_node->{const_types}->{'ARRAY'})        ||
                defined($current_node->{const_types}->{'ORIGIN_NEAR'}) ||
                defined($current_node->{const_types}->{'ORIGIN_ARRAY'}) ||
                defined($current_node->{const_types}->{'ORIGIN_FAR'})
            )
            && $current_node->{address} % 8 != 0
        )
    {
        $expat->xpcroak("Zone $current_node->{name} requires address to be byte aligned for constant");
    }

    # Create mask if necessary

    if(defined($current_node->{const_types}->{MASK8})   ||
       defined($current_node->{const_types}->{MASK16})  ||
       defined($current_node->{const_types}->{MASK32}))
    {
        $current_node->{mask} = 0;

        my $mask_size;
        if(defined($current_node->{const_types}->{MASK8}))
        {
            $mask_size = 8;
        }
        elsif(defined($current_node->{const_types}->{MASK16}))
        {
            $mask_size = 16;
        }
        else # (defined($current_node->{const_types}->{MASK32}))
        {
            $mask_size = 32;
        }

        # Calculate mask

        my $offset = $current_node->{address} % $mask_size;

        # Return error if mask is longer than limit

        if($offset + $current_node->{numbits} > $mask_size)
        {
            $expat->xpcroak("Mask length exceeds $mask_size bit limit");
        }

        for(my $i = 0 ; $i < $current_node->{numbits} ; $i++)
        {
            $current_node->{mask} += 2 ** ($offset + $i);
        }

        if(defined($current_node->{const_types}->{MASK8}))
        {
            # Create shift if necessary

            if(defined($current_node->{const_types}->{SHIFT}))
            {
                $current_node->{shift} = $offset;
            }
        }
        elsif(defined($current_node->{const_types}->{MASK16}))
        {
            # Create shift if necessary

            $current_node->{shift} = $offset;

            # Swap bytes in mask and offset

            if($current_node->{endian} eq "big")
            {
                $current_node->{mask} = unpack('n', pack('S', $current_node->{mask}));

                if(defined($current_node->{const_types}->{SHIFT}))
                {
                    my @byte_swapped_shift  = ( 8,  9, 10, 11, 12, 13, 14, 15,
                                                0,  1,  2,  3,  4,  5,  6,  7);
                    $current_node->{shift}  = $byte_swapped_shift[$offset];
                }
            }
        }
        elsif(defined($current_node->{const_types}->{MASK32}))
        {
            # Create shift if necessary

            $current_node->{shift} = $offset;

            # Swap bytes in mask and offset

            if($current_node->{endian} eq "big")
            {
                $current_node->{mask} = unpack('N', pack('L', $current_node->{mask}));

                if(defined($current_node->{const_types}->{SHIFT}))
                {
                    my @byte_swapped_shift  = (24, 25, 26, 27, 28, 29, 30, 31,
                                               16, 17, 18, 19, 20, 21, 22, 23,
                                                8,  9, 10, 11, 12, 13, 14, 15,
                                                0,  1,  2,  3,  4,  5,  6,  7);
                    $current_node->{shift}  = $byte_swapped_shift[$offset];
                }
            }
        }
    }
    elsif(defined($current_node->{const_types}->{SHIFT}))
    {
        expat->xpcroak("shift constant requested without mask");
    }

    # Check that the origin is set if an origin-based constant is requested

    if(!defined($current_node->{origin_offset})                 &&
       (defined($current_node->{const_types}->{ORIGIN_NEAR})    ||
        defined($current_node->{const_types}->{ORIGIN_FAR})     ||
        defined($current_node->{const_types}->{ORIGIN_ARRAY})))
    {
        $expat->xpcroak("No origin is defined, but an origin-based constant is requested");
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Calculate access rule from children's rules if not manually

    if(!defined($current_node->{access}))
    {
        if(defined($current_node->{children}))
        {
            my $readable_flag   = 0;
            my $writeable_flag  = 0;

            for my $child (@{$current_node->{children}})
            {
                if($child->{access} eq "R")
                {
                    $readable_flag = 1;
                }
                elsif($child->{access} eq "W")
                {
                    $writeable_flag = 1;
                }
                elsif($child->{access} eq "RW")
                {
                    $readable_flag = $writeable_flag = 1;
                }

                last if($writeable_flag && $readable_flag);
            }

            # Set access mode

            $current_node->{access}  = "";
            $current_node->{access}  = "R" if($readable_flag);
            $current_node->{access} .= "W" if($writeable_flag);
        }
        else
        {
            # Default to READ/WRITE

            $current_node->{access} = "RW";
        }
    }

    # If this node is the origin, clear it

    if(defined($current_node->{origin}))
    {
        $origin = undef;
    }
}

# EOF
