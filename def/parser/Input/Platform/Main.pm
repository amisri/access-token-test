#!/usr/bin/perl -w
#
# Name:     Input/Platform/Main.pm
# Purpose:  Provides functions relating to the FGC platforms' main definitions
# Author:   Stephen Page

package Input::Platform::Main;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %platform_ids;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
        # Handle top-level area

        if($current_node->{parent}->{node_tag} eq "platform") # Top-level area
        {
            $current_node->{parent}->{sitemap} = $current_node;
        }
        else # low-level area
        {
            # Check that appropriate attributes are defined

            for my $attribute (qw(name))
            {
                if(!defined($current_node->{$attribute}))
                {
                    $expat->xpcroak("$attribute not defined");
                }
            }
        }
    }
    elsif($element eq "link")
    {
        if($current_node->{parent}->{node_tag} ne "area")
        {
            $expat->xpcroak("link must be a child of an area");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(name url))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Add link to parent's links

        push(@{$current_node->{parent}->{links}}, $current_node);
    }
    elsif($element eq "platform")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(id title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check for a duplicate platform ID

        if(defined($platform_ids{$current_node->{id}}))
        {
            $expat->xpcroak("Platform ID $current_node->{id} is duplicated");
        }
        else
        {
            $platform_ids{$current_node->{id}} = 1;
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
    }
    elsif($element eq "link")
    {
        # Remove from parent's children

        pop(@{$current_node->{parent}->{children}});
    }
    elsif($element eq "platform")
    {
    }
}

# EOF
