#!/usr/bin/perl -w
#
# Name:     Input/Platform/Hw/Xilinxes.pm
# Purpose:  Provides functions relating to Xilinx chips for platform hardware
# Author:   Stephen Page

package Input::Platform::Hw::Xilinxes;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # This file may contain <xilinx>, <program> and <revision> tags - handle them appropriately

    if($current_node->{node_tag} eq "xilinx")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(logiware purpose type))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Add reference to hardware

        my $hardware_name   = $superhash->{current_platform}->{current_hardware_name};
        my $hardware        = $superhash->{current_platform}->{hardware}->{$hardware_name};

        $current_node->{hardware} = $hardware;

        # If Xilinx is not on the tester, make it explicit

        $current_node->{tester} = 0 if(!defined($current_node->{tester}));

        if($current_node->{tester})
        {
            $hardware->{tester_xilinxes}++;
        }
        else # Xilinx is not on a tester
        {
            # Give the Xilinx an IC number

            $hardware->{last_xilinx_number} = 1 if(!defined($hardware->{last_xilinx_number}));
            $current_node->{ic_number}      = $hardware->{last_xilinx_number}++;
        }
    }
    elsif($current_node->{node_tag} eq "program")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(revision title))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check that parent is a Xilinx

        if($current_node->{parent}->{node_tag} ne "xilinx")
        {
            $expat->xpcroak("Parent of program must be Xilinx");
        }

        # Check that parent is not on the tester

        if($current_node->{parent}->{tester})
        {
            $expat->xpcroak("Tester Xilinxes may not have programs");
        }

        # Add program ID

        if(!defined($current_node->{parent}->{last_program_id}))
        {
            $current_node->{parent}->{last_program_id} = 'A';
        }

        $current_node->{id} = $current_node->{parent}->{last_program_id}++;
    }
    elsif($current_node->{node_tag} eq "revision")
    {
        # Check that parent is a Xilinx

        if($current_node->{parent}->{node_tag} ne "program")
        {
            $expat->xpcroak("Parent of revision must be program");
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    if($current_node->{node_tag} eq "xilinx" && !$current_node->{tester})
    {
        my $revision = 1;

        # Convert children to programs

        if(defined($current_node->{children}))
        {
            $current_node->{programs}   = $current_node->{children};
            $revision                   = $current_node->{programs}->[0]->{revision};
        }

        # Construct usercode

        my $usercode_dev;
        if(defined($current_node->{hardware}->{usercode_dev}))
        {
            $usercode_dev = $current_node->{hardware}->{usercode_dev};
        }
        else
        {
            $usercode_dev = substr($current_node->{hardware}->{lhc_id}, 5, 1);
        }

        $current_node->{usercode} = $usercode_dev.$current_node->{ic_number}."A".$revision;
    }
    elsif($current_node->{node_tag} eq "program")
    {
        # Convert children to revisions

        if(!defined($current_node->{children}))
        {
            $expat->xpcroak("Program has no revisions");
        }

        $current_node->{revisions} = $current_node->{children};
    }
    elsif($current_node->{node_tag} eq "revision")
    {
        # Check that revision has documentation

        if(!defined($current_node->{doc}))
        {
            $expat->xpcroak("Revision has no documentation");
        }
    }
}

# EOF
