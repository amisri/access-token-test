#!/usr/bin/perl -w
#
# Name:     Input/Platform/Hw/Connectors.pm
# Purpose:  Provides functions relating to hardware's connectors
# Author:   Stephen Page

package Input::Platform::Hw::Connectors;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %connectors;
my %pins;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag

    return if(defined($current_node->{pseudoroot}));

    # This file may contain <connector> and <pin> tags - handle them appropriately

    if($current_node->{node_tag} eq "connector")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(id name type))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check whether connector has already been defined

        if(defined($connectors{$current_node->{id}}))
        {
            $expat->xpcroak("Connector $current_node->{id} duplicated");
        }

        $connectors{$current_node->{id}} = 1;

        # Create empty pin array for this connector

	$current_node->{pins} = [];

        # Clear pins hash

        %pins = ();
    }
    elsif($current_node->{node_tag} eq "pin")
    {
        # Return error if parent node is not a connector

        if($current_node->{parent}->{node_tag} ne "connector")
        {
            $expat->xpcroak("Pin has wrong parent node type");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(label number sig_type use))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Check whether pin has already been assigned

        if(defined($pins{$current_node->{number}}))
        {
            $expat->xpcroak("Pin $current_node->{number} duplicated");
        }

        $pins{$current_node->{number}} = 1;

        # Check that sig_type is value

        my $valid_flag = 0;
        for my $sig_type (qw(A D G I P S U))
        {
            if($current_node->{sig_type} eq $sig_type)
            {
                $valid_flag = 1;
                last;
            }
        }

        if(!$valid_flag)
        {
            $expat->xpcroak("Invalid sig_type \'$current_node->{sig_type}\'");
        }

	# Add pin to connector's pin array

	push(@{$current_node->{parent}->{pins}}, $current_node);
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle pseudoroot tag (EOF)

    if(defined($current_node->{pseudoroot}))
    {
        # Clear connectors hash

        %connectors = ();

        return;
    }
}

# EOF
