#!/usr/bin/perl -w
#
# Name:     Input/Platform/Hw/Main.pm
# Purpose:  Provides functions relating to the main attributes of a piece of platform hardware
# Author:   Stephen Page

package Input::Platform::Hw::Main;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %lhc_ids;
my %ab_po_ids;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Check that appropriate attributes are defined

    for my $attribute (qw(lhc_id ab_po_id title label))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check for a duplicate LHC ID (global)

    if(defined($lhc_ids{$current_node->{lhc_id}}))
    {
        $expat->xpcroak("LHC ID $current_node->{lhc_id} is duplicated");
    }
    else
    {
        $lhc_ids{$current_node->{lhc_id}} = 1;
    }

    # Check for a duplicate AB-PO ID (global)

    if(defined($ab_po_ids{$current_node->{ab_po_id}}))
    {
        $expat->xpcroak("AB-PO ID $current_node->{ab_po_id} is duplicated");
    }
    else
    {
        $ab_po_ids{$current_node->{ab_po_id}} = 1;
    }

    # Check for a duplicate label (within this platform)

    for my $hardware (@{$current_node->{parent}->{children}})
    {
        if($hardware != $current_node &&
           $hardware->{label} eq $current_node->{label})
        {
            $expat->xpcroak("Duplicate label");
        }
    }

    # Set platform

    $current_node->{platform} = $superhash->{current_platform};

    # Set count of tester Xilinxes to default of 0

    $current_node->{tester_xilinxes} = 0;

    # Split sub-hardware if defined

    if(defined($current_node->{subhw}))
    {
        my @subhw               = split(' ', $current_node->{subhw});
        $current_node->{subhw}  = \@subhw;
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;
}

# EOF
