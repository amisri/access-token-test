#!/usr/bin/perl -w
#
# Name:     Input/Platform/Boot/Menu.pm
# Purpose:  Provides functions relating to the boot menu definition
# Author:   Stephen Page

package Input::Platform::Boot::Menu;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %cnames;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag

    return if(defined($current_node->{pseudoroot}));

    # This file may contain <arg>, <menu> and <return> tags - handle them appropriately

    if($current_node->{node_tag} eq "arg")
    {
        # Return error if parent node is not a menu

        if($current_node->{parent}->{node_tag} ne "menu")
        {
            $expat->xpcroak("Arg has wrong parent node type");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(desc type))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # If node is implicitly not optional make it explicitly not optional

        if(!defined($current_node->{optional}))
        {
            $current_node->{optional} = 0;
        }

        # Push onto parent's args array

        push(@{$current_node->{parent}->{args}}, $current_node);

        # Remove from parent's children

        pop(@{$current_node->{parent}->{children}});
    }
    elsif($current_node->{node_tag} eq "menu")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(name))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Set platform

        $current_node->{platform} = $superhash->{current_platform};

        # Clear C names if top-node (no parent) and set ID

        if(!defined($current_node->{parent})) # Root node
        {
            %cnames             = ();
            $current_node->{id} = "";
        }
        else
        {
            $current_node->{id} = $current_node->{parent}->{id}.(scalar(@{$current_node->{parent}->{children}}) - 1);
        }

        # Convert name for use in C (de-capitalise and replace non-word characters with underscores)

        $current_node->{cname}  =  lc($current_node->{name});
        $current_node->{cname}  =~ s/\W/_/g;

        # Check whether the C name for this node clashes with another

        if(defined($cnames{$current_node->{cname}}))
        {
            $expat->xpcroak("C name $current_node->{cname} duplicated");
        }
        $cnames{$current_node->{cname}} = 1;

        # Explicitly make node non-fatal if not set as fatal

        $current_node->{fatal} = 0 if(!defined($current_node->{fatal}));

        # Set confirm flag

        if(!defined($current_node->{confirm}))
        {
            $current_node->{confirm} = defined($current_node->{parent}) ? $current_node->{parent}->{confirm} : 0;
        }

        # Set recursive flag

        if(!defined($current_node->{recurse}))
        {
            $current_node->{recurse} = defined($current_node->{parent}) ? $current_node->{parent}->{recurse} : 0;
        }
    }
    elsif($current_node->{node_tag} eq "return")
    {
        # Return error if parent node is not a menu

        if($current_node->{parent}->{node_tag} ne "menu")
        {
            $expat->xpcroak("Return has wrong parent node type");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(desc type))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Push onto parent's return array

        push(@{$current_node->{parent}->{return}}, $current_node);

        # Remove from parent's children

        pop(@{$current_node->{parent}->{children}});
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag

    return if(defined($current_node->{pseudoroot}));

    if($current_node->{node_tag} eq "menu")
    {
        # A node must have either children or a function, but never both

        if(defined($current_node->{children})           &&
           scalar(@{$current_node->{children}}) != 0    &&
           defined($current_node->{function}))
        {
            $expat->xpcroak("Node has both children and function");
        }
        elsif(!defined($current_node->{children}) &&
              !defined($current_node->{function}))
        {
            $expat->xpcroak("Node must have either children or function defined");
        }

        # A recursive node may not have arguments

        if($current_node->{recurse} && defined($current_node->{args}))
        {
            $expat->xpcroak("Recursive nodes may not have arguments");
        }
    }
}

# EOF
