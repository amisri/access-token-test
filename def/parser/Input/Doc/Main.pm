#!/usr/bin/perl -w
#
# Name:     Input/Doc/Main.pm
# Purpose:  Provides functions relating to the main documentation
# Author:   Stephen Page

package Input::Doc::Main;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
        # Check that appropriate attributes are defined

        for my $attribute (qw(name))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }
    }
    elsif($element eq "link")
    {
        if($current_node->{parent}->{node_tag} ne "area")
        {
            $expat->xpcroak("link must be a child of an area");
        }

        # Check that appropriate attributes are defined

        for my $attribute (qw(name url))
        {
            if(!defined($current_node->{$attribute}))
            {
                $expat->xpcroak("$attribute not defined");
            }
        }

        # Add link to parent's links

        push(@{$current_node->{parent}->{links}}, $current_node);
    }
    elsif($element eq "main")
    {
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Handle different elements

    if($element eq "area")
    {
    }
    elsif($element eq "link")
    {
        # Remove from parent's children

        pop(@{$current_node->{parent}->{children}});
    }
    elsif($element eq "main")
    {
    }
}

# EOF
