#!/usr/bin/perl -w
#
# Name:     Input/Error.pm
# Purpose:  Provides functions relating to the FGC errors
# Author:   Stephen Page

package Input::Error;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %error_messages;
my $error_number    = 0;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Check that appropriate attributes are defined

    for my $attribute (qw(message))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check for a duplicate error message

    if(defined($error_messages{$current_node->{message}}))
    {
        $expat->xpcroak("Error message $current_node->{message} is duplicated");
    }

    $error_messages{$current_node->{message}} = 1;

    # Give the error a number

    $current_node->{number} = $error_number++;

    # Check include attribute

    if(defined($current_node->{include}))
    {
        my @platform_names = split(" ", $current_node->{include});

        # Convert include into a hash

        $current_node->{include} = {};

        # Validate platforms in include list and add to include hash

        for my $platform_name (@platform_names)
        {
            if(!defined($superhash->{platforms}->{$platform_name}))
            {
                $expat->xpcroak("Platform $platform_name does not exist");
            }

            $current_node->{include}->{$platform_name} = $superhash->{platforms}->{$platform_name};
        }
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
             
    return if(defined($current_node->{pseudoroot}));

    # Check that doc is defined
     
    if(!defined($current_node->{doc}))
    {
        $expat->xpcroak("Error $current_node->{message} has no doc");
    }
}

# EOF
