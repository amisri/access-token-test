#!/usr/bin/perl -w
#
# Name:     Input/Getoption.pm
# Purpose:  Provides functions relating to the FGC get options
# Author:   Stephen Page

package Input::Getoption;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my $getoption_count     = 0;
my $max_getoptions      = 16;
my %getoption_symbols;
my $bitmask             = 0x0001;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
     
    return if(defined($current_node->{pseudoroot}));

    $getoption_count++;

    # Check that the maximum number of getoptions has not been exceeded

    if($getoption_count > $max_getoptions)
    {
        $expat->xpcroak("Too many getoptions - maximum $max_getoptions");
    }

    # Check that appropriate attributes are defined

    for my $attribute (qw(symbol title))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check for a duplicate getoption symbol

    if(defined($getoption_symbols{$current_node->{symbol}}))
    {
        $expat->xpcroak("Get option symbol $current_node->{symbol} is duplicated");
    }

    $getoption_symbols{$current_node->{symbol}} = 1;

    # Give the getoption a bitmask

    $current_node->{mask} = sprintf("0x%04lx", $bitmask);

    # Double bitmask ready for next getoption

    $bitmask *= 2;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
     
    return if(defined($current_node->{pseudoroot}));

    # Check that doc is defined
     
    if(!defined($current_node->{doc}))
    {
        $expat->xpcroak("Get option $current_node->{symbol} has no doc");
    }
}

# EOF
