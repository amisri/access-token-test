#!/usr/bin/perl -w
#
# Name:     Input/Flag.pm
# Purpose:  Provides functions relating to the FGC flags
# Author:   Stephen Page

package Input::Flag;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %flag_names;
my %flag_values;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
     
    return if(defined($current_node->{pseudoroot}));

    # Check that appropriate attributes are defined

    for my $attribute (qw(name title value))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Convert value to decimal if it is in hex

    if($current_node->{value} =~ /^0x\w+/) # Value is in hex
    {
        $current_node->{value} =~ s/(0x\w+)/hex($1)/e;
    }
    $current_node->{mask} = sprintf("0x%08X", $current_node->{value});

    # Check for a duplicate flag name

    if(defined($flag_names{$current_node->{name}}))
    {
        $expat->xpcroak("Flag name $current_node->{name} is duplicated");
    }
    $flag_names{$current_node->{name}} = 1;

    # Check for a duplicate flag value

    if(defined($flag_values{$current_node->{mask}}))
    {
        $expat->xpcroak("Flag value $current_node->{value} is duplicated");
    }
    $flag_values{$current_node->{mask}} = 1;
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag
     
    return if(defined($current_node->{pseudoroot}));

    # Check that doc is defined
     
    if(!defined($current_node->{doc}))
    {
        $expat->xpcroak("Flag $current_node->{name} has no doc");
    }
}

# EOF
