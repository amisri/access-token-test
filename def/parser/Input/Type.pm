#!/usr/bin/perl -w
#
# Name:     Input/Type.pm
# Purpose:  Provides functions relating to the FGC types
# Author:   Stephen Page

package Input::Type;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(start_tag_handler end_tag_handler);

my %type_names;

return(1);


# Begin functions

sub start_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag

    return if(defined($current_node->{pseudoroot}));

    # Check that appropriate attributes are defined

    for my $attribute (qw(name title size))
    {
        if(!defined($current_node->{$attribute}))
        {
            $expat->xpcroak("$attribute not defined");
        }
    }

    # Check for a duplicate type name

    if(defined($type_names{$current_node->{name}}))
    {
        $expat->xpcroak("Type name $current_node->{name} is duplicated");
    }
    else
    {
        $type_names{$current_node->{name}} = 1;
    }
}

sub end_tag_handler($$$$)
{
    my ($expat, $element, $current_node, $superhash) = @_;

    # Ignore pseudoroot tag

    return if(defined($current_node->{pseudoroot}));

    # Check that doc is defined
     
    if(!defined($current_node->{doc}))
    {
        $expat->xpcroak("$current_node->{node_tag} function $current_node->{name} has no doc");
    }
}

# EOF
