#!/usr/bin/perl -w
#
# Name:     parse.pl
# Purpose:  Parses XML and outputs relating files and documentation
# Author:   Stephen Page

use Fcntl qw/O_WRONLY O_CREAT O_EXCL/;
use File::Basename;
use strict;

# Get path to directory containing this program

my $progpath    = dirname($0);
$progpath       = "." if($progpath eq "");

# Add program directory to library path

use lib dirname($0);

# Change directory to program directory

chdir "$progpath";

use Input::Parser;
use Output::Writer;

my %roots;      # Hash to store references to definition roots
my %superhash;  # Hash to store references to all definitions

$superhash{roots} = \%roots;

# Get path to project directory

my $projpath = "../..";

# Set creation date

$superhash{creationdate} = localtime;

# Parse input files

Input::Parser::parse($projpath, \%superhash, \%roots);

# Write output files

Output::Writer::write($projpath, \%superhash);

print "Generation completed\n\n";

# EOF
