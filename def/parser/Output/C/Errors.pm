#!/usr/bin/perl -w
#
# Name:     Output/C/Errors.pm
# Purpose:  Provides functions to generate fgc_errs C header file
# Author:   Stephen Page

package Output::C::Errors;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$)
{
    my ($outputfile, $outputfilename, $superhash) = @_;

    my $errorshash = $superhash->{errors};

    print $outputfile   "// $outputfilename - This file is automatically generated - DO NOT EDIT\n\n",
                        "#ifndef FGC_ERRS_H\n",
                        "#define FGC_ERRS_H\n",
                        "\n",
                        "enum fgc_errno\n{\n";

    my @sorted_errors = sort {$a <=> $b} keys(%$errorshash);

    foreach my $errornumber (@sorted_errors)
    {
        my $const = $errorshash->{$errornumber}->{message};
        $const =~ s/ /_/g;
        $const =~ s/(\w)/\U$1/g;
        printf $outputfile ("    /* %3u */ FGC_$const,\n",$errornumber);
    }

    print $outputfile   "    /*     */ FGC_NUM_ERRS\n",
                        "};\n",
                        "\n",
                        "// Duplicate const string typeddef to avoid dependencies of the fgc_ether library with the rest of the FGC framerwork\n",
                        "#ifdef __HC16__\n",
                        "typedef char * FAR deftypes_const_string;\n",
                        "#else\n",
                        "typedef const char * deftypes_const_string;\n",
                        "#endif\n",
                        "\n",
                        "extern deftypes_const_string fgc_errmsg[];\n",
                        "\n",
                        "#ifdef FGC_GLOBALS\n",
                        "deftypes_const_string fgc_errmsg[] =\n",
                        "{\n";

    foreach my $errornumber (@sorted_errors)
    {
        printf $outputfile ("    /* %3u */ \"$errorshash->{$errornumber}->{message}\",\n",$errornumber);
    }

    print $outputfile   "};\n",
                        "#endif\n",
                        "\n",
                        "#endif\n",
                        "\n",
                        "// End of file: $outputfilename\n";
}

# EOF
