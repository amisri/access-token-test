#!/usr/bin/perl -w
#
# Name:     Output/C/Defsyms.pm
# Purpose:  Provides functions to generate defsyms C header file
# Author:   Stephen Page

package Output::C::Defsyms;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        %definfo
                        write
                    );

our %definfo = (
                defaults => {
                                get             => 'NONE',
                                set             => 'NONE',
                                setif           => 'ALWAYS',
                                symtabconst     => 'NULL',
                                symtabprop      => 'NULL',
                            },
                prefixes => {
                                flag            => 'PF',
                                get             => 'GET',
                                getoption       => 'GET_OPT',
                                property        => 'PROP',
                                set             => 'SET',
                                setif           => 'SETIF',
                                symtabconst     => 'STC',
                                symtabprop      => 'STP',
                                type            => 'PT',
                            },
              );
              

	
use constant FGC_SYMENTRIES_LIMIT   => 13;
use constant FGCD_SYMENTRIES_LIMIT  => 23;    
use constant SYMENTRIES_UPPER_LIMIT => 50;        
             

return(1);


# Begin functions

sub write($$$)
{
    my ($outputfile, $outputfilename, $superhash) = @_;

    my $class = $superhash->{current_class};
     
    my $sym_entry_limit = SYMENTRIES_UPPER_LIMIT; 
    my $class_name = $$class{"name"};
    
    if ($class_name =~ "^FGC[2-3]")
    {
    	$sym_entry_limit = FGC_SYMENTRIES_LIMIT; 
    }  
    elsif ($class_name =~ "^FGCD")
    {
    	$sym_entry_limit = FGCD_SYMENTRIES_LIMIT;    	
    }
   
	
    my $class_id = $superhash->{current_class}->{id};

    print $outputfile   "// $outputfilename - This file is automatically generated by `".__FILE__."`  DO NOT EDIT\n",
                        "\n",
                        "#ifndef DEFSYMS_${class_id}_H\n",
                        "#define DEFSYMS_${class_id}_H\n",
                        "\n",
                        "#include \"classes/$class->{id}/defconst.h\"\n",
                        "#include \"label.h\"\n",
                        "#include \"deftypes.h\"\n",
                        "\n";

    # Construct a hash of symbol lists used by the class properties

    my %symlists;

    for my $property (@{$class->{properties}})
    {
        if(defined($property->{symlist}))
        {
            $symlists{$property->{symlist}->{name}} = $property->{symlist};
        }

        if(defined($property->{class_symlist}))
        {
            $symlists{$property->{class_symlist}}   = $property->{class_symlists}->{$class->{id}};
        }
    }

    # Write symbol lists

    for my $symlist (sort { $a->{name} cmp $b->{name} } values(%symlists))
    {
        print $outputfile   "// $symlist->{name}\n",
                            "\n",
                            "extern CONST struct sym_name sym_names_\L$symlist->{name}\[];\n",
                            "#ifdef DEFSYMS\n",
                            "CONST struct sym_name sym_names_\L$symlist->{name}\[]\n",
                            "        =\n",
                            "{\n";

        for my $symbol (@{$symlist->{consts}})
        {
        	        	
        	if ( length($symbol->{symbol}) > $sym_entry_limit )
        	{
        		die "Symlist entry $symbol->{symbol} exceeds limit of $sym_entry_limit for class $class_name. \n";
        	}
        	
        	
            printf $outputfile  "    { FGC_%-25s, %-27s },\n",
                                "\U$symbol->{name}", "\"\U$symbol->{symbol}\"";
        }

        print $outputfile   "    { 0                            , 0                           }\n",
                            "}\n",
                            "#endif\n",
                            ";\n",
                            "\n";
    }

    print $outputfile   "#endif\n",
                        "\n",
                        "// End of file: $outputfilename\n";
}

# EOF
