#!/usr/bin/perl -w
#
# Name:     Output/C/FGC_cmw_sub_data.pm
# Purpose:  Provides functions to generate fgc_cmw_sub_data C header file
# Author:   Miguel Hermo Serans

package Output::C::Sub_cmwdata_cpp;

use strict;

use Exporter;
use Output::C::Sub_defsyms;

use Data::Dumper;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

my $header = <<'HEADER';

#include <sub_cmwdata_gen.h>

#define MAX_CMW_STRING_LENGTH 255

#include <arpa/inet.h>

#include <fgc_stat.h>
#include <cmw.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <logging.h>
#include <string.h>
#include <sub_utilities.h>

HEADER


my $fgc_cmw_fill_sub_data = <<'FGC_CMW_FILL_SUB_DATA';
void subcmwdataFillSubData(Data *cmw_data, fgc_stat *status, int32_t sub_device)
{
    char buffer[MAX_CMW_STRING_LENGTH];

    switch(status->class_id)
    {

__SWITCH_CASES__

        // class 59 is hard-coded

        case 59:

            // Common status data

            subUtilitiesEnumToText((status->class_data.c59.state_op), sym_names_59_op, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("STATE_OP", buffer);

            subUtilitiesEnumToText((status->class_data.c59.state_pll), sym_names_59_pll, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("STATE_PLL", buffer);

            subUtilitiesBitmaskToText(ntohs(status->class_data.c59.st_faults), sym_names_59_flt, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("ST_FAULTS", buffer);

            subUtilitiesBitmaskToText(ntohs(status->class_data.c59.st_latched), sym_names_59_lat, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("ST_LATCHED", buffer);

            subUtilitiesBitmaskToText(ntohs(status->class_data.c59.st_unlatched), sym_names_59_unl, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("ST_UNLATCHED", buffer);

            subUtilitiesBitmaskToText(ntohs(status->class_data.c59.st_warnings), sym_names_59_wrn, buffer, MAX_CMW_STRING_LENGTH);
            cmw_data->append("ST_WARNINGS", buffer);

            // sub device dependent status data

            if(sub_device == 0)
            {
                const int32_t   MAX_SUBDEV_59 = 16;
                const char      *state_func_ptr[MAX_SUBDEV_59];
                bool            st_abort_evt[MAX_SUBDEV_59];
                bool            st_lim_chan_max[MAX_SUBDEV_59];
                bool            st_lim_chan_min[MAX_SUBDEV_59];
                bool            st_lim_rate_neg[MAX_SUBDEV_59];
                bool            st_lim_rate_pos[MAX_SUBDEV_59];
                bool            st_lim_rt_neg[MAX_SUBDEV_59];
                bool            st_lim_rt_pos[MAX_SUBDEV_59];
                bool            st_not_ack[MAX_SUBDEV_59];
                bool            st_rt_ctrl[MAX_SUBDEV_59];
                bool            st_start_evt[MAX_SUBDEV_59];
                bool            st_trigger[MAX_SUBDEV_59];

                // Initialize arrays

                for(int i=0; i < MAX_SUBDEV_59; ++i)
                {
                    state_func_ptr[i] = "";
                }
                memset(st_abort_evt     ,0,sizeof(st_abort_evt));
                memset(st_lim_chan_max  ,0,sizeof(st_lim_chan_max));
                memset(st_lim_chan_min  ,0,sizeof(st_lim_chan_min));
                memset(st_lim_rate_neg  ,0,sizeof(st_lim_rate_neg));
                memset(st_lim_rate_pos  ,0,sizeof(st_lim_rate_pos));
                memset(st_lim_rt_neg    ,0,sizeof(st_lim_rt_neg));
                memset(st_lim_rt_pos    ,0,sizeof(st_lim_rt_pos));
                memset(st_not_ack       ,0,sizeof(st_not_ack));
                memset(st_rt_ctrl       ,0,sizeof(st_rt_ctrl));
                memset(st_start_evt     ,0,sizeof(st_start_evt));
                memset(st_trigger       ,0,sizeof(st_trigger));
                
                for(int i=0; i < MAX_SUBDEV_59; ++i)
                {
                    int16_t sub_device_mask = (1<<i);

                    // set STATE_FUNC

                    if(ntohs(status->class_data.c59.st_aborting) & sub_device_mask)
                    {
                        state_func_ptr[i] = "ABORTING";
                    }
                    else if(ntohs(status->class_data.c59.st_armed) & sub_device_mask)
                    {
                        state_func_ptr[i] = "ARMED";
                    }
                    else if(ntohs(status->class_data.c59.st_idle) & sub_device_mask)
                    {
                        state_func_ptr[i] = "IDLE";
                    }
                    else if(ntohs(status->class_data.c59.st_running) & sub_device_mask)
                    {
                        state_func_ptr[i] = "RUNNING";
                    }
                    else
                    {
                        state_func_ptr[i] = "OFF";
                    }    

                    st_abort_evt[i]     =  (ntohs(status->class_data.c59.st_abort_evt)      & sub_device_mask) != 0;          
                    st_lim_chan_max[i]  =  (ntohs(status->class_data.c59.st_lim_chan_max)   & sub_device_mask) != 0;          
                    st_lim_chan_min[i]  =  (ntohs(status->class_data.c59.st_lim_chan_min)   & sub_device_mask) != 0;          
                    st_lim_rate_neg[i]  =  (ntohs(status->class_data.c59.st_lim_rate_neg)   & sub_device_mask) != 0;          
                    st_lim_rate_pos[i]  =  (ntohs(status->class_data.c59.st_lim_rate_pos)   & sub_device_mask) != 0;          
                    st_lim_rt_neg[i]    =  (ntohs(status->class_data.c59.st_lim_rt_neg)     & sub_device_mask) != 0;          
                    st_lim_rt_pos[i]    =  (ntohs(status->class_data.c59.st_lim_rt_pos)     & sub_device_mask) != 0;          
                    st_not_ack[i]       =  (ntohs(status->class_data.c59.st_not_ack)        & sub_device_mask) != 0;          
                    st_rt_ctrl[i]       =  (ntohs(status->class_data.c59.st_rt_ctrl)        & sub_device_mask) != 0;          
                    st_start_evt[i]     =  (ntohs(status->class_data.c59.st_start_evt)      & sub_device_mask) != 0;          
                    st_trigger[i]       =  (ntohs(status->class_data.c59.st_trigger)        & sub_device_mask) != 0;    

                    cmw_data->appendArray("STATE_FUNC"       , state_func_ptr    , MAX_SUBDEV_59);
                    cmw_data->appendArray("ST_ABORT_EVT"     , st_abort_evt      , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_CHAN_MAX"  , st_lim_chan_max   , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_CHAN_MIN"  , st_lim_chan_min   , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_RATE_NEG"  , st_lim_rate_neg   , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_RATE_POS"  , st_lim_rate_pos   , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_RT_NEG"    , st_lim_rt_neg     , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_LIM_RT_POS"    , st_lim_rt_pos     , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_NOT_ACK"       , st_not_ack        , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_RT_CTRL"       , st_rt_ctrl        , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_START_EVT"     , st_start_evt      , MAX_SUBDEV_59);       
                    cmw_data->appendArray("ST_TRIGGER"       , st_trigger        , MAX_SUBDEV_59);       

                }
            }
            else
            {
                const char      *state_func_ptr = "";
                bool            st_abort_evt    = false;            ;
                bool            st_lim_chan_max = false;
                bool            st_lim_chan_min = false;
                bool            st_lim_rate_neg = false;
                bool            st_lim_rate_pos = false;
                bool            st_lim_rt_neg   = false;
                bool            st_lim_rt_pos   = false;
                bool            st_not_ack      = false;
                bool            st_rt_ctrl      = false;
                bool            st_start_evt    = false;
                bool            st_trigger      = false;

                int16_t sub_device_mask = (1<<(sub_device-1));

                // set STATE_FUNC

                if(ntohs(status->class_data.c59.st_aborting) & sub_device_mask)
                {
                    state_func_ptr = "ABORTING";
                }
                else if(ntohs(status->class_data.c59.st_armed) & sub_device_mask)
                {
                    state_func_ptr = "ARMED";
                }
                else if(ntohs(status->class_data.c59.st_idle) & sub_device_mask)
                {
                    state_func_ptr = "IDLE";
                }
                else if(ntohs(status->class_data.c59.st_running) & sub_device_mask)
                {
                    state_func_ptr = "RUNNING";
                }
                else
                {
                    state_func_ptr = "OFF";
                }    

                st_abort_evt        =  (ntohs(status->class_data.c59.st_abort_evt)      & sub_device_mask) != 0;          
                st_lim_chan_max     =  (ntohs(status->class_data.c59.st_lim_chan_max)   & sub_device_mask) != 0;          
                st_lim_chan_min     =  (ntohs(status->class_data.c59.st_lim_chan_min)   & sub_device_mask) != 0;          
                st_lim_rate_neg     =  (ntohs(status->class_data.c59.st_lim_rate_neg)   & sub_device_mask) != 0;          
                st_lim_rate_pos     =  (ntohs(status->class_data.c59.st_lim_rate_pos)   & sub_device_mask) != 0;          
                st_lim_rt_neg       =  (ntohs(status->class_data.c59.st_lim_rt_neg)     & sub_device_mask) != 0;          
                st_lim_rt_pos       =  (ntohs(status->class_data.c59.st_lim_rt_pos)     & sub_device_mask) != 0;          
                st_not_ack          =  (ntohs(status->class_data.c59.st_not_ack)        & sub_device_mask) != 0;          
                st_rt_ctrl          =  (ntohs(status->class_data.c59.st_rt_ctrl)        & sub_device_mask) != 0;          
                st_start_evt        =  (ntohs(status->class_data.c59.st_start_evt)      & sub_device_mask) != 0;          
                st_trigger          =  (ntohs(status->class_data.c59.st_trigger)        & sub_device_mask) != 0;    

                cmw_data->append("STATE_FUNC"       , state_func_ptr    );
                cmw_data->append("ST_ABORT_EVT"     , st_abort_evt      );       
                cmw_data->append("ST_LIM_CHAN_MAX"  , st_lim_chan_max   );       
                cmw_data->append("ST_LIM_CHAN_MIN"  , st_lim_chan_min   );       
                cmw_data->append("ST_LIM_RATE_NEG"  , st_lim_rate_neg   );       
                cmw_data->append("ST_LIM_RATE_POS"  , st_lim_rate_pos   );       
                cmw_data->append("ST_LIM_RT_NEG"    , st_lim_rt_neg     );       
                cmw_data->append("ST_LIM_RT_POS"    , st_lim_rt_pos     );       
                cmw_data->append("ST_NOT_ACK"       , st_not_ack        );       
                cmw_data->append("ST_RT_CTRL"       , st_rt_ctrl        );       
                cmw_data->append("ST_START_EVT"     , st_start_evt      );       
                cmw_data->append("ST_TRIGGER"       , st_trigger        );       
            }

            break;

	    default:
            cmw_data->appendArray("value", (const int8_t *)status, sizeof(*status));;
            break;
	}
}
FGC_CMW_FILL_SUB_DATA

return(1);

# Begin functions

sub write($$$)
{
    my ($outputfile, $outputfilename, $superhash) = @_;

	# Switch cases for treating each class
	
    my @sorted_classes = sort { $a->{id} <=> $b->{id} } values %{$superhash->{classes}};
    
    my $switch_cases = "";
    foreach my $class (@sorted_classes)
    {
        if($class->{id} != 59) 
        {
            $switch_cases .= "       case $class->{id}:\n";
            $switch_cases .= append_data_for_class($class);
            $switch_cases .= "            break;\n\n";
        }
    }
    
	# Print file

	$fgc_cmw_fill_sub_data =~ s/__SWITCH_CASES__/$switch_cases/;
	
    print $outputfile   "// $outputfilename - This file is automatically generated by ".__FILE__." - DO NOT EDIT\n\n";
    print $outputfile $header;
    print $outputfile $fgc_cmw_fill_sub_data;
    print $outputfile "\n";

};

sub append_data_for_class($)
{
    my ($current_class) = @_;
    
    # Add elements to structure definition
    my $indent = "            ";
    my $c_code = "";
    
    my $next_expected_address   = 0;

	my $class_id    = $current_class->{id};

    foreach my $zone (sort {$a->{name} cmp $b->{name}} (values(%{$current_class->{pubdata}})))
    {
        if($zone->{data_position} >= 0)
        {
            my $field_label = $zone->{name};
			my $var_name    = lc($zone->{name});
    		
            # Translate bit masks to text
            my $type = get_property_type($zone);
            my $endianness = correct_endianness($type);
            my $cast = get_type_cast($type);

            my $symlist = Output::C::Sub_defsyms::get_symlist($zone, $class_id);
            if(defined $symlist)
            {
                # CHECK zone->property->IDX
                if( $zone->{property}->{flags}->{BIT_MASK})
                {
                    $c_code .= $indent."subUtilitiesBitmaskToText(${endianness}(status->class_data.c${class_id}.${var_name}), sym_names_${class_id}_".lc($symlist->{name}).", buffer, MAX_CMW_STRING_LENGTH);\n";
                    $c_code .= $indent."cmw_data->append(\"${field_label}\", buffer);\n\n";
                }
                else
                {
                    $c_code .= $indent."subUtilitiesEnumToText(${endianness}(status->class_data.c${class_id}.${var_name}), sym_names_${class_id}_".lc($symlist->{name}).", buffer, MAX_CMW_STRING_LENGTH);\n";
                    $c_code .= $indent."cmw_data->append(\"${field_label}\", buffer);\n\n";
                }
                
            } 
            else
            {

	            $c_code .= $indent;
	            $c_code .= "cmw_data->append(\"${field_label}\",";
	            $c_code .= $cast;
                $c_code .= $endianness;
				$c_code .= "(status->class_data.c${class_id}.${var_name}));\n";
			}

            # Check for gap in addresses and pad with reserved bytes if necessary
            $next_expected_address = $zone->{address} + $zone->{numbits};
        }
    }
    
    # Close function definition
    return "$c_code\n";
};

sub get_property_type($)
{
	my ($zone) = @_;
	my $type;

    # Work out type based upon original property type and zone size

    if($zone->{property}->{type}->{name} =~ /^INT\d*(\w)/)  # Property is an integer
    {
        $type = ($1 eq 'U' ? 'u' : '')."int$zone->{numbits}_t";
    }
    elsif($zone->{property}->{type}->{name} eq "FLOAT")     # Property is a float
    {
        $type = 'float';
    }
    
    return $type;
};


sub get_type_cast($)
{
    my ($type) = @_;
    
    if( $type =~ /(u?)int(\d*)_t/ )
    {
        my $unsigned = $1;
        my $size = int($2);
       
        # CMW does not support unsigned values, so we have to cast to next bigger type
        $size*=2 if($unsigned);
        
        # This is required to ensure that SUB properties behave the same way as SUB_XXX properties,
        # Since the japc code used to translate SUB_XXX properties publishes all integers as either 'int' or 'long'
        # But not 'short' or 'byte'
        $size = 32 if($size < 32);

        return "(int${size}_t)";
    }
    else
    {
        return "";
    }
    
};

sub correct_endianness($)
{
    my ($type) = @_;

    return "subUtilitiesSignedNtohs" if($type eq "int16_t");
    return "subUtilitiesSignedNtohl" if($type eq "int32_t");
    return "ntohs"                 if($type eq "uint16_t");
    return "ntohl"                 if($type eq "uint32_t");
    return "subUtilitiesNtohf"       if($type eq "float");
    return "";    
}

# EOF