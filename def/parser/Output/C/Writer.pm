#!/usr/bin/perl -w
#
# Name:     Output/C/Writer.pm
# Purpose:  Provides functions to generate C files
# Author:   Stephen Page

package Output::C::Writer;

use Exporter;
use strict;

use File::Path qw(mkpath);
use Output::C::Boot::Menu;
use Output::C::Boot::Menu_consts;
use Output::C::Defconst;
use Output::C::Defconst_assert;
use Output::C::Definfo;
use Output::C::Defprops;
use Output::C::Defpropsdsp;
use Output::C::Defsyms;
use Output::C::Errors;
use Output::C::Sub_defsyms;
use Output::C::Sub_defconst;
use Output::C::Sub_cmwdata_h;
use Output::C::Sub_cmwdata_cpp;
use Output::C::FGC_codes;
use Output::C::FGC_consts_gen;
use Output::C::FGC_parser_consts;
use Output::C::FGC_runlog_entries;
use Output::C::FGC_stat;
use Output::C::FGC_stat_consts;
use Output::C::Memmap;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of C files **********\n\n";

    my $inc_path = "$projpath/sw/inc";

    my %paths = (
                    defconst            => "defconst.h",
                    defconst_assert     => "defconst_assert.h",
                    definfo             => "definfo.h",
                    defprops            => "defprops.h",
                    defsyms             => "defsyms.h",
                    sub_defsyms         => "sub_defsyms.h",
                    sub_defconst        => "sub_defconst.h",
                    sub_cmwdata_h       => "sub_cmwdata_gen.h",
                    sub_cmwdata_cpp     => "sub_cmwdata_gen.cpp",
                    errors              => "fgc_errs.h",
                    fgc_codes           => "fgc_codes_gen.h",
                    fgc_consts_gen      => "fgc_consts_gen.h",
                    fgc_parser_consts   => "fgc_parser_consts.h",
                    fgc_runlog_entries  => "fgc_runlog_entries.h",
                    fgc_stat_consts     => "fgc_stat_consts.h",
                    interpretation      => "interprt.h",
                    memmap              => "memmap",
                    stat                => "#_stat.h",
                );

    my %writefuncs = (
                        bootmenu            => \&Output::C::Boot::Menu::write,
                        bootmenu_consts     => \&Output::C::Boot::Menu_consts::write,
                        defconst            => \&Output::C::Defconst::write,
                        defconst_assert     => \&Output::C::Defconst_assert::write,
                        definfo             => \&Output::C::Definfo::write,
                        defprops            => \&Output::C::Defprops::write,
                        defsyms             => \&Output::C::Defsyms::write,
                        errors              => \&Output::C::Errors::write,
                        sub_defconst        => \&Output::C::Sub_defconst::write,
                        sub_defsyms         => \&Output::C::Sub_defsyms::write,
                        sub_cmwdata_h       => \&Output::C::Sub_cmwdata_h::write,
                        sub_cmwdata_cpp     => \&Output::C::Sub_cmwdata_cpp::write,
                        fgc_codes           => \&Output::C::FGC_codes::write,
                        fgc_consts_gen      => \&Output::C::FGC_consts_gen::write,
                        fgc_parser_consts   => \&Output::C::FGC_parser_consts::write,
                        fgc_runlog_entries  => \&Output::C::FGC_runlog_entries::write,
                        fgc_stat_consts     => \&Output::C::FGC_stat_consts::write,
                        memmap              => \&Output::C::Memmap::write,
                        stat                => \&Output::C::FGC_stat::write,
                     );

    # Generate CMW decondign header and implementation

    my $outputfilename = "$projpath/sw/fgcd/inc/" . $paths{sub_cmwdata_h};
    open(FILE, ">", $outputfilename)
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    $writefuncs{sub_cmwdata_h}(\*FILE,$outputfilename,$superhash);

    $outputfilename = "$projpath/sw/fgcd/src/" . $paths{sub_cmwdata_cpp};
    open(FILE, ">", $outputfilename)
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    $writefuncs{sub_cmwdata_cpp}(\*FILE,$outputfilename,$superhash);

    # Generate generic files

    foreach my $headerfile (qw(errors fgc_codes fgc_consts_gen fgc_parser_consts fgc_runlog_entries))
    {
        my $outputfilename = "$inc_path/$paths{$headerfile}";
        open(FILE, ">", $outputfilename)
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        $writefuncs{$headerfile}(\*FILE, $outputfilename, $superhash);
        close(FILE);
    }

    # Generate platform specific files

    foreach my $platform (values(%{$superhash->{platforms}}))
    {
        # Make header file directory for platform

        my $platform_inc_dir = "$inc_path/platforms/$platform->{name}";
        mkpath($platform_inc_dir);

        my $outputfilename;

        # Write boot files for device platforms

        if($platform->{id} >= 50 && defined($platform->{boot}))
        {
            mkpath("$platform_inc_dir/boot");

            $outputfilename = "$platform_inc_dir/boot/menutree.h";
            open(FILE, ">", $outputfilename)
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            $writefuncs{bootmenu}(\*FILE, $outputfilename, $superhash, $platform);
            close(FILE);

            $outputfilename = "$platform_inc_dir/boot/menuconsts.h";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            $writefuncs{bootmenu_consts}(\*FILE, $outputfilename, $superhash, $platform);
            close(FILE);
        }

        # Write memory map file

        if(defined($platform->{memmaps}))
        {
            foreach my $memmap (keys(%{$platform->{memmaps}}))
            {
                my $outputfilename  =  "$platform_inc_dir/memmap_\L$memmap.h";
                open(FILE, ">", $outputfilename)
                    or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
                $writefuncs{memmap}(\*FILE, $outputfilename, $platform->{memmaps}->{$memmap});
                close(FILE);
            }
        }
    }

    # Generate class specific files

    foreach my $class (values(%{$superhash->{classes}}))
    {
        # Make header file directory for class

        my $class_inc_dir = "$inc_path/classes/$class->{id}";
        mkpath($class_inc_dir);

        $superhash->{current_class} = $class;

        foreach my $headerfile (qw(defconst defconst_assert sub_defconst sub_defsyms definfo defprops defsyms stat fgc_stat_consts))
        {
            my $outputfilename  =  "$class_inc_dir/$paths{$headerfile}";
            $outputfilename     =~ s/#/$class->{id}/;
            open(FILE, ">", $outputfilename)
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            $writefuncs{$headerfile}(\*FILE, $outputfilename, $superhash);
            close(FILE);
        }

        # Write DSP header files

        foreach my $dsp (values(%{$superhash->{current_class}->{dsps}}))
        {
            my $headerfile = "defprops_dsp_$dsp->{name}.h";

            my $outputfilename  =  "$class_inc_dir/$headerfile";
            open(FILE, ">", $outputfilename)
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            Output::C::Defpropsdsp::write(\*FILE, $outputfilename, $superhash, $dsp->{name});
            close(FILE);
        }

        delete $superhash->{current_class};
    }

    print "********** Generation of C files completed successfully **********\n\n";
}

# EOF
