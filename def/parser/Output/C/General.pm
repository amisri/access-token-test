#!/usr/bin/perl -w
#
# Name:     Output/C/Defprops.pm
# Purpose:  Provides generic functions to generate C header file

package Output::C::General;

use Exporter;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
                        writedefmask
                        writeenum
                        nextprime
                    );

return(1);


# Begin functions


# Write an enumeration

sub writeenum($$$$$;$;$)
{
    my ($outputfile, $elementstrings, $label, $name, $prefix, $line1, $camel_case) = @_;

    my $numels = (@$elementstrings);

    $numels++ if(defined($line1));

    print $outputfile   "/*!\n",
                        " * $label constants ($numels)\n",
                        " */\n",
                        "enum ${name}_e\n",
                        "{\n";

    my $n = 0;

    if(defined($line1))
    {
        printf $outputfile ("    /* %3d */ ${prefix}_$line1,\n", $n++);
    }

    for my $element (@$elementstrings)
    {
        my $symbol  =  $element;
        if (defined ($camel_case) && ($camel_case = 1))
        {
            $symbol    =~ s/\./_/g;
        }
        else
        {
            $symbol     =~ s/(\w*)/\U$1/g;
        }
        
        printf $outputfile ("    /* %3d */ ${prefix}_$symbol,\n", $n++);
    }

    print $outputfile   "    NUM_\U$name\n",
                        "};\n",
                        "\n";
}

# Write a def mask

sub writedefmask($$$$$)
{
    my ($outputfile, $elements, $nameattribute, $label, $prefix) = @_;

    my $numels = (@$elements);

    print $outputfile "// $label constants ($numels)\n\n";

    for my $element (@$elements)
    {
        my $symbol  =  $element->{$nameattribute};
        $symbol     =~ s/(\w*)/\U$1/g;
        printf $outputfile ("#define %-30s %s\n", "${prefix}_$symbol", $element->{mask});
    }

    print $outputfile "\n";
}

# Find the next prime greater than or equal to the passed value

sub nextprime($)
{
    my ($value) = @_;

    for(my $is_prime = 0 ; !$is_prime ; $value++)
    {
        # Set is_prime to 1 initially

        $is_prime = 1;

        # Check for factors

        for(my $i = 2 ; $is_prime && $i < ($value / 2) ; $i++)
        {
            if($value % $i == 0)
            {
                $is_prime = 0;
            }
        }
    }
    return(--$value);
}

# EOF
