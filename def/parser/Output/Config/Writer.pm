#!/usr/bin/perl -w
#
# Name:     Output/Config/Writer.pm
# Purpose:  Provides functions to generate config files
# Author:   Stephen Page

package Output::Config::Writer;

use Exporter;
use File::Basename;
use File::Path qw(mkpath);
use Output::Config::Alarm_fault_codes;
use Output::Config::FGCs;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    my %paths = (
                    alarm_fault_codes   => "sw/utilities/perl/databases/controls/fault_codes",
                    fgcs                => "hw/fgc2/pld/tools/jtagprog/fgcs",
                );

    my %writefuncs = (
                        alarm_fault_codes   => \&Output::Config::Alarm_fault_codes::write,
                        fgcs                => \&Output::Config::FGCs::write,
                     );

    print "********** Beginning generation of config files **********\n\n";

    foreach my $configfile (sort(keys(%writefuncs)))
    {
        my $outputfilename = "$projpath/$paths{$configfile}";
        mkpath(dirname($outputfilename));

        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";

        $writefuncs{$configfile}(\*FILE, $outputfilename, $superhash);

        close(FILE);
    }

    print "********** Generation of config files completed successfully **********\n\n";
}

# EOF
