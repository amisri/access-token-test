#!/usr/bin/perl -w
#
# Name:     Output/Config/Alarm_fault_codes.pm
# Purpose:  Provides functions to generate alarm faults codes config file
# Author:   Stephen Page

package Output::Config::Alarm_fault_codes;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$)
{
    my ($outputfile, $outputfilename, $superhash) = @_;

    for my $class (sort {$a->{id} <=> $b->{id}} values(%{$superhash->{classes}}))
    {
        # Find faults symlist

        my $faults_symlist;
        my $warnings_symlist;
        for my $memzone (@{$class->{pubdataroot}->{children}})
        {
            if($memzone->{name} eq "ST_FAULTS")
            {
                if(defined($memzone->{property}->{symlist}))
                {
                    $faults_symlist = $memzone->{property}->{symlist};
                }
                else
                {
                    $faults_symlist = $memzone->{property}->{class_symlists}->{$class->{id}};
                }
            }

            if($memzone->{name} eq "ST_WARNINGS")
            {
                if(defined($memzone->{property}->{symlist}))
                {
                    $warnings_symlist = $memzone->{property}->{symlist};
                }
                else
                {
                    $warnings_symlist = $memzone->{property}->{class_symlists}->{$class->{id}};
                }
            }
        }

        # Skip classes without faults and warnings

        next if(!defined($faults_symlist) || !defined($warnings_symlist));

        # Write warnings

        for my $alarm (sort {
                                $class->{symtab_consts}->{$a->{name}}->{value} <=>
                                $class->{symtab_consts}->{$b->{name}}->{value}
                            } @{$warnings_symlist->{consts}})
        {
            my $fault_code  = 0;
            my $value       = $class->{symtab_consts}->{$alarm->{name}}->{value};

            while(1 << $fault_code != $value)
            {
                $fault_code++;
            }

            # Replace newline characters with spaces in action, cause and consequence

            (my $action         = $alarm->{action}      || '') =~ s/[\012\015]/ /g;
            (my $cause          = $alarm->{cause}       || '') =~ s/[\012\015]/ /g;
            (my $consequence    = $alarm->{consequence} || '') =~ s/[\012\015]/ /g;

            # Remove leading and trailing whitespace

            $action         =~ s/(^\s+|\s+$)//;
            $cause          =~ s/(^\s+|\s+$)//;
            $consequence    =~ s/(^\s+|\s+$)//;

            print $outputfile   "$class->{id}:$alarm->{symbol}:$fault_code:",
                                defined($alarm->{priority}) ? $alarm->{priority} : 2,
                                ":$alarm->{title}:$cause:$consequence:$action\n";
        }

        # Write faults

        for my $alarm (sort {
                                $class->{symtab_consts}->{$a->{name}}->{value} <=>
                                $class->{symtab_consts}->{$b->{name}}->{value}
                            } @{$faults_symlist->{consts}})
        {
            my $fault_code  = 0;
            my $value       = $class->{symtab_consts}->{$alarm->{name}}->{value};

            while(1 << $fault_code != $value)
            {
                $fault_code++;
            }

            # Replace newline characters with spaces in action, cause and consequence

            (my $action         = $alarm->{action}      || '') =~ s/[\012\015]/ /g;
            (my $cause          = $alarm->{cause}       || '') =~ s/[\012\015]/ /g;
            (my $consequence    = $alarm->{consequence} || '') =~ s/[\012\015]/ /g;

            # Remove leading whitespace

            $action         =~ s/^\s+//g;
            $cause          =~ s/^\s+//g;
            $consequence    =~ s/^\s+//g;

            print $outputfile   "$class->{id}:$alarm->{symbol}:",
                                $fault_code + 16,
                                ":",
                                defined($alarm->{priority}) ? $alarm->{priority} : 3,
                                ":$alarm->{title}:$cause:$consequence:$action\n";
        }

        # Write device unreachable

        print $outputfile   "$class->{id}::32:2:Device unreachable:".
                            "Device cannot be contacted and its status is unknown:".
                            "Device may or may not be operating correctly.  It may not receive commands:".
                            "Check status of power, WorldFIP and network\n";
    }
}

# EOF
