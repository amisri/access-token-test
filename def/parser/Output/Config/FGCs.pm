#!/usr/bin/perl -w
#
# Name:     Output/Config/FGCs.pm
# Author:   Stephen Page
# Purpose:  Provides functions to generate FGCs config file

package Output::Config::FGCs;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$)
{
    my ($outputfile, $outputfilename, $superhash) = @_;

    foreach my $card (sort {$a->{label} cmp $b->{label}} (values(%{$superhash->{platforms}->{fgc}->{hardware}})))
    {
        next if(!defined($card->{subhw}));

        # Write card label

        print $outputfile   "$card->{label}:";

        # Write sub-card labels

        my $first_flag = 1;
        foreach my $subcard (@{$card->{subhw}})
        {
            if($first_flag)
            {
                $first_flag = 0;
            }
            else
            {
                print $outputfile   ",";
            }

            print $outputfile   "$subcard->{label}";
        }

        print $outputfile   "\n";
    }
}

# EOF
