#!/usr/bin/perl -w
#
# Name:     Output/Tango/Writer.pm
# Purpose:  Provides functions to generate tango files
# Author:   Tsampikos Livisianos

package Output::Tango::Writer;

use Scalar::Util qw(looks_like_number);
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub properties_to_json($)
{
    my ($properties) = @_;

    my $json = "{\n";

    my $first = 1;
    for my $prop (sort keys %$properties)
    {
        if(!$first)
        {
            $json .= "    },\n";
        }
        $json .= "    \"$properties->{$prop}->{name}\": {\n";
        $json .= "        \"type\": \"$properties->{$prop}->{type}\",\n";
        $json .= "        \"access\": \"$properties->{$prop}->{access}\"";
        if(defined($properties->{$prop}->{length}))
        {
            $json .= ",\n";
            $json .= "        \"length\": $properties->{$prop}->{length}\n";
        }
        else
        {
            $json .= "\n";
        }
        if($first)
        {
            $first = 0;
        }
    }

    $json .= "    }\n";
    $json .= "}\n";
    return $json;
}

sub make_property($$)
{
    my $prop = $_[0];
    my $class = $_[1];
    my $property;
    $property->{name} = $prop->{name};
    $property->{type} = $prop->{type}->{name};
    if(defined($prop->{flags}->{SYM_LST}))
    {
        $property->{type} = "ENUM";

    }
    if(defined($prop->{set}) && defined($prop->{get}))
    {
        $property->{access} = "ALL";
    }
    elsif(defined($prop->{set}))
    {
        $property->{access} = "SET";
    }
    elsif(defined($prop->{get}))
    {
        $property->{access} = "GET";
    }

    my $maxels_class = "maxels_" . $class->{id};
    if(defined($prop->{$maxels_class}))
    {
        my $prop_maxels = substr $prop->{$maxels_class}, 4;
        $property->{length} = $class->{consts}->{$prop_maxels};
    }
    elsif(looks_like_number($prop->{maxels}) && $prop->{maxels} > 1)
    {
        $property->{length} = $prop->{maxels};
    }
    elsif($prop->{maxels} =~ /^FGC/)
    {
        my $prop_maxels = substr $prop->{maxels}, 4;
        $property->{length} = $class->{consts}->{$prop_maxels};
    }
    return $property;
}

sub make_pubdata($$)
{
    my $pubd = $_[0];
    my $class = $_[1];

    my $prop = $pubd->{property};

    my $property;
    $property->{name} = $pubd->{fullname};
    $property->{type} = $prop->{type}->{name};
    $property->{access} = "SUB";
    if(defined($prop->{flags}->{SYM_LST}))
    {
        $property->{type} = "ENUM";

    }
    if(defined($prop->{flags}->{BIT_MASK}))
    {
        $property->{type} = "BITMASK";

    }

    return $property;
}

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of Tango files **********\n\n";

    my $fgctango_path = "$projpath/sw/kt/fgctango";
    my $fgctango_defs_path = "$fgctango_path/fgctango/defs";

    my $outputfile = "$fgctango_defs_path/prop_info.json";

    foreach my $class (values(%{$superhash->{classes}}))
    {
        if(defined($class->{kt}) && $class->{kt})
        {
            my $properties;
            foreach my $prop (@{$class->{properties}})
            {
                if(defined($prop->{kt}) && $prop->{kt} && ($prop->{type}->{name} ne "PARENT"))
                {
                    $properties->{$prop->{name}} = make_property($prop, $class);
                }
            }
            foreach my $pubd (values(%{$class->{pubdata}}))
            {
                my $prop = $pubd->{property};
                if(defined($prop))
                {
                    $properties->{$pubd->{fullname}} = make_pubdata($pubd, $class);
                }
            }
            my $testoutfile = "$fgctango_defs_path/class_$class->{id}_prop_info.json";
            open(my $fh, '>', $testoutfile) or die "Could not open file '$testoutfile' $!";
            print $fh properties_to_json($properties);
        }
    }


    print "********** Generation of Tango files completed successfully **********\n\n";
}

# EOF
