#!/usr/bin/perl -w
#
# Name:     Output/Perl/Writer.pm
# Purpose:  Provides functions to generate Perl files
# Author:   Stephen Page

package Output::Perl::Writer;

use Exporter;
use File::Path qw(mkpath);
use Output::Perl::Class::API;
use Output::Perl::Class::Auto;
use Output::Perl::Components;
use Output::Perl::Errors;
use Output::Perl::Properties;
use Output::Perl::RegFGC3_parameters;
use Output::Perl::Platform::Auto;
use Output::Perl::Runlog_entries;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    my $fgc_perl_path = "$projpath/sw/clients/perl/FGC";
    mkpath($fgc_perl_path);

    # Properties module

    my $outputfilename = "$fgc_perl_path/Properties.pm";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Perl::Properties::write(\*FILE, $outputfilename, $superhash);
    close(FILE);

    # Parameters module

    $outputfilename = "$fgc_perl_path/RegFGC3_parameters.pm";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Perl::RegFGC3_parameters::write(\*FILE, $outputfilename, $superhash);
    close(FILE);
    
    # Errors module

    $outputfilename = "$fgc_perl_path/Errors.pm";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Perl::Errors::write(\*FILE, $outputfilename, $superhash);
    close(FILE);

    # Components module

    $outputfilename = "$fgc_perl_path/Components.pm";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Perl::Components::write(\*FILE, $outputfilename, $superhash);
    close(FILE);

    # Runlog entries module

    $outputfilename = "$fgc_perl_path/Runlog_entries.pm";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Perl::Runlog_entries::write(\*FILE, $outputfilename, $superhash);
    close(FILE);

    # Classes

    print "********** Beginning generation of Perl client class modules **********\n\n";

    foreach my $class (values(%{$superhash->{classes}}))
    {
        # Make directory for class

        my $class_dir = "$fgc_perl_path/Class/$class->{id}";
        mkpath($class_dir);

        $superhash->{current_class} = $class;

        # Auto module

        $outputfilename = "$class_dir/Auto.pm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Perl::Class::Auto::write(\*FILE, $outputfilename, $superhash);
        close(FILE);

        # API Snapshot

        $outputfilename = "$class_dir/API.pm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Perl::Class::API::write(\*FILE, $outputfilename, $superhash);
        close(FILE);

        delete $superhash->{current_class};
    }

    print "********** Generation of Perl client class modules completed successfully **********\n\n";

    # Platforms

    print "********** Beginning generation of Perl client platform modules **********\n\n";

    foreach my $platform (values(%{$superhash->{platforms}}))
    {
        # Make directory for platform

        my $platform_dir = "$fgc_perl_path/Platform/"."\u$platform->{name}";
        mkpath($platform_dir);

        $superhash->{current_platform} = $platform;

        # Auto module

        print "********** Beginning generating Perl client auto module for platform $platform->{name} ... **********\n\n";

        $outputfilename = "$platform_dir/Auto.pm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Perl::Platform::Auto::write(\*FILE, $outputfilename, $superhash);
        close(FILE);

        delete $superhash->{current_platform};
    }

    print "********** Generation of Perl client platform modules completed successfully **********\n\n";
}

# EOF
