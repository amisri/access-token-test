#!/usr/bin/perl -w
#
# Name:     Output/Python/Writer.pm
# Purpose:  Provides functions to generate Python files
# Author:   Marc Magrans de Abril

package Output::Python::Writer;

use Exporter;
use File::Path qw(mkpath);
use Output::Python::Class::Fgc_xyz;
use Output::Python::Properties;
use Output::Python::RegFgc3::Converters;
use Output::Python::RegFgc3::Parameters;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    # Properties module  
    my $fgc_python_path = "$projpath/sw/clients/python/fgc/fgc";
    mkpath($fgc_python_path);

    my $outputfilename = "$fgc_python_path/properties.py";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Python::Properties::write(\*FILE, $outputfilename, $superhash);
    close(FILE);

    # RegFgc3 
    print "********** Beginning generation of Python RegFgc3 converter modules **********\n\n";

    foreach my $converter (values(%{$superhash->{regfgc3}->{converters}}))
    {
        my $outputfilename = "$fgc_python_path/regfgc3_converters.py";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Python::RegFgc3::Converters::write(\*FILE, $outputfilename, $superhash);
        close(FILE);
        
    }

    print "********** Generation of Python RegFgc3 converter modules completed successfully **********\n\n";

    print "********** Beginning generation of Python RegFgc3 parameter modules **********\n\n";

    foreach my $board (values(%{$superhash->{regfgc3}->{boards}}))
    {
        my $outputfilename = "$fgc_python_path/regfgc3_parameters.py";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Python::RegFgc3::Parameters::write(\*FILE, $outputfilename, $superhash);
        close(FILE);
        
    }

    print "********** Generation of Python RegFgc3 parameters completed successfully **********\n\n";

    # Classes
    print "********** Beginning generation of Python client class modules **********\n\n";
    
    $fgc_python_path = "$projpath/sw/clients/python/pyfgc_decoders/pyfgc_decoders";
    mkpath($fgc_python_path);

    foreach my $class (values(%{$superhash->{classes}}))
    {
        # Make directory for class

        my $class_dir = "$fgc_python_path/classes";
        mkpath($class_dir);

        $superhash->{current_class} = $class;

        # Auto module

        my $outputfilename = "$class_dir/fgc_$class->{id}.py";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Python::Class::Fgc_xyz::write(\*FILE, $outputfilename, $superhash);
        close(FILE);

        # TODO: API snapshot

        delete $superhash->{current_class};
    }

    print "********** Generation of Python client class modules completed successfully **********\n\n";
}

# EOF
