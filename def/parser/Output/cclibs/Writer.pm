#!/usr/bin/perl -w
#
# Name:     Output/cclibs/Writer.pm
# Purpose:  Provides functions to generate files for CCLIBS
# Author:   Quentin King

package Output::cclibs::Writer;

use Exporter;
use File::Path qw(mkpath);
use Output::cclibs::evtlogSymlists;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);

# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of cclibs files **********\n\n";

    my $inc_path = "$projpath/sw/inc";

    # Generate class-specific files

    foreach my $class (values(%{$superhash->{classes}}))
    {
        # Make header file directory for class

        my $class_csv_dir = "$projpath/def/src/classes/$class->{id}";
        mkpath($class_csv_dir);

        $superhash->{current_class} = $class;

        my $outputfilename = "$class_csv_dir/evtlog_symlists.csv";

        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::cclibs::evtlogSymlists::write(\*FILE, $outputfilename, $superhash);
        close(FILE);

        delete $superhash->{current_class};
    }

    print "********** Generation of cclibs files completed successfully **********\n\n";
}

# EOF
