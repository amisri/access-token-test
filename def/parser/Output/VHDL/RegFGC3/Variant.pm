#!/usr/bin/perl -w
#
# Name:     Output/VHDL/RegFGC3/Variant.pm
# Purpose:  Provides functions to generate VHDL for RegFGC3 board variant's ID and API version
# Author:   Stephen Page

package Output::VHDL::RegFGC3::Variant;

use Exporter;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$$$)
{
    my ($outputfile, $outputfilename, $superhash, $variant_name, $variant_number, $version_number) = @_;

    # Write the header

    print $outputfile   "--//*****************************************************************************\n",
                        "--// $outputfilename\n",
                        "--// This file is automatically generated - DO NOT EDIT\n",
                        "--// DESCRIPTION : Definition of the variant and API version used for this Converter\n",
                        "--//",
                        "--//*****************************************************************************\n",
                        "\n",
                        "library IEEE;\n",
                        "use IEEE.Std_logic_1164.all;\n",
                        "use IEEE.Numeric_std.all;\n",
                        "\n",
                        "package A_FGC3CVM_variant_pkg is\n",
                        "\n",
                        "--//--------------------------------------------------------------\n",
                        "\n",
                        "constant FPGA_FGC3CVM_VARIANT_ID:   integer range 0 to 1023 := $variant_number;  --// Program for $variant_name\n",
                        "constant FPGA_FGC3CVM_API_REVISION:  integer range 0 to 1023 := $version_number;\n",
                        "\n",
                        "--//--------------------------------------------------------------\n",
                        "\n",
                        "end package A_FGC3CVM_variant_pkg;\n",
                        "\n",
                        "package body A_FGC3CVM_variant_pkg is\n",
                        "\n",
                        "end;\n";
}

# EOF
