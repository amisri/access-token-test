#!/usr/bin/perl -w
#
# Name:     Output/VHDL/Writer.pm
# Purpose:  Provides functions to generate VHDL files
# Author:   Stephen Page

package Output::VHDL::Writer;

use Carp;
use Exporter;
use File::Path qw(mkpath);
use List::Util;
use Output::VHDL::RegFGC3::Block;
use Output::VHDL::RegFGC3::Variant;
use Scalar::Util;

use strict;

use constant BLOCKS_NUM => 7; 

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of VHDL files **********\n\n";

    my $vhdl_path = "$projpath/hw/vhdl";

    # Generate RegFGC3 board files
    for my $board (keys(%{$superhash->{regfgc3}->{boards}}))
    {
        # Generate RegFGC3 device 
        for my $device (keys(%{$superhash->{regfgc3}->{boards}->{$board}->{devices}}))
        {
            # Generate RegFGC3 variant files
            for my $variant_name (keys (%{$superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}}))
            {
                my @variant_keys   = keys %{$superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}->{$variant_name}};
                my @numerical_keys = grep(/\d+/, @variant_keys); 
                my $latest_version = List::Util::max(@numerical_keys);
                my $variant        = $superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}->{$variant_name};
                my $api_definition = $superhash->{regfgc3}->{boards}->{$board}->{devices}->{$device}->{variants}->{$variant_name}->{$latest_version};
                
                # Make the VHDL directories

                my $vhdl_path = "$vhdl_path/regfgc3/boards/$board/$device/$variant_name/src/rtl";
                mkpath("$vhdl_path/FGC3CVM/Read");
                mkpath("$vhdl_path/FGC3CVM/Write");

                # Write the VHDL file
                my $outputfilename = "$vhdl_path/A_FGC3CVM_variant_pkg.vhd";

                open(FILE, ">", $outputfilename) or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
                binmode(FILE);
                Output::VHDL::RegFGC3::Variant::write(\*FILE, $outputfilename, $superhash, $variant_name, $variant->{variant_symbol}->{value},  $latest_version);
                close(FILE);

                # Write each block

                for my $block_type ("Read", "Write")
                {
                    for my $block_index (0..BLOCKS_NUM - 1)
                    {
                        $outputfilename = "$vhdl_path/FGC3CVM/$block_type/A_FGC3CVM_${block_type}_Block_${block_index}_pkg.vhd";
                        
                        my $block = $block_type eq "Read" ? $api_definition->{read_blocks}[$block_index] : $api_definition->{write_blocks}[$block_index];
                        if (!defined($block))
                        {
                            # Empty block must have at least one element for generated vhdl
                            $block->{type}   = $block_type;
                            $block->{number} = $block_index;
                        }
                        
                        open(FILE, ">", $outputfilename) or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
                        binmode(FILE);
                        Output::VHDL::RegFGC3::Block::write(\*FILE, $outputfilename, $superhash, $block);
                        close(FILE);
                    }
                }
            }
        }
    }

    print "********** Generation of VHDL files completed successfully **********\n\n";
}

# EOF
