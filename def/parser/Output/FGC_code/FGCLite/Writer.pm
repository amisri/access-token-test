# Name:     Output/FGC_code/FGCLite/Writer.pm
# Purpose:  Provides functions to generate FGCLite code files
# Author:   Stephen Page

package Output::FGC_code::FGCLite::Writer;

use Exporter;
use File::Basename;
use File::Path qw(mkpath);
use Output::FGC_code::FGCLite::SysDb;
use Output::FGC_code::FGCLite::CompDb;
use Output::FGC_code::FGCLite::DimDb;
use Output::FGC_code::Writer;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($proj_path, $superhash) = @_;

    my $code_path = "$proj_path/sw/fgc/codes";

    my $platform_paths    =
        {
            compdb => "$code_path/C027_90_SysDB-C028_90_CompDB-C029_90_DIMDB/compdb.bin",
            dimdb  => "$code_path/C027_90_SysDB-C028_90_CompDB-C029_90_DIMDB/dimdb.bin",
            sysdb  => "$code_path/C027_90_SysDB-C028_90_CompDB-C029_90_DIMDB/sysdb.bin",
        };

    # Hardcode platfrom name which is used to extract filtered data from superhash

    my $platform_name     = 'fgclite';

    # Extract some data from superhash to pass them to functions which generates databases

    my $platform          = $superhash->{platforms}->{$platform_name};

    my $consts            = $superhash->{platforms}->{fgc2}->{classes}->{50}->{consts};

    my $dim_names         = $superhash->{dim_names};

    my $log_menu_names    = $superhash->{log_menu_names};

    my $log_menu_props    = $superhash->{log_menu_props};

    # Construct an array of systems for this platform

    my @platform_systems    = Output::FGC_code::Writer::get_platform_systems($superhash, $platform_name);

    # Construct an array of components for this platform

    my @platform_components = Output::FGC_code::Writer::get_platform_components($superhash, $platform_name);

    # Construct an array of DIM types for this platform

    my @platform_dim_types  = Output::FGC_code::Writer::get_platform_dim_types($superhash, "fgc2");

    # Generate FGC DBs

    # Open files

    my %files;
    for my $db_name (keys(%$platform_paths))
    {
        my $path = $platform_paths->{$db_name};
         mkpath(dirname($path));
        open($files{$db_name}, '>', $path)
            or confess "$path(9999): ERROR Unable to open file for writing : $!\n";
        binmode($files{$db_name});
    }

    Output::FGC_code::FGCLite::DimDb::write(  $files{dimdb},  $consts, \@platform_dim_types, $dim_names);

    Output::FGC_code::FGCLite::SysDb::write(  $files{sysdb},  $consts, \@platform_systems, $log_menu_names, $log_menu_props);

    Output::FGC_code::FGCLite::CompDb::write( $files{compdb}, $consts, \@platform_systems, \@platform_components);

    for my $file (values(%files))
    {
        close($file);
    }
}

# EOF
