# Name:     Output/FGC_code/FGCLite/DimDb.pm
# Purpose:  Generate Dim database and log
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGCLite::DimDb;

use Exporter;
use strict;
use Carp;
use Output::FGC_code::SafePack qw( safe_pack_string );

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);

# Begin functions

sub pack_heap($$)
{
    my ( $consts, $sorted_symbols ) = @_;
  
    my $packed_heap = ''; 

    # Generate heap
    
    $packed_heap .= pack("C", 0); 
    my $heap_size = 1; 
    
    foreach my $symbol (@$sorted_symbols)
    {       
        $packed_heap .= pack("a".(length($symbol) + 1), $symbol);
        $heap_size += length($symbol) + 1;
    }
    
    my $packed_heap_size = length($packed_heap); # heap size in bytes
     
    # Heap size will be stored in words, so make sure it is aligned by padding the remainder with null bytes
    
    my $word_size = length(pack("l"));   # word size in bytes
    my $bytes;
    
    if(($packed_heap_size % $word_size) ne 0)
    {
         $bytes = $word_size - ($packed_heap_size % $word_size);
         $packed_heap .= pack("x".$bytes);
         $packed_heap_size = length($packed_heap);
    }   
    
    # Pack heap size (in words) on top
    
    my $heap = pack("n", $packed_heap_size / $word_size);
    
    $heap .= $packed_heap;
    
    return $heap;
}

# Write DIMDB version

sub pack_version($)
{
    my ( $consts ) = @_;

    pack("n", $consts->{DIMDB_VERSION});
}

sub pack_names($$)
{
    my ( $consts, $dim_names ) = @_;

    my $packed_names = '';

    # Generate names array

    foreach my $dim_name (sort
                          {
                            $dim_names->{$a}->{dim_name_idx}
                            <=>
                            $dim_names->{$b}->{dim_name_idx}
                          } keys(%{$dim_names}))
    {
        $packed_names .= safe_pack_string($consts->{DIMDB_MAX_DIM_NAME_LEN} + 1, $dim_name);
    }
                            
    return $packed_names;
}


sub pack_headers($$)
{
    my ( $dim_names, $sorted_dim_types ) = @_;

    my $packed_headers = '';

    # Generate n_dim_names

    $packed_headers .= pack("n", $dim_names);

    # Generate n_dim_types

    $packed_headers .= pack("n", $sorted_dim_types);

    return $packed_headers;
}


sub pack_ana($$$)
{
    my ( $consts, $ana_channels, $symbols ) = @_;

    my $packed_ana = '';

    my $ana_size =    4 + # gain
                      4 + # offset
                      2 + # label
                      2;  # units


    # Generate ana array

    foreach my $ana_channel (@$ana_channels)
    {
        # Pad if channel is not defined

        if(!defined($ana_channel))
        {
           $packed_ana .= pack("x".($ana_size));
           next;
        }

        my $gain    = unpack('I', pack('f', $ana_channel->{gain}));
        my $offset  = unpack('I', pack('f', $ana_channel->{offset}));

        $packed_ana .= pack( "N".    # gain
                             "N".    # offset
                             "n".    # label
                             "n",    # units

                             $gain,
                             $offset,
                             $symbols->{$ana_channel->{label}},
                             $symbols->{$ana_channel->{units}});
    }
    $packed_ana .= pack("x".($ana_size *
                            ($consts->{N_DIM_ANA_CHANS} - @$ana_channels)));

    return $packed_ana;
}

sub pack_dig($$$)
{
    my ( $consts, $dig_banks, $symbols ) = @_;

    my $packed_dig = '';

    my $dig_size =    2 + # chan_lbl
                      2 + # zero_lbl
                      2;  # one_lbl

    # Generate dig array

    foreach my $dig_bank (@$dig_banks[4..5])
    {
        my $faults_mask = 0;

        foreach my $input (@{$dig_bank->{inputs}})
        {
            # Pad if input is not defined

            if(!defined($input))
            {
                $packed_dig .= pack("x".($dig_size));
                next;
            }

            $packed_dig .= pack( "n".    # chan_lbl
                                 "n".    # one_lbl
                                 "n",    # zero_lbl

                                 $symbols->{$input->{label}},
                                 $symbols->{$input->{one}},
                                 $symbols->{$input->{zero}});

            $faults_mask |= ($input->{fault} << $input->{index});
        }
        $packed_dig .= pack("x".($dig_size *
                                ($consts->{N_DIM_DIG_INPUTS} - @{$dig_bank->{inputs}})));

        # Generate faults mask for the bank

        $packed_dig .= pack("n", $faults_mask);

    }

    return $packed_dig;
}


sub pack_dimdb($$$)
{
    my ( $consts, $sorted_dim_types, $dim_names ) = @_;

    # Generate DimDB code

    my $fgc_dimdb = '';

    # Construct hash of all symbols

    my %symbols;

    for my $dim_type (@$sorted_dim_types)
    {
        foreach my $ana_channel (@{$dim_type->{ana_channels}})
        {
            next if(!defined($ana_channel));

            $symbols{$ana_channel->{label}} = 0 if(!defined($symbols{$ana_channel->{label}}));
            $symbols{$ana_channel->{units}} = 0 if(!defined($symbols{$ana_channel->{units}}));
        }

        foreach my $dig_bank (@{$dim_type->{dig_banks}})
        {
            foreach my $input (@{$dig_bank->{inputs}})
            {
                next if(!defined($input));

                $symbols{$input->{label}}   = 0 if(!defined($symbols{$input->{label}}));
                $symbols{$input->{one}}     = 0 if(!defined($symbols{$input->{one}}));
                $symbols{$input->{zero}}    = 0 if(!defined($symbols{$input->{zero}}));
            }
        }
    }

    # Set symbol indexes

    my $index           = 1;
    my @sorted_symbols  = sort(keys(%symbols));
    foreach my $symbol (@sorted_symbols)
    {
        $symbols{$symbol} = $index;

        $index += length($symbol) + 1;
    }
 
    $fgc_dimdb .= pack_version($consts);

    # Generate heap

    $fgc_dimdb .= pack_heap($consts, \@sorted_symbols);   
    
    # Pack number of dim names and types

    $fgc_dimdb .= pack_headers(keys(%{$dim_names}), @$sorted_dim_types);
    
    # Generate names array
    
    $fgc_dimdb .= pack_names($consts, $dim_names);     

    foreach my $dim_type (@$sorted_dim_types)
    {
        # Generate ana array

        $fgc_dimdb .= pack_ana($consts, $dim_type->{ana_channels}, \%symbols);

        # Generate dig array

        $fgc_dimdb .= pack_dig($consts, $dim_type->{dig_banks}, \%symbols);
    }

    # Return packed dim database
    
    return $fgc_dimdb;
}


sub write($$$$)
{
    my ( $dimdb_file, $consts, $sorted_dim_types, $dim_names ) = @_;

    # Generate DimDB code

    my $fgc_dimdb = pack_dimdb($consts, $sorted_dim_types, $dim_names);

    # Write data to file

    print $dimdb_file $fgc_dimdb;
}

# EOF
