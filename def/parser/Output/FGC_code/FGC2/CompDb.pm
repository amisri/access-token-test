# Name:     Output/FGC_code/FGC2/CompDb.pm
# Purpose:  Generate Comp database
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGC2::CompDb;

use Exporter;
use strict;
use Output::FGC_code::SafePack qw( safe_pack_string );

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);


return(1);


# Begin functions

sub pack_headers($$)
{
    my ( $consts, $components ) = @_;

    my $packed_headers = '';

    $packed_headers .= pack("n", $consts->{COMPDB_VERSION});

    # Number of all defined components

    $packed_headers .= pack("n", $components);

    return $packed_headers;
}

sub pack_component($$)
{
    my ( $consts, $component ) = @_;

    my $packed_component = '';

    # Pack type also known as barcode

    $packed_component .= safe_pack_string($consts->{COMPDB_COMP_LEN} + 1, $component->{barcode});

    # Pack label

    $packed_component .= safe_pack_string($consts->{COMPDB_COMP_LBL_LEN} + 1, $component->{label});

    # Pack prop_temp

    $packed_component .= safe_pack_string($consts->{COMPDB_PROP_LEN}, $component->{temp_prop}    || '');

    # Pack barcode_prop

    $packed_component .= safe_pack_string($consts->{COMPDB_PROP_LEN}, $component->{barcode_prop} || '');

    return $packed_component;
}

# Generate group array

sub pack_group($$$)
{
    my ( $consts, $component, $sorted_systems ) = @_;

    my $index = 0;
    my $packed_group = '';

    if(defined($component->{sys_groups}))
    {
        foreach my $system (@$sorted_systems)
        {
            if ($component->{sys_groups}->{$system->{type}}){
                $packed_group .= pack("C", $index);
                $packed_group .= pack("C", $component->{sys_groups}->{$system->{type}});
            }
            $index++;     
        }
    }

    return $packed_group;
}

sub pack_compdb($$$)
{
    my ($consts, $sorted_systems, $sorted_components) = @_;

    my $fgc_compdb = '';
    my $fgc_compdb_comps = '';
    my $fgc_compdb_groups = '';
    my $fgc_group_tmp = '';


    my $fgc_groups_size = 0;
    my $fgc_groups_offset = 0;

    $fgc_compdb .= pack_headers($consts, @$sorted_components);

    foreach my $component (@$sorted_components)
    {
        $fgc_group_tmp = pack_group($consts, $component, $sorted_systems);
        $fgc_groups_size = (length($fgc_group_tmp)/2);
        
        $fgc_compdb_comps .= pack_component($consts, $component);
        $fgc_compdb_comps .= pack("n", $fgc_groups_size);
        $fgc_compdb_comps .= pack("n", $fgc_groups_offset);
        
        # Generate group array
        
        $fgc_compdb_groups .= $fgc_group_tmp;
        
        $fgc_groups_offset += $fgc_groups_size;
    }
    
    my $word_size = length(pack("l"));   # word size in bytes
    my $packed_comps_size = length($fgc_compdb_comps);

    if(($packed_comps_size % $word_size) ne 0)
    {
         my $bytes = $word_size - ($packed_comps_size % $word_size);
         $fgc_compdb_comps .= pack("x".$bytes);
    } 

    $fgc_compdb .= $fgc_compdb_comps;
    $fgc_compdb .= $fgc_compdb_groups;

    return $fgc_compdb;
}

sub write($$$$)
{
    my ($compdb_file, $consts, $sorted_systems, $sorted_components) = @_;

    my $fgc_compdb = pack_compdb($consts, $sorted_systems, $sorted_components);

    # Write data to file

    print $compdb_file $fgc_compdb;
}

# EOF
