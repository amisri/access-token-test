# Name:     Output/FGC_code/FGC2/SysDb.pm
# Purpose:  Generate Sys database and log
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGC2::SysDb;

use Exporter;
use strict;
use Output::FGC_code::SafePack qw( safe_pack_string );

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);


return(1);

# Begin functions

# Pack headers about db version and how many systems there are

sub pack_headers($$)
{
    my ( $consts, $systems ) = @_;

    my $packed_headers = '';

    $packed_headers   .= pack("n", $consts->{SYSDB_VERSION});
    $packed_headers   .= pack("n", 1 + scalar($systems));  # n_sys

    return $packed_headers;
}

# Pack menu_name array

sub pack_menu_names($$)
{
    my ( $consts, $log_menu_names ) = @_;

    my $packed_menu_names = '';

    foreach my $menu_name (sort
                           {
                            $log_menu_names->{$a}
                            <=>
                            $log_menu_names->{$b}
                           } (keys(%{$log_menu_names})))
    {
        $packed_menu_names .= safe_pack_string($consts->{SYSDB_MENU_NAME_LEN}, $menu_name);
    }
    $packed_menu_names .= pack("x".($consts->{SYSDB_MENU_NAME_LEN} *
                                   ($consts->{MAX_LOG_MENU_NAMES} - keys(%{$log_menu_names}))));

    return $packed_menu_names;
}

# Pack menu_prop array

sub pack_menu_props($$)
{
    my ( $consts, $log_menu_props ) = @_;

    my $packed_menu_props = '';

    foreach my $menu_prop (sort
                           {
                            $log_menu_props->{$a}
                            <=>
                            $log_menu_props->{$b}
                           } (keys(%{$log_menu_props})))
    {
        $packed_menu_props .= safe_pack_string($consts->{SYSDB_MENU_PROP_LEN}, $menu_prop);
    }
    $packed_menu_props .= pack("x".($consts->{SYSDB_MENU_PROP_LEN} *
                                   ($consts->{MAX_LOG_MENU_NAMES} - keys(%{$log_menu_props}))));

    return $packed_menu_props;
}

# Pack sys array

sub pack_system_desc($$$)
{
    my ( $consts, $type, $label ) = @_;

    my $packed_system_desc = '';

    $packed_system_desc .= safe_pack_string($consts->{SYSDB_SYS_LEN} + 1, $type);
    $packed_system_desc .= safe_pack_string($consts->{SYSDB_SYS_LBL_LEN} + 1, $label);

    # pac after label

    $packed_system_desc .= pack('x');
}

# Pack required array

sub pack_system_required($$)
{
    my ( $consts, $groups ) = @_;

    my $packed_system_required = '';

    my @sorted_groups = sort { $a->{index} <=> $b->{index} } values(%{$groups});

    # Pack required[0]

    $packed_system_required .= pack( 'C', 0 );

    foreach my $group (@sorted_groups)
    {
        $packed_system_required .= pack("C", $group->{required});
    }
    $packed_system_required .= pack("C".($consts->{N_COMP_GROUPS} - 1 - @sorted_groups), 0);

    return $packed_system_required;
}

# Pack menu indexes array

sub pack_system_menu_idxs($$)
{
    my ( $consts, $log_menus ) = @_;

    my $packed_system_menu_idxs = '';

    # Generate n_log_menus

    if(!defined($log_menus))
    {
        $log_menus = [];
    }
    
    $packed_system_menu_idxs .= pack("n", scalar(@$log_menus));
    
    # Generate menu_name_idx array

    foreach my $log_menu (@$log_menus)
    {
        $packed_system_menu_idxs .= pack("C", $log_menu->{log_menu_name_idx});
    }
    $packed_system_menu_idxs .= pack("x".($consts->{MAX_LOG_MENUS} - @$log_menus));

    # Generate menu_prop_idx array

    foreach my $log_menu (@$log_menus)
    {
        $packed_system_menu_idxs .= pack("C", $log_menu->{log_menu_prop_idx});
    }
    $packed_system_menu_idxs .= pack("x".($consts->{MAX_LOG_MENUS} - @$log_menus));

    return $packed_system_menu_idxs;
}

# Pack dims array

# Dims array has holes with 'undef' values, which should be packed with default values

# Also end of the list needs to be filled with this default values

sub pack_system_dims($$)
{
    my ( $consts, $dims )  = @_;

    my $packed_system_dims = '';

    my %unused_dim = (
                        dim_idx     => 0xFF,
                        bus_addr    => 0,
                        name_idx    => 0,
                        type        => { platform_dim_type_idx => 0 }
                     );

     # Generate dims array

    foreach my $dim (@$dims)
    {
        $dim = \%unused_dim if(!defined($dim));

        $packed_system_dims .= pack( "C".    # dim_idx
                                     "C".    # bus_addr
                                     "n".    # name_idx
                                     "n",    # dim_type_idx

                                     $dim->{dim_idx},
                                     $dim->{bus_addr},
                                     $dim->{name_idx},
                                     $dim->{type}->{platform_dim_type_idx});
    }

    # Pad to the end of dims array

    for(my $i = $consts->{MAX_DIMS} - @$dims ; $i ; $i--)
    {
        $packed_system_dims .= pack( "C".    # dim_idx
                                     "C".    # bus_addr
                                     "n".    # name_idx
                                     "n",    # dim_type_idx

                                     $unused_dim{dim_idx},
                                     $unused_dim{bus_addr},
                                     $unused_dim{name_idx},
                                     $unused_dim{type}->{platform_dim_type_idx});
    }

    return $packed_system_dims;
}

sub pack_sysdb($$$$)
{
    my ($consts, $sorted_systems, $log_menu_names, $log_menu_props) = @_;

    my $fgc_sysdb = '';

    $fgc_sysdb .= pack_headers($consts, @$sorted_systems - 1);

    $fgc_sysdb .= pack_menu_names($consts, $log_menu_names);

    $fgc_sysdb .= pack_menu_props($consts, $log_menu_props);

    foreach my $system ( @$sorted_systems )
    {
        $fgc_sysdb .= pack_system_desc($consts, $system->{type}, $system->{label});

        $fgc_sysdb .= pack_system_required($consts, $system->{groups});

        $fgc_sysdb .= pack_system_menu_idxs($consts, $system->{log_menus});

        $fgc_sysdb .= pack_system_dims($consts, $system->{dims});
    }

    # Return packed sys database

    return $fgc_sysdb;
}

# Sysdb

sub write($$$$$)
{
    my ($sysdb_file, $consts, $sorted_systems, $log_menu_names, $log_menu_props) = @_;

    # Generate system database

    my $fgc_sysdb = pack_sysdb($consts, $sorted_systems, $log_menu_names, $log_menu_props);

    # Write data to file

    print $sysdb_file $fgc_sysdb;
}

# EOF
