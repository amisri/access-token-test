# Name:     Output/FGC_code/FGC3/SysDb.pm
# Purpose:  Generate Sys database and log
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGC3::SysDb;

use Exporter;
use strict;
use Output::FGC_code::Heap qw(make_get_offset pack_heap);
use Output::FGC_code::SafePack qw( safe_pack_string );

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);

# Begin functions

# Pack headers about db version and how many systems there are

sub pack_headers($$$$)
{
    my ( $consts, $systems, $log_menu_names, $log_menu_props ) = @_;

    my $packed_headers = '';

    $packed_headers   .= pack("n", $consts->{SYSDB_VERSION});
    $packed_headers   .= pack("n", scalar($systems) + 1);            # n_sys
    $packed_headers   .= pack("n", scalar(keys(%$log_menu_names)));  # n_menu_names
    $packed_headers   .= pack("n", scalar(keys(%$log_menu_props)));  # n_menu_props

    return $packed_headers;
}

# Pack menu_name array

sub pack_menu_names($$)
{
    my ( $consts, $log_menu_names ) = @_;

    my $packed_menu_names = '';

    foreach my $menu_name (sort
                           {
                            $log_menu_names->{$a}
                            <=>
                            $log_menu_names->{$b}
                           } (keys(%{$log_menu_names})))
    {
        $packed_menu_names .= safe_pack_string($consts->{SYSDB_MENU_NAME_LEN}, $menu_name);
    }

    return $packed_menu_names;
}

# Pack menu_prop array

sub pack_menu_props($$)
{
    my ( $consts, $log_menu_props ) = @_;

    my $packed_menu_props = '';

    foreach my $menu_prop (sort
                           {
                            $log_menu_props->{$a}
                            <=>
                            $log_menu_props->{$b}
                           } (keys(%{$log_menu_props})))
    {
        $packed_menu_props .= safe_pack_string($consts->{SYSDB_MENU_PROP_LEN}, $menu_prop);
    }

    return $packed_menu_props;
}

# Pack flags

sub pack_flags($$)
{
    my ( $consts, $system ) = @_;

    my $sysdb_flag_mask = 'SYSDB_FLAG_MASK_';

    my $flags = 0;

    # Look for setted system flag attributes

    for my $flag_name (keys(%{$system->{flags}}))
    {
        # Get the bitmask name and value

        my $sysdb_flag_mask_name = uc($sysdb_flag_mask.$flag_name);

        my $sysdb_flag_mask_value = $consts->{$sysdb_flag_mask_name};

        # Update flags

        if ($system->{flags}->{$flag_name} && defined($consts->{$sysdb_flag_mask_name}))
        {
            $flags |= $system->{flags}->{$flag_name} << ($sysdb_flag_mask_value-1);
        }
    }

    return pack( 'n', $flags );
}

# Pack log menus size

sub pack_log_menus($)
{
    my ( $log_menus ) = @_;

    # Pack n_log_menus

    return pack( 'C', $log_menus );
}

# Pack sys array

sub pack_system_desc($$$$$)
{
    my ( $consts, $type, $label, $heap, $get_offset ) = @_;

    my $packed_system_desc = '';

    # Pack type

    $packed_system_desc .= safe_pack_string($consts->{SYSDB_SYS_LEN} + 1, $type );

    # Pack label

    $packed_system_desc .= pack( 'n', $get_offset->($heap, "$label\0") );

    return $packed_system_desc;
}

# Pack required array

sub pack_system_required($$$$)
{
    my ( $consts, $groups, $heap, $get_offset ) = @_;

    # Pack required[0]

    my $packed_system_required = pack( 'C', 0 );

    my @sorted_groups = ();

    if(defined($groups))
    {
        @sorted_groups = sort { $a->{index} <=> $b->{index} } values(%{$groups});

        foreach my $group (@sorted_groups)
        {
            $packed_system_required .= pack("C", $group->{required});
        }
    }
    $packed_system_required .= pack("C".($consts->{N_COMP_GROUPS} - 1 - @sorted_groups), 0);

    return pack('n', $get_offset->($heap, $packed_system_required));
}

# Pack menu indexes array

sub pack_system_menu_idxs($$$$)
{
    my ( $consts, $log_menus, $heap, $get_offset ) = @_;

    # Generate menu_name_idx array

    my $log_menu_name_key = '';

    foreach my $log_menu (@$log_menus)
    {
        $log_menu_name_key .= pack("C", $log_menu->{log_menu_name_idx});
    }
    $log_menu_name_key .= pack("x".($consts->{MAX_LOG_MENUS} - @$log_menus));

    my $log_menu_name_offset = pack('n', $get_offset->($heap, $log_menu_name_key));

    # Generate menu_prop_idx array

    my $log_menu_prop_key = '';

    foreach my $log_menu (@$log_menus)
    {
        $log_menu_prop_key .= pack("C", $log_menu->{log_menu_prop_idx});
    }
    $log_menu_prop_key .= pack("x".($consts->{MAX_LOG_MENUS} - @$log_menus));

    my $log_menu_prop_offset = pack('n', $get_offset->($heap, $log_menu_prop_key));

    return $log_menu_name_offset.$log_menu_prop_offset;
}

# Pack dims array

# Dims array has holes with 'undef' values, which should be packed with default values

# Also end of the list needs to be filled with this default values

sub pack_system_dims($$$)
{
    my ( $dims, $heap, $get_offset ) = @_;

    my $packed_system_dims = '';

    my $dims_size = pack('n', 0);

    my %unused_dim = (
                        dim_idx     => 0xFF,
                        bus_addr    => 0,
                        name_idx    => 0,
                        type        => { platform_dim_type_idx => 0 }
                     );

    if(defined($dims))
    {
        # Pack dims array size

        my $dims_size = pack('n', $#$dims);

        # Generate dims array

        foreach my $dim (@$dims)
        {
            $dim = \%unused_dim if(!defined($dim));

            $packed_system_dims .= pack( "C".    # dim_idx
                                         "C".    # bus_addr
                                         "C".    # name_idx
                                         "C",    # dim_type_idx

                                         $dim->{dim_idx},
                                         $dim->{bus_addr},
                                         $dim->{name_idx},
                                         $dim->{type}->{platform_dim_type_idx});
        }
    }
    my $dims_offset = pack('n', $get_offset->($heap, $packed_system_dims));

    return $dims_size.$dims_offset;
}

sub pack_sysdb($$$$)
{
    my ($consts, $sorted_systems, $log_menu_names, $log_menu_props) = @_;

    # Init heap

    my $heap = {};

    my $get_offset = make_get_offset($heap);

    # Generate SysDb code

    my $fgc_sysdb = '';

    $fgc_sysdb .= pack_headers($consts, @$sorted_systems - 1, $log_menu_names, $log_menu_props);

    $fgc_sysdb .= pack_menu_names($consts, $log_menu_names);

    $fgc_sysdb .= pack_menu_props($consts, $log_menu_props);

    foreach my $system ( @$sorted_systems )
    {
        $fgc_sysdb .= pack_flags($consts, $system);

        $fgc_sysdb .= pack_log_menus(@{ $system->{log_menus} });;

        $fgc_sysdb .= pack_system_dims($system->{dims}, $heap, $get_offset);

        $fgc_sysdb .= pack_system_desc($consts, $system->{type}, $system->{label}, $heap, $get_offset);

        $fgc_sysdb .= pack_system_required($consts, $system->{groups}, $heap, $get_offset);

        $fgc_sysdb .= pack_system_menu_idxs($consts, $system->{log_menus}, $heap, $get_offset);
    }

    $fgc_sysdb .= pack_heap( $heap );

    # Return packed sys database

    return $fgc_sysdb;
}

# Sysdb

sub write($$$$$)
{
    my ($sysdb_file, $consts, $sorted_systems, $log_menu_names, $log_menu_props) = @_;

    # Generate system database

    my $fgc_sysdb = pack_sysdb($consts, $sorted_systems, $log_menu_names, $log_menu_props);

    # Write data to file

    print $sysdb_file $fgc_sysdb;
}

# EOF
