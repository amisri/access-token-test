# Name:     Output/FGC_code/FGC3/DimDb.pm
# Purpose:  Generate Dim database and log
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGC3::DimDb;

use Carp;
use strict;
use Exporter;
use Output::FGC_code::Heap qw(make_get_offset pack_heap);

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);


return(1);

# Write DIMDB version

sub pack_version($)
{
    my ( $consts ) = @_;

    pack("n", $consts->{DIMDB_VERSION});
}

# Generate sizes of heaps

sub pack_headers($$$)
{
    my ( $n_dim_types, $n_ana_chans, $n_dig_banks ) = @_;

    my $packed_headers = '';

    $packed_headers .= pack("n", $n_dim_types);

    $packed_headers .= pack("n", $n_ana_chans);

    $packed_headers .= pack("n", $n_dig_banks);

    return $packed_headers;
}

# Generate names array

sub pack_names($$$$)
{
    my ( $consts, $dim_names, $strings_heap, $strings_get_offset ) = @_;

    my $packed_names = '';

    # Generate names array

    foreach my $dim_name (sort
                          {
                            $dim_names->{$a}->{dim_name_idx}
                            <=>
                            $dim_names->{$b}->{dim_name_idx}
                          } keys(%{$dim_names}))
    {
        $packed_names .= pack('n', $strings_get_offset->($strings_heap, "$dim_name\0"));
    }
    $packed_names .= pack('n'.($consts->{DIMDB_MAX_DIM_NAMES} - keys(%{$dim_names})));

    return $packed_names;
}

sub pack_ana($$$)
{
    my ( $ana_channel, $strings_heap, $strings_get_offset ) = @_;

    my $gain    = unpack('I', pack('f', $ana_channel->{gain}));
    my $offset  = unpack('I', pack('f', $ana_channel->{offset}));

    pack("N".    # gain
         "N".    # offset
         "n".    # label
         "n",    # units

         $gain,
         $offset,
         $strings_get_offset->($strings_heap, "$ana_channel->{label}\0"),
         $strings_get_offset->($strings_heap, "$ana_channel->{units}\0"));
}

sub pack_dig($$$$)
{
    my ( $consts, $dig_bank, $strings_heap, $strings_get_offset ) = @_;

    my $fgc_dimdb_dig_size =    2 + # chan_lbl
                                2 + # zero_lbl
                                2;  # one_lbl

    # Generate dig array

    my $faults_mask = 0;

    my $packed_dig = '';

    foreach my $input (@{$dig_bank->{inputs}})
    {
        # Pad if input is not defined

        if(!defined($input))
        {
            $packed_dig .= pack("x".($fgc_dimdb_dig_size));
            next;
        }

        $packed_dig .= pack( "n".    # chan_lbl
                             "n".    # one_lbl
                             "n",    # zero_lbl

                             $strings_get_offset->($strings_heap, "$input->{label}\0"),
                             $strings_get_offset->($strings_heap, "$input->{one}\0"  ),
                             $strings_get_offset->($strings_heap, "$input->{zero}\0" ));

        # Genearate faults mask for each input

        $faults_mask |= ($input->{fault} << $input->{index});
    }

    # Pad the rest of the inputs

    $packed_dig .= pack( '(nnn)'.($consts->{N_DIM_DIG_INPUTS} - @{$dig_bank->{inputs}}), 0, 0, 0);

    my $packed_faults_mask = pack('n', $faults_mask);

    # Return $falts_mask on the begin, then $packed dig

    return $packed_faults_mask.$packed_dig;
}


sub pack_regs($$)
{
    my ( $consts, $regs ) = @_;

    my $packed_regs = '';

    foreach my $reg (@$regs)
    {
        $packed_regs .= pack('s', $reg || 0);
    }

    $packed_regs .= pack('s'.($consts->{DIMDB_N_DIM_REGISTERS} - @$regs));

    return $packed_regs;
}

sub pack_dim_types($$$$$$$$)
{
    my ( $consts, $sorted_dim_types, $strings_heap, $strings_get_offset, $anas_heap, $anas_get_offset, $digs_heap, $digs_get_offset ) = @_;

    my $packed_dim_types = '';

    for my $dim_type (@$sorted_dim_types)
    {
        my @regs = ();

        if(defined($dim_type->{children}))
        {
            for my $channel (@{$dim_type->{children}})
            {
                next unless $channel;

                # Ensure that channel isn't already taken

                if(defined($regs[$channel->{index}]))
                {
                    confess "Error while packing dims array for DimDb\n".
                        'System:    '.$dim_type->{type}."\n".
                        'Platform:  '.(($dim_type->{platform}) ? $dim_type->{platform} : 'all' )."\n".
                        'Dim index: '.$channel->{index}." is already taken\n";
                }

                # Push to regs if it is analog or digital channel

                if ($channel->{node_tag} eq 'ana_channel')
                {
                    $regs[$channel->{index}] = - $anas_get_offset->($anas_heap, scalar(pack_ana($channel, $strings_heap, $strings_get_offset)));
                }
                elsif ($channel->{node_tag} eq 'dig_bank')
                {
                    $regs[$channel->{index}] = + $digs_get_offset->($digs_heap, scalar(pack_dig($consts, $channel, $strings_heap, $strings_get_offset)));
                }
            }
        }
        $packed_dim_types .= pack_regs($consts, \@regs);
    }

    return $packed_dim_types;
}

sub pack_dimdb($$$)
{
    my ( $consts, $sorted_dim_types, $dim_names ) = @_;

    # DimDB code

    my $fgc_dimdb = '';

    # Initialize heaps

    my $strings_heap = {};

    my $strings_get_offset = make_get_offset($strings_heap);

    my $anas_heap = {};

    my $anas_get_offset = make_get_offset($anas_heap);

    # Pack something alligned to 2 bytes to not use offset 0 as it means not used channel

    $anas_get_offset->($anas_heap, pack('n', 0));

    my $digs_heap = {};

    my $digs_get_offset = make_get_offset($digs_heap);

    # Pack something alligned to 2 bytes to not use offset 0 as it means not used channel

    $digs_get_offset->($digs_heap, pack('n', 0));

    # Generate dim_types

    # Side effect is constructing all the heaps

    my $packed_dim_types = pack_dim_types($consts, $sorted_dim_types, $strings_heap, $strings_get_offset, $anas_heap, $anas_get_offset, $digs_heap, $digs_get_offset);

    # Generate dim db

    # Generate current database version

    $fgc_dimdb .= pack_version($consts);

    # Generate headers which contains sizes of all the heaps

    # They are known as they are constructed already

    # Substruct one from anas and digs heap becauce first elements are dummy ones (2bytes of zeros)

    $fgc_dimdb .= pack_headers( scalar(@$sorted_dim_types), keys(%{$anas_heap})-1, keys(%{$digs_heap})-1 );

    # Generate names array

    $fgc_dimdb .= pack_names($consts, $dim_names, $strings_heap, $strings_get_offset);

    # Append already generated packed_dim_types

    $fgc_dimdb .= $packed_dim_types;

    # Generate ana heap

    $fgc_dimdb .= pack_heap($anas_heap);

    # Generate dig heap

    $fgc_dimdb .= pack_heap($digs_heap);

    # Generate string heap

    $fgc_dimdb .= pack_heap($strings_heap);

    # Return packed dim database

    return $fgc_dimdb;
}


sub write($$$$)
{
    my ( $dimdb_file, $consts, $sorted_dim_types, $dim_names ) = @_;

    # Generate DimDB code

    my $fgc_dimdb = pack_dimdb($consts, $sorted_dim_types, $dim_names);

    # Write data to file

    print $dimdb_file $fgc_dimdb;
}

# EOF
