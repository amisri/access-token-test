# Name:     Output/FGC_code/FGC3/CompDb.pm
# Purpose:  Generate Comp database
# Author:   Stephen Page
# Notes:    See sw/fgc/mcu/inc/fgc_*db.h for details of generated data structures
#           See def/doc/sys_comp_dim_db_dizes.xlsx for size calculations for data structures
#           Increment XXXDB_VERSION if the structure changes in a way that needs a change of software in the FGC

package Output::FGC_code::FGC3::CompDb;

use Exporter;
use strict;
use Output::FGC_code::Heap qw(make_get_offset pack_heap);
use Output::FGC_code::SafePack qw( safe_pack_string );

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub pack_headers($$)
{
    my ( $consts, $components ) = @_;

    my $packed_headers = '';

    $packed_headers .= pack("n", $consts->{COMPDB_VERSION});

    # Number of all defined components

    $packed_headers .= pack("n", $components);

    return $packed_headers;
}

sub pack_component($$$$)
{
    my ( $consts, $component, $heap, $get_offset ) = @_;

    my $packed_component = '';

    # Pack type also known as barcode

    $packed_component .= safe_pack_string($consts->{COMPDB_COMP_LEN} + 1, $component->{barcode});

    # Pack label

    $packed_component .= pack('n', $get_offset->( $heap, "$component->{label}\0"));

    # Pack legacy bus field, it's always 0 right now

    $packed_component .= safe_pack_string(2, 0);

    # Pack prop_temp

    $packed_component .= pack('n', $get_offset->( $heap, ($component->{temp_prop} || '')."\0"));

    # Pack barcode_prop

    $packed_component .= pack('n', $get_offset->( $heap, ($component->{barcode_prop} || '')."\0"));

    return $packed_component;
}

# Generate group array

sub pack_group($$$$$)
{
    my ( $consts, $component, $sorted_systems, $heap, $get_offset ) = @_;

    my $packed_group = '';

    if(defined($component->{sys_groups}))
    {
        # pack group[0] as it is always 0

        $packed_group .= pack('C', 0);

        foreach my $system (@$sorted_systems)
        {
            $packed_group .= pack("C", $component->{sys_groups}->{$system->{type}} || 0);
        }
        $packed_group .= pack("C".($consts->{MAX_SYS} - 1 - @$sorted_systems), 0);
    }
    else # Component does not appear in any systems
    {
        # Pad entire group array

         $packed_group .= pack("C".($consts->{MAX_SYS}), 0);
    }

    return pack('n', $get_offset->($heap, $packed_group));
}

sub pack_compdb($$$)
{
    my ($consts, $sorted_systems, $sorted_components) = @_;

    my $fgc_compdb_comp_size =  $consts->{COMPDB_COMP_LEN} + 1     +   # type
                                $consts->{COMPDB_COMP_LBL_LEN} + 1 +   # label
                                2                                  +   # legacy bus field
                                $consts->{COMPDB_PROP_LEN}         +   # prop_temp
                                $consts->{COMPDB_PROP_LEN}         +   # prop_barcode
                                $consts->{MAX_SYS};                    # group

    # Init heap

    my $heap = {};

    my $get_offset = make_get_offset($heap);

    # Generate CompDB code

    my $fgc_compdb = '';

    $fgc_compdb .= pack_headers($consts, @$sorted_components);

    foreach my $component (@$sorted_components)
    {
        $fgc_compdb .= pack_component($consts, $component, $heap, $get_offset);

        # Generate group array

        $fgc_compdb .= pack_group($consts, $component, $sorted_systems, $heap, $get_offset);
    }
    $fgc_compdb .= pack("x".($fgc_compdb_comp_size * ($consts->{COMPDB_N_COMPS} - @$sorted_components + 1)));

    $fgc_compdb .= pack_heap($heap);

    return $fgc_compdb;
}

sub write($$$$)
{
    my ($compdb_file, $consts, $sorted_systems, $sorted_components) = @_;

    my $fgc_compdb = pack_compdb($consts, $sorted_systems, $sorted_components);

    # Write data to file

    print $compdb_file $fgc_compdb;
}

# EOF
