# Name:     Output/FGC_code/Heap.pm
# Purpose:  Maintain a heap for databases structures
# Author:   Krystian Wojtas

package Output::FGC_code::Heap;

use Exporter;
use List::Util qw(first);
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(make_get_offset get_data get_string pack_heap);

# Begin functions

# Get offset for particular key and make it firstly if it doesn't exist yet

sub make_get_offset($)
{
    my ( $heap ) = @_;

    # Current offset - size or top of the heap

    my $heap_offset = 0;

    sub
        {
            my ( $heap, $data ) = @_;

            if (!defined($heap->{$data})) {
                $heap->{$data} = $heap_offset;
                $heap_offset += length $data;
            }
            $heap->{$data};
        }
}

# Get data from the heap basic on the specified offset

sub get_data($$)
{
    my %heap   = %{ $_[0] };
    my $offset =    $_[1];

    # Find first occurrence of the data with given offset on the heap

    first
        { $heap{$_} eq $offset }
        keys
            %heap;
}

# Get string from the heap starting on the specified offset

# It gets rid of tailing newline character used as a part of the string key

sub get_string($$)
{
    my ( $heap, $offset ) = @_;

    substr
    (
        get_data($heap, $offset),
        0,
        -1
    );
}

# Pack whole heap

sub pack_heap($)
{
    my %heap = %{ $_[0] };

    pack
        '(a*)*',
        sort
            { $heap{$a} <=> $heap{$b} }
            keys
                %heap;
}
