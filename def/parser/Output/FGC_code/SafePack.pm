# Name:     Output/FGC_code/SafePack.pm
# Purpose:  Provide safe_pack function
# Author:   Krystian Wojtas
#

package Output::FGC_code::SafePack;

use Exporter;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(safe_pack_string);

return(1);

# Begin functions

# Sub packs provided $string using built-in pack function

# but it also ensure that $string to pack is under provided $max_length limi

# Then packed $string is not truncated

sub safe_pack_string($$)
{
    my ( $max_length, $string ) = @_;

    if(length($string) > $max_length)
    {
        confess "ERROR: String to pack '$string' exceed possible length limit ".length($string)."/'$max_length'\n";
    }

    pack 'a'.$max_length, $string;
}
