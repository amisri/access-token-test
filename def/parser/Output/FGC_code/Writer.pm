# Name:     Output/FGC_code/Writer.pm
# Purpose:  Provides functions to generate FGC code files
# Author:   Stephen Page

package Output::FGC_code::Writer;

use Exporter;
use File::Basename;
use File::Path qw(mkpath);
use Output::FGC_code::FGC2::Writer;
use Output::FGC_code::FGC3::Writer;
use Output::FGC_code::FGCLite::Writer;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

# Construct an array of systems for this platform

sub get_platform_systems($$)
{
    my ( $superhash, $platform_name ) = @_;

    # Return new sorted array with systems filtered by passed $platfrom_name

    my @sorted_systems = sort { $a->{type} cmp $b->{type} }
                         grep {
                                !defined($_->{platform}) ||
                                index($_->{platform}, $platform_name) != -1         # permit a system with multiple platforms (RPMBD)
                              } values(%{$superhash->{systems}});

    return(
              { type => "UKNWN", label => "Unknown system", log_menus => [] },
              @sorted_systems
          );
}

# Construct an array of components for this platform

sub get_platform_components($$)
{
    my ( $superhash, $platform_name) = @_;

    # Construct a sorted array of components for this platform

    my @platform_sorted_components = sort 
                                     { 
                                        $a->{barcode} cmp $b->{barcode} 
                                     }

                                     # Filter components for dallas id and platform if defined

                                     grep
                                     {
                                        defined($_->{dallas_id}) and $_->{dallas_id} =~ /yes/i 
                                        and 
                                        (not defined($_->{platform}) or $_->{platform} =~ $platform_name)
                                     }
                                     
                                     values %{$superhash->{components}};

    # Return a list of components for this platform prefixed by the unknown component

    return(
              { barcode => 'UNKNOWN___', label => 'Unknown component', bus => '', temp_prop => '', barcode_prop => 0 },
              @platform_sorted_components
          );
}

# Construct a sorted array of DIM types for this platform

sub get_platform_dim_types($$)
{
    my ( $superhash, $platform_name ) = @_;

    # Construct a sorted array of DIMs for this platform

    my @platform_sorted_dims =  sort { $a->{dim_type_idx} <=> $b->{dim_type_idx} }

                                     # If platform is defined then filter it for this platform

                                     grep
                                     {
                                       !defined($_->{platform}) || $_->{platform} eq $platform_name
                                     } values %{$superhash->{dim_types}};

    # Add the DIM type index for the platform to each DIM

    my $i = 0;
    for my $dim (@platform_sorted_dims)
    {
        $dim->{platform_dim_type_idx} = $i++;
    }

    return @platform_sorted_dims;
}

# Begin functions

sub write($$)
{
    my ($proj_path, $superhash) = @_;

    print "********** Beginning generation of FGC code files **********\n\n";

    Output::FGC_code::FGC2::Writer::write($proj_path, $superhash);

    Output::FGC_code::FGC3::Writer::write($proj_path, $superhash);

    Output::FGC_code::FGCLite::Writer::write($proj_path, $superhash);

    print "********** Generation of FGC code files completed successfully **********\n\n";
}

# EOF
