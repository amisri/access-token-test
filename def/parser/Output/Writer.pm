#!/usr/bin/perl -w
#
# Name:     Output/Writer.pm
# Purpose:  Provides functions to generate output files
# Author:   Stephen Page

package Output::Writer;

use Exporter;
use Output::C::Writer;
use Output::Config::Writer;
use Output::Doc::Writer;
use Output::FGC_code::Writer;
use Output::Perl::Writer;
use Output::Python::Writer;
use Output::VHDL::Writer;
use Output::cclibs::Writer;
use Output::EPICS::Writer;
use Output::Tango::Writer;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    # Create HTML documentation

    Output::Doc::Writer::write($projpath, $superhash);

    # Create C

    Output::C::Writer::write($projpath, $superhash);

    # Create Perl

    Output::Perl::Writer::write($projpath, $superhash);

    # Create Python

    Output::Python::Writer::write($projpath, $superhash);

    # Create config files

    Output::Config::Writer::write($projpath, $superhash);

    # Create FGC code files

    Output::FGC_code::Writer::write($projpath, $superhash);

    # Create VHDL files

    Output::VHDL::Writer::write($projpath, $superhash);

    # Create cclibs files

    Output::cclibs::Writer::write($projpath, $superhash);

    # Create EPICS files

    Output::EPICS::Writer::write($projpath, $superhash);

    # Create Tango files

    Output::Tango::Writer::write($projpath, $superhash);

}

# EOF
