#!/usr/bin/perl -w
#
# Name:     Output/Doc/Flags.pm
# Purpose:  Provides functions relating to writing the FGC flags documentation
# Author:   Stephen Page

package Output::Doc::Flags;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $flagshash) = @_;

    my $col1width = "25%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Property Flags");
    Output::Doc::General::writetabletitle($docinfo, $outputfile, $col1width, "Property Flag");
    print $outputfile   "</TABLE>\n",
                        "\n";

    for my $flagname (sort keys(%$flagshash))
    {
        print $outputfile   "<!-- $flagname -->\n",
                            "\n",
                            "<A name=\"$flagname\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        Output::Doc::General::writerowtitle($docinfo, $outputfile, $col1width, $flagshash->{$flagname}->{name});
        Output::Doc::General::writerowdoc($docinfo, $outputfile, $flagshash->{$flagname}->{doc});

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
