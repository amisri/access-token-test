#!/usr/bin/perl -w
#
# Name:     Output/Doc/Setifs.pm
# Purpose:  Provides functions relating to writing the FGC 'set if' functions' documentation
# Author:   Stephen Page

package Output::Doc::Setifs;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $setifshash) = @_;

    my $col1width = "25%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Setif Functions");
    Output::Doc::General::writepageindex($docinfo, $outputfile, "Setif Function", "Setif", $setifshash, "name", "title");

    for my $setifname (sort keys(%$setifshash))
    {
        print $outputfile   "<!-- $setifname -->\n",
                            "\n",
                            "<A name=\"$setifname\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        # Write index links

        Output::Doc::General::writeindexlink($docinfo, $outputfile, "#top", "INDEX", "START");
        print $outputfile   "<TH class=\"centreindex\">Setif$setifname</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, $docinfo->{paths}->{property_index}, "PROPERTIES", "END");

        # Write documentation

        print $outputfile   "<TR>\n",
                            "<TD colspan=\"3\">\n",
                            "$setifshash->{$setifname}->{doc}\n",
                            "</TD>\n",
                            "</TR>\n",
                            "\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
