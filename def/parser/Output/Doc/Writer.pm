#!/usr/bin/perl -w
#
# Name:     Output/Doc/Writer.pm
# Purpose:  Provides functions to generate documentation
# Author:   Stephen Page

package Output::Doc::Writer;

use Exporter;
use File::Path qw(mkpath);
use Output::Doc::Classes::Writer;
use Output::Doc::Codes;
use Output::Doc::Components;
use Output::Doc::Dim_types;
use Output::Doc::Errors;
use Output::Doc::Flags;
use Output::Doc::Getoptions;
use Output::Doc::GetSet;
use Output::Doc::Properties;
use Output::Doc::Runlog;
use Output::Doc::Setifs;
use Output::Doc::Project_sitemap;
use Output::Doc::Symlists;
use Output::Doc::Systems;
use Output::Doc::Platforms::Writer;
use Output::Doc::Types;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

# Create a hash to contain definitions required for HTML documentation

my %docinfo = (
                icon_path               => "images/logo.gif",
                style_sheet             => "style.css",

                # Doc links and file names

                top_path                => "../..",
                cern_link               => "http://cern.ch",
                about_link              => "static/Project/about.htm",
                sitemap_link            => "SiteMap.htm",
              );

$docinfo{paths} = {
                    class                   => "Class",
                    class_index             => "ClassIndex.htm",
                    codes                   => "Codes.htm",
                    components              => "Components.htm",
                    dim_type                => "DimType",
                    dim_type_index          => "DimTypeIndex.htm",
                    errors                  => "Errors.htm",
                    flags                   => "Flags.htm",
                    getfuncs                => "GetFuncs.htm",
                    getoptions              => "GetOptions.htm",
                    hardware                => "Hw",
                    hardware_index          => "HwIndex",
                    incfiles                => "IncFiles.htm",
                    inputs                  => "Inputs.htm",
                    menu                    => "menu.txt",
                    pcs_flags               => "PcsFlags.htm",
                    platform                => "Platform",
                    platform_index          => "PlatformIndex.htm",
                    property                => "Property",
                    property_index          => "PropertyIndex.htm",
                    run_codes               => "RunCodes.htm",
                    runlog                  => "Runlog.htm",
                    setfuncs                => "SetFuncs.htm",
                    setiffuncs              => "SetifFuncs.htm",
                    sitemap                 => $docinfo{sitemap_link},
                    sitemenu                => "SiteMenu.htm",
                    symlists                => "SymList",
                    system                  => "System",
                    system_index            => "SystemIndex.htm",
                    state                   => "States",
                    trans                   => "Trans",
                    types                   => "Types.htm",
                  };

$docinfo{links} = {
                    ERR     => $docinfo{paths}->{errors},
                    FLAG    => $docinfo{paths}->{flags},
                    GET     => $docinfo{paths}->{getfuncs},
                    GOPT    => $docinfo{paths}->{getoptions},
                    SET     => $docinfo{paths}->{setfuncs},
                    SETIF   => $docinfo{paths}->{setiffuncs},
                    TYPE    => $docinfo{paths}->{types},
                  };

$docinfo{splinks} = {
                        PROP    => $docinfo{paths}->{property},
                        STAT    => $docinfo{paths}->{state},
                        SYMLSTC => $docinfo{paths}->{symlists},
                        TRAN    => $docinfo{paths}->{trans},
                    };

return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of HTML documentation **********\n\n";

    # Set documentation version

    $docinfo{docversion} = $superhash->{creationdate};

    # Create a hash to contain write functions for each documentation element

    my %writefuncs =    (
                            classes             => \&Output::Doc::Classes::write,
                            class_index         => \&Output::Doc::Classes::writeindex,
                            codes               => \&Output::Doc::Codes::write,
                            components          => \&Output::Doc::Components::write,
                            dim_type            => \&Output::Doc::Dim_types::write,
                            dim_type_index      => \&Output::Doc::Dim_types::writeindex,
                            errors              => \&Output::Doc::Errors::write,
                            flags               => \&Output::Doc::Flags::write,
                            getfuncs            => \&Output::Doc::GetSet::write,
                            getoptions          => \&Output::Doc::Getoptions::write,
                            properties          => \&Output::Doc::Properties::write,
                            property_index      => \&Output::Doc::Properties::writeindex,
                            runlog              => \&Output::Doc::Runlog::write,
                            setfuncs            => \&Output::Doc::GetSet::write,
                            setiffuncs          => \&Output::Doc::Setifs::write,
                            sitemap             => \&Output::Doc::Project_sitemap::write,
                            symlists            => \&Output::Doc::Symlists::write,
                            systems             => \&Output::Doc::Systems::write,
                            systems_index       => \&Output::Doc::Systems::writeindex,
                            types               => \&Output::Doc::Types::write,
                            xilinx_progs        => \&Output::Doc::Platforms::Xilinx_progs::write,
                        );

    $docinfo{doc_path} = "$projpath/website/gendoc/def";

    # Remove old HTML documentation

    unlink <$docinfo{doc_path}/*>;

    # Make documentation directory if it does not exist

    if(!-d $docinfo{doc_path})
    {
        mkpath($docinfo{doc_path})
            or confess "$docinfo{doc_path}(9999): ERROR Failed to make directory: $!\n";
    }

    # Generate the general HTML documentation

    for my $doc (qw(codes components errors flags getfuncs getoptions runlog setfuncs setiffuncs sitemap types))
    {
        my $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{$doc}";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";

        $writefuncs{$doc}(\%docinfo, \*FILE, $outputfilename, $superhash->{$doc}, $superhash);

        close(FILE);
    }

    # Write documentation for each symlist

    for my $symlistname (keys(%{$superhash->{symlists}}))
    {
        my $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{symlists}$symlistname.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";

        my $symlist = $superhash->{symlists}->{$symlistname};

        $writefuncs{symlists}(\%docinfo, \*FILE, $outputfilename, $symlist, $superhash);

        close(FILE);
    }

    # Write DIM types

    my $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{dim_type_index}";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
    $writefuncs{dim_type_index}(\%docinfo, \*FILE, "$outputfilename", $superhash);
    close(FILE);

    # Generate documentation for DIM types

    for my $dim_type_name (keys(%{$superhash->{dim_types}}))
    {
        $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{dim_type}$dim_type_name.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        $writefuncs{dim_type}(\%docinfo, \*FILE, $outputfilename, $superhash->{dim_types}->{$dim_type_name});
        close(FILE);
    }

    # Write index of systems

    $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{system_index}";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
    $writefuncs{systems_index}(\%docinfo, \*FILE, "$outputfilename", $superhash->{systems});
    close(FILE);

    for my $system (values(%{$superhash->{systems}}))
    {
        $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{system}$system->{type}.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
        $writefuncs{systems}(\%docinfo, \*FILE, "$outputfilename", $system, $superhash);
        close(FILE);
    }
    
    # Write index of properties

    $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{property_index}";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    $writefuncs{property_index}(\%docinfo, \*FILE, "$outputfilename", $superhash->{properties});

    close(FILE);

    # Generate the property documentation

    for my $property (values(%{$superhash->{properties}}))
    {
        # Match only top-level properties

        if(!defined($property->{parent}))
        {
            my $outputfilename = "$docinfo{doc_path}/$docinfo{paths}->{property}$property->{name}.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";

            $writefuncs{properties}(\%docinfo, \*FILE, $outputfilename, $property, $superhash);

            close(FILE);
        }
    }

    # Write platform documentation

    Output::Doc::Platforms::Writer::write($projpath, $superhash, \%docinfo);

    # Write class documentation

    Output::Doc::Classes::Writer::write($projpath, $superhash, \%docinfo);

    print "********** Generation of HTML documentation completed successfully **********\n\n";
}

# Function to provide other packages with access to %docinfo

sub get_docinfo()
{
    return(\%docinfo);
}

# EOF
