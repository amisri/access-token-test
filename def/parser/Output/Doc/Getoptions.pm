#!/usr/bin/perl -w
#
# Name:     Output/Doc/Getoptions.pm
# Purpose:  Provides functions relating to writing the FGC 'get options' documentation
# Author:   Stephen Page

package Output::Doc::Getoptions;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $getoptionshash) = @_;

    my $col1width = "25%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Get Options");
    Output::Doc::General::writepageindex($docinfo, $outputfile, "Get option", "", $getoptionshash, "symbol", "title");

    for my $getoptionsymbol (sort keys(%$getoptionshash))
    {
        print $outputfile   "<!-- $getoptionsymbol -->\n",
                            "\n",
                            "<A name=\"$getoptionsymbol\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        # Write index links

        Output::Doc::General::writeindexlink($docinfo, $outputfile, "#top", "INDEX", "START");
        print $outputfile   "<TH class=\"centreindex\">$getoptionsymbol</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, $docinfo->{paths}->{property_index}, "PROPERTIES", "END");

        # Write documentation

        print $outputfile   "<TR>\n",
                            "<TD colspan=\"3\">\n",
                            "$getoptionshash->{$getoptionsymbol}->{doc}\n",
                            "</TD>\n",
                            "</TR>\n",
                            "\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
