#!/usr/bin/perl -w
#
# Name:     Output/Doc/Project_sitemap.pm
# Purpose:  Provides functions relating to writing a project sitemap
# Author:   Stephen Page

package Output::Doc::Project_sitemap;

use strict;
use Exporter;

use Output::Doc::General;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub recursivewrite($$$$);

sub recursivewrite($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $node) = @_;

    print $outputfile   "<LI type=\"circle\"><SMALL>$node->{name}</SMALL>\n",
                        "<UL>\n";

    foreach my $link (@{$node->{links}})
    {
        # Replace any ampersands in url by "&amp;"

        my $url =  $link->{url};
        $url    =~ s/&/&amp;/g;

        if($url =~ /^http/)
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
        }
        else
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
        }
    }

    foreach my $child (@{$node->{children}})
    {
        recursivewrite($docinfo, $outputfile, $outputfilename, $child);
    }

    print $outputfile   "</UL>\n",
                        "</LI>\n";
                        "\n";
}

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $siteareas, $superhash) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Project Site Map");

    # Write header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH class=\"sitemap\">Reference</TH>\n",
                        "<TH class=\"sitemap\">Documentation</TH>\n",
                        "</TR>\n",
                        "\n",
                        "<TR>\n";

    # Write reference area

    print $outputfile   "<TD VALIGN=\"top\">\n",
                        "<UL>\n",
                        "\n";

    if(defined($superhash->{platforms}))
    {
        print $outputfile   "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{platform_index}\">Platforms</A></SMALL>\n",
                            "<UL>\n";

        foreach my $platform (sort { $a->{id} <=> $b->{id} } values(%{$superhash->{platforms}}))
        {
            print   $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$platform->{name}.htm\">";
            printf  $outputfile ("%d %s%s\n", $platform->{id}, "\U$platform->{name}", "</A></SMALL></LI>");
        }

        print $outputfile   "</UL>\n",
                            "</LI>\n",
    }

    if(defined($superhash->{classes}))
    {
        print $outputfile   "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{class_index}\">Classes</A></SMALL>\n",
                            "<UL>\n";

        foreach my $class (sort { $a->{id} <=> $b->{id} } values(%{$superhash->{classes}}))
        {
            print   $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">";
            printf  $outputfile ("%d $class->{name}</A></SMALL></LI>\n", $class->{id});
        }

        print $outputfile   "</UL>\n",
                            "</LI>\n";
    }

    # Write property list

    print $outputfile   "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{property_index}\">Properties</A></SMALL>\n",
                        "<UL>\n";

    if(defined($superhash->{getfuncs}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{getfuncs}\">Get functions</A></SMALL></LI>\n";
    }

    if(defined($superhash->{getoptions}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{getoptions}\">Get options</A></SMALL></LI>\n";
    }

    if(defined($superhash->{setfuncs}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{setfuncs}\">Set functions</A></SMALL></LI>\n";
    }

    if(defined($superhash->{setiffuncs}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{setiffuncs}\">Setif functions</A></SMALL></LI>\n";
    }

    if(defined($superhash->{flags}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{flags}\">Property flags</A></SMALL></LI>\n";
    }

    if(defined($superhash->{types}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{types}\">Types</A></SMALL></LI>\n";
    }

    if(defined($superhash->{errors}))
    {
        print $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{errors}\">Errors</A></SMALL></LI>\n";
    }

    print $outputfile   "</UL>\n",
                        "</LI>\n";

    if(defined($superhash->{runlog}))
    {
        print $outputfile "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{runlog}\">Run log entries</A></SMALL></LI>\n";
    }

    if(defined($superhash->{codes}))
    {
        print $outputfile "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{codes}\">Codes</A></SMALL></LI>\n";
    }

    if(defined($superhash->{components}))
    {
        print $outputfile "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{components}\">Component types</A></SMALL></LI>\n";
    }

    if(defined($superhash->{dim_types}))
    {
        print $outputfile "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{dim_type_index}\">DIM types</A></SMALL></LI>\n";
    }

    if(defined($superhash->{systems}))
    {
        print $outputfile "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{system_index}\">Systems</A></SMALL>\n";
    }

    print $outputfile   "</UL>\n",
                        "</TD>\n",
                        "\n";

    # Write documentation area

    print $outputfile   "<TD VALIGN=\"top\">\n",
                        "<UL>\n",
                        "\n";

    foreach my $link (@{$siteareas->{links}})
    {
        # Replace any ampersands in url by "&amp;"

        my $url =  $link->{url};
        $url    =~ s/&/&amp;/g;

        if($url =~ /^http/)
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
        }
        else
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
        }
    }

    foreach my $child (@{$siteareas->{children}})
    {
        print $outputfile   "<!-- $child->{name} -->\n",
                            "\n";

        recursivewrite($docinfo, $outputfile, $outputfilename, $child);
        print $outputfile   "\n";
    }

    print $outputfile   "</UL>\n",
                        "</TD>\n";

    print $outputfile   "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
