#!/usr/bin/perl -w
#
# Name:     Output/Doc/Properties.pm
# Purpose:  Provides functions relating to writing the FGC Properties' documentation
# Author:   Stephen Page

package Output::Doc::Properties;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writeindex write);

return(1);

# Begin functions

# Function to recursively generate the property index table

sub writeindextableentries($$$$);

sub writeindextableentries($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $property) = @_;

    print $outputfile "<TR>\n<TD align=\"left\"><SMALL><A href=\"#$property->{name}\">$property->{name}</A></SMALL></TD>\n";
    Output::Doc::General::writerowdoc($docinfo, $outputfile, $property->{title});

    # Recurse for each of the property's children

    if(defined($property->{children}))
    {
        for my $child (@{$property->{children}})
        {
            print $outputfile   "<!-- $child->{name} -->\n",
                                "\n";

            writeindextableentries($docinfo, $outputfile, $outputfilename, $child);
        }
    }
}

# Function to recursively generate the property details table

sub writepropertydetails($$$$$);

sub writepropertydetails($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $property, $superhash) = @_;

    my $col1width = "70%";
    my $col3width = "30%";

    # Write property header

    print $outputfile   "<!-- $property->{name} -->\n",
                        "\n",
                        "<A name=\"$property->{name}\"></A>\n",
                        "<TABLE class=\"main\">\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "#TOP", "INDEX", "START");
    print $outputfile   "<TH class=\"centreindex\">$property->{name}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{property_index}", "PROPERTIES", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    print $outputfile   "<TABLE class=\"main\">\n";

    # Write property group

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">\n", $col3width, "baseline", "Property group:");
    print $outputfile   "$property->{group}->{name}";
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD><TD></TD></TR>\n");

    # Write classes

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">\n", $col3width, "baseline", "Classes:", $docinfo->{paths}->{class_index});

    my $firstitem = 1;
    for my $class (sort keys(%{$property->{classes}}))
    {
        if(!$firstitem)
        {
            print $outputfile   ", ";
        }

        print $outputfile   "<A href=\"$docinfo->{paths}->{class}$class.htm\">$class</A>";
        $firstitem = 0;
    }

    # If $firstitem is still 1, then no flags are present

    if($firstitem)
    {
        print $outputfile   "None";
    }

    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write type

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Type:", $docinfo->{paths}->{types});
    print $outputfile   "<A href=\"$docinfo->{paths}->{types}#$property->{type}->{name}\">$property->{type}->{name}</A>\n";
    if(defined($property->{specific_types}))
    {
        print $outputfile   "<BR>",
                            "\n",
                            "Class specific types:",
                            "\n",
                            "<BR>",;
        for my $class (sort keys(%{$property->{classes}}))
        {
            # get platform type
            my $platform = $superhash->{classes}->{$class}->{platform}->{name};
            if(defined($property->{specific_types}->{$class}))
            {
                print $outputfile   "<A href=\"$docinfo->{paths}->{class}$class.htm\">$class</A>: ",
                                    "<A href=\"$docinfo->{paths}->{types}#$property->{specific_types}->{$class}->{name}\">$property->{specific_types}->{$class}->{name}</A>\n",
                                    "<BR>",
                                    "\n";
            }
            elsif(defined($property->{specific_types}->{$platform}))
            {
                print $outputfile   "<A href=\"$docinfo->{paths}->{class}$class.htm\">$class</A>: ",
                                    "<A href=\"$docinfo->{paths}->{types}#$property->{specific_types}->{$platform}->{name}\">$property->{specific_types}->{$platform}->{name}</A>\n",
                                    "<BR>",
                                    "\n";
            }
        }
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");

    # Write flags

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR valign=\"top\"><TD>", $col3width, "baseline", "Flags:", $docinfo->{paths}->{flags});

    $firstitem = 1;
    for my $flag (sort {$a->{name} cmp $b->{name}} values(%{$property->{flags}}))
    {
        if(!$firstitem)
        {
            print $outputfile   ", ";
        }

        print $outputfile   "<A href=\"$docinfo->{paths}->{flags}#$flag->{name}\">$flag->{name}</A>";
        $firstitem = 0;
    }

    # If $firstitem is still 1, then no flags are present

    if($firstitem)
    {
        print $outputfile   "None";
    }

    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write property range

    if(defined($property->{limits}))
    {
        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Range:");
        print $outputfile "Min&nbsp;=&nbsp;$property->{min}, Max&nbsp;=&nbsp;$property->{max}";
    }
    elsif(defined($property->{symlist}))
    {
        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Range:");

        # Range is a symlist, write links to constants

        for my $const (@{$property->{symlist}->{consts}})
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{symlists}$property->{symlist}->{name}.htm#$property->{symlist}->{name}.$const->{symbol}\">$const->{symbol}</A>\n",
                                "\n",
                                "<BR>\n",
                                "\n";
        }
    }
    elsif(defined($property->{class_symlist}))
    {
        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Ranges by class:");

        # Range is a class symlist, write links to class symlists

        for my $class (sort { $a->{id} cmp $b->{id} } values(%{$property->{classes}}))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{class}$class->{id}_$docinfo->{paths}->{symlists}_$property->{class_symlist}.htm\">$class->{name}</A>\n",
                                "\n",
                                "<BR>\n",
                                "\n";
        }
    }
    else
    {
        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Range:");
        print $outputfile   "None";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

    # Write get function

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">\n", $col3width, "baseline", "Get function:", $docinfo->{paths}->{getfuncs});
    if(defined($property->{get}))
    {
        print $outputfile   "<A href=\"$docinfo->{paths}->{getfuncs}#$property->{get}->{name}\">Get$property->{get}->{name}</A>";
    }
    else
    {
        print $outputfile   "None";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write size

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Size:&nbsp;");
    
    # Range is a class symlist, write links to class symlists

    for my $key (grep { /^maxels/ } keys(%{$property}))
    {
        my $level = ($key =~ /maxels[_]{1}(.*)/ );
        
        if ($1)
        {
             print $outputfile "<b>" . uc($1) . "</b>" . "&nbsp;$property->{$key}\n<BR>";
        }
        else
        {
             # It is either a parent or maxels
             
             if ($property->{maxels} =~ /^N_ELS/)
             {
                  # Parent property
                  
                  print $outputfile scalar(@{$property->{children}}) . "\n<BR>";
             }
             else
             {
                 # Attribute is "maxels" and it has a value different than 0
                 
                 if($property->{maxels})
                 {
                     print $outputfile $property->{maxels} . "\n<BR>"; 
                 } 
             }
             
        } 
    }
    
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

    # Write set function

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">\n", $col3width, "baseline", "Set function:", $docinfo->{paths}->{setfuncs});
    if(defined($property->{set}))
    {
        print $outputfile   "<A href=\"$docinfo->{paths}->{setfuncs}#$property->{set}->{name}\">Set$property->{set}->{name}</A>";
        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

        # Write setif function

        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD width=\"50%\">\n", $col3width, "baseline", "Set condition:", $docinfo->{paths}->{setiffuncs});
        if(defined($property->{setif}))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{setiffuncs}#$property->{setif}->{name}\">Setif$property->{setif}->{name}</A>";
        }
        else
        {
            print $outputfile   "None";
        }

        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");
    }
    else
    {
        print $outputfile   "None";
        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>\n<TD width=\"50%\">&nbsp;</TD></TR>\n");
    }

    # Write documentation

    print $outputfile   "<TR>\n",
                        "<TD colspan=\"2\">\n",
                        "$property->{doc}\n",
                        "</TD>\n",
                        "</TR>\n",
            "\n",
                        "</TABLE>\n",
                        "\n";

    # Recurse for each of the property's children

    if(defined($property->{children}))
    {
        for my $child (@{$property->{children}})
        {
            writepropertydetails($docinfo, $outputfile, $outputfilename, $child, $superhash);
        }
    }
}

# Function to generate the page: PropertyIndex.htm

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $propertieshash) = @_;

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Properties Index");
    Output::Doc::General::writetabletitle($docinfo, $outputfile, "40%", "Top-Level&nbsp;Property");

    for my $property (sort {$a->{name} cmp $b->{name}} values(%$propertieshash))
    {
        # Match only top-level properties

        if(!defined($property->{parent}))
        {
            print $outputfile   "<TR>\n",
                                "<TD><SMALL><A href=\"$docinfo->{paths}->{property}$property->{name}.htm\">$property->{name}</A></SMALL></TD>\n";

            Output::Doc::General::writerowdoc($docinfo, $outputfile, $property->{title});
        }
    }
    print $outputfile "</TABLE>\n";
    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# Function to generate all of the pages: Property{TLP}.htm

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $property, $superhash) = @_;

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$property->{name} Properties");

    # Use recursive function writeindextableentries() to generate index table

    Output::Doc::General::writetabletitle($docinfo, $outputfile, "40%", "$property->{name}&nbsp;property");
    writeindextableentries($docinfo, $outputfile, $outputfilename, $property);
    print $outputfile "</TABLE>\n";

    # Use recursive function writepropertydetails() to generate details tables

    writepropertydetails($docinfo, $outputfile, $outputfilename, $property, $superhash);

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
