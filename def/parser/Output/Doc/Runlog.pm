#!/usr/bin/perl -w
#
# Name:     Output/Doc/Runlog.pm
# Purpose:  Provides functions relating to writing the documentation for run log entries
# Author:   Stephen Page

package Output::Doc::Runlog;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $runlog_entries, $superhash) = @_;

    my $col3width = "30%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Run Log Entries");

    # Write table of run log entries

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH>ID</TH>\n",
                        "<TH>Label/[Constant]</TH>\n",
                        "<TH>Words</TH>\n",
                        "<TH>Documentation</TH>\n",
                        "</TR>\n",
                        "\n";

    # Write each run log entry

    my $number = 0;
    for my $entry (@$runlog_entries)
    {
        # Write entry

        print   $outputfile     "<!-- Runlog entry $entry->{id} -->\n",
                                "\n",
                                "<TR>\n";
        printf  $outputfile    ("<TD align=\"center\" valign=\"top\"><SMALL>0x%02X</SMALL></TD>\n", $number++);
        print   $outputfile     "<TD align=\"left\"   valign=\"top\"><SMALL><B>$entry->{label}</B><BR><SMALL>[FGC_$entry->{id}]</SMALL></SMALL></TD>\n",
                                "<TD align=\"center\" valign=\"top\"><SMALL>$entry->{length}</SMALL></TD>\n",
                                "<TD align=\"left\"   valign=\"top\"><SMALL>$entry->{doc}</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
    }
    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
