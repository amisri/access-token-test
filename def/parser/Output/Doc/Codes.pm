#!/usr/bin/perl -w
#
# Name:     Output/Doc/Codes.pm
# Purpose:  Provides functions relating to writing the FGC code documentation
# Author:   Stephen Page

package Output::Doc::Codes;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $codes, $superhash) = @_;

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Codes");

    # Write table of codes

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH>ID</TH>\n",
                        "<TH>Name</TH>\n",
                        "<TH>Size</TH>\n",
                        "<TH>Documentation</TH>\n",
                        "</TR>\n",
                        "\n";

    for my $code (sort { $a->{id} <=> $b->{id} } values(%$codes))
    {
        print $outputfile   "<!-- $code->{full_name} -->\n",
                            "\n",
                            "<TR>\n",
                            "<TD align=\"center\" valign=\"middle\"><SMALL>$code->{id}</SMALL></TD>\n",
                            "<TD align=\"center\" valign=\"middle\"><SMALL>$code->{code_dir_name}</SMALL></TD>\n",
                            "<TD align=\"center\" valign=\"middle\"><SMALL>$code->{size_kb}KB</SMALL></TD>\n",
                            "<TD align=\"left\"   valign=\"top\"><SMALL>$code->{doc}</SMALL></TD>\n",
                            "</TR>\n",
                            "\n";
    }
    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
