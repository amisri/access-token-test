#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Xilinx_progs.pm
# Purpose:  Provides functions relating to writing the documentation for each Xilinx's programs
# Author:   Stephen Page

package Output::Doc::Platforms::Xilinx_progs;

use Exporter;
use File::Basename;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $xilinx) = @_;

    my $col3width = "30%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$xilinx->{hardware}->{label} Xilinx $xilinx->{ic_number}");

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware}$xilinx->{hardware}->{lhc_id}.htm", $xilinx->{hardware}->{label}, "START");
    print $outputfile   "<TH>Xilinx $xilinx->{ic_number}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware_index}$xilinx->{hardware}->{platform}->{name}.htm", "HARDWARE", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    # Write type and purpose

    print $outputfile   "<TR>\n",
                        "<TD>Type: $xilinx->{type}</TD>",
                        "<TD>Purpose: $xilinx->{purpose}</TD>",
                        "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";

    # Write program entries

    foreach my $program (@{$xilinx->{programs}})
    {
        print $outputfile   "<!-- Xilinx program $program->{id} -->\n",
                            "\n",
                            "<A name=\"$program->{id}\"></A>\n",
                            "<TABLE class=\"main\">",
                            "\n",
                            "<TR><TH>Program $program->{id}: $program->{title}</TH></TR>\n",
                            "\n";

        # Write project path

        my $project_dir =  $xilinx->{logiware};
        $project_dir    =~ s/(.*)\\.*/$1/;

        #my $project     =  "pld\\$xilinx->{hardware}->{label}\\$xilinx->{ic_number}\\$program->{id}\\".$xilinx->{programs}->[0]->{revision}."\\$project_dir";
        my $project     =  "pld\\$xilinx->{hardware}->{label}\\$xilinx->{ic_number}\\$program->{id}\\".$program->{revision}."\\$project_dir";

        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "baseline", "Project path: $project");
        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");

        # Write revision descriptions

        print $outputfile   "<TR>\n",
                            "<TD>\n",
                            "<TABLE class=\"field\">\n",
                            "\n";

        my $revision_number = 1;
        foreach my $revision (@{$program->{revisions}})
        {
            print $outputfile   "<TR>\n",
                                "<TD align=\"left\" valign=\"top\" style=\"width: 15%\">Revision $revision_number: </TD>\n",
                                "<TD valign=\"top\" style=\"width: 85%\">$revision->{doc}<BR><BR></TD>\n",
                                "</TR>\n",
                                "\n";

            $revision_number++;
        }

        print $outputfile   "</TABLE>\n",
                            "\n",
                            "</TD>\n",
                            "</TR>\n",
                            "\n";

        # Write description

        print $outputfile   "<TR><TD>$program->{description}</TD></TR>\n",
                            "\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
