#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Hw.pm
# Purpose:  Provides functions relating to writing the top-level documentation for each piece of hardware
# Author:   Stephen Page

package Output::Doc::Platforms::Hw;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writeindex write);

my @sorted_hardware;

return(1);


# Begin functions

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $hardware) = @_;

    @sorted_hardware = sort {$a->{lhc_id} cmp $b->{lhc_id}} values(%{$hardware});

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, uc($sorted_hardware[0]->{platform}->{name})." Hardware Index");
    Output::Doc::General::writetabletitle($docinfo, $outputfile, "40%", "Hardware", "Title");

    foreach my $hardware_item (@sorted_hardware)
    {
        print $outputfile   "<TR>\n",
                            "<TD><SMALL><A href=\"$docinfo->{paths}->{hardware}$hardware_item->{lhc_id}.htm\">$hardware_item->{lhc_id} - $hardware_item->{label}</A></SMALL></TD>\n";
        Output::Doc::General::writerowdoc($docinfo, $outputfile, $hardware_item->{title});
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $hardware) = @_;

    my $col3width   = "30%";
    my $navarrow    = " vspace=\"1\" width=\"15\" height=\"15\" border=\"0\"";

    # Find next and previous hardware links

    my $next_hardware;
    my $previous_hardware;
    my $i;
    for($i = 0 ; $i < @sorted_hardware ; $i++)
    {
        if($sorted_hardware[$i] == $hardware)
        {
            # Set next hardware
            # Note that this may be undefined

            $next_hardware = $sorted_hardware[$i + 1];

            if($i > 0)
            {
                $previous_hardware = $sorted_hardware[$i - 1];
            }
        }
    }

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$hardware->{label}");

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "../../download/pdf/$hardware->{label}.pdf", "DIAGRAM", "START");

    print $outputfile   "<TH>$hardware->{title}</TH>\n";
    print $outputfile   "<TH class=\"sideindex\">\n";

    # Write navigation arrows

    if(defined($previous_hardware))
    {
        print $outputfile "<A href=\"$docinfo->{paths}->{hardware}$previous_hardware->{lhc_id}.htm\"><img alt=\"Left arrow\" src=\"../../images/blueleft.gif\"$navarrow></A>\n";
    }
    else
    {
        print $outputfile "<img alt=\"Left arrow\" src=\"../../images/greyleft.gif\"$navarrow>\n";
    }

    print $outputfile   "<A href=\"$docinfo->{paths}->{hardware_index}$hardware->{platform}->{name}.htm\"><img alt=\"Up arrow\" src=\"../../images/blueup.gif\"$navarrow hspace=\"6\"></A>\n";

    if(defined($next_hardware))
    {
        print $outputfile "<A href=\"$docinfo->{paths}->{hardware}$next_hardware->{lhc_id}.htm\"><img alt=\"Right arrow\" src=\"../../images/blueright.gif\"$navarrow></A>\n";
    }
    else
    {
        print $outputfile "<img alt=\"Right arrow\" src=\"../../images/greyright.gif\"$navarrow>\n";
    }

    print $outputfile   "</TH>\n",
                        "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";


    # Write hardware information

    print $outputfile   "<TABLE class=\"main\">\n";

    # Write LHC ID and AP-PO ID

    print $outputfile   "<TR>\n",
                        "\n",
                        "\n",
                        "<TD>LHC ID: $hardware->{lhc_id}</TD>",
                        "<TD>AB-PO ID: $hardware->{ab_po_id}</TD>",
                        "\n",
                        "</TR>\n",
                        "\n";

    # Write pin group links

    print $outputfile   "<TR>\n",
                        "<TD colspan=\"2\">\n",
                        "Pin groups: \n";

    my $first = 1;
    foreach my $pin_group (@{$hardware->{tester}->{pins}->{pin_groups}})
    {
        if(!$first)
        {
            print $outputfile   ", ";
        }
        $first = 0;

        print $outputfile   "<A href=\"$docinfo->{paths}->{test_functions}_$hardware->{tester_platform}->{name}_$pin_group->{name}.htm\">$pin_group->{name}</A>";
    }
    print $outputfile   "</TD>\n",
                        "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";

    # Write connector entries if hardware has connectors

    if(scalar(@{$hardware->{connectors}}) > 0)
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH width=\"20%\">Connector ID</TH>\n",
                            "<TH width=\"35%\">Connector Name</TH>\n",
                            "<TH>Connector Type</TH>\n",
                            "</TR>\n",
                            "\n";

        foreach my $connector (@{$hardware->{connectors}})
        {
            print $outputfile   "<!-- Connector $connector->{id} -->\n",
                                "\n",
                                "<TR>\n",
                                "<TD align=\"center\"><A href=\"$docinfo->{paths}->{hardware}$hardware->{lhc_id}_connect.htm#$connector->{id}\"><SMALL>$connector->{id}</SMALL></A></TD>\n",
                                "<TD align=\"center\"><SMALL>$connector->{name}</SMALL></TD>\n",
                                "<TD align=\"center\"><SMALL>$connector->{type}</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write Xilinx entries if there are any

    if(defined($hardware->{xilinxes}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH width=\"20%\">Xilinx IC</TH>\n",
                            "<TH width=\"20%\">Xilinx Type</TH>\n",
                            "<TH width=\"15%\">Usercode</TH>\n",
                            "<TH align=\"left\">Purpose</TH>\n",
                            "</TR>\n",
                            "\n";

        foreach my $xilinx (@{$hardware->{xilinxes}})
        {
            # Ignore Xilinxes on tester

            if(!$xilinx->{tester})
            {
                print $outputfile   "<!-- Xilinx $xilinx->{ic_number} -->\n",
                                    "\n",
                                    "<TR>\n";

                if(defined($xilinx->{programs}))
                {
                    print $outputfile   "<TD align=\"center\"><A href=\"$docinfo->{paths}->{hardware}$hardware->{lhc_id}_xilinx$xilinx->{ic_number}.htm\"><SMALL>Xilinx $xilinx->{ic_number}</SMALL></A></TD>\n";
                }
                else
                {
                    print $outputfile   "<TD align=\"center\"><SMALL>Xilinx $xilinx->{ic_number}</SMALL></TD>\n";
                }

                print $outputfile   "<TD align=\"center\"><SMALL>$xilinx->{type}</SMALL></TD>\n",
                                    "<TD align=\"center\"><SMALL>$xilinx->{usercode}</SMALL></TD>\n",
                                    "<TD align=\"left\"><SMALL>$xilinx->{purpose}</SMALL></TD>\n",
                                    "</TR>\n",
                                    "\n";
            }
        }

        print $outputfile "</TABLE>\n\n";
    }
    elsif(defined($hardware->{subhw}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR>\n",
                            "<TH width=\"20%\">Xilinx IC</TH>\n",
                            "<TH width=\"20%\">Xilinx Type</TH>\n",
                            "<TH width=\"15%\">Usercode</TH>\n",
                            "<TH align=\"left\">Purpose</TH>\n",
                            "</TR>\n";

        foreach my $subhw (@{$hardware->{subhw}})
        {
            foreach my $xilinx (@{$subhw->{xilinxes}})
            {
                # Ignore Xilinxes on tester

                if(!$xilinx->{tester})
                {
                    print $outputfile   "<!-- Xilinx $xilinx->{ic_number} -->\n",
                                        "\n",
                                        "<TR>\n";

                    if(defined($xilinx->{programs}))
                    {
                        print $outputfile   "<TD align=\"center\"><A href=\"$docinfo->{paths}->{hardware}$subhw->{lhc_id}_xilinx$xilinx->{ic_number}.htm\"><SMALL>$subhw->{label} Xilinx $xilinx->{ic_number}</SMALL></A></TD>\n";
                    }
                    else
                    {
                        print $outputfile   "<TD align=\"center\"><SMALL>$subhw->{label} Xilinx $xilinx->{ic_number}</SMALL></TD>\n";
                    }

                    print $outputfile   "<TD align=\"center\"><SMALL>$xilinx->{type}</SMALL></TD>\n",
                                        "<TD align=\"center\"><SMALL>$xilinx->{usercode}</SMALL></TD>\n",
                                        "<TD align=\"left\"><SMALL>$xilinx->{purpose}</SMALL></TD>\n",
                                        "</TR>\n";
                }
            }
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write main documentation

    if(defined($hardware->{doc}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR>\n",
                            "<TD>$hardware->{doc}</TD>",
                            "</TR>\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
