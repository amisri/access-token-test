#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Boot/Menu.pm
# Purpose:  Provides functions relating to writing the boot menu documentation
# Author:   Stephen Page

package Output::Doc::Platforms::Boot::Menu;

use Exporter;
use Output::Doc::General;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write writesummary);

return(1);


# Begin functions

sub writesummary($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $menu_node) = @_;

    # Get a capitalised version of the platform name

    my $capital_platform = uc($menu_node->{platform}->{name});

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$capital_platform Boot Menu");

    # Write table header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}.htm", "PLATFORM", "START");
    print $outputfile   "<TH>Boot Menu Summary</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$menu_node->{id}.htm", "DETAILS", "END");

    print $outputfile   "</TABLE>\n",
                        "\n";

    # Write tree

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";
    Output::Doc::Platforms::Boot::Menu::writenodesummaryline($docinfo, $outputfile, $outputfilename, $menu_node);
    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub writenodesummaryline($$$$);

sub writenodesummaryline($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $menu_node) = @_;

    print $outputfile   "<!-- Node ID $menu_node->{id} -->\n",
                        "\n",
                        "<TR>\n",
                        "<TD bgcolor=\"#FFFF".sprintf("%x", (256 - (51 + (32 * length($menu_node->{id})))))."\"width=\"15%\" align=\"left\"><SMALL>$menu_node->{id}";

    if(defined($menu_node->{function}))
    {
        print $outputfile   "*";
    }

    print $outputfile   "&nbsp;</SMALL></TD>\n",
                        "<TD align=\"left\"><SMALL><B><A href=\"$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$menu_node->{id}.htm\">",
                        "".("&nbsp;" x (8 * length($menu_node->{id})))."$menu_node->{name}",
                        "</A></B></SMALL></TD>\n",
                        "</TR>\n",
                        "\n";

    # Write children

    if(defined($menu_node->{children}))
    {
        foreach my $child (@{$menu_node->{children}})
        {
            Output::Doc::Platforms::Boot::Menu::writenodesummaryline($docinfo, $outputfile, $outputfilename, $child);
        }
    }
}

sub write($$$$;$;$);

sub write($$$$;$;$)
{
    my ($docinfo, $outputfile, $outputfilename, $menu_node, $previous_node, $next_node) = @_;

    my $col3width   = "30%";
    my $navarrow    = " vspace=\"1\" width=\"15\" height=\"15\" border=\"0\"";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, uc($menu_node->{platform}->{name})." Boot Menu $menu_node->{id}");

    print $outputfile   "<!-- Node ID $menu_node->{id} -->\n",
                        "\n",
                        "<A name=\"$menu_node->{id}\"></A>\n",
                        "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}.htm", "PLATFORM", "START");

    # Set table title depending whether node is the root

    my $table_title;

    if(defined($menu_node->{parent})) # Non-root node
    {
        $table_title = "$menu_node->{id}: $menu_node->{name}";
    }
    else # Root node
    {
        $table_title = $menu_node->{name};
    }

    print $outputfile   "<TH>$table_title</TH>\n";

    # Generate navigation buttons

    if(defined($menu_node->{parent}))
    {
        print $outputfile "<TH class=\"sideindex\">\n";

        if(defined($previous_node))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$previous_node->{id}.htm\"><img alt=\"Left arrow\" src=\"../../images/blueleft.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile   "<img alt=\"Left arrow\" src=\"../../images/greyleft.gif\"$navarrow>\n";
        }

        print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$menu_node->{parent}->{id}.htm\"><img alt=\"Up arrow\" src=\"../../images/blueup.gif\"$navarrow hspace=\"6\"></A>\n";

        if(defined($next_node))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$next_node->{id}.htm\"><img alt=\"Right arrow\" src=\"../../images/blueright.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile   "<img alt=\"Right arrow\" src=\"../../images/greyright.gif\"$navarrow>\n";
        }

        print $outputfile   "</TH>\n",
                            "</TR>\n",
                            "\n";
    }
    else
    {
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_summary.htm", "SUMMARY", "END");
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    # Write C name

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "baseline", "C name:");
    print $outputfile $menu_node->{cname};
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write Function

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Function:");
    if(defined($menu_node->{function}))
    {
        print $outputfile $menu_node->{function};
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");

    # Write recurse

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "baseline", "Recursive:");
    if(defined($menu_node->{recurse}) && $menu_node->{recurse} == 1)
    {
        print $outputfile   "Yes";
    }
    else
    {
        print $outputfile   "No";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write fatal

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD width=\"50%\">", $col3width, "baseline", "Fatal:");
    if(defined($menu_node->{fatal}) && $menu_node->{fatal} == 1)
    {
        print $outputfile   "Yes";
    }
    else
    {
        print $outputfile   "No";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");
    print $outputfile   "</TABLE>\n";

    # Write arguments

    if(defined($menu_node->{args}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TD colspan=\"2\">\n",
                            "Arguments: &nbsp; ";

        my $first_flag = 1;
        foreach my $value (@{$menu_node->{args}})
        {
            if($first_flag)
            {
                $first_flag = 0;
            }
            else
            {
                print $outputfile   ", ";
            }

            print $outputfile "$value->{type}";
        }

        print $outputfile   "\n",
                            "</TD>\n",
                            "</TR>\n",
                            "</TABLE>\n",
                            "\n";

        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH width=\"20%\">Argument type</TH>\n",
                            "<TH>Description</TH>\n",
                            "</TR>\n",
                            "\n";

        foreach my $value (@{$menu_node->{args}})
        {
            print $outputfile   "<TR>\n",
                                "<TD align=\"center\">$value->{type}</TD>\n",
                                "<TD>$value->{desc}</TD>\n",
                                "</TR>\n",
                                "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write returned values

    if(defined($menu_node->{return}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TD>\n",
                            "Returned values: &nbsp; ";

        my $first_flag = 1;
        foreach my $value (@{$menu_node->{return}})
        {
            if($first_flag)
            {
                $first_flag = 0;
            }
            else
            {
                print $outputfile ", ";
            }

            print $outputfile "$value->{type}";
        }

        print $outputfile   "\n",
                            "</TD>\n",
                            "</TR>\n",
                            "</TABLE>\n",
                            "\n";

        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH width=\"20%\">Return type</TH>\n",
                            "<TH>Description</TH>\n",
                            "</TR>\n",
                            "\n";

        foreach my $value (@{$menu_node->{return}})
        {
            print $outputfile   "<TR>\n",
                                "<TD align=\"center\">$value->{type}</TD>\n",
                                "<TD>$value->{desc}</TD>\n",
                                "</TR>\n",
                                "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write documentation if defined

    if(defined($menu_node->{doc}))
    {
        # Write documentation

        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR><TD>$menu_node->{doc}</TD></TR>\n",
                            "</TABLE>\n",
                            "\n";
    }

    # Write children

    if(defined($menu_node->{children}) && scalar(@{$menu_node->{children}}))
    {
        # Write children

        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH width=\"20%\">ID</TH>\n",
                            "<TH>Child</TH>\n",
                            "<TH>Fatal</TH>\n",
                            "</TR>\n",
                            "\n";

        my $i;
        for($i = 0 ; $i < scalar(@{$menu_node->{children}}) ; $i++)
        {
            my $child           = $menu_node->{children}[$i];
            my $childfilename   = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$child->{id}.htm";
            local *CHILDFILE;

            open(CHILDFILE, ">$childfilename") || confess "$childfilename(0): ERROR Unable to open version file for writing : $!\n";

            my $previous_node;
            if($i == 0)
            {
                undef($previous_node);
            }
            else
            {
                $previous_node = $menu_node->{children}[$i - 1];
            }

            my $next_node;
            if($i == (scalar(@{$menu_node->{children}}) - 1))  # This is the last node
            {
                undef($next_node);
            }
            else
            {
                $next_node = $menu_node->{children}[$i + 1];
            }

            Output::Doc::Platforms::Boot::Menu::write($docinfo, *CHILDFILE, $childfilename, $child, $previous_node, $next_node);
            close(CHILDFILE);

            print $outputfile   "<!-- Child $child->{id}. $child->{name} -->\n",
                                "\n",
                                "<TR>\n",
                                "<TD align=\"center\"><SMALL>$child->{id}</SMALL></TD>\n",
                                "<TD align=\"left\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$menu_node->{platform}->{name}_boot_menu_$child->{id}.htm\">$child->{name}</A></SMALL></TD>\n",
                                "<TD align=\"center\"><SMALL>";

            if(defined($child->{fatal}) && $child->{fatal} == 1)
            {
                print $outputfile "Yes";
            }
            else
            {
                print $outputfile "No";
            }

            print $outputfile   "</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
