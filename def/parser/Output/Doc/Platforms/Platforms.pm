#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Platforms.pm
# Purpose:  Provides functions relating to writing the documentation for each platform
# Author:   Stephen Page

package Output::Doc::Platforms::Platforms;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writeindex write);

return(1);


# Begin functions

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $platformshash) = @_;

    my $col1width = "40%";

    # Get a sorted list of platforms (by ID)

    my @sorted_platforms = sort {$a->{id} <=> $b->{id}} values(%$platformshash);

    # Print header

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Platform Index");

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH>ID</TH>\n",
                        "<TH>Platform</TH>\n",
                        "<TH align=\"left\">Description</TH>\n",
                        "</TR>\n";

    # Print platforms

    foreach my $platform (@sorted_platforms)
    {
        print $outputfile   "<TR>\n",
                            "<TD align=\"center\"><SMALL>$platform->{id}</SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$platform->{name}.htm\">", "\U$platform->{name}", "</A></SMALL></TD>\n",
                            "<TD align=\"left\"><SMALL>$platform->{title}</SMALL></TD>\n",
                            "</TR>\n";
    }

    print $outputfile "</TABLE>\n\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $platform) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Platform $platform->{id}: \U$platform->{name}");

    # Write header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH class=\"sitemap\">Reference</TH>\n",
                        "<TH class=\"sitemap\">Documentation</TH>\n",
                        "</TR>\n",
                        "\n",
                        "<TR>\n";

    # Write reference area

    print $outputfile   "<TD VALIGN=\"top\">\n";

    if( defined($platform->{boot})      ||
        defined($platform->{classes})   ||
        defined($platform->{hardware})  ||
        defined($platform->{memmaps}))
    {
        print $outputfile   "<UL>\n",
                            "\n";
    }

    if(defined($platform->{memmaps}))
    {
        foreach my $memmap (sort(keys(%{$platform->{memmaps}})))
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$platform->{name}_memmap_$memmap.htm\">$memmap memory map</A></SMALL></LI>\n";
        }
    }

    if(defined($platform->{boot}) && defined($platform->{boot}->{menu}))
    {
        print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$platform->{name}_boot_menu_$platform->{boot}->{menu}->{id}.htm\">Boot menu</A></SMALL></LI>\n";
    }

    if(defined($platform->{classes}))
    {
        print $outputfile   "<LI type=\"circle\"><SMALL>Classes</SMALL>\n",
                            "<UL>\n";

        foreach my $class (sort { $a->{id} <=> $b->{id} } values(%{$platform->{classes}}))
        {
            print   $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">";
            printf  $outputfile ("%d $class->{name}</A></SMALL></LI>\n", $class->{id});
        }

        print $outputfile   "</UL>\n",
                            "</LI>\n",
    }

    if(defined($platform->{hardware}))
    {
        print $outputfile   "<LI type=\"circle\"><SMALL><A href=\"$docinfo->{paths}->{hardware_index}$platform->{name}.htm\">Hardware</A></SMALL>\n",
                            "<UL>\n";

        foreach my $hardware_item (sort { $a->{lhc_id} cmp $b->{lhc_id} } values(%{$platform->{hardware}}))
        {
            print   $outputfile "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{hardware}$hardware_item->{lhc_id}.htm\">$hardware_item->{lhc_id} - $hardware_item->{label}</A></SMALL></LI>\n";
        }

        print $outputfile   "</UL>\n",
                            "</LI>\n";
    }

    if( defined($platform->{boot})      ||
        defined($platform->{classes})   ||
        defined($platform->{dim_types}) ||
        defined($platform->{hardware})  ||
        defined($platform->{memmaps})   ||
        defined($platform->{systems}))
    {
        print $outputfile   "</UL>\n";
    }

    print $outputfile   "</TD>\n",
                        "\n";

    # Write documentation area

    print $outputfile   "<TD VALIGN=\"top\">\n";

    if(defined($platform->{sitemap}))
    {
        print $outputfile   "<UL>\n",
                            "\n";

        foreach my $link (@{$platform->{sitemap}->{links}})
        {
            # Replace any ampersands in url by "&amp;"

            my $url =  $link->{url};
            $url    =~ s/&/&amp;/g;

            if($url =~ /^http/)
            {
                print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
            }
            else
            {
                print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
            }
        }

        foreach my $child (@{$platform->{sitemap}->{children}})
        {
            print $outputfile   "<!-- $child->{name} -->\n",
                                "\n";

            recursivewrite($docinfo, $outputfile, $outputfilename, $child);
        }

        print $outputfile   "</UL>\n";
    }

    print $outputfile   "</TD>\n",
                        "\n";

    print $outputfile   "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub recursivewrite($$$$);

sub recursivewrite($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $node) = @_;

    print $outputfile   "<LI type=\"circle\"><SMALL>$node->{name}</SMALL>\n",
                        "<UL>\n";

    foreach my $link (@{$node->{links}})
    {
        # Replace any ampersands in url by "&amp;"

        my $url =  $link->{url};
        $url    =~ s/&/&amp;/g;

        if($url =~ /^http/)
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
        }
        else
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
        }
    }

    foreach my $child (@{$node->{children}})
    {
        recursivewrite($docinfo, $outputfile, $outputfilename, $child);
    }

    print $outputfile   "</UL>\n",
                        "</LI>\n";
                        "\n";
}

# EOF
