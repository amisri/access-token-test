#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Writer.pm
# Purpose:  Provides functions to generate documentation for each platform
# Author:   Stephen Page

package Output::Doc::Platforms::Writer;

use Exporter;
use Output::Doc::Platforms::Boot::Menu;
use Output::Doc::Platforms::Connectors;
use Output::Doc::Platforms::Hw;
use Output::Doc::Platforms::Memmap;
use Output::Doc::Platforms::Platforms;
use Output::Doc::Platforms::Xilinx_progs;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);


# Begin functions

sub write($$$)
{
    my ($projpath, $superhash, $docinfo) = @_;

    # Write platform documentation

    # Write platform index

    my $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform_index}";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Doc::Platforms::Platforms::writeindex($docinfo, \*FILE, "$outputfilename", $superhash->{platforms});
    close(FILE);

    # Write documentation for each platform

    foreach my $platform (values(%{$superhash->{platforms}}))
    {
        my $outputfilename;

        # Generate platform page

        $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$platform->{name}.htm";

        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Doc::Platforms::Platforms::write($docinfo, \*FILE, $outputfilename, $platform);
        close(FILE);

        # Generate documentation for boot menu

        if(defined($platform->{boot}) && defined($platform->{boot}->{menu}))
        {
            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$platform->{name}_boot_menu_$platform->{boot}->{menu}->{id}.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            Output::Doc::Platforms::Boot::Menu::write($docinfo, \*FILE, $outputfilename, $platform->{boot}->{menu});
            close(FILE);

            # Write summary page

            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$platform->{name}_boot_menu_summary.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            Output::Doc::Platforms::Boot::Menu::writesummary($docinfo, \*FILE, $outputfilename, $platform->{boot}->{menu});
            close(FILE);
        }

        # Write index of hardware

        if(defined($platform->{hardware}))
        {
            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{hardware_index}$platform->{name}.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
            Output::Doc::Platforms::Hw::writeindex($docinfo, \*FILE, "$outputfilename", $platform->{hardware});
            close(FILE);
        }

        # Write documentation for testing of hardware

        foreach my $hardware (values(%{$platform->{hardware}}))
        {
            # Write top-level hardware documentation

            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{hardware}$hardware->{lhc_id}.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
            Output::Doc::Platforms::Hw::write($docinfo, \*FILE, $outputfilename, $hardware);
            close(FILE);

            # Write documentation for Xilinxes

            foreach my $xilinx (@{$hardware->{xilinxes}})
            {
                # Ignore Xilinxes on tester

                if(!$xilinx->{tester} && defined($xilinx->{programs}))
                {
                    $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{hardware}$hardware->{lhc_id}_xilinx$xilinx->{ic_number}.htm";
                    open(FILE, ">$outputfilename")
                        or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
                    Output::Doc::Platforms::Xilinx_progs::write($docinfo, \*FILE, $outputfilename, $xilinx);
                    close(FILE);
                }
            }

            # Write documentation for connectors

            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{hardware}$hardware->{lhc_id}_connect.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing\n";
            Output::Doc::Platforms::Connectors::write($docinfo, \*FILE, $outputfilename, $hardware);
            close(FILE);
        }

        # Generate documentation for platform's memory map

        if(defined($platform->{memmaps}))
        {
            foreach my $memmap (keys(%{$platform->{memmaps}}))
            {
                my $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$platform->{name}_memmap_$memmap.htm";
                open(FILE, ">$outputfilename")
                    or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
                Output::Doc::Platforms::Memmap::write($docinfo, \*FILE, $outputfilename, $memmap, $platform->{memmaps}->{$memmap});
                close(FILE);

                # Write summary page

                $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$platform->{name}_memmap_${memmap}_summary.htm";
                open(FILE, ">$outputfilename")
                    or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
                Output::Doc::Platforms::Memmap::writesummary($docinfo, \*FILE, $outputfilename, $memmap, $platform->{memmaps}->{$memmap});
                close(FILE);
            }
        }
    }
}

# EOF
