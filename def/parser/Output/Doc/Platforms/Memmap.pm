#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Memmap.pm
# Purpose:  Provides functions relating to writing platform memory map documentation
# Author:   Stephen Page

package Output::Doc::Platforms::Memmap;

use Carp;
use strict;
use Exporter;

use Output::Doc::General;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writesummary write);

return(1);


# Begin functions

sub writesummary($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $name, $memzone) = @_;

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, uc($memzone->{platform}->{name})." $name MemMap");

    # Write table header

    print $outputfile   "<A name=\"$memzone->{fullname}\"></A>\n",
                        "<TABLE class=\"main\">\n",
                        "\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$memzone->{platform}->{name}.htm", "PLATFORM", "START");
    print $outputfile   "<TH>Memory Map Summary</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_$name.htm", "DETAILS", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    # Write zone tree

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";
    Output::Doc::Platforms::Memmap::writezonesummaryline($docinfo, $outputfile, $outputfilename, $name, $memzone);
    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub writezonesummaryline($$$$$);

sub writezonesummaryline($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $name, $memzone) = @_;

    print $outputfile   "<TR>\n",
                        "<TD align=\"left\"><SMALL><B>";

    if($memzone->{fullname} ne "")
    {
        print $outputfile "<A href=\"$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_${name}_$memzone->{fullname}.htm\">$memzone->{fullname}</A>";
    }
    else
    {
        print $outputfile "<A href=\"$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_$name.htm\">[TOP]</A>";
    }

    print $outputfile   "</B></SMALL></TD>\n",
                        "<TD align=\"center\"><SMALL>";
    printf $outputfile ("0x%08X:%d</SMALL>\n", int($memzone->{address} / 8), $memzone->{address} % 8);
    Output::Doc::General::writerowdoc($docinfo, $outputfile, $memzone->{comment});

    # Write children

    if(defined($memzone->{children}))
    {
        # Sort depending upon endianness

        my @sorted_children;

        if($memzone->{endian} eq "big")
        {
            # Sort children backwards by bit address, then forwards by byte address

            @sorted_children = sort {
                                        $a->{byte_address}  <=> $b->{byte_address}
                                        ||
                                        ($b->{address} % 8) <=> ($a->{address} % 8)
                                    } @{$memzone->{children}};
        }
        else # ($memzone->{endian} eq "little")
        {
            # Sort children forwards by bit address, then forwards by byte address

            @sorted_children = sort {
                                        $a->{byte_address}  <=> $b->{byte_address}
                                        ||
                                        ($a->{address} % 8) <=> ($b->{address} % 8)
                                    } @{$memzone->{children}};
        }

        foreach my $child (@sorted_children)
        {
            Output::Doc::Platforms::Memmap::writezonesummaryline($docinfo, $outputfile, $outputfilename, $name, $child);
        }
    }
}

sub write($$$$$;$;$);

sub write($$$$$;$;$)
{
    my ($docinfo, $outputfile, $outputfilename, $name, $memzone, $previous_zone, $next_zone) = @_;

    my $col3width   = "30%";
    my $navarrow    = " vspace=\"1\" width=\"15\" height=\"15\" border=\"0\"";
    my $tabtitle    = $memzone->{fullname} eq "" ? "[TOP]" : $memzone->{fullname};

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, uc($memzone->{platform}->{name})." $name MemMap $tabtitle");

    print $outputfile   "<!-- Memzone $memzone->{fullname} -->\n",
                        "\n",
                        "<A name=\"$memzone->{fullname}\"></A>\n",
                        "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$memzone->{platform}->{name}.htm", "PLATFORM", "START");

    print $outputfile "<TH>$tabtitle</TH>\n";

    # Generate navigation buttons

    if(defined($memzone->{parent}))
    {
        print $outputfile   "<TH class=\"sideindex\">\n";

        if(defined($previous_zone))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$previous_zone->{platform}->{name}_memmap_${name}_$previous_zone->{fullname}.htm\"><img alt=\"Left arrow\" src=\"../../images/blueleft.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile   "<img alt=\"Left arrow\" src=\"../../images/greyleft.gif\"$navarrow>\n";
        }

        # Check whether parent is the root node

        if($memzone->{parent}->{fullname} eq "")
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_$name.htm\"><img alt=\"Up arrow\" src=\"../../images/blueup.gif\"$navarrow hspace=\"6\"></A>\n";
        }
        else
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_${name}_$memzone->{parent}->{fullname}.htm\"><img alt=\"Up arrow\" src=\"../../images/blueup.gif\"$navarrow hspace=\"6\"></A>\n";
        }

        if(defined($next_zone))
        {
            print $outputfile "<A href=\"$docinfo->{paths}->{platform}_$next_zone->{platform}->{name}_memmap_${name}_$next_zone->{fullname}.htm\"><img alt=\"Right arrow\" src=\"../../images/blueright.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile "<img alt=\"Right arrow\" src=\"../../images/greyright.gif\"$navarrow>\n";
        }

        print $outputfile   "</TH>\n",
                            "</TR>\n",
                            "\n";
    }
    else
    {
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_${name}_summary.htm", "SUMMARY", "END");
    }

    print $outputfile   "</TABLE>\n",
                        "\n",
                        "<TABLE class=\"main\">\n",
                        "\n";

    # Write address

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "left", "Address:");
    printf $outputfile ("0x%08X:%d\n", int($memzone->{address} / 8), $memzone->{address} % 8);
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write size

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "left", "Size:");
    print $outputfile   $memzone->{size};
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

    # Write type

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "left", "Type:");
    if(defined($memzone->{type}))
    {
        print $outputfile   $memzone->{type};
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write access

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD width=\"50%\">", $col3width, "left", "Access:");
    if(!defined($memzone->{access}) || $memzone->{access} eq "RW")
    {
        print $outputfile   "READ/WRITE";
    }
    elsif($memzone->{access} eq "R")
    {
        print $outputfile   "READ ONLY";
    }
    elsif($memzone->{access} eq "W")
    {
        print $outputfile   "WRITE ONLY";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

    # Write constants

    print $outputfile   "<TR>\n",
                        "<TD colspan=\"3\"><TABLE class=\"field\">\n",
                        "<TR>\n",
                        "<TD>Constants: </TD>\n",
                        "\n";

    if(defined($memzone->{consts}))
    {
        # Write revision descriptions

        my $first_flag = 1;
        foreach my $const_type (sort(keys %{$memzone->{const_types}}))
        {
            if($first_flag)
            {
                $first_flag = 0;
            }
            else
            {
                print $outputfile   "<TR><TD>&nbsp;</TD>";
            }

            print $outputfile   "<TD>$const_type</TD>";

            if($const_type eq "16BIT")
            {
                # Truncate address to 16 bits

                my $truncated_address   =  sprintf("%04X", $memzone->{address} / 8);
                $truncated_address      =~ s/^.*(.{4,4})$/$1/;

                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_16</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%s</TD>", $truncated_address);
            }
            elsif($const_type eq "24BIT")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_24</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%06X</TD>", $memzone->{address} / 8);
            }
            elsif($const_type eq "32BIT")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_32</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%08X</TD>", $memzone->{address} / 8);
            }
            elsif($const_type eq "PAGE")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_PG</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%X</TD>", $memzone->{address} / 524288);
            }
            elsif($const_type eq "ARRAY")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_A</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;((%-6s *)0x%05X)</TD>", $memzone->{type}, $memzone->{address} / 8);
            }
            elsif($const_type eq "FAR")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_FP</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;*(%-6s *far)0x%05X</TD>", $memzone->{type}, $memzone->{address} / 8);
            }
            elsif($const_type eq "MASK8")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_MASK8</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%02X</TD>", $memzone->{mask});
            }
            elsif($const_type eq "MASK16")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_MASK16</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%04X</TD>", $memzone->{mask});
            }
            elsif($const_type eq "MASK32")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_MASK32</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%08X</TD>", $memzone->{mask});
            }
            elsif($const_type eq "NEAR" || $const_type eq "POINTER")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_P</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;*(%-6s *)0x%08X</TD>", $memzone->{type}, $memzone->{address} / 8);
            }
            elsif($const_type eq "SHIFT")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_SHIFT</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;0x%04X</TD>", $memzone->{shift});
            }
            elsif($const_type eq "SIZEBITS")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_BITS</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;%6u</TD>", $memzone->{numbits});
            }
            elsif($const_type eq "SIZEBYTES")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_B</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;%6u</TD>", ($memzone->{numbits} / 8) + (($memzone->{numbits} % 8) ? 1 : 0));
            }
            elsif($const_type eq "SIZEWORDS")
            {
                printf $outputfile  ("<TD>&nbsp;&nbsp;&nbsp;&nbsp;$memzone->{fullname}_W</TD><TD nowrap>&nbsp;&nbsp;&nbsp;&nbsp;%6u</TD>", ($memzone->{numbits} / 16) + (($memzone->{numbits} % 16) ? 1 : 0));
            }

            print $outputfile   "</TR>\n",
                                "\n";
        }
    }
    else
    {
        print $outputfile   "</TR>\n",
                            "\n";
    }

    print $outputfile   "</TABLE>\n",
                        "\n",
                        "</TD>\n",
                        "</TR>\n",
                        "\n";

    # Write documentation if defined, otherwise comment

    if(defined($memzone->{doc}))
    {
        # Write documentation

        print $outputfile   "<TR>\n",
                            "<TD colspan=\"3\">\n",
                            "$memzone->{doc}\n",
                            "</TD>\n",
                            "</TR>\n",
                            "</TABLE>\n",
                            "\n";
    }
    else
    {
        # Write comment

        print $outputfile   "<TR>\n",
                            "<TD colspan=\"3\">\n",
                            "$memzone->{comment}\n",
                            "</TD>\n",
                            "</TR>\n",
                            "</TABLE>\n";
    }

    # Write children

    if(defined($memzone->{children}))
    {
        # Write children

        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH>Child zone</TH>\n",
                            "<TH>Address</TH>\n",
                            "<TH>Size</TH>\n",
                            "<TH>Documentation</TH>\n",
                            "</TR>\n",
                            "\n";

        # Sort depending upon endianness

        my @sorted_children;

        if($memzone->{endian} eq "big")
        {
            # Sort children backwards by bit address, then forwards by byte address

            @sorted_children = sort {
                                        $a->{byte_address}  <=> $b->{byte_address}
                                        ||
                                        ($b->{address} % 8) <=> ($a->{address} % 8)
                                    } @{$memzone->{children}};
        }
        else # ($memzone->{endian} eq "little")
        {
            # Sort children forwards by bit address, then forwards by byte address

            @sorted_children = sort {
                                        $a->{byte_address}  <=> $b->{byte_address}
                                        ||
                                        ($a->{address} % 8) <=> ($b->{address} % 8)
                                    } @{$memzone->{children}};
        }

        for(my $i = 0 ; $i < scalar(@sorted_children) ; $i++)
        {
            my $child           = $sorted_children[$i];
            my $childfilename   = "$docinfo->{doc_path}/$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_${name}_$child->{fullname}.htm";
            local *CHILDFILE;

            open(CHILDFILE, ">$childfilename") || confess "$childfilename(0): ERROR Unable to open version file for writing : $!\n";

            my $previous_zone;
            if($i == 0)
            {
                undef($previous_zone);
            }
            else
            {
                $previous_zone = $sorted_children[$i - 1];
            }

            my $next_zone;
            if($i == (scalar(@sorted_children) - 1))  # This is the last zone
            {
                undef($next_zone);
            }
            else
            {
                $next_zone = $sorted_children[$i + 1];
            }

            Output::Doc::Platforms::Memmap::write($docinfo, *CHILDFILE, $childfilename, $name, $child, $previous_zone, $next_zone);
            close(CHILDFILE);

            print  $outputfile  "<TR>\n",
                                "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$memzone->{platform}->{name}_memmap_${name}_$child->{fullname}.htm\">$child->{name}</A></SMALL></TD>\n";
            printf $outputfile ("<TD align=\"center\"><SMALL>0x%08X:%d</SMALL></TD>\n", int($child->{address} / 8), $child->{address} % 8);
            print  $outputfile  "<TD align=\"center\"><SMALL>$child->{size}</SMALL>\n";
            Output::Doc::General::writerowdoc($docinfo, $outputfile, $child->{comment});
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
