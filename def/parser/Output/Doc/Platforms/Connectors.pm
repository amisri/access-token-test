#!/usr/bin/perl -w
#
# Name:     Output/Doc/Platforms/Connectors.pm
# Purpose:  Provides functions relating to writing the documentation for each piece of hardware's connectors
# Author:   Stephen Page

package Output::Doc::Platforms::Connectors;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $hardware) = @_;

    my $col3width = "30%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$hardware->{label} Connectors");

    # Write heading if there are no connectors

    if(scalar(@{$hardware->{connectors}}) == 0)
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware}$hardware->{lhc_id}.htm", $hardware->{label}, "START");
        print $outputfile   "<TH>No connectors</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware_index}$hardware->{platform}->{name}.htm", "HARDWARE", "END");
        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write a table for each connector

    foreach my $connector (sort {$a->{id} cmp $b->{id}} (@{$hardware->{connectors}}))
    {
        # Write table heading with links

        print $outputfile   "<!-- Connector $connector->{id} -->\n",
                            "\n",
                            "<A name=\"$connector->{id}\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware}$hardware->{lhc_id}.htm", $hardware->{label}, "START");
        print $outputfile   "<TH>$connector->{id}: $connector->{name} - $connector->{type}</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{hardware_index}$hardware->{platform}->{name}.htm", "HARDWARE", "END");
        print $outputfile   "</TABLE>\n",
                            "\n";

        # Write table of pins

        print $outputfile   "<A name=\"$connector->{id}\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TH nowrap width=\"15%\">Number</TH>\n",
                            "<TH nowrap class=\"alt\">Sig Type</TH>\n",
                            "<TH nowrap>IO</TH>\n",
                            "<TH nowrap>Label</TH>\n",
                            "<TH nowrap>Use</TH>\n",
                            "</TR>\n",
                            "\n";

        # Write pin entries and count types of signal

        my %sig_type_count;
        foreach my $pin (@{$connector->{pins}})
        {
            # Write pin entry

            print $outputfile   "<!-- Connector $connector->{id}, pin $pin->{number} -->\n",
                                "\n",
                                "<TR>\n",
                                "<TD align=\"center\" valign=\"top\"><SMALL>$pin->{number}</SMALL></TD>\n",
                                "<TD align=\"center\" valign=\"top\"><SMALL>$pin->{sig_type}</SMALL></TD>\n",
                                "<TD align=\"center\" valign=\"top\"><SMALL>", $pin->{IO} || "&nbsp;", "</SMALL></TD>\n",
                                "<TD align=\"left\"   valign=\"top\"><SMALL>$pin->{label}</SMALL></TD>\n",
                                "<TD align=\"left\"   valign=\"top\"><SMALL>$pin->{use}&nbsp;</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";

            # Increment sig_type count

            $sig_type_count{$pin->{sig_type}}++;
        }

        print $outputfile "</TABLE>\n\n";

        # Write signal type counts

        my %sig_type_names = (
                                A => "Analog",
                                D => "Dynamic",
                                G => "Ground",
                                I => "Independent",
                                S => "Static",
                                P => "Power",
                                U => "Undefined",
                             );

        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR class=\"alt\">\n";

        foreach my $sig_type (sort(keys(%sig_type_count)))
        {
            if(defined($sig_type_names{$sig_type}))
            {
                print $outputfile "<TD class=\"alt\"><SMALL><B>", substr($sig_type_names{$sig_type}, 0, 1), "</B>", substr($sig_type_names{$sig_type}, 1), ": $sig_type_count{$sig_type}</SMALL></TD>\n";
            }
            else
            {
                print $outputfile "<TD class=\"alt\"><SMALL><B>$sig_type: $sig_type_count{$sig_type}</B></SMALL></TD>\n";
            }
        }

        print $outputfile   "</TR>\n",
                            "</TABLE>\n",
                            "\n";

        # Leave a space after each connector

        print $outputfile   "<BR>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
