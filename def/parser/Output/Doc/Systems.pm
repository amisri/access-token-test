#!/usr/bin/perl -w
#
# Name:     Output/Doc/Systems.pm
# Purpose:  Provides functions to write documentation of systems
# Author:   Stephen Page

package Output::Doc::Systems;

use Exporter;
use Output::Doc::General;
use Carp;

use strict; 

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writeindex write);

my @sorted_hardware;

return(1);


# Begin functions

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $systems) = @_;

    my @sorted_systems = sort {$a->{type} cmp $b->{type}} values(%{$systems});

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "System Index");
    Output::Doc::General::writetabletitle($docinfo, $outputfile, "10%", "System", "Platform", "Description");

    for my $system (@sorted_systems)
    {
        print $outputfile   "<TR>\n",
                            "<TD><SMALL><A href=\"$docinfo->{paths}->{system}$system->{type}.htm\">$system->{type}</A></SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL>$system->{platform}</SMALL></TD>\n";
        Output::Doc::General::writerowdoc($docinfo, $outputfile, $system->{label});
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $system, $superhash) = @_;
    my $component_labels = $superhash->{component_labels};

    my $col3width   = "30%";
    my $navarrow    = " vspace=\"1\" width=\"15\" height=\"15\" border=\"0\"";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$system->{type} System");

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "", "", "START");
    print $outputfile   "<TH>$system->{type}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{system_index}", "SYSTEMS", "END");

    print $outputfile   "</TABLE>\n",
                        "\n";


    # Write system information

    print $outputfile   "<TABLE class=\"main\">\n";

    # Write label

    print $outputfile   "<TR><TD>$system->{label}</TD></TR>\n";

    # Write flags table if any defined

    if(defined($system->{flags}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR>\n",
                            "<TH><SMALL>Flag Name</SMALL></TH>\n",
                            "<TH><SMALL>Enabled</SMALL></TH>\n",
                            "</TR>\n",
                            "\n";

        for my $flag_name (keys(%{$system->{flags}}))
        {
            print $outputfile   "<TR>\n",
                                "<TD><SMALL>".uc($flag_name)."</TD>\n",
                                "<TD><SMALL>".($system->{flags}->{$flag_name})."</TD>\n",
                                "</TR>\n";
        }

        print $outputfile   "</TABLE>\n";
    }

    # Start components table

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH><SMALL>Barcode</SMALL></TH>\n",
                        "<TH><SMALL>Label</SMALL></TH>\n",
                        "<TH><SMALL>Required</SMALL></TH>\n",
                        "</TR>\n",
                        "\n";

    # Write system components

    for my $component
    (
        # Filter single components without any bigger group

        grep
            { keys %{ $_->{barcodes} } == 1 }
            values
                %{ $system->{groups} }
    )
    {
        my $barcode = (keys %{ $component->{barcodes}})[0];
        my $label = $component_labels->{$barcode}->{label};

        my $output_html_string = "<TR>\n";
        $output_html_string .= "<TD><SMALL>$barcode</SMALL></TD>\n";
        $output_html_string .= "<TD><SMALL>$label</SMALL></TD>\n";
        $output_html_string .= "<TD align=\"center\"><SMALL>$component->{required}</SMALL></TD>\n";
        $output_html_string .= "</TR>\n";

        print $outputfile $output_html_string; 
    }

    # Write systems groups
    for my $group
    (
        # Filter real groups with more than one barcode inside

        grep
            { keys %{ $_->{barcodes} } > 1 }
            values
                %{ $system->{groups} }
    )
    {
        my @barcodes = keys %{ $group->{barcodes} };

        my $label = $component_labels->{$barcodes[0]}->{label};

        my $output_html_string = "<TR>\n";
        $output_html_string .= "<TD><SMALL>$barcodes[0]</SMALL></TD>\n";
        $output_html_string .= "<TD><SMALL>$label</SMALL></TD>\n";
        $output_html_string .= "<TD align=\"center\" rowspan=\"".@barcodes."\"><SMALL>$group->{required}</SMALL></TD>\n";
        $output_html_string .= "</TR>\n";

        print $outputfile $output_html_string; 

        for my $barcode (@barcodes[1..$#barcodes])
        {
            my $label = $component_labels->{$barcode}->{label};

            print $outputfile   "<TR>\n".
                                "<TD><SMALL>$barcode</SMALL></TD>\n".
                                "<TD><SMALL>$label</SMALL></TD>\n".
                                "</TR>\n";

        }
    }

    # End components table

    print $outputfile   "</TABLE>\n";

    # Write dims

    if(defined($system->{dims}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR>\n",
                            "<TH width=\"20%\"><SMALL>DIM Index</SMALL></TH>\n",
                            "<TH><SMALL>DIM Name</SMALL></TH>\n",
                            "<TH><SMALL>DIM Type</SMALL></TH>\n",
                            "<TH width=\"20%\"><SMALL>Bus Address</SMALL></TH>\n",
                            "</TR>\n",
                            "\n";

        for(my $i = 0 ; $i < @{$system->{dims}} ; $i++)
        {
            my $dim = $system->{dims}->[$i];
            next if(!defined($dim));

            print $outputfile   "<TR>\n",
                                "<TD align=\"center\" width=\"20%\"><SMALL>$i</SMALL></TD>\n",
                                "<TD><SMALL>$dim->{name}</SMALL></TD>\n",
                                "<TD><SMALL><A href=\"$docinfo->{paths}->{dim_type}$dim->{type}->{type}.htm\">$dim->{type}->{type}</A></SMALL></TD>\n",
                                "<TD align=\"center\" width=\"20%\"><SMALL>$dim->{bus_addr}</SMALL></TD>\n",
                                "</TR>\n";
        }
        print $outputfile   "</TABLE>\n";
    }

    print $outputfile   "</TABLE>\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
