#!/usr/bin/perl -w
#
# Name:     Output/Doc/GetSet.pm
# Purpose:  Provides functions relating to writing the FGC get and set functions' documentation
# Author:   Stephen Page

package Output::Doc::GetSet;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $getsethash) = @_;

    my $col1width = "30%";

    my ($firstelement)  = values(%$getsethash);
    my $functype        = $firstelement->{node_tag};

    # Capitalise first letter of $functype
    
    if(defined($functype))
    {
        $functype =~ s/^(.*)$/\u$1/;
    }
    else
    {
        $functype = "";
    }

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$functype Functions");
    Output::Doc::General::writepageindex($docinfo, $outputfile, "$functype Function", $functype, $getsethash, "name", "title");

    for my $getsetname (sort keys(%$getsethash))
    {
        print $outputfile   "<!-- $getsetname -->\n",
                            "\n",
                            "<A name=\"$getsetname\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        # Write index links

        Output::Doc::General::writeindexlink($docinfo, $outputfile, "#TOP", "INDEX", "START");
        print $outputfile   "<TH class=\"centreindex\">$functype$getsetname</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, $docinfo->{paths}->{property_index}, "PROPERTIES", "END");

        print $outputfile   "</TABLE>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        # Write types
        
        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD colspan=\"2\" valign=\"top\" width=\"50%\">", $col1width, "baseline", "Property types:", $docinfo->{paths}->{types});
        
        my $firstitem = 1;
        for my $type (@{$getsethash->{$getsetname}->{types}})
        {
            if(!$firstitem)
            {
                print $outputfile   ", ";
            }

            print $outputfile   "<A href=\"$docinfo->{paths}->{types}#$type->{name}\">$type->{name}</A>";
            
            $firstitem = 0;
        }

        # If writing get functions then write get options

        if($functype eq "Get")
        {
            Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

            Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD colspan=\"2\" valign=\"top\">", $col1width, "baseline", "Get options:", $docinfo->{paths}->{getoptions});

            $firstitem = 1;
            for my $getoption (@{$getsethash->{$getsetname}->{getopts}})
            {
                if(!$firstitem)
                {
                    print $outputfile   ", ";
                }

                print $outputfile   "<A href=\"$docinfo->{paths}->{getoptions}#$getoption->{symbol}\">$getoption->{symbol}</A>";
        
                $firstitem = 0;
            }
        }
        else
        {
            Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");
            Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD colspan=\"2\" valign=\"top\">", $col1width, "baseline", "&nbsp", "");
        }

        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

        # Write documentation

        print $outputfile   "<TR>\n",
                            "<TD colspan=\"4\">\n",
                            "$getsethash->{$getsetname}->{doc}\n",
                            "</TD>\n",
                            "</TR>\n",
                            "\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
