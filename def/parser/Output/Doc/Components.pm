#!/usr/bin/perl -w
#
# Name:     Output/Doc/Components.pm
# Purpose:  Provides functions related to writing the FGC component type documentation
# Author:   Stephen Page

package Output::Doc::Components;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $components, $superhash) = @_;

    my $col1width = "15%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Component Types");

    # Write table header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "", "", "START");
    print $outputfile   "<TH>Component Types</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "", "", "END");

    print $outputfile   "</TABLE>\n",
                        "\n",
                        "<TABLE class=\"main\">\n";
                        
    for my $component (sort {$a->{barcode} cmp $b->{barcode}} values(%$components))
    {
        my $rowspan = defined($component->{temp_prop}) ? 2 : 1;

        print $outputfile   "<A name=\"$component->{barcode}\"></A>\n",
                            "<TR>\n",
                            "<TD width=\"85px\"  rowspan=\"$rowspan\"><SMALL>$component->{barcode}</SMALL></A></TD>\n",
                            "<TD width=\"145px\" align=\"center\"><SMALL>";

        if(defined($component->{barcode_prop}))
        {
           (my $toplevelpropertyname    = $component->{barcode_prop}) =~ s/(.*?)\..*/$1/;
           (my $channelpropertyname     = $component->{barcode_prop}) =~ s/\?/A/;
            my $propertypage = "$docinfo->{paths}->{property}$toplevelpropertyname.htm";

            print $outputfile   "<A href=\"$propertypage#$channelpropertyname\">$component->{barcode_prop}</A>";
        }
        else
        {
            print $outputfile   "&nbsp;"
        }

        print $outputfile   "</SMALL></TD>\n",
                            "<TD><SMALL>$component->{label}</SMALL></A></TD>\n",
                            "</TR>\n",
                            "\n";

        # Write the second row if required

        if($rowspan == 2)
        {
            print $outputfile   "<TR>\n",
                                "<TD align=\"center\"><SMALL>";

            if(defined($component->{temp_prop}))
            {
               (my $toplevelpropertyname = $component->{temp_prop}) =~ s/(.*?)\..*/$1/;
               (my $channelpropertyname = $component->{temp_prop}) =~ s/\?/A/;
                my $propertypage = "$docinfo->{paths}->{property}$toplevelpropertyname.htm";

                print $outputfile   "<A href=\"$propertypage#$channelpropertyname\">$component->{temp_prop}</A>";
            }
            else
            {
                print $outputfile   "&nbsp;"
            }

            print $outputfile   "</SMALL></TD>\n",
                                "<TD><SMALL>";

            print $outputfile   "</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
        }

    }
    
    print $outputfile   "</TABLE>\n\n";
    
    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
