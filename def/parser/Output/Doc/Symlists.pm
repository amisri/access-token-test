#!/usr/bin/perl -w
#
# Name:     Output/Doc/Symlists.pm
# Purpose:  Provides functions relating to writing the FGC symlists' documentation
# Author:   Stephen Page

package Output::Doc::Symlists;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


sub write($$$$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $symlist, $superhash, $class_id) = @_;

    my $col1width = "70%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "$symlist->{title}");

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH width=\"$col1width\">$symlist->{name}: $symlist->{title}</TH>\n",
                        "</TR>\n",
                        "</TABLE>\n",
                        "\n";

    # Write documentation if defined

    if(defined($symlist->{doc}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "\n",
                            "<TR>\n",
                            "<TD>\n",
                            "$symlist->{doc}\n",
                            "</TD>\n",
                            "</TR>\n";
    }

    print $outputfile   "\n",
                        "</TABLE>\n",
                        "\n";

    # Write constants column headers

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH width=\"30%\">Symbol Name</TH>\n",
                        "<TH width=\"70%\">Title</TH>\n",
                        "</TR>\n",
                        "\n";

    # Write information for each constant

    for my $const (@{$symlist->{consts}})
    {
        print $outputfile   "<!-- $const->{symbol} -->\n",
                            "\n",
                            "<TR>\n",
                            "<TD width=\"30%\"><SMALL><A href=\"#$symlist->{name}.$const->{symbol}\">$const->{symbol}</A></SMALL></TD>\n",
                            "<TD width=\"70%\"><SMALL>$const->{title}<SMALL></TD>\n",
                            "</TR>\n",
                            "\n";
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    # If documentation is defined for this symlist, write documentation for each constant

    if(defined($symlist->{doc}))
    {
        for my $const (@{$symlist->{consts}})
        {
            print $outputfile   "<!-- $const->{name} -->\n",
                                "\n",
                                "<A name=\"$symlist->{name}.$const->{symbol}\"></A>\n",
                                "<TABLE class=\"main\">\n";
            Output::Doc::General::writeindexlink($docinfo, $outputfile, "#top", "INDEX", "START");
            print $outputfile "<TH colspan=\"2\" width=\"$col1width\">$const->{symbol}</TH>\n";
            Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{property_index}", "PROPERTIES", "END");
            print $outputfile   "<TR><TD colspan=\"4\">\n",
                                "$const->{doc}\n";

            # Print cause, consequence and action for alarm constants

            if(defined($const->{cause}))
            {
                print $outputfile   "<BR><BR>Cause: $const->{cause}\n",
                                    defined($const->{consequence})  ? "<BR><BR>Consequence: $const->{consequence}\n"    : "",
                                    defined($const->{action})       ? "<BR><BR>Action: $const->{action}\n"              : "";
            }

            print $outputfile   "</TD></TR>\n",
                                "\n";

            # Write constant values for each class

            for my $class (sort { $a->{id} <=> $b->{id} } (values(%{$superhash->{classes}})))
            {
                next if(defined($class_id) && $class_id != $class->{id});

                # Check whether class uses the constant

                if(defined($class->{symtab_consts}->{$const->{name}}))
                {
                    my $value   = $class->{consts}->{$const->{name}};
                    $value      = $superhash->{consts}->{$const->{name}} if(!defined($value));

                    print $outputfile   "<TR>\n",
                                        "<TD><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">$class->{id}</A></SMALL></TD>\n",
                                        "<TD><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">$class->{name}</A></SMALL></TD>\n",
                                        "<TD><SMALL>$class->{title}</SMALL></TD>\n";

                    if($value =~ /^\d+$/) # Constant value is numeric
                    {
                        printf $outputfile ("<TD><SMALL>0x%04X</SMALL></TD>\n", $value);
                    }
                    else # Constant value is a string
                    {
                        print $outputfile "<TD><SMALL>$value</SMALL></TD>\n";
                    }

                    print $outputfile   "</TR>\n";
                }
            }

            print $outputfile   "</TABLE>\n",
                                "\n";
        }
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
