#!/usr/bin/perl -w
#
# Name:     Output/Doc/Dim_types.pm
# Purpose:  Provides functions relating to writing the DIM types' documentation
# Author:   Stephen Page

package Output::Doc::Dim_types;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write writeindex);

return(1);


# Begin functions

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $superhash) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "DIM Type Index");

    # Write diag type column headers

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH width=\"$col1width\">DIM Type</TH>\n",
                        "<TH>DIM Type Label</TH>\n",
                        "</TR>\n",
                        "\n";

    for my $dim_type (sort {$a->{type} cmp $b->{type}} values(%{$superhash->{dim_types}}))
    {
        print $outputfile   "<TR>\n",
                            "<TD><A href=\"$docinfo->{paths}->{dim_type}$dim_type->{type}.htm\"><SMALL>$dim_type->{type}</SMALL></A></TD>\n",
                            "<TD><SMALL>$dim_type->{comp}->{label}",
                            defined($dim_type->{purpose}) ? " - $dim_type->{purpose}" : '',
                            "</SMALL></TD>\n",
                            "</TR>\n",
                            "\n";
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $dim_type) = @_;

    my $col1width = "40%";
    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "DIM $dim_type->{type}");

    # Write main header

    print $outputfile   "<TABLE class=\"main\">\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "", "", "START");
    print $outputfile   "<TH width=\"$col1width\">$dim_type->{type}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{dim_type_index}", "DIM TYPES", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR><TD>$dim_type->{comp}->{label}",
                        defined($dim_type->{purpose}) ? " - $dim_type->{purpose}" : '',
                        "</TD></TR>\n",
                        "</TABLE>\n",
                        "\n";

    # Write analogue channel information

    if(defined($dim_type->{ana_channels}))
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR><TH class=\"sideindex\" colspan=\"5\"><SMALL>Analogue</SMALL></TH></TR>\n",
                            "<TR>\n",
                            "<TH width=\"10%\"><SMALL>Index</SMALL></TH>\n",
                            "<TH><SMALL>Label</SMALL></TH>\n",
                            "<TH><SMALL>Min</SMALL></TH>\n",
                            "<TH><SMALL>Max</SMALL></TH>\n",
                            "<TH><SMALL>Units</SMALL></TH>\n",
                            "</TR>\n",
                            "\n";

        for my $ana_channel (@{$dim_type->{ana_channels}})
        {
            next if(!defined($ana_channel));

            print $outputfile   "<TR>\n",
                                "<TD align=\"center\"><SMALL>$ana_channel->{index}</SMALL></TD>\n",
                                "<TD><SMALL>$ana_channel->{label}</SMALL></TD>\n",
                                "<TD align=\"center\"><SMALL>$ana_channel->{min_0V}</SMALL></TD>\n",
                                "<TD align=\"center\"><SMALL>$ana_channel->{max_5V}</SMALL></TD>\n",
                                "<TD align=\"center\"><SMALL>$ana_channel->{units}</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    # Write digital channel information

    if(defined($dim_type->{dig_banks}) && grep { defined && defined($_->{inputs}) } @{$dim_type->{dig_banks}})
    {
        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR><TH class=\"sideindex\" colspan=\"6\"><SMALL>Digital</SMALL></TH></TR>\n",
                            "<TR>\n",
                            "<TH width=\"10%\"><SMALL>Bank Index</SMALL></TH>\n",
                            "<TH width=\"10%\"><SMALL>Input Index</SMALL></TH>\n",
                            "<TH width=\"15%\"><SMALL>Input Index inside Bank</SMALL></TH>\n",
                            "<TH><SMALL>Label</SMALL></TH>\n",
                            "<TH><SMALL>Zero</SMALL></TH>\n",
                            "<TH><SMALL>One</SMALL></TH>\n",
                            "</TR>\n",
                            "\n";

        my $i = -1;
        for my $dig_bank (@{$dim_type->{dig_banks}})
        {
            next if(!defined($dig_bank));

            my $first_input = 1;
            for my $input (@{$dig_bank->{inputs}})
            {
                $i++;
                next if(!defined($input));

                print $outputfile   "<TR>\n";

                # Write bank index if first input in bank

                if($first_input)
                {
                    print $outputfile   "<TD align=\"center\"rowspan=\"", scalar(grep { defined($_) } @{$dig_bank->{inputs}}),"\">", $dig_bank->{index}, "</TD>\n";
                }

                print $outputfile   "<TD align=\"center\"><SMALL>$i</SMALL></TD>\n";

                print $outputfile   "<TD align=\"center\"><SMALL>$input->{index}</SMALL></TD>\n",
                                    "<TD><SMALL>", $input->{fault} ? "FLT:" : "STA:", "$input->{label}</SMALL></TD>\n",
                                    "<TD align=\"center\"><SMALL>$input->{zero}</SMALL></TD>\n",
                                    "<TD align=\"center\"><SMALL>$input->{one}</SMALL></TD>\n",
                                    "</TR>\n",
                                    "\n";

                $first_input = 0;
            }
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
