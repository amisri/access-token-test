#!/usr/bin/perl -w
#
# Name:     Output/Doc/Types.pm
# Purpose:  Provides functions relating to writing the FGC types documentation
# Author:   Stephen Page

package Output::Doc::Types;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $typeshash) = @_;

    my $col1width = "30%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Property Types");
    Output::Doc::General::writepageindex($docinfo, $outputfile, "Property Type", "", $typeshash, "name", "title");

    for my $typename (sort keys(%$typeshash))
    {
        print $outputfile   "<!-- $typename -->\n",
                            "\n",
                            "<A name=\"$typename\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";

        # Write index links

        Output::Doc::General::writeindexlink($docinfo, $outputfile, "#top", "INDEX", "START");
        print $outputfile   "<TH class=\"centreindex\" colspan=\"2\">$typename</TH>\n";
        Output::Doc::General::writeindexlink($docinfo, $outputfile, $docinfo->{paths}->{property_index}, "PROPERTIES", "END");

        # Write size of properties of this type

        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD colspan=\"2\" valign=\"top\" width=\"50%\">", $col1width, "baseline", "Size in bytes:");
        print $outputfile   "$typeshash->{$typename}->{size}\n";
        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

        # Write default limits of properties of this type

        Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD colspan=\"2\" valign=\"top\">", $col1width, "baseline", "Default limits:");
        if(defined($typeshash->{$typename}->{limits}))
        {
            print $outputfile   "Min = $typeshash->{$typename}->{min}, Max = $typeshash->{$typename}->{max}";
        }
        else
        {
            print $outputfile   "None";
        }
        Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>\n");

        # Write documentation

        print $outputfile   "<TR><TD colspan=\"4\">$typeshash->{$typename}->{doc}</TD></TR>\n",
                            "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
