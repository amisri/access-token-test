#!/usr/bin/perl -w
#
# Name:     Output/Doc/Classes/ConfigProperties.pm
# Purpose:  Provides functions relating to writing the FGC classes' config properties documentation
# Author:   Stephen Page

package Output::Doc::Classes::ConfigProperties;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $class) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Class $class->{id} Config properties");

    print $outputfile   "<TABLE class=\"main\">\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{class}$class->{id}.htm", "CLASS", "START");
    print $outputfile   "<TH width=\"$col1width\">$class->{name}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$class->{platform}->{name}.htm", "PLATFORM", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    # Write property column headers

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH width=\"$col1width\">Property Name</TH>\n",
                        "<TH>Property Title</TH>\n",
                        "</TR>\n",
                        "\n";

    foreach my $property (@{$class->{properties}})
    {
        my $toplevelpropertyname    =  $property->{name};
        $toplevelpropertyname       =~ s/(.*?)\..*/$1/;

        my $propertypage = "$docinfo->{paths}->{property}$toplevelpropertyname.htm";

        # Check whether property is in group that has config flag set

        if(defined $property->{flags}->{CONFIG})
        {
            # Write details for property

            print $outputfile   "<TR>\n",
                                "<TD><A href=\"$propertypage#$property->{name}\"><SMALL>$property->{name}</SMALL></A></TD>\n",
                                "<TD><SMALL>$property->{title}</SMALL></TD>\n",
                                "</TR>\n",
                                "\n";
        }
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
