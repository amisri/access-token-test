#!/usr/bin/perl -w
#
# Name:     Output/Doc/Classes/Classes.pm
# Purpose:  Provides functions relating to writing the FGC classes' documentation
# Author:   Stephen Page

package Output::Doc::Classes::Classes;

use Exporter;
use Output::Doc::General;
use List::MoreUtils qw(any);
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(writeindex write);

return(1);


# Begin functions

sub recursivewrite($$$$);

sub recursivewrite($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $node) = @_;

    print $outputfile   "<LI type=\"circle\"><SMALL>$node->{name}</SMALL>\n",
                        "<UL>\n";

    foreach my $link (@{$node->{links}})
    {
        # Replace any ampersands in url by "&amp;"

        my $url =  $link->{url};
        $url    =~ s/&/&amp;/g;

        if($url =~ /^http/)
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
        }
        else
        {
            print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
        }
    }

    foreach my $child (@{$node->{children}})
    {
        recursivewrite($docinfo, $outputfile, $outputfilename, $child);
    }

    print $outputfile   "</UL>\n",
                        "</LI>\n";
                        "\n";
}

sub writeindex($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $classeshash) = @_;

    my $col1width = "40%";

    # Get a sorted list of classes (by ID)

    my @sorted_classes = sort {$a->{id} <=> $b->{id}} values(%$classeshash);

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Class Index");

    # Print front-end classes header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH>ID</TH>\n",
                        "<TH>Front-End Classes</TH>\n",
                        "<TH>Platform</TH>\n",
                        "<TH align=\"left\">Description</TH>\n",
                        "</TR>\n";

    # Print front-end classes

    foreach my $class (@sorted_classes)
    {
        # Stop at the start of the equipment device range

        last if($class->{id} >= 30);

        print $outputfile   "<TR>\n",
                            "<TD align=\"center\"><SMALL>$class->{id}</SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">$class->{name}</A></SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$class->{platform}->{name}.htm\">", "\U$class->{platform}->{name}", "</A></SMALL></TD>\n",
                            "<TD align=\"left\"><SMALL>$class->{title}</SMALL></TD>\n",
                            "</TR>\n";
    }

    # Print equipment classes header

    print $outputfile   "<TR>\n",
                        "<TH>ID</TH>\n",
                        "<TH>Equipment Classes</TH>\n",
                        "<TH>Platform</TH>\n",
                        "<TH align=\"left\">Description</TH>\n",
                        "</TR>\n";

    # Print equipment classes

    foreach my $class (@sorted_classes)
    {
        # Skip the gateway device range

        next if($class->{id} < 30);

        print $outputfile   "<TR>\n",
                            "<TD align=\"center\"><SMALL>$class->{id}</SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}.htm\">$class->{name}</A></SMALL></TD>\n",
                            "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$class->{platform}->{name}.htm\">", "\U$class->{platform}->{name}", "</A></SMALL></TD>\n",
                            "<TD align=\"left\"><SMALL>$class->{title}</SMALL></TD>\n",
                            "</TR>\n";
    }

    print $outputfile "</TABLE>\n\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $class) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Class $class->{id}: \U$class->{name}");

    # Write header

    print $outputfile   "<TABLE class=\"main\">\n",
                        "\n",
                        "<TR>\n",
                        "<TH class=\"sitemap\">Reference</TH>\n",
                        "<TH class=\"sitemap\">Documentation</TH>\n",
                        "</TR>\n",
                        "\n",
                        "<TR>\n";

    # Write reference area

    print $outputfile   "<TD VALIGN=\"top\">\n",
                        "<UL>\n",
                        "\n";

    print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{platform}_$class->{platform}->{name}.htm\">Platform</A></SMALL></LI>\n";
    print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}_properties.htm\">Properties</A></SMALL></LI>\n";
    print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}_config_properties.htm\">Config properties</A></SMALL></LI>\n" if(any { $_->{flags}->{CONFIG} } @{ $class->{properties} });
    print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$docinfo->{paths}->{class}$class->{id}_pubdata.htm\">Published data</A></SMALL></LI>\n";

    print $outputfile   "</UL>\n",
                        "</TD>\n",
                        "\n";

    # Write documentation area

    print $outputfile   "<TD VALIGN=\"top\">\n";

    if(defined($class->{sitemap}))
    {
        print $outputfile   "<UL>\n",
                            "\n";

        foreach my $link (@{$class->{sitemap}->{links}})
        {
            # Replace any ampersands in url by "&amp;"

            my $url =  $link->{url};
            $url    =~ s/&/&amp;/g;

            if($url =~ /^http/)
            {
                print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"$url\">$link->{name}</A></SMALL></LI>\n";
            }
            else
            {
                print $outputfile   "<LI type=\"disc\"><SMALL><A href=\"../../$url\">$link->{name}</A></SMALL></LI>\n";
            }
        }

        foreach my $child (@{$class->{sitemap}->{children}})
        {
            print $outputfile   "<!-- $child->{name} -->\n",
                                "\n";

            recursivewrite($docinfo, $outputfile, $outputfilename, $child);
        }

        print $outputfile   "</UL>\n";
    }
    print $outputfile   "</TD>\n",
                        "\n";

    print $outputfile   "</TR>\n",
                        "\n",
                        "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
