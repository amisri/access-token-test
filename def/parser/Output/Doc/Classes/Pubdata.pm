#!/usr/bin/perl -w
#
# Name:     Output/Doc/Classes/Pubdata.pm
# Purpose:  Provides functions relating to writing the FGC published data documentation
# Author:   Stephen Page

package Output::Doc::Classes::Pubdata;

use Exporter;
use Output::Doc::General;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$;$;$);

sub write($$$$;$;$)
{
    my ($docinfo, $outputfile, $outputfilename, $memzone, $previous_zone, $next_zone) = @_;

    my $col1width   = "70%";
    my $col3width   = "30%";
    my $navarrow    = " vspace=\"1\" width=\"15\" height=\"15\" border=\"0\"";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Class $memzone->{class}->{id}: Published Data");

    print $outputfile   "<A name=\"$memzone->{fullname}\"></A>\n",
                        "<TABLE class=\"main\">\n";

    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{class}$memzone->{class}->{id}.htm", "CLASS", "START");

    if($memzone->{name} ne "")
    {
        print $outputfile   "<TH width=\"$col1width\">$memzone->{fullname}</TH>\n";
    }
    else
    {
        print $outputfile   "<TH width=\"$col1width\">[TOP]</TH>\n";
    }

    # Generate MemMap navigation buttons

    if(defined($memzone->{parent}))
    {
        print $outputfile   "<TH class=\"sideindex\">\n";

        if(defined($previous_zone))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{class}$previous_zone->{class}->{id}_pubdata$previous_zone->{fullname}.htm\"><img alt=\"Left arrow\" src=\"../../images/blueleft.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile   "<img alt=\"Left arrow\" src=\"../../images/greyleft.gif\"$navarrow>\n";
        }

        print $outputfile   "<A href=\"$docinfo->{paths}->{class}$memzone->{class}->{id}_pubdata$memzone->{parent}->{fullname}.htm\"><img alt=\"Up arrow\" src=\"../../images/blueup.gif\"$navarrow hspace=\"6\"></A>\n";

        if(defined($next_zone))
        {
            print $outputfile   "<A href=\"$docinfo->{paths}->{class}$next_zone->{class}->{id}_pubdata$next_zone->{fullname}.htm\"><img alt=\"Right arrow\" src=\"../../images/blueright.gif\"$navarrow></A>\n";
        }
        else
        {
            print $outputfile   "<img alt=\"Right arrow\" src=\"../../images/greyright.gif\"$navarrow>\n";
        }

        print $outputfile   "</TH></TR>\n";
    }
    else
    {
        Output::Doc::General::writeindexlink($docinfo, $outputfile, "", "&nbsp;", "END");
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    print $outputfile   "<TABLE class=\"main\">\n";

    # Write address

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD width=\"50%\">", $col3width, "baseline", "Address:");
    printf $outputfile ("0x%05X:%d\n", int($memzone->{address} / 8), $memzone->{address} % 8);
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD>");

    # Write size

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TD>", $col3width, "baseline", "Size:");
    print $outputfile   $memzone->{size};
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");

    # Write property

    Output::Doc::General::writestartrowlist($docinfo, $outputfile, "<TR><TD colspan=\"2\">", "14%", "baseline", "Property:");

    if(defined($memzone->{property}))
    {
        my $toplevelpropertyname    =  $memzone->{property}->{name};
        $toplevelpropertyname       =~ s/(.*?)\..*/$1/;

        my $propertypage = "$docinfo->{paths}->{property}$toplevelpropertyname.htm";

        print $outputfile   "<A href=\"$propertypage#$memzone->{property}->{name}\">$memzone->{property}->{name}</A>";
    }
    Output::Doc::General::writeendrowlist($docinfo, $outputfile, "</TD></TR>");

    # Write doc

    print $outputfile   "<TR><TD colspan=\"2\">$memzone->{doc}</TD></TR>\n" if(defined($memzone->{doc}));

    print $outputfile   "</TABLE>\n";

    # Write children

    if(defined($memzone->{children}))
    {
        # Write children

        my $col1width = "20%";
        my $col2width = "13%";
        my $col3width = "13%";

        print $outputfile   "<TABLE class=\"main\">\n",
                            "<TR>\n",
                            "<TH>Child zone</TH>\n",
                            "<TH>Offset</TH>\n",
                            "<TH>Size</TH>\n",
                            "<TH>Documentation</TH>\n",
                            "</TR>\n";

        my @sorted_children = sort  {
                                        $a->{byte_address}  <=> $b->{byte_address}
                                        ||
                                        ($b->{address} % 8) <=> ($a->{address} % 8)
                                    } @{$memzone->{children}};

        for(my $i = 0 ; $i < scalar(@sorted_children) ; $i++)
        {
            my $child           = $sorted_children[$i];
            my $childfilename   = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$memzone->{class}->{id}_pubdata$child->{fullname}.htm";
            local *CHILDFILE;

            open(CHILDFILE, ">$childfilename") || confess "$childfilename(0): ERROR Unable to open version file for writing : $!\n";

            my $previous_zone;
            if($i == 0)
            {
                undef($previous_zone);
            }
            else
            {
                $previous_zone = $sorted_children[$i - 1];
            }

            my $next_zone;
            if($i == (scalar(@sorted_children) - 1))  # This is the last zone
            {
                undef($next_zone);
            }
            else
            {
                $next_zone = $sorted_children[$i + 1];
            }

            Output::Doc::Classes::Pubdata::write($docinfo, *CHILDFILE, $childfilename, $child, $previous_zone, $next_zone);
            close(CHILDFILE);

            print $outputfile   "<!-- $child->{name} -->\n",
                                "\n",
                                "<TR>\n",
                                "<TD align=\"center\"><SMALL><A href=\"$docinfo->{paths}->{class}$memzone->{class}->{id}_pubdata$child->{fullname}.htm\">$child->{name}</A></SMALL></TD>\n";
            printf $outputfile ("<TD align=\"center\"><SMALL>0x%05X:%d</SMALL></TD>\n", int($child->{address} / 8), $child->{address} % 8);
            print $outputfile   "<TD align=\"center\"><SMALL>$child->{size}</SMALL></TD>";
            Output::Doc::General::writerowdoc($docinfo, $outputfile, $child->{comment});
            print $outputfile   "\n";
        }

        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
