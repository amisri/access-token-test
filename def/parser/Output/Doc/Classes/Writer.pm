#!/usr/bin/perl -w
#
# Name:     Output/Doc/Classes/Writer.pm
# Purpose:  Provides functions to generate documentation for each class
# Author:   Stephen Page

package Output::Doc::Classes::Writer;

use Exporter;
use Output::Doc::Classes::Classes;
use Output::Doc::Classes::Properties;
use Output::Doc::Classes::ConfigProperties;
use Output::Doc::Classes::Pubdata;
use Carp;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$)
{
    my ($projpath, $superhash, $docinfo) = @_;

    # Write class documentation

    # Write class index

    my $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class_index}";
    open(FILE, ">$outputfilename")
        or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
    Output::Doc::Classes::Classes::writeindex($docinfo, \*FILE, "$outputfilename", $superhash->{classes});
    close(FILE);

    # Write documentation for each class

    foreach my $class (values(%{$superhash->{classes}}))
    {
        my $outputfilename;

        # Generate class page

        $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$class->{id}.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Doc::Classes::Classes::write($docinfo, \*FILE, $outputfilename, $class);
        close(FILE);

        # Generate documentation for class's symlists

        foreach my $symlist (values(%{$class->{symlists}}))
        {
            $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$class->{id}_$docinfo->{paths}->{symlists}_$symlist->{name}.htm";
            open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
            Output::Doc::Symlists::write($docinfo, \*FILE, $outputfilename, $symlist, $superhash, $class->{id});
            close(FILE);
        }

        # Generate documentation for class's properties

        $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$class->{id}_properties.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Doc::Classes::Properties::write($docinfo, \*FILE, $outputfilename, $class);
        close(FILE);

        $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$class->{id}_config_properties.htm";
        open(FILE, ">$outputfilename")
                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Doc::Classes::ConfigProperties::write($docinfo, \*FILE, $outputfilename, $class);
        close(FILE);

        # Generate documentation for class's published data

        $outputfilename = "$docinfo->{doc_path}/$docinfo->{paths}->{class}$class->{id}_pubdata.htm";
        open(FILE, ">$outputfilename")
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        Output::Doc::Classes::Pubdata::write($docinfo, \*FILE, $outputfilename, \%{$class->{pubdataroot}});
        close(FILE);
    }
}

# EOF
