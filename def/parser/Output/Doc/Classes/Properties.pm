#!/usr/bin/perl -w
#
# Name:     Output/Doc/Classes/Properties.pm
# Purpose:  Provides functions relating to writing the FGC classes' properties documentation
# Author:   Stephen Page

package Output::Doc::Classes::Properties;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $class) = @_;

    my $col1width = "40%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Class $class->{id} Properties");

    print $outputfile   "<TABLE class=\"main\">\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{class}$class->{id}.htm", "CLASS", "START");
    print $outputfile   "<TH width=\"$col1width\">$class->{name}</TH>\n";
    Output::Doc::General::writeindexlink($docinfo, $outputfile, "$docinfo->{paths}->{platform}_$class->{platform}->{name}.htm", "PLATFORM", "END");
    print $outputfile   "</TABLE>\n",
                        "\n";

    # Write property column headers

    print $outputfile   "<TABLE class=\"main\">\n",
                        "<TR>\n",
                        "<TH width=\"$col1width\">Property Name</TH>\n",
                        "<TH>Pub</TH>\n",
                        "<TH>Config</TH>\n",
                        "<TH>OP</TH>\n",
                        "<TH>KT</TH>\n",
                        "<TH>FROM SPARE</TH>\n",
                        "<TH>Property Title</TH>\n",
                        "</TR>\n",
                        "\n";

    foreach my $property (@{$class->{properties}})
    {
        my $toplevelpropertyname    =  $property->{name};
        $toplevelpropertyname       =~ s/(.*?)\..*/$1/;

        my $propertypage = "$docinfo->{paths}->{property}$toplevelpropertyname.htm";

        # Check for property within published data

        my $published = "&nbsp";
        if(defined($class->{pubdata}))
        {
            foreach my $zone (values(%{$class->{pubdata}}))
            {
                if(defined($zone->{property}) && $zone->{property}->{name} eq $property->{name})
                {
                    $published = "<A href=\"$docinfo->{paths}->{class}$class->{id}_pubdata$zone->{fullname}.htm\">YES</A>";
                    last;
                }
            }
        }

        # Check whether property is in groups that have the config and kt flags set

        my $config_flag = defined($property->{flags}->{CONFIG}) ? "YES" : "&nbsp;";
        
        # operational property
        my $op_property = 0;
        if(defined($property->{class_operational}) && defined($property->{class_operational}{$class->{id}}))
        {
            $op_property = $property->{class_operational}{$class->{id}};
        }
        elsif(defined($property->{operational}) && $property->{operational})
        {
            $op_property = $property->{operational};
        }


        my $op_flag     = $op_property != 0 ? "YES" : "&nbsp;";
        my $kt_flag     = defined($property->{kt}) && $property->{kt} != 0 ? "YES" : "&nbsp;";
        my $from_spare  = defined($property->{from_spare_converter}) && $property->{from_spare_converter} != 0 ? "YES" : "&nbsp;";

        # Write details for property

        print $outputfile   "<TR>\n",
                            "<TD><A href=\"$propertypage#$property->{name}\"><SMALL>$property->{name}</SMALL></A></TD>\n",
                            "<TD align=\"CENTER\"><SMALL>$published</SMALL></TD>\n",
                            "<TD align=\"CENTER\"><SMALL>$config_flag</SMALL></TD>\n",
                            "<TD align=\"CENTER\"><SMALL>$op_flag</SMALL></TD>\n",
                            "<TD align=\"CENTER\"><SMALL>$kt_flag</SMALL></TD>\n",
                            "<TD align=\"CENTER\"><SMALL>$from_spare</SMALL></TD>\n",
                            "<TD><SMALL>$property->{title}</SMALL></TD>\n",
                            "</TR>\n",
                            "\n";
    }

    print $outputfile   "</TABLE>\n",
                        "\n";

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
