#!/usr/bin/perl -w
#
# Name:     Output/Doc/Errors.pm
# Purpose:  Provides functions relating to writing the FGC error documentation
# Author:   Stephen Page

package Output::Doc::Errors;

use Exporter;
use Output::Doc::General;
use strict;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(write);

return(1);


# Begin functions

sub write($$$$)
{
    my ($docinfo, $outputfile, $outputfilename, $errorshash) = @_;

    my $col1width = "25%";

    Output::Doc::General::writeheader($docinfo, $outputfile, $outputfilename, "Error Messages");
    Output::Doc::General::writetabletitle($docinfo, $outputfile, $col1width, "Error Message");
    print $outputfile   "</TABLE>\n",
                        "\n";

    for my $errornumber (sort {$a <=> $b} keys(%$errorshash))
    {
        my $bookmark = $errorshash->{$errornumber}->{message};

        $bookmark =~ s/ /_/g;

        print $outputfile   "<!-- $errorshash->{$errornumber}->{message} -->\n",
                            "\n",
                            "<A name=\"$bookmark\"></A>\n",
                            "<TABLE class=\"main\">\n",
                            "\n";
        Output::Doc::General::writerowtitle($docinfo, $outputfile, $col1width, "! $errornumber $errorshash->{$errornumber}->{message}.");
        Output::Doc::General::writerowdoc($docinfo, $outputfile, $errorshash->{$errornumber}->{doc});
        print $outputfile   "</TABLE>\n",
                            "\n";
    }

    Output::Doc::General::writefooter($docinfo, $outputfile, $outputfilename);
}

# EOF
