#!/usr/bin/perl -w
#
# Name:     Output/EPICS/records/utils.pm
# Purpose:  Util functions for EPICS record generation
# Author:   Joao Afonso

package Output::EPICS::records::utils;

use Exporter;
use strict;
use Carp;

our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(get_size_40_description get_units get_precision record_type record_name);

# Constants

my $DEFAULT_UNITS      = "";
my $DEFAULT_PRECISION  = "3";

# Helper functions

sub record_name($)
{
    my $name = $_[0];
    $name =~ s/\./:/g;
    return $name;
}

sub get_size_40_description($)
{
    my ($zone) = @_;
    my $description = $zone->{kt_title} || $zone->{title};

    if(length($description) > 40)
    {
        carp "WARNING: $zone->{name} title used for EPICS decription is longer than 40 characters. Create shorter version under 'kt_title'.\n";
    }

    return ${\substr($description, 0, 40)};
}

sub get_units($;$)
{
    my ($zone, $data_axis) = @_;
    my $suffix = defined($data_axis) ? "_$data_axis" : "";
    return $zone->{"kt_units$suffix"} || $DEFAULT_UNITS;
}

sub get_precision($;$)
{
    my ($zone, $data_axis) = @_;
    my $suffix = defined($data_axis) ? "_$data_axis" : "";
    return $zone->{"kt_precision$suffix"} || $DEFAULT_PRECISION;
}

#------------------------------#
# Property type vs record type #
#------------------------------#
#
# NULL               - NULL     - stringin
# STRING             - W_STRING - waveform
# FLOAT              - REAL     - ai/ao
# ABSTIME            - REAL     - ai/ao
# ABSTIME8           - REAL     - ai/ao
# INT8/16/32         - INTEGER  - longin/longout
# SYM_LST & BIT_MASK - W_STRING - waveform
# SYM_LST            - STRING   - stringin/stringout
# POINT              - POINT    - 2 waveforms (time & ref)
#
sub record_type($)
{
    my ($zone) = @_;

    if(defined($zone->{children}))
    {
        # This property should not match any EPICS record
        return "unknown";
    }
    elsif(defined($zone->{flags}->{SYM_LST}))
    {
        if(defined($zone->{flags}->{BIT_MASK}))
        {
            # Decoded bitmask property: waveform record (string)
            return "W_STRING";
        }
        else
        {
            # Decoded enum property: string record
            return "STRING";
        }
    }
    elsif($zone->{type}->{name} eq "NULL")
    {
        # Null property (set-only): string record (with value ignored)
        return "NULL";
    }
    elsif($zone->{type}->{name} eq "STRING")
    {
        # String property: waveform record (string)
        return "W_STRING";
    }
    elsif($zone->{type}->{name} eq "FLOAT")
    {
        # Float property: analog record
        return "REAL";
    }
    elsif($zone->{type}->{name} eq "ABSTIME")
    {
        # Abstime property: analog record
        return "REAL";
    }
    elsif($zone->{type}->{name} eq "ABSTIME8")
    {
        # Abstime property: analog record
        return "REAL";
    }
    elsif($zone->{type}->{name} =~ /^INT\d*(\w)/)
    {
        # Integer properties (any size): long record
        return "INTEGER";
    }
    elsif($zone->{type}->{name} eq "POINT")
    {
        # Point properties (ref. tables): 2 waveform records (x/y axis data)
        return "POINT";
    }
    else
    {
        return "unknown";
    }
}

# EOF
