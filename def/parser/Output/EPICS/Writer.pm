#!/usr/bin/perl -w
#
# Name:     Output/EPICS/Writer.pm
# Purpose:  Provides functions to generate EPICS modules
# Author:   Joao Afonso

package Output::EPICS::Writer;

use Exporter;
use strict;
use Carp;

use File::Path qw(mkpath);
use File::Basename;

use Output::EPICS::asynDriver::FGC_handler;
use Output::EPICS::asynDriver::FGC_handler_fact;
use Output::EPICS::asynDriver::FGC_handler_make;
use Output::EPICS::asynDriver::pub_data;
use Output::EPICS::records::udp_records;
use Output::EPICS::records::udp_records_make;
use Output::EPICS::records::cmd_records;
use Output::EPICS::records::cmd_records_make;


return(1);


# Begin functions

sub write($$)
{
    my ($projpath, $superhash) = @_;

    print "********** Beginning generation of EPICS files **********\n\n";

    my $fgcudp_path             = "$projpath/sw/kt/fgcepics/fgcudpSup";
    my $fgcudp_Db_path          = "$fgcudp_path/Db";
    my $fgcudp_src_path         = "$fgcudp_path/src";
    my $fgcudp_src_classes_path = "$fgcudp_src_path/classes";
    my $fgcudp_src_common_path  = "$fgcudp_src_path/common";

    my $fgccmd_path      = "$projpath/sw/kt/fgcepics/fgccmdSup";
    my $fgccmd_Db_path   = "$fgccmd_path/Db";

    my %paths = (
                        class_params_h      => "$fgcudp_src_classes_path/#/class_#_params.h",
                        class_params_cpp    => "$fgcudp_src_classes_path/class_#_params.cpp",
                        class_selector_h    => "$fgcudp_src_common_path/inc/class_selector.h",
                        class_params_make   => "$fgcudp_src_path/Makefile",
                        stat_h              => "$fgcudp_src_classes_path/#/#_stat.h",
                        sub_defconst        => "$fgcudp_src_classes_path/#/sub_defconst.h",
                        sub_defsyms         => "$fgcudp_src_classes_path/#/sub_defsyms.h",
                        pub_data_h          => "$fgcudp_src_common_path/inc/pub_data.h",
                        fgcudp_records      => "$fgcudp_Db_path/fgcudp_class_#.substitutions",
                        fgcudp_records_make => "$fgcudp_Db_path/Makefile",
                        fgccmd_records      => "$fgccmd_Db_path/fgccmd_class_#.substitutions",
                        fgccmd_records_make => "$fgccmd_Db_path/Makefile",
                );

    my %writefuncs = (
    	                class_params_h      => \&Output::EPICS::asynDriver::FGC_handler::write_h,
                        class_params_cpp    => \&Output::EPICS::asynDriver::FGC_handler::write_cpp,
                        class_selector_h    => \&Output::EPICS::asynDriver::FGC_handler_fact::write,
                        class_params_make   => \&Output::EPICS::asynDriver::FGC_handler_make::write,
                        pub_data_h          => \&Output::EPICS::asynDriver::pub_data::write,
                        fgcudp_records      => \&Output::EPICS::records::udp_records::write,
                        fgcudp_records_make => \&Output::EPICS::records::udp_records_make::write,
                        fgccmd_records      => \&Output::EPICS::records::cmd_records::write,
                        fgccmd_records_make => \&Output::EPICS::records::cmd_records_make::write,
                     );

    my %symlinks = (
                        stat_h              => "../../../../../../inc/classes/#/#_stat.h",
                        sub_defconst        => "../../../../../../inc/classes/#/sub_defconst.h",
                        sub_defsyms         => "../../../../../../inc/classes/#/sub_defsyms.h",
                   );


    # Generate EPICS database files

    foreach my $class (values(%{$superhash->{classes}}))
    {
    	if(defined($class->{kt}) && $class->{kt})
    	{
	        $superhash->{current_class} = $class;

	        foreach my $file (qw(fgcudp_records fgccmd_records))
	        {
	            my $outputfilename =  "$paths{$file}";
	            $outputfilename    =~ s/#/$class->{id}/g;

                my $outputfiledir = dirname($outputfilename);
                mkpath($outputfiledir);

	            open(FILE, ">", $outputfilename)
	                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
	            $writefuncs{$file}(\*FILE, $outputfilename, $superhash);
	            close(FILE);
	        }

	        delete $superhash->{current_class};
    	}
    }

    # Generate EPICS database Makefiles

    foreach my $file (qw(fgcudp_records_make fgccmd_records_make))
    {
        my $outputfilename  =  "$paths{$file}";
        my $outputfiledir = dirname($outputfilename);
        mkpath($outputfiledir);

        open(FILE, ">", $outputfilename)
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        $writefuncs{$file}(\*FILE, $outputfilename, $superhash);
        close(FILE);
    }

    # Generate EPICS UDP device support C++ files and symlinks

    foreach my $class (values(%{$superhash->{classes}}))
    {
    	if(defined($class->{kt}) && $class->{kt})
    	{
	        $superhash->{current_class} = $class;

	        foreach my $file (qw(class_params_cpp class_params_h))
	        {
	            my $outputfilename  =  "$paths{$file}";
	            $outputfilename     =~ s/#/$class->{id}/g;

                my $outputfiledir = dirname($outputfilename);
                mkpath($outputfiledir);

	            open(FILE, ">", $outputfilename)
	                or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
	            $writefuncs{$file}(\*FILE, $outputfilename, $superhash);
	            close(FILE);
	        }

	        foreach my $file (qw(stat_h sub_defconst sub_defsyms))
	        {
	            my $outputfilename = "$paths{$file}";
	            my $outputlinkname = "$symlinks{$file}";

	            $outputfilename     =~ s/#/$class->{id}/g;
	            $outputlinkname     =~ s/#/$class->{id}/g;

                my $outputfiledir = dirname($outputfilename);
                mkpath($outputfiledir);

	            unlink($outputfilename);
				symlink($outputlinkname, $outputfilename)
	   				or die("Can't create symlink \"$outputfilename\": $!\n");
	        }

	        delete $superhash->{current_class};
    	}
    }

    # Generate EPICS common files

    foreach my $file (qw(class_selector_h pub_data_h class_params_make))
    {
        my $outputfilename  =  "$paths{$file}";
        my $outputfiledir = dirname($outputfilename);
        mkpath($outputfiledir);

        open(FILE, ">", $outputfilename)
            or confess "$outputfilename(9999): ERROR Unable to open file for writing : $!\n";
        $writefuncs{$file}(\*FILE, $outputfilename, $superhash);
        close(FILE);
    }

    print "********** Generation of EPICS files completed successfully **********\n\n";
}

# EOF
