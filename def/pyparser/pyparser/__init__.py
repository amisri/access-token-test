import os
import pathlib

ROOT_DIR = pathlib.Path(os.path.dirname(os.path.realpath(__file__)))
DEFAULT_PARSER_SRC_PATH = (ROOT_DIR / '..' / '..' / 'src').resolve()
PROJECT_PATH = (ROOT_DIR / '..' / '..' / '..').resolve()
