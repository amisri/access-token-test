import re
from fgc_parser_schemas import SuperDict, BaseModel

from pyparser.output.Doc.doc_generator import links_paths


def extra_parse(superdict: SuperDict):
    # iterate over everything
    stack = list()

    stack.append(superdict)

    while stack:
        element = stack.pop()

        if getattr(element, '_seen_by_extra_parser', False):
            continue

        if isinstance(element, BaseModel):
            element._seen_by_extra_parser = True

            for field_name in element.__fields__.keys():
                field_value = getattr(element, field_name)
                process_element(superdict, field_name, element)
                stack.append(field_value)
        elif isinstance(element, dict):
            # superdict elements for example
            for val in element.values():
                stack.append(val)
        elif isinstance(element, list):
            for child in element:
                stack.append(child)


def process_element(superdict: SuperDict, field_name: str, element):

    # docs
    if field_name == "doc":
        # get value
        field_value = getattr(element, field_name)
        if field_value is None:
            field_value = ""

        # make sure it has '_processed_docs'
        if 'processed_docs_' not in element.__fields__:
            raise Exception(f"Element {element.__repr_name__()} doesn't have processed_docs_")

        # process doc && store in "processed_docs_"
        element.processed_docs_ = parse_docs(superdict, field_value)


def parse_docs(superdict: SuperDict, value: str) -> str:
    # create all the links (no validation here, already done in testers.py)

    #  match new style all types of links, note " (quote) mark at the beginning
    new_links_regex = re.compile(r'"{(\w+) (\w+)(.*?)}')
    # match old style all types of links, no quote mark at the beginning
    old_links_regex = re.compile(r'{(\w+) (\w+)(.*?)}')

    link_types = {
        "ERR": links_paths["errors_path"],
        "FLAG": links_paths["flags_path"],
        "GET": links_paths["getfuncs_path"],
        "GOPT": links_paths["getoptions_path"],
        "SET": links_paths["setfuncs_path"],
        "SETIF": links_paths["setiffuncs_path"],
        "TYPE": links_paths["types_path"]
    }
    splink_types = {
        "PROP": links_paths["property_path"],
        "STAT": links_paths["state_path"],
        "SYMLSTC": {
            'global': links_paths["symlists_path"],
            'class': links_paths["class_symlist_index_path"]
        },
        "TRAN": links_paths["trans_path"]
    }

    def get_symlstc_address(symlist_name: str) -> str:
        if symlist_name in superdict.class_symlists:
            return splink_types["SYMLSTC"]["class"] + symlist_name
        return splink_types["SYMLSTC"]["global"] + symlist_name

    def new_links_replace_fn(match: re.Match) -> str:
        link_type = match.group(1)
        if link_type in link_types:
            return f'"{link_types[link_type]}#{match.group(2)}'
        elif link_type in splink_types:
            if link_type == "SYMLSTC":
                # if it is a class symlist, use another link
                return f'"{get_symlstc_address(match.group(2))}.htm#{match.group(2)}{match.group(3)}'
            return f'"{splink_types[link_type]}{match.group(2)}.htm#{match.group(2)}{match.group(3)}'
        return f"NOTFOUND: {link_type}{match.group(2)}{match.group(3)}"
        # raise Exception(f"Failed to parse link! {str(match)}")

    def old_links_replace_fn(match: re.Match) -> str:
        link_type = match.group(1)
        if link_type in link_types:
            return f'<A href="{link_types[link_type]}#{match.group(2)}">{match.group(2)}</A>'
        elif link_type in splink_types:
            if link_type == "SYMLSTC":
                # for symlist links, do not show symlist name in the link.
                # skip first character in match, which is a dot.
                # also if it is a class symlist, use another link
                return f'<A href="{get_symlstc_address(match.group(2))}.htm#{match.group(2)}{match.group(3)}">' \
                       f'{match.group(3)[1:]}</A>'
            return f'<A href="{splink_types[link_type]}{match.group(2)}.htm#{match.group(2)}{match.group(3)}">' \
                   f'{match.group(2)}{match.group(3)}</A>'
        return f"NOTFOUND: {link_type}{match.group(2)}{match.group(3)}"
        # raise Exception(f"Failed to parse link! {str(match)}")

    value = new_links_regex.sub(new_links_replace_fn, value)
    return old_links_regex.sub(old_links_replace_fn, value)
