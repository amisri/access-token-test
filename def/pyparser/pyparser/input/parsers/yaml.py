from pathlib import Path
from abc import ABC, abstractmethod
from dataclasses import dataclass
import logging
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap, CommentedSeq
from fgc_parser_schemas import DictWrapper, ListWrapper

logger = logging.getLogger(__name__)


@dataclass
class FileLineColumn:
    file: Path
    line: int
    column: int

    def __str__(self):
        if self.line == -1:
            return f"\"{self.file}\""
        return f"\"{self.file}:{self.line}\""


class BaseYamlParser(ABC):
    def __init__(self, yaml_file: Path, base_path: Path, load_includes=True):
        logger.debug(f"Loading {yaml_file}")

        self.base_path = base_path
        self.input_file = yaml_file

        yaml = self._get_yaml()

        self.data = yaml.load(self.input_file)
        logger.debug(f"{yaml_file} loaded")
        if load_includes:
            self._recurse(self.data)

    @abstractmethod
    def _get_yaml(self):
        pass

    @abstractmethod
    def _recurse(self, item):
        pass

    @staticmethod
    def get_file_line_column(parent, item=None):
        pass

    def get_include_file_full_path(self, filename: str) -> Path:
        # make sure forward slashes are used
        filename = filename.replace('\\', '/')

        # construct path based on base path
        full_path = self.base_path / Path(filename)

        if full_path.exists():
            return full_path

        # construct path based on path of the file that has the "include"
        including_file_path = self.input_file.parent

        full_path = including_file_path / Path(filename)

        if full_path.exists():
            return full_path

        raise Exception(f"Could not find file '{filename}' referenced in include element in file {self.input_file}")


def construct_yaml_map(self, node):
    data = DictWrapper()
    yield data
    value = self.construct_mapping(node)
    data.update(value)


def construct_yaml_seq(self, node):
    data = ListWrapper()
    yield data
    data.extend(self.construct_sequence(node))


class FastYamlParser(BaseYamlParser):
    def _get_yaml(self):
        yaml = YAML(typ="unsafe")

        yaml.Constructor.add_constructor('tag:yaml.org,2002:map', construct_yaml_map)
        yaml.Constructor.add_constructor('tag:yaml.org,2002:seq', construct_yaml_seq)

        return yaml

    def _recurse(self, item):
        if isinstance(item, dict):
            # add info about file
            if hasattr(item, "_origin_file"):
                # already parsed
                return
            else:
                item._origin_file = self.input_file
            # look for includes
            for k, v in item.copy().items():
                if k == "include":
                    if "file" in v:
                        filename = v["file"]
                        # make sure forward slashes are used
                        full_path = self.get_include_file_full_path(filename)
                        logger.debug(f"Loading include {filename}")
                        include_parser = FastYamlParser(full_path, self.base_path)
                        v["data"] = include_parser.data
                    else:
                        filename = v
                        # make sure forward slashes are used
                        full_path = self.get_include_file_full_path(filename)
                        logger.debug(f"Loading include {filename}")
                        include_parser = FastYamlParser(full_path, self.base_path)
                        item["data"] = include_parser.data
                # keep on recursing
                self._recurse(v)
        elif isinstance(item, list):
            # add info about file
            item._origin_file = self.input_file
            for element in item:
                self._recurse(element)

    @staticmethod
    def get_file_line_column(parent, item=None):
        return FileLineColumn(file=parent._origin_file, line=-1, column=-1)


class RoundTripYAMLParser(BaseYamlParser):
    def _get_yaml(self):
        return YAML()

    def _recurse(self, item, parent=None):
        if isinstance(item, CommentedMap):
            # add info about parent
            item._parent = parent
            # add info about file
            if hasattr(item, "_origin_file"):
                # already parsed
                return
            else:
                item._origin_file = self.input_file

            # look for includes
            for k, v in item.items():
                if k == "include":
                    if "file" in v:
                        filename = v["file"]
                        full_path = self.get_include_file_full_path(filename)
                        logger.debug(f"Loading include {filename}")
                        include_parser = RoundTripYAMLParser(full_path, self.base_path)
                        v["data"] = include_parser.data
                    else:
                        filename = v
                        full_path = self.get_include_file_full_path(filename)
                        logger.debug(f"Loading include {filename}")
                        include_parser = RoundTripYAMLParser(full_path, self.base_path)
                        item["data"] = include_parser.data
                # keep on recursing
                self._recurse(v, parent=item)
        elif isinstance(item, CommentedSeq):
            # add info about file
            item._origin_file = self.input_file
            # add info about parent
            item._parent = parent
            for element in item:
                self._recurse(element, parent=item)

    @staticmethod
    def get_file_line_column(parent, item=None):
        assert isinstance(parent, (CommentedMap, CommentedSeq))
        file = parent._origin_file

        if isinstance(parent, CommentedMap):
            if item is None:
                return FileLineColumn(file, parent.lc.line, parent.lc.col)
            l, c = parent.lc.data[item][0:2]
        else:
            # parent is CommentedSeq
            if item is not None:
                if isinstance(item, CommentedMap):
                    # it should have it's own data
                    return FileLineColumn(file, item.lc.line, item.lc.col)
                else:
                    l, c = parent.lc.data[item]
            else:
                l, c = parent.lc.line, parent.lc.col
        return FileLineColumn(file, l+1, c)
