import logging
import re
from pathlib import Path
from typing import Optional

from fgc_parser_schemas import SuperDict, Property
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class PropertyReader(BaseReader):
    def __init__(self, src_path: Path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)

        self.files = (self.src_path / 'properties').glob("*.yaml")

        self.classes = [int(x.name) for x in (self.src_path / "classes").iterdir() if x.is_dir()]
        self.platforms = [x.name for x in (self.src_path / "platforms").iterdir() if x.is_dir()]

    @staticmethod
    def _is_reference(value: str):
        return re.match(r'^\(.*\)\s*&', str(value))

    def _get_children(self, property_element):
        children = []

        for idx, child in enumerate(property_element.get("children", [])):
            if "property" in child:
                # simple case - "property" child
                children.append(child["property"])
            elif "include" in child:
                # included file
                inc_data = child["include"]["data"]
                # allowed elements in included file:
                # - single property
                # - properties (with children)
                if "property" in inc_data:
                    children.append(inc_data["property"])
                elif "properties" in inc_data:
                    result = self._get_children(inc_data["properties"])
                    children.extend(result)
                else:
                    # wrong node
                    flc = self.parser_type.get_file_line_column(inc_data)
                    raise Exception(f"Unknown element in {flc}.")
            else:
                # wrong node
                flc = self.parser_type.get_file_line_column(child, idx)
                raise Exception(f"Unknown element in {flc}.")
        return children

    def _parse_property(self, property_element, parent_property: Optional[Property]):
        children = self._get_children(property_element)
        if children:
            del property_element["children"]

        # compose full name
        if parent_property is None:
            # root
            property_element["full_name"] = property_element["name"]
        else:
            property_element["full_name"] = parent_property.full_name + "." + property_element["name"]

        # check duplicates
        if property_element["full_name"] in self.superdict.properties:
            flc = self.parser_type.get_file_line_column(property_element)
            raise Exception(f'Property {property_element["full_name"]} already exists! {flc}')

        # check type
        if "type" in property_element:
            property_type = property_element["type"]
            try:
                property_element["type"] = self.superdict.types[property_type]
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "type")
                raise Exception(f"Not valid type {property_type} in {flc}.")
        else:
            flc = self.parser_type.get_file_line_column(property_element, "type")
            raise Exception(f"Type required in {flc}.")

        # parse flags
        flags = {}
        if "flags" in property_element and len(property_element["flags"]):
            property_element["flags"] = property_element["flags"].strip()
            flags_str = property_element["flags"].split(' ')
            for flag in flags_str:
                try:
                    flags[flag] = self.superdict.flags[flag]
                except KeyError:
                    flc = self.parser_type.get_file_line_column(property_element, "flags")
                    raise Exception(f"Not valid flag {flag} in {flc}.")
        property_element["flags"] = flags

        # class specific attributes
        class_attribs = {}

        for cls in self.classes:
            if cls in property_element:
                class_attribs[cls] = property_element[cls].copy()
                del property_element[cls]

        # specific attributes

        for cl, v in class_attribs.items():
            # class specific type
            if "type" in v:
                spec_type = v["type"]
                try:
                    property_element.setdefault("specific_types", dict())[cl] = self.superdict.types[spec_type]
                except KeyError:
                    flc = self.parser_type.get_file_line_column(v, "type")
                    raise Exception(f"Not valid type {spec_type} in {flc}.")

            # class specific get function
            if "get" in v:
                spec_get = v["get"]
                try:
                    get_function = self.superdict.gets[spec_get]
                    property_element.setdefault("specific_get", dict())[cl] = get_function
                except KeyError:
                    flc = self.parser_type.get_file_line_column(v, "get")
                    raise Exception(f"Not valid get function {spec_get} in {flc}.")
                # Check that node type corresponds to one of the get function's types
                node_type = property_element["type"]
                if node_type not in get_function.types:
                    flc = self.parser_type.get_file_line_column(v, "type")
                    raise Exception(f"{node_type.name} is not a valid type for get function {get_function.name} in {flc}")

            # class specific set function
            if "set" in v:
                spec_set = v["set"]
                try:
                    set_function = self.superdict.sets[spec_set]
                    property_element.setdefault("specific_set", dict())[cl] = set_function
                except KeyError:
                    flc = self.parser_type.get_file_line_column(v, "set")
                    raise Exception(f"Not valid set function {spec_set} in {flc}.")
                # Check that node type corresponds to one of the set function's types
                node_type = property_element["type"]
                if node_type not in set_function.types:
                    flc = self.parser_type.get_file_line_column(v, "type")
                    raise Exception(
                        f"{node_type.name} is not a valid type for set function {set_function.name} in {flc}")

            # class specific numels
            if "numels" in v:
                # set the value
                property_element.setdefault("specific_numels", dict())[cl] = v["numels"]

            # class specific maxels
            if "maxels" in v:
                property_element.setdefault("specific_maxels", dict())[cl] = v["maxels"]

            # other class specific
            if "dsp" in v:
                property_element.setdefault("specific_dsp", dict())[cl] = v["dsp"]

            if 'pars' in v:
                property_element.setdefault("specific_pars", dict())[cl] = v['pars']

            if 'value' in v:
                property_element.setdefault('specific_value', dict())[cl] = v['value']

            if 'operational' in v:
                property_element.setdefault('specific_operational', dict())[cl] = v['operational']

            if 'step' in v:
                property_element.setdefault('specific_step', dict())[cl] = v['step']

            if 'flags' in v:
                specific_flags = {}
                v["flags"] = v["flags"].strip()
                flags_str = v["flags"].split(' ')

                # handle special "NONE" flag
                # it should apply NO flags (no global ones).
                if "NONE" in flags_str:
                    if len(flags_str) > 1:
                        flc = self.parser_type.get_file_line_column(property_element, "flags")
                        raise Exception(f"NONE flag should be the only flag set for class {cl}, "
                                        f"please remove the rest: {flags_str} in {flc}.")
                else:
                    for flag in flags_str:
                        try:
                            specific_flags[flag] = self.superdict.flags[flag]
                        except KeyError:
                            flc = self.parser_type.get_file_line_column(property_element, "flags")
                            raise Exception(f"Not valid class {cl} specific flag {flag} in {flc}.")
                property_element.setdefault("specific_flags", dict())[cl] = specific_flags

        # If the property has the CONFIG flag set, check that all conditions are met
        if "CONFIG" in property_element["flags"]:
            # Check that NON_VOLATILE flag is set
            if "NON_VOLATILE" not in property_element["flags"]:
                flc = self.parser_type.get_file_line_column(property_element, "flags")
                raise Exception(f"CONFIG flag is set, but not NON_VOLATILE flag is not in {flc}.")
            # Check that number of elements is 0
            if "numels" in property_element and int(property_element["numels"]) != 0:
                flc = self.parser_type.get_file_line_column(property_element, "numels")
                raise Exception(f"CONFIG flag is set, but number of elements is not 0 in {flc}.")

        # Check that no DSP flag is set directly
        if any([flag.startswith("DSP_") for flag in property_element["flags"].keys()]):
            flc = self.parser_type.get_file_line_column(property_element, "flags")
            raise Exception(f"DSP flags may not be set directly in {flc}.")

        if "limits" in property_element and \
                property_element["type"].limits and \
                property_element["limits"] != property_element["type"].limits:
            flc = self.parser_type.get_file_line_column(property_element, "limits")
            raise Exception(f"Mismatch of 'limits' in {property_element['full_name']}: "
                            f"{property_element['limits']} != {property_element['type'].limits} (type.limits) in {flc}")

        # If limits are not defined then obtain them from the property's type
        if "symlist" not in property_element \
                and "class_symlist" not in property_element \
                and "limits" not in property_element:
            # Check extra types (specific for class)
            for cl, val in property_element.get("specific_types", dict()).items():
                if val.limits is not None:
                    property_element.setdefault("specific_limits", dict())[cl] = val.limits
                    property_element.setdefault("specific_min", dict())[cl] = val.min
                    property_element.setdefault("specific_max", dict())[cl] = val.max

            if property_element["type"].limits is not None:
                property_element["limits"] = property_element["type"].limits
                property_element["min"] = property_element["type"].min
                property_element["max"] = property_element["type"].max

        if "symlist" in property_element or "class_symlist" in property_element:
            property_element["flags"]["SYM_LST"] = self.superdict.flags["SYM_LST"]

        if "limits" in property_element:
            flc = self.parser_type.get_file_line_column(property_element)
            if "min" not in property_element:
                raise Exception(f"Min limit not defined in {flc}")
            if "max" not in property_element:
                raise Exception(f"Max limit not defined in {flc}")
            property_element["flags"]["LIMITS"] = self.superdict.flags["LIMITS"]

        # setif

        if "setif" in property_element:
            setif = property_element["setif"]
            try:
                property_element["setif"] = self.superdict.setifs[setif]
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "setif")
                raise Exception(f"Not valid set if function {setif} in {flc}.")

        # Check that symlist of this node is valid
        if "symlist" in property_element:
            if "class_symlist" in property_element:
                flc = self.parser_type.get_file_line_column(property_element, "symlist")
                raise Exception(f"symlist and class_symlist are mutually exclusive in {flc}")
            symlist = property_element["symlist"]
            try:
                property_element["symlist"] = self.superdict.symlists[symlist]
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "symlist")
                raise Exception(f"Not valid symlist {symlist} in {flc}.")

        # Set property group
        if "group" in property_element:
            group = property_element["group"]
            try:
                property_element["group"] = self.superdict.property_groups[group]
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "group")
                raise Exception(f"Not valid property group {group} in {flc}.")
        else:
            if parent_property is not None:
                property_element["group"] = parent_property.group
            else:
                property_element["group"] = self.superdict.property_groups["DEFAULT"]

        # KT - if not defined, get from parent
        if "kt" not in property_element:
            if parent_property is not None:
                if parent_property.kt:
                    property_element["kt"] = True

        # EPCCCS-7439: check operational flag
        # if we reached a leaf node, check that at max one property in the property's hierarchy is marked as operational
        if property_element["type"].name != "PARENT":
            op_count = 1 if "operational" in property_element and property_element["operational"] else 0
            parent = parent_property
            while parent is not None:
                if parent.operational:
                    op_count += 1
                    if op_count > 1:
                        flc = self.parser_type.get_file_line_column(property_element)
                        raise Exception(f"Operational flag is defined at more than one level of the property's hierarchy"
                                        f" - {flc}")
                parent = parent.parent

        # get function
        if "get" in property_element and not len(property_element["get"]):
            # TODO: move this to translator
            del property_element["get"]

        if "get" in property_element:
            get_fn_name = property_element["get"]
            try:
                get_function = self.superdict.gets[get_fn_name]
                property_element["get"] = get_function
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "get")
                raise Exception(f"Not valid get function {get_fn_name} in {flc}.")
            # Check that node type corresponds to one of the get function's types
            node_type = property_element["type"]
            if node_type not in get_function.types:
                flc = self.parser_type.get_file_line_column(property_element, "type")
                raise Exception(
                    f"{node_type.name} is not a valid type for get function {get_function.name} in {flc}")
        # set function
        if "set" in property_element:
            set_fn_name = property_element["set"]
            try:
                set_function = self.superdict.sets[set_fn_name]
                property_element["set"] = set_function
            except KeyError:
                flc = self.parser_type.get_file_line_column(property_element, "set")
                raise Exception(f"Not valid set function {set_fn_name} in {flc}.")
            # Check that node type corresponds to one of the set function's types
            node_type = property_element["type"]
            if node_type not in set_function.types:
                flc = self.parser_type.get_file_line_column(property_element, "type")
                raise Exception(
                    f"{node_type.name} is not a valid type for set function {set_function.name} in {flc}")

        # rename class_symlist to class_symlist_str
        # as class_symlist will contain a reference to Symlist, done when parsing classes

        # if 'class_symlist' in property_element:
        #     property_element['class_symlist_str'] = property_element.pop('class_symlist')



        # lets create a Property object
        try:
            prop = Property(**property_element, yaml_node=property_element,
                            children=[],  # empty children for now
                            parent=parent_property
                            )
        except ValidationError as e:
            flc = self.parser_type.get_file_line_column(property_element)
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # parse children

        for child in children:
            prop.children.append(self._parse_property(child, prop))

        # extra validation with children

        # set number of elements for parent properties
        if prop.type.name == "PARENT":
            if not prop.children or not prop.children[0].dynamic:
                # property has no children or first child is not dynamic
                if prop.numels is not None:
                    flc = self.parser_type.get_file_line_column(property_element)
                    raise Exception(f"Number of elements should not be specified for parent properties in {flc}")
                if prop.children:
                    full_name_lc = prop.full_name.lower().replace('.', '_')
                    prop.numels = f"FGC_N_ELS(prop_{full_name_lc})"
                else:
                    prop.numels = 0
            else:
                prop.numels = 0

        # If INDIRECT_N_ELS flag is present, check that values of all numels attributes are references
        if "INDIRECT_N_ELS" in prop.flags:
            # flags to check
            numels_list = list(prop.specific_numels.values())
            if prop.numels is not None:
                numels_list.append(prop.numels)
            if not numels_list:
                flc = self.parser_type.get_file_line_column(property_element)
                raise Exception(f"INDIRECT_N_ELS flag present, but no numels attribute defined in {flc}")
            for numels in numels_list:
                if not self._is_reference(numels):
                    flc = self.parser_type.get_file_line_column(property_element)
                    raise Exception(f"INDIRECT_N_ELS flag present, but value of numels attribute {numels} is not a reference in {flc}")

        # numels

        for pl_cl, numels in prop.specific_numels.items():
            # Case 1: numels is a reference. Check that INDIRECT_N_ELS flag is set.
            if self._is_reference(numels):
                if "INDIRECT_N_ELS" not in prop.flags:
                    # check if INDIRECT_N_ELS is defined in specific flags
                    # TODO: check this logic after getting rid of platform specific attributes
                    #  and flags handling upgrade
                    # Possibly only specific flags here should be checked (if they are defined), not the global ones
                    if pl_cl not in prop.specific_flags or "INDIRECT_N_ELS" not in prop.specific_flags[pl_cl]:
                        flc = self.parser_type.get_file_line_column(property_element)
                        raise Exception(f"INDIRECT_N_ELS flag must be set when numels is a reference in {flc}")
            else:
                # Case 2: numels is a constant
                if pl_cl not in prop.specific_maxels:
                    # maxels not defined - set from numels
                    prop.specific_maxels[pl_cl] = numels
        # maxels
        for pl_cl, maxels in prop.specific_maxels.items():
            if pl_cl not in prop.specific_numels:
                prop.specific_numels[pl_cl] = "0"

        # global numels and maxels

        if prop.numels is not None:
            if self._is_reference(prop.numels):
                if "INDIRECT_N_ELS" not in prop.flags:
                    flc = self.parser_type.get_file_line_column(property_element)
                    raise Exception(f"INDIRECT_N_ELS flag must be set when numels is a reference in {flc}")
            else:
                if prop.maxels is None:
                    prop.maxels = prop.numels

        # maxels
        if prop.maxels is not None:
            if prop.numels is None:
                prop.numels = "0"

        # Set the PPM flag for direct parents of PPM leaf properties
        # if the class flags are set, they take the precedence over global flags
        # we also need to check if PPM is set for class specific flag, and then propagate it to the parent
        if prop.type.name != "PARENT":
            if "PPM" in prop.flags:

                if prop.parent is None:
                    flc = self.parser_type.get_file_line_column(property_element)
                    raise Exception(f"Property type != PARENT but it has no parent! {flc}")

                prop.parent.flags["PPM"] = self.superdict.flags["PPM"]

            for cl, cl_flags in prop.specific_flags.items():
                if "PPM" in cl_flags:
                    if prop.parent is None:
                        flc = self.parser_type.get_file_line_column(property_element)
                        raise Exception(f"Property type != PARENT but it has no parent! {flc}")

                    # check if parent has class specific flags
                    if cl in prop.parent.specific_flags:
                        # add PPM to parent's class specific flags
                        prop.parent.specific_flags[cl]["PPM"] = self.superdict.flags["PPM"]
                    else:
                        # parent doesn't have class specific flags
                        # copy global flags into class specific and add PPM
                        prop.parent.specific_flags[cl] = {}
                        for flag_name, flag in prop.parent.flags.items():
                            prop.parent.specific_flags[cl][flag_name] = flag
                        prop.parent.specific_flags[cl]["PPM"] = self.superdict.flags["PPM"]

        # If numels is still undefined, set it to zero. There are a few cases where numels should be zero:
        # NULL properties and properties which trigger a function rather than reading from a data structure.
        if prop.numels is None:
            prop.numels = "0"
            prop.maxels = "0"

        # add to SuperDict
        self.superdict.properties[prop.full_name] = prop
        # return property
        return prop

    def _parse(self):
        for property_file in self.files:
            parser = self.parser_type(yaml_file=property_file, base_path=self.src_path)
            data = parser.data

            # here we should have only files with a "properties" as root
            try:
                properties = data["properties"]
                root = properties["children"][0]["property"]
            except Exception:
                raise Exception(f"Failed to parse file {property_file} - incorrect format.")
            try:
                self._parse_property(root, None)
            except Exception:
                logging.error(f"Failed to parse file {property_file}")
                raise
