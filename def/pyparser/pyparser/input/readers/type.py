from fgc_parser_schemas import Type
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class TypesReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'types.yaml',
                         parser_type=parser_type)
        self.mandatory_attributes = ['title', 'name', 'size', 'doc']

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["types"]):
            element = element["type"]
            # tests
            flc = self.parser.get_file_line_column(element)

            try:
                type_el = Type(**element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            if type_el.name in self.superdict.types:
                raise Exception(f"Type {type_el.name} is not unique! {flc}")

            self.superdict.types[type_el.name] = type_el
