from typing import Optional

from fgc_parser_schemas import SuperDict, System, SystemComponent, SystemComponentGroup, LogMenu, Dim
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class SystemsReader(BaseReader):
    def __init__(self, src_path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)
        self.files = (self.src_path / 'systems').glob("*.yaml")
        self.group_index = 0

        self.dim_names = []

    def _parse(self):
        for system_file in self.files:
            parser = self.parser_type(yaml_file=system_file, base_path=self.src_path)
            data = parser.data
            try:
                element = data["system"]
            except KeyError:
                raise Exception(f"No system node in {system_file}!")

            element_children = element.pop("children", [])
            if element_children is None:
                element_children = []

            if not element_children:
                raise Exception(f"System {system_file} should have children!")

            flc = parser.get_file_line_column(element)

            # get the system type
            system_type = system_file.stem.upper()

            # lets create System object
            try:
                system = System(**element, yaml_node=element,
                                type=system_type,
                                # empty for now:
                                components={},
                                groups={},
                                log_menus=[],
                                dims={},
                                )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            if system.comp not in self.superdict.components:
                raise Exception(f"The component {system.comp} is not defined in components.yaml; {flc}")

            system.label = self.superdict.component_labels[system.comp].label

            # reset group index
            self.group_index = 1

            # reset dim names
            self.dim_names = []

            # iterate over children

            for child in element_children:
                child_flc = parser.get_file_line_column(child)
                if "include" in child:
                    self._handle_include(child, system)
                elif "group" in child:
                    self._handle_group(child, system)
                elif "component" in child:
                    self._handle_component(child, system)
                else:
                    raise Exception(f"Unknown child when parsing {child_flc}")

            self.superdict.systems[system.type] = system

    def _handle_include(self, include_element, system: System):
        flc = self.parser_type.get_file_line_column(include_element)
        # included file
        try:
            inc_data = include_element["data"]
        except KeyError:
            raise Exception(f"Error when parsing include {flc}")

        if "components" in inc_data:
            self._handle_components(inc_data, system)
        elif "dims" in inc_data:
            self._handle_dims(inc_data, system)
        elif "log_menus" in inc_data:
            self._handle_log_menus(inc_data, system)
        else:
            raise Exception(f"Error when parsing include {self.parser_type.get_file_line_column(inc_data)}")

    def _handle_components(self, components_element, system: System):
        flc = self.parser_type.get_file_line_column(components_element)
        components_element = components_element["components"]

        if not components_element:
            raise Exception(f"components is empty in {flc}")

        for child in components_element:
            # iterate over children
            child_flc = self.parser_type.get_file_line_column(child)
            if "component" in child:
                self._handle_component(child, system)
            elif "group" in child:
                self._handle_group(child, system)
            else:
                raise Exception(f"Unknown child when parsing {child_flc}")

    def _handle_dim(self, dim_element, system: System):
        dim_element = dim_element["dim"]
        flc = self.parser_type.get_file_line_column(dim_element)
        # fix hex bus_addr
        try:
            if not isinstance(dim_element["bus_addr"], int):
                dim_element["bus_addr"] = int(str(dim_element["bus_addr"]), 16)
        except (KeyError, ValueError):
            # let it be treated below
            pass
        try:
            dim = Dim(**dim_element,
                      yaml_node=dim_element,
                      name_idx=-1  # set later
                      )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # check that bus_addr, name and dim_idx are unique
        for d in system.dims.values():
            if d.bus_addr == dim.bus_addr:
                raise Exception(f"bus_addr is not unique in {flc}")
            if d.name == dim.name:
                raise Exception(f"name is not unique in {flc}")
        if dim.dim_idx in system.dims:
            raise Exception(f"dim_idx {dim.dim_idx} is not unique in {flc}")

        # check that DIM name is known and unique for this system
        if dim.name not in self.superdict.dim_names:
            raise Exception(f"Unknown dim name {dim.name} in {flc} (consult def/src/dim_names.xml)")
        if dim.name in self.dim_names:
            raise Exception(f"DIM names must be unique: {flc}")
        self.dim_names.append(dim.name)

        dim.name_idx = self.superdict.dim_names[dim.name].index

        # check that DIM type is known
        if dim.full_type not in self.superdict.dim_types:
            raise Exception(f"Unknown DIM type: {dim.full_type} in {flc}")

        # assign DimType
        dim.dim_type = self.superdict.dim_types[dim.full_type]

        # add to parent
        system.dims[dim.dim_idx] = dim

    def _handle_dims(self, dims_element, system: System):
        flc = self.parser_type.get_file_line_column(dims_element)
        dims_element = dims_element["dims"]

        # can have empty dims
        if not dims_element:
            return

        for child in dims_element:
            if "dim" in child:
                self._handle_dim(child, system)
            else:
                raise Exception(f"Error when parsing dims element {flc}")

    def _handle_log_menus(self, log_menus_element, system: System):
        flc = self.parser_type.get_file_line_column(log_menus_element)
        log_menus_element = log_menus_element["log_menus"]
        if not log_menus_element:
            raise Exception(f"log_menus element should have children! in {flc}")

        for child in log_menus_element:
            if "log_menu" in child:
                self._handle_log_menu(child, system)
            else:
                raise Exception(f"Error when parsing log_menus {flc}")

    def _handle_log_menu(self, log_menu_element, system: System):
        log_menu_element = log_menu_element["log_menu"]
        flc = self.parser_type.get_file_line_column(log_menu_element)
        try:
            log_menu = LogMenu(**log_menu_element, yaml_node=log_menu_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        system.log_menus.append(log_menu)

        # set indicies
        try:
            log_menu.log_menu_name_idx = self.superdict.log_menu_names[log_menu.name]
        except KeyError:
            # log menu not in superdict
            self.superdict.log_menu_names[log_menu.name] = len(self.superdict.log_menu_names)
            log_menu.log_menu_name_idx = self.superdict.log_menu_names[log_menu.name]

        try:
            log_menu.log_menu_prop_idx = self.superdict.log_menu_props[log_menu.prop]
        except KeyError:
            # log menu not in superdict
            self.superdict.log_menu_props[log_menu.prop] = len(self.superdict.log_menu_props)
            log_menu.log_menu_prop_idx = self.superdict.log_menu_props[log_menu.prop]

    def _handle_component(self, component_element, system: System, group: Optional[SystemComponentGroup] = None) -> SystemComponent:
        flc = self.parser_type.get_file_line_column(component_element)
        component_element = component_element["component"]
        try:
            sys_component = SystemComponent(**component_element, yaml_node=component_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # Check whether component is of a valid type
        try:
            component = self.superdict.components[sys_component.barcode]
        except KeyError:
            raise Exception(f"Component type {sys_component.barcode} is not defined: {flc}")

        # Check whether a component of this type is already defined for the system
        try:
            system_comp = system.components[sys_component.barcode]
            if system_comp.required is not None:
                system_comp.required += sys_component.required
            else:
                system_comp.required = sys_component.required
        except KeyError:
            system.components[sys_component.barcode] = sys_component

        # Check whether component's parent is a group
        if group is not None:
            sys_component.group = group
            # Check whether another system group is already defined for this component type
            if system.type in component.sys_groups and component.sys_groups[system.type] != sys_component.group.index:
                raise Exception(f"Component {component.barcode} cannot be a member of multiple groups in {flc}")
            component.sys_groups[system.type] = sys_component.group.index

        else:  # current node's parent is not a group
            # Component is a member of the individual group for its type

            # Set required count to 1 by default
            if sys_component.required is None:
                sys_component.required = 1

            # Check whether a group for this component type already exists
            if sys_component.barcode in system.groups:
                sys_component.group = system.groups[sys_component.barcode]
                sys_component.group.required += sys_component.required
            else:  # Create new group for component type
                new_group = SystemComponentGroup(index=self.group_index,
                                                 required=sys_component.required,
                                                 tag=sys_component.barcode,
                                                 yaml_node=None
                                                 )
                # increment group index
                self.group_index += 1
                sys_component.group = new_group
                system.groups[sys_component.barcode] = sys_component.group

                # Check whether another system group is already defined for this component type
                if system.type in component.sys_groups and component.sys_groups[system.type] != sys_component.group.index:
                    raise Exception(f"Component {sys_component.barcode} cannot be member of multiple groups {flc}")
                component.sys_groups[system.type] = sys_component.group.index

        sys_component.group.barcodes[sys_component.barcode] = 1
        # TODO: check if we need this dict or other structure should be here

        return sys_component

    def _handle_group(self, group_element, system: System) -> SystemComponentGroup:
        flc = self.parser_type.get_file_line_column(group_element)
        group_element = group_element["group"]

        # group can have children (components)

        children_element = group_element.pop("components", [])

        if children_element is None:
            children_element = []

        try:
            group = SystemComponentGroup(**group_element, yaml_node=group_element,
                                         index=-1)  # temp set to -1
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # Check whether group with tag already exists

        if group.tag in system.groups:
            system.groups[group.tag].required += group.required
            group = system.groups[group.tag]
        else:
            system.groups[group.tag] = group
            group.index = self.group_index
            # increment group index
            self.group_index += 1

        # handle children

        for child in children_element:
            child_flc = self.parser_type.get_file_line_column(child)
            if "component" in child:
                self._handle_component(child, system, group)
            else:
                raise Exception(f"Unknown child when parsing {child_flc}")

        return group
