import re
from pathlib import Path
from typing import Optional

from fgc_parser_schemas import SuperDict, DSP, Class, Area, Link, Const, Assert, Zone
from pydantic import ValidationError
from ruamel.yaml.comments import CommentedMap, CommentedSeq

from pyparser.input.readers.base_reader import BaseReader
from pyparser.input.readers.symlist import SymlistReader


class ClassReader(BaseReader):
    def __init__(self, src_path: Path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)

        self.classes_path = self.src_path / 'classes'
        self.classes = [cls for cls in self.classes_path.iterdir() if cls.is_dir()]

        # for validation
        self.class_names = {}
        # zone names
        self.zone_names = {}
        # next possible address for a zone
        self.next_possible_address = 0
        # zone data position
        self.zone_data_position = 0
        # parsed properties
        self.parsed_properties_with_parents = {}

    def _parse(self):
        for class_dir in self.classes:
            class_name = class_dir.stem
            self.logger.info(f"Parsing class {class_name} from {class_dir}")

            class_main = class_dir / "main.yaml"

            if not class_main.exists():
                raise Exception(f"{class_main} doesn't exist")

            # clear stuff
            # zone names
            self.zone_names = {}
            # next possible address for a zone
            self.next_possible_address = 0
            # zone data position
            self.zone_data_position = 0
            # parsed properties
            self.parsed_properties_with_parents = {}

            cls = self._parse_main_def(class_main, class_name)

            # Add reference to class in platform node
            cls.platform.classes[cls.id] = cls
            self.superdict.classes[cls.id] = cls

            # Check that the consts definition for the class exists
            const_file = class_dir / 'consts.yaml'
            if not const_file.exists():
                raise Exception(f"Consts definition file ({const_file}) for class ID {class_name} does not exist!")

            # parse consts and asserts
            self._parse_consts_file(const_file, cls)

            # parse symlists

            # symlist parser
            symlist_reader = SymlistReader(self.src_path, self.superdict, self.parser_type)

            symlist_files = (class_dir / 'symlists').glob("*.yaml")

            for symlist_file in symlist_files:
                symlist_name = symlist_file.stem.upper()
                cls.symlists[symlist_name] = symlist_reader.parse_file(symlist_file)

                symlist = cls.symlists[symlist_name]

                # add to superdict
                self.superdict.class_symlists.setdefault(symlist_name, dict())
                self.superdict.class_symlists[symlist_name][cls.id] = symlist

                if symlist_name in self.superdict.symlists:
                    raise Exception(f"Symlist {symlist_name} defined both globally and for class {cls.id}")

                # Add symlist name as prefix of constants in symlist
                for const in symlist.consts:
                    const.symbol = const.name
                    const.name = f"{symlist_name}_{const.symbol}"

            # Check that the properties definition for the class exists

            properties_file = class_dir / 'properties.yaml'
            if not properties_file.exists():
                raise Exception(f"Properties definition file for class {cls.id} doesn't exist!")

            self._parse_properties_file(properties_file, cls)

            # Parse the published data map for the class

            # clear zone names & address
            self.zone_names.clear()
            self.next_possible_address = 0

            pubdata_file = class_dir / 'pubdata.yaml'
            if pubdata_file.exists():
                cls.pubdata = self._parse_pubdata_file(pubdata_file, cls)

            # construct a dict of all symlist used by this class

            for property in cls.properties.values():
                if property.symlist:
                    cls.class_symlists[property.symlist.name] = property.symlist
                elif property.class_symlist:
                    cls.class_symlists[property.class_symlists[cls.id].name] = property.class_symlists[cls.id]

            # Check if all class symlists are being used
            for symlist_name in cls.symlists.keys():
                if symlist_name not in cls.class_symlists:
                    self.logger.warning(f"Class symlist {symlist_name} is not being used by class {cls.id}")

            # Construct a dict of all constants within properties in this class

            # prepare regex
            pattern = re.compile(r'^\*(.*)\.(.*)_(.*?)$')

            for symlist in cls.class_symlists.values():
                for const in symlist.consts:
                    # Add const value to global constants if set
                    if const.value is not None:
                        if const.name in self.superdict.consts:
                            raise Exception(f"Constant {const.symbol} has values in both"
                                            f" symlist {symlist.name} and global consts")
                        elif const.name in cls.consts:
                            raise Exception(f"Constant {const.symbol} has values in both"
                                            f" symlist {symlist.name} and class {cls.id} consts")

                        match = pattern.match(const.value)
                        if match:
                            memmap_name = match.group(1)
                            memzone_name = match.group(2)
                            const_type = match.group(3)

                            # Locate memzone
                            memzone_tree = memzone_name.split('_')
                            memzone = cls.platform.memmaps[memmap_name]
                            found = False
                            for target_name in memzone_tree:
                                for child in memzone.children:
                                    if child.name == target_name:
                                        memzone = child
                                        found = True
                                        break
                            if not found:
                                raise Exception(f"Memzone {memzone_name} not found for class {cls.id} "
                                                f"symlist {symlist.name} "
                                                f"const {const.symbol}")

                            # fix consts based on type
                            if const_type == "16BIT":
                                # Truncate address to 16 bits
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=hex(memzone.address / 8)[:-4])
                            elif const_type == "32BIT":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.address / 8)
                            elif const_type == "24BIT":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.address / 8)
                            elif const_type == "PAGE":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.address / 524288)
                            elif const_type == "MASK8":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.mask)
                            elif const_type == "MASK16":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.mask)
                            elif const_type == "MASK32":
                                cls.consts[const.name] = Const(name=const.name,
                                                               val=memzone.mask)
                            else:
                                raise Exception(f"Invalid const type {const_type} for "
                                                f"symlist {symlist.name} "
                                                f"symbol {const.symbol}")
                        else:
                            cls.consts[const.name] = Const(name=const.name, val=const.value, yaml_node=None)
                    # Set value symbol table constant to global constant value if defined, otherwise zero
                    if const.name in self.superdict.consts:
                        value = self.superdict.consts[const.name].val
                    elif const.name in cls.consts:
                        value = cls.consts[const.name].val
                    else:
                        value = 0
                    cls.symtab_consts[const.name] = const.copy()
                    # update the value
                    cls.symtab_consts[const.name].value = value

            # Check that all symlist constants are defined
            for const in cls.symtab_consts.values():
                if const.name not in cls.consts and const.name not in self.superdict.consts:
                    raise Exception(f"Value for const {const.name} not defined in class {cls.id}")

    def _parse_pubdata_file(self, pubdata_file: Path, cls: Class) -> Zone:
        parser = self.parser_type(pubdata_file, self.src_path)
        data = parser.data

        if 'zone' not in data:
            raise Exception(f"Error when parsing {pubdata_file} - missing 'zone' top level element")

        zone_element = data['zone']
        return self._handle_zone_element(zone_element, None, cls)

    def _handle_zone_element(self, zone_element, parent: Optional[Zone], cls: Class) -> Zone:
        flc = self.parser_type.get_file_line_column(zone_element)
        # get children - 'zones'
        zones = zone_element.pop('zones', [])

        # check if it has property - and convert to Property element
        # check if the property is defined for this class
        if 'property' in zone_element:
            prop_name = zone_element['property']
            try:
                # zone_element['property'] = self.superdict.properties[prop_name]
                zone_element['property'] = cls.properties[prop_name]
            except KeyError:
                raise Exception(f"Property {prop_name} is not valid in {flc}")

        # create Zone object
        try:
            zone = Zone(**zone_element, yaml_node=zone_element,
                        parent=parent,
                        cls=cls,
                        data_position=0,    # TBD later
                        c_offset=0,         # TBD later
                        numbits=0,          # TBD later
                        )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # capitalize name
        zone.name = zone.name.upper()

        # check for duplicate full name
        if zone.full_name:
            if zone.full_name in self.zone_names:
                raise Exception(f"Zone name {zone.full_name} is duplicated in {flc}")
            else:
                self.zone_names[zone.full_name] = True

        # calculate offset

        memsizes = {
            'b': 1,  # bit
            'B': 8,  # byte
            'K': 8192,  # kilobyte
            'M': 8388608,  # megabyte
            'G': 8589934592  # gigabyte
        }

        offset = zone.offset
        memsize = offset[-1]
        if memsize not in memsizes:
            raise Exception(f"Invalid offset unit character of zone {zone.name} in {flc}")

        try:
            offset = int(offset[:-1], 0)
        except ValueError:
            raise Exception(f"Invalid offset of zone {zone.name} in {flc}")
        zone.c_offset = offset * memsizes[memsize]

        # check that the address doesn't overlap with previous node
        if zone.address < self.next_possible_address:
            raise Exception(f"Zone overlap in {flc}")

        # Check that size is validly formatted and get number of bits
        # calculate size (in bits)
        size = zone.size
        memsize = size[-1]
        if memsize not in memsizes:
            raise Exception(f"Invalid size unit character of zone {zone.name} in {flc}")

        try:
            size = int(size[:-1], 0)
        except ValueError:
            raise Exception(f"Invalid size of zone {zone.name} in {flc}")
        zone.numbits = size * memsizes[memsize]

        # Check that this node does not overrun the end of its parent
        if zone.parent and (zone.c_offset + zone.numbits) > zone.parent.numbits:
            raise Exception(f"Zone {zone.name} overruns the end of its parent {zone.parent.name} in {flc}")

        # parse children
        for child in zones:
            if 'zone' in child:
                child_zone = self._handle_zone_element(child['zone'], zone, cls)
                zone.children.append(child_zone)
            else:
                child_flc = self.parser_type.get_file_line_column(child)
                raise Exception(f"Unknown element in {child_flc} - expected 'zone'")

        # If this is a bottom-level node (has no children) and
        # is not also the top-level node (it has a parent), then it must have a valid property attribute
        if not zone.children and zone.parent:
            # check that property is defined
            if not zone.property:
                raise Exception(f"Property not defined in {flc}")

            # check that the property is of allowed type
            property_allowed_types = ('INT8U',
                                      'INT8S',
                                      'INT16U',
                                      'INT16S',
                                      'INT32U',
                                      'INT32S',
                                      'FLOAT')

            if zone.property.type.name not in property_allowed_types:
                raise Exception(f"Property {zone.property.name} is not of allowed type "
                                f"({zone.property.type.name}) in {flc}")

            # Check that the size of the zone matches that of the property's type
            property_numbits = int(zone.property.type.size) * 8
            if zone.numbits != property_numbits:
                raise Exception(f"Size of zone {zone.name} ({zone.numbits}) does not match that of property"
                                f" {zone.property.name} ({property_numbits}) in {flc}")

            # add position with data
            zone.data_position = self.zone_data_position
            self.zone_data_position += 1
        else:
            # Check that property is not defined
            if zone.property:
                raise Exception(f"Zone may not have a property defined in {flc}")
            # Zone does not directly contain a property, set data position to -1 to indicate this
            zone.data_position = -1

        self.next_possible_address = zone.address + zone.numbits

        return zone

    def _parse_properties_file(self, properties_file: Path, cls: Class):
        parser = self.parser_type(properties_file, self.src_path)
        data = parser.data

        # make sure it has 'properties' node
        if 'properties' not in data:
            raise Exception(f"Properties file for class {cls.id} must have 'properties' root node")

        # parse 'includes'
        if 'includes' in data:
            includes_obj = data['includes']
            if includes_obj is None:
                includes_obj = []
            for include in includes_obj:
                if 'include' in include:
                    path = self.src_path / include['include']
                    self._parse_properties_file(path, cls)
                else:
                    raise Exception(f"Unknown element in includes of {properties_file}. Expected 'include'.")

        properties_element = data['properties']
        flc = parser.get_file_line_column(properties_element)

        # to store properties
        properties = {}

        # to store pars functions
        pars_functions = {}

        # validate properties
        for property_name in properties_element:
            # Check that property exists within defined properties
            try:
                property = self.superdict.properties[property_name]
            except KeyError:
                raise Exception(f"Property {property_name} is not a valid property in {flc}")
            # Check whether property has already been added to the class (duplicated in the file)
            if property_name in properties:
                raise Exception(f"Property {property_name} is duplicated in {flc}")

            property.classes[cls.id] = cls

            # Check whether property has a class-specific symlist
            if property.class_symlist:
                # check if symlist exists for this class
                try:
                    symlist = cls.symlists[property.class_symlist]
                except KeyError:
                    raise Exception(f"Missing class symlist {property.class_symlist} "
                                    f"required for property {property.full_name} in {flc}")
                property.class_symlists[cls.id] = symlist

            # Check whether property is for a DSP
            if cls.id in property.specific_dsp:
                dsp = property.specific_dsp[cls.id]
            else:
                dsp = None

            if dsp is not None:
                if dsp not in cls.dsps:
                    # add DSP to class' DSPs
                    cls.dsps[dsp] = DSP(name=dsp, properties={}, property_idxs={}, yaml_node=None)
                dsp_node = cls.dsps[dsp]
                # add property to DSP properties
                dsp_node.properties[property.full_name] = property
                dsp_node.property_idxs[property.full_name] = len(dsp_node.property_idxs)

            # Define pars function

            if cls.id in property.specific_pars:
                pars_functions[property.specific_pars[cls.id]] = True
            elif property.pars:
                pars_functions[property.pars] = True

            # Class and platform specific flags

            # copy from specific flags
            if not property.class_flags:
                property.class_flags = property.specific_flags.copy()

            if cls.id not in property.class_flags:
                property.class_flags[cls.id] = property.flags.copy()

            if property.symlist or property.class_symlist:
                property.class_flags.setdefault(cls.id, {})['SYM_LST'] = self.superdict.flags['SYM_LST']

            if property.limits:
                # min and max are checked that are defined - in property reader
                property.class_flags.setdefault(cls.id, {})['LIMITS'] = self.superdict.flags['LIMITS']

            # Add property and its parents to this class's properties
            prop = property
            while prop is not None:
                # see if already added
                if prop.full_name not in self.parsed_properties_with_parents:
                    self.parsed_properties_with_parents[prop.full_name] = True
                    properties[prop.full_name] = True
                    # add property to class' property array
                    cls.properties[prop.full_name] = prop

                    # add reference to this class within property's dict of classes
                    prop.classes[cls.id] = cls

                    # add property to this class' level array
                    cls.property_levels.setdefault(prop.level, list()).append(prop)
                # check parent
                prop = prop.parent

        cls.parsfunctions = sorted(pars_functions.keys())

    def _parse_consts_file(self, consts_file: Path, cls: Class):
        parser = self.parser_type(consts_file, self.src_path)
        data = parser.data

        # main node is 'consts'

        if 'consts' not in data:
            raise Exception(f"Consts file should have top level 'consts' element in {consts_file}")

        consts_element = data['consts']
        self._handle_consts_element(consts_element, cls)

    def _handle_consts_element(self, consts_element, cls: Class):
        flc = self.parser_type.get_file_line_column(consts_element)
        # it can be either a sequence, or a mapping (dictionary)
        if isinstance(consts_element, (CommentedSeq, list)):
            for element in consts_element:
                # it can be either 'include' or 'consts' or 'asserts'
                if 'include' in element:
                    data = element['data']
                    # need to have 'consts' as root
                    if 'consts' not in data:
                        raise Exception(f"No consts element in {element['include']}")
                    self._handle_consts_element(data['consts'], cls)
                elif 'consts' in element:
                    self._handle_consts_element(element['consts'], cls)
                elif 'asserts' in element:
                    self._handle_asserts_dict(element['asserts'], cls)
                else:
                    raise Exception(f"Unknown element in {self.parser_type.get_file_line_column(consts_element, element)}")
        elif isinstance(consts_element, (CommentedMap, dict)):
            # constants set directly
            self._handle_consts_dict(consts_element, cls)
        else:
            raise Exception(f"Not valid consts element syntax! {flc}")

    def _handle_consts_dict(self, consts_element, cls: Class):
        flc = self.parser_type.get_file_line_column(consts_element)
        for const_name, const_value in consts_element.items():
            # check if not duplicated
            if const_name in cls.consts:
                raise Exception(f"const {const_name} already defined for this class! {flc}")
            # Check if not already defined globally
            if const_name in self.superdict.consts:
                raise Exception(f"const {const_name} already defined globally! {flc}")

            # assign
            try:
                const = Const(name=const_name, val=const_value, yaml_node=consts_element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            cls.consts[const_name] = const

            if re.match(r'^\*(.*)\.(.*)_(.*?)$', const.name):
                #TODO see if it is needed - implementation in parser/Input/Class/Consts.pl ~36l
                pass

    def _handle_asserts_dict(self, asserts_element, cls: Class):
        for assert_element in asserts_element:
            flc = self.parser_type.get_file_line_column(asserts_element)
            if 'assert' not in assert_element:
                raise Exception(f"Asserts element should have 'assert' inside {flc}")
            # update flc
            flc = self.parser_type.get_file_line_column(assert_element)
            assert_element = assert_element['assert']
            # assign
            try:
                ass = Assert(**assert_element, yaml_node=assert_element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            cls.asserts.append(ass)

    def _parse_main_def(self, main_file: Path, class_id) -> Class:
        parser = self.parser_type(main_file, self.src_path)
        data = parser.data

        # get main element
        if "class" not in data:
            raise Exception(f"class element not in {main_file}")
        class_element = data["class"]
        flc = parser.get_file_line_column(class_element)

        # get main area
        children = class_element.pop("children", [])
        # children are optional
        # if not children:
        #     raise Exception(f"When parsing main file: children missing in {flc}")

        # get platform
        if "platform" in class_element:
            try:
                platform = self.superdict.platforms[class_element["platform"]]
            except KeyError:
                raise Exception(f"Platform not found in class {flc}")
            class_element["platform"] = platform
        # create Class
        try:
            cls = Class(**class_element, yaml_node=class_element,
                        id=class_id,
                        consts={},
                        symlists={},
                        asserts=[],
                        )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # check if name is unique
        if cls.name in self.class_names:
            raise Exception(f"Class name {cls.name} is not unique! {flc}")
        self.class_names[cls.name] = True

        # there should be single area in children if they are there
        # TODO: perhaps change translator and make it same to existing area/links translators

        if children:
            try:
                main_area_element = children[0]["area"]
            except (KeyError, IndexError):
                raise Exception(f"Missing main area element in {flc}")

            cls.sitemap = self._parse_area(main_area_element, top_level=True)

        return cls

    def _parse_area(self, area_element, top_level=False) -> Area:
        # flc
        flc = self.parser_type.get_file_line_column(area_element)

        # if top level, add empty name attribute as it is mandatory but can be empty there
        if top_level:
            area_element["name"] = ""

        # get children
        children = area_element.pop("children", [])

        areas = []
        links = []
        if children:
            for child in children:
                if "area" in child:
                    areas.append(self._parse_area(child["area"]))
                elif "link" in child:
                    links.append(self._parse_link(child["link"]))
                else:
                    raise Exception(f"Unknown child in {flc}")

        try:
            area = Area(**area_element, yaml_node=area_element,
                        areas=areas,
                        links=links)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return area

    def _parse_link(self, link_element) -> Link:
        # flc
        flc = self.parser_type.get_file_line_column(link_element)
        try:
            link = Link(**link_element, yaml_node=link_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return link
