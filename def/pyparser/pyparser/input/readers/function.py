from fgc_parser_schemas import GetFunction, SetFunction, SetIfFunction
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class FunctionReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type, file):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / file,
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        if "gets" in data:
            func_type, key, tag = GetFunction, "gets", "get"
        elif "sets" in data:
            func_type, key, tag = SetFunction, "sets", "set"
        elif "setifs" in data:
            func_type, key, tag = SetIfFunction, "setifs", "setif"
        else:
            raise Exception(f"File {self.parser.input_file} doesn't have any of gets/sets/setifs")

        for idx, element in enumerate(data[key]):
            element = element[tag]

            # tests
            flc = self.parser.get_file_line_column(element)

            # if it's get or set, types are mandatory
            if "types" in element:
                types_list = element["types"].split()
                # replace with list of Type object
                element["types"] = []
                for type_el in types_list:
                    try:
                        element["types"].append(self.superdict.types[type_el])
                    except KeyError:
                        raise Exception(f"{type_el} is not a valid type: {flc}")

            elif func_type in (GetFunction, SetFunction):
                raise Exception(f"{tag} function {element['name']} has no types: {flc}")

            # getoptions
            if "getopts" in element:
                getoptions = element["getopts"].split()
                element["getopts"] = []
                for getoption in getoptions:
                    try:
                        element["getopts"].append(self.superdict.getoptions[getoption])
                    except KeyError:
                        raise Exception(f"{getoption} is not a valid get option: {flc}")

            elif func_type == GetFunction:
                raise Exception(f"{tag} function {element['name']} has no get options: {flc}")

            try:
                func_element = func_type(**element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            if func_element.name in getattr(self.superdict, key):
                raise Exception(f"{tag} name {func_element.name} is not unique! {flc}")

            getattr(self.superdict, key)[func_element.name] = func_element
