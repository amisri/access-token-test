from fgc_parser_schemas import Flag
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class FlagsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'flags.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["flags"]):
            element = element["flag"]
            # tests
            flc = self.parser.get_file_line_column(element)
            try:
                flag = Flag(**element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            # duplicates
            if flag.name in self.superdict.flags:
                raise Exception(f"Flag name {flag.name} is not unique! {flc}")
            for el in self.superdict.flags.values():
                if el.value == flag.value:
                    raise Exception(f"Flag value {el.value} is not unique! {flc}")

            self.superdict.flags[flag.name] = flag
