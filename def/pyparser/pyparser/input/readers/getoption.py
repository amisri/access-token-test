from fgc_parser_schemas import GetOption
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class GetOptionsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'getoptions.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        bitmask = 0x0001
        max_getoptions = 16

        if len(data["getoptions"]) > max_getoptions:
            raise Exception(f"Too many getoptions - maximum =  {max_getoptions}")

        for idx, element in enumerate(data["getoptions"]):
            element = element["getoption"]
            # tests
            flc = self.parser.get_file_line_column(element)
            try:
                getopt = GetOption(**element, bitmask=bitmask, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            # duplicates
            if getopt.symbol in self.superdict.getoptions:
                raise Exception(f"Getoption symbol {getopt.symbol} is not unique! {flc}")

            # Double bitmask ready for next getoption
            bitmask *= 2

            self.superdict.getoptions[getopt.symbol] = getopt
