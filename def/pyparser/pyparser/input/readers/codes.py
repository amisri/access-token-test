from fgc_parser_schemas import Code
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class CodesReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'codes.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        attrib_matrix = {
            'class': 'class_id'
        }

        for expected_id, code_element in enumerate(data["codes"]):
            code_element = code_element["code"]
            fix_attribs = {}
            for k, v in code_element.items():
                if k in attrib_matrix:
                    fix_attribs[attrib_matrix[k]] = v
                else:
                    fix_attribs[k] = v

            # tests
            flc = self.parser.get_file_line_column(code_element)

            try:
                code = Code(**fix_attribs, yaml_node=code_element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            for c in self.superdict.codes:
                if c.full_name == code.full_name:
                    raise Exception(f"Code {code.full_name} is not unique! {flc}")

            # Check that ID matches expected
            if expected_id != code.id:
                raise Exception(f"Code ID {code.id} does not match expected ID {expected_id} in {flc}")

            self.superdict.codes.append(code)
