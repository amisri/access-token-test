from pathlib import Path
from typing import Optional, Type
import logging
from pyparser.input.parsers.yaml import BaseYamlParser
from fgc_parser_schemas import SuperDict


class BaseReader:
    def __init__(self, src_path: Path,
                 superdict: SuperDict,
                 parser_type: Type[BaseYamlParser],
                 yaml_file: Optional[Path] = None,
                 *args,
                 **kwargs):
        self.src_path = src_path
        self.superdict = superdict
        self.parser_type = parser_type
        if yaml_file:
            self.parser = self.parser_type(yaml_file, src_path)

        self.logger = logging.getLogger(f"parser.{self.__class__.__name__}")

    def _parse(self):
        pass

    def parse(self):
        self.logger.info(f"Started parsing")
        self._parse()
        self.logger.info(f"Finished parsing")
