from fgc_parser_schemas import ComponentLabel
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class ComponentLabelsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type,
                         yaml_file=src_path / 'component_labels.yaml')

    def _parse(self):
        data = self.parser.data

        for component_element in data["component_labels"]:
            component_element = component_element["component_label"]

            # tests
            flc = self.parser.get_file_line_column(component_element)

            try:
                component = ComponentLabel(**component_element, yaml_node=component_element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            if component.barcode in self.superdict.component_labels:
                raise Exception(f"Component label with barcode {component.barcode} is not unique! {flc}")

            # if component with such barcode doesn't exist, dont put it in the
            try:
                self.superdict.components[component.barcode].label = component.label
            except KeyError:
                pass

            self.superdict.component_labels[component.barcode] = component

        # check that all components have labels
        for comp in self.superdict.components.values():
            if not comp.label:
                raise Exception(f"Component {comp.barcode} doesn't have a label defined")
