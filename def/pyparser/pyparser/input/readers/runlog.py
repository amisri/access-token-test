from fgc_parser_schemas import Runlog
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class RunlogReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'runlog.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["runlog"]):

            element = element["entry"]
            # tests
            flc = self.parser.get_file_line_column(element)
            try:
                entry = Runlog(**element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            # duplicates
            if entry.id in self.superdict.runlog:
                raise Exception(f"Runlog entry name {entry.id} is not unique! {flc}")

            # add RL_ prefix
            entry.id = f"RL_{entry.id}"

            self.superdict.runlog[entry.id] = entry
