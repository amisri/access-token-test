from fgc_parser_schemas import Const
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class ConstsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type,
                         yaml_file=src_path / 'consts.yaml')

    def _parse(self):
        data = self.parser.data

        for k, v in data["consts"].items():

            # tests
            flc = self.parser.get_file_line_column(data["consts"], item=k)

            try:
                const = Const(name=k, val=v, yaml_node=data)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            if const.name in self.superdict.consts:
                raise Exception(f"Const name {const.name} is not unique! {flc}")

            self.superdict.consts[const.name] = const
