import re
import struct
from pathlib import Path
from typing import List, Optional

from fgc_parser_schemas import (SuperDict, Platform, Area, Link, HW,
                                XilinxProgram, XilinxRevision, Xilinx, Connector, Pin,
                                BootMenu, BootMenuReturn, BootMenuArg,
                                MemoryMap)
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class PlatformReader(BaseReader):
    def __init__(self, src_path: Path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)

        self.platforms_path = self.src_path / 'platforms'
        self.platforms = [cls for cls in self.platforms_path.iterdir() if cls.is_dir()]

        # some dictionaries for testing uniqness
        self.lhc_ids = {}
        self.epc_ids = {}

        self.menu_cnames = {}

        self.memmaps_names = {}

        self.memmap_origin = None

    def _parse(self):
        for platform_dir in self.platforms:
            platform_name = platform_dir.stem
            self.logger.info(f"Parsing platform {platform_name} from {platform_dir}")

            # main definition should be there
            main_file = platform_dir / "main.yaml"
            if not main_file.exists():
                raise Exception(f"{main_file} doesn't exist!")
            platform = self._parse_main_def(main_file, platform_name)

            # parse constants if they are there
            consts_file = platform_dir / "consts.yaml"
            if consts_file.exists():
                self._parse_consts(consts_file)

            # parse HW

            # get a list of HW dirs
            hardware_dirs = []
            platform_hw_dir = platform_dir / 'hw'
            if platform_hw_dir.exists():
                hardware_dirs.extend([x for x in platform_hw_dir.iterdir() if x.is_dir()])

            for hw_dir in hardware_dirs:
                hw_name = hw_dir.stem

                # main hw definition
                main_hw_file = hw_dir / "main.yaml"
                if not main_hw_file.exists():
                    raise Exception(f"{main_hw_file} doesn't exist!")

                hw = self._parse_main_hw(main_hw_file, platform)

                # check for duplicate HW labels (platform level)
                for el in platform.hardware.values():
                    if hw.label == el.label:
                        raise Exception(f"Duplicated label in {main_hw_file}")

                platform.hardware[hw_name] = hw

                # Parse xilinxes file if one exists
                xilinxes_file = hw_dir / "xilinxes.yaml"
                if xilinxes_file.exists():
                    hw.xilinxes = self._parse_xilinxes_file(xilinxes_file, hw)

                # Parse connectors file is one exists
                connectors_file = hw_dir / "connectors.yaml"
                if connectors_file.exists():
                    hw.connectors = self._parse_connectors_file(connectors_file)

            # Convert sub-hardware to references
            for hw in platform.hardware.values():
                if hw.subhw_str:  # may be None
                    for subhw in hw.subhw_str:
                        # let's see if it's defined
                        try:
                            hw.subhw.append(platform.hardware[subhw])
                        except KeyError:
                            raise Exception(f"Sub-hardware {subhw} doesn't exist for {platform_dir}/hw/{hw.label}")

            # Parse boot for device platforms (id >=50) (if exists)
            boot_menu_file = platform_dir / 'boot' / 'menu.yaml'
            if platform.id >= 50 and boot_menu_file.exists():
                platform.boot_menu = self._parse_boot_menu_file(boot_menu_file, platform=platform)
                # reset cnames dict
                self.menu_cnames.clear()

            # parse memmaps
            memmaps = platform_dir.glob("memmaps/*.yaml")

            for memmap in memmaps:
                memmap_name = memmap.stem.upper()
                platform.memmaps[memmap_name] = self._parse_memory_map_file(memmap, platform)

            # add to SD
            self.superdict.platforms[platform_name] = platform

    def _parse_memory_map_file(self, memory_map_file: Path, platform: Platform) -> MemoryMap:
        parser = self.parser_type(memory_map_file, self.src_path)
        data = parser.data

        # get root "zone" element
        if "zone" not in data:
            raise Exception(f"No root element zone in {memory_map_file}")

        zone_element = data["zone"]

        # clear before parsing
        self.memmaps_names.clear()

        return self._parse_zone_element(zone_element, None, platform)

    def _parse_zone_element(self, zone_element, parent: Optional[MemoryMap], platform: Platform) -> MemoryMap:
        flc = self.parser_type.get_file_line_column(zone_element)

        # get children
        children = zone_element.pop("children", [])

        # Default to parent's endianness if not defined, otherwise BIG
        if "endian" not in zone_element:
            if parent:
                zone_element["endian"] = parent.endian
            else:
                zone_element["endian"] = MemoryMap.Endian.BIG

        # parse consts
        zone_element["const_types"] = {}
        for const in zone_element.get("consts", "").split():
            # capitalize const
            zone_element["const_types"][str(const).upper()] = True
        # create MemoryMap object
        try:
            memmap = MemoryMap(**zone_element, yaml_node=zone_element,
                               platform=platform,
                               parent=parent,
                               children=[],  # empty for now
                               c_offset=0,   # TBD later
                               numbits=0,    # TBD later
                               )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # check full name uniqness

        if memmap.full_name in self.memmaps_names:
            raise Exception(f"Zone name {memmap.full_name} is duplicated! in {flc}")

        self.memmaps_names[memmap.full_name] = True

        # calculate offset

        memsizes = {
            'b': 1,          # bit
            'B': 8,          # byte
            'K': 8192,       # kilobyte
            'M': 8388608,    # megabyte
            'G': 8589934592  # gigabyte
        }

        offset = memmap.offset
        memsize = offset[-1]
        if memsize not in memsizes:
            raise Exception(f"Invalid offset unit character of zone {memmap.name} in {flc}")

        try:
            offset = int(offset[:-1], 0)
        except ValueError:
            raise Exception(f"Invalid offset of zone {memmap.name} in {flc}")
        memmap.c_offset = offset * memsizes[memsize]

        # calculate size (in bits)
        size = memmap.size
        memsize = size[-1]
        if memsize not in memsizes:
            raise Exception(f"Invalid size unit character of zone {memmap.name} in {flc}")

        try:
            size = int(size[:-1], 0)
        except ValueError:
            raise Exception(f"Invalid size of zone {memmap.name} in {flc}")
        memmap.numbits = size * memsizes[memsize]

        # Check that this node does not overrun the end of its parent
        if memmap.parent and (memmap.c_offset + memmap.numbits) > memmap.parent.numbits:
            raise Exception(f"Zone {memmap.name} overruns the end of its parent {memmap.parent.name} in {flc}")

        # Check whether the zone was set as an origin
        if memmap.origin:
            if self.memmap_origin is not None:
                raise Exception(f"Origin already defined as {hex(self.memmap_origin)}")
            self.memmap_origin = memmap.byte_address

        # Set the origin offset for the zone
        if self.memmap_origin is not None:
            memmap.origin_offset = memmap.byte_address - self.memmap_origin

        # Check whether type needs to be defined
        if not memmap.type:
            const_types_with_type = ('NEAR', 'FAR', 'ARRAY', 'ORIGIN_NEAR', 'ORIGIN_FAR', 'ORIGIN_ARRAY')
            for const_type in const_types_with_type:
                if const_type in memmap.const_types:
                    raise Exception(f"Zone {memmap.name} requires type to be defined {flc}")

        # Check for address byte alignment when 16BIT, 32BIT, ARRAY, NEAR or FAR constants requested
        if memmap.address % 8 != 0:
            aligned_types = ('PAGE', '16BIT', '32BIT', 'NEAR', 'FAR', 'ARRAY',
                             'ORIGIN_NEAR', 'ORIGIN_ARRAY', 'ORIGIN_FAR')
            for const_type in aligned_types:
                if const_type in memmap.const_types:
                    raise Exception(f"Zone {memmap.name} requires address to be byte aligned for constant in {flc}")
        # Create mask if necessary
        if 'MASK8' in memmap.const_types or 'MASK16' in memmap.const_types or 'MASK32' in memmap.const_types:
            memmap.mask = 0
            if 'MASK8' in memmap.const_types:
                mask_size = 8
            elif 'MASK16' in memmap.const_types:
                mask_size = 16
            else:  # MASK32
                mask_size = 32

            # Calculate mask
            offset = memmap.address % mask_size

            # Return error if mask is longer than limit
            if offset + memmap.numbits > mask_size:
                raise Exception(f"Mask length exceeds {mask_size} bit limit in {flc}")

            for i in range(0, memmap.numbits):
                memmap.mask += 2 ** (offset + i)

            if 'MASK8' in memmap.const_types:
                # create shift if necessary
                if 'SHIFT' in memmap.const_types:
                    memmap.shift = offset
            elif 'MASK16' in memmap.const_types:
                memmap.shift = offset

                # Swap bytes in mask and offset
                if memmap.endian == MemoryMap.Endian.BIG:
                    memmap.mask = struct.unpack(">H", struct.pack("<H", memmap.mask))[0]

                    if 'SHIFT' in memmap.const_types:
                        byte_swapped_shift = (8,  9, 10, 11, 12, 13, 14, 15,
                                              0,  1,  2,  3,  4,  5,  6,  7)
                        memmap.shift = byte_swapped_shift[offset]
            elif 'MASK32' in memmap.const_types:
                memmap.shift = offset
                # Swap bytes in mask and offset
                if memmap.endian == MemoryMap.Endian.BIG:
                    memmap.mask = struct.unpack(">L", struct.pack("<L", memmap.mask))[0]
                    if 'SHIFT' in memmap.const_types:
                        byte_swapped_shift = (24, 25, 26, 27, 28, 29, 30, 31,
                                              16, 17, 18, 19, 20, 21, 22, 23,
                                              8, 9, 10, 11, 12, 13, 14, 15,
                                              0, 1, 2, 3, 4, 5, 6, 7)
                        memmap.shift = byte_swapped_shift[offset]
        elif 'SHIFT' in memmap.const_types:
            raise Exception(f"Shift constant requested without mask in {flc}")

        # Check that the origin is set if an origin-based constant is requested

        if memmap.origin_offset is None and ('ORIGIN_NEAR' in memmap.const_types or
                                             'ORIGIN_FAR' in memmap.const_types or
                                             'ORIGIN_ARRAY' in memmap.const_types):
            raise Exception(f"No origin in defined, but an origin-based constant is requested in {flc}")

        # parse children
        for child in children:
            memmap.children.extend(self._handle_memmap_include(child, memmap, platform))

        # Calculate access rule from children's rules if not manually
        if not memmap.access:
            if memmap.children:
                r_flag = False
                w_flag = False
                for child in memmap.children:
                    if child is None:
                        print("OOPS")
                    if child.access == MemoryMap.Access.R:
                        r_flag = True
                    elif child.access == MemoryMap.Access.W:
                        w_flag = True
                    elif child.access == MemoryMap.Access.RW:
                        r_flag = w_flag = True

                    if r_flag and w_flag:
                        break

                # set access mode
                if r_flag:
                    if w_flag:
                        memmap.access = MemoryMap.Access.RW
                    else:
                        memmap.access = MemoryMap.Access.R
                else:
                    if w_flag:
                        memmap.access = MemoryMap.Access.W
                    else:
                        # keep it as None
                        memmap.access = None
            else:
                # Default to READ/WRITE
                memmap.access = MemoryMap.Access.RW

        # If this node is the origin, clear it
        if memmap.origin is not None:
            self.memmap_origin = None

        return memmap

    def _handle_memmap_include(self, child, parent: MemoryMap, platform: Platform) -> List[MemoryMap]:
        flc = self.parser_type.get_file_line_column(child)
        if 'zone' in child:
            return [self._parse_zone_element(child['zone'], parent, platform)]
        elif 'include' in child:
            data = child["data"]
            if 'zone' in data:
                return [self._parse_zone_element(data['zone'], parent, platform)]
            elif 'fakezone' in data:
                # special case - root element to be ignored
                fz = data["fakezone"]
                # it must have children
                if 'children' not in fz:
                    raise Exception(f"fakezone element in {child['include']} must have children!")
                result = []
                for ch2 in fz['children']:
                    result.extend(self._handle_memmap_include(ch2, parent, platform))
                return result
            else:
                raise Exception(f"Unknown element in {child['include']}")
        else:
            raise Exception(f"Unknown child in children of {flc}")

    def _parse_boot_menu_file(self, boot_menu_file: Path, platform: Platform) -> BootMenu:
        parser = self.parser_type(boot_menu_file, self.src_path)
        data = parser.data

        # get root "menu" element
        if "menu" not in data:
            raise Exception(f"No root menu in {boot_menu_file}")
        menu_element = data["menu"]

        return self._handle_menu_element(menu_element, parent=None, platform=platform)

    def _handle_menu_element(self, menu_element, parent: Optional[BootMenu], platform: Platform) -> BootMenu:
        flc = self.parser_type.get_file_line_column(menu_element)
        # grab children
        children = menu_element.pop("children", [])

        # set cname - lowercase and non-word characters are underscores
        if "name" in menu_element:
            menu_element["cname"] = re.sub(r'\W', '_', str(menu_element["name"]).lower())

        # # function attribute - change name
        # if "function" in menu_element:
        #     menu_element["fn"] = menu_element.pop("function")

        if parent is None:
            # top level

            # create top level menu
            try:
                boot_menu = BootMenu(**menu_element, yaml_node=menu_element,
                                     parent=None,
                                     platform=platform,
                                     returns=[],
                                     menus=[],
                                     args=[],
                                     id="",          # empty as this is TL
                                     confirm=False,  # as this is TL
                                     recurse=False,  # as this is TL
                                     doc="",         # TL
                                     )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
        else:
            # not top level

            # confirm flag
            if "confirm" not in menu_element:
                menu_element["confirm"] = parent.confirm

            # recurse flag
            if "recurse" not in menu_element:
                menu_element["recurse"] = parent.recurse

            # id flag
            menu_element["id"] = f"{parent.id}{len(parent.menus)}"

            # create menu
            try:
                boot_menu = BootMenu(**menu_element, yaml_node=menu_element,
                                     parent=parent,
                                     platform=platform,
                                     returns=[],
                                     menus=[],
                                     args=[],
                                     )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # parse children
        for child in children:
            if "menu" in child:
                boot_menu.menus.append(self._handle_menu_element(child["menu"], boot_menu, platform))
            elif "return" in child:
                boot_menu.returns.append(self._handle_menu_return_element(child["return"], boot_menu))
            elif "arg" in child:
                boot_menu.args.append(self._handle_menu_arg_element(child["arg"], boot_menu))
            elif "include" in child:
                data = child["data"]
                if "menu" in data:
                    boot_menu.menus.append(self._handle_menu_element(data["menu"], boot_menu, platform))
                else:
                    raise Exception(f"Unknown element in included file {child['include']} - expected 'menu'")

            else:
                raise Exception(f"Unknown element in children of {flc}")

        # checks

        # A node must have either children (menus) or a function, but never both

        if boot_menu.menus and boot_menu.function:
            raise Exception(f"Boot menu have both child menus and function! {flc}")

        if not boot_menu.menus and not boot_menu.function:
            raise Exception(f"Boot menu must have either child menus or function defined! {flc}")

        # A recursive node may not have arguments
        if boot_menu.recurse and boot_menu.args:
            raise Exception(f"Recursive boot menu may not have arguments defined! {flc}")

        # check if cname is unique
        if boot_menu.cname in self.menu_cnames:
            raise Exception(f"C name {boot_menu.cname} is not unique in {flc}")
        self.menu_cnames[boot_menu.cname] = True

        return boot_menu

    def _handle_menu_arg_element(self, arg_element, parent: BootMenu) -> BootMenuArg:
        flc = self.parser_type.get_file_line_column(arg_element)

        # create arg
        try:
            arg = BootMenuArg(**arg_element, yaml_node=arg_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return arg

    def _handle_menu_return_element(self, return_element, parent: BootMenu) -> BootMenuReturn:
        flc = self.parser_type.get_file_line_column(return_element)

        # create return
        try:
            ret = BootMenuReturn(**return_element, yaml_node=return_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return ret

    def _parse_connectors_file(self, connectors_file: Path) -> List[Connector]:
        parser = self.parser_type(connectors_file, self.src_path)
        data = parser.data

        # connectors list
        connectors = []

        # connectors id - to check uniqueness
        connectors_id = []

        for element in data:
            if "connector" not in element:
                raise Exception(f"connector element expected in {connectors_file}")
            connector_element = element["connector"]
            flc = parser.get_file_line_column(connector_element)
            # get its children (pins)
            children = connector_element.pop("children", [])

            if not children:
                raise Exception(f"Connector element must have pins {flc}")

            # create Connector
            try:
                connector = Connector(**connector_element, yaml_node=connector_element,
                                      pins=[],  # empty for now
                                      )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            if connector.id in connectors_id:
                raise Exception(f"Connector id {connector.id} is not unique! {flc}")
            connectors_id.append(connector.id)

            # pins

            # to check for uniqueness
            pin_numbers = []

            for child in children:
                if "pin" not in child:
                    raise Exception(f"Pin element expected in children of connector: {flc}")

                pin_element = child["pin"]
                flc = parser.get_file_line_column(pin_element)
                # create Pin
                try:
                    pin = Pin(**pin_element, yaml_node=pin_element)
                except ValidationError as e:
                    raise Exception(f"Validation error occurred for the element {flc}: {e}")

                if pin.number in pin_numbers:
                    raise Exception(f"pin number {pin.number} is not unique! {flc}")
                pin_numbers.append(pin.number)

                # add pin to connector
                connector.pins.append(pin)
            # add connector to connectors
            connectors.append(connector)
        return connectors

    def _parse_xilinxes_file(self, xilinxes_file: Path, hw: HW) -> List[Xilinx]:
        parser = self.parser_type(xilinxes_file, self.src_path)
        data = parser.data

        # xilinxes list
        xilinxes = []

        # for Xilinx
        last_xilinx_number = 1

        for element in data:
            flc = parser.get_file_line_column(element)
            if 'xilinx' not in element:
                raise Exception(f"xilinx element expected in {flc}")
            xilinx_element = element['xilinx']

            # get programs from xilinx element (its children)
            programs = xilinx_element.pop("children", [])
            # programs are optional
            # if not programs:
            #     raise Exception(f"xilinx element should have programs under children in {flc}")

            # create Xilinx object
            try:
                xilinx = Xilinx(**xilinx_element, yaml_node=xilinx_element,
                                programs=[],  # empty for now
                                hardware=hw,
                                usercode=""
                                )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # tester parsing
            if xilinx.tester:
                hw.tester_xilinxes += 1
            else:
                # Xilinx is not on a tester
                # give it an IC number
                xilinx.ic_number = last_xilinx_number
                last_xilinx_number += 1

            # last program id
            last_program_id = 'A'

            for program_element in programs:
                if "program" not in program_element:
                    raise Exception(f"Wrong element under children of {flc}")
                program_element = program_element["program"]
                program_flc = parser.get_file_line_column(program_element)

                # get children (revisions)
                revisions = program_element.pop("children", [])

                if not revisions:
                    raise Exception(f"program element should have revision(s) under children in {program_flc}")

                # create XilinxProgram object
                try:
                    xilinx_program = XilinxProgram(**program_element, yaml_node=program_element,
                                                   revisions=[],  # empty for now
                                                   id="",         # empty for now, set later
                                                   )
                except ValidationError as e:
                    raise Exception(f"Validation error occurred for the element {program_flc}: {e}")

                # Check that parent is not on the tester
                if xilinx.tester:
                    raise Exception(f"Tester Xilinxes may not have programs: {program_flc}")

                # add program ID
                xilinx_program.id = last_program_id
                # increment program ID
                last_program_id = chr(ord(last_program_id) + 1)

                # parse revisions
                for revision_element in revisions:
                    if "revision" not in revision_element:
                        raise Exception(f"Wrong element under children of {program_flc}")
                    revision_element = revision_element["revision"]
                    revision_flc = parser.get_file_line_column(revision_element)

                    # create XilinxRevision object
                    try:
                        xilinx_revision = XilinxRevision(**revision_element, yaml_node=revision_element)
                    except ValidationError as e:
                        raise Exception(f"Validation error occurred for the element {revision_flc}: {e}")

                    # add revision to program
                    xilinx_program.revisions.append(xilinx_revision)
                # add xilinx program to xilinx
                xilinx.programs.append(xilinx_program)

            # check xilinx - construct usercode
            if not xilinx.tester:
                if xilinx.programs:
                    revision = xilinx.programs[0].revision
                else:
                    revision = 1

                if xilinx.hardware.usercode_dev:
                    usercode_dev = xilinx.hardware.usercode_dev
                else:
                    usercode_dev = xilinx.hardware.lhc_id[5:6]

                xilinx.usercode = f"{usercode_dev}{xilinx.ic_number}A{revision}"

            # add xilinx to list
            xilinxes.append(xilinx)
        return xilinxes

    def _parse_main_hw(self, main_hw_file: Path, platform: Platform) -> HW:
        parser = self.parser_type(main_hw_file, self.src_path)
        data = parser.data

        # get hw element
        if "hw" not in data:
            raise Exception(f"hw element not in {main_hw_file}")
        hw_element = data["hw"]

        flc = self.parser_type.get_file_line_column(hw_element)

        # TODO fix in translators: ab_po_id -> epc_id
        try:
            hw_element["epc_id"] = hw_element.pop("ab_po_id")
        except:
            # will be validated later
            pass

        # split subhw
        try:
            hw_element["subhw_str"] = hw_element["subhw"].split(" ")
            hw_element.pop("subhw")
        except:
            # will be validated later
            pass

        try:
            hw = HW(**hw_element, yaml_node=hw_element,
                    tester_xilinxes=0,  # set count of tester Xilinxes to default of 0
                    platform=platform,
                    xilinxes=[],        # will be filled in later (if exist)
                    connectors=[],      # same story as xilinxes
                    subhw=[]
                    )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # test for duplicates (global)
        if hw.lhc_id in self.lhc_ids:
            raise Exception(f"lhc_id is duplicated in {flc}")

        if hw.epc_id in self.epc_ids:
            raise Exception(f"epc_id is duplicated in {flc}")

        self.lhc_ids[hw.lhc_id] = True
        self.epc_ids[hw.epc_id] = True

        return hw

    def _parse_consts(self, consts_file: Path):
        parser = self.parser_type(consts_file, self.src_path)
        data = parser.data

        # TODO: see if needed, as it doesn't to be present, only "shared_consts.yaml/xml" is there

    def _parse_main_def(self, main_file: Path, platform_name: str) -> Platform:
        parser = self.parser_type(main_file, self.src_path)
        data = parser.data

        # get main element
        if "platform" not in data:
            raise Exception(f"platform element not in {main_file}")
        platform_element = data["platform"]
        flc = parser.get_file_line_column(platform_element)

        # get main area
        children = platform_element.pop("children", [])
        # children are optional
        # if not children:
        #     raise Exception(f"When parsing main file: children missing in {flc}")

        # create Platform
        try:
            platform = Platform(**platform_element, yaml_node=platform_element,
                                name=platform_name,
                                hardware={},  # empty for now
                                memmaps={}    # empty for now
                                )
        except ValidationError as e:
            flc = self.parser_type.get_file_line_column(platform_element)
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # there should be single area in children if they are there
        # TODO: perhaps change translator and make it same to existing area/links translators

        if children:
            try:
                main_area_element = children[0]["area"]
            except (KeyError, IndexError):
                raise Exception(f"Missing main area element in {flc}")

            platform.sitemap = self._parse_area(main_area_element, top_level=True)

        return platform

    def _parse_area(self, area_element, top_level=False) -> Area:
        # flc
        flc = self.parser_type.get_file_line_column(area_element)

        # if top level, add empty name attribute as it is mandatory but can be empty there
        if top_level:
            area_element["name"] = ""

        # get children
        children = area_element.pop("children", [])

        areas = []
        links = []
        if children:
            for child in children:
                if "area" in child:
                    areas.append(self._parse_area(child["area"]))
                elif "link" in child:
                    links.append(self._parse_link(child["link"]))
                else:
                    raise Exception(f"Unknown child in {flc}")

        try:
            area = Area(**area_element, yaml_node=area_element,
                        areas=areas,
                        links=links)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return area

    def _parse_link(self, link_element) -> Link:
        # flc
        flc = self.parser_type.get_file_line_column(link_element)
        try:
            link = Link(**link_element, yaml_node=link_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        return link
