from pathlib import Path

from fgc_parser_schemas import SuperDict, SymlistConst, Symlist
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class SymlistReader(BaseReader):
    def __init__(self, src_path: Path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)

        self.files = (self.src_path / 'symlists').glob("*.yaml")

    def parse_file(self, symlist_file: Path) -> Symlist:
        parser = self.parser_type(yaml_file=symlist_file, base_path=self.src_path)
        data = parser.data

        element = data["symlist"]

        element_children = element.pop("children", [])

        flc = parser.get_file_line_column(element)
        try:
            symlist = Symlist(**element, yaml_node=element,
                              name=symlist_file.stem.upper(),  # capitalized file name without extension
                              consts=[]  # temporarily put empty list
                              )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        children = []

        for child in element_children:
            if "const" in child:
                children.append(child)
            if "include" in child:
                consts_element = child["data"]["consts"]
                # sometimes doc element is defined in the included file...
                if 'doc' in consts_element:
                    # add it to symlist...
                    symlist.doc = consts_element['doc']
                for el in consts_element["children"]:
                    children.append(el)

        consts = []
        for child in children:
            flc = parser.get_file_line_column(child)

            try:
                # rename "assert" to "assertion"
                if "assert" in child["const"]:
                    child["const"]["assertion"] = child["const"].pop("assert")

                const = SymlistConst(**child["const"], yaml_node=child["const"])

                if const.meaning is None:
                    if symlist.meaning is not None:
                        const.meaning = symlist.meaning
                    else:
                        const.meaning = SymlistConst.Meaning.NONE

                # If symlist has documentation, then all constants within the list should also have documentation
                # and the title will be used if the documentation is not defined.
                # If symlist has no documentation, then neither should the constants within the list
                if symlist.doc is not None:

                    # If no doc provided then use the title instead
                    if const.doc is None:
                        const.doc = const.title
                else:
                    # Check that no constants have documentation
                    if const.doc is not None:
                        raise Exception(f"Constant {const.name} may not have doc defined: {flc}")

                consts.append(const)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
        # set processed consts
        symlist.consts = consts
        return symlist

    def _parse(self):
        for symlist_file in self.files:
            symlist = self.parse_file(symlist_file)
            # add symlist name as prefix for constants in symlist
            for const in symlist.consts:
                const.symbol = const.name
                const.name = f"{symlist.name}_{const.symbol}"

            self.superdict.symlists[symlist.name] = symlist
