import functools

from .codes import CodesReader
from .dim_names import DimNamesReader
from .error import ErrorsReader
from .flags import FlagsReader
from .type import TypesReader
from .getoption import GetOptionsReader
from .function import FunctionReader
from .property_groups import PropertyGroupsReader
from .runlog import RunlogReader
from .doc_main import DocMainReader
from .consts import ConstsReader
from .symlist import SymlistReader
from .property import PropertyReader
from .components import ComponentsReader
from .component_labels import ComponentLabelsReader
from .dim_types import DimTypesReader
from .systems import SystemsReader
from .platforms import PlatformReader
from .classes import ClassReader
from .regfgc3 import RegFgc3Reader


def partialclass(cls, *args, **kwargs):
    class NewCls(cls):
        __init__ = functools.partialmethod(cls.__init__, *args, **kwargs)
    return NewCls


READERS = [CodesReader,
           DimNamesReader,
           ErrorsReader,
           FlagsReader,
           TypesReader,
           GetOptionsReader,
           partialclass(FunctionReader, file="getfuncs.yaml"),
           PropertyGroupsReader,
           partialclass(FunctionReader, file="setfuncs.yaml"),
           partialclass(FunctionReader, file="setiffuncs.yaml"),
           RunlogReader,
           DocMainReader,
           ConstsReader,
           SymlistReader,
           PropertyReader,
           ComponentsReader,
           ComponentLabelsReader,
           DimTypesReader,
           SystemsReader,
           PlatformReader,
           ClassReader,
           RegFgc3Reader
           ]
