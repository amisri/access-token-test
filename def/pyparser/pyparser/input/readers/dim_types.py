import concurrent.futures
from fgc_parser_schemas import SuperDict, DimType, AnaChannel, DigBank, DigInput
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class DimTypesReader(BaseReader):
    def __init__(self, src_path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)
        self.files = (self.src_path / 'dim_types').glob("*.yaml")

    def _parse(self):
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            future_to_dim_file = {executor.submit(self._parse_file, dim_type_file): dim_type_file
                                  for dim_type_file in self.files}
            for future in concurrent.futures.as_completed(future_to_dim_file):
                # this will throw if something went wrong
                dim_type = future.result()
                self.superdict.dim_types[dim_type.type] = dim_type
        # add indexes
        for idx, dim_type_name in enumerate(sorted(self.superdict.dim_types.keys())):
            self.superdict.dim_types[dim_type_name].index = idx

    def _parse_file(self, dim_type_file) -> DimType:
        parser = self.parser_type(yaml_file=dim_type_file, base_path=self.src_path)
        data = parser.data
        try:
            element = data["dim_type"]
        except KeyError:
            raise Exception(f"No dim_type node in {dim_type_file}!")

        element_children = element.pop("children", [])
        if element_children is None:
            element_children = []

        flc = parser.get_file_line_column(element)

        try:
            dim_type = DimType(**element, yaml_node=element,
                               dig_banks=dict(),        # temporarily put empty
                               ana_channels=dict(),     # temporarily put empty
                               index=-1                 # set later
                               )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # check if such component exist
        if dim_type.comp not in self.superdict.component_labels:
            raise Exception(f'Unknown component {dim_type.comp}: {flc}')

        for child in element_children:
            flc = parser.get_file_line_column(child)
            if "ana_channel" in child:
                try:
                    ana_channel = AnaChannel(**child["ana_channel"], yaml_node=child["ana_channel"])
                except ValidationError as e:
                    raise Exception(f"Validation error occurred for the element {flc}: {e}")
                # capitalize label
                ana_channel.label = ana_channel.label.upper()

                # add to parent
                dim_type.ana_channels[ana_channel.index] = ana_channel
            elif "dig_bank" in child:
                dig_bank_element = child["dig_bank"]
                # parse its children - inputs
                dig_bank_children_element = dig_bank_element.pop("children", [])

                if dig_bank_children_element is None:
                    dig_bank_children_element = []
                    # TODO: see if exception if there's no children is needed
                    # raise Exception(f"dig_bank element should have children {flc}")

                # create dig_bank
                try:
                    dig_bank = DigBank(**child["dig_bank"], yaml_node=child["dig_bank"],
                                       inputs=dict()  # use empty inputs for the moment
                                       )
                except ValidationError as e:
                    raise Exception(f"Validation error occurred for the element {flc}: {e}")

                for dig_bank_child in dig_bank_children_element:
                    flc_dig_bank_child = parser.get_file_line_column(dig_bank_child)
                    if "input" in dig_bank_child:
                        input_element = dig_bank_child["input"]

                        try:
                            dig_input = DigInput(**input_element, yaml_node=input_element)
                        except ValidationError as e:
                            raise Exception(f"Validation error occurred for the element {flc_dig_bank_child}: {e}")

                        # set case for label, one and zero
                        dig_input.label = dig_input.label.upper()
                        dig_input.one = dig_input.one.upper()
                        dig_input.zero = dig_input.zero.lower()

                        # add to parent
                        dig_bank.inputs[dig_input.index] = dig_input
                    else:
                        raise Exception(f"Unknown child - 'input' expected in {flc_dig_bank_child}")
                # add dig bank to parent
                dim_type.dig_banks[dig_bank.index] = dig_bank
            else:
                raise Exception(f"{child} Is not valid element in {flc}")
        return dim_type
