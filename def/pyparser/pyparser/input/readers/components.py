from fgc_parser_schemas import Component
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class ComponentsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type,
                         yaml_file=src_path / 'components.yaml')

    def _parse(self):
        data = self.parser.data

        for component_element in data["components"]:
            component_element = component_element["component"]

            # tests
            flc = self.parser.get_file_line_column(component_element)

            # Check that references to properties are valid

            if "barcode_prop" in component_element:
                barcode_prop = component_element["barcode_prop"]
                if "?" in barcode_prop:
                    prop_a = barcode_prop.replace('?', 'A')
                    prop_b = barcode_prop.replace('?', 'B')

                    if prop_a not in self.superdict.properties and prop_b not in self.superdict.properties:
                        flc = self.parser.get_file_line_column(component_element, "barcode_prop")
                        raise Exception(f"Barcode property not valid in {flc}")
                else:
                    if barcode_prop not in self.superdict.properties:
                        flc = self.parser.get_file_line_column(component_element, "barcode_prop")
                        raise Exception(f"Barcode property not valid in {flc}")

            if "temp_prop" in component_element:
                temp_prop = component_element["temp_prop"]
                if "?" in temp_prop:
                    prop_a = temp_prop.replace('?', 'A')
                    prop_b = temp_prop.replace('?', 'B')

                    if prop_a not in self.superdict.properties and prop_b not in self.superdict.properties:
                        flc = self.parser.get_file_line_column(component_element, "temp_prop")
                        raise Exception(f"Temp property not valid in {flc}")
                else:
                    if temp_prop not in self.superdict.properties:
                        flc = self.parser.get_file_line_column(component_element, "temp_prop")
                        raise Exception(f"Temp property not valid in {flc}")

            if "dallas_id" in component_element:
                component_element["dallas_id"] = True if component_element["dallas_id"] == "yes" else False

            if "platform" in component_element:
                component_element["platforms"] = component_element["platform"].split(",")
                component_element.pop("platform")

            try:
                component = Component(**component_element, yaml_node=component_element,
                                      label=""  # empty for now, will be filled in when processing component_labels
                                      )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            if component.barcode in self.superdict.components:
                raise Exception(f"Component with barcode {component.barcode} is not unique! {flc}")

            self.superdict.components[component.barcode] = component
