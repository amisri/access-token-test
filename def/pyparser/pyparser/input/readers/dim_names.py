from fgc_parser_schemas import DimName
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class DimNamesReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'dim_names.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["dim_names"]):
            # tests
            flc = self.parser.get_file_line_column(data["dim_names"], idx)
            try:
                dim = DimName(name=element,
                              index=idx,
                              yaml_node=data)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            self.superdict.dim_names[dim.name] = dim
