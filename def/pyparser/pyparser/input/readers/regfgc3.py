import os.path
import re
from pathlib import Path
from typing import Optional

from fgc_parser_schemas import (SuperDict, RegFgc3Board, RegFgc3Crate, RegFgc3Device, RegFgc3,
                                RegFgc3VariantReadWriteBlock, RegFgc3VariantReadWriteBlockElement, RegFgc3Variant,
                                RegFgc3DeviceDict, RegFgc3BoardDict, RegFgc3VariantDict,
                                RegFgc3BoardListBoardType, RegFgc3BoardsList)
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class RegFgc3Reader(BaseReader):
    def __init__(self, src_path: Path, superdict: SuperDict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type)

        self.regfgc3_path = self.src_path / 'regfgc3'
        self.converters_path = self.regfgc3_path / 'converters'
        self.boards_path = self.regfgc3_path / 'boards'

    def _parse(self):
        # init regfgc3
        self.superdict.regfgc3 = RegFgc3(converters={},
                                         boards={},
                                         yaml_node=None)
        # Parse RegFGC3 converter definition
        for converter_file in self.converters_path.glob("*.yaml"):
            # deduce converter name from the filename
            converter_name = converter_file.stem
            crate = self._parse_converter(converter_file)
            # Check file name is consistent with type in the xml
            if crate.type != converter_name:
                raise Exception(f"File name {converter_file} doesn't match {crate.type}")

            self.superdict.regfgc3.converters[crate.type] = crate

        # Parse RegFGC3 board definition

        regfgc3_board_dirs = [x for x in self.boards_path.iterdir() if x.is_dir()]

        for board_dir in regfgc3_board_dirs:
            self._parse_board_dir(board_dir)

        # Parse RegFGC3 boards definition

        board_list = self._parse_boards_definition(self.boards_path / 'boards.yaml')

        # merge board_list with superdict

        for board_name, board in board_list.board_types.items():
            # super dict board
            sd_b = self.superdict.regfgc3.boards[board_name]
            for field_name in board.__fields__.keys():
                setattr(sd_b, field_name, getattr(board, field_name))

    def _parse_boards_definition(self, boards_def_file: Path) -> RegFgc3BoardsList:
        parser = self.parser_type(boards_def_file, self.src_path)
        data = parser.data

        # get main element
        if "board_list" not in data:
            raise Exception(f"board_list element not in {boards_def_file}")
        board_list_element = data["board_list"]
        flc = parser.get_file_line_column(board_list_element)

        # grab children (board_types)
        children = board_list_element.pop("children", [])

        try:
            board_list = RegFgc3BoardsList(**board_list_element, yaml_node=board_list_element,
                                           board_types={},  # empty for now
                                           )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        for child in children:
            if 'board_type' not in child:
                raise Exception(f"Unknown child in children of board_list in {flc}")
            board_type_element = child['board_type']
            # grab children (barcodes)
            barcodes = board_type_element.pop("children", [])
            try:
                board_type = RegFgc3BoardListBoardType(**board_type_element, yaml_node=board_type_element,
                                                       barcodes=[],  # empty for now
                                                       )
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # Check that the board type corresponds to a known type defined in regfgc3 board symlist
            # Add reference to this symlist
            board_type_symlist = self.superdict.symlists['REGFGC3_BOARDS'].consts

            for const in board_type_symlist:
                if const.symbol == board_type.name:
                    board_type.board_symbol = const
                    break
            else:
                raise Exception(f"Value for board {board_type.name} not defined in symlist")

            for barcode in barcodes:
                barcode_text = barcode['barcode']
                if barcode_text:
                    board_type.barcodes.append(barcode_text)

                    if barcode_text not in self.superdict.components:
                        raise Exception(f"Component with barcode {barcode_text} does not exist")
                    self.superdict.components[barcode_text].regfgc3_def = board_type

            board_list.board_types[board_type.name] = board_type

        return board_list

    def _parse_board_dir(self, board_dir: Path):
        variant_files = board_dir.rglob("*.yaml")
        for variant_file in variant_files:
            # Extract data of interest from file
            rel_path = variant_file.relative_to(self.boards_path)
            board, device, variant, yaml_file = os.path.normpath(rel_path).split(os.sep)
            api_version = yaml_file.split('_')[1].replace('.yaml', '')

            # check data agains symlists

            # boards
            board_type_symlist = self.superdict.symlists['REGFGC3_BOARDS'].consts

            for const in board_type_symlist:
                if const.symbol == board:
                    board_symbol = const
                    break
            else:
                raise Exception(f"Value for board {board} not defined in symlist regfgc3_boards.yaml")

            # Devices
            devices_symlist = self.superdict.symlists['REGFGC3_DEVICES'].consts
            for const in devices_symlist:
                if const.symbol == device:
                    device_symbol = const
                    break
            else:
                raise Exception(f"Value for device {device} not defined in symlist regfgc3_devices.yaml")

            # Variants
            variant_symlist = self.superdict.symlists['REGFGC3_VARIANTS'].consts
            for const in variant_symlist:
                if const.symbol == variant:
                    variant_symbol = const
                    break
            else:
                raise Exception(f"Value for variant {variant} not defined in symlist regfgc3_variants.yaml")

            board_dict = self.superdict.regfgc3.boards.setdefault(board, RegFgc3BoardDict(yaml_node=None))
            devices_dict = board_dict.devices.setdefault(device, RegFgc3DeviceDict(yaml_node=None))
            variant_dict = devices_dict.variants

            variant_obj = self._parse_variant(variant_file)

            if variant in variant_dict:
                variant_dict[variant].variant[api_version] = variant_obj
            else:
                variant_dict[variant] = RegFgc3VariantDict(
                    variant_symbol=variant_symbol,
                    variant={api_version: variant_obj},
                    yaml_node=None
                )

    def _parse_variant(self, variant_file: Path) -> RegFgc3Variant:
        parser = self.parser_type(variant_file, self.src_path)
        data = parser.data

        # get main element
        if "variant" not in data:
            raise Exception(f"variant element not in {variant_file}")
        variant_element = data["variant"]
        flc = parser.get_file_line_column(variant_element)

        # grab children (read/write blocks)
        children = variant_element.pop("children", [])

        if children is None:
            children = []
            # TODO: fix translator

        try:
            variant = RegFgc3Variant(**variant_element, yaml_node=variant_element,
                                     read_blocks=[],   # empty for now
                                     write_blocks=[],  # empty for now
                                     )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        for child in children:
            if 'read_block' in child:
                block = self._parse_variant_read_write_block(child['read_block'],
                                                             RegFgc3VariantReadWriteBlock.Type.Read,
                                                             variant)
                variant.read_blocks.append(block)
            elif 'write_block' in child:
                block = self._parse_variant_read_write_block(child['write_block'],
                                                             RegFgc3VariantReadWriteBlock.Type.Write,
                                                             variant)
                variant.write_blocks.append(block)
            else:
                raise Exception(f"Unknown child in children of variant {flc}")

        return variant

    def _parse_variant_read_write_block(self, block_element,
                                        block_type: RegFgc3VariantReadWriteBlock.Type,
                                        parent: RegFgc3Variant) -> RegFgc3VariantReadWriteBlock:
        # get children (elements)
        children = block_element.pop("children", [])

        flc = self.parser_type.get_file_line_column(block_element)

        try:
            block = RegFgc3VariantReadWriteBlock(**block_element, yaml_node=block_element,
                                                 elements=[],  # empty for now
                                                 type=block_type
                                                 )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        for child in children:
            if 'element' in child:
                element = self._parse_variant_read_write_block_element(child['element'], block)
                block.elements.append(element)
            else:
                raise Exception(f"Unknown child in children of {flc}")

        return block

    def _parse_variant_read_write_block_element(self,
                                                element_element,
                                                parent: RegFgc3VariantReadWriteBlock
                                                ) -> RegFgc3VariantReadWriteBlockElement:
        flc = self.parser_type.get_file_line_column(element_element)

        # check if there's symlist attribute and convert it to symlist
        if 'symlist' in element_element:
            symlist_str = element_element['symlist']

            if symlist_str not in self.superdict.symlists:
                raise Exception(f"Symlist {symlist_str} is not a valid symlist in {flc}")
            element_element['symlist'] = self.superdict.symlists[symlist_str]

        try:
            element = RegFgc3VariantReadWriteBlockElement(**element_element, yaml_node=element_element,
                                                          parent=parent,
                                                          number=len(parent.elements))
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # If the element is within a write block, then check that it has a default value

        if parent.type == RegFgc3VariantReadWriteBlock.Type.Write:
            # Check that the default value is in hexadecimal format, then decode it
            if element.default_value is None:
                raise Exception(f"Default value must be set for element {flc} as its parent if WRITE")
            try:
                int(element.default_value, 16)
            except ValueError:
                raise Exception(f"Default value is not in hexadecimal in {flc}")

        # Check that symlist of this node is valid
        # if not a symlist we expect a scaling_gain + scaling_offset.
        if element.symlist:
            if element.scaling_gain is not None or element.scaling_offset is not None:
                raise Exception(f"Parameter {element.name}: symlist and scaling are exclusive in {flc}")
        elif element.scaling_gain is not None and element.scaling_offset is not None:
            if element.scaling_gain == 0:
                raise Exception(f"Parameter {element.name} gain is zero in {flc}")
        else:
            element.scaling_gain = 1
            element.scaling_offset = 0
            element.unit = '?'

        # if limits min, max, check as for properties

        if element.limits:
            if element.min is None:
                raise Exception(f"Min limit not defined in {flc}")
            if element.max is None:
                raise Exception(f"Max limit not defined in {flc}")
            if element.limits not in ('int', 'float'):
                raise Exception(f"Unknown limit type in {flc}")

        return element


    def _parse_converter(self, converter_file: Path) -> RegFgc3Crate:
        parser = self.parser_type(converter_file, self.src_path)
        data = parser.data

        # get main element
        if "crate" not in data:
            raise Exception(f"crate element not in {converter_file}")
        crate_element = data["crate"]
        flc = parser.get_file_line_column(crate_element)

        # grab children (boards)
        children = crate_element.pop("children", [])

        try:
            crate = RegFgc3Crate(**crate_element, yaml_node=crate_element,
                                 boards={},  # empty for now
                                 )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # parse boards
        for child in children:
            if 'board' in child:
                # Create hash with boards
                # check that 2 boards of the same type share the same variant
                # merge the boards of same type.
                new_board = self._parse_converter_board(child['board'], crate)

                if new_board.name in crate.boards:
                    crate.boards[new_board.name].slots.append(new_board.slot)
                else:
                    new_board.slots.append(new_board.slot)
                    crate.boards[new_board.name] = new_board
            else:
                raise Exception(f"Unknown element in children of {flc}")

        return crate

    def _parse_converter_board(self, board_element, parent: RegFgc3Crate) -> RegFgc3Board:
        flc = self.parser_type.get_file_line_column(board_element)

        # grab children (devices)
        children = board_element.pop("children", [])

        try:
            board = RegFgc3Board(**board_element, yaml_node=board_element,
                                 devices=[],  # empty for now
                                 slots=[],    # empty for now
                                 )
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        for child in children:
            if 'device' in child:
                device = self._parse_converter_device(child['device'], board)
                board.devices.append(device)
            else:
                raise Exception(f"Unknown child in children of {flc}")

        # Check that the board type corresponds to a known type defined in regfgc3 board symlist
        # Add reference to this symlist
        board_type_symlist = self.superdict.symlists['REGFGC3_BOARDS'].consts
        for const in board_type_symlist:
            if const.symbol == board.name:
                board.board_symbol = const
                break
        else:
            raise Exception(f"Value for board {board.name} ({flc}) not defined in regfgc3_boards.yaml")

        return board

    def _parse_converter_device(self, device_element, parent: RegFgc3Board) -> RegFgc3Device:
        flc = self.parser_type.get_file_line_column(device_element)

        try:
            device = RegFgc3Device(**device_element, yaml_node=device_element)
        except ValidationError as e:
            raise Exception(f"Validation error occurred for the element {flc}: {e}")

        # Check device's name validity against symlist
        device_symlist = self.superdict.symlists['REGFGC3_DEVICES'].consts
        for const in device_symlist:
            if const.symbol == device.name.upper():
                break
        else:
            raise Exception(f"Value for device {repr(device.name)} ({flc}) not defined in symlist regfgc3_devices.yaml")

        # Check that the variant type corresponds to a known type defined in regfgc3 variant symlist
        # Add reference to this symlist
        variant_symlist = self.superdict.symlists['REGFGC3_VARIANTS'].consts
        for const in variant_symlist:
            if const.symbol == device.variant:
                device.variant_symbol = const
                break
        else:
            raise Exception(f"Value for device {repr(device.name)} variant {repr(device.variant)} ({flc}) not defined in symlist regfgc3_variants.yaml")

        return device
