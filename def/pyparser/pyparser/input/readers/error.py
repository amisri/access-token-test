from fgc_parser_schemas import Error
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class ErrorsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'errors.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["errors"]):
            element = element["error"]
            # tests
            flc = self.parser.get_file_line_column(element)

            try:
                error = Error(number=idx, **element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            # duplicates
            for el in self.superdict.errors:
                if el.message == error.message:
                    raise Exception(f"Error {el.message} is not unique! {flc}")

            self.superdict.errors.append(error)
