from fgc_parser_schemas import PropertyGroup
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class PropertyGroupsReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         yaml_file=src_path / 'property_groups.yaml',
                         parser_type=parser_type)

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["property_groups"]):
            element = element["property_group"]
            # tests
            flc = self.parser.get_file_line_column(element)
            try:
                prop_group = PropertyGroup(**element, yaml_node=element)
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")
            # duplicates
            if prop_group.name in self.superdict.property_groups:
                raise Exception(f"Property group name {prop_group.name} is not unique! {flc}")

            self.superdict.property_groups[prop_group.name] = prop_group
