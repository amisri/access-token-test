from fgc_parser_schemas import Link, Area
from pydantic import ValidationError

from pyparser.input.readers.base_reader import BaseReader


class DocMainReader(BaseReader):
    def __init__(self, src_path, superdict, parser_type):
        super().__init__(src_path=src_path,
                         superdict=superdict,
                         parser_type=parser_type,
                         yaml_file=src_path / 'doc' / 'main.yaml')

    def _parse(self):
        data = self.parser.data

        for idx, element in enumerate(data["main"]):

            element = element["area"]

            # first handle links

            links = []

            for link in element["links"]:
                link = link["link"]
                flc = self.parser.get_file_line_column(link)
                try:
                    links.append(Link(**link, yaml_node=link))
                except ValidationError as e:
                    raise Exception(f"Validation error occurred for the element {flc}: {e}")

            element["links"] = links

            # tests
            flc = self.parser.get_file_line_column(element)

            try:
                area = Area(**element, yaml_node=element,
                            areas=[])  # areas empty for now, TODO see if needed recurse
            except ValidationError as e:
                raise Exception(f"Validation error occurred for the element {flc}: {e}")

            self.superdict.sitemap.append(area)
