import re
import logging
from typing import List, Callable, Optional
from itertools import chain

from fgc_parser_schemas import SuperDict, BaseModel, Property


class BaseTester:
    FIELDS: List[str] = []

    def __init__(self, superdict: SuperDict, yaml_parser):
        self.superdict = superdict
        self.yaml_parser = yaml_parser
        self.logger = logging.getLogger(self.__class__.__name__)

    def test_element(self, name, value, parent: BaseModel):
        pass

    def print_test_results(self) -> bool:
        return False


class DocLinksTester(BaseTester):
    FIELDS = ['doc']

    class LinkType:
        def __init__(self, link_type_name: str, superdict_dict=None, match_function: Optional[Callable] = None):
            assert(superdict_dict is not None or match_function is not None)
            self.link_type_name = link_type_name
            if superdict_dict is not None:
                self.match_function = lambda x: x in superdict_dict
            elif match_function:
                self.match_function = match_function

            self.regex = None

    def __init__(self, superdict: SuperDict, yaml_parser):
        super().__init__(superdict, yaml_parser)
        self.unique_wrong_links = set()
        self.external_links = set()

        # link types
        self.link_types = [
            DocLinksTester.LinkType("ERR",
                                    # change spaces to underscores
                                    match_function=lambda x:
                                    any([True for err in superdict.errors if err.message.replace(' ', '_') == x])),
            DocLinksTester.LinkType("FLAG", superdict.flags),
            DocLinksTester.LinkType("GET", superdict.gets),
            DocLinksTester.LinkType("GOPT", superdict.getoptions),
            DocLinksTester.LinkType("SET", superdict.sets),
            DocLinksTester.LinkType("SETIF", superdict.setifs),
            DocLinksTester.LinkType("TYPE", superdict.types)
        ]

        # sp links types
        self.splink_types = [
            DocLinksTester.LinkType("PROP", match_function=lambda x, y: f"{x}{y}" in superdict.properties),
            DocLinksTester.LinkType("SYMLSTC", match_function=lambda x, y: f"{x}" in superdict.symlists or
                                                                           f"{x}" in superdict.class_symlists)
        ]
        # also: STAT and TRAN, but dunno them.

        for link_type in self.link_types:
            r = r"\{" + link_type.link_type_name + r" (\w+)\}"
            link_type.regex = re.compile(r)
        for link_type in self.splink_types:
            r = r"\{" + link_type.link_type_name + r" (\w+)(.*?)\}"
            link_type.regex = re.compile(r)

        # special regex to match all types of links
        r = r"\{(\w+) (\w+)(.*?)\}"
        self.all_links_regex = re.compile(r)

        # Get external links
        self.external_links_regex = re.compile(r'"(http.?:\/\/[\w\d.\/#]*)"')

    def print_test_results(self) -> bool:
        if self.unique_wrong_links:
            self.logger.warning(f"Wrong links summary: {sorted(self.unique_wrong_links)}")
        self.logger.debug(f"External links: {sorted(self.external_links)}")
        if self.unique_wrong_links:
            return True
        return False

    def test_element(self, name, value, parent: BaseModel):
        if not value:
            return
        # get file, line, column
        try:
            flc = self.yaml_parser.get_file_line_column(parent.yaml_node, "doc")
        except KeyError:
            # Some symlists have doc attribute defined in a different file -
            # - this is combined when parsing later.
            # give at least some info...
            flc = self.yaml_parser.get_file_line_column(parent.yaml_node)

        # first 'simple' links
        for link_type in self.link_types:
            for res in link_type.regex.finditer(value):
                link = res.group(1)
                if not link_type.match_function(link):
                    self.unique_wrong_links.add(f"{{{link_type.link_type_name} {link}}}")
                    self.logger.error(f"Link {{{link_type.link_type_name} {link}}}  not found! Location: {flc}")
        # complex links
        for link_type in self.splink_types:
            for res in link_type.regex.finditer(value):
                link1 = res.group(1)
                link2 = res.group(2)
                if not link_type.match_function(link1, link2):
                    self.unique_wrong_links.add(f"{{{link_type.link_type_name} {link1}{link2}}}")
                    self.logger.error(f"Link {{{link_type.link_type_name} {link1}{link2}}} not valid! Location: {flc}")

        # any link
        # todo: this recognizes all links... possibly this could be only regex applied.
            for res in self.all_links_regex.finditer(value):
                found_link_type = res.group(1)
                link1 = res.group(2)
                link2 = res.group(3)

                found = any([True for link_type in chain(self.link_types, self.splink_types)
                             if link_type.link_type_name == found_link_type])

                if not found:
                    self.unique_wrong_links.add(f"{{{found_link_type} {link1}{link2}}}")
                    self.logger.error(f"Link {{{found_link_type} {link1}{link2}}} not valid"
                                      f" - the link type {found_link_type} is unknown. Location: {flc}")
        # get external links
        for res in self.external_links_regex.finditer(value):
            link = res.group(1)
            self.external_links.add(link)


class Tester:
    def __init__(self):
        self.testers = {}

    def register(self, testing_class: BaseTester):
        for tested_field in testing_class.FIELDS:
            self.testers.setdefault(tested_field, []).append(testing_class)

    def test_element(self, name, value, parent: BaseModel):
        for test_class in self.testers.get(name, []):
            test_class.test_element(name, value, parent)

    def print_results(self) -> bool:
        results = []
        for field, test_classes in self.testers.items():
            for test_class in test_classes:
                results.append(test_class.print_test_results())
        return any(results)


def test_all_properties_are_used(superdict: SuperDict) -> bool:
    # construct a set of all used properties
    used_properties = set()
    for cls in superdict.classes.values():
        # over their properties...
        for prop in cls.properties.values():
            used_properties.add(prop.full_name)
    # set of all available properties

    # ignore the DIAG properties with dynamic=True,
    # they are added to the property tree in the FGC on startup according to the DIMs that are actually in use
    # They exist as placeholders in the XML, but are not statically instantiated in any class.
    available_properties = filter(lambda prop: not (prop.full_name.startswith("DIAG") and prop.dynamic),
                                  superdict.properties.values())
    available_properties = set(prop.full_name for prop in available_properties)

    not_used_properties = sorted(available_properties - used_properties)
    if not_used_properties:
        logging.warning(f"Not used properties: {not_used_properties}")
        return True
    return False


def test_all_types_are_used(superdict: SuperDict) -> bool:
    all_types = set(superdict.types.keys())
    used_types = set()

    for prop in superdict.properties.values():
        used_types.add(prop.type.name)
        [used_types.add(typ.name) for typ in prop.specific_types.values()]

    not_used_types = sorted(all_types - used_types)

    if not_used_types:
        logging.warning(f"Not used types: {not_used_types}")
        return True
    return False


def test_all_set_setif_get_are_used(superdict: SuperDict) -> bool:
    all_gets = set(superdict.gets.keys())
    all_sets = set(superdict.sets.keys())
    all_setifs = set(superdict.setifs.keys())

    used_gets = set()
    used_sets = set()
    used_setifs = set()

    for prop in superdict.properties.values():
        # get
        if prop.get:
            used_gets.add(prop.get.name)
        [used_gets.add(get.name) for get in prop.specific_get.values() if get]
        # set
        if prop.set:
            used_sets.add(prop.set.name)
        [used_sets.add(setf.name) for setf in prop.specific_set.values() if setf]
        # setif
        if prop.setif:
            used_setifs.add(prop.setif.name)

    not_used_gets = sorted(all_gets - used_gets)
    not_used_sets = sorted(all_sets - used_sets)
    not_used_setifs = sorted(all_setifs - used_setifs)

    if not_used_gets:
        logging.warning(f"Not used get functions: {not_used_gets}")

    if not_used_sets:
        logging.warning(f"Not used set functions: {not_used_sets}")

    if not_used_setifs:
        logging.warning(f"Not used set if functions: {not_used_setifs}")

    if not_used_gets or not_used_sets or not_used_setifs:
        return True
    return False


def test_all_global_symlists_are_used(superdict: SuperDict) -> bool:
    all_symlists = set(superdict.symlists.keys())
    used_symlists = set()
    for prop in superdict.properties.values():
        if prop.symlist:
            used_symlists.add(prop.symlist.name)

    not_used_symlists = sorted(all_symlists - used_symlists)
    if not_used_symlists:
        logging.warning(f"Not used global symlists: {not_used_symlists}")
        return True
    return False


def test_all_property_groups_are_used(superdict: SuperDict) -> bool:
    all_prop_groups = set(superdict.property_groups.keys())
    used_prop_groups = set()

    for prop in superdict.properties.values():
        if prop.group:
            used_prop_groups.add(prop.group.name)

    not_used_prop_groups = sorted(all_prop_groups - used_prop_groups)
    if not_used_prop_groups:
        logging.warning(f"Not used property groups: {not_used_prop_groups}")
        return True
    return False


def test_no_unnecessary_global_flags(superdict: SuperDict) -> bool:
    props_to_check = []
    for prop in superdict.properties.values():
        if prop.flags:
            keys_to_skip = {"PPM", "LIMITS", "SYM_LST"}
            flags_set = set(prop.flags.keys())

            diff = flags_set - keys_to_skip

            if not diff:
                # ignore properties with only global flags added by the parser
                continue

            if prop.type.name == "PARENT":
                # ignore parent properties
                continue

            classes_using_property = set(prop.classes.keys())
            specific_flags = set(prop.specific_flags.keys())

            if classes_using_property == specific_flags:
                props_to_check.append((prop.full_name, set(prop.flags.keys())))

    if props_to_check:
        logging.warning(f"Properties with global flags that are not used by any class (specific flags present): {props_to_check}")
        return True
    return False


def test_specific_logic_not_used(superdict: SuperDict) -> bool:
    results = []
    for prop in superdict.properties.values():
        # ignore dynamic properties
        if prop.full_name.startswith("DIAG") and prop.dynamic:
            continue
        # ignore REG.B/REG.I properties as they are cloned from REG.I
        if prop.full_name.startswith("REG.B") or prop.full_name.startswith("REG.I"):
            continue

        classes_using_property = set(prop.classes.keys())

        specific_attributes = [attr for attr in dir(prop) if attr.startswith("specific")
                               and isinstance(getattr(prop, attr), dict)]

        specific_classes = set()
        for specific_attribute in specific_attributes:
            specific_classes.update(getattr(prop, specific_attribute).keys())

        specific_classes_not_used = specific_classes - classes_using_property

        if specific_classes_not_used:
            results.append((prop.full_name, specific_classes_not_used, classes_using_property))

    if results:
        logging.warning(f"Following properties have class specific attributes, but aren't used by these class(es):")
        for result in results:
            logging.warning(f"Property {result[0]}, NOT USED BY CLASSES: {result[1]}, used by: {result[2]}")
        return True
    return False


def test(superdict: SuperDict, yaml_parser):
    logging.info(f"Testing started...")

    tester = Tester()
    tester.register(DocLinksTester(superdict, yaml_parser))

    stack = list()

    stack.append(superdict)

    while stack:
        element = stack.pop()

        if getattr(element, '_seen_by_test', False):
            continue

        if isinstance(element, BaseModel):
            element._seen_by_test = True

            for field_name in element.__fields__.keys():
                field_value = getattr(element, field_name)
                tester.test_element(field_name, field_value, element)
                stack.append(field_value)
        elif isinstance(element, dict):
            # superdict elements for example
            for val in element.values():
                stack.append(val)
        elif isinstance(element, list):
            for child in element:
                stack.append(child)
    tester_results = tester.print_results()

    # additional tests

    test_all_properties_are_used(superdict)
    test_all_types_are_used(superdict)
    test_all_set_setif_get_are_used(superdict)
    test_all_global_symlists_are_used(superdict)
    test_all_property_groups_are_used(superdict)
    test_no_unnecessary_global_flags(superdict)
    test_specific_logic_not_used(superdict)

    # get the output - can also contain test_all_* in the future.
    results = tester_results

    logging.info(f"Testing finished")
    return results
