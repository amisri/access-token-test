import operator
from pathlib import Path
from typing import Optional, Dict, List
import re
from fgc_parser_schemas import (SuperDict, Zone, BootMenu, Property, GetFunction, SetFunction, SetIfFunction, Symlist,
                                Class, Flag, Type)
from pyparser.output.base import BaseJinjaSuperDictOutGen, jinja_fn


class FGC4Generator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate FGC4 stuff.
    """

    RESERVED_CPP_KEYWORDS = [
        "alignas",
        "alignof",
        "and",
        "and_eq",
        "asm",
        "auto",
        "bitand",
        "bitor",
        "bool",
        "break",
        "case",
        "catch",
        "char",
        "char8_t",
        "char16_t",
        "char32_t",
        "class",
        "compl",
        "concept",
        "const",
        "consteval",
        "constexpr",
        "constinit",
        "const_cast",
        "continue",
        "co_await",
        "co_return",
        "co_yield",
        "decltype",
        "default",
        "delete",
        "do",
        "double",
        "dynamic_cast",
        "else",
        "enum",
        "explicit",
        "export",
        "extern",
        "false",
        "float",
        "for",
        "friend",
        "goto",
        "if",
        "inline",
        "int",
        "long",
        "mutable",
        "namespace",
        "new",
        "noexcept",
        "not",
        "not_eq",
        "nullptr",
        "operator",
        "or",
        "or_eq",
        "private",
        "protected",
        "public",
        "register",
        "reinterpret_cast",
        "requires",
        "return",
        "short",
        "signed",
        "sizeof",
        "static",
        "static_assert",
        "static_cast",
        "struct",
        "switch",
        "synchronized",
        "template",
        "this",
        "thread_local",
        "throw",
        "true",
        "try",
        "typedef",
        "typeid",
        "typename",
        "union",
        "unsigned",
        "using",
        "virtual",
        "void",
        "volatile",
        "wchar_t",
        "while",
        "xor",
        "xor_eq"
    ]

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # filters & tests

        # add a suffix to reserved (C++ keywords etc.) symbols
        self.add_filter("fix_symbol", FGC4Generator.fix_symbol)

        # test if property has at least 1 non-dynamic child
        self.add_test("non_dynamic_children",
                      lambda prop: prop.children and any([not child.dynamic for child in prop.children]))

        # work only for class 123 for now!
        try:
            cls = superdict.classes["123"]
        except KeyError:
            return

        # output path
        generated_path = self.output_path / 'sw' / 'fgcd2' / 'generated'

        # generate consts
        self.add_template("consts.h.j2",
                          out_path=generated_path,
                          cls=cls,
                          consts=sorted(cls.consts.values(), key=lambda const: const.name),
                          )

        # symlists

        symlists: Dict[str, Symlist] = {}
        for prop in cls.properties.values():
            if prop.symlist:
                symlists[prop.symlist.name] = prop.symlist

            if prop.class_symlist:
                symlists[prop.class_symlist] = prop.class_symlists[cls.id]

        self.add_template("enums.h.j2",
                          out_path=generated_path,
                          cls=cls,
                          symlists=symlists,
                          get_pascal_name=FGC4Generator.get_pascal_case,

                          )

        # properties init
        self.add_template("properties_init.h.j2",
                          out_path=generated_path,
                          cls=cls,
                          get_property_value=FGC4Generator.get_property_value,
                          get_field_offset=FGC4Generator.get_field_offset,
                          get_PPM_offset=FGC4Generator.get_PPM_offset,
                          get_property_flags=FGC4Generator.get_property_flags,
                          get_property_children=FGC4Generator.get_property_children,
                          is_property_ppm=FGC4Generator.is_property_ppm,
                          get_property_template_arguments=FGC4Generator.get_property_template_arguments,
                          get_property_basic_type=FGC4Generator.get_property_basic_type,
                          get_property_min=FGC4Generator.get_property_min,
                          get_property_max=FGC4Generator.get_property_max,
                          get_property_type=FGC4Generator.get_property_type,
                          get_property_config_flags=FGC4Generator.get_property_config_flags,
                          get_property_get_fn_source=FGC4Generator.get_property_get_fn_source,
                          get_property_set_fn_source=FGC4Generator.get_property_set_fn_source,
                          get_base_address_offset=FGC4Generator.get_base_address_offset,
                          )
        # properties
        self.add_template("properties.h.j2",
                          out_path=generated_path,
                          cls=cls,
                          get_property_children=FGC4Generator.get_property_children,
                          is_property_ppm=FGC4Generator.is_property_ppm,
                          get_property_template_arguments=FGC4Generator.get_property_template_arguments,
                          get_property_value=FGC4Generator.get_property_value,
                          get_field_offset=FGC4Generator.get_field_offset,
                          get_property_flags=FGC4Generator.get_property_flags,
                          get_property_basic_type=FGC4Generator.get_property_basic_type,
                          get_property_min=FGC4Generator.get_property_min,
                          get_property_max=FGC4Generator.get_property_max,
                          get_property_type=FGC4Generator.get_property_type,
                          get_property_config_flags=FGC4Generator.get_property_config_flags,
                          get_property_get_fn_source=FGC4Generator.get_property_get_fn_source,
                          get_property_set_fn_source=FGC4Generator.get_property_set_fn_source,



                          )

    @staticmethod
    def get_property_get_fn_source(prop: Property, cls: Class) -> str:
        # get fn
        get_fn = FGC4Generator.get_property_get_function(prop, cls)
        if get_fn is not None:
            get_fn_name = get_fn.name.lower()
            if get_fn_name in ['integer', 'float', 'string', 'char', 'point']:
                # flags
                flags = prop.class_flags[cls.id]
                if "LIMITS" in flags:
                    return "limits"
                return "automatic"
            return "user"
        return "warning"

    @staticmethod
    def get_property_set_fn_source(prop: Property, cls: Class) -> str:
        # set fn
        set_fn = FGC4Generator.get_property_set_function(prop, cls)
        if set_fn is not None:
            set_fn_name = set_fn.name.lower()
            if set_fn_name in ['integer', 'float', 'string', 'char', 'point']:
                # flags
                flags = prop.class_flags[cls.id]
                if "LIMITS" in flags:
                    return "limits"
                return "automatic"
            return "user"
        return "warning"

    @staticmethod
    def get_property_set_function(prop: Property, cls: Class) -> Optional[SetFunction]:
        try:
            return prop.specific_set[cls.id]
        except KeyError:
            return prop.set

    @staticmethod
    def get_property_get_function(prop: Property, cls: Class) -> Optional[GetFunction]:
        try:
            return prop.specific_get[cls.id]
        except KeyError:
            return prop.get

    @staticmethod
    def get_property_type(prop: Property, cls: Class) -> str:
        # set and get functions
        get_fn = FGC4Generator.get_property_get_function(prop, cls)
        set_fn = FGC4Generator.get_property_set_function(prop, cls)

        # should be one of: setting/derrived_setting/acquisition/configuration/method
        # See fgcd2/source/modules/properties/PropertyType.h

        if get_fn is not None:
            if set_fn is not None:
                # set and get
                return "setting"
            else:
                # only get
                return "acquisition"
        else:
            if set_fn is not None:
                # only set
                return "method"
            else:
                # no set and get
                raise Exception(f"Property {prop.full_name} doesn't have get and set function")

    @staticmethod
    def get_property_config_flags(prop: Property, cls: Class) -> List[str]:
        flags = prop.class_flags[cls.id]
        out_flags = []
        if "LIMITS" in flags:
            out_flags.append("require_limits")
        if "CONFIG" in flags:
            out_flags.append("config_file")
        if "NON_VOLATILE" in flags:
            out_flags.append("non_volatile")
        if "HIDE" in flags:
            out_flags.append("hide")
        if "SUB_DEV" in flags:
            out_flags.append("sub_dev")
        return out_flags

    @staticmethod
    def get_pascal_case(name: str) -> str:
        return "".join([word[0].upper() + word[1:] for word in name.lower().split('_')])

    # @staticmethod
    # def get_camel_case(name: str) -> str:
    #     return "".join([word for word in name.lower().split('_')])

    @staticmethod
    def fix_symbol(name: str) -> str:
        if name.lower() in FGC4Generator.RESERVED_CPP_KEYWORDS:
            return name + "_"
        return name

    @staticmethod
    def get_base_address_offset(prop: Property, cls: Class) -> str:
        specific_value = FGC4Generator.get_property_value(prop, cls)
        is_ppm = FGC4Generator.is_property_ppm(prop, cls)

        macro_regex = r"^\w*\(&([\w.\[\]]*),\W*\w*\)$"
        struct_regex = r"^([\w.\[\]]*)$"
        match_macro = re.match(macro_regex, specific_value)
        match_struct = re.match(struct_regex, specific_value)
        if match_macro:
            # sometimes it's a struct
            nested_struct = re.match(struct_regex, match_macro.group(1))
            if nested_struct:
                elements = match_macro.group(1).split('.')
                # hacky hack
                if "device" in elements[1]:
                    return f"&{elements[0]}.{elements[1]}.{elements[2]} /* {specific_value} */"
                return f"{elements[0]}.{elements[1]} /* {specific_value} */"
            else:
                return f"&{match_macro.group(1)}"
        elif match_struct:
            elements = specific_value.split('.')
            if len(elements) > 1:
                # hacky hack for equipdev platforms
                if not is_ppm and "device" in elements[1]:
                    return f"&{elements[0]}.{elements[1]}.{elements[2]} /* {specific_value} */"


                #if "device" in elements[1]:
                     # return f"{elements[0]}.{elements[1]}.{elements[2]} /* {specific_value} */"
                return f"&{elements[0]}.{elements[1]} /* {specific_value} */"
        return f"FIXME: {specific_value}"

    @staticmethod
    def get_PPM_offset(prop: Property, cls: Class) -> str:
        specific_value = FGC4Generator.get_property_value(prop, cls)

        macro_regex = r"^\w*\(&([\w.\[\]]*),\W*\w*\)$"
        struct_regex = r"^([\w.\[\]]*)$"
        match_macro = re.match(macro_regex, specific_value)
        match_struct = re.match(struct_regex, specific_value)
        if match_macro:
            # sometimes it's a struct
            nested_struct = re.match(struct_regex, match_macro.group(1))
            if nested_struct:
                elements = match_macro.group(1).split('.')
                return f"sizeof({elements[0]}.{elements[1]} /* {specific_value} */ )"
            else:
                return f"&{match_macro.group(1)}"
        elif match_struct:
            elements = specific_value.split('.')
            if len(elements) > 1:
                # hacky hack for equipdev platforms
                if "device" in elements[1]:
                    return f"sizeof({elements[0]}.{elements[1]}.{elements[2]} /* {specific_value} */)"
                return f"sizeof({elements[0]}.{elements[1]} /* {specific_value} */)"
        return f"FIXME: {specific_value}"

    @staticmethod
    def get_field_offset(prop: Property, cls: Class) -> str:
        specific_value = FGC4Generator.get_property_value(prop, cls)

        macro_regex = r"^(\w*)\(&([\w.\[\]]*),(\W*\w*)\)$"
        struct_regex = r"^([\w.\[\]]*)$"
        match_macro = re.match(macro_regex, specific_value)
        match_struct = re.match(struct_regex, specific_value)
        if match_macro:
            macro_name = match_macro.group(1)
            macro_arg1 = match_macro.group(2)
            macro_arg2 = match_macro.group(3)

            supported_macros = {
                # define regMgrParAppValue(REG_PARS,PAR_NAME)       ((REG_PARS)->values.REG_PAR_ ## PAR_NAME)
                "regMgrParAppValue": ("REG_pars", "values.REG_PAR_", ""),
                # define regMgrParValue(REG_MGR,PAR_NAME)           (REG_MGR)->par_values.REG_PAR_ ## PAR_NAME[0]
                "regMgrParValue": ("REG_mgr", "par_values.REG_PAR_", "[0]"),
                # define regMgrVarValue(REG_MGR, VAR_NAME)    ((REG_MGR)->REG_VAR_ ## VAR_NAME)
                "regMgrVarValue": ("REG_mgr", "REG_VAR_", ""),
                # #define refMgrParValue(REF_MGR,PAR_NAME)                (REF_MGR)->REF_PAR_ ## PAR_NAME[0]
                "refMgrParValue": ("REF_mgr", "REF_PAR_", "[0]"),
                # define refMgrVarValue(REF_MGR, VAR_NAME)    ((REF_MGR)->REF_VAR_ ## VAR_NAME)
                "refMgrVarValue": ("REF_mgr", "REF_VAR_", "")
            }

            if macro_name in supported_macros:
                struct_name, field_prefix, field_suffix = supported_macros[macro_name]
                field_name = macro_arg2
                return f"offsetof({struct_name}, {field_prefix}{field_name}{field_suffix})"
        elif match_struct:
            fields = match_struct.group(1).split('.')

            supported_structs = {
                "sig_struct": "SIG_struct",
                "polswitch_mgr": "POLSWITCH_mgr",
                "ref_mgr": "REF_mgr",
                "reg_mgr": "REG_mgr",
                # special case
                "log": {
                    "menus": "LOG_menus",
                    "mgr": "LOG_mgr",
                    "read": "LOG_read",
                    "read_control": "LOG_read_control",
                    "structs": "LOG_structs",
                    "buffers": "LOG_buffers"
                }
            }

            for supported_struct, struct_type in supported_structs.items():
                if supported_struct in fields:
                    index_of_struct = fields.index(supported_struct)
                    if supported_struct == "log":
                        log_struct_index = index_of_struct + 1
                        log_struct = fields[log_struct_index]
                        if log_struct in struct_type:
                            struct_type = struct_type[log_struct]
                            index_of_struct = log_struct_index
                    return f"offsetof({struct_type}, {'.'.join(fields[index_of_struct+1:])})"

            else:
                return "0, // FIXME: can't get (guess) valid offset out of value field"
        else:
            return "0, // FIXME: can't get (guess) valid offset out of value field"

    @staticmethod
    def get_property_children(prop: Property, cls: Class)-> List[Property]:
        """
               Create array of property's children that are present in current class
               """
        res = []
        for child in prop.children:
            if child.dynamic:
                continue
            for classproperty in cls.properties.values():
                if classproperty.full_name == child.full_name:
                    res.append(classproperty)
        return res

    @staticmethod
    def is_property_ppm(prop: Property, cls: Class) -> bool:
        return "PPM" in prop.class_flags[cls.id]

    @staticmethod
    def get_property_fgc_type(prop: Property, cls: Class) -> Type:
        if cls.id in prop.specific_types:
            return prop.specific_types[cls.id]
        return prop.type

    @staticmethod
    def get_property_numels(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_numels:
            return prop.specific_numels[cls.id]
        return prop.numels

    @staticmethod
    def get_property_maxels(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_maxels:
            return prop.specific_maxels[cls.id]
        return prop.maxels

    @staticmethod
    def get_property_value(prop: Property, cls: Class) -> str:
        try:
            return prop.specific_value[cls.id]
        except KeyError:
            return prop.value

    @staticmethod
    def get_property_flags(prop: Property, cls: Class) -> Dict[str, Flag]:
        return prop.class_flags[cls.id]

    @staticmethod
    def get_property_min(prop: Property, cls: Class):
        try:
            return prop.specific_min[cls.id]
        except KeyError:
            return prop.min

    @staticmethod
    def get_property_max(prop: Property, cls: Class):
        try:
            return prop.specific_max[cls.id]
        except KeyError:
            return prop.max

    @staticmethod
    def get_property_symlist(prop: Property, cls: Class) -> Optional[Symlist]:
        if cls.id in prop.class_symlists:
            return prop.class_symlists[cls.id]
        return prop.symlist

    @staticmethod
    def get_property_basic_type(prop: Property, cls: Class) -> str:
        # figure out basic type
        property_type = FGC4Generator.get_property_fgc_type(prop, cls)
        property_type_name = property_type.name

        # get flags
        flags = FGC4Generator.get_property_flags(prop, cls)
        if "SYM_LST" in flags:
            # get symlist...
            symlist = FGC4Generator.get_property_symlist(prop, cls)

            return FGC4Generator.get_pascal_case(symlist.name)

        basic_type = f"??? /* {property_type_name} */"

        if property_type_name == "ABSTIME":
            basic_type = "Abstime"
        elif property_type_name == "ABSTIME8":
            basic_type = "Abstime8"
        elif property_type_name == "CHAR":
            basic_type = "char"
        elif property_type_name == "DEV_NAME":
            pass
        elif property_type_name == "FIFOADC":
            pass
        elif property_type_name == "FLOAT":
            basic_type = "float"
        elif property_type_name.startswith("INT"):
            signed = property_type_name.endswith('S')
            bits = int(property_type_name[3:-1])
            basic_type = f"int{bits}_t"
            if not signed:
                basic_type = f"u{basic_type}"
        elif property_type_name == "LOG":
            pass
        elif property_type_name == "LOGEVT":
            pass
        elif property_type_name == "LOGPSU":
            pass
        elif property_type_name == "MAC":
            pass
        elif property_type_name == "NULL":
            basic_type = "void"
        elif property_type_name == "PARENT":
            pass
        elif property_type_name == "POINT":
            pass
        elif property_type_name == "FL_POINT":
            pass
        elif property_type_name == "RUNLOG":
            pass
        elif property_type_name == "STRING":
            pass
        elif property_type_name == "CYCSEL":
            pass
        elif property_type_name == "UNIXTIME":
            pass
        else:
            raise Exception(f"Unhandled property type {property_type_name} of property {prop.full_name}")
        return basic_type

    @staticmethod
    def get_property_template_arguments(prop: Property, cls: Class) -> str:
        basic_type = FGC4Generator.get_property_basic_type(prop, cls)

        # numels & maxels
        numels = FGC4Generator.get_property_numels(prop, cls)
        maxels = FGC4Generator.get_property_maxels(prop, cls)

        # try to convert maxels into an int
        is_array = False
        is_external_value = False
        is_empty = False
        try:
            maxels = int(maxels)
            if maxels > 1:
                is_array = True
        except ValueError:
            # if maxels.isascii():
            is_array = True

        # value
        value = FGC4Generator.get_property_value(prop, cls)
        if value:
            is_external_value = True

        # is ppm
        is_ppm = FGC4Generator.is_property_ppm(prop, cls)

        # if external, make it a pointer
        if is_external_value:
            basic_type = f"{basic_type}*"

        if is_array:
            return f"{basic_type}, {str(is_ppm).lower()}, {maxels}"
        return f"{basic_type}, {str(is_ppm).lower()}"



