from pathlib import Path
from fgc_parser_schemas import (SuperDict, RegFgc3VariantReadWriteBlockElement, RegFgc3VariantReadWriteBlock)

from pyparser.output.base import BaseJinjaSuperDictOutGen


class VHDLGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate VHDL stuff.
    """

    BLOCKS_NUM = 7

    @staticmethod
    def get_block_default_value(block: RegFgc3VariantReadWriteBlockElement):
        default_value = int(block.default_value, 0)
        return (default_value & 0xFFFF0000) >> 16, default_value & 0x0000FFFF

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        vhdl_path = project_path / 'hw' / 'vhdl'

        for board_name, board in self.superdict.regfgc3.boards.items():
            for device_name, device in board.devices.items():
                for variant_name, variant in device.variants.items():
                    variant_keys = variant.variant.keys()
                    latest_version = max([int(key) for key in variant_keys])
                    api_definition = variant.variant[str(latest_version)]

                    # VHDL directories
                    out_path = vhdl_path / 'regfgc3' / 'boards' / board_name / device_name / variant_name / 'src' / 'rtl'

                    self.add_template(name="variant.j2.vhd",
                                      out_name="A_FGC3CVM_variant_pkg.vhd",
                                      out_path=out_path,
                                      variant_name=variant_name,
                                      variant_value=variant.variant_symbol.value,
                                      latest_version=latest_version)

                    for block_type in ("Read", "Write"):
                        if block_type == "Read":
                            block_arr = api_definition.read_blocks
                            bl_type = RegFgc3VariantReadWriteBlock.Type.Read
                        else:
                            block_arr = api_definition.write_blocks
                            bl_type = RegFgc3VariantReadWriteBlock.Type.Write

                        for block_index in range(self.BLOCKS_NUM):
                            # get block with given block_index
                            for bl in block_arr:
                                if bl.number == block_index:
                                    block = bl
                                    break
                            else:
                                block = RegFgc3VariantReadWriteBlock(number=block_index,
                                                                     title="",
                                                                     elements=[],
                                                                     type=bl_type)

                            self.add_template(name="block.j2.vhd",
                                              out_name=f"A_FGC3CVM_{block_type}_Block_{block_index}_pkg.vhd",
                                              out_path=out_path / 'FGC3CVM' / block_type,
                                              block=block,
                                              get_block_default_value=self.get_block_default_value,
                                              )


