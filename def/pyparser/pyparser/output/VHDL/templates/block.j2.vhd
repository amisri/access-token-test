--//*****************************************************************************
--// ../../{{__output_relative_path__ | replace('\\', '/') }}/{{__output_filename__}}
--// This file is automatically generated - DO NOT EDIT
--// DESCRIPTION : constants used in the Modules controlling the exchange of parameters between
--//    FGC3 <-> Converter Modules
--//        For {{block.type.name }} block: {{block.number}}
--//*****************************************************************************
{% set num_elements = block.elements | length %}
{% set MAX_ELEMENTS = 31 %}

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_std.all;

use work.A_FGC3CVM_constants_pkg.all;

package A_FGC3CVM_{{block.type.name }}_Block_{{block.number}}_pkg is

--//--------------------------------------------------------------
--// Definition for Converter Module Data

constant {{block.type.name | upper}}_BLOCK_{{block.number}}_INSTALLED_ELEMENTS: integer range 0 to 31 := {{ num_elements }};

--//--------------------------------------------------------------
{% set ns = namespace(bit_index=-1) %}
{% if num_elements > 0 %}
--// Definitions for every element

constant {{block.type.name | upper}}_BLOCK_{{block.number}}_ELEMENT_WIDTH: TYPE_Element_width := (
    {% for i in range(MAX_ELEMENTS) %}
        {% if i < block.elements | length %}
	{{ block.elements[i].size_bits }}{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% else %}
	0{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% endif %}
    {% endfor %}
	);

    {# Write default values if this is a write block #}
    {% if block.type.name == "Write" %}
--// Default value for each element

constant WRITE_BLOCK_{{block.number}}_ELEMENT_DEFAULT_VALUE:  TYPE_Element_DefaultValue := (
        {% for i in range(MAX_ELEMENTS) %}
            {% if i < block.elements | length %}
	X"{{ "%04X_%04X" | format( get_block_default_value(block.elements[i])[0], get_block_default_value(block.elements[i])[1] ) }}"{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
            {% else %}
	X"0000_0000"{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
            {% endif %}
        {% endfor %}
	);

    {% endif %}
    {# Write indices of first bits of elements #}
--// First bit of the Element's data to CVM, put as string
--//    Element 0 = 0
--//    Element 1 and on = Previous_First + Previous_Element_Width

constant {{block.type.name | upper}}_BLOCK_{{block.number}}_STRING_ELEMENT_FIRSTBIT:  TYPE_String_ElementBit := (
    {% set ns.bit_index = 0 %}
    {% for i in range(MAX_ELEMENTS) %}
        {% if i < block.elements | length %}
	{{ ns.bit_index }}{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% set ns.bit_index = ns.bit_index + block.elements[i].size_bits %}
        {% else %}
	0{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% endif %}
    {% endfor %}
	);

    {# Write indices of last bits of elements #}
--// Last bit of the Element's data to CVM, put as string
--//    Element 0 and on: First + Element_Width - 1

constant {{block.type.name | upper}}_BLOCK_{{block.number}}_STRING_ELEMENT_LASTBIT:  TYPE_String_ElementBit := (
    {% set ns.bit_index = -1 %}
    {% for i in range(MAX_ELEMENTS) %}
        {% if i < block.elements | length %}
            {% set ns.bit_index = ns.bit_index + block.elements[i].size_bits %}
	{{ ns.bit_index }}{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% else %}
	0{{"," if not loop.last }}{{"\t"}}--// Element {{i}}
        {% endif %}
    {% endfor %}
	);
    {# write the total number of bits #}
    {% set ns.bit_index = ns.bit_index + 1 %}

--// Total number of bits of the Elements to CVM, put as string

constant {{block.type.name | upper}}_BLOCK_{{block.number}}_STRING_TOTAL_BITS: integer := ( {{ns.bit_index}} );

    {# Write element names #}
--// Definition of the Data record
type TYPE_{{block.type.name }}_Block_{{block.number}} is
record
    {% for i in range(MAX_ELEMENTS) %}
        {% if i < block.elements | length %}
	{{ "%-35s" | format(block.elements[i].name) }}: std_logic_vector (({{block.type.name | upper}}_BLOCK_{{block.number}}_ELEMENT_WIDTH({{"%02d" | format(i)}}) - 1) downto 0);  --// Element {{i}}
        {% endif %}
    {% endfor %}
end record;

{% else %}

--// No Elements instantiated for this block

{% endif %}
--//--------------------------------------------------------------
--//--------------------------------------------------------------
--//--------------------------------------------------------------

end package A_FGC3CVM_{{block.type.name }}_Block_{{block.number}}_pkg;

package body A_FGC3CVM_{{block.type.name }}_Block_{{block.number}}_pkg is

end;
{{ '' }}{# empty line at the end of file hack... #}