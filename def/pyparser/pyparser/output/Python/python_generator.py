from pathlib import Path
from fgc_parser_schemas import SuperDict, Class

from pyparser.output.base import BaseJinjaSuperDictOutGen

from pyparser.output.base import get_symlist, get_const_value


class PythonGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate Python stuff.
    """

    @staticmethod
    def get_unpack_string(cls: Class):
        unpack_vals = []
        next_expected_address = 0
        for zone in sorted(cls.pubdata.children, key=lambda z: z.data_position):
            if zone.data_position < 0:
                continue
            # Check for gap in addresses and pad with reserved bytes if necessary

            if zone.address != next_expected_address:
                num_pad_bytes = int((zone.address - next_expected_address) / 8)
                unpack_vals.append(f"{num_pad_bytes}x")
            unpack_string = ""
            if zone.property.type.name.startswith("INT"):
                # property is an integer
                if zone.numbits == 8:
                    if zone.property.type.name.endswith("U"):
                        unpack_string += 'B'
                    else:  # 'S'
                        unpack_string += 'b'
                elif zone.numbits == 16:
                    if zone.property.type.name.endswith("U"):
                        unpack_string += 'H'
                    else:  # 'S'
                        unpack_string += 'h'
                elif zone.numbits == 32:
                    if zone.property.type.name.endswith("U"):
                        unpack_string += 'I'
                    else:  # 'S'
                        unpack_string += 'i'
            elif zone.property.type.name == "FLOAT":
                # property is a float
                # Unpack initially as 32-bit network byte-order integer
                unpack_string += 'f'
            else:
                raise Exception(f"Not expected type for publication field {zone.name} in class FGC_{cls.id}")
            unpack_vals.append(unpack_string)

            next_expected_address = zone.address + zone.numbits

        return unpack_vals

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # path
        fgc_python_decoders_path = project_path / 'sw' / 'clients' / 'python' / 'pyfgc_decoders' / 'pyfgc_decoders'

        for cls in self.superdict.classes.values():
            if cls.platform.name == "fgc4":
                continue
            fgc_python_cls_path = fgc_python_decoders_path / 'classes'
            self.add_template("fgc_class_py.j2",
                              out_path=fgc_python_cls_path,
                              out_name=f"fgc_{cls.id}.py",
                              cls=cls,
                              unpack_vals=PythonGenerator.get_unpack_string(cls),
                              get_symlist=get_symlist,
                              get_const_value=get_const_value)
