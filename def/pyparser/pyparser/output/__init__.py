from .C.c_cpp_generator import CppGenerator
from .cclibs.cclibs_generator import CclibsGenerator
from .Config.config_generator import ConfigGenerator
from .Doc.doc_generator import DocsGenerator
from .EPICS.epics_generator import EPICSGenerator
from .FGC_code.fgc_code_generator import FGCCodeGenerator
from .Perl.perl_generator import PerlGenerator
from .Python.python_generator import PythonGenerator
from .Schema.schema_generator import SchemaGenerator
from .Tango.tango_generator import TangoGenerator
from .FGC4.fgc4_generator import FGC4Generator
# from .VHDL.vhdl_generator import VHDLGenerator
GENERATORS = [
    CppGenerator,
    CclibsGenerator,
    ConfigGenerator,
    DocsGenerator,
    EPICSGenerator,
    FGCCodeGenerator,
    PerlGenerator,
    PythonGenerator,
#    SchemaGenerator,
    TangoGenerator,
    # VHDLGenerator,
    FGC4Generator
]

GENERATORS_NAMES = {
    generator.__name__.replace('Generator', '').lower(): generator
    for generator in GENERATORS
}
