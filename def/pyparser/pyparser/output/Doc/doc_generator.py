from pathlib import Path
from typing import List, Optional, Dict, Set

import re

from jinja2 import pass_context
from jinja2.runtime import Context

from fgc_parser_schemas import (SuperDict, MemoryMap, Zone, Class, SymlistConst)

from pyparser.output.base import BaseJinjaSuperDictOutGen

links_paths = {
            "icon_path": "images/logo.gif",
            "style_sheet": "style.css",

            "top_path": "../..",
            "cern_link": "http://cern.ch",
            "about_link": "static/Project/about.htm",
            "sitemap_link": "SiteMap.htm",

            "class_index_path": "ClassIndex.htm",
            "class_symlist_index_path": "ClassSymListIndex",
            "class_path": "Class",
            "codes_path": "Codes.htm",
            "components_path": "Components.htm",
            "dim_type_path": "DimType",
            "dim_type_index_path": "DimTypeIndex.htm",
            "errors_path": "Errors.htm",
            "flags_path": "Flags.htm",
            "getfuncs_path": "GetFuncs.htm",
            "getoptions_path": "GetOptions.htm",
            "hardware_path": "Hw",
            "hardware_index_path": "HwIndex",
            "incfiles_path": "IncFiles.htm",
            "menu_path": "menu.txt",
            "pcs_flags_path": "PcsFlags.htm",
            "platform_path": "Platform",
            "platform_index_path": "PlatformIndex.htm",
            "property_path": "Property",
            "property_index_path": "PropertyIndex.htm",
            "properties_index_class_path": "PropertyIndexClasses.htm",
            "run_codes_path": "RunCodes.htm",
            "runlog_path": "Runlog.htm",
            "setfuncs_path": "SetFuncs.htm",
            "setiffuncs_path": "SetifFuncs.htm",
            "sitemap_path": "SiteMap.htm",
            "sitemenu_path": "SiteMenu.htm",
            "symlists_path": "SymList",
            "system_path": "System",
            "system_index_path": "SystemIndex.htm",
            "state_path": "States",
            "trans_path": "Trans",
            "types_path": "Types.htm",
}


class DocsGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate HTML documentation
    """

    @pass_context
    def parse_docs(self, context: Context, element) -> str:
        # get the template name from context
        template_name = context.name

        # doc attribute of this element needs be parsed differently
        # 1) in case of class specific properties:
        #   - it needs to display only documentation specific to given class
        #   - all the links of properties should point to the same class specific properties
        # 2) in case of class generic properties:
        #   - only generic documentation (specific one filtered out) + div with star
        #   - links pointing to the generic properties
        # 3) in case of class symlists:
        #   - should point to class specific page

        is_property_specific = template_name == "Property_specific.htm.j2"
        is_property_generic = template_name == "Property.htm.j2"
        is_class_symlist = template_name == "SymList.htm.j2" and context.get('class_id') is not None

        if not is_property_specific and not is_property_generic and not is_class_symlist:
            return element.processed_docs_

        value = element.doc
        # 1) filter the documentation for given classes if necessary

        # match for example: <div class="-62"> ... </div>.
        # class ids should be comma separated, with optional prefix of "+" or "-" in case of blacklists.
        # An additional symbol '*' can be used, both with prefix of '+' or '-'.
        # It means that given part of documentation should (+) or shouldn't (-) end up in the generic property page.
        doc_split_regex = re.compile(r'\s*<div class="([\d,+\- \*]*)">((\s|\S)*?)<\/div>')

        # get class from the context
        cls: Optional[Class] = None
        if is_property_specific:
            cls = context.get("cls")
        elif is_class_symlist:
            cls = self.superdict.classes[context.get('class_id')]

        # get superdict from the context
        superdict: SuperDict = context.get("superdict")

        def parse_docs_per_class(match: re.Match) -> str:
            classes_selector = match.group(1)
            classes = classes_selector.split(',')
            whitelist = [cls_id.replace('+', '').strip() for cls_id in classes if not cls_id.strip().startswith('-')]
            blacklist = [cls_id.replace('-', '').strip() for cls_id in classes if cls_id.strip().startswith('-')]

            whitelisted_star = '*' in whitelist
            blacklisted_star = '*' in blacklist

            # check if it's valid
            if not all(cls_id in superdict.classes.keys() for cls_id in whitelist if cls_id != "*") or \
                    not all(cls_id in superdict.classes.keys() for cls_id in blacklist if cls_id != "*"):
                raise Exception(f"Specific class documentation selector contains non-existing classes "
                                f"(not including special '*'): "
                                f"{classes_selector}")

            # class generic properties - no cls
            if cls is None:
                # whitelisted part
                if whitelisted_star:
                    return match.group(0)
                # no whitelisted part
                return ""
            # class specific properties - there's cls
            else:
                if cls.id in blacklist:
                    # documentation is hidden
                    return ""
                elif cls.id in whitelist:
                    # return everything
                    return match.group(0)
                elif blacklisted_star:
                    # this should appear everywhere in class specific, but not in generic
                    return match.group(0)
                elif whitelisted_star:
                    # this belongs only to class generic properties
                    return ""
                elif whitelist:
                    # some whitelisted classes, but not this one
                    return ""
                else:
                    # not blacklisted, not whitelisted, no stars
                    # get everything
                    return match.group(0)

        if not is_class_symlist:
            # not needed for class symlists
            value = doc_split_regex.sub(parse_docs_per_class, value)

        # 2) create all the links (no validation here, already done in testers.py)

        #  match new style all types of links, note " (quote) mark at the beginning
        new_links_regex = re.compile(r'"{(\w+) (\w+)(.*?)}')
        # match old style all types of links, no quote mark at the beginning
        old_links_regex = re.compile(r'{(\w+) (\w+)(.*?)}')

        link_types = {
            "ERR": links_paths["errors_path"],
            "FLAG": links_paths["flags_path"],
            "GET": links_paths["getfuncs_path"],
            "GOPT": links_paths["getoptions_path"],
            "SET": links_paths["setfuncs_path"],
            "SETIF": links_paths["setiffuncs_path"],
            "TYPE": links_paths["types_path"]
        }
        splink_types = {
            "PROP": links_paths["property_path"],
            "STAT": links_paths["state_path"],
            "SYMLSTC": {
                'global': links_paths["symlists_path"],
                'class': links_paths["class_symlist_index_path"]
            },
            "TRAN": links_paths["trans_path"]
        }

        # special case for property specific link type
        # point property links to specific properties
        # and also symlist links
        if is_property_specific or is_class_symlist:
            splink_types["PROP"] = splink_types["PROP"] + "_" + cls.id + "_"
            splink_types["SYMLSTC"]['class'] = f"Class{cls.id}_SymList_"

        def get_symlstc_address(symlist_name: str) -> str:
            if symlist_name in superdict.class_symlists:
                return splink_types["SYMLSTC"]["class"] + symlist_name
            return splink_types["SYMLSTC"]["global"] + symlist_name

        def new_links_replace_fn(match: re.Match) -> str:
            link_type = match.group(1)
            if link_type in link_types:
                return f'"{link_types[link_type]}#{match.group(2)}'
            elif link_type in splink_types:
                if link_type == "SYMLSTC":
                    # if it is a class symlist, use another link
                    return f'"{get_symlstc_address(match.group(2))}.htm#{match.group(2)}{match.group(3)}'
                return f'"{splink_types[link_type]}{match.group(2)}.htm#{match.group(2)}{match.group(3)}'
            return f"NOTFOUND: {link_type}{match.group(2)}{match.group(3)}"
            # raise Exception(f"Failed to parse link! {str(match)}")

        def old_links_replace_fn(match: re.Match) -> str:
            link_type = match.group(1)
            if link_type in link_types:
                return f'<A href="{link_types[link_type]}#{match.group(2)}">{match.group(2)}</A>'
            elif link_type in splink_types:
                if link_type == "SYMLSTC":
                    # for symlist links, do not show symlist name in the link.
                    # skip first character in match, which is a dot.
                    # also if it is a class symlist, use another link
                    return f'<A href="{get_symlstc_address(match.group(2))}.htm#{match.group(2)}{match.group(3)}">' \
                           f'{match.group(3)[1:]}</A>'
                return f'<A href="{splink_types[link_type]}{match.group(2)}.htm#{match.group(2)}{match.group(3)}">' \
                       f'{match.group(2)}{match.group(3)}</A>'
            return f"NOTFOUND: {link_type}{match.group(2)}{match.group(3)}"
            # raise Exception(f"Failed to parse link! {str(match)}")

        value = new_links_regex.sub(new_links_replace_fn, value)
        return old_links_regex.sub(old_links_replace_fn, value)

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        self.output_path = project_path / 'website' / 'gendoc' / 'def'

        # setup parsing docs

        self.jinja_env.filters["docs"] = self.parse_docs

        # sort classes
        self.sorted_classes = sorted(self.superdict.classes.values(), key=lambda cls: int(cls.id))

        self.gen_misc_docs()
        self.gen_symlists_docs()
        self.gen_dims_docs()
        self.gen_systems_docs()
        self.gen_platform_docs()
        self.gen_classes_docs()
        self.gen_properties_docs()

    def gen_misc_docs(self):
        self.add_template("Codes.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Codes"
                          )
        self.add_template("Components.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Component Types"
                          )
        self.add_template("Errors.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Error Messages"
                          )

        self.add_template("Flags.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Property Flags")

        self.add_template("GetFuncs.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Get Functions")
        self.add_template("SetFuncs.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Set Functions")

        self.add_template("SiteMap.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Project Site Map",
                          sorted_classes=self.sorted_classes)

        self.add_template("GetOptions.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Get Options")

        self.add_template("Runlog.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Run Log Entries")

        self.add_template("SetifFuncs.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Setif Functions")

        self.add_template("Types.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Property Types")

    def gen_symlists_docs(self):
        for k, v in self.superdict.symlists.items():
            self.add_template("SymList.htm.j2",
                              out_path=self.output_path,
                              out_name=f"SymList{k}.htm",
                              **links_paths,
                              title=v.title,
                              symlist=v,
                              sorted_classes=self.sorted_classes,
                              class_id=None)

        # generate also 'generic' pages for class symlists
        for symlist_name, cls_id_symlist in self.superdict.class_symlists.items():
            # Dict[symbol, Dict[classID, symbol]
            symbols: Dict[str, Dict[str, SymlistConst]] = dict()
            for cls_id, symlist in cls_id_symlist.items():
                for symbol in symlist.consts:
                    symbols.setdefault(symbol.symbol, dict())[cls_id] = symbol
            self.add_template("ClassSymListIndex.htm.j2",
                              out_path=self.output_path,
                              out_name=f"ClassSymListIndex{symlist_name}.htm",
                              **links_paths,
                              title=f"{symlist_name} class symlist index",
                              symlist_name=symlist_name,
                              symbols=symbols)

    def gen_dims_docs(self):
        self.add_template("DimTypeIndex.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="DIM Type Index")

        # dim types
        for k, v in self.superdict.dim_types.items():
            self.add_template("DimType.htm.j2",
                              out_path=self.output_path,
                              out_name=f"DimType{k}.htm",
                              **links_paths,
                              title=f"DIM {v.type}",
                              dim_type=v)

    def gen_systems_docs(self):
        self.add_template("SystemIndex.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title=f"System Index")
        for k, v in self.superdict.systems.items():
            groups_with_1_barcode = [group for group in v.groups.values() if len(group.barcodes) == 1]
            groups_with_barcodes = [group for group in v.groups.values() if len(group.barcodes) > 1]
            self.add_template("System.htm.j2",
                              out_path=self.output_path,
                              out_name=f"System{v.type}.htm",
                              **links_paths,
                              title=f"{v.type} System",
                              system=v,
                              groups_with_1_barcode=groups_with_1_barcode,
                              groups_with_barcodes=groups_with_barcodes)

    def gen_properties_docs(self):
        # Write property index (global)
        self.add_template("PropertyIndex.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Properties Index"
                          )

        self.add_template("PropertyIndexClasses.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title="Properties Index per Class",
                          sorted_classes=self.sorted_classes,
                          )

        # Write specific property documentation for every top level property for every class
        for cls in self.superdict.classes.values():
            for prop in filter(lambda p: p.parent is None, cls.properties.values()):
                self.add_template("Property_specific.htm.j2",
                                  out_name=f"Property_{cls.id}_{prop.name}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"Class {cls.id} {prop.name} Properties",
                                  generic=False,
                                  cls=cls,
                                  prop=prop)

        # Write (generic) documentation for every top level
        for prop in filter(lambda p: p.parent is None, self.superdict.properties.values()):
            self.add_template("Property.htm.j2",
                              out_name=f"Property{prop.name}.htm",
                              out_path=self.output_path,
                              **links_paths,
                              title=f"{prop.name} Properties",
                              generic=True,
                              cls=None,
                              prop=prop)

    def gen_classes_docs(self):
        # class index
        self.add_template("ClassIndex.htm.j2",
                          out_path=self.output_path,
                          title="Class Index",
                          **links_paths,
                          sorted_classes=self.sorted_classes)

        # Write docs for each class
        for cls in self.superdict.classes.values():

            has_config_properties = any([True for prop in cls.properties.values() if "CONFIG" in prop.flags])

            self.add_template("Class.htm.j2",
                              out_name=f"Class{cls.id}.htm",
                              out_path=self.output_path,
                              **links_paths,
                              title=f"Class {cls.id}: {cls.name.upper()}",
                              cls=cls,
                              has_config_properties=has_config_properties)

            # Write docs for class symlist

            for symlist in cls.symlists.values():
                self.add_template("SymList.htm.j2",
                                  out_name=f"Class{cls.id}_SymList_{symlist.name}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{symlist.title}",
                                  class_id=cls.id,
                                  symlist=symlist,
                                  sorted_classes=self.sorted_classes)

            # Class properties
            self.add_template("Class_properties.htm.j2",
                              out_name=f"Class_{cls.id}_properties.htm",
                              out_path=self.output_path,
                              **links_paths,
                              title=f"Class {cls.id} Properties",
                              cls=cls)

            # config properties
            self.add_template("Class_config_properties.htm.j2",
                              out_name=f"Class_{cls.id}_config_properties.htm",
                              out_path=self.output_path,
                              **links_paths,
                              title=f"Class {cls.id} Config Properties",
                              cls=cls)

            # pubdata

            def sort_pubdata(zone: Zone) -> List[Zone]:
                return sorted(zone.children, key=lambda z: (z.byte_address, z.address % 8))

            def recurse_class_pubdata(zone: Zone, cls):
                for i, child in enumerate(zone.children):
                    if i == 0:
                        prev_node = None
                    else:
                        prev_node = zone.children[i - 1]

                    if i == len(zone.children) - 1:
                        next_node = None
                    else:
                        next_node = zone.children[i + 1]

                    self.add_template("Class_pubdata.htm.j2",
                                      out_name=f"Class{cls.id}_pubdata{child.full_name}.htm",
                                      out_path=self.output_path,
                                      **links_paths,
                                      title=f"Class {zone.cls.id}: Published Data",
                                      memzone=child,
                                      prev_memzone=prev_node,
                                      next_memzone=next_node,
                                      cls=cls,
                                      sort_pubdata=sort_pubdata)
                    recurse_class_pubdata(child, cls)

            self.add_template("Class_pubdata.htm.j2",
                              out_name=f"Class{cls.id}_pubdata.htm",
                              out_path=self.output_path,
                              **links_paths,
                              title=f"Class {cls.id}: Published Data",
                              cls=cls,
                              memzone=cls.pubdata,
                              prev_memzone=None,
                              next_memzone=None,
                              sort_pubdata=sort_pubdata)
            recurse_class_pubdata(cls.pubdata, cls)

    def gen_platform_docs(self):
        # platforms documentation
        # platform index
        self.add_template("PlatformIndex.htm.j2",
                          out_path=self.output_path,
                          **links_paths,
                          title=f"Platform Index")

        # write for each platform
        for k_platform, v_platform in self.superdict.platforms.items():
            # platform page
            self.add_template("Platform.htm.j2",
                              out_path=self.output_path,
                              out_name=f"Platform_{v_platform.name}.htm",
                              **links_paths,
                              title=f"Platform {v_platform.id}: {v_platform.name.upper()}",
                              platform=v_platform)

            # generate documentation for boot menu
            if v_platform.boot_menu:
                self.add_template("BootMenu.htm.j2",
                                  out_name=f"Platform_{v_platform.name}_boot_menu_{v_platform.boot_menu.id}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{v_platform.name.upper()} Boot Menu {v_platform.boot_menu.id}",
                                  boot_menu=v_platform.boot_menu,
                                  prev_menu=None,
                                  next_menu=None)

                def recurse_boot_menu(boot_menu):
                    for i, child in enumerate(boot_menu.menus):
                        if i == 0:
                            prev_node = None
                        else:
                            prev_node = boot_menu.menus[i-1]

                        if i == len(boot_menu.menus)-1:
                            next_node = None
                        else:
                            next_node = boot_menu.menus[i+1]

                        self.add_template("BootMenu.htm.j2",
                                          out_name=f"Platform_{v_platform.name}_boot_menu_{child.id}.htm",
                                          out_path=self.output_path,
                                          **links_paths,
                                          title=f"{v_platform.name.upper()} Boot Menu {child.id}",
                                          boot_menu=child,
                                          prev_menu=prev_node,
                                          next_menu=next_node)
                        recurse_boot_menu(child)
                recurse_boot_menu(v_platform.boot_menu)

                # boot menu summary
                self.add_template("BootMenuSummary.htm.j2",
                                  out_name=f"Platform_{v_platform.name}_boot_menu_summary.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{v_platform.name.upper()} Boot Menu",
                                  boot_menu=v_platform.boot_menu)

            # write index of hardware
            if v_platform.hardware:
                self.add_template("HwIndex.htm.j2",
                                  out_name=f"HwIndex{v_platform.name}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{v_platform.name.upper()} Hardware Index",
                                  platform=v_platform)

            # Write documentation for testing of hardware
            hardware = list(v_platform.hardware.values())
            for i, hw in enumerate(hardware):
                # top level HW documentation

                self.add_template("HwTopLevel.htm.j2",
                                  out_name=f"Hw{hw.lhc_id}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{hw.label}",
                                  hw=hw,
                                  prev_hw=None if i == 0 else hardware[i-1],
                                  next_hw=None if i == len(hardware)-1 else hardware[i+1]
                                  )
                # Write documentation for xilinxes
                for xilinx in hw.xilinxes:
                    if not xilinx.tester and xilinx.programs:
                        self.add_template("HwXilinx.htm.j2",
                                          out_name=f"Hw{hw.lhc_id}_xilinx{xilinx.ic_number}.htm",
                                          out_path=self.output_path,
                                          **links_paths,
                                          title=f"{xilinx.hardware.label} Xilinx {xilinx.ic_number}",
                                          xilinx=xilinx)
                # Write documentation for connectors
                sig_type_names = {"A": "Analog",
                                  "D": "Dynamic",
                                  "G": "Ground",
                                  "I": "Independent",
                                  "S": "Static",
                                  "P": "Power",
                                  "U": "Undefined"}
                self.add_template("HwConnect.htm.j2",
                                  out_name=f"Hw{hw.lhc_id}_connect.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{hw.label} Connectors",
                                  hw=hw,
                                  sig_type_names=sig_type_names)
            # memory maps

            def sort_children(zone: MemoryMap) -> List[MemoryMap]:
                if zone.endian == MemoryMap.Endian.BIG:
                    sort = sorted(zone.children, key=lambda z: z.address % 8, reverse=True)
                    return sorted(sort, key=lambda z: z.byte_address)

                # little
                sort = sorted(zone.children, key=lambda z: z.address % 8)
                return sorted(sort, key=lambda z: z.byte_address)

            def recurse_memmap(memmap: MemoryMap, name: str):
                for i, child in enumerate(memmap.children):
                    if i == 0:
                        prev_node = None
                    else:
                        prev_node = memmap.children[i - 1]

                    if i == len(memmap.children) - 1:
                        next_node = None
                    else:
                        next_node = memmap.children[i + 1]

                    self.add_template("Platform_memmap.htm.j2",
                                      out_name=f"Platform_{v_platform.name}_memmap_{name}_{child.full_name}.htm",
                                      out_path=self.output_path,
                                      **links_paths,
                                      title=f"{v_platform.name.upper()} {name} MemMap {'[TOP]' if not memmap.full_name else memmap.full_name}",
                                      memmap=child,
                                      map_name=name,
                                      prev_memmap=prev_node,
                                      next_memmap=next_node,
                                      sort_children=sort_children
                                      )
                    recurse_memmap(child, name)

            for k_map, v_map in v_platform.memmaps.items():
                self.add_template("Platform_memmap.htm.j2",
                                  out_name=f"Platform_{v_platform.name}_memmap_{k_map}.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{v_platform.name.upper()} {k_map} MemMap",
                                  memmap=v_map,
                                  map_name=k_map,
                                  prev_memmap=None,
                                  next_memmap=None,
                                  sort_children=sort_children)
                recurse_memmap(v_map, name=k_map)

                self.add_template("Platform_memmap_summary.htm.j2",
                                  out_name=f"Platform_{v_platform.name}_memmap_{k_map}_summary.htm",
                                  out_path=self.output_path,
                                  **links_paths,
                                  title=f"{v_platform.name.upper()} {k_map} MemMap",
                                  memmap=v_map,
                                  map_name=k_map,
                                  sort_children=sort_children)
