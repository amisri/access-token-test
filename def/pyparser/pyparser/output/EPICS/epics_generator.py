from pathlib import Path
from typing import Dict, Union
import re
from functools import partial
from fgc_parser_schemas import (SuperDict, Class, Symlist, Property, SymlistConst)

from pyparser.output.base import BaseJinjaSuperDictOutGen
from pyparser.output.base import get_symlist, get_const_value


class EPICSGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate EPICS stuff.
    """

    def get_size_40_description(self, prop: Union[Property, SymlistConst]):
        if hasattr(prop, 'kt_title') and prop.kt_title:
            descr = prop.kt_title
        else:
            descr = prop.title

        if len(descr) > 40:
            self.logger.warning(f"Property {prop.full_name} title used for EPICS description is longer than 40 chars. "
                                f"Create shorter version in 'kt_title' attribute.")

        return descr[0:41]

    @staticmethod
    def get_consts_with_value(sym: Symlist, cls: Class, superdict: SuperDict):
        res = []
        for const in sym.consts:
            if get_const_value(const, cls, superdict) is not None:
                res.append(const)
        return res

    @staticmethod
    def get_units(prop: Property, axis=None):
        default_unit = ""
        field = "kt_units"
        if axis:
            assert axis == "x" or axis == "y"
            field += "_" + axis
        if getattr(prop, field):
            return getattr(prop, field)
        return default_unit

    @staticmethod
    def get_precision(prop: Property, axis=None):
        default_precision = "3"
        field = "kt_precision"
        if axis:
            assert axis == "x" or axis == "y"
            field += "_" + axis
        if getattr(prop, field):
            return getattr(prop, field)
        return default_precision

    def get_enum_inputs(self, cls: Class):
        # split into zone + zone name + list of up to 16 enum fields
        zones = []
        for zone in sorted(cls.pubdata.children, key=lambda zone: zone.data_position):
            if zone.data_position >= 0 and "SYM_LST" in zone.property.flags and "BIT_MASK" not in zone.property.flags:
                symlist = get_symlist(zone, cls)
                if symlist:
                    consts = self.get_consts_with_value(symlist, cls, self.superdict)
                    consts_size = len(consts)
                    for i, const in enumerate(consts):
                        if i % 16 == 0:
                            zones.append((
                                zone,
                                zone.full_name + ((":" + str(int(i/16))) if consts_size >= 16 else ""),
                                consts[i:i+16]
                            ))
        return zones

    @staticmethod
    def get_epics_record_type(prop: Property):
        # NULL               - NULL     - stringin
        # STRING             - W_STRING - waveform
        # FLOAT              - REAL     - ai/ao
        # ABSTIME            - REAL     - ai/ao
        # ABSTIME8           - REAL     - ai/ao
        # INT8/16/32         - INTEGER  - longin/longout
        # SYM_LST & BIT_MASK - W_STRING - waveform
        # SYM_LST            - STRING   - stringin/stringout
        # POINT              - POINT    - 2 waveforms (time & ref)
        if prop.children:
            # this property should not match any EPICS record
            return "unknown"
        elif "SYM_LST" in prop.flags:
            if "BIT_MASK" in prop.flags:
                # Decoded bitmask property: waveform record (string)
                return "W_STRING"
            else:
                # Decoded enum property: string record
                return "STRING"
        elif prop.type.name == "NULL":
            # Null property (set-only): string record (with value ignored)
            return "NULL"
        elif prop.type.name == "STRING":
            # String property: waveform record (string)
            return "W_STRING"
        elif prop.type.name == "FLOAT":
            # Float property: analog record
            return "REAL"
        elif prop.type.name == "ABSTIME":
            # Abstime property: analog record
            return "REAL"
        elif prop.type.name == "ABSTIME8":
            # Abstime property: analog record
            return "REAL"
        elif prop.type.name.startswith("INT"):
            # Integer properties (any size): long record
            return "INTEGER"
        elif prop.type.name == "POINT":
            # Point properties (ref. tables): 2 waveform records (x/y axis data)
            return "POINT"
        else:
            return "unknown"

    def test_all_epics_properties_are_implemented(self, cls: Class):
        for prop in cls.properties.values():
            if prop.kt and not prop.children:
                prop_type = self.get_epics_record_type(prop)
                if prop_type == "NULL" and prop.set is not None:
                    pass
                elif prop_type == "W_STRING" and prop.get is not None:
                    pass
                elif prop_type == "W_STRING" and prop.set is not None:
                    pass
                elif prop_type == "STRING" and prop.get is not None:
                    pass
                elif prop_type == "STRING" and prop.set is not None:
                    pass
                elif prop_type == "REAL" and prop.get is not None:
                    pass
                elif prop_type == "REAL" and prop.set is not None:
                    pass
                elif prop_type == "INTEGER" and prop.get is not None:
                    pass
                elif prop_type == "INTEGER" and prop.set is not None:
                    pass
                elif prop_type == "POINT" and (prop.get is not None or prop.set is not None):
                    pass
                elif prop_type == "POINT" and prop.get is not None:
                    pass
                elif prop_type == "POINT" and prop.set is not None:
                    pass
                else:
                    raise Exception(f"Property {prop.full_name} was not implemented!")

    @staticmethod
    def get_zone_count(cls: Class):
        zone_count = 0
        for zone in cls.pubdata.children:
            if "SYM_LST" in zone.property.flags:
                zone_count += 2
            else:
                zone_count += 1
        return zone_count

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # paths

        fgcepics_path = project_path / 'sw' / 'kt' / 'fgcepics'

        fgcudp_path = fgcepics_path / 'fgcudpSup'
        fgcudp_db_path = fgcudp_path / 'Db'
        fgcudp_src_path = fgcudp_path / 'src'
        fgcudp_src_classes_path = fgcudp_src_path / 'classes'
        fgcudp_src_common_path = fgcudp_src_path / 'common'

        fgccmd_path = fgcepics_path / 'fgccmdSup'
        fgccmd_db_path = fgccmd_path / 'Db'

        for cls in self.superdict.classes.values():
            if not cls.kt:
                continue
            self.test_all_epics_properties_are_implemented(cls)
            # records

            self.add_template("fgcudp_class.substitutions.j2",
                              out_path=fgcudp_db_path,
                              out_name=f"fgcudp_class_{cls.id}.substitutions",
                              cls=cls,
                              get_size_40_description=partial(EPICSGenerator.get_size_40_description, self),
                              get_symlist=get_symlist,
                              get_const_value=get_const_value,
                              get_consts_with_value=EPICSGenerator.get_consts_with_value,
                              enum_inputs=self.get_enum_inputs(cls),
                              get_precision=EPICSGenerator.get_precision,
                              get_units=EPICSGenerator.get_units,
                              )
            self.add_template("fgccmd_class.substitutions.j2",
                              out_path=fgccmd_db_path,
                              out_name=f"fgccmd_class_{cls.id}.substitutions",
                              cls=cls,
                              get_record_type=EPICSGenerator.get_epics_record_type,
                              get_size_40_description=partial(EPICSGenerator.get_size_40_description, self),
                              get_precision=EPICSGenerator.get_precision,
                              get_units=EPICSGenerator.get_units,
                              )

            # EPICS UDP device support C++ files and symlinks
            self.add_template("class_params.h.j2",
                              out_path=fgcudp_src_classes_path / cls.id,
                              out_name=f"class_{cls.id}_params.h",
                              cls=cls,
                              zone_count=EPICSGenerator.get_zone_count(cls))
            self.add_template("class_params.cpp.j2",
                              out_path=fgcudp_src_classes_path,
                              out_name=f"class_{cls.id}_params.cpp",
                              cls=cls,
                              get_symlist=get_symlist)
            # symlinks

            stat_h = fgcudp_src_classes_path / cls.id / f"{cls.id}_stat.h"
            stat_h_target = Path(f"../../../../../../inc/classes/{cls.id}/{cls.id}_stat.h")

            # create output directory structure
            self._create_output_path(stat_h.parent)

            sub_defconst = fgcudp_src_classes_path / cls.id / "sub_defconst.h"
            sub_defconst_target = Path(f"../../../../../../inc/classes/{cls.id}/sub_defconst.h")

            sub_defsyms = fgcudp_src_classes_path / cls.id / "sub_defsyms.h"
            sub_defsyms_target = Path(f"../../../../../../inc/classes/{cls.id}/sub_defsyms.h")

            try:
                stat_h.symlink_to(stat_h_target)
                sub_defconst.symlink_to(sub_defconst_target)
                sub_defsyms.symlink_to(sub_defsyms_target)
            except OSError:
                # Windows 10 missing privilege?
                # try to copy
                import shutil
                try:
                    shutil.copy2(self.output_path / 'sw' / 'inc' / 'classes' / cls.id / f'{cls.id}_stat.h', stat_h)
                except shutil.SameFileError:
                    pass
                try:
                    shutil.copy2(self.output_path / 'sw' / 'inc' / 'classes' / cls.id / "sub_defconst.h", sub_defconst)
                except shutil.SameFileError:
                    pass
                try:
                    shutil.copy2(self.output_path / 'sw' / 'inc' / 'classes' / cls.id / "sub_defsyms.h", sub_defsyms)
                except shutil.SameFileError:
                    pass

        # EPICS database Makefiles
        self.add_template("fgcudp_Makefile.j2",
                          out_path=fgcudp_db_path,
                          out_name="Makefile")
        self.add_template("fgccmd_Makefile.j2",
                          out_path=fgccmd_db_path,
                          out_name="Makefile")

        # EPICS common files
        self.add_template("class_selector.h.j2",
                          out_path=fgcudp_src_common_path / 'inc')
        self.add_template("pub_data.h.j2",
                          out_path=fgcudp_src_common_path / 'inc')
        self.add_template("class_params_Makefile.j2",
                          out_path=fgcudp_src_path,
                          out_name="Makefile")
