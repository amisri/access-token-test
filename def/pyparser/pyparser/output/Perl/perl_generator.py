from pathlib import Path
import re
from pathlib import Path
from typing import List, Optional

from operator import attrgetter

from fgc_parser_schemas import (SuperDict, Property, Class, SymlistConst, Zone, Symlist)

from pyparser.output.base import BaseJinjaSuperDictOutGen, jinja_fn
from pyparser.output.base import (get_symlist, get_const_value, get_class_flags, has_specific_get, has_specific_set,
                                  get_maxels)


class PerlGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate Perl stuff.
    """

    @staticmethod
    def get_perl_maxels(prop: Property, cls: Class, superdict: SuperDict) -> str:
        maxels = get_maxels(prop, cls, superdict)
        if maxels is None:
            return "undef"
        return maxels

    @staticmethod
    def get_display_zones(cls: Class):
        zones = sorted(cls.pubdata.children, key=attrgetter('name'))
        zones.sort(key=lambda zone: zone.display_ord if zone.display_ord is not None else 9999)
        return zones

    @staticmethod
    def get_unpack_string(cls: Class):
        next_expected_address = 0
        unpack_string = "x${offset}CCxCx${preclass_offset}"
        for zone in sorted(cls.pubdata.children, key=lambda z: z.data_position):
            if zone.data_position < 0:
                continue

            # Check for gap in addresses and pad with reserved bytes if necessary

            if zone.address != next_expected_address:
                num_pad_bytes = int((zone.address - next_expected_address) / 8)
                unpack_string += f"x{num_pad_bytes}"

            # Construct unpack string based upon original property type and zone size

            if zone.property.type.name.startswith("INT"):
                # property is an integer
                if zone.numbits == 8:
                    if zone.property.type.name.endswith("U"):
                        unpack_string += 'C'
                    else:  # 'S'
                        unpack_string += 'c'
                elif zone.numbits == 16:
                    unpack_string += 'n'
                elif zone.numbits == 32:
                    unpack_string += 'N'
            elif zone.property.type.name == "FLOAT":
                # property is a float
                # Unpack initially as 32-bit network byte-order integer
                unpack_string += 'N'
            else:
                raise Exception(f"Not expected type for publication field {zone.name} in class FGC_{cls.id}")

            next_expected_address = zone.address + zone.numbits

        return unpack_string

    @staticmethod
    def get_perl_operational(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_operational:
            return "1" if prop.specific_operational[cls.id] else "0"
        return "1" if prop.operational else "0"

    @staticmethod
    def get_perl_type(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_types:
            return prop.specific_types[cls.id].name
        return prop.type.name

    @staticmethod
    def get_perl_symlists_consts(prop: Property, cls: Class) -> List[SymlistConst]:
        if prop.symlist:
            return prop.symlist.consts
        elif prop.class_symlists:
            return prop.class_symlists[cls.id].consts
        return []

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # regex match test
        self.add_test("matching_regex", lambda val, pattern: re.match(pattern, val))

        fgc_perl_path = self.output_path / 'sw' / 'clients' / 'perl' / 'FGC'

        # Properties module
        self.add_template(name="Properties.pm.j2",
                          out_path=fgc_perl_path,
                          get_const_value=get_const_value,
                          get_class_flags=get_class_flags,
                          has_specific_get=has_specific_get,
                          has_specific_set=has_specific_set,
                          get_perl_maxels=PerlGenerator.get_perl_maxels,
                          get_perl_operational=PerlGenerator.get_perl_operational,
                          get_perl_type=PerlGenerator.get_perl_type,
                          get_perl_symlists_consts=PerlGenerator.get_perl_symlists_consts)

        # Parameters module
        self.add_template(name="RegFGC3_parameters.pm.j2",
                          out_path=fgc_perl_path)

        # Errors module
        self.add_template(name="Errors.pm.j2",
                          out_path=fgc_perl_path)

        # Components module
        self.add_template(name="Components.pm.j2",
                          out_path=fgc_perl_path)

        # Runlog entries module
        self.add_template(name="Runlog_entries.pm.j2",
                          out_path=fgc_perl_path)

        # Classes

        for cls in self.superdict.classes.values():
            if cls.platform.name == "fgc4":
                continue
            class_dir = fgc_perl_path / 'Class' / cls.id

            # Auto module
            self.add_template(name="class_auto.pm.j2",
                              out_path=class_dir,
                              out_name="Auto.pm",
                              cls=cls,
                              get_display_zones=PerlGenerator.get_display_zones,
                              get_unpack_string=PerlGenerator.get_unpack_string,
                              get_symlist=get_symlist,
                              get_const_value=get_const_value
                              )

            # API Snapshot
            self.add_template(name="class_api.pm.j2",
                              out_path=class_dir,
                              out_name="API.pm",
                              cls=cls,
                              get_const_value=get_const_value,
                              get_class_flags=get_class_flags,
                              has_specific_get=has_specific_get,
                              has_specific_set=has_specific_set,
                              get_perl_maxels=PerlGenerator.get_perl_maxels,
                              get_perl_operational=PerlGenerator.get_perl_operational,
                              get_perl_type=PerlGenerator.get_perl_type,
                              get_perl_symlists_consts=PerlGenerator.get_perl_symlists_consts
                              )

        for platform in self.superdict.platforms.values():
            if platform.name == "fgc4":
                continue
            platform_name_cap_first = platform.name[0].upper() + platform.name[1:]
            platform_dir = fgc_perl_path / 'Platform' / platform_name_cap_first

            # Auto module
            self.add_template(name="platform_auto.pm.j2",
                              out_path=platform_dir,
                              out_name="Auto.pm",
                              platform=platform,
                              platform_name_cap_first=platform_name_cap_first)
