from pathlib import Path
from typing import Dict

from fgc_parser_schemas import (SuperDict, Class, Symlist)

from pyparser.output.base import BaseJinjaSuperDictOutGen


class CclibsGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate cclibs stuff.
    """

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # generate evtlogSymlists
        for cls in self.superdict.classes.values():
            self._gen_evtlog_symlists(cls)

    def _gen_evtlog_symlists(self, cls: Class):
        output_path = self.output_path / 'def' / 'src' / 'classes' / cls.id

        # prepare symlists
        symlists: Dict[str, Symlist] = {}
        for prop in cls.properties.values():
            if prop.symlist:
                symlists[prop.symlist.name] = prop.symlist
            if prop.class_symlist:
                symlists[prop.class_symlist] = prop.class_symlists[cls.id]
        symlists = dict(sorted(symlists.items(), key=lambda kv: kv[0]))

        self.add_template("evtlog_symlists.csv.j2",
                          out_path=output_path,
                          cls=cls,
                          symlists=symlists)


