from pathlib import Path
from typing import Dict

from pydantic.fields import ModelField
from pydantic.typing import display_as_type
from ruamel.yaml import YAML, CommentedSeq, CommentedMap

from fgc_parser_schemas import (SuperDict, Property)

from pyparser.output.base import BaseJinjaSuperDictOutGen


class SchemaGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate YAML schema for the GUI.
    """

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        output_path = self.output_path / 'def'

        # get Property model's fields
        self.fields = Property.__fields__.copy()

        yaml = YAML()

        data = self._get_data()

        with open(output_path / 'schema.yaml', 'w') as f:
            yaml.dump(data, f)

    def _get_data(self) -> CommentedMap:
        data = CommentedMap()

        data["parametrizable_attributes"] = self._get_parametrizable_attributes()
        data["comment"] = self._get_comment_structure()
        data["include"] = self._get_include_structure()
        data["property"] = self._get_property_structure(attribs=data["parametrizable_attributes"],
                                                        comment=data["comment"],
                                                        include=data["include"])

        # root
        data["name"] = "Schema for properties"
        data["type"] = "map"
        data["mapping"] = CommentedMap()
        data["mapping"]["properties"] = CommentedMap()
        data["mapping"]["properties"]["type"] = "map"
        mapping = CommentedMap()
        data["mapping"]["properties"]["mapping"] = mapping
        mapping["name"] = {"type": "str", "required": True}
        mapping["children"] = CommentedMap()
        mapping["children"]["type"] = "seq"
        children_sequence = CommentedSeq()
        mapping["children"]["sequence"] = children_sequence

        sequence_item = CommentedMap()
        sequence_item["type"] = "map"
        sequence_item["mapping"] = CommentedMap()
        sequence_item["mapping"]["property"] = data["property"]
        sequence_item["mapping"]["include"] = data["include"]
        sequence_item["mapping"]["comment"] = data["comment"]

        children_sequence.append(sequence_item)

        return data

    def _get_parametrizable_attributes(self) -> CommentedMap:
        data = CommentedMap()
        data.yaml_set_anchor("attribs", True)
        data["type"] = "map"
        mapping = CommentedMap()
        data["mapping"] = mapping
        # get fields starting with 'specific' and remove this prefix
        specific_fields: Dict[str, ModelField] = {field_name.replace('specific_', ''): field for field_name, field in self.fields.items()
                           if field_name.startswith('specific_')}

        for field_name, field in specific_fields.items():
            # fix name
            if field_name == "types":
                field_name = "type"
            mapping[field_name] = self._treat_field(field_name, field)
        return data

    def _treat_field(self, field_name: str, field: ModelField) -> CommentedMap:
        if self._is_special_field(field_name, field):
            return self._treat_special_field(field_name, field)

        field_yaml_mapping = CommentedMap()

        field_type = self._get_specific_field_type(field)

        if self._is_not_standard_type(field_type):
            field_yaml_mapping["type"] = "str"
            field_yaml_mapping["enum"] = []

        else:
            field_yaml_mapping["type"] = field_type

        if field.required:
            field_yaml_mapping["required"] = True

        return field_yaml_mapping

    def _treat_special_field(self, field_name: str, field: ModelField) -> CommentedMap:
        field_yaml_mapping = CommentedMap()
        if field_name == "type":
            field_yaml_mapping["type"] = "str"
            data = CommentedSeq(sorted(self.superdict.types.keys()))
            data.fa.set_flow_style()
            field_yaml_mapping["enum"] = data

        elif field_name == "flags":
            field_yaml_mapping["type"] = "str"

        elif field_name == "symlist":
            field_yaml_mapping["type"] = "str"

        elif field_name == "name":
            field_yaml_mapping["type"] = "str"
            field_yaml_mapping["required"] = True
            field_yaml_mapping["py_valid"] = "unique_name"
        elif field_name == "doc":
            field_yaml_mapping["type"] = "long_str"
            field_yaml_mapping["visible"] = False

        elif field_name == "get":
            field_yaml_mapping["type"] = "str"
            data = CommentedSeq(sorted(self.superdict.gets.keys()))
            data.fa.set_flow_style()
            field_yaml_mapping["enum"] = data
        elif field_name == "set":
            field_yaml_mapping["type"] = "str"
            data = CommentedSeq(sorted(self.superdict.sets.keys()))
            data.fa.set_flow_style()
            field_yaml_mapping["enum"] = data
        elif field_name == "setif":
            field_yaml_mapping["type"] = "str"
            data = CommentedSeq(sorted(self.superdict.setifs.keys()))
            data.fa.set_flow_style()
            field_yaml_mapping["enum"] = data
        elif field_name == "group":
            field_yaml_mapping["type"] = "str"
            data = CommentedSeq(sorted(self.superdict.property_groups.keys()))
            data.fa.set_flow_style()
            field_yaml_mapping["enum"] = data
            field_yaml_mapping["default"] = "DEFAULT"

        return field_yaml_mapping

    def _is_special_field(self, field_name: str, field: ModelField) -> bool:
        return field_name in ["type", "flags", "name", "doc", "get", "set", "setif", "symlist", "group"]

    def _get_specific_field_type(self, field: ModelField) -> str:
        return display_as_type(field.type_)

    def _is_not_standard_type(self, type: str) -> bool:
        return type not in ['str', 'bool', 'int', 'float']

    def _get_comment_structure(self) -> CommentedMap:
        data = CommentedMap()
        data.yaml_set_anchor("comment", True)
        data["type"] = "map"
        comment_mapping = CommentedMap()
        data["mapping"] = comment_mapping
        comment = CommentedMap()
        comment_mapping["comment"] = comment
        comment["type"] = "long_str"
        comment["required"] = True
        return data

    def _get_include_structure(self) -> CommentedMap:
        data = CommentedMap()
        data.yaml_set_anchor("include", True)
        data["type"] = "map"
        data["mapping"] = {
            "name": {"type": "str", "required": True},
            "file": {"type": "file", "required": True},
            "comment": {"type": "long_str"},
            "computed": {
                "type": "map",
                "removable": False,
                "addable": False,
                "mapping": {
                    "file_path": {
                        "type": "file",
                        "required": False,
                        "removable": False,
                        "addable": False,
                        "editable": False,
                        "tooltip": "Full path to include's file"
                    }
                }
            }
        }
        return data

    def _get_property_structure(self, attribs: CommentedMap, include: CommentedMap, comment: CommentedMap) -> CommentedMap:
        data = CommentedMap()
        data.yaml_set_anchor("property", True)
        data["type"] = "map"

        mapping = CommentedMap()
        data["mapping"] = mapping
        # get fields NOT starting with 'specific' and remove this prefix
        generic_fields: Dict[str, ModelField] = {field_name: field for field_name, field in self.fields.items()
                           if not field_name.startswith('specific_')}
        ignored_fields = ["yaml_node", "class_flags", "classes", "class_symlists", "parent", "full_name", "children"]
        for field_name, field in generic_fields.items():
            if field_name in ignored_fields or field_name.endswith('_'):
                continue
            mapping[field_name] = self._treat_field(field_name, field)
            # special case for type - here is required
            if field_name == "type":
                mapping[field_name]["required"] = True

        # now let's generate class/platform specific stuff
        # it's grouped by platform, then per class
        for platform_name, platform in self.superdict.platforms.items():
            mapping[platform_name] = CommentedMap()
            mapping[platform_name]["type"] = "map"

            class_mapping = CommentedMap()
            mapping[platform_name]["mapping"] = class_mapping

            # add generic platform mapping
            class_mapping["platform"] = attribs
            # classes
            for cls in sorted(platform.classes.keys(), key=lambda c: int(c)):
                cls = int(cls)
                class_mapping[cls] = attribs

        # write children
        children = CommentedMap()
        mapping["children"] = children
        children["type"] = "seq"
        children_seq = CommentedSeq()
        children["sequence"] = children_seq

        sequence_item = CommentedMap()
        sequence_item["type"] = "map"
        sequence_item["mapping"] = CommentedMap()
        sequence_item["mapping"]["property"] = data
        sequence_item["mapping"]["include"] = include
        sequence_item["mapping"]["comment"] = comment

        children_seq.append(sequence_item)

        return data