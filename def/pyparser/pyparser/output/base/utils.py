from typing import List, Optional
from fgc_parser_schemas import (SuperDict, Property, Class, SymlistConst, Zone, Symlist)


def get_symlist(zone: Zone, cls: Class) -> Optional[Symlist]:
    if zone.property.symlist:
        return zone.property.symlist
    elif zone.property.class_symlist:
        return zone.property.class_symlists[cls.id]
    return None


def get_const_value(sym_const: SymlistConst, cls: Class, superdict: SuperDict):
    value = None
    if sym_const.name in cls.consts:
        value = cls.consts[sym_const.name].val
    if value is None and sym_const.name in superdict.consts:
        value = superdict.consts[sym_const.name].val

    if value.startswith('0x'):
        value = str(int(value, 0))
    return value


def has_specific_get(prop: Property, cls: Class) -> bool:
    if cls.id in prop.specific_get:
        return True
    if prop.get is not None:
        return True
    return False


def has_specific_set(prop: Property, cls: Class) -> bool:
    if cls.id in prop.specific_set:
        return True
    if prop.set is not None:
        return True
    return False


def get_class_flags(prop: Property, cls: Class) -> List[str]:
    flags = {}
    if cls.id in prop.class_flags:
        flags = prop.class_flags[cls.id]
    elif prop.flags:
        flags = prop.flags

    flags = list(flags.keys())
    flags.sort()

    return flags


def get_maxels(prop: Property, cls: Class, superdict: SuperDict) -> Optional[str]:
    maxels = None
    if prop.type.name == "PARENT":
        maxels = None

    if cls.id in prop.specific_maxels:
        maxels = prop.specific_maxels[cls.id]
    elif prop.maxels is not None:
        maxels = prop.maxels
    else:
        maxels = None

    # translate maxels to number
    try:
        int(maxels, 0)
    except ValueError:
        # not a number...
        if maxels.startswith("FGC_"):
            # maxels is a constant
            const = maxels.replace('FGC_', '')

            if const in cls.consts:
                maxels = cls.consts[const].val
            elif const in superdict.consts:
                # global const
                maxels = superdict.consts[const].val
            else:
                # Maxels is of a type that cannot be interpreted (probably a constant defined in the code)
                maxels = None

        else:
            # Maxels is of a type that cannot be interpreted (probably a constant defined in the code)
            maxels = None

    else:
        # a number, do nothing
        pass

    return maxels
