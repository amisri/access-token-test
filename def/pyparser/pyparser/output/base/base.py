"""
Module with base component - base classes for other components
"""
import logging
import os

from pathlib import Path
from typing import Callable, Iterable, Tuple, Optional
from abc import ABC, abstractmethod
from collections.abc import MutableSet, MutableMapping, MutableSequence
import copy
from jinja2 import meta
from fgc_parser_schemas import SuperDict
from pyparser.output.base.config import JINJA_ENV
from pyparser import ROOT_DIR


class BaseOutGen(ABC):
    GENERATOR_FILENAME = __file__
    """
    This is a base class for every output generator with execute method.
    """

    def __init__(self, output_path: Path, **kwargs):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.output_path = output_path
        self._create_output_path(self.output_path)
        self.kwargs = kwargs

    @abstractmethod
    def execute(self, **kwargs):
        """
        Abstract execute method. This method is called to process the component
        Parameters
        ----------
        kwargs
            Arguments passed to, for instance, template parser
        """
        return NotImplemented

    @staticmethod
    def _create_output_path(output_path: Path):
        """
        This method creates output directory structure if missing.
        Parameters
        ----------
        output_path: str
            output path
        """
        if not output_path.exists():
            os.makedirs(output_path, exist_ok=True)


class BaseJinjaOutGen(BaseOutGen):
    """
    Base class for a component that uses Jinja
    """
    def __init__(self, output_path: Path, **kwargs):
        super().__init__(output_path, **kwargs)
        self.templates = []
        self.jinja_env = kwargs.get('jinja_env', JINJA_ENV)

    def clear(self):
        self.templates.clear()

    def add_template(self, name: str, out_name: Optional[str] = None, out_path: Optional[Path] = None, **kwargs):
        """
        Add template into pool for processing. Kwargs are accessible in the template itself

        Parameters
        ----------
        name: str
            template filename (lookup as in utils.jinja.config.py)
        out_name: Optional[str]
            output filename
        out_path: Optional[Path]
            output path (if different than default)
        kwargs:
            All kwargs are accessible in the template body.
        """

        if out_name is None:
            out_name = Path(name).stem
        if out_path is None:
            out_path = self.output_path

        template_source, template_path, uptodate = self.jinja_env.loader.get_source(self.jinja_env, name)
        # try:
        #     ast = self.jinja_env.parse(template_source)
        # except Exception as e:
        #     raise Exception(f"Template parsing failed: {template_path}: {e}")

        for k, v in kwargs.items():
            if isinstance(v, (MutableSequence, MutableMapping, MutableSet)):
                kwargs[k] = copy.copy(v)

        # add meta vars to kwargs
        kwargs["__output_path__"] = str(out_path)
        kwargs["__output_filename__"] = str(out_name)
        kwargs["__output_relative_path__"] = out_path.relative_to(Path(os.path.commonprefix((ROOT_DIR, out_path))))
        kwargs["__generator_name__"] = self.__class__.__name__
        kwargs["__generator_filename__"] = Path(self.GENERATOR_FILENAME).name
        kwargs["__template_name__"] = name
        kwargs["__template_filename__"] = Path(template_path).relative_to(ROOT_DIR)
        # try:
        #     undefined_vars = meta.find_undeclared_variables(ast) - set(kwargs.keys())
        # except Exception as e:
        #     raise Exception(f"Template parsing failed: {template_path}: {e}")
        # if undefined_vars:
        #     self.logger.warning(f"{name}: undefined variables: {undefined_vars}")

        template = {'name': name, 'out_name': out_name, 'out_path': out_path, 'kwargs': kwargs}
        self.templates.append(template)
        self.logger.debug(f"Added template: {name} -> {out_path / out_name}")

    def execute(self, **kwargs):
        """
        Iterate over templates added by add_template method
        All kwargs are injected into the template

        Parameters
        ----------
        kwargs
            variables to inject into template, in addition to template-specific.
        """
        self.logger.info("Writing...")
        for template in self.templates:
            self.write(template['name'], template['out_name'], template['out_path'], **template['kwargs'], **kwargs)
        self.logger.info("Done!")

    def add_filters(self, filters: Iterable[Tuple[str, Callable]]):
        """
        Add multiple filters
        Parameters
        ----------
        filters: iterable with tuples (name, f_filter)
        """
        for item in filters:
            self.add_filter(*item)

    def add_filter(self, name: str, f_filter: Callable):
        """
            Add filter to use in the templates. Maybe instance specific -
             (as you can put different envs)
        Parameters
        ----------
        name: str
            name of filter to use in a template. use with "|" syntax:
            {% set name = node | node_name %}
        f_filter: callable
            function
        """
        self.jinja_env.filters[name] = f_filter

    def add_tests(self, tests: Iterable[Tuple[str, Callable]]):
        """
        Add multiple tests
        Parameters
        ----------
        tests: iterable with tuples (name, f_filter)
        """
        for item in tests:
            self.add_test(*item)

    def add_test(self, name: str, f_test: Callable):
        """
            Add test function to use in the templates. Maybe instance specific -
             (as you can put different envs)
        Parameters
        ----------
        name: str
            name of test to use in a template. use with "is" syntax:
            {% if node is register %}
        f_test: callable
            function
        """
        self.jinja_env.tests[name] = f_test

    def add_function(self, fn: Callable):
        self.jinja_env.globals[fn.__name__] = fn

    def write(self, template_name: str, output_name: str, output_path: Path, **kwargs):
        """
            Render the template, using kwargs
        Parameters
        ----------
        template_name: str
            name of the template
        output_name: str`
            output name of the template
        output_path: path
            where to write templates
        kwargs:
            variables passed to rendering process
        """
        self._create_output_path(output_path)
        template = self.jinja_env.get_template(template_name)
        with open(output_path / output_name, 'w+') as f:
            self.logger.debug(
                f"Writing to {output_name} - template rendered: {template_name}")
            try:
                f.write(template.render(**kwargs))
            except Exception as e:
                raise Exception(f"Error occured during parsing template {template_name}: {e}")


class BaseJinjaSuperDictOutGen(BaseJinjaOutGen):
    """
    Base class for a component that uses Jinja and a SuperDict
    """
    def __init__(self, project_path: Path, superdict: SuperDict, **kwargs):
        self.superdict = superdict
        super().__init__(project_path, **kwargs)
        # add superdict to globals
        self.jinja_env.globals['superdict'] = self.superdict

        self.logger.info(f"Initialization")
