"""
Configuration file for all jinja output generators
"""
import re
import jinja2.environment
from jinja2.filters import do_sort, do_dictsort, pass_environment
from pyparser import ROOT_DIR

JINJA_LOADER = jinja2.ChoiceLoader([
    # generic stuff, macros etc.
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'base' / 'templates'),
    # templates for the generation of C/C++ stuff
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'C' / 'templates'),
    # templates for Perl generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'Perl' / 'templates'),
    # templates for cclibs generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'cclibs' / 'templates'),
    # templates for config generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'Config' / 'templates'),
    # templates for EPICS generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'EPICS' / 'templates'),
    # templates for Tango generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'Tango' / 'templates'),
    # templates for Python generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'Python' / 'templates'),
    # templates for VHDL generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'VHDL' / 'templates'),
    # templates for Docs generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'Doc' / 'templates'),
    # templates for FGC4 generation
    jinja2.FileSystemLoader(ROOT_DIR / 'output' / 'FGC4' / 'templates'),
])
JINJA_ENV = jinja2.Environment(
    extensions=[
        # allows to use {% do item.function(arg) %} construction
        'jinja2.ext.do',
        # continue/break in loops
        'jinja2.ext.loopcontrols'
    ],
    loader=JINJA_LOADER,
    lstrip_blocks=True,
    trim_blocks=True
)


def jinja_test(name):
    """
        Decorator that registers function as a jinja test
    Parameters
    ----------
    name: str
        name of the test (to use in a template)

    Returns
    -------

    """
    def test_decorator(func):
        """
        Inner decorate function
        Parameters
        ----------
        func: function
            function to decorate
        Returns
        -------
        function
        """
        JINJA_ENV.tests[name] = func
        return func
    return test_decorator


def jinja_filter(name):
    """
        Decorator that registers function as a jinja filter
    Parameters
    ----------
    name: str
        name of the filter (to use in a template)

    Returns
    -------

    """
    def test_decorator(func):
        """
        Inner decorate function
        Parameters
        ----------
        func: function
            function to decorate
        Returns
        -------
        function
        """
        JINJA_ENV.filters[name] = func
        return func
    return test_decorator


def jinja_fn(func):
    if func.__name__ in JINJA_ENV.globals:
        raise Exception(f'Jinja fn "{func.__name__}" is already defined!')
    JINJA_ENV.globals[func.__name__] = func
    return func


@pass_environment
def do_sort2(environment, value, reverse=False, case_sensitive=True, attribute=None):
    # our do_sort implementation with case_sensitive defaulted to True
    return do_sort(environment, value, reverse, case_sensitive, attribute)


def do_dictsort2(value, case_sensitive=True, by="key", reverse=False):
    # our do_sort implementation with case_sensitive defaulted to True
    return do_dictsort(value, case_sensitive, by, reverse)


# global filters
JINJA_ENV.filters["quote"] = lambda s: '"' + str(s) + '"'
JINJA_ENV.filters["single_quote"] = lambda s: "'" + str(s) + "'"
JINJA_ENV.filters["sort"] = do_sort2
JINJA_ENV.filters["dictsort"] = do_dictsort2

# global tests
JINJA_ENV.tests["matching_regex"] = lambda val, pattern: re.match(pattern, val)

# useful in many templates
JINJA_ENV.globals["getattr"] = getattr
JINJA_ENV.globals["setattr"] = setattr
JINJA_ENV.globals["hasattr"] = hasattr
