from struct import pack, calcsize
from typing import Dict, List
from fgc_parser_schemas import (System, Const, Component)
from ..utils import safe_pack_string


def pack_headers(consts: Dict[str, Const], len_sorted_components: int) -> bytes:
    packed_headers = b''

    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    # python: !H
    packed_headers += pack('!H', int(consts['COMPDB_VERSION'].val))

    # number of all defined components
    packed_headers += pack('!H', len_sorted_components)

    return packed_headers


def pack_group(consts: Dict[str, Const], component: Component, sorted_systems: List[System]) -> bytes:
    packed_group = b''

    if component.sys_groups:
        for i, system in enumerate(sorted_systems):
            if system.type in component.sys_groups:
                # perl: C An unsigned char (octet) value.
                packed_group += pack('B', i)
                packed_group += pack('B', component.sys_groups[system.type])
    return packed_group


def pack_component(consts: Dict[str, Const], component: Component) -> bytes:
    packed_component = b''

    # pack type also known as barcode
    packed_component += safe_pack_string(int(consts['COMPDB_COMP_LEN'].val) + 1, component.barcode)

    # pack label
    packed_component += safe_pack_string(int(consts['COMPDB_COMP_LBL_LEN'].val) + 1, component.label)

    # pack prop_temp
    packed_component += safe_pack_string(int(consts['COMPDB_PROP_LEN'].val),
                                         component.temp_prop if component.temp_prop else '')

    # pack barcode prop
    packed_component += safe_pack_string(int(consts['COMPDB_PROP_LEN'].val),
                                         component.barcode_prop if component.barcode_prop else '')

    return packed_component


def gen_compdb(consts: Dict[str, Const],
               sorted_systems: List[System],
               sorted_components: List[Component]) -> bytes:
    fgc_compdb = b''
    fgc_compdb_comps = b''
    fgc_compdb_groups = b''

    fgc_groups_offset = 0

    fgc_compdb += pack_headers(consts, len(sorted_components))
    for component in sorted_components:
        fgc_group = pack_group(consts, component, sorted_systems)
        fgc_groups_size = len(fgc_group) // 2
        fgc_compdb_comps += pack_component(consts, component)
        # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
        fgc_compdb_comps += pack("!H", fgc_groups_size)
        fgc_compdb_comps += pack("!H", fgc_groups_offset)

        # Generate group array
        fgc_compdb_groups += fgc_group
        fgc_groups_offset += fgc_groups_size

    # perl: l  A signed long (32-bit) value.
    word_size = 4  #  calcsize('l')  # word size in bytes
    packed_comps_size = len(fgc_compdb_comps)

    if packed_comps_size % word_size != 0:
        padding = word_size - (packed_comps_size % word_size)
        fgc_compdb_comps += pack(f"{padding}x")

    fgc_compdb += fgc_compdb_comps
    fgc_compdb += fgc_compdb_groups

    return fgc_compdb
