from struct import pack, unpack
from typing import Dict, List
from logging import Logger
from fgc_parser_schemas import (Const, DimName, DimType, AnaChannel, DigBank)
from ..utils import safe_pack_string


def pack_version(consts: Dict[str, Const]) -> bytes:
    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    # python: >H
    return pack(">H", int(consts["DIMDB_VERSION"].val))


def pack_heap(consts: Dict[str, Const], sorted_symbols: List[str]) -> bytes:
    packed_heap = b''

    # Generate heap
    # perl: C An unsigned char (octet) value.
    packed_heap += pack("B", 0)

    heap_size = 1
    for symbol in sorted_symbols:
        # perl: "a".(length($symbol) + 1) ;;; a  A string with arbitrary binary data, will be null padded.
        packed_heap += pack(f"{len(symbol)+1}s", symbol.encode('ascii'))
        heap_size += len(symbol) + 1

    packed_heap_size = len(packed_heap)  # in bytes

    # Heap size will be stored in words, so make sure it is aligned by padding the remainder with null bytes
    # perl: l  A signed long (32-bit) value.
    word_size = 4  # len(pack("l", 0))  # should be 4...

    if packed_heap_size % word_size != 0:
        padding = word_size - (packed_heap_size % word_size)
        packed_heap += pack(f"{padding}x")
        packed_heap_size = len(packed_heap)

    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    heap = pack("!H", packed_heap_size // word_size)

    heap += packed_heap

    return heap


def pack_headers(dim_names: Dict[str, DimName], platform_dim_types: List[DimType]) -> bytes:
    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    packed_headers = pack('!H', len(dim_names))
    packed_headers += pack('!H', len(platform_dim_types))

    return packed_headers


def pack_names(consts: Dict[str, Const], dim_names: Dict[str, DimName]) -> bytes:
    packed_names = b''

    # maximum length from consts
    max_length = int(consts['DIMDB_MAX_DIM_NAME_LEN'].val) + 1

    # generate names array
    for dim_name in sorted(dim_names.values(), key=lambda dim: dim.index):
        packed_names += safe_pack_string(max_length, dim_name.name)
    return packed_names


def pack_ana(consts: Dict[str, Const], ana_channels: Dict[int, AnaChannel], symbols: Dict[str, int]) -> bytes:
    packed_ana = b''

    # gain + offset + label + units
    ana_size = 4 + 4 + 2 + 2
    max_ana_channels = int(consts['N_DIM_ANA_CHANS'].val)
    for i in range(0, max_ana_channels):
        if i not in ana_channels:
            # pad if channel is not defined
            # perl: "x".($ana_size);; x  A null byte (a.k.a ASCII NUL, "\000", chr(0))
            packed_ana += pack(f"{ana_size}x")
            continue
        ana_channel = ana_channels[i]
        # perl:
        # pack: f  A single-precision float in native format.
        # unpack: I  An unsigned integer value.
        gain = unpack('I', pack('f', ana_channel.gain))
        offset = unpack('I', pack('f', ana_channel.offset))

        # perl: gain,offset: N  An unsigned long (32-bit) in "network" (big-endian) order.
        # label, units: n  An unsigned short (16-bit) in "network" (big-endian) order.
        packed_ana += pack("!L", gain[0])
        packed_ana += pack("!L", offset[0])
        packed_ana += pack("!H", symbols[ana_channel.label])
        packed_ana += pack("!H", symbols[ana_channel.units])

    # padding already applied above
    # perl: "x".($ana_size * ($consts->{N_DIM_ANA_CHANS} - @$ana_channels))
    # padding = ana_size * (int(consts['N_DIM_ANA_CHANS'].val) - len(ana_channels))

    # packed_ana += pack(f"{padding}x")
    return packed_ana


def pack_dig(consts: Dict[str, Const], dig_banks: Dict[int, DigBank], symbols: Dict[str, int]) -> bytes:
    packed_dig = b''

    # chan_lbl + zero lbl + one lbl
    dig_size = 2 + 2 + 2

    # channels number per bank
    max_channels = int(consts['N_DIM_DIG_INPUTS'].val)

    for bank_no in (4, 5):

        if bank_no not in dig_banks:
            # apply padding and that's it
            # perl: "x".($dig_size * ($consts->{N_DIM_DIG_INPUTS} - @{$dig_bank->{inputs}})));
            padding = dig_size * max_channels
            packed_dig += pack(f"{padding}x")
            # fault mask (0)
            packed_dig += pack("!H", 0)
            continue

        dig_bank = dig_banks[bank_no]
        faults_mask = 0

        for i in range(0, max_channels):
            # pad if input is not defined
            if i not in dig_bank.inputs:
                packed_dig += pack(f"{dig_size}x")
                continue
            inp = dig_bank.inputs[i]

            # perl:  n  An unsigned short (16-bit) in "network" (big-endian) order.
            packed_dig += pack("!H", symbols[inp.label])
            packed_dig += pack("!H", symbols[inp.one])
            packed_dig += pack("!H", symbols[inp.zero])

            faults_mask |= (int(inp.fault) << inp.index)

        # padding not necessary here
        # fault mask
        packed_dig += pack("!H", faults_mask)

        # perl: "x".($dig_size * ($consts->{N_DIM_DIG_INPUTS} - @{$dig_bank->{inputs}})));
        # padding = dig_size * (int(consts['N_DIM_DIG_INPUTS'].val) - len(dig_bank.inputs))
        # packed_dig += apply_padding(padding, faults_mask)

    return packed_dig


def gen_dimdb(logger: Logger, consts: Dict[str, Const], platform_dim_types: List[DimType], dim_names: Dict[str, DimName]):
    fgc_dimdb = b''

    sorted_dim_types = platform_dim_types

    def standardize_label(label: str) -> str:
        # make uppercase
        label = label.upper()
        # trim
        label = label.strip()
        # more to come here...
        # ...

        return label

    # construct dict of all symbols
    symbols = dict()

    for dim_type in sorted_dim_types:
        for ana_channel in dim_type.ana_channels.values():
            ana_channel.label = standardize_label(ana_channel.label)
            if ana_channel.label not in symbols:
                symbols[ana_channel.label] = 0
            ana_channel.units = standardize_label(ana_channel.units)
            if ana_channel.units not in symbols:
                symbols[ana_channel.units] = 0
        for dig_bank in dim_type.dig_banks.values():
            for input in dig_bank.inputs.values():
                input.label = standardize_label(input.label)
                if input.label not in symbols:
                    symbols[input.label] = 0
                input.one = standardize_label(input.one)
                if input.one not in symbols:
                    symbols[input.one] = 0
                input.zero = standardize_label(input.zero)
                if input.zero not in symbols:
                    symbols[input.zero] = 0
    # set symbol indexes

    index = 1
    sorted_symbols = sorted(symbols.keys())

    for symbol in sorted_symbols:
        symbols[symbol] = index
        index += len(symbol) + 1

    # check symbols if they fit in uint16
    max_index = 65535
    if index > max_index:
        raise Exception(f"Unable to generate the codes - "
                        f"the total length of DIM labels can't be represented in a 16 bit unsigned integer. "
                        f"Please remove duplicate labels or unused DIM types. "
                        f"You can check current labels here: "
                        f"https://wikis.cern.ch/display/TEEPCCCS/DIM+definition+statistics "
                        f"Current length: {index} (max: {max_index}; diff: {abs(index - max_index)})")

    if index > 0.8 * max_index:
        logger.warning(f"The total length of DIM labels very soon won't be possible "
                       f"to be represented in a 16 bit unsigned integer. "
                       f"Please remove duplicate labels or unused DIM types. "
                       f"You can check current labels here: "
                       f"https://wikis.cern.ch/display/TEEPCCCS/DIM+definition+statistics "
                       f"Current length: {index} (max: {max_index}; diff: {abs(index - max_index)})")

    fgc_dimdb += pack_version(consts)

    # generate heap

    fgc_dimdb += pack_heap(consts, sorted_symbols)

    # Pack number of dim names and types

    fgc_dimdb += pack_headers(dim_names, sorted_dim_types)

    # Generate names array

    fgc_dimdb += pack_names(consts, dim_names)
    for dim_type in sorted_dim_types:
        fgc_dimdb += pack_ana(consts, dim_type.ana_channels, symbols)
        fgc_dimdb += pack_dig(consts, dim_type.dig_banks, symbols)

    return fgc_dimdb
