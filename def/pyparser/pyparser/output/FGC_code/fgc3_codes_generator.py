from pathlib import Path
from fgc_parser_schemas import (SuperDict)

from pyparser.output.base import BaseJinjaSuperDictOutGen

from .utils import get_platform_dim_types, get_platform_systems, get_platform_components

# sysdb, compdb and dimdb generation is the same as for FGCLite

from .FGC3.dimdb import gen_dimdb
from .FGC3.sysdb import gen_sysdb
from .FGC3.compdb import gen_compdb


class FGC3CodeGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate FGC 3 codes.
    """

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)
        codes_path = project_path / 'sw' / 'fgc' / 'codes' / 'C020_60_SysDB-C021_60_CompDB-C022_60_DIMDB'
        self._create_output_path(codes_path)
        compdb = codes_path / 'compdb.bin'
        dimdb = codes_path / 'dimdb.bin'
        sysdb = codes_path / 'sysdb.bin'

        # Extract some data from superdict to pass them to functions which generate databases

        consts = superdict.platforms['fgc3'].classes['60'].consts

        dim_names = superdict.dim_names

        platform_dim_types = get_platform_dim_types(self.superdict, 'fgc3')

        platform_systems = get_platform_systems(self.superdict, "fgc3")

        platform_components = get_platform_components(self.superdict, "fgc3")

        # sysdb, compdb and dimdb generation is the same as for FGCLite

        with open(dimdb, 'wb') as f:
            f.write(gen_dimdb(self.logger, consts, platform_dim_types, dim_names))

        with open(sysdb, 'wb') as f:
            f.write(gen_sysdb(consts, platform_systems, self.superdict.log_menu_names, self.superdict.log_menu_props))

        with open(compdb, 'wb') as f:
            f.write(gen_compdb(consts, platform_systems, platform_components))
