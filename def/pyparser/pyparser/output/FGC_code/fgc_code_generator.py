from pathlib import Path
from fgc_parser_schemas import (SuperDict)
from pyparser.output.base import BaseJinjaSuperDictOutGen
from .fgclite_codes_generator import FGCLiteCodeGenerator
from .fgc2_codes_generator import FGC2CodeGenerator
from .fgc3_codes_generator import FGC3CodeGenerator


class FGCCodeGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate FGC codes.
    """

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        self.sub_generators = [
            FGC2CodeGenerator(project_path, superdict),
            FGCLiteCodeGenerator(project_path, superdict),
            FGC3CodeGenerator(project_path, superdict)
        ]

    def execute(self, **kwargs):
        for sub_generator in self.sub_generators:
            sub_generator.execute(**kwargs)
