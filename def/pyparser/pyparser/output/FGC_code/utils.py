from struct import pack
from typing import List
from fgc_parser_schemas import SuperDict, System, DimType, Component


def get_platform_dim_types(superdict: SuperDict, platform: str) -> List[DimType]:
    platform_dim_types = sorted(
        filter(lambda dim_type: dim_type.platform is None or dim_type.platform == platform,
               superdict.dim_types.values()),
        key=lambda dim_type: dim_type.index)
    for i, dim in enumerate(platform_dim_types):
        dim.platform_dim_type_idx = i
    return platform_dim_types


def safe_pack_string(max_length: int, string: str) -> bytes:
    if len(string) > max_length:
        raise Exception(f"String to pack '{string}' exceed possible length limit: {len(string)}/{max_length}")
    # perl: 'a'.$max_length
    return pack(f"{max_length}s", string.encode('ascii'))


def get_platform_systems(superdict: SuperDict, platform: str) -> List[System]:
    # Construct an array of systems for this platform
    # Permit a system with multiple platforms (RPMBD)
    sorted_systems = sorted(
        filter(lambda system: system.platform is None or platform in system.platform,
               superdict.systems.values()),
        key=lambda system: system.type
    )

    # create "empty" unknown system and push front
    unknown_system = System(comp="",
                            type="UKNWN",
                            components={},
                            groups={},
                            label="Unknown system",
                            log_menus=[],
                            dims={})
    sorted_systems.insert(0, unknown_system)

    return sorted_systems


def get_platform_components(superdict: SuperDict, platform: str) -> List[Component]:
    platform_sorted_components = sorted(
        filter(lambda component: component.dallas_id and
                                 (component.platforms is None or platform in component.platforms),
               superdict.components.values()),
        key=lambda component: component.barcode
    )

    # create "unknown" component and push front
    unknown_component = Component(barcode="UNKNOWN___",
                                  label="Unknown component"
                                  )
    platform_sorted_components.insert(0, unknown_component)

    return platform_sorted_components
