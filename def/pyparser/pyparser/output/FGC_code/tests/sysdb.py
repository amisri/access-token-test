from pathlib import Path
from utils import unpack_value

# consts
SYSDB_MENU_NAME_LEN = 10
MAX_LOG_MENU_NAMES = 50
SYSDB_MENU_PROP_LEN = 20
SYSDB_SYS_LEN = 5
SYSDB_SYS_LBL_LEN = 80
N_COMP_GROUPS = 30
MAX_LOG_MENUS = 20
MAX_DIMS = 20


def read_sysdb(sysdb_file: Path):
    offset = 0

    # get its contents
    with open(sysdb_file, 'rb') as f:
        contents = f.read()

    # unpack headers

    sysdb_version, offset = unpack_value(">H", contents, offset)
    len_systems, offset = unpack_value(">H", contents, offset)

    print(f"Sysdb version: {sysdb_version}, len_systems: {len_systems}")

    # menu names
    print("Menu names:")

    for i in range(MAX_LOG_MENU_NAMES):
        val, offset = unpack_value(f"{SYSDB_MENU_NAME_LEN}s", contents, offset)
        print(val, offset)

    # menu props
    print("Menu props:")

    for i in range(MAX_LOG_MENU_NAMES):
        val, offset = unpack_value(f"{SYSDB_MENU_PROP_LEN}s", contents, offset)
        print(val, offset)

    # systems
    print("Systems:")

    for i in range(len_systems):
        print(f"system {i}")

        system_type, offset = unpack_value(f"{SYSDB_SYS_LEN + 1}s", contents, offset)
        system_label, offset = unpack_value(f"{SYSDB_SYS_LBL_LEN + 1}s", contents, offset)

        print(f"System type: {system_type}, system label: {system_label}")

        # padded after label

        offset += 1

        # system required

        required_0, offset = unpack_value(f"B", contents, offset)
        print(f"Required 0: {required_0}")

        for _ in range(N_COMP_GROUPS - 1):
            required, offset = unpack_value(f"B", contents, offset)
            print(f"required: {required}")

        # Menu idxs
        len_log_menus, offset = unpack_value(f"!H", contents, offset)

        print(f"Len log menus {len_log_menus}")

        for _ in range(MAX_LOG_MENUS):
            log_menu_name_idx, offset = unpack_value(f"B", contents, offset)
            print(f"Log menu name idx: {log_menu_name_idx}")

        for _ in range(MAX_LOG_MENUS):
            log_menu_prop_idx, offset = unpack_value(f"B", contents, offset)
            print(f"Log menu prop idx: {log_menu_prop_idx}")

        # system dims:
        print("Dims:")
        for _ in range(MAX_DIMS):
            dim_idx, offset = unpack_value(f"B", contents, offset)
            dim_bus_addr, offset = unpack_value(f"B", contents, offset)
            dim_name_idx, offset = unpack_value(f"!H", contents, offset)
            dim_type_idx, offset = unpack_value(f"!H", contents, offset)
            print(f"idx: {dim_idx}, bus_addr: {dim_bus_addr}, name_idx: {dim_name_idx}, type_idx: {dim_type_idx}")


if __name__ == "__main__":
    # get filename
    import argparse
    parser = argparse.ArgumentParser(description='Test the SysDB')
    parser.add_argument('sysdb', help='SysDB file', type=Path)
    args = parser.parse_args()

    read_sysdb(args.sysdb)
