from pathlib import Path
from struct import calcsize

from utils import unpack_value, read_string


# consts
DIMDB_MAX_DIM_NAME_LEN = 13 + 1
N_DIM_ANA_CHANS = 4
N_DIM_DIG_INPUTS = 12
N_DIM_DIG_BANKS = 2


def read_dimdb(dimdb_file: Path):
    # begin parsing file

    offset = 0

    word_size = 4  # calcsize('l')

    # get its contents
    with open(dimdb_file, 'rb') as f:
        contents = f.read()

    version, offset = unpack_value("!H", contents, offset)
    print(f"DIMDB_VERSION: {version}")
    # unpack heap
    heap_size_words, offset = unpack_value("!H", contents, offset)
    heap_size = heap_size_words * word_size
    print(f"Heap size: {heap_size_words} words, {heap_size} bytes")
    zero, offset = unpack_value("B", contents, offset)
    assert zero == 0
    # parse heap
    heap_start_offset = offset
    heap_end = heap_start_offset + heap_size
    print(f"Heap start: {heap_start_offset}, heap end: {heap_end}")
    while offset < heap_end:
        string, offset = read_string(contents, offset)
        print(string)
    print(f"Headers. Offset: {offset}")
    # headers:
    offset -= 1  # ? :(
    len_dim_names, offset = unpack_value("!H", contents, offset)
    print(f"Len dim names: {len_dim_names}, offset: {offset}")
    len_platform_dim_types, offset = unpack_value("!H", contents, offset)
    print(f"Len platform dim types: {len_platform_dim_types}, offset: {offset}")
    # names array

    for i in range(len_dim_names):
        val, offset = unpack_value(f"{DIMDB_MAX_DIM_NAME_LEN}s", contents, offset)
        print(val, offset)

    # dim types
    for i in range(len_platform_dim_types):
        # ana
        # ana size = 4 + 4 + 2 + 2
        for j in range(N_DIM_ANA_CHANS):
            ana_gain, offset = unpack_value("!L", contents, offset)
            ana_offset, offset = unpack_value("!L", contents, offset)
            ana_label, offset = unpack_value("!H", contents, offset)
            ana_units, offset = unpack_value("!H", contents, offset)
            print(f"ANA: {ana_gain}, {ana_offset}, {ana_label}, {ana_units}")
        # dig
        for bank in range(N_DIM_DIG_BANKS):
            for j in range(N_DIM_DIG_INPUTS):
                dig_label, offset = unpack_value("!H", contents, offset)
                dig_one, offset = unpack_value("!H", contents, offset)
                dig_zero, offset = unpack_value("!H", contents, offset)
                print(f"DIG: {dig_label}, {dig_one}, {dig_zero}")
            dig_fault_mask, offset = unpack_value("!H", contents, offset)
            print(f"FM: {dig_fault_mask}")
    assert len(contents) == offset


if __name__ == "__main__":
    # get filename
    import argparse
    parser = argparse.ArgumentParser(description='Test the DimDB')
    parser.add_argument('dimdb', help='DimDB file', type=Path)
    args = parser.parse_args()

    read_dimdb(args.dimdb)
