from typing import Tuple
from struct import unpack, unpack_from, calcsize


def unpack_value(fmt: str, buffer: bytes, offset: int):
    # get value and the new offset based on the format
    size = calcsize(fmt)
    return unpack_from(fmt, buffer, offset)[0], size + offset


def apply_padding(offset: int) -> int:
    word_size = 4  # bytes
    if offset % word_size != 0:
        offset += word_size - (offset % word_size)
    return offset


def unpack_helper(fmt: str, d: bytes):
    size = calcsize(fmt)
    return unpack(fmt, d[:size])[0], d[size:]


def read_string(buffer: bytes, offset: int) -> Tuple[str, int]:
    data = buffer[offset:]
    size = calcsize("s")
    res = b''
    s, data = unpack_helper("s", data)
    offset += size
    res += s
    while s != b'\x00':
        s, data = unpack_helper("s", data)
        offset += size
        res += s
    # skip last byte == \x00 while returning. decode to string
    return res[:-1].decode('utf8'), offset

