from struct import pack
from typing import Dict, List
from fgc_parser_schemas import (System, Const, SystemComponentGroup, Dim, LogMenu)
from ..utils import safe_pack_string


def pack_headers(consts: Dict[str, Const], len_systems: int) -> bytes:
    # Pack headers about db version and how many systems there are
    packed_headers = b''

    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    # python: >H
    packed_headers += pack(">H", int(consts["SYSDB_VERSION"].val))
    packed_headers += pack(">H", len_systems)  # n_sys

    return packed_headers


def pack_menu_names(consts: Dict[str, Const], log_menu_names: Dict[str, int]) -> bytes:
    packed_menu_names = b''

    for menu_name in sorted(log_menu_names.items(), key=lambda item: item[1]):
        packed_menu_names += safe_pack_string(int(consts['SYSDB_MENU_NAME_LEN'].val), menu_name[0])

    # perl: "x".($consts->{SYSDB_MENU_NAME_LEN} * ($consts->{MAX_LOG_MENU_NAMES} - keys(%{$log_menu_names}))))
    padding = int(consts['SYSDB_MENU_NAME_LEN'].val) * (int(consts['MAX_LOG_MENU_NAMES'].val) - len(log_menu_names))
    packed_menu_names += pack(f"{padding}x")

    return packed_menu_names


def pack_menu_props(consts: Dict[str, Const], log_menu_props: Dict[str, int]) -> bytes:
    packed_menu_props = b''
    for menu_prop in sorted(log_menu_props.items(), key=lambda item: item[1]):
        packed_menu_props += safe_pack_string(int(consts['SYSDB_MENU_PROP_LEN'].val), menu_prop[0])
    # padding
    # perl: "x".($consts->{SYSDB_MENU_PROP_LEN} * ($consts->{MAX_LOG_MENU_NAMES} - keys(%{$log_menu_props}))
    padding = int(consts['SYSDB_MENU_PROP_LEN'].val) * (int(consts['MAX_LOG_MENU_NAMES'].val) - len(log_menu_props))
    packed_menu_props += pack(f"{padding}x")
    return packed_menu_props


def pack_system_desc(consts: Dict[str, Const], system_type: str, system_label: str) -> bytes:
    packed_system_desc = b''

    packed_system_desc += safe_pack_string(int(consts['SYSDB_SYS_LEN'].val) + 1, system_type)
    packed_system_desc += safe_pack_string(int(consts['SYSDB_SYS_LBL_LEN'].val) + 1, system_label)

    # pad after label
    packed_system_desc += pack('x')

    return packed_system_desc


def pack_system_required(consts: Dict[str, Const], groups: Dict[str, SystemComponentGroup]) -> bytes:
    packed_system_required = b''

    sorted_groups = sorted(groups.values(), key=lambda group: group.index)

    # pack required[0]
    # perl: C An unsigned char (octet) value.
    packed_system_required += pack('B', 0)

    for group in sorted_groups:
        packed_system_required += pack('B', group.required)

    padding = int(consts['N_COMP_GROUPS'].val) - 1 - len(sorted_groups)
    # $packed_system_required .= pack("C".($consts->{N_COMP_GROUPS} - 1 - @sorted_groups), 0);
    for _ in range(0, padding):
        packed_system_required += pack('B', 0)

    return packed_system_required


def pack_system_menu_idx(consts: Dict[str, Const], log_menus: List[LogMenu]) -> bytes:
    packed_system_menu_idx = b''

    # generate n_log_menus
    # perl: n  An unsigned short (16-bit) in "network" (big-endian) order.
    packed_system_menu_idx += pack('!H', len(log_menus))

    # Generate menu_name_idx array

    for log_menu in log_menus:
        # perl: C An unsigned char (octet) value.
        packed_system_menu_idx += pack('B', log_menu.log_menu_name_idx)
    padding = int(consts['MAX_LOG_MENUS'].val) - len(log_menus)
    packed_system_menu_idx += pack(f'{padding}x')

    # Generate menu_prop_idx array
    for log_menu in log_menus:
        # perl: C An unsigned char (octet) value.
        packed_system_menu_idx += pack('B', log_menu.log_menu_prop_idx)
    padding = int(consts['MAX_LOG_MENUS'].val) - len(log_menus)
    packed_system_menu_idx += pack(f'{padding}x')

    return packed_system_menu_idx


def pack_system_dims(consts: Dict[str, Const], dims: Dict[int, Dim]) -> bytes:
    # Pack dims array
    # Dims array has holes with 'undef' values, which should be packed with default values
    # Also end of the list needs to be filled with this default values

    packed_system_dims = b''

    unused_dim = Dim(dim_idx=0xFF,
                     bus_addr=0,
                     name_idx=0,
                     # rest of mandatory attribs...
                     name="",
                     position="",
                     type="",
                     variant=-1
                     )

    if dims:
        max_dim_idx = max(dims.keys())
        dim_count = max_dim_idx + 1
        for i in range(0, dim_count):
            if i in dims:
                dim = dims[i]
                dim_type_idx = dim.dim_type.platform_dim_type_idx
            else:
                dim = unused_dim
                dim_type_idx = 0
            # perl: C An unsigned char (octet) value.
            packed_system_dims += pack('B', dim.dim_idx)
            packed_system_dims += pack('B', dim.bus_addr)
            # perl: n An unsigned short (16-bit) in "network" (big-endian) order.
            packed_system_dims += pack('!H', dim.name_idx)
            packed_system_dims += pack('!H', dim_type_idx)
    else:
        dim_count = 0

    # Pad to the end of dims array
    dims_to_pad = int(consts['MAX_DIMS'].val) - dim_count
    for i in range(0, dims_to_pad):
        dim_type_idx = 0
        # perl: C An unsigned char (octet) value.
        packed_system_dims += pack('B', unused_dim.dim_idx)
        packed_system_dims += pack('B', unused_dim.bus_addr)
        # perl: n An unsigned short (16-bit) in "network" (big-endian) order.
        packed_system_dims += pack('!H', unused_dim.name_idx)
        packed_system_dims += pack('!H', dim_type_idx)

    return packed_system_dims


def gen_sysdb(consts: Dict[str, Const],
              sorted_systems: List[System],
              log_menu_names: Dict[str, int],
              log_menu_props: Dict[str, int]) -> bytes:
    fgc_sysdb = b''

    fgc_sysdb += pack_headers(consts, len(sorted_systems))  # nb of systems + unknown
    fgc_sysdb += pack_menu_names(consts, log_menu_names)
    fgc_sysdb += pack_menu_props(consts, log_menu_props)
    for system in sorted_systems:
        fgc_sysdb += pack_system_desc(consts, system.type, system.label)
        fgc_sysdb += pack_system_required(consts, system.groups)
        fgc_sysdb += pack_system_menu_idx(consts, system.log_menus)
        fgc_sysdb += pack_system_dims(consts, system.dims)

    return fgc_sysdb


