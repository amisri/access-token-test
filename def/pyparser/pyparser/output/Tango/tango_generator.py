from pathlib import Path
from fgc_parser_schemas import (SuperDict, Class, Property, Zone)

from pyparser.output.base import BaseJinjaSuperDictOutGen


class TangoGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate Tango stuff.
    """

    @staticmethod
    def make_property(prop: Property, cls: Class):
        new_prop = dict()
        new_prop["name"] = prop.full_name
        new_prop["type"] = prop.type.name

        if "SYM_LST" in prop.flags:
            new_prop["type"] = "ENUM"
        if prop.set and prop.get:
            new_prop["access"] = "ALL"
        elif prop.set:
            new_prop["access"] = "SET"
        elif prop.get:
            new_prop["access"] = "GET"

        # check if maxels is an int
        try:
            int(prop.maxels)
        except:
            maxels_is_int = False
        else:
            maxels_is_int = True

        if cls.id in prop.specific_maxels:
            maxels = prop.specific_maxels[cls.id][4:]
            new_prop["length"] = cls.consts[maxels].val
        elif maxels_is_int and int(prop.maxels) > 1:
            new_prop["length"] = prop.maxels
        elif prop.maxels.startswith("FGC"):
            maxels = prop.maxels[4:]
            new_prop["length"] = cls.consts[maxels].val

        return new_prop

    @staticmethod
    def make_pubdata(zone: Zone, cls: Class):
        prop = zone.property
        new_prop = dict()
        new_prop["name"] = zone.full_name
        new_prop["type"] = prop.type.name
        new_prop["access"] = "SUB"
        if "SYM_LST" in prop.flags:
            new_prop["type"] = "ENUM"
        if "BIT_MASK" in prop.flags:
            new_prop["type"] = "BITMASK"

        return new_prop

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # paths

        tango_path = project_path / 'sw' / 'kt' / 'fgctango' / 'fgctango' / 'defs'

        for cls in self.superdict.classes.values():
            if cls.kt:
                properties = {}
                for prop in cls.properties.values():
                    if prop.kt and prop.type.name != "PARENT":
                        properties[prop.full_name] = self.make_property(prop, cls)
                for zone in cls.pubdata.children:
                    prop = zone.property
                    if prop:
                        properties[zone.full_name] = self.make_pubdata(zone, cls)

                self.add_template("prop_info.json.j2",
                                  out_name=f"class_{cls.id}_prop_info.json",
                                  out_path=tango_path,
                                  properties=properties.values(),
                                  )
