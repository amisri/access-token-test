from pathlib import Path
from math import log2

from fgc_parser_schemas import (SuperDict)

from pyparser.output.base import BaseJinjaSuperDictOutGen


class ConfigGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate config stuff.
    """

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        alarm_fault_codes_path = project_path / 'sw' / 'utilities' / 'perl' / 'databases' / 'controls'

        # prepare needed data for fault codes
        symlists = {}
        for cls in sorted(superdict.classes.values(), key=lambda v: int(v.id)):
            if cls.platform.name == "fgc4":
                continue
            # find faults symlist
            faults_symlist = None
            warnings_symlist = None
            for memzone in cls.pubdata.children:
                if memzone.name == "ST_FAULTS":
                    if memzone.property.symlist:
                        faults_symlist = memzone.property.symlist
                    else:
                        faults_symlist = memzone.property.class_symlists[cls.id]

                if memzone.name == "ST_WARNINGS":
                    if memzone.property.symlist:
                        warnings_symlist = memzone.property.symlist
                    else:
                        warnings_symlist = memzone.property.class_symlists[cls.id]
            # skip classes without faults and warnings
            if not faults_symlist or not warnings_symlist:
                continue

            # sort warnings and faults
            warnings_sorted = sorted(warnings_symlist.consts, key=lambda const: cls.symtab_consts[const.name].value)
            faults_sorted = sorted(faults_symlist.consts, key=lambda const: cls.symtab_consts[const.name].value)

            symlists[cls.id] = (cls, warnings_sorted, faults_sorted)

        self.add_template("fault_codes.j2",
                          out_path=alarm_fault_codes_path,
                          symlists=symlists,
                          log2=log2)
        # noting generated as $superhash->{platforms}->{fgc} doesn't exist...
        # fgcs_path = project_path / 'hw' / 'fgc2' / 'pld' / 'tools' / 'jtagprog'
        # self.add_template("fgcs.j2", out_path=fgcs_path)
