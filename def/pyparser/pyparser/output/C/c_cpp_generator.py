import operator
from pathlib import Path
from typing import Optional, Dict, List
import re
from fgc_parser_schemas import (SuperDict, Zone, BootMenu, Property, GetFunction, SetFunction, SetIfFunction, Symlist,
                                Class, Flag, Type)
from pyparser.output.base import BaseJinjaSuperDictOutGen, jinja_fn


class CppGenerator(BaseJinjaSuperDictOutGen):
    GENERATOR_FILENAME = __file__
    """
    Generate C/C++ stuff.
    """

    SYMENTRIES_UPPER_LIMIT = 50
    FGC_SYMETRIES_LIMIT = 13
    FGCD_SYMENTRIES_LIMIT = 23

    @staticmethod
    @jinja_fn
    def get_next_prime(nb: int) -> int:
        is_prime = False

        while not is_prime:
            is_prime = True
            i = 2
            while is_prime and i < (nb / 2):
                if nb % i == 0:
                    is_prime = False
                i += 1
            nb += 1
        return nb - 1

    @staticmethod
    @jinja_fn
    def get_symlist(zone: Zone, class_id: str) -> Optional[Symlist]:
        if not zone.property:
            return None
        if zone.property.type.name.startswith("INT"):
            # property is an integer
            # check wether property has a symlist
            if zone.property.symlist:
                return zone.property.symlist
            elif zone.property.class_symlist:
                return zone.property.class_symlists[class_id]
        return None

    @staticmethod
    @jinja_fn
    def get_property_type(zone: Zone) -> str:
        if zone.property.type.name.startswith("INT"):
            if zone.property.type.name.endswith('U'):
                return f"uint{zone.numbits}_t"
            return f"int{zone.numbits}_t"
        elif zone.property.type.name == "FLOAT":
            return 'float'
        raise ValueError

    @staticmethod
    @jinja_fn
    def get_endianness(type: str) -> str:
        mapping = {
            "int16_t": "subUtilitiesSignedNtohs",
            "int32_t": "subUtilitiesSignedNtohl",
            "uint16_t": "ntohs",
            "uint32_t": "ntohl",
            "float": "subUtilitiesNtohf"
        }
        return mapping.get(type, "")

    @staticmethod
    @jinja_fn
    def get_type_cast(type: str) -> str:
        match = re.match(r"(u?)int(\d*)_t", type)

        if match:
            unsigned = match.group(1)
            size = int(match.group(2))
            # CMW does not support unsigned values, so we have to cast to next bigger type
            if unsigned:
                size *= 2

            # This is required to ensure that SUB properties behave the same way as SUB_XXX properties,
            # Since the japc code used to translate SUB_XXX properties publishes all integers as either 'int' or 'long'
            # But not 'short' or 'byte'
            if size < 32:
                size = 32

            return f"(int{size}_t)"
        return ""

    @staticmethod
    def get_boot_menus_functions(current_node: BootMenu, functions):
        if current_node.menus:
            for child in current_node.menus:
                CppGenerator.get_boot_menus_functions(child, functions)
        else:
            functions.append(current_node.function)

    @staticmethod
    @jinja_fn
    def get_prop_children(prop: Property, cls: Class) -> List[Property]:
        """
        Create array of property's children that are present in current class
        """
        res = []
        for child in prop.children:
            if child.dynamic:
                continue
            for classproperty in cls.properties.values():
                if classproperty.full_name == child.full_name:
                    res.append(classproperty)
        return res

    @staticmethod
    @jinja_fn
    def get_dsp_flag(prop: Property, cls: Class):
        if cls.id in prop.specific_dsp:
            return prop.specific_dsp[cls.id]
        return None

    @staticmethod
    @jinja_fn
    def get_prop_flags(prop: Property, cls: Class, add_dsp_flag=True) -> List[str]:
        flags = {}
        if cls.id in prop.specific_flags:
            flags = prop.specific_flags[cls.id]

        if flags:
            # Add flags not specified in the property attribute but added explicitly in Property
            if 'SYM_LST' in prop.flags:
                flags['SYM_LST'] = prop.flags['SYM_LST']
            if 'LIMITS' in prop.flags:
                flags['LIMITS'] = prop.flags['LIMITS']
        else:
            flags = prop.flags

        flags = sorted(list(flags.keys()))
        if add_dsp_flag:
            # Add DSP flag if this is a DSP property
            dsp = CppGenerator.get_dsp_flag(prop, cls)
            if dsp:
                flags.append(f"DSP_{dsp.upper()}")

        return flags

    @staticmethod
    @jinja_fn
    def get_type(prop: Property, cls: Class) -> Optional[Type]:
        if cls.id in prop.specific_types:
            return prop.specific_types[cls.id]
        elif prop.type:
            return prop.type
        return None

    @staticmethod
    @jinja_fn
    def get_set_index(prop: Property, cls: Class) -> Optional[str]:
        if cls.id in prop.specific_set:
            return prop.specific_set[cls.id].name
        elif prop.set:
            return prop.set.name
        return None

    @staticmethod
    @jinja_fn
    def get_get_index(prop: Property, cls: Class) -> Optional[str]:
        if cls.id in prop.specific_get:
            return prop.specific_get[cls.id].name
        elif prop.get:
            return prop.get.name
        return None

    @staticmethod
    @jinja_fn
    def get_maxels(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_maxels:
            return prop.specific_maxels[cls.id]
        elif prop.maxels:
            return prop.maxels
        return "0"

    @staticmethod
    @jinja_fn
    def get_numels(prop: Property, cls: Class, skip_dsp_properties=True) -> str:
        if CppGenerator.get_dsp_flag(prop, cls) and skip_dsp_properties:
            # property is a DSP property - skipped
            return "0"

        # If the property is not CONFIG and a saclar (maxels = 1), then force the numels to 1.
        # This will guarantee that non-CONFIG scalar properties always return a value.

        maxels = CppGenerator.get_maxels(prop, cls)
        flags = CppGenerator.get_prop_flags(prop, cls)
        if maxels == "1" and "CONFIG" not in flags:
            return "1"
        else:
            if cls.id in prop.specific_numels:
                return prop.specific_numels[cls.id]
            elif prop.numels:
                return prop.numels
        return "0"

    @staticmethod
    @jinja_fn
    def get_steps(prop: Property, cls: Class) -> str:
        """
        Check whether a step has been defined, otherwise default to type size
        """
        if cls.id in prop.specific_step:
            return prop.specific_step[cls.id]
        elif prop.type.name != "PARENT":
            return CppGenerator.get_type(prop, cls).size
        else:
            return "1"

    @staticmethod
    @jinja_fn
    def get_pars(prop: Property, cls: Class) -> Optional[str]:
        """
        Get parameter function
        """
        if cls.id in prop.specific_pars:
            return prop.specific_pars[cls.id]
        elif prop.pars:
            return prop.pars
        return None

    @staticmethod
    @jinja_fn
    def get_value(prop: Property, cls: Class) -> str:
        dsp = CppGenerator.get_dsp_flag(prop, cls)

        if cls.id in prop.specific_value and not dsp:
            if re.match("^[a-z]", prop.specific_value[cls.id]):
                return f"(void *) & ({prop.specific_value[cls.id]})"
            else:
                return f"(void *)({prop.specific_value[cls.id]})"
        elif prop.type.name == "PARENT":  # is also a parent property
            return f"& prop_{cls.id}_{prop.full_name}".replace(".", "_").lower()
        else:  # child has no value
            return "NULL"

    @staticmethod
    @jinja_fn
    def get_el_size(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_types:
            el_size = prop.specific_types[cls.id].size
        else:
            el_size = prop.type.size

        if cls.platform.name == "fgc2":
            # FGC2: property element size is in words
            el_size += "/4"

        return el_size

    @staticmethod
    @jinja_fn
    def get_value_dsp(prop: Property, cls: Class) -> str:
        if cls.id in prop.specific_value:
            if re.match("^[a-z]", prop.specific_value[cls.id]):
                return f"&{prop.specific_value[cls.id]}"
            else:
                return prop.specific_value[cls.id]
        else:
            return "NULL"

    @staticmethod
    def calculate_limits(cls: Class, prop: Property, limits: Dict[str, List[Dict[str, str]]], prop_limits, prop_min, prop_max):
        if prop.limits == "int":
            limitindex = 0
            for limit in limits.get("int", []):
                if prop_min and prop_max and limit["min"] == prop_min and limit["max"] == prop_max:
                    prop.limit_[cls.id] = f"(void *) & (int_limits_{cls.id}[{limitindex}])"
                limitindex += 1

            # If property's limit is undefined,
            # then add the property's min and max as the next element in limits

            if prop.limit_[cls.id] is None:
                new_limit = {"min": prop_min, "max": prop_max}
                limits.setdefault("int", []).append(new_limit)
                limitindex = len(limits["int"]) - 1
                prop.limit_[cls.id] = f"(void *) & (int_limits_{cls.id}[{limitindex}])"
        else:  # limits == float
            limitindex = 0
            for limit in limits.get("float", []):
                if limit["min"] == prop_min and limit["max"] == prop_max:
                    prop.limit_[cls.id] = f"(void *) & (float_limits_{cls.id}[{limitindex}])"
                limitindex += 1

            # If propery's limit is undefined,
            # then add the property's min and max as the next element in limits

            if prop.limit_[cls.id] is None:
                new_limit = {"min": prop_min, "max": prop_max}
                limits.setdefault("float", []).append(new_limit)
                limitindex = len(limits["float"]) - 1
                prop.limit_[cls.id] = f"(void *) & (float_limits_{cls.id}[{limitindex}])"

    def __init__(self, project_path: Path, superdict: SuperDict):
        super().__init__(project_path, superdict)

        # filters and tests

        self.add_filter("sort_by_id", lambda x: sorted(x, key=lambda y: int(operator.attrgetter('id')(y))))

        # test if property has at least 1 non-dynamic child
        self.add_test("non_dynamic_children",
                      lambda prop: prop.children and any([not child.dynamic for child in prop.children]))

        # FGCD inc
        fgcd_inc_path = self.output_path / 'sw' / 'fgcd' / 'inc'
        self.add_template("sub_cmwdata_gen.h.j2", out_path=fgcd_inc_path)

        # FGCD src
        fgcd_src_path = self.output_path / 'sw' / 'fgcd' / 'src'
        self.add_template("sub_cmwdata_gen.cpp.j2", out_path=fgcd_src_path)

        # generic files
        inc_path = self.output_path / 'sw' / 'inc'

        self.add_template("fgc_errs.h.j2", out_path=inc_path)
        self.add_template("fgc_codes_gen.h.j2", out_path=inc_path)
        self.add_template("fgc_consts_gen.h.j2", out_path=inc_path)
        self.add_template("fgc_parser_consts.h.j2", out_path=inc_path)
        self.add_template("fgc_runlog_entries.h.j2", out_path=inc_path)

        # Platform specific files
        for platform in self.superdict.platforms.values():
            if platform.name == "fgc4":
                continue
            platform_inc_path = inc_path / 'platforms' / platform.name

            if platform.id >= 50 and platform.boot_menu is not None:
                # get functions
                functions = []
                self.get_boot_menus_functions(platform.boot_menu, functions)
                self.add_template("menutree.h.j2",
                                  out_path=platform_inc_path / 'boot',
                                  platform=platform,
                                  functions=functions)
                self.add_template("menuconsts.h.j2",
                                  out_path=platform_inc_path / 'boot',
                                  platform=platform)

            for memmap_name, memmap in platform.memmaps.items():
                self.add_template("memmap.h.j2",
                                  out_name=f"memmap_{memmap_name.lower()}.h",
                                  out_path=platform_inc_path,
                                  memmap=memmap)
        # Class specific files
        for cls in sorted(self.superdict.classes.values(), key=lambda x: int(x.id)):
            if cls.platform.name == "fgc4":
                continue
            class_inc_dir = inc_path / 'classes' / cls.id

            self.add_template("defconst.h.j2",
                              out_path=class_inc_dir,
                              cls=cls)

            # get class specific asserts
            asserts = []
            for symlist in sorted(cls.class_symlists.values(), key=operator.attrgetter('name')):
                duplicates = []
                for const in symlist.consts:
                    if not const.assertion or not const.name:
                        continue

                    if const.value is None:
                        raise Exception(f"Assert defined but value attribute is missing for const {const.name}")
                        # todo: move it from here.
                    if const.assertion.upper() in duplicates:
                        raise Exception(f"Duplicate assertion {const.assertion}")
                        # todo: move it from here too. Perhaps it should be checked when reading yaml
                    duplicates.append(const.assertion.upper())

                    # escape
                    escaped = const.assertion
                    for ch in ['[', ']', '(', ')', ' ']:
                        escaped = escaped.replace(ch, '_')
                    asserts.append((const.assertion, const.value, escaped))

            self.add_template("defconst_assert.h.j2",
                              out_path=class_inc_dir,
                              cls=cls,
                              asserts=asserts)
            self.add_template("sub_defconst.h.j2",
                              out_path=class_inc_dir,
                              cls=cls)

            # prepare symlists for sub_defsyms
            symlists = []
            sym_dups = []
            if cls.pubdata:
                for zone in sorted(cls.pubdata.children, key=operator.attrgetter("name")):
                    symlist = None
                    if zone.property.type.name.startswith('INT'):
                        if zone.property.symlist:
                            symlist = zone.property.symlist
                        elif zone.property.class_symlist:
                            symlist = zone.property.class_symlists[cls.id]

                    if not symlist or symlist.name in sym_dups:
                        continue
                    sym_dups.append(symlist.name)
                    symlists.append(symlist)

            self.add_template("sub_defsyms.h.j2",
                              out_path=class_inc_dir,
                              cls=cls,
                              symlists=symlists)

            self.add_template("definfo.h.j2",
                              out_path=class_inc_dir,
                              cls=cls)

            # prepare data for defprops
            getfuncs: Dict[str, GetFunction] = {}
            setfuncs: Dict[str, SetFunction] = {}
            setiffuncs: Dict[str, SetIfFunction] = {}
            symlists: Dict[str, Symlist] = {}
            parsfuncs: Dict[str, bool] = {}
            propertysymbols: Dict[str, Property] = {}

            for prop in cls.properties.values():
                # Check that no property with this symbol is already defined
                # or currently defined property is not a top-level property
                if prop.name not in propertysymbols or (prop.name in propertysymbols and
                                                        propertysymbols[prop.name].parent):
                    propertysymbols[prop.name] = prop

                # Callbacks (class specific, platform specific or generic)
                # get
                if cls.id in prop.specific_get:
                    get_f = prop.specific_get[cls.id]
                else:
                    get_f = prop.get

                if get_f:
                    getfuncs[get_f.name] = get_f

                # set
                if cls.id in prop.specific_set:
                    set_f = prop.specific_set[cls.id]
                else:
                    set_f = prop.set

                if set_f:
                    setfuncs[set_f.name] = set_f

                # setif
                if prop.setif:
                    setiffuncs[prop.setif.name] = prop.setif

                # symlist
                if prop.symlist:
                    symlists[prop.symlist.name] = prop.symlist

                if prop.class_symlist:
                    symlists[prop.class_symlist] = prop.class_symlists[cls.id]

                # parse callbacks
                pars = CppGenerator.get_pars(prop, cls)

                if pars and cls.id not in prop.specific_dsp:
                    parsfuncs[pars] = True
            # calculate properties limits
            limits = {}
            for prop in cls.properties.values():
                # set property limit to None
                prop.limit_[cls.id] = None

                if cls.id in prop.specific_limits:
                    self.calculate_limits(cls,
                                          prop,
                                          limits,
                                          prop.specific_limits[cls.id],
                                          prop.specific_min[cls.id],
                                          prop.specific_max[cls.id]
                                          )
                elif prop.limits:
                    self.calculate_limits(cls,
                                          prop,
                                          limits,
                                          prop.limits,
                                          prop.min,
                                          prop.max
                                          )
                elif prop.symlist:
                    prop.limit_[cls.id] = f"& sym_lst_{cls.id}_{prop.symlist.name}".lower()
                elif prop.class_symlist:
                    prop.limit_[cls.id] = f"& sym_lst_{cls.id}_{prop.class_symlists[cls.id].name.lower()}"
                else:
                    prop.limit_[cls.id] = "NULL"

            # check symlist limit

            if cls.name.startswith("FGC2") or cls.name.startswith("FGC3"):
                symlist_entry_limit = self.FGC_SYMETRIES_LIMIT
            elif cls.name.startswith("FGCD"):
                symlist_entry_limit = self.FGCD_SYMENTRIES_LIMIT
            else:
                symlist_entry_limit = self.SYMENTRIES_UPPER_LIMIT

            for symlist in symlists.values():
                for symbol in symlist.consts:
                    if len(symbol.symbol) > symlist_entry_limit:
                        raise Exception(f"Symlist entry {symbol.symbol} exceeds limit of {symlist_entry_limit} for class {cls.name}")

            self.add_template("defprops.h.j2",
                              out_path=class_inc_dir,
                              cls=cls,
                              getfuncs=getfuncs,
                              setfuncs=setfuncs,
                              setiffuncs=setiffuncs,
                              symlists=symlists,
                              parsfuncs=parsfuncs,
                              propertysymbols=propertysymbols,
                              limits=limits,
                              )
            self.add_template("defsyms.h.j2",
                              out_path=class_inc_dir,
                              cls=cls,
                              symlists=symlists)
            self.add_template("stat.h.j2",
                              out_path=class_inc_dir,
                              out_name=f"{cls.id}_stat.h",
                              cls=cls)


            # EPCCCS-9422
            # If a class has a SPY symlist, generate spy_class.h
            if "SPY" in symlists:
                self.add_template("spy_class.h.j2",
                                  out_path=class_inc_dir,
                                  spy_symlist=symlists["SPY"],
                                  cls=cls
                                  )

            # prepare consts
            consts = []
            for zone in cls.pubdata.children:
                if zone.property:
                    if zone.property.symlist:
                        symlist = zone.property.symlist
                    elif zone.property.class_symlist:
                        symlist = zone.property.class_symlists[cls.id]
                    else:
                        continue
                    consts.extend(symlist.consts)

            self.add_template("fgc_stat_consts.h.j2",
                              out_path=class_inc_dir,
                              cls=cls,
                              consts=consts)

            # write DSP header files
            for dsp in cls.dsps.values():
                properties = sorted(dsp.properties.values(), key=lambda property: dsp.property_idxs[property.full_name])

                # Create a list of pars functions for this DSP
                parsfuncs.clear()

                for prop in properties:
                    if cls.id in prop.specific_pars:
                        pars = prop.specific_pars[cls.id]
                    else:
                        pars = prop.pars

                    if pars and cls.id in prop.specific_dsp:
                        parsfuncs[pars] = True

                self.add_template("defprops_dsp.h.j2",
                                  out_path=class_inc_dir,
                                  out_name=f"defprops_dsp_{dsp.name}.h",
                                  cls=cls,
                                  dsp=dsp,
                                  parsfuncs=parsfuncs,
                                  properties=properties)
