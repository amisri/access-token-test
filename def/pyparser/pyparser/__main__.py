import argparse
import logging
import pathlib
import pickle
import concurrent.futures
from pathlib import Path
from typing import Optional, List

from fgc_parser_schemas import SuperDict

from pyparser import DEFAULT_PARSER_SRC_PATH, PROJECT_PATH
from pyparser.input.readers import READERS
from pyparser.input.extra_parsing import extra_parse
from pyparser.translators.translator import TRANSLATORS_NAMES
from pyparser.input.testers import test
from pyparser.output import GENERATORS, GENERATORS_NAMES
from pyparser.input.parsers.yaml import FastYamlParser, RoundTripYAMLParser
from pyparser.fgc_parser_schemas_hashing import get_fgc_parser_schemas_hash
from pyparser.fgc_parser_schemas_hash import FGC_PARSER_SCHEMAS_HASH

VERBOSE = False


# set up logging
# https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
# with minor changes

class CustomColorLogFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(levelname)s:%(name)s:%(message)s"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def log_error(msg: str, exc_info):
    if VERBOSE:
        logging.error(msg, exc_info=exc_info)
    else:
        logging.error(msg)


def parse(src_path: Path, superdict: SuperDict, yaml_parser):
    logging.info("Start parsing!")
    # call readers
    try:
        for reader in READERS:
            reader(src_path, superdict, yaml_parser).parse()
        # additional parsing
        extra_parse(superdict)
    except Exception as e:
        log_error(f"{e}", exc_info=e)
        exit(1)
    logging.info("Done parsing!")


def dump(out_file: Path, superdict: SuperDict):
    logging.info(f"Pickling SuperDict and writing to {out_file}...")
    with open(out_file, 'wb') as f:
        pickle.dump(superdict, f)


def load(in_file: Path) -> SuperDict:
    logging.info(f"Initializing SuperDict from pickle {in_file}")
    with open(in_file, 'rb') as f:
        return pickle.load(f)


def gen_output(superdict: SuperDict, project_path: Path, generators: Optional[List]):
    from pyparser.output import EPICSGenerator, CppGenerator
    logging.info(f"Starting to generate output in {project_path}")

    # remove duplicates
    generators = set(generators if generators else [])

    # exceptions
    exceptions = []

    # EpicsGenerator depends on CppGenerator
    if EPICSGenerator in generators:
        generators.add(CppGenerator)

    if not generators:
        # generate all
        generators = GENERATORS
    else:
        selected_generators = ' '.join([gen.__name__ for gen in generators])
        logging.info(f"Selected generator(s): {selected_generators}")
        # generate only selected ones
        if len(generators) == 1:
            generator = generators.pop()
            try:
                generator(project_path, superdict).execute()
            except Exception as exc:
                exceptions.append((generator, exc))

    # check if we have still something to do...
    if generators:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            # EpicsGenerator depends on CppGenerator,
            # hence easiest way is to remove it from the pool and run separately.
            future_generator = {executor.submit(lambda gen: gen(project_path, superdict).execute(), generator):
                                    generator for generator in generators if generator != EPICSGenerator}
            for future in concurrent.futures.as_completed(future_generator):
                exc = future.exception()
                generator = future_generator[future]
                if exc:
                    exceptions.append((generator, exc))

    # run EpicsGenerator separately, to make sure it's run after CppGenerator.
    if EPICSGenerator in generators:
        try:
            EPICSGenerator(project_path, superdict).execute()
        except Exception as exc:
            exceptions.append((EPICSGenerator, exc))

    if exceptions:
        logging.error(f"Generation failed! {len(exceptions)} exception(s) occurred during generation of the output!")
        for generator, exc in exceptions:
            log_error(f"Exception occurred while running {generator.__name__}: {exc}", exc_info=exc)
        exit(1)
    else:
        logging.info(f"Done generating output")


def check_hash():
    # get hash
    current_hash = get_fgc_parser_schemas_hash()
    if current_hash != FGC_PARSER_SCHEMAS_HASH:
        logging.fatal("Stored hash of the fgc_parser_schemas.py doesn't match the one from the repository.\n"
                      "Please run update_hash.sh!")
        exit(1)


def main():
    def translator_type(translator_name: str):
        try:
            return TRANSLATORS_NAMES[translator_name]
        except KeyError:
            raise argparse.ArgumentTypeError(f"Translator {translator_name} doesn't exist.")

    def generator_type(generator_name: str):
        try:
            return GENERATORS_NAMES[generator_name]
        except KeyError:
            raise argparse.ArgumentTypeError(f"Generator {generator_name} doesn't exist.")

    cli = argparse.ArgumentParser(
        description="pyParser FGC definitions"
    )
    cli.add_argument("--src-path",
                     type=pathlib.Path,
                     default=DEFAULT_PARSER_SRC_PATH)

    cli.add_argument("--out-path",
                     type=pathlib.Path,
                     default=PROJECT_PATH,
                     help="Path to project root. Used for output generation.")

    cli.add_argument("--translate-all",
                     action='store_true',
                     help="Translate all XML input files to yaml")
    cli.add_argument("--translate",
                     action="append",
                     help=f"Translate only selected part of XML files. Choices: "
                          f"{sorted(TRANSLATORS_NAMES.keys())}",
                     type=translator_type)
    cli.add_argument("--generate",
                     action="append",
                     help=f"Run only selected generators. Choices: "
                          f"{sorted(GENERATORS_NAMES.keys())}",
                     type=generator_type)
    cli.add_argument("--dump-schema-to-pickle",
                     type=pathlib.Path,
                     help="Dump the contents of filled-in fgc parser schema into a pickle binary file")

    cli.add_argument("--load-schema-from-pickle",
                     type=pathlib.Path,
                     help="Instead of reading input yaml files, use pickled schema")
    cli.add_argument("--skip-parsing",
                     action='store_true',
                     help="Parsing will be skipped")
    cli.add_argument("--skip-generation",
                     action='store_true',
                     help="Generation will be skipped")
    cli.add_argument("--skip-tests",
                     action='store_true',
                     help="Tests will be skipped")
    cli.add_argument("--yaml-parser",
                     choices=['round-trip', 'fast'],
                     default="fast",
                     help="'fast' is the default but provides limited error reporting (no line/column in errors). "
                          "'round-trip' is slower but more accurate")
    cli.add_argument("--verbose",
                     action='store_true',
                     default=False,
                     help="Add verbosity to the logs")
    cli.add_argument("--skip-hash-check",
                     action='store_true',
                     help="Skip hash check needed for pickle generation"
                     )
    cli.add_argument("--copy-class-attributes",
                     nargs=2,
                     type=int,
                     help="Duplicate class specific attributes for properties. "
                          "Add 2 arguments: source and destination")
    cli.add_argument("--remove-class-attributes",
                     type=int,
                     help="Remove class specific attributes from properties for a given class")
    cli.add_argument("--remove-unused-class-attributes",
                     action='store_true',
                     help="Remove unused class attributes from the property files"
                     )

    args = cli.parse_args()

    if args.yaml_parser == 'round-trip':
        YAMLParser = RoundTripYAMLParser
    else:
        YAMLParser = FastYamlParser

    # root logger
    logger = logging.getLogger()
    streamhandler = logging.StreamHandler()
    streamhandler.setLevel(logging.DEBUG)
    streamhandler.setFormatter(CustomColorLogFormatter())
    logger.addHandler(streamhandler)

    if args.verbose:
        global VERBOSE
        VERBOSE = True
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    # sources path
    src_path = args.src_path

    # clone class specific attributes
    if args.copy_class_attributes:
        source = args.copy_class_attributes[0]
        target = args.copy_class_attributes[1]
        logging.info(f"Cloning property class specific attributes FROM {source} TO {target}...")
        from pyparser.translators.class_attributes import PropertyClassAttributesTool
        tool = PropertyClassAttributesTool(src_path, source, target)
        tool.translate()
        logging.info(f"Clone of {source} into {target} done. Please check diff.")
        exit(0)

    # remove class specific attributes
    if args.remove_class_attributes is not None:
        cls_to_remove = args.remove_class_attributes
        logging.info(f"Removing property class specific attributes FROM {cls_to_remove}...")
        from pyparser.translators.class_attributes import PropertyClassAttributesTool
        tool = PropertyClassAttributesTool(src_path, cls_to_remove)
        tool.translate()
        logging.info(f"Removal of {cls_to_remove} done. Please check diff.")
        exit(0)

    # create instance of SuperDict
    superdict = SuperDict()

    if args.translate_all:
        for translator in TRANSLATORS_NAMES.values():
            translator(src_path).translate()
    elif args.translate:
        for translator in args.translate:
            translator(src_path).translate()

    if args.load_schema_from_pickle:
        try:
            superdict = load(args.load_schema_from_pickle)
        except FileNotFoundError:
            logging.fatal(f"File {args.load_schema_from_pickle} doesn't exist!")
            raise
        except Exception as e:
            logging.fatal(f"Loading pickle failed! {e}")
            raise
    else:
        if not args.skip_parsing:
            parse(src_path, superdict, YAMLParser)
        else:
            logging.info("Skipping parsing!")

    if not args.skip_tests:
        test_results = test(superdict, YAMLParser)
        if test_results:
            logging.error("Tests failed.")
            exit(1)
    else:
        logging.info("Skipping tests!")

    if args.dump_schema_to_pickle:
        if args.skip_hash_check:
            logging.warning("Skipping hash check of the FGC schemas! Do not use in the production environment!")
        else:
            logging.info("Comparing stored FGC schemas hash with the existing one...")
            # compare
            check_hash()
            logging.info("Comparison ok!")
        dump(args.dump_schema_to_pickle, superdict)

    # remove UNUSED class specific attributes
    # SuperDict is needed to see what properties are used
    if args.remove_unused_class_attributes:
        logging.info(f"Removing UNUSED class specific attributes...")
        from pyparser.translators.class_attributes import PropertyClassAttributesTool
        tool = PropertyClassAttributesTool(src_path, remove_unused=True, superdict=superdict)
        tool.translate()
        logging.info(f"Removal of unused properties done. Please check diff.")
        exit(0)

    if not args.skip_generation:
        gen_output(superdict, args.out_path, args.generate)
    else:
        logging.info("Skipping generation!")


if __name__ == "__main__":
    main()

