from .base import TranslatorBase
import xml.etree.ElementTree as ET
import ruamel.yaml


class DimNamesTranslator(TranslatorBase):
    def translate(self):
        input_xml = self.src_path / "dim_names.xml"
        output_yaml = self.src_path / "dim_names.yaml"

        tree = ET.parse(input_xml)
        root = tree.getroot()

        output = {"dim_names": []}

        for item in root:
            output["dim_names"].append(item.attrib["name"])
        yaml = ruamel.yaml.YAML()
        with open(output_yaml, 'w') as f:
            f.write("""
# FGC2 platform DIM names

# All DIM names must be defined in this file before they can be
# referenced in system definition files.  Max is 13 characters
# because they are FGC symbols.

# 1234567890123\n""")
            yaml.dump(output, f)
            f.write("# EOF")
