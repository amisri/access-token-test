import copy
from pathlib import Path
from pyparser.input.parsers.yaml import RoundTripYAMLParser
from fgc_parser_schemas import SuperDict
from ruamel.yaml.comments import CommentedMap
from typing import List, Tuple, Dict, Optional
import logging

class PropertyClassAttributesTool:
    """
    * Clone class specific attributes from another class
    * Remove class specific attributes of a given class
    * Remove unused class specific attributes
    """

    def __init__(self, src_path: Path, cls: int = None, target_cls: Optional[int] = None, remove_unused: bool = False,
                 superdict: SuperDict = None):
        self.src_path = src_path

        self.properties_path = src_path / "properties"

        # get only top level properties
        self.properties_files = list(self.properties_path.glob("*.yaml"))

        self.classes = [int(x.name) for x in (self.src_path / "classes").iterdir() if x.is_dir()]

        self.source = cls
        self.target = target_cls

        self.class_properties = {}

        self.remove_unused = remove_unused

        self.superdict = superdict

        if self.remove_unused:
            # get the class properties
            for cls in self.classes:
                self.class_properties[cls] = self._get_class_properties(cls)

        self.included_files = {}

    def _get_class_properties(self, cls_id: int) -> List[str]:
        cls_id = str(cls_id)
        if cls_id in self.superdict.classes:
            properties = self.superdict.classes[cls_id].properties.keys()
            return list(properties)
        else:
            return []

        # properties_file = self.src_path / "classes" / str(cls_id) / "properties.yaml"
        # if not properties_file.exists():
        #     return []
        #
        # yaml = RoundTripYAMLParser(properties_file, self.src_path, load_includes=False)
        #
        # return yaml.data["properties"]

    def _handle_property_element(self, property_element: CommentedMap, input_file: Path, parent_name: str = None):
        if parent_name is None:
            parent_name = ""

        # get property name
        property_name = property_element["name"]

        # compose full property name
        if parent_name == "":
            full_property_name = property_name
        else:
            full_property_name = f"{parent_name}.{property_name}"

        # handle nodes
        if "children" in property_element:
            for child in property_element["children"]:
                if "include" in child:
                    # run again for that file
                    included_file_path = child["include"]["file"]
                    included_file_path = self.get_include_file_full_path(input_file, included_file_path)

                    # single file might be included from different files
                    self.included_files.setdefault(included_file_path, []).append(full_property_name)
                    # parse it later
                    # self.handle_property_file(included_file_path, full_property_name)
                elif "property" in child:
                    self._handle_property_element(child["property"], input_file, full_property_name)
        # handle other tags
        if self.source is not None:
            # clone or remove given class
            if self.source in property_element:
                source_element = property_element[self.source].copy()
                if self.target is None:
                    # removal
                    del property_element[self.source]
                else:
                    # clone
                    position = list(property_element.keys()).index(self.source)
                    property_element.insert(position, self.target, source_element)

        elif self.remove_unused:
            # get class-like attributes
            attributes = [attr for attr in property_element.keys() if str(attr).isnumeric()]
            for attribute in attributes:
                if attribute in self.classes:
                    # existing class - check if property is used
                    if full_property_name not in self.class_properties[attribute]:
                        del property_element[attribute]
                else:
                    # class doesn't exist - remove attribute
                    del property_element[attribute]

    def handle_property_file(self, property_file: Path, parent_name: str = None):
        yaml = RoundTripYAMLParser(property_file, self.src_path, load_includes=False)

        if "properties" in yaml.data:
            properties_element = yaml.data["properties"]
            for property_element in properties_element["children"]:
                self._handle_property_element(property_element["property"], property_file, parent_name)
        elif "property" in yaml.data:
            self._handle_property_element(yaml.data["property"], property_file, parent_name)
        # save
        with open(property_file, 'w') as f:
            YAML = yaml._get_yaml()
            YAML.dump(yaml.data, f)

    def translate(self):
        for property_file in self.properties_files:
            self.handle_property_file(property_file, parent_name=None)

        # handle included files
        for included_file, parents in self.included_files.items():
            if len(parents) == 1:
                self.handle_property_file(included_file, parents[0])
            else:
                logging.info(f"Skipped file: {included_file} as it's being included multiple times")

    def get_include_file_full_path(self, input_file: Path, filename: str) -> Path:
        # make sure forward slashes are used
        filename = filename.replace('\\', '/')

        # construct path based on base path
        full_path = self.properties_path / Path(filename)

        if full_path.exists():
            return full_path

        # construct path based on path of the file that has the "include"
        including_file_path = input_file.parent

        full_path = including_file_path / Path(filename)

        if full_path.exists():
            return full_path

        raise Exception(f"Could not find file '{filename}' referenced in include element in file {input_file}")