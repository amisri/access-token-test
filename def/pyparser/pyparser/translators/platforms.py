from pathlib import Path
from .base import TranslatorBase, Element


class PlatformsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "platforms"
        super().__init__(src_path)

        self.platforms_dirs = [x for x in self.src_path.iterdir() if x.is_dir()]

    def translate(self):
        for platform in self.platforms_dirs:
            PlatformsBootMenuTranslator(platform / "boot").translate()
            PlatformsHwTranslator(platform / "hw").translate()
            PlatformMemMapTranslator(platform / "memmaps").translate()
            PlatformMemMapIncTranslator(platform / "memmaps_inc").translate()
            PlatformSharedSymsTranslator(platform / "shared_syms").translate()

            self.input_files.clear()

            self.element_to_yaml_fn = element_to_yaml

            main = platform / "main.xml"
            if main.exists():
                self.input_files.append(main)
                super().translate()
                self.input_files.clear()

            shared_consts = platform / "shared_consts.xml"
            if shared_consts.exists():
                self.element_to_yaml_fn = consts_element_to_yaml
                self.input_files.append(shared_consts)
                self.backup_fake_root = "consts"
                super().translate()


class PlatformsBootMenuTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        if self.src_path.exists():
            self.input_files = [x for x in (src_path / "submenus").iterdir() if x.is_file() and x.suffix == ".xml"]
            self.input_files.append(src_path / "menu.xml")
            self.element_to_yaml_fn = element_to_yaml


class PlatformsHwTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)

        if self.src_path.exists():
            hw_dirs = [x for x in src_path.iterdir() if x.is_dir()]

            for hw_dir in hw_dirs:
                connectors = hw_dir / "connectors.xml"
                if connectors.exists():
                    self.input_files.append(connectors)
                main = hw_dir / "main.xml"
                if main.exists():
                    self.input_files.append(main)
                xilinxes = hw_dir / "xilinxes.xml"
                if xilinxes.exists():
                    self.input_files.append(xilinxes)

            self.element_to_yaml_fn = element_to_yaml


class PlatformMemMapTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)

        if self.src_path.exists():
            self.input_files = [x for x in self.src_path.iterdir() if x.is_file() and x.suffix == ".xml"]
            self.element_to_yaml_fn = element_to_yaml


class PlatformSharedSymsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)

        if self.src_path.exists():
            self.input_files = [x for x in self.src_path.iterdir() if x.is_file() and x.suffix == ".xml"]
            self.element_to_yaml_fn = element_to_yaml
            self.fake_root_name = "consts"


class PlatformMemMapIncTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)

        if self.src_path.exists():
            self.input_files = list(self.src_path.rglob("*.xml"))
            self.element_to_yaml_fn = element_to_yaml
            self.backup_fake_root = "fakezone"


def consts_element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    # check for pseudoroot
    if ("pseudoroot" in node.attrs and len(node.attrs) == 1) or node.tag == "fakeroot":
        return representer.represent_sequence("", node.children)
    if node.attrs and not node.children:
        return representer.represent_mapping("", {node.tag: {**node.attrs}})
    elif not node.attrs and node.children:
        return representer.represent_mapping("", {node.tag: node.children})

    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)

def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    # check for pseudoroot
    if ("pseudoroot" in node.attrs and len(node.attrs) == 1) or node.tag == "fakeroot":
        return representer.represent_sequence("", node.children)

    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)
