from pathlib import Path
from .base import TranslatorBase


class CodesTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        self.input_files = [self.src_path / "codes.xml"]
