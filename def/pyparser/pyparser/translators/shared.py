from pathlib import Path
from .base import TranslatorBase, Element


class SharedTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "shared"
        super().__init__(src_path)

        self.proplist = self.src_path / "proplist" / "libref" / "fg.xml"

    def translate(self):
        SharedConstsTranslator(self.src_path / "consts").translate()
        SharedSymsTranslator(self.src_path / "syms").translate()
        SharedMemMapsTranslator(self.src_path / "memmaps").translate()
        if self.proplist.exists():
            self._translate_proplist()

    def _translate_proplist(self):
        properties = []
        with open(self.proplist, 'r') as f:
            contents = f.readlines()
        for line in contents:
            line = line.strip()
            if line:
                properties.append(line)

        yaml_data = {'properties': properties}

        yaml = self._get_yaml()

        yaml_contents = self._dump_yaml_to_string(yaml, yaml_data)

        output_yaml = self.proplist.with_suffix(".yaml")

        self._write_to_file(output_yaml, yaml_contents)


class SharedConstsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]
        self.element_to_yaml_fn = element_to_yaml


class SharedMemMapsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]
        self.element_to_yaml_fn = element_to_yaml
        self.backup_fake_root = "fakezone"
        #self.fake_root_name = "zone"


class SharedSymsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)

        self.input_files = self.src_path.rglob("*.xml")

        self.element_to_yaml_fn = syms_element_to_yaml
        self.fake_root_name = "consts"


def syms_element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.transform_text_child_to_attribute(node, 'cause')
    Element.transform_text_child_to_attribute(node, 'consequence')
    Element.transform_text_child_to_attribute(node, 'action')

    Element.fix_attributes(node)
    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)
