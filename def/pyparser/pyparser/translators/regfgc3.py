from pathlib import Path
from .base import TranslatorBase, Element


class RegFGC3Translator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "regfgc3"
        super().__init__(src_path)

        self.input_files = list(src_path.rglob("*.xml"))

        self.element_to_yaml_fn = element_to_yaml


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    # check for pseudoroot
    if ("pseudoroot" in node.attrs and len(node.attrs) == 1) or node.tag == "fakeroot":
        return representer.represent_sequence("", node.children)

    # special case for regfgc3/boards/boards.xml
    if node.tag == "barcode":
        attributes = {"barcode": str(node.text).strip()}
    else:
        attributes = {
            node.tag: {
                **node.attrs,
                "children": node.children
            }
        }
        if not node.children:
            attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)
