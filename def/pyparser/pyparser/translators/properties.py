from pathlib import Path
from .base import TranslatorBase, Element


class PropertiesTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "properties"
        super().__init__(src_path)

        properties_inc = list((self.src_path / "inc").rglob("*.xml"))

        properties = [x for x in self.src_path.iterdir() if x.is_file() and x.suffix == ".xml"]

        self.element_to_yaml_fn = element_to_yaml
        self.include_to_yaml_fn = include_to_yaml

        self.input_files = properties + properties_inc

        self.backup_fake_root = "properties"

        # paths in properties are relative to the file that it includes
        self.include_path_relative_to_source = False

    def _process_root_element(self, root_element: Element, output_yaml: Path):
        # add "properties" top level node
        if root_element.tag == "properties":
            root_element.attrs["name"] = output_yaml.name
            root_element.attrs.move_to_end("name", last=False)
            return root_element

        # create new top
        properties_element = Element(tag="properties")
        properties_element.attrs["name"] = output_yaml.name
        properties_element.children.append(root_element)
        return properties_element


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    # check for pseudoroot
    platforms_classes_mapping = {
        "fgcd": [0, 1, 2, 3, 4, 5, 6, 7, 8],
        "g64": [],
        "fgc2": [50, 51, 53, 59],
        "fgc3": [60, 62, 63, 64, 65],
        "fgcdeqp": [90, 91, 92, 93, 94, 98],
        "calsys": [150, 151]
    }
    classes = []
    for clses in platforms_classes_mapping.values():
        classes.extend(clses)

    platforms = platforms_classes_mapping.keys()

    def get_platform(cls):
        for k, v in platforms_classes_mapping.items():
            if cls in v:
                return k
        raise Exception(f"Class {cls} not found in any platforms")

    if 'symbol' in node.attrs:
        # replace it
        node.attrs = {"name" if k == "symbol" else k: v for k, v in node.attrs.items()}

    new_attrs = {}
    attrs_to_delete = []

    # transform class/platform specific attributes
    for attribute, value in node.attrs.items():
        for platform in platforms:
            suffix = f"_{platform}"
            if attribute.endswith(suffix):
                new_attr_name = attribute.replace(suffix, '')
                new_attrs.setdefault(platform, {})
                new_attrs[platform].setdefault("platform", {})
                new_attrs[platform]["platform"][new_attr_name] = value
                attrs_to_delete.append(attribute)

        for sclass in classes:
            suffix = f"_{sclass}"
            if attribute.endswith(suffix):
                new_attr_name = attribute.replace(suffix, '')
                platform = get_platform(sclass)
                new_attrs.setdefault(platform, {})
                new_attrs[platform].setdefault(sclass, {})
                new_attrs[platform][sclass][new_attr_name] = value
                attrs_to_delete.append(attribute)

    for attr in attrs_to_delete:
        node.attrs.pop(attr)

    node.attrs = {**node.attrs, **new_attrs}
    attributes = {
        node.tag:
            {
                **node.attrs,
                "children": node.children
            }
    }
    if not node.children:
        attributes["property"].pop("children")
    return representer.represent_mapping("", attributes)


def include_to_yaml(representer, node):
    mapping = {
        'include':
            {
                "name": node.name,
                "file": node.file
            }
    }
    return representer.represent_mapping("", mapping)
