from pathlib import Path
from .base import TranslatorBase, Element


class SystemsIncTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "systems_inc"
        super().__init__(src_path)

        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]
        self.element_to_yaml_fn = element_to_yaml

        # fake root needed

        self.fake_root_name = "components"


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    if node.tag == "components":
        attributes = {"components": node.children}
    else:
        attributes = {
            node.tag: {
                **node.attrs,
                "components": node.children
            }
        }
    if not node.children:
        attributes[node.tag].pop("components")
    return representer.represent_mapping("", attributes)
