from pathlib import Path
from .base import TranslatorBase, Element


class SymlistsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "symlists"
        super().__init__(src_path)

        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]
        self.element_to_yaml_fn = element_to_yaml


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)
