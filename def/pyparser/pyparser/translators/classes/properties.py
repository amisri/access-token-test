from ruamel.yaml import RoundTripRepresenter
from ..base import TranslatorBase, XMLEventReader, Element


class ClassPropertiesTranslator(TranslatorBase):
    def translate(self):
        input_xml = self.src_path
        output_yaml = input_xml.with_suffix(".yaml")

        with open(input_xml) as f:
            contents = f.readlines()
        contents = "".join(contents)

        output = XMLEventReader().parse(contents)
        yaml = self._get_yaml()
        Element.to_yaml = element_to_yaml
        yaml.register_class(Element)

        idx, root_element = self._find_non_comment_node(output)
        self.top_level_comments = self._get_top_level_comments(output, idx)

        # children -> includes and comments
        properties_children = root_element.children
        output_dict = {}
        if properties_children:
            output_dict["includes"] = properties_children
        output_dict["properties"] = root_element.text.split()

        contents = self._dump_yaml_to_string(yaml, output_dict)

        # change comment from yaml node to real comment
        contents = contents.replace("- comment:", "#")

        self._write_to_file(output_yaml, contents)


def element_to_yaml(representer: RoundTripRepresenter, node):
    Element.transform_doc_child_to_attribute(node)

    if node.tag == "":  # root
        attributes = {
            "tl_properties": node.children
        }
    else:
        attributes = {
            node.tag: {
                **node.attrs,
                "includes": node.children,
                "properties": node.text.split()
            }
        }
        if not node.children:
            attributes[node.tag].pop("includes")
    return representer.represent_mapping("", attributes)
