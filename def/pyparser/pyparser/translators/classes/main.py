from pathlib import Path
from ruamel.yaml import RoundTripRepresenter
from ..base import TranslatorBase, Element


class ClassMainTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        self.input_files = [self.src_path / "main.xml"]
        self.element_to_yaml_fn = element_to_yaml


def element_to_yaml(representer: RoundTripRepresenter, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    # check for pseudoroot
    if ("pseudoroot" in node.attrs and len(node.attrs) == 1) or node.tag == "fakeroot":
        return representer.represent_sequence("", node.children)

    attributes = {
        node.tag: {
            **node.attrs,
            "children": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("children")
    return representer.represent_mapping("", attributes)
    # Element.transform_doc_child_to_attribute(node)
    # if node.children and not node.attrs:
    #     attributes = {node.tag: node.children}
    # else:
    #     # lets try to convert to int what's possible
    #     if node.tag == "class":  # top level
    #         attributes = {
    #             node.tag: {**node.attrs,
    #                       "areas": node.children
    #                       }
    #         }
    #     elif node.tag == "area" and node.children:  # links
    #         attributes = {
    #             node.tag: {**node.attrs,
    #                       "links": node.children
    #                       }
    #         }
    #     else:
    #         attributes = {
    #             node.tag: {**node.attrs}
    #         }
    # return representer.represent_mapping("", attributes)
