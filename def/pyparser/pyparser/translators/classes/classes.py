from pathlib import Path
from pyparser.translators.base import TranslatorBase
from .main import ClassMainTranslator
from .properties import ClassPropertiesTranslator
from .pubdata import ClassPubdataTranslator


class ClassConstsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        # besides "consts.xml" it might be also "consts_log.xml" in case of 62.
        self.input_files.extend(list(src_path.glob("consts*.xml")))


class ClassLogMenuTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        log_menu = self.src_path / "log_menu.xml"
        if log_menu.exists():
            self.backup_fake_root = "log_menus"
            self.input_files = [log_menu]


class ClassSymlistsTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "symlists"
        super().__init__(src_path)
        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]


class ClassesTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "classes"
        super().__init__(src_path)

        self.classes_dirs = [x for x in self.src_path.iterdir() if x.is_dir()]

    def translate(self):
        for class_dir in self.classes_dirs:
            ClassConstsTranslator(class_dir).translate()
            ClassMainTranslator(class_dir).translate()
            ClassPropertiesTranslator(class_dir / "properties.xml").translate()
            ClassPubdataTranslator(class_dir).translate()
            ClassSymlistsTranslator(class_dir).translate()
            ClassLogMenuTranslator(class_dir).translate()

            if class_dir.stem == "65":
                from pyparser.translators.shared import syms_element_to_yaml

                class Class65SymsTranslator(TranslatorBase):
                    def __init__(self, src_path: Path):
                        super().__init__(src_path)

                        self.input_files = [self.src_path / "faults.xml", self.src_path / "warnings.xml"]

                        self.element_to_yaml_fn = syms_element_to_yaml
                        self.fake_root_name = "consts"

                Class65SymsTranslator(class_dir).translate()
