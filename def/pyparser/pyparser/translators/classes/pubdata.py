from ruamel.yaml import RoundTripRepresenter
from pathlib import Path
from ..base import TranslatorBase, Element


class ClassPubdataTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        super().__init__(src_path)
        self.input_files = [self.src_path / "pubdata.xml"]
        self.element_to_yaml_fn = element_to_yaml


def element_to_yaml(representer: RoundTripRepresenter, node):
    Element.transform_doc_child_to_attribute(node)
    attributes = {
        node.tag: {
            **node.attrs,
            "zones": node.children
        }
    }
    if not node.children:
        attributes[node.tag].pop("zones")
    return representer.represent_mapping("", attributes)
