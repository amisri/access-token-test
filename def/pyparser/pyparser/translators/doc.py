from pathlib import Path
from .base import TranslatorBase, Element


class DocTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "doc"
        super().__init__(src_path)

        self.input_files = [self.src_path / "main.xml"]
        self.element_to_yaml_fn = element_to_yaml


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    if node.tag == "main":
        attributes = {"main": node.children}
    else:
        attributes = {
            node.tag: {
                **node.attrs,
                "links": node.children
            }
        }
    if not node.children:
        attributes[node.tag].pop("links")
    return representer.represent_mapping("", attributes)
