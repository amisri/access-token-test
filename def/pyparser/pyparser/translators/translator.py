import pathlib
from .codes import CodesTranslator
from .component_labels import ComponentLabelsTranslator
from .components import ComponentsTranslator
from .consts import ConstsTranslator
from .dim_names import DimNamesTranslator
from .errors import ErrorsTranslator
from .flags import FlagsTranslator
from .getfuncs import GetFuncsTranslator
from .getoptions import GetOptionsTranslator
from .property_groups import PropertyGroupsTranslator
from .runlog import RunlogTranslator
from .setfuncs import SetFuncsTranslator
from .setiffuncs import SetIfFuncsTranslator
from .types import TypesTranslator
from .classes.classes import ClassesTranslator
from .dim_types import DimTypesTranslator
from .dims_inc import DimsIncTranslator
from .doc import DocTranslator
from .platforms import PlatformsTranslator
from .properties import PropertiesTranslator
from .regfgc3 import RegFGC3Translator
from .shared import SharedTranslator
from .symlists import SymlistsTranslator
from .systems import SystemsTranslator
from .systems_inc import SystemsIncTranslator


TRANSLATORS = [
    CodesTranslator,
    ComponentLabelsTranslator,
    ComponentsTranslator,
    ConstsTranslator,
    DimNamesTranslator,
    ErrorsTranslator,
    FlagsTranslator,
    GetFuncsTranslator,
    GetOptionsTranslator,
    PropertyGroupsTranslator,
    RunlogTranslator,
    SetFuncsTranslator,
    SetIfFuncsTranslator,
    TypesTranslator,
    ClassesTranslator,
    DimTypesTranslator,
    DimsIncTranslator,
    DocTranslator,
    PlatformsTranslator,
    PropertiesTranslator,
    RegFGC3Translator,
    SharedTranslator,
    SymlistsTranslator,
    SystemsTranslator,
    SystemsIncTranslator
]

TRANSLATORS_NAMES = {
    translator.__name__.replace('Translator', ''): translator
    for translator in TRANSLATORS
}
