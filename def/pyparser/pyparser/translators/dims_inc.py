from pathlib import Path
from .base import TranslatorBase, Element, HexInt


class DimsIncTranslator(TranslatorBase):
    def __init__(self, src_path: Path):
        src_path = src_path / "dims_inc"
        super().__init__(src_path)

        self.input_files = [x for x in self.src_path.iterdir() if x.suffix == ".xml" and x.is_file()]
        self.element_to_yaml_fn = element_to_yaml

        # fake root needed

        self.fake_root_name = "dims"


def element_to_yaml(representer, node):
    Element.transform_doc_child_to_attribute(node)
    Element.fix_attributes(node)
    if node.tag == "dims":
        attributes = {"dims": node.children}
    else:
        if "bus_addr" in node.attrs:
            node.attrs["bus_addr"] = HexInt(int(node.attrs["bus_addr"], 16))
        attributes = {
            node.tag: {
                **node.attrs,
                "dims": node.children
            }
        }
    if not node.children:
        attributes[node.tag].pop("dims")
    return representer.represent_mapping("", attributes)
