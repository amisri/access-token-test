from ruamel.yaml import RoundTripRepresenter
from .base import TranslatorBase, XMLEventReader, Element


class RunlogTranslator(TranslatorBase):
    def translate(self):
        input_xml = self.src_path / "runlog.xml"
        output_yaml = input_xml.with_suffix(".yaml")

        with open(input_xml) as f:
            contents = f.readlines()
        contents = "".join(contents)

        output = XMLEventReader().parse(contents)
        yaml = self._get_yaml()
        yaml.width = 4096

        Element.to_yaml = element_to_yaml
        yaml.register_class(Element)

        idx, root_element = self._find_non_comment_node(output)

        self.top_level_comments = self._get_top_level_comments(output, idx)
        contents = self._dump_yaml_to_string(yaml, root_element)

        # change comment from yaml node to real comment
        contents = contents.replace("- comment:", "#")

        self._write_to_file(output_yaml, contents)


def element_to_yaml(representer: RoundTripRepresenter, node):
    Element.transform_doc_child_to_attribute(node)
    if node.children:
        attributes = {"runlog": node.children}
    else:
        node.attrs['length'] = int(node.attrs['length'])

        attributes = {
            "entry": {**node.attrs}
        }
        # attributes = node.attrs
    return representer.represent_mapping("", attributes)
