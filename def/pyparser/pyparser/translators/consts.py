from ruamel.yaml import RoundTripRepresenter
from .base import TranslatorBase, XMLEventReader, Element


class ConstsTranslator(TranslatorBase):
    def translate(self):
        input_xml = self.src_path / "consts.xml"
        output_yaml = input_xml.with_suffix(".yaml")

        with open(input_xml) as f:
            contents = f.readlines()
        contents = "".join(contents)

        output = XMLEventReader().parse(contents)
        yaml = self._get_yaml()
        Element.to_yaml = element_to_yaml
        yaml.register_class(Element)

        idx, root_element = self._find_non_comment_node(output)

        self.top_level_comments = self._get_top_level_comments(output, idx)
        contents = self._dump_yaml_to_string(yaml, root_element)

        # change comment from yaml node to real comment
        contents = contents.replace("- comment:", "#")
        # mutate the structure - treat as dictionary instead of a sequence of consts
        contents = contents.replace("- const:\n", "")

        self._write_to_file(output_yaml, contents)


def element_to_yaml(representer: RoundTripRepresenter, node):
    if node.children and not node.attrs:
        attributes = {"consts": node.children}
    else:
        Element.fix_attributes(node)
        attributes = {
            "const": {**node.attrs}
        }
    return representer.represent_mapping("", attributes)
