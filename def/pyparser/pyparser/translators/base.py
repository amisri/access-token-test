import pathlib
from io import StringIO
import xml.parsers.expat
import dataclasses
import typing
import logging
from collections import OrderedDict

import ruamel.yaml
from ruamel.yaml import RoundTripRepresenter
from ruamel.yaml.emitter import Emitter

from pyparser import DEFAULT_PARSER_SRC_PATH


class TranslatorBase:
    def __init__(self, src_path: pathlib.Path):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.src_path = src_path
        self.top_level_comments = []
        self.eof_comment = "# EOF\n"

        self.element_to_yaml_fn = default_element_to_yaml
        self.comment_to_yaml_fn = default_comment_to_yaml
        self.include_to_yaml_fn = default_include_to_yaml

        self.input_files = []

        # fake root
        self.fake_root_name = ""
        # backup fake root
        self.backup_fake_root = ""

        # translate includes relative source path. If False, it will be relative to the file included.
        self.include_path_relative_to_source = True

        self.logger.info(f"Translating in {self.src_path}...")

    def translate(self):
        for input_xml in self.input_files:

            # depending on the flag set:
            # include path will be relative to the file that has an "include" or to source path
            if self.include_path_relative_to_source:
                input_file_path = None
            else:
                input_file_path = input_xml

            output_yaml = input_xml.with_suffix(".yaml")
            with open(input_xml) as f:
                contents = f.readlines()
            if self.fake_root_name:
                contents.insert(0, f"<{self.fake_root_name}>\n")
                contents.append(f"</{self.fake_root_name}>\n")
            contents = "".join(contents)
            try:
                output = XMLEventReader(input_file_path=input_file_path).parse(contents)
            except Exception as exp:
                if self.backup_fake_root:
                    try:
                        contents = f"<{self.backup_fake_root}>\n" + contents + f"</{self.backup_fake_root}>\n"
                        output = XMLEventReader(input_file_path=input_file_path).parse(contents)
                    except Exception as exp2:
                        self.logger.error(f"Parsing {input_xml} failed twice:\n"
                                          f"{exp}\n"
                                          f"{exp2}")
                        continue
                else:
                    self.logger.error(f"Parsing error of file {input_xml}: {exp}.")
                    continue
            yaml = self._get_yaml()

            Element.to_yaml = self.element_to_yaml_fn
            yaml.register_class(Element)

            Comment.to_yaml = self.comment_to_yaml_fn
            yaml.register_class(Comment)

            Include.to_yaml = self.include_to_yaml_fn
            yaml.register_class(Include)

            idx, root_element = self._find_non_comment_node(output)

            root_element = self._process_root_element(root_element, output_yaml)

            self.top_level_comments = self._get_top_level_comments(output, idx)
            contents = self._dump_yaml_to_string(yaml, root_element)

            # change comment from yaml node to real comment

            contents = contents.replace("- comment: |-", "")
            contents = contents.replace("- comment:", "#")

            self._write_to_file(output_yaml, contents)

            self._finalize(output_yaml)

    def _process_root_element(self, root_element, output_yaml: pathlib.Path):
        return root_element

    def _finalize(self, output_yaml: pathlib.Path):
        pass

    def _process_str_root_element(self, root_element: str):
        return root_element

    def _write_to_file(self, output_yaml: pathlib.Path, contents: str):
        with open(output_yaml, 'w') as f:
            for comment in self.top_level_comments:
                f.write(comment)
            f.write(contents)
            f.write(self.eof_comment)

    @staticmethod
    def _find_non_comment_node(output):
        for i, child in enumerate(output.children):
            if not isinstance(child, Comment):
                return i, child

    def _get_top_level_comments(self, output, idx_non_comment=None):
        if idx_non_comment is None:
            idx_non_comment, _ = self._find_non_comment_node(output)
        comments = []
        for i in range(0, idx_non_comment):
            comment = "# "
            comment += output.children[i].comment
            comment = comment.replace("\n", "\n#")
            comment += "\n"
            comments.append(comment)
        return comments

    @staticmethod
    def _get_yaml():
        yaml = ruamel.yaml.YAML()

        def change_style(style, representer):
            def new_representer(dumper, data):
                scalar = representer(dumper, data)
                scalar.style = style
                return scalar

            return new_representer

        represent_literal_str = change_style('|', ruamel.yaml.SafeRepresenter.represent_str)

        yaml.representer.add_representer(LiteralString, represent_literal_str)
        yaml.representer.add_representer(HexInt, representer)

        def noop(*_, **__):
            pass

        Emitter.process_tag = noop

        yaml.register_class(Element)
        yaml.register_class(Text)
        yaml.register_class(Include)
        yaml.register_class(Comment)

        return yaml

    @staticmethod
    def _dump_yaml_to_string(yaml, root_element):
        stringstream = StringIO()
        yaml.dump(root_element, stringstream)
        output_str = stringstream.getvalue()
        stringstream.close()
        return output_str


class LiteralString(str):
    pass


class HexInt(int):
    pass


def representer(dumper, data):
    return ruamel.yaml.ScalarNode(
        'tag:yaml.org,2002:int',
        '0x{:08x}'.format(data))


@dataclasses.dataclass
class Element:
    tag: str
    attrs: typing.OrderedDict = dataclasses.field(default_factory=typing.OrderedDict)
    children: typing.List = dataclasses.field(default_factory=list)
    text: str = ""

    @staticmethod
    def transform_doc_child_to_attribute(node):
        # look for Text children
        docs_elements = [item for item in node.children if
                         isinstance(item, Element) and len(item.children) == 1 and isinstance(item.children[0], Text)]
        if docs_elements:
            node.children.remove(docs_elements[0])
            docs = docs_elements[0].children[0].text
            if docs.strip():
                # only if not empty
                node.attrs['doc'] = LiteralString(docs)

    @staticmethod
    def transform_text_child_to_attribute(node, child_tag):
        # find element
        elements_to_transfer = [item for item in node.children if isinstance(item, Element) and item.tag == child_tag]
        for element in elements_to_transfer:
            node.children.remove(element)
            el = element.text
            if el.strip():
                # only if not empty
                node.attrs[element.tag] = LiteralString(el)

    @staticmethod
    def fix_attributes(node):
        for k, v in node.attrs.items():
            try:
                val = int(v, 10)
            except:
                pass
            else:
                node.attrs[k] = val

    @staticmethod
    def to_yaml(representer: RoundTripRepresenter, node):
        Element.transform_doc_child_to_attribute(node)
        if node.children and not node.attrs:
            attributes = {node.tag: node.children}
        else:
            Element.fix_attributes(node)
            attributes = {
                node.tag: {
                    **node.attrs,
                    "children": node.children
                }
            }
            if not node.children:
                attributes[node.tag].pop("children")
        return representer.represent_mapping("", attributes)


default_element_to_yaml = Element.to_yaml


@dataclasses.dataclass
class Text:
    text: str


@dataclasses.dataclass
class Include:
    name: str
    file: str

    @staticmethod
    def fix_file_path(path: pathlib.Path, root_file: pathlib.Path = None) -> str:
        if root_file is not None:
            root_path = root_file.parent
            full_path = DEFAULT_PARSER_SRC_PATH / path
            path = full_path.relative_to(root_path)
        # make sure forward slashes are used
        return str(path.with_suffix(".yaml")).replace('\\', '/')

    @staticmethod
    def to_yaml(representer: RoundTripRepresenter, node):
        return representer.represent_mapping("", {'include': node.file})


default_include_to_yaml = Include.to_yaml


@dataclasses.dataclass
class Comment:
    comment: str

    @staticmethod
    def to_yaml(representer: RoundTripRepresenter, node):
        return representer.represent_mapping("", {"comment": LiteralString(node.comment)})


default_comment_to_yaml = Comment.to_yaml


class XMLEventReader:
    def __init__(self, input_file_path: pathlib.Path = None, handle_comments=True):
        self.input_file_path = input_file_path

        self.p = xml.parsers.expat.ParserCreate()
        # define handlers
        self.p.StartElementHandler = self._start_element
        self.p.EndElementHandler = self._end_element
        self.p.CharacterDataHandler = self._char_data
        self.p.ExternalEntityRefHandler = self._ext_ent
        self.p.StartCdataSectionHandler = self._start_cdata
        self.p.EndCdataSectionHandler = self._end_cdata

        if handle_comments:
            self.p.CommentHandler = self._comment

        self.cdata = None
        self.text = None
        self.root = None
        self.tag_stack = None

    def parse(self, contents):
        self._clear()
        self.p.Parse(contents)
        return self.root

    def _clear(self):
        self.cdata = False
        self.text = ""

        self.root = Element("")
        self.tag_stack = [self.root]

    def _start_element(self, tag, attributes):
        element = Element(tag, OrderedDict(attributes))
        self.tag_stack[-1].children.append(element)
        self.tag_stack.append(element)

    def _end_element(self, tag):
        self.tag_stack.pop()

    def _char_data(self, data):
        if self.cdata:
            if self.text:
                self.text += data.strip(' ')
            else:
                self.text += data.strip()
            # self.text += data.strip(' ')
        else:
            if self.tag_stack[-1].text:
                self.tag_stack[-1].text += data.strip(' ')
            else:
                self.tag_stack[-1].text += data.strip()

    def _comment(self, text: str):
        text = text.strip()
        output_text = ""
        for line in text.splitlines(keepends=True):
            output_text += f"# {line}"
        comment = Comment(output_text)
        self.tag_stack[-1].children.append(comment)

    def _ext_ent(self, context, base, system_id, public_id):
        include = Include(context, Include.fix_file_path(pathlib.Path(system_id), self.input_file_path))
        self.tag_stack[-1].children.append(include)
        return 1

    def _start_cdata(self):
        self.cdata = True

    def _end_cdata(self):
        self.cdata = False
        # add text
        text = Text(self.text)
        self.tag_stack[-1].children.append(text)
        # reset text
        self.text = ""
