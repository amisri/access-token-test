<!-- MEAS.SVC properties for FGC_64 (SVCs). 40 signals encoded in 10 DIMs. -->

<!-- They will be organized in groups:

    MEAS.SVC.P   for active power
    MEAS.SVC.Q   for reactive power
    MEAS.SVC.V   for voltage
    MEAS.SVC.I   for current
    MEAS.SVC.REG for regulation properties

So, ideally we would like to get data as: "g reg" and then get all the reg logs. -->

    <property
        symbol  = "SVC"
        type    = "PARENT"
        get     = "Parent"
        group   = "OPERATIONAL"
        title   = "SVC measurements"
    >
        <doc><![CDATA[
            This property contains the properties containing the RMS values of power, voltage, current, etc. derived by calculations in VsRegulationDSP.
        ]]></doc>


        <property
            symbol  = "P"
            type    = "PARENT"
            get     = "Parent"
            group   = "OPERATIONAL"
            title   = "Active power properties"
        >
            <doc><![CDATA[
                This property contains the properties containing the calculated active powers in various point of the network.
            ]]></doc>

           <property
                symbol  = "FEEDER"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Feeder active power properties"
            >
                <doc><![CDATA[
                    This property contains calculated active powers of the feeder, in W.
                    Positive values: Feeder supplies active power,
                    Negative values: Feeder absorbs active power.
                ]]></doc>

               <property
                    symbol         = "SUM"
                    title          = "Sum of active power"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.feeder.sum.value"
                >
                    <doc><![CDATA[
                        Feeder 3-phase active power in W.
                    ]]></doc>
                </property>

               <property
                    symbol         = "R"
                    title          = "Feeder active power of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.feeder.r.value"
                >
                    <doc><![CDATA[
                        Feeder active power of phase R.
                     ]]></doc>
                </property>

               <property
                    symbol         = "S"
                    title          = "Feeder active power of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.feeder.s.value"
                >
                    <doc><![CDATA[
                        Feeder active power of phase S.
                    ]]></doc>
                </property>

               <property
                    symbol         = "T"
                    title          = "Feeder active power of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.feeder.t.value"
                >
                    <doc><![CDATA[
                        Feeder active power of phase T.
                    ]]></doc>
                </property>
            </property>

            <property
                symbol  = "LOAD"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Load active power properties"
            >
                <doc><![CDATA[
                    This property contains calculated active powers of the load, in W.
                    Positive values: Load absorbs active power from the network.
                    Negative values: Load injects active power to the network.
                ]]></doc>

                <property
                    symbol         = "SUM"
                    title          = "Sum of active power"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.load.sum.value"
                >
                    <doc><![CDATA[
                        Load 3-phase active power.
                    ]]></doc>
                </property>

                <property
                    symbol         = "R"
                    title          = "Load active power of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.load.r.value"
                >
                    <doc><![CDATA[
                        Load active power of phase R.
                    ]]></doc>
                </property>

                <property
                    symbol         = "S"
                    title          = "Load active power of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.load.s.value"
                >
                    <doc><![CDATA[
                        Load active power of phase S.
                    ]]></doc>
                </property>

                <property
                    symbol         = "T"
                    title          = "Load active power of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.p.load.t.value"
                >
                    <doc><![CDATA[
                        Load active power of phase T.
                    ]]></doc>
                </property>
            </property>

        </property>

        <property
            symbol  = "Q"
            type    = "PARENT"
            get     = "Parent"
            group   = "OPERATIONAL"
            title   = "Reactive power properties"
        >
            <doc><![CDATA[
                This property contains the properties containing the calculated reactive powers in various point of the network.
            ]]></doc>

            <property
                symbol  = "FEEDER"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Feeder reactive power properties"
            >
                    <doc><![CDATA[
                        This property contains calculated powers of the feeder, in VAR.
                        Positive values: Feeder supplies active power,
                        Negative values: Feeder absorbs active power.
                    ]]></doc>
                <property
                    symbol         = "SUM"
                    title          = "Sum of reactive power"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.feeder.sum.value"
                >
                    <doc><![CDATA[
                        3-phase reactive power.
                    ]]></doc>
                </property>

                <property
                    symbol         = "R"
                    title          = "Feeder reactive power of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.feeder.r.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase R.
                    ]]></doc>
                </property>

                <property
                    symbol         = "S"
                    title          = "Feeder reactive power of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.feeder.s.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase S.
                    ]]></doc>
                </property>

                <property
                    symbol         = "T"
                    title          = "Feeder reactive power of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.feeder.t.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase T.
                    ]]></doc>
                </property>

           </property>

           <property
                symbol  = "LOAD"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Load reactive power properties"
            >
                <doc><![CDATA[
                    This property contains calculated powers of the load, in VAR.
                    Positive values: Load absorbs reactive power from the network,
                    Negative values: Load supplies reactive power.
                ]]></doc>

               <property
                    symbol         = "SUM"
                    title          = "Sum of reactive power"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.load.sum.value"
                >
                    <doc><![CDATA[
                        3-phase reactive power.
                    ]]></doc>
                </property>

                <property
                    symbol         = "R"
                    title          = "Load reactive power of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.load.r.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase R.
                    ]]></doc>
                </property>

                <property
                    symbol         = "S"
                    title          = "Load reactive power of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.load.s.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase S.
                    ]]></doc>
                </property>

                <property
                    symbol         = "T"
                    title          = "Load reactive power of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.load.t.value"
                >
                    <doc><![CDATA[
                        Reactive power of phase T.
                    ]]></doc>
                </property>

           </property>

           <property
                symbol  = "TCR"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "TCR reactive power properties"
            >
                <doc><![CDATA[
                    This property contains calculated powers of the TCR, in VAR.
                ]]></doc>

                <property
                    symbol         = "SUM"
                    title          = "Sum of reactive power"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.tcr.sum.value"
                >
                    <doc><![CDATA[
                        3-phase reactive power.
                    ]]></doc>
                </property>

                <property
                    symbol         = "RS"
                    title          = "Reactive power of phase RS"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.tcr.rs.value"
                >
                    <doc><![CDATA[
                        TCR reactive power of phase RS.
                    ]]></doc>
                </property>

                <property
                    symbol         = "ST"
                    title          = "Reactive power of phase ST"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.tcr.st.value"
                >
                    <doc><![CDATA[
                        TCR reactive power of phase ST.
                    ]]></doc>
                </property>

                <property
                    symbol         = "TR"
                    title          = "Reactive power of phase TR"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.q.tcr.tr.value"
                >
                    <doc><![CDATA[
                        TCR reactive power of phase TR.
                    ]]></doc>
                </property>
            </property>

        </property>

        <property
            symbol  = "V"
            type    = "PARENT"
            get     = "Parent"
            group   = "OPERATIONAL"
            title   = "Voltages in the busbar"
        >
            <doc><![CDATA[
                This property contains the analogue measurements of the phase-to-phase and phase-to-ground voltages in the busbar, in V.
            ]]></doc>

           <property
                symbol         = "RS"
                title          = "Voltage of phase RS at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.rs.value"
            >
                <doc><![CDATA[
                    Busbar voltage of phase RS.
                ]]></doc>
            </property>

            <property
                symbol         = "ST"
                title          = "Voltage of phase ST at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.st.value"
            >
                <doc><![CDATA[
                        Busbar voltage of phase ST.
                    ]]></doc>
            </property>

            <property
                symbol         = "TR"
                title          = "Voltage of phase TR at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.tr.value"
            >
                <doc><![CDATA[
                        Busbar voltage of phase TR.
                    ]]></doc>
            </property>

            <property
                symbol         = "R"
                title          = "Voltage of phase R at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.r.value"
            >
                <doc><![CDATA[
                        Busbar voltage of phase R.
                    ]]></doc>
            </property>

            <property
                symbol         = "S"
                title          = "Voltage of phase S at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.s.value"
            >
                <doc><![CDATA[
                        Busbar voltage of phase S.
                    ]]></doc>
            </property>

            <property
                symbol         = "T"
                title          = "Voltage of phase T at the busbar"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.v.t.value"
            >
                <doc><![CDATA[
                        Busbar voltage of phase T.
                    ]]></doc>
            </property>

       </property>

       <property
            symbol  = "I"
            type    = "PARENT"
            get     = "Parent"
            group   = "OPERATIONAL"
            title   = "Currents"
        >
            <doc><![CDATA[
                This property contains the properties that contain various current measurements.
            ]]></doc>

            <property
                symbol  = "FEEDER"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Feeder currents"
            >
                <doc><![CDATA[
                    This property contains the line currents of the feeder, in A.
                ]]></doc>

                <property
                    symbol         = "R"
                    title          = "Line current of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.feeder.r.value"
                >
                    <doc><![CDATA[
                        Line current of phase R.
                    ]]></doc>
                </property>

                <property
                    symbol         = "S"
                    title          = "Line current of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.feeder.s.value"
                >
                    <doc><![CDATA[
                        Line current of phase S.
                    ]]></doc>
                </property>

                <property
                    symbol         = "T"
                    title          = "Line current of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.feeder.t.value"
                >
                    <doc><![CDATA[
                        Line current of phase T.
                    ]]></doc>
                </property>

            </property>

            <property
                symbol  = "LOAD"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "Feeder currents"
            >
                <doc><![CDATA[
                    This property contains the line currents of the load, in A.
                ]]></doc>

                <property
                    symbol         = "R"
                    title          = "Line current of phase R"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.load.r.value"
                >
                    <doc><![CDATA[
                        Line current of phase R.
                    ]]></doc>
                </property>

                <property
                    symbol         = "S"
                    title          = "Line current of phase S"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.load.s.value"
                >
                    <doc><![CDATA[
                        Line current of phase S.
                    ]]></doc>
                </property>

                <property
                    symbol         = "T"
                    title          = "Line current of phase T"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.load.t.value"
                >
                    <doc><![CDATA[
                        Line current of phase T.
                    ]]></doc>
                </property>
            </property>

            <property
                symbol  = "TCR"
                type    = "PARENT"
                get     = "Parent"
                group   = "OPERATIONAL"
                title   = "TCR currents"
            >
                <doc><![CDATA[
                    This property contains the RMS TCR branch currents in A.
                ]]></doc>

                <property
                    symbol         = "RS"
                    title          = "TCR current of phase RS"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.tcr.rs.value"
                >
                    <doc><![CDATA[
                        RMS TCR current of phase RS.
                    ]]></doc>
                </property>

                <property
                    symbol         = "ST"
                    title          = "TCR current of phase ST"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.tcr.st.value"
                >
                    <doc><![CDATA[
                        RMS TCR current of phase ST.
                    ]]></doc>
                </property>

                <property
                    symbol         = "TR"
                    title          = "TCR current of phase TR"
                    type           = "FLOAT"
                    get            = "Float"
                    numels         = "1"
                    value_64       = "meas_svc.i.tcr.tr.value"
                >
                    <doc><![CDATA[
                        RMS TCR current of phase TR.
                    ]]></doc>
                </property>

            </property>

        </property>

        <property
            symbol  = "REG"
            type    = "PARENT"
            get     = "Parent"
            group   = "OPERATIONAL"
            title   = "Currents"
        >
            <doc><![CDATA[
                This property contains various signals derived from the regulation algorithms running in VsRegulationDSP.
            ]]></doc>

            <property
                symbol         = "ALPHA"
                title          = "Firing angle of the TCR"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.reg.alpha.value"
            >
                <doc><![CDATA[
                    General firing angle of the TCR, in degrees.
                ]]></doc>
                </property>

            <property
                symbol         = "FREQ"
                title          = "Frequency"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.reg.freq.value"
            >
                <doc><![CDATA[
                    The network frequency in Hz, as estimated by the Synchronization PLL in VsRegulationDSP.
                ]]></doc>
            </property>

            <property
                symbol         = "PLL_OK"
                title          = "PLL ok"
                type           = "INT8U"
                get            = "Integer"
                numels         = "1"
                value_64       = "meas_svc.reg.pll_ok.value"
            >
                <doc><![CDATA[
                    Synchronization status of the Synchronization PLL in VsRegulationDSP.
                ]]></doc>
            </property>

            <property
                symbol         = "PF"
                title          = "Power factor at the feeder"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.reg.pf.value"
            >
                <doc><![CDATA[
                    Power factor at the feeder (per unit).
                ]]></doc>
            </property>

            <property
                symbol         = "PIFLAG_V"
                title          = "Integrator flag for voltage regulator"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.reg.piflag_v.value"
            >
                <doc><![CDATA[
                    Integrator flag for voltage regulator (0: enable, 1: freeze, 2:reset).
                ]]></doc>
            </property>

            <property
                symbol         = "PIFLAG_Q"
                title          = "Integrator flag for reactive power regulator"
                type           = "FLOAT"
                get            = "Float"
                numels         = "1"
                value_64       = "meas_svc.reg.piflag_q.value"
            >
                <doc><![CDATA[
                    Integrator flag for reactive power regulator (0: enable, 1: freeze, 2:reset).
                ]]></doc>
            </property>
        </property>

    </property>

<!-- EOF -->
