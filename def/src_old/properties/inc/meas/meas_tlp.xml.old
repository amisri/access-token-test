    <!-- MEAS top level properties ====================================================================== -->

    <property
        symbol    = "DSP_GROUP"
        title     = "VS_REG DSP acquisition gruop"
        type      = "INT32U"
        get       = "Integer"
        numels    = "1"
        value_64  = "(void*)&amp;dpcls.mcu.meas.acq_group"
    >
        <doc><![CDATA[
            <p>This property controls which signals are retrieved from the VS_REG DSP card via the
            SPIVS bus. Only 7 signals can be retreived at the same time.</p>
            <table class="embedded">
            <tr>
            <th class="alt">Group</th>
            <th class="alt">Signal 1</th>
            <th class="alt">Signal 2</th>
            <th class="alt">Signal 3</th>
            <th class="alt">Signal 4</th>
            <th class="alt">Signal 5</th>
            <th class="alt">Signal 6</th>
            <th class="alt">Signal 7</th>
            </tr>
            <tr><td><center>0</center></td> <td>VBUSBAR_RS</td> <td>ITCR_RS</td>    <td>IFEEDER_R</td>  <td>PFEEDER</td>   <td>QFEEDER</td>   <td>ALPHA</td>     <td>QTCR</td>    </tr>
            <tr><td><center>1</center></td> <td>VBUSBAR_R</td>  <td>VBUSBAR_S</td>  <td>VBUSBAR_T</td>  <td>IFEEDER_R</td> <td>IFEEDER_S</td> <td>IFEEDER_T</td> <td>QFEEDER</td> </tr>
            <tr><td><center>2</center></td> <td>VBUSBAR_RS</td> <td>VBUSBAR_ST</td> <td>VBUSBAR_TR</td> <td>ITCR_RS</td>   <td>ITCR_ST</td>   <td>ITCR_TR</td>   <td>QTCR</td>    </tr>
            <tr><td><center>3</center></td> <td>VBUSBAR_RS</td> <td>VLL_THETA</td>  <td>VLL_FREQ</td>   <td>PLL_OK</td>    <td>VD_TCR</td>    <td>V_CONTROL</td> <td>VQ_TCR</td>  </tr>
            <tr><td><center>4</center></td> <td>ALPHA</td>      <td>PFEEDER</td>    <td>QFEEDER</td>    <td>PLOAD</td>     <td>QLOAD</td>     <td>QTCR</td>      <td>PF</td>      </tr>
            <tr><td><center>5</center></td> <td>IRES_R</td>     <td>IRES_S</td>     <td>IRES_T</td>     <td></td>          <td></td>          <td></td>          <td></td>        </tr>
            <tr><td><center>6</center></td> <td>B_TCR</td>      <td>ALPHA</td>      <td>PI_flag</td>    <td></td>          <td></td>          <td></td>          <td></td>        </tr>
            <tr><td><center>7</center></td> <td>VBUSBAR_R</td>  <td>ILOAD_R</td>    <td>ILOAD_S</td>    <td>ILOAD_T</td>   <td>PLOAD</td>     <td>QLOAD</td>     <td>PF</td>      </tr>
            <tr><td><center>8</center></td> <td>V_CONTROL</td>  <td>V_REF_FGC</td>  <td>V_REF_INT</td>  <td>Q_FEEDER</td>  <td>Q_REF_FGC</td> <td>Q_REF_INT</td> <td>ALPHA</td>   </tr>
            </table>
        ]]></doc>
    </property>

    <property
        symbol          = "SIM"
        title           = "Simulate measurements"
        type            = "INT32U"
        flags           = "NON_VOLATILE"
        dsp_fgc2        = "fgc"
        dsp_fgc3        = "fgc"
        pars_fgc2       = "MeasSim"
        pars_62         = "MeasSim"
        pars_fgcdeqp    = "MeasSim"
        get             = "Integer"
        set             = "Integer"
        setif           = "PcOff"
        numels          = "1"
        symlist         = "CTRL"
        value_fgc2      = "property.meas.sim"
        value_fgc3      = "property.meas.sim"
        value_fgcdeqp   = "equipdev.device[0].meas.sim"
    >
        <doc><![CDATA[
            This property controls the simulation of the circuit measurements.
            This is only significant when {PROP STATE.OP} is SIMULATION, and if ENABLED, the software
            will use a model of the voltage source, circuit and DCCTs to simulate the DCCT voltage
            signals.
        ]]></doc>
    </property>

    <property
        symbol          = "REF"
        title           = "Measured reference output"
        type            = "FLOAT"
        get             = "Float"
        numels          = "1"
        value_fgcdeqp   = "equipdev.device[0].meas.ref_meas"
        value_93        = "equipdev.device[0].status.v_ref"
    >
        <doc><![CDATA[
            This property contains the measured analogue output of the function generator's DAC.

            In Class 93 (ISEG), this property contains the voltage reference sent to the HV channel.
        ]]></doc>
    </property>

    <property
        symbol         = "U_LEADS"
        title          = "60/120A current lead voltages"
        type           = "FLOAT"
        get            = "Float"
        numels         = "2"
        value_fgc2     = "dpcls.dsp.meas.ileads.u_leads"
        value_fgc3     = "dpcls.dsp.meas.ileads.u_leads"
        value_92       = "equipdev.device[0].meas.u_leads[0]"
    >
        <doc><![CDATA[
            For the 60A and 120A circuits, this property contains the measurement
            of the current lead voltages.  These are sampled using the DIM in the
            voltage source at 50Hz (12-bit uncalibrated ADC accurate to 8-bit).
            A 1 second average of the 50Hz data is calculated at 1Hz and made available
            in this property, and via the ILEADS post mortem log.  The property contains
            two values:
            <ul>
            <li> [0] - U_LEAD_POS</li>
            <li> [1] - U_LEAD_NEG</li>
            </ul>
            If the measurement exceeds 150mV, the converter will trip off.
        ]]></doc>
    </property>

<!-- EOF -->
