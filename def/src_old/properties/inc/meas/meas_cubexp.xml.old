<!-- MEAS.CUBEXP -->

<property
   symbol      = "CUBEXP"
   type        = "PARENT"
   operational = "1"
   get         = "Parent"
   title       = "CUBEXP acquisition properties"
>
    <doc><![CDATA[
        This property contains the properties related to recording the measurement of the regulated signal
        at defined moments during cycle when using the CUBEXP reference function in CYCLING state.
    ]]></doc>

    <property
       symbol      = "VALUE"
       title       = "CUBEXP acquisition value"
       kt_units    = "A|V|G"
       flags       = "PPM"
       kt          = "1"
       dsp_fgc3    = "fgc"
       type        = "FLOAT"
       get         = "Float"
       numels      = "1"
       value_63    = "property.ppm[0].meas.acq_value[0]"
    >
       <doc><![CDATA[
             This property holds the acquisition value from the time triggered by 
             {PROP MEAS.TRIG.DELAY_MS}.
         ]]></doc>
    </property>

    <property
        symbol        = "VALUE_UNITS"
        title         = "Cubexp acquisition units"
        dsp_63        = "fgc"
        type          = "INT32U"
        get           = "Integer"
        numels        = "1"
        symlist       = "REG_MODE_UNITS"
        value_63      = "refMgrParGetValue(&amp;ref_mgr, MODE_REG_MODE_CYC)"
    >
        <doc><![CDATA[
            This property contains the units. They are taken from the
            default regulation mode specified in {PROP REG.MODE_CYC}.
        ]]></doc>
    </property>
</property>

<property
   symbol      = "CUBEXP2"
   type        = "PARENT"
   operational = "1"
   get         = "Parent"
   title       = "CUBEXP function second acquisition properties"
>
    <doc><![CDATA[
        This property contains the properties related to the second recording of the measurement
        at defined moments during cycle when using the CUBEXP reference function in CYCLING state.
    ]]></doc>

    <property
       symbol      = "VALUE"
       title       = "Second cubexp acquisition value"
       kt_units    = "A|V|G"
       flags       = "PPM"
       kt          = "1"
       dsp_fgc3    = "fgc"
       type        = "FLOAT"
       get         = "Float"
       numels      = "1"
       value_63    = "property.ppm[0].meas.acq_value[1]"
    >
       <doc><![CDATA[
             This property holds the second acquisition value associated to {PROP REF.CUBEXP}.
         ]]></doc>
    </property>

    <property
        symbol        = "VALUE_UNITS"
        title         = "CUBEXP reference units"
        dsp_63        = "fgc"
        type          = "INT32U"
        get           = "Integer"
        numels        = "1"
        symlist       = "REG_MODE_UNITS"
        value_63      = "refMgrParGetValue(&amp;ref_mgr, MODE_REG_MODE_CYC)"
    >
        <doc><![CDATA[
            This property contains the units. They are taken from the
            default regulation mode specified in {PROP REG.MODE_CYC}.
        ]]></doc>
    </property>
</property>

<!-- EOF -->
