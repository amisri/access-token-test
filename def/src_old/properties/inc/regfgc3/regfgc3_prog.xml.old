<!-- REGFGC3.PROG -->

	<property
        symbol      = "PROG"
        title       = "RegFGC3 programming properties"
        type        = "PARENT"
        flags       = "HIDE"
        get         = "Parent"
        set         = "Reset"
    >
        <doc><![CDATA[
               Group of properties related to the reprogramming of a RegFGC3 card.
        ]]></doc>

        <property
            symbol          = "LOCK"
            title           = "FGC3 lock for the program manager"
            flags           = "NON_VOLATILE"
            type            = "INT8U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            symlist         = "REGFGC3_PROG_LOCK"
            value_fgc3      = "regfgc3_prog_prop.prog_data.lock"
        >
            <doc><![CDATA[
                This property implements a sort of lock for the program manager to act on the FGC3. Its values can be ENABLED/DISABLED.
                When the program manager service determines that certain HW need to be reprogrammed, it will try to
                initiate the required actions by setting the property REGFGC3.PROG.FSM.MODE to the different modes of the reprogramming FSM. This
                will not take place if the lock is on (property value = DISABLED).
            ]]></doc>
        </property>

        <property
            symbol          = "MANAGER"
            title           = "Program manager switch"
            type            = "INT8U"
            flags           = "NON_VOLATILE"
            get             = "Integer"
            set             = "Integer"
            maxels          = "1"
            symlist         = "CTRL"
            value_fgc3      = "regfgc3_prog_prop.prog_data.manager"
        >
            <doc><![CDATA[
                By means of this property the program manager can act upon one FGC3.
                The FGC3 will raise the SYNC_FW flag when this property has the value "ON", thus signalling the program
                manager that kit has to check whether the FGC3 FW needs to be synchronized.
            ]]></doc>
        </property>

        <property
            symbol          = "SLOT"
            title           = "Slot the programming action is taken place"
            type            = "INT8U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            min             = "1"
            max             = "FGC_SCIVS_N_SLOT"
            value_fgc3      = "regfgc3_prog_prop.prog_data.slot"
        >
            <doc><![CDATA[
                Displays the number of slot in the crate where the current programming action is taking place.
            ]]></doc>
        </property>

        <property
            symbol          = "BOARD"
            title           = "Board the programming action is taken place at"
            type            = "INT8U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            min             = "1"
            symlist         = "REGFGC3_BOARDS"
            value_fgc3      = "regfgc3_prog_prop.prog_data.board"
        >
            <doc><![CDATA[
                Displays the board name that is being reprogrammed.
            ]]></doc>
        </property>

        <property
            symbol          = "DEVICE"
            title           = "Card device targeted for reprogramming"
            type            = "INT8U"
            flags           = "SYM_LST"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            symlist         = "REGFGC3_DEVICES"
            value_fgc3      = "regfgc3_prog_prop.prog_data.device"
        >
            <doc><![CDATA[
                Displays the reprogrammable device the current programming action is taking place on.
            ]]></doc>
        </property>

        <property
            symbol          = "VARIANT"
            title           = "RegFGC3 card variant"
            type            = "INT16U"
            get             = "Integer"
            set             = "Integer"
            min             = "1"
            max             = "1023"
            numels          = "1"
            symlist         = "REGFGC3_VARIANTS"
            value_fgc3      = "regfgc3_prog_prop.prog_data.variant"
        >
            <doc><![CDATA[
                This is the variant of the FW file sent to a given device in a RegFGC3 board.
            ]]></doc>

        </property>

        <property
            symbol          = "API_REVISION"
            title           = "API revision"
            type            = "INT16U"
            get             = "Integer"
            set             = "Integer"
            min             = "1"
            max             = "1023"
            numels          = "1"
            value_fgc3      = "regfgc3_prog_prop.prog_data.api_revision"
        >
            <doc><![CDATA[
                API version of the RegFGC3 firmware sent to a given device in a RegFGC3 board.
            ]]></doc>

        </property>

        <property
            symbol          = "VARIANT_REVISION"
            title           = "Variant revision"
            type            = "INT16U"
            get             = "Integer"
            set             = "Integer"
            min             = "1"
            max             = "1023"
            numels          = "1"
            value_fgc3      = "regfgc3_prog_prop.prog_data.variant_revision"
        >
            <doc><![CDATA[
                Variant version of the RegFGC3 firmware sent to a given device in a RegFGC3 board.
            ]]></doc>

        </property>

        <property
            symbol          = "BIN_SIZE_BYTES"
            title           = "Binary size"
            type            = "INT32U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_fgc3      = "regfgc3_prog_prop.prog_data.bin_size_bytes"
        >
            <doc><![CDATA[
                Size in bytes of the binary file sent to a given device in a RegFGC3 board.
            ]]></doc>

        </property>

        <property
            symbol          = "BIN_CRC"
            title           = "Binary CRC"
            type            = "INT16U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_fgc3      = "regfgc3_prog_prop.prog_data.bin_crc"
        >
            <doc><![CDATA[
                The CRC of the FW file, including its header.
            ]]></doc>

        </property>

        <property
            symbol          = "BIN"
            title           = "Binary image"
            type            = "INT32U"
            dsp_fgc3        = "fgc"
            flags           = "HIDE"
            get             = "Integer"
            setif           = "PcOff"
            set             = "Integer"
            maxels          = "FGC_REGFGC3_MAX_BIN_SIZE_WORDS"
            value_fgc3      = "(void *) (SDRAM_REGFGC3_BIN_32)"
        >
            <doc><![CDATA[
                Points to the DSP memory address where the RegFGC3 binary is stored before transferring it to a RegFGC3 board.
            ]]></doc>
        </property>

        <property
            symbol          = "MODE"
            title           = "Reprogramming mode"
            type            = "INT8U"
            flags           = "SYM_LST"
            get             = "Integer"
            set             = "RegFgc3ProgMode"
            numels          = "1"
            symlist         = "REGFGC3_PROG_MODE"
            value_fgc3      = "regfgc3_prog_prop.prog_data.mode"
        >
            <doc><![CDATA[
                Overall RegFGC3 reprogramming state: PROG_NONE, PROG_WARNING and PROG_FAILED.
            ]]></doc>

        </property>

        <property
            symbol          = "STATE"
            title           = "Overal reprogramming state"
            type            = "INT8U"
            flags           = "SYM_LST"
            get             = "Integer"
            numels          = "1"
            symlist         = "REGFGC3_PROG_STATE"
            value_fgc3      = "regfgc3_prog_prop.prog_data.state"
        >
            <doc><![CDATA[
                Overall RegFGC3 reprogramming state: STANDALONE, SYNCHRONIZED, UNSYNCED.
            ]]></doc>

        </property>

        <property
            symbol      = "FSM"
            title       = "RegFGC3 reprogramming Finite State Machine"
            type        = "PARENT"
            get         = "Parent"
        >
            <doc><![CDATA[
                This property contains two children properties to interact with the reprogramming finite state machine,
                REGFGC3.PROG.FSM.STATE, and REGFGC3.PROG.FSM.MODE. The RegFGC3 Program Manager will set the mode to different allowed values,
                and will poll the FGC3 for the FSM STATE, to check whether the required action was successful or not.
            ]]></doc>

            <property
                symbol     = "STATE"
                title      = "Reprogramming FSM state"
                type       = "INT8U"
                flags      = "SYM_LST"
                get        = "Integer"
                numels     = "1"
                symlist    = "REGFGC3_PROG_FSM"
                value_fgc3 = "regfgc3_prog_fsm_prop.state"
            >
            <doc><![CDATA[
                <p>Reprogramming FSM state. The FSM is illustrated in the following figure:</p>
                <p align="center"><img border="0" src="../../images/StateMachines/regfgc3_prog.png"></p>
                <p>When the Controller starts up, it begins in {NONE} state, where it will stay until the RegFGC3
                program manager sets the MODE to INIT. In the INIT state, the DSP external memory will be cleaned up from
                previous binary files temporarily stored there. At this point, the RegPM will start sending the binary file (state
                TIP. After that, the RegPM will instruct the FGC3 to check the file integrity (state TRF_CHECK).
                Reprogramming can then start (state PROGRAMMING). Finally, the FSM will return to its initial state (state NONE).
                If errors occur during transmission or programming, the FSM will stay in (state ERROR)</p>
            ]]></doc>
            </property>

            <property
                symbol     = "MODE"
                title      = "Reprogramming FSM mode"
                type       = "INT8U"
                flags      = "SYM_LST"
                get        = "Integer"
                setif      = "PcOff"
                set        = "RegFgc3ProgFsmMode"
                numels     = "1"
                symlist    = "REGFGC3_PROG_FSM"
                value_fgc3 = "regfgc3_prog_fsm_prop.mode"
            >
            <doc><![CDATA[
                This property will be used by the program manager to exercise the reprogramming state machine.
                After setting the FSM mode, the RegPM will poll the FGC3 using the REGFGC3.PROG.FSM.STATE property, and decide
                then to which state to transition next.
            ]]></doc>
            </property>

            <property
                symbol     = "LAST_STATE"
                title      = "Last state before error"
                type       = "INT8U"
                flags      = "SYM_LST"
                get        = "Integer"
                numels     = "1"
                symlist    = "REGFGC3_PROG_FSM"
                value_fgc3 = "regfgc3_prog_fsm_prop.last_state"
            >
            <doc><![CDATA[
                This property will be used by the program manager to exercise the reprogramming state machine.
                After setting the FSM mode, the RegPM will poll the FGC3 using the REGFGC3.PROG.FSM.STATE property, and decide
                then to which state to transition next.
            ]]></doc>

            </property>
        </property>

        <!-- REGFGC3.STATS -->

        <property
            symbol      = "STATS"
            title       = "REGFGC3 programming statistics"
            type        = "PARENT"
            get         = "Parent"
            flags       = "HIDE"
        >
            <doc><![CDATA[
                Contains statistics on traffic over the SCIVS bus with regards to reprogramming.
            ]]></doc>

            <property
                symbol      = "SLICES_SENT"
                title       = "Slices sent to given slot"
                type        = "INT16U"
                flags       = "GET_ZERO"
                get         = "Integer"
                numels      = "FGC_SCIVS_N_SLOT"
                value_fgc3  = "regfgc3_prog_prop.stats.slices_sent"
            >
                <doc><![CDATA[
                    Count of slices sent to a given slot.
                ]]></doc>
            </property>

            <property
                symbol      = "SLICES_PROG"
                title       = "Slices programmed to given slot"
                type        = "INT16U"
                flags       = "GET_ZERO"
                get         = "Integer"
                numels      = "FGC_SCIVS_N_SLOT"
                value_fgc3  = "regfgc3_prog_prop.stats.slices_programmed"
            >
                <doc><![CDATA[
                    Count of slices programmed to a given slot.
                ]]></doc>
            </property>

            <property
                symbol      = "BATCHES_SENT"
                title       = "Batches sent to given slot for the current slice"
                type        = "INT16U"
                flags       = "GET_ZERO"
                get         = "Integer"
                numels      = "FGC_SCIVS_N_SLOT"
                value_fgc3  = "regfgc3_prog_prop.stats.batches_sent"
            >
                <doc><![CDATA[
                    Count of batches sent to a given slot for the current slice.
                ]]></doc>
            </property>

            <property
                symbol      = "ERRORS"
                title       = "Count of errors at given slot"
                type        = "INT16U"
                flags       = "GET_ZERO"
                get         = "Integer"
                numels      = "FGC_SCIVS_N_SLOT"
                value_fgc3  = "regfgc3_prog_prop.stats.errors"
            >
                <doc><![CDATA[
                    Count of all bad frames. See runlog for further details about the errors.
                ]]></doc>
            </property>

        </property>

        <!-- Temporary property to commision program manager -->
        <property
            symbol      = "DEBUG"
            title       = "RegFGC3 debug programming properties"
            type        = "PARENT"
            flags       = "HIDE"
            get         = "Parent"
        >
        <doc><![CDATA[
            This set of properties have been added to expose additional information on the reprogramming process
            that will be needed mainly during commissioning.
        ]]></doc>

            <property
                symbol          = "ACTION"
                title           = "Issues actions on the cards"
                type            = "INT16U"
                flags           = "SYM_LST"
                get             = "Integer"
                setif           = "PcOff"
                set             = "RegFgc3ProgAction"
                numels          = "1"
                symlist         = "REGFGC3_PROG_ACTION"
                value_fgc3      = "regfgc3_prog_prop.debug.action"
            >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "BOARD_ERROR"
                title           = "Error comming from the regfgc3 board"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.board_error"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "SLICE_BYTES"
                title           = "slice bytes"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.slice_bytes"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "SLICE_BATCHES"
                title           = "slice batches"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.slice_batches"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "TIME_PROG"
                title           = "time program"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.time_prog"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "TIME_CRC"
                title           = "time crc"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.time_crc"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>

            <property
                symbol          = "TIME_PARAMS"
                title           = "time params"
                type            = "INT16U"
                get             = "Integer"
                numels          = "1"
                value_fgc3      = "regfgc3_prog_prop.debug.time_params"
                >
                <doc><![CDATA[
                    Added temporarily for the RegPM commissioning
                ]]></doc>

            </property>
        </property>

    </property>