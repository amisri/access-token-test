
    <!-- VS.FILTER ======================================================================================= -->

    <property
        symbol  = "FILTER"
        type    = "PARENT"
        get     = "Parent"
        title   = "Converter output filter parameters"
    >
        <doc><![CDATA[
            <p>This property contains the properties the define the output filter model
            of the converter. These are only used if {PROP VS.ACTUATION} is set to FIRING_REF.
            The model is shown in the following figure:</p>
            <p align="center"><img border="0" width="600" src="../../images/pc_filter_model.png"></p>
            <p>The filter damping uses the partial I_CAPA, which can be estimated from the full I_CAPA signal
            using a first order filter, as indicated in this
            <a target="_blank" href="../../images/cclibs/firing_regulation.pdf">figure</a>.
            This can be enabled using {PROP REG.V.FILTER.FULL_I_CAPA}.</p>
        ]]></doc>

        <property
            symbol                = "HENRYS"
            title                 = "Converter output filter inductance"
            type                  = "FLOAT"
            group                 = "CONFIG"
            flags                 = "NON_VOLATILE CONFIG"
            dsp_fgc3              = "fgc"
            pars                  = "RegPars"
            get                   = "Float"
            set                   = "Float"
            setif                 = "RegStateNone"
            maxels                = "1"
            limits                = "float"
            min                   = "0.0"
            max                   = "10.0"
            from_spare_converter  = "1"

            value_63              = "regMgrParAppValue(&amp;reg_pars,PC_HENRYS)"
        >
            <doc><![CDATA[
                This property defines the inductance of the output filter. This is
                used if {PROP VS.ACTUATION} is set to FIRING_REF.
            ]]></doc>
        </property>

        <property
            symbol                = "OHMS"
            title                 = "Converter output filter resistance"
            type                  = "FLOAT"
            group                 = "CONFIG"
            flags                 = "NON_VOLATILE CONFIG"
            dsp_fgc3              = "fgc"
            pars                  = "RegPars"
            get                   = "Float"
            set                   = "Float"
            setif                 = "RegStateNone"
            maxels                = "1"
            limits                = "float"
            min                   = "0.0"
            max                   = "10.0"
            from_spare_converter  = "1"

            value_63              = "regMgrParAppValue(&amp;reg_pars,PC_OHMS)"
        >
            <doc><![CDATA[
                This property defines the resistance of the output filter. This is on the branch
                with the capacitor specified in {PROP VS.FILTER.FARADS1}. This is
                used if {PROP VS.ACTUATION} is set to FIRING_REF.
            ]]></doc>
        </property>

        <property
            symbol                = "FARADS1"
            title                 = "Converter output filter damped capacitance"
            type                  = "FLOAT"
            group                 = "CONFIG"
            flags                 = "NON_VOLATILE CONFIG"
            dsp_fgc3              = "fgc"
            pars                  = "RegPars"
            get                   = "Float"
            set                   = "Float"
            setif                 = "RegStateNone"
            maxels                = "1"
            limits                = "float"
            min                   = "0.0"
            max                   = "10.0"
            from_spare_converter  = "1"

            value_63              = "regMgrParAppValue(&amp;reg_pars,PC_FARADS1)"
        >
            <doc><![CDATA[
                This property defines the damped capacitance of the output filter.
                This is for the branch in series with the resistance specified in {PROP VS.FILTER.OHMS}.
                This is used if {PROP VS.ACTUATION} is set to FIRING_REF.
            ]]></doc>
        </property>

        <property
            symbol                = "FARADS2"
            title                 = "Converter output filter undamped capacitance"
            type                  = "FLOAT"
            group                 = "CONFIG"
            flags                 = "NON_VOLATILE CONFIG"
            dsp_fgc3              = "fgc"
            pars                  = "RegPars"
            get                   = "Float"
            set                   = "Float"
            setif                 = "RegStateNone"
            maxels                = "1"
            limits                = "float"
            min                   = "0.0"
            max                   = "10.0"
            from_spare_converter  = "1"

            value_63              = "regMgrParAppValue(&amp;reg_pars,PC_FARADS2)"
        >
            <doc><![CDATA[
                This property defines the undamped capacitance of the output filter.
                This is used if {PROP VS.ACTUATION} is set to FIRING_REF.
            ]]></doc>
        </property>

    </property>

<!-- EOF -->
