
    <!-- ======================== LOG.CAPTURE ======================== -->

    <property
        symbol  = "CAPTURE"
        type    = "PARENT"
        get     = "Parent"
        title   = "Capture log properties"
    >
        <doc><![CDATA[
            <p>
            This property contains the capture log properties.  The PPM user index
            is specified in {PROP LOG.CAPTURE.USER} in order to arm the system to capture data
            the next time that user index is played.
            </p>
            <p>
            For Class 53 the buffer contains nine signals recorded at 1kHz:
            <ul>
            <li><A href="{PROP MEAS.B.REF}">B_REF</A> (G) or <A href="{PROP MEAS.I.REF}">I_REF</A> (A) according to the regulation mode</li>
            <li><A href="{PROP MEAS.V.REF}">V_REF</A> (V)</li>
            <li><A href="{PROP MEAS.B}">B_MEAS</A> (G)</li>
            <li><A href="{PROP MEAS.I}">I_MEAS</A> (A)</li>
            <li><A href="{PROP MEAS.V}">V_MEAS</A> (V)</li>
            <li>B_ERR (G)or I_ERR (A) according to the regulation mode</li>
            <li>B_SIM (G)</li>
            <li>I_SIM (A)</li>
            <li>V_SIM (V)</li>
            </ul>
            </p>
        ]]></doc>

        <property
            symbol      = "STATE"
            title       = "Capture log state"
            operational = "1"
            group       = "LOG"
            type        = "INT32U"
            get         = "Integer"
            numels      = "1"
            symlist     = "LOG"
            value_fgc2  = "dpcls.dsp.log.capture.state"
            value_fgc3  = "dpcls.dsp.log.capture.state"
        >
            <doc><![CDATA[
                This property reports the capture log state.
            ]]></doc>
        </property>

        <property
            symbol      = "USER"
            title       = "Capture log acquisition trigger"
            operational = "1"
            group       = "LOG"
            type        = "INT32S"
            get         = "Integer"
            set         = "LogCaptureUser"
            limits      = "int"
            min         = "-1"
            max         = "FGC_MAX_USER"
            numels      = "1"
            value_fgc2  = "dpcls.dsp.log.capture.user"
            value_fgc3  = "dpcls.dsp.log.capture.user"
        >
            <doc><![CDATA[
                <p>This property must be set to arm the capture log. Once the log is frozen, this property
                reflects the cycle user which was captured. The property LOG.CAPTURE.USER can be set to:
                <ul>
                <li>-1 to unfreeze the capture log (provided this is not a cycle check warning capture log);</li>
                <li>0 to capture the log on the next cycle, regardless of the user for that cycle;</li>
                <li>n > 0 to capture the log for the next cycle of user n.</li>
                </ul>
                </p>
                <p>The setting of this property can only be done when {PROP LOG.CAPTURE.CYC_CHK_TIME} is
                zero, meaning no check warning has triggered a log freeze. This specific case can only be
                reset by reading the capture log using Spy with no user specified (<i>PPM mux</i> must be
                set to <i>None</i>).</p>
                <p>Another constraint is that LOG.CAPTURE.USER can be modified from -1 to a positive value,
                or from a positive value to -1, but all other transitions are forbidden. In pratice if one
                wants to arm the capture log for a user, while it is already set for another,
                then the sequence to switch users requires two steps, as demonstrated in the example below:
                <PRE>
                G LOG.CAPTURE.USER
                &nbsp;&nbsp;&nbsp;&nbsp;USER:   6
                S LOG.CAPTURE.USER  -1
                S LOG.CAPTURE.USER  18
                G LOG.CAPTURE.USER
                &nbsp;&nbsp;&nbsp;&nbsp;USER:  18
                </PRE>
                </p>
            ]]></doc>
        </property>

        <property
            symbol      = "CYC_CHK_TIME"
            title       = "Cycle check capture log time"
            type        = "ABSTIME"
            get         = "AbsTime"
            numels      = "1"
            value_fgc2  = "dpcls.dsp.log.capture.cyc_chk_time"
            value_fgc3  = "dpcls.dsp.log.capture.cyc_chk_time"
        >
            <doc><![CDATA[
                This property contains the time of the first sample of the capture log after it has
                been triggered by a cycle check.  For as long as this property is non-zero attempts to arm
                the capture log for a particular user with {PROP LOG.CAPTURE.USER} will be rejected.
                This property is reset automatically when the capture log is read out using
                Spy without a user being specified (set <i>PPM mux</i> to <i>None</i>).
            ]]></doc>
        </property>

        <property
            symbol      = "BUF"
            title       = "PPM data buffer"
            flags       = "HIDE"
            dsp_fgc2    = "fgc"
            dsp_fgc3    = "fgc"
            type        = "FLOAT"
            get         = "Float"
            numels      = "FGC_LOG_CAPTURE_BUF_LEN"
            value_fgc2  = "(void *)LOG_CAPTURE_BASEADDR"
            value_fgc3  = "log_capture_buffer"
        >
            <doc><![CDATA[
                This property is used internally by the FGC software to readout the PPM log data.
                An application can read this but the data will be in floats and will contain all
                the signals mixed together.
            ]]></doc>
        </property>

	<!-- ======================== LOG.CAPTURE.SIGNALS ======================== -->

        <property
            symbol  = "SIGNALS"
            type    = "PARENT"
            get     = "Parent"
            title   = "Capture log signal properties"
        >
            <doc><![CDATA[
                This property gives access to individual capture log signals.  The
                application must write the user number of the cycle they want to acquire
                to {PROP LOG.CAPTURE.USER} and wait for the readback to become -1 before
		trying to read the signal.
            ]]></doc>
                                    <!-- numels retrieves the low word returned from the DSP (+2) -->
            <property
                symbol      = "REF"
                title       = "Field or current reference log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_REF"
                value_fgc3  = "FGC_LOG_CAPTURE_REF"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
                    <A href="{PROP MEAS.B.REF}">B_REF</A> or <A href="{PROP MEAS.I.REF}">I_REF</A> signal.
                ]]></doc>
            </property>

            <property
                symbol      = "V_REF"
                title       = "Converter voltage reference log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_V_REF"
                value_fgc3  = "FGC_LOG_CAPTURE_V_REF"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
                    <A href="{PROP MEAS.V.REF}">V_REF</A> signal.
                ]]></doc>
            </property>

            <property
                symbol      = "B_MEAS"
                title       = "Field measurement log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_B_MEAS"
                value_fgc3  = "FGC_LOG_CAPTURE_B_MEAS"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
                    <A href="{PROP MEAS.B}">B_MEAS</A> signal.
                ]]></doc>
            </property>

            <property
                symbol      = "I_MEAS"
                title       = "Current measurement log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_I_MEAS"
                value_fgc3  = "FGC_LOG_CAPTURE_I_MEAS"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
                    <A href="{PROP MEAS.I}">I_MEAS</A> signal.
                ]]></doc>
            </property>

            <property
                symbol      = "V_MEAS"
                title       = "Converter voltage measurement log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_V_MEAS"
                value_fgc3  = "FGC_LOG_CAPTURE_V_MEAS"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
                    <A href="{PROP MEAS.V}">V_MEAS</A> signal.
                ]]></doc>
            </property>

            <property
                symbol      = "ERR"
                title       = "Field or current regulation error log buffer"
                operational = "1"
                flags       = "HIDE INDIRECT_N_ELS"
                type        = "FLOAT"
                get         = "LogCaptureSig"
                numels      = "(uintptr_t) &amp; dpcls.dsp.log.capture.n_samples + 2"
                maxels      = "FGC_LOG_CAPTURE_LEN"
                value_fgc2  = "FGC_LOG_CAPTURE_ERR"
                value_fgc3  = "FGC_LOG_CAPTURE_ERR"
            >
                <doc><![CDATA[
                    This property provides access to the capture log buffer for the
		    B_ERR or I_ERR signal.
                ]]></doc>
            </property>
        </property>

	<!-- ======================== LOG.CAPTURE.SPY ======================== -->

        <property
            symbol  = "SPY"
            type    = "PARENT"
            get     = "Parent"
            flags   = "HIDE"
            title   = "PPM log properties"
        >
            <doc><![CDATA[
                <p>This property contains the Capture log spy properties.  These give access to the
                capture logs to the Spy interface.  The buffers are prefixed with a header that
                tells Spy the names of the signals and various other pieces of information.</p>
            ]]></doc>

            <property
                symbol          = "MPX"
                title           = "Spy signal selector for capture log"
                type            = "INT32U"
                flags           = "DONT_SHRINK"
                get             = "Integer"
                set             = "Integer"
                numels          = "FGC_LOG_CAPTURE_MPX_LEN"
                class_symlist   = "SPY"
                value_fgc2      = "dpcls.mcu.log_capture_mpx"
                value_fgc3      = "dpcls.mcu.log_capture_mpx"
            >
                <doc><![CDATA[
                    When the FGC makes an acquisition, three of the signals are selectable using
                    this property.  They can be read out using Spy by called the "ALL" buffer.
                    By default the selection of signals for class 53 is:
                    <ul>
                    <li><b>BSIM</b> - the simulated field (G)</li>
                    <li><b>ISIM</b> - the simulated current (A)</li>
                    <li><b>VSIM</b> - the simulated converter voltage (V)</li>
                    </ul>
                    </p>
                ]]></doc>
            </property>

            <property
                symbol  = "ALL"
                title   = "Spy buffer with all logged signals (real and simulated)"
                flags   = "HIDE"
                type    = "LOG"
                get     = "LogCaptureSpy"
                numels  = "FGC_LOG_CAPTURE_LEN"
            >
                <doc><![CDATA[
                    This property provides access to Spy to all the PPM log signals.
                    To take data the property {PROP LOG.CAPTURE.USER} must be written
                    first to specify the USER that is to be captured.
                    <p>
                    For Class 53 the bufer contains nine signals recorded at 1kHz:
                    <ul>
                    <li><A href="{PROP MEAS.B.REF}">B_REF</A> (G) or <A href="{PROP MEAS.I.REF}">I_REF</A> (A) according to the regulation mode</li>
                    <li><A href="{PROP MEAS.V.REF}">V_REF</A> (V)</li>
                    <li><A href="{PROP MEAS.B}">B_MEAS</A> (G)</li>
                    <li><A href="{PROP MEAS.I}">I_MEAS</A> (A)</li>
                    <li><A href="{PROP MEAS.V}">V_MEAS</A> (V)</li>
                    <li>B_ERR (G)or I_ERR (A) according to the regulation mode</li>
                    <li>Signal from {PROP LOG.CAPTURE.SPY.MPX}[0]</li>
                    <li>Signal from {PROP LOG.CAPTURE.SPY.MPX}[1]</li>
                    <li>Signal from {PROP LOG.CAPTURE.SPY.MPX}[2]</li>
                    </ul>
                    If the converter isn't running then the B/I and V reference and B/I error signals will
                    not be returned.
                    </p>
                ]]></doc>
            </property>

            <property
                symbol  = "OP"
                title   = "Spy buffer for all real operational signals"
                flags   = "HIDE"
                type    = "LOG"
                get     = "LogCaptureSpy"
                numels  = "FGC_LOG_CAPTURE_LEN"
            >
                <doc><![CDATA[
                    This property provides access to Spy to the 6 real operational signals.
                    These are:
                    <ul>
                    <li><A href="{PROP MEAS.B.REF}">B_REF</A> (G) or <A href="{PROP MEAS.I.REF}">I_REF</A> (A) according to the regulation mode</li>
                    <li><A href="{PROP MEAS.V.REF}">V_REF</A> (V)</li>
                    <li><A href="{PROP MEAS.B}">B_MEAS</A> (G)</li>
                    <li><A href="{PROP MEAS.I}">I_MEAS</A> (A)</li>
                    <li><A href="{PROP MEAS.V}">V_MEAS</A> (V)</li>
                    <li>B_ERR (G)or I_ERR (A) according to the regulation mode</li>
                    </ul>
                ]]></doc>
            </property>

            <property
                symbol  = "REG"
                title   = "Spy buffer for regulation signals"
                flags   = "HIDE"
                type    = "LOG"
                get     = "LogCaptureSpy"
                numels  = "FGC_LOG_CAPTURE_LEN"
            >
                <doc><![CDATA[
                    This property provides access to the 3 spy log signals related
                    to evaluation the regulation loop performance: Reference, Measurement and Error.
                    If regulation is not active then getting the property returns BAD_STATE.
                ]]></doc>
            </property>
        </property>
    </property>

<!-- EOF -->
