<?xml version='1.0'?>

<property
    symbol  = "TIME"
    title   = "Time related properties"
    type    = "PARENT"
    group   = "OPERATIONAL"
    get     = "Parent"
>
    <doc><![CDATA[
        This property contains properties related to time. Absolute time is based upon the Unix
        Time concept of seconds elapsed since the start of 1970, combined with the elapsed
        milliseconds within the second.
    ]]></doc>

    <property
        symbol          = "NOW"
        title           = "Current time"
        type            = "ABSTIME"
        get             = "AbsTime"
        numels          = "1"
        value_fgc2      = "mst.time"
        value_fgc3      = "ms_task.time_us"
        value_fgcd      = "timing.user_time"
        value_fgcdeqp   = "timing.user_time"
    >
        <doc><![CDATA[
            This property contains the current time.  If an FGC is connected to the
            WorldFIP network, it will be synchronised by the gateway and this
            property will contain UTC time.  If the FGC is operating standalone then
            TIME.NOW will increment from zero following the last reset.
        ]]></doc>
    </property>

    <property
        symbol      = "POWER"
        title       = "Power on time (unixtime)"
        type        = "INT32U"
        get         = "Integer"
        numels      = "1"
        value_fgc2  = "SM_ONTIME_16"
        value_fgc3  = "shared_mem.power_on_utc"
        
    >
        <doc><![CDATA[
            This property contains the UTC UNIX time that the device was powered on.
        ]]></doc>
    </property>

    <property
        symbol          = "START"
        title           = "Start time (unixtime)"
        type            = "INT32U"
        get             = "Integer"
        numels          = "1"
        value_fgc2      = "SM_STARTTIME_16"
        value_fgc3      = "shared_mem.run_software_utc"
        value_fgcd      = "fgcd.time_start.tv_sec"
        value_fgcdeqp   = "fgcd.time_start.tv_sec"
    >
        <doc><![CDATA[
            This property contains the UTC UNIX time that the device was started.
        ]]></doc>
    </property>

    <property
        symbol          = "START_A"
        title           = "Start time"
        type            = "UNIXTIME"
        get             = "UnixTime"
        numels          = "1"
        value_fgcd      = "fgcd.time_start"
        value_fgcdeqp   = "fgcd.time_start"
    >
        <doc><![CDATA[
            The date/time that the device was started.
        ]]></doc>
    </property>

    <property
        symbol      = "READ"
        title       = "Re-read the time"
        type        = "INT32S"
        group       = "CONFIG"
        set         = "Integer"
        limits      = "int"
        min         = "0"
        max         = "1"
        numels      = "1"
        value_fgcd  = "fip.read_time"
    >
        <doc><![CDATA[
            Re-read the time on next cycle if set to 1.
        ]]></doc>
    </property>

    <property
        symbol  = "CYCLE"
        title   = "PPM cycle related time properties"
        type    = "PARENT"
        group   = "OPERATIONAL"
        get     = "Parent"
    >
        <doc><![CDATA[
            This property contains the time properties related to PPM cycles.
        ]]></doc>

        <property
            symbol          = "START"
            title           = "Timestamps of starts of cycles"
            type            = "ABSTIME"
            get             = "AbsTime"
            numels          = "FGC_MAX_USER_PLUS_1"
            numels_8        = "NUM_CYC_SELECTORS"
            numels_94       = "NUM_CYC_SELECTORS"
            value_fgcd      = "timing.user_time"
            value_fgcdeqp   = "timing.user_time"
        >
            <doc><![CDATA[
                Absolute timestamps of starts of cycles.  Element 0 is the start time of the current cycle.
            ]]></doc>
        </property>

        <property
            symbol          = "MS"
            title           = "Millisecond within current cycle"
            type            = "INT32U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_fgcd      = "timing.cycle_ms"
            value_fgcdeqp   = "timing.cycle_ms"
        >
            <doc><![CDATA[
                Millisecond number within current cycle.
            ]]></doc>
        </property>

        <property
            symbol          = "USER"
            title           = "Current user"
            type            = "INT8U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_fgcd      = "timing.user"
            value_fgcdeqp   = "timing.user"
        >
            <doc><![CDATA[
                Current user received from timing system.
            ]]></doc>
        </property>

        <property
            symbol          = "NUM"
            title           = "Cycle number within the supercycle"
            type            = "INT8U"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_fgcd      = "timing.cycle_num"
            value_fgcdeqp   = "timing.cycle_num"
        >
            <doc><![CDATA[
                Cycle number within the supercycle.
            ]]></doc>
        </property>
               
    </property>              
          
</property>

<!-- EOF -->
