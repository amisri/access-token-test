<?xml version='1.0'?>

<property
    symbol  = "GW"
    title   = "Gateway properties"
    type    = "PARENT"
    group   = "OPERATIONAL"
    get     = "Parent"
>
    <doc><![CDATA[
        Properties related to the gateway.
    ]]></doc>

    <property
        symbol      = "PC_PERMIT"
        title       = "PC Permit"
        type        = "INT32U"
        group       = "CONFIG"
        symlist     = "CTRL"
        get         = "Integer"
        set         = "Integer"
        numels      = "1"
        value_fgcd  = "fieldbus.pc_permit"
    >
        <doc><![CDATA[
            Software PC_PERMIT signal read from the timing system and applied to all systems on
            the WorldFIP bus.  Note that this may be overridden by {PROP GW.PC_PERMIT_OVERRIDE}.
        ]]></doc>
    </property>

    <property
        symbol      = "PC_PERMIT_OVERRIDE"
        title       = "PC Permit Override"
        type        = "INT32S"
        group       = "CONFIG"
        get         = "Integer"
        set         = "Integer"
        limits      = "int"
        max         = "1"
        min         = "-1"
        numels      = "1"
        value_fgcd  = "fieldbus.pc_permit_override"
    >
        <doc><![CDATA[
            Override for software PC_PERMIT signal.  This allows a gateway to override the PC_PERMIT
            received from the timing system (-1=force false, 1=force true, 0=no override).
        ]]></doc>
    </property>

    <property
        symbol      = "SECTOR_ACCESS"
        title       = "Sector Access"
        type        = "INT32U"
        group       = "CONFIG"
        symlist     = "CTRL"
        get         = "Integer"
        set         = "Integer"
        numels      = "1"
        value_fgcd  = "fieldbus.sector_access"
    >
        <doc><![CDATA[
            SECTOR_ACCESS signal read from the timing system and applied to all systems on the
            WorldFIP bus.  Note that this may be overridden by {PROP.GW.SECTOR_ACCESS_OVERRIDE}.
        ]]></doc>
    </property>

    <property
        symbol      = "SECTOR_ACCESS_OVERRIDE"
        title       = "Sector Access Override"
        type        = "INT32S"
        group       = "CONFIG"
        get         = "Integer"
        set         = "Integer"
        limits      = "int"
        max         = "1"
        min         = "-1"
        numels      = "1"
        value_fgcd  = "fieldbus.sector_access_override"
    >
        <doc><![CDATA[
            Override for sector access signal.  This allows a gateway to override the SECTOR_ACCESS
            received from the timing system (-1=force false, 1=force true, 0=no override).
        ]]></doc>
    </property>

    <property
        symbol  = "READCODES"
        title   = "Read FGC code files"
        group   = "CODE"
        type    = "NULL"
        set     = "ReadCodes"
    >
        <doc><![CDATA[
            Setting this property causes the FGC code files to be re-read for download over the WorldFIP.
        ]]></doc>
    </property>

    <property
        symbol  = "FGC"
        title   = "FGC related properties in the gateway"
        type    = "PARENT"
        flags   = "FGC"
        get     = "Parent"
    >
        <doc><![CDATA[
            These properites relate to FGCs, independent of their class.
            Many are used only by the gateways to report the state of the FGCs
            that they are managing.  Some are FGC properties that are common to
            all the classes based on the FGC platform.
        ]]></doc>

        <property
            symbol      = "MASK"
            title       = "Bit mask indicating devices online"
            type        = "INT32U"
            get         = "Integer"
            flags       = ""
            numels      = "1"
            value_fgcd  = "fgcddev.status.fgc_mask"
        >
            <doc><![CDATA[
                <p>Bit mask indicating devices online.</p>

                <p>Bits are defined as follows:</p>

                <TABLE class="embedded">

                <TR>
                <TH>Bit</TH>
                <TH>Meaning</TH>
                </TR>

                <TR>
                <TD ALIGN="CENTER">0</TD>
                <TD>Indicates whether the gateway (device 0) is online and should therefore always have a value of 1.</TD>
                </TR>

                <TR>
                <TD ALIGN="CENTER">1-30</TD>
                <TD>Indicate whether FGC's on channels 1-30 respectively are online.</TD>
                </TR>

                <TR>
                <TD ALIGN="CENTER">31</TD>
                <TD>Indiciates whether the diagnostic device is online.</TD>
                </TR>

                </TABLE>
            ]]></doc>
        </property>

        <property
            symbol      = "RESET"
            title       = "Send RESET command to FGClite device"
            type        = "INT32U"
            numels      = "1"
            min         = "1"
            max         = "30"
            set         = "GwTerminate"
        >
            <doc><![CDATA[
                <p>Send a RESET to the nanoFIP controller on a FGClite device.</p>
                <p>This command is the same as sending S DEVICE.RESET to the device,
                with the exception that the device can be offline.</p>
            ]]></doc>
        </property>

        <property
            symbol      = "PWRCYC"
            title       = "Send PWRCYC command to FGClite device"
            type        = "INT32U"
            numels      = "1"
            min         = "1"
            max         = "30"
            set         = "GwTerminate"
        >
            <doc><![CDATA[
                <p>Send a PWRCYC command to a FGClite device.</p>
                <p>This command is the same as sending S DEVICE.PWRCYC to the device,
                with the exception that the device can be offline.</p>
            ]]></doc>
        </property>

        <property
            symbol      = "FIELDBUS"
            title       = "FGC fieldbus properties"
            type        = "PARENT"
            flags       = "FGC"
            get         = "Parent"
        >
            <doc><![CDATA[
                FGC fieldbus properties.
            ]]></doc>

            <property
                symbol      = "APER_MSGS"
                title       = "Aperiodic message received count"
                type        = "INT32U"
                flags       = "FGC GET_ZERO"
                get         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW"
                step_fgcd   = "sizeof(struct FIP_channel)"
                value_fgcd  = "fip.channel[0].aper_msg_rec_count"
            >
                <doc><![CDATA[
                    Count of aperiodic messages received for channel.
                ]]></doc>
            </property>

            <property
                symbol      = "DEADLINE"
                title       = "Deadline for FGClite status packets"
                type        = "INT32U"
                flags       = "FGC"
                get         = "Integer"
                set         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW+1"
                step_fgcd   = "sizeof(uint32_t)"
                value_fgcd  = "equipdev.deadline[0]"
            >
                <doc><![CDATA[
                    <p>Deadlines to receive the status packet from each FGClite device and the
                    diagnostic device, relative to the start of each 20ms cycle.</p>
                    <p>All FGClite devices on the WorldFIP fieldbus send a status packet to the
                    gateway once per FIP cycle. The status packet contains 60 bytes of critical data
                    and 64 bytes of paged data, as defined in the FGCLITE: FIELDBUS CYCLE document
                    (EDMS 1523469).</p>
                    <p>The gateway calculates the reception deadline for each status packet and the
                    deadlines are exposed in this property. In normal operation, these values should
                    be read-only. However, the status packet deadlines can also be set here to help
                    debug problems with the FIP timing.</p>
                ]]></doc>
            </property>

            <property
                symbol      = "ERROR"
                title       = "FGC fieldbus errors"
                type        = "PARENT"
                flags       = "FGC"
                get         = "Parent"
            >
                <doc><![CDATA[
                    FGC fieldbus errors.
                ]]></doc>

                <property
                    symbol      = "ACK_MISS"
                    title       = "Missed acknowledgement count"
                    type        = "INT32U"
                    flags       = "FGC GET_ZERO"
                    get         = "Integer"
                    numels      = "FGC_MAX_DEVS_PER_GW"
                    step_fgcd   = "sizeof(struct FGCSM_channel)"
                    value_fgcd  = "fgcsm.channel[0].count.ack_miss"
                >
                    <doc><![CDATA[
                        Count of missed command packet acknowledgements for channel.
                    ]]></doc>
                </property>

                <property
                    symbol      = "RD_MISS"
                    title       = "Missed acknowledgement count"
                    type        = "INT32U"
                    flags       = "FGC GET_ZERO"
                    get         = "Integer"
                    numels      = "FGC_MAX_DEVS_PER_GW"
                    step_fgcd   = "sizeof(struct FGCSM_channel)"
                    value_fgcd  = "fgcsm.channel[0].count.rd_miss"
                >
                    <doc><![CDATA[
                        Count of missed real-time variables for channel.
                    ]]></doc>
                </property>

                <property
                    symbol      = "TIME_MISS"
                    title       = "Missed time variable count"
                    type        = "INT32U"
                    flags       = "FGC GET_ZERO"
                    get         = "Integer"
                    numels      = "FGC_MAX_DEVS_PER_GW"
                    step_fgcd   = "sizeof(struct FGCSM_channel)"
                    value_fgcd  = "fgcsm.channel[0].count.time_miss"
                >
                    <doc><![CDATA[
                        Count of missed time variables for channel.
                    ]]></doc>
                </property>

                <property
                    symbol      = "PROTO"
                    title       = "Protocol error count"
                    type        = "INT32U"
                    flags       = "FGC GET_ZERO"
                    get         = "Integer"
                    numels      = "FGC_MAX_DEVS_PER_GW"
                    step_fgcd   = "sizeof(struct FGCSM_channel)"
                    value_fgcd  = "fgcsm.channel[0].count.proto_error"
                >
                    <doc><![CDATA[
                        Count of protocol errors for channel.
                    ]]></doc>
                </property>

                <property
                    symbol      = "WFIP"
                    title       = "FGC WorldFIP errors"
                    type        = "PARENT"
                    flags       = "FGC"
                    get         = "Parent"
                >
                    <doc><![CDATA[
                        FGC WorldFIP errors.
                    ]]></doc>

                    <property
                        symbol      = "NONSIG_FLT"
                        title       = "Non-significance fault count"
                        type        = "INT32U"
                        flags       = "FGC GET_ZERO"
                        get         = "Integer"
                        numels      = "FGC_MAX_DEVS_PER_GW"
                        step_fgcd   = "sizeof(struct FIP_channel)"
                        value_fgcd  = "fip.channel[0].fip_stats.nonsignificance_fault_count"
                    >
                        <doc><![CDATA[
                            Count of non-significance faults for channel.
                        ]]></doc>
                    </property>

                    <property
                        symbol      = "PROMPT_FAIL"
                        title       = "Promptness failure count"
                        type        = "INT32U"
                        flags       = "FGC GET_ZERO"
                        get         = "Integer"
                        numels      = "FGC_MAX_DEVS_PER_GW"
                        step_fgcd   = "sizeof(struct FIP_channel)"
                        value_fgcd  = "fip.channel[0].fip_stats.promptness_fail_count"
                    >
                        <doc><![CDATA[
                            Count of promptness failures for channel.
                        ]]></doc>
                    </property>

                    <property
                        symbol      = "SIG_FLT"
                        title       = "Significance fault count"
                        type        = "INT32U"
                        flags       = "FGC GET_ZERO"
                        get         = "Integer"
                        numels      = "FGC_MAX_DEVS_PER_GW"
                        step_fgcd   = "sizeof(struct FIP_channel)"
                        value_fgcd  = "fip.channel[0].fip_stats.significance_fault_count"
                    >
                        <doc><![CDATA[
                            Count of significance faults for channel.
                        ]]></doc>
                    </property>

                    <property
                        symbol      = "USER_ERROR"
                        title       = "User error count"
                        type        = "INT32U"
                        flags       = "FGC GET_ZERO"
                        get         = "Integer"
                        numels      = "FGC_MAX_DEVS_PER_GW"
                        step_fgcd   = "sizeof(struct FIP_channel)"
                        value_fgcd  = "fip.channel[0].fip_stats.user_error_count"
                    >
                        <doc><![CDATA[
                            Count of user errors for channel.
                        ]]></doc>
                    </property>
                </property>
            </property>

            <property
                symbol      = "OFFLINES"
                title       = "Offline transition count"
                type        = "INT32U"
                flags       = "FGC GET_ZERO"
                get         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW"
                step_fgcd   = "sizeof(struct FGCSM_channel)"
                value_fgcd  = "fgcsm.channel[0].count.offline_transition"
            >
                <doc><![CDATA[
                    Count of number of times channel has gone offline.
                ]]></doc>
            </property>

            <property
                symbol      = "STATUS_MISS"
                title       = "Status variable miss count"
                type        = "INT32U"
                flags       = "FGC GET_ZERO"
                get         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW"
                step_fgcd   = "sizeof(struct FGCSM_channel)"
                value_fgcd  = "fgcsm.channel[0].count.status_miss"
            >
                <doc><![CDATA[
                    Count of status variables missed for channel.
                ]]></doc>
            </property>

            <property
                symbol      = "STATUS_REC"
                title       = "Status variable count"
                type        = "INT32U"
                flags       = "FGC GET_ZERO"
                get         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW"
                step_fgcd   = "sizeof(struct FGCSM_channel)"
                value_fgcd  = "fgcsm.channel[0].count.status_rec"
            >
                <doc><![CDATA[
                    Count of status variables received for channel.
                ]]></doc>
            </property>

            <property
                symbol  = "SYNC_LOGS"
                title   = "Send FGC log synchronisation signal"
                type    = "NULL"
                set     = "FGCLogSync"
            >
                <doc><![CDATA[
                    Sends FGC log synchronisation signal.  Setting this
                    property with no value will cause the gateway to set
                    the FGC log synchronisation bit in the WorldFIP time
                    variable for a pre-defined number of cycles.  The
                    FGCs will align their log acquisitions to the start
                    of the second containing the transition of the bit.
                ]]></doc>
            </property>

            <property
                symbol      = "UNLOCKS"
                title       = "Unlock transition count"
                type        = "INT32U"
                flags       = "FGC GET_ZERO"
                get         = "Integer"
                numels      = "FGC_MAX_DEVS_PER_GW"
                step_fgcd   = "sizeof(struct FGCSM_channel)"
                value_fgcd  = "fgcsm.channel[0].count.unlock_transition"
            >
                <doc><![CDATA[
                    Count of number of times channel has unlocked.
                ]]></doc>
            </property>
        </property>

        <property
            symbol      = "ID"
            title       = "Channel ID"
            type        = "INT32S"
            flags       = "FGC"
            get         = "Integer"
            numels      = "FGC_MAX_DEVS_PER_GW"
            step_fgcd   = "sizeof(struct FGCD_device)"
            value_fgcd  = "fgcd.device[0].id"
        >
            <doc><![CDATA[
                Channel ID number.
            ]]></doc>
        </property>

        <property
            symbol      = "NAME"
            title       = "Device name"
            group       = "CONFIG"
            type        = "STRING"
            flags       = "FGC"
            get         = "String"
            numels      = "FGC_MAX_DEVS_PER_GW"
            step_fgcd   = "sizeof(struct FGCD_device)"
            value_fgcd  = "fgcd.device[0].name"
        >
            <doc><![CDATA[
                The name of the device.
            ]]></doc>
        </property>

        <property
            symbol      = "LHC_SECTORS"
            title       = "LHC Sectors affected by the device"
            type        = "INT16U"
            flags       = "BIT_MASK FGC"
            get         = "Integer"
            symlist     = "LHC_SECTORS"
            numels      = "FGC_MAX_DEVS_PER_GW"
            step_fgcd   = "sizeof(struct FGCD_device)"
            value_fgcd  = "fgcd.device[0].omode_mask"
        >
            <doc><![CDATA[
                LHC sectors affected by the device.
            ]]></doc>
        </property>

        <property
            symbol      = "STATE"
            title       = "Channel state"
            type        = "INT32U"
            symlist     = "FGC_STATE"
            flags       = "FGC"
            get         = "Integer"
            numels      = "FGC_MAX_DEVS_PER_GW"
            step_fgcd   = "sizeof(struct FGCSM_channel)"
            value_fgcd  = "fgcsm.channel[0].cmd_state"
        >
            <doc><![CDATA[
                State within the command state machine for this channel.
            ]]></doc>
        </property>

        <property
            symbol      = "RUNLOG"
            title       = "FGC run log"
            type        = "RUNLOG"
            flags       = "FGC HIDE"
            get         = "Bin"
            numels      = "FGC_MAX_DEVS_PER_GW"
            step_fgcd   = "sizeof(struct Fieldbus_channel)"
            value_fgcd  = "fieldbus.channel[0].runlog"
        >
            <doc><![CDATA[
                FGC run log.
            ]]></doc>
        </property>
    </property>
</property>

<!-- EOF -->
