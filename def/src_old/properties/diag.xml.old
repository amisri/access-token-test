<?xml version='1.0'?>

<!DOCTYPE memmap [
        <!ENTITY dims                SYSTEM "properties/inc/dims.xml"                >
]>

<property
    symbol  = "DIAG"
    type    = "PARENT"
    title   = "Diagnostic interface properties"
    group   = "DEFAULT"
    get     = "Parent"
    set     = "Reset"
>
    <doc><![CDATA[
        This property contains properties related the diagnostic interface. This interface
        can support up to 30 diagnostic interface modules (DIMs),
        each providing two banks of 12 digital inputs and
        four analogue channels, converted using a 12 bit ADC.
        The diagnostic interface is divided into two buses(A and B) by a multiplexer
        and upto 15 DIMs can be connected on each bus.
    ]]></doc>

    <property
        symbol      = "DIMS_EXPECTED"
        title       = "Number of DIMs expected on each diagnostic bus (A and B)"
        dsp_fgc2    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "2"
        value_fgc2  = "qspi_misc.expected"
        value_fgc3  = "qspi_misc.expected"
        value_92    = "equipdev.device[0].diag.dims_expected"
    >
        <doc><![CDATA[
            This property contains the number of DIMs expected on each diagnostic bus.
            This is based on the type of system that the FGC is controlling, which is
            defined by the start of the {PROP DEVICE.NAME}.  If the number of DIMs detected
            on each bus ({PROP DIAG.DIMS_DETECTED}) is different to the number expected, then the
            DIAG_FLT bit will be set in the {PROP STATUS.ST_LATCHED} property, which will produce
            an <A href="{PROP STATUS.WARNINGS}">FGC_HW</A> warning.
        ]]></doc>
    </property>

    <property
        symbol      = "DIMS_DETECTED"
        title       = "Number of DIM detected on each diagnostic bus (A and B)"
        dsp_fgc2    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "2"
        value_fgc2  = "qspi_misc.detected"
        value_fgc3  = "qspi_misc.detected"
        value_92    = "equipdev.device[0].diag.dims_detected"
    >
        <doc><![CDATA[
            This property contains the number of DIM cards detected on each diagnostic bus.
            The maximum number of DIMs per bus is 15.  The check is performed after every 20ms scan.
            If the number of DIMs expected
            on each bus is different to the number expected ({PROP DIAG.DIMS_EXPECTED}), then the
            DIAG_FLT bit will be set in the {PROP STATUS.ST_LATCHED} property, which will produce
            an <A href="{PROP STATUS.WARNINGS}">FGC_HW</A> warning.
        ]]></doc>
    </property>

    <property
        symbol      = "DIMS_EXP_ERRS"
        title       = "Number of DIMS expected errors"
        flags       = "GET_ZERO"
        dsp_fgc2    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "1"
        value_fgc2  = "qspi_misc.expected_detected_mismatches"
        value_fgc3  = "qspi_misc.expected_detected_mismatches"
        value_92    = "equipdev.device[0].diag.dims_exp_errs"
    >
        <doc><![CDATA[
            This property contains the counter of the number of times the DIMS_DETECTED
            did not match the DIMS_EXPECTED.
        ]]></doc>
    </property>

    <property
        symbol      = "SYNC_RESETS"
        title       = "Number of unsolicited DIAG resets"
        flags       = "GET_ZERO"
        type        = "INT32U"
        get         = "Integer"
        numels      = "1"
        value_fgc2  = "diag.sync_resets"
        value_fgc3  = "diag.sync_resets"
        value_92    = "equipdev.device[0].diag.sync_resets"
    >
        <doc><![CDATA[
            This property contains the number of times the diagnostic interface has been reset
            due to DIM synchronisation faults since the last time the FGC was reset (or the property
            was read with the {GOPT ZERO} get option.  The number of synchronisation faults per DIM
            can be read from the property {PROP DIAG.DIM_SYNC_FLTS}.
        ]]></doc>
    </property>

    <property
        symbol      = "DIM_SYNC_FLTS"
        title       = "Number of synchronisation faults per DIM"
        flags       = "HIDE GET_ZERO"
        dsp_fgc2    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "FGC_MAX_DIM_BUS_ADDR"
        value_fgc2  = "qspi_misc.flat_qspi_non_valid_counts"
        value_fgc3  = "qspi_misc.flat_qspi_non_valid_counts"
        value_92    = "equipdev.device[0].diag.dim_sync_flts"
    >
        <doc><![CDATA[
            This property contains the number of times a channel synchronisation failure was
            detected per DIM.  The array is in DIM bus address order, where DIMS on branch A are from
            [1] to [15] and for branch B from [17] to [31].  When ever a synchronisation fault is
            detected a reset is issued (this always affects both buses).  The total number
            of these resets can be seen from the property {PROP DIAG.SYNC_RESETS}.
        ]]></doc>
    </property>

    <property
        symbol      = "DEBUGLOG"
        title       = "Debug data on DIPS channels"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        dsp_fgc3    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "FGC_LOG_DIAG_DBUG_LEN"
        value_fgc2  = "dsp_debug.buf"
        value_fgc3  = "dsp_debug.buf"
    >
        <doc><![CDATA[
            This property contains debug data related to the detection of channel sync faults.
        ]]></doc>
    </property>

    <property
        symbol      = "DEBUGIDX"
        title       = "DEBUGLOG index"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        dsp_fgc3    = "fgc"
        type        = "INT32U"
        get         = "Integer"
        numels      = "1"
        value_fgc2  = "dsp_debug.idx"
        value_fgc3  = "dsp_debug.idx"
    >
        <doc><![CDATA[
            This property contains the index of the oldest sample in the debug log.  This is a
            circular buffer that will be frozen with 50% post-trigger samples in the event of a
            desynchronisation being detected.  The buffer can be unfrozen by S PC RESET or
            S PC SB/IL.
        ]]></doc>
    </property>

    <property
        symbol      = "DATA"
        title       = "Diagnostic data"
        type        = "INT16U"
        flags       = "HIDE"
        get         = "Integer"
        numels      = "352"
        value_fgc2  = "diag.data"
        value_fgc3  = "diag.data"
        value_92    = "equipdev.device[0].diag.data"
    >
        <doc><![CDATA[
            <p>This property contains the diagnostic data in DIM bus order.
            The first 128 elements are DIM analogue channels (4 per DIM), the following 64 elements are DIM digital channels
            (2*12 bits per DIM), then there are 64 elements of software channels, 32 channels of diagnostic channels
            (DIM timers), and finally 64 elements for the latched DIM digital channels. Note that DIM channels with no
            valid card will normally appear to have the value 0xBABn provided the loop back at the last DIM is working,
            where n=0,1,2,3.... Otherwise 0xFFFF may be seen, or potentially any other value if there is a bad DIM or
            a cabling problem.</p>
            <p>
            The DIAG.DATA array is organized in columns (one column per DIM). To calculate the index where to look for a particular register
            on a given DIM, use the following formula:</p>
            <p><b>Index in DIAG.DATA = dim_idx + 0x20 * reg_idx</b></p>
            <p><b>Where:</b>
            <ul>
            <li><b>dim_idx</b>: the physical DIM ID. That is range 0..14 for Bus A, and range 16..30 for bus B.</li>
            <li><b>reg_idx</b>: an identifier for the analog, digital, timing, etc. registers banks. List below:
                <pre>
                    ANA1            0
                    ANA2            1
                    ANA3            2
                    ANA4            3
                    DIG1            4
                    DIG2            5
                    SWTIMING1       6
                    SWTIMING2       7
                    DIMCOUNTERS     8
                    LATCHEDDIG1     9
                    LATCHEDDIG2    10
                </pre>
            </li>
            </ul>
            </p>
            <p>Regarding the software channels, SWTIMING1/2, they are not directly related to DIMs. They provide debugging
            information on the MCU CPU consumption.<br/>
            <b>SWTIMING1</b> comprises of the Tasks and ISRs CPU consumption integrated over 1 second. The
            unit is a 2us tick, so a value of 50000 in DIAG.DATA would correspond to a 100% CPU usage. The first half of
            this bank (16 entries) is dedicated to the MCU tasks. The second half is dedicated to the ISRs.<br/>
            <b>SWTIMING2</b> provides a measurement of the max duration of the total MST process (meaning, the MST ISR plus
            the MST task), for each millisecond in a 20ms frame. The max duration is reset every 100ms and the unit of 
            measurement is the microsecond.</p>
        ]]></doc>
    </property>

    <property
        symbol      = "LIST0"
        title       = "First diagnostic channel list"
        type        = "INT8U"
        flags       = "HIDE INDIRECT_N_ELS"
        get         = "DiagChan"
        set         = "Integer"
        numels      = "(uintptr_t) &amp; diag.list_len[0]"
        maxels      = "FGC_DIAG_LIST_LEN"
        value_fgc2  = "diag.list[0]"
        value_fgc3  = "diag.list[0]"
    >
        <doc><![CDATA[
            This property defines which diagnostic data fields to return via RT Channel 0 over
            the fieldbus to the gateway.
        ]]></doc>
    </property>

    <property
        symbol      = "LIST1"
        title       = "Second diagnostic channel list"
        type        = "INT8U"
        flags       = "HIDE INDIRECT_N_ELS"
        get         = "DiagChan"
        set         = "Integer"
        numels      = "(uintptr_t) &amp; diag.list_len[1]"
        maxels      = "FGC_DIAG_LIST_LEN"
        value_fgc2  = "diag.list[1]"
        value_fgc3  = "diag.list[1]"
    >
        <doc><![CDATA[
            This property defines which diagnostic data fields to return via RT Channel 1 over
            the fieldbus to the gateway.
        ]]></doc>
    </property>

    <property
        symbol      = "LIST2"
        title       = "Third diagnostic channel list"
        type        = "INT8U"
        flags       = "HIDE INDIRECT_N_ELS"
        get         = "DiagChan"
        set         = "Integer"
        numels      = "(uintptr_t) &amp; diag.list_len[2]"
        maxels      = "FGC_DIAG_LIST_LEN"
        value_fgc2  = "diag.list[2]"
        value_fgc3  = "diag.list[2]"
    >
        <doc><![CDATA[
            This property defines which diagnostic data fields to return via RT Channel 2 over
            the fieldbus to the gateway.
        ]]></doc>
    </property>

    <property
        symbol      = "LIST3"
        title       = "Fourth diagnostic channel list"
        type        = "INT8U"
        flags       = "HIDE INDIRECT_N_ELS"
        get         = "DiagChan"
        set         = "Integer"
        numels      = "(uintptr_t) &amp; diag.list_len[3]"
        maxels      = "FGC_DIAG_LIST_LEN"
        value_fgc2  = "diag.list[3]"
        value_fgc3  = "diag.list[3]"
    >
        <doc><![CDATA[
            This property defines which diagnostic data fields to return via RT Channel 3 over
            the fieldbus to the gateway.
        ]]></doc>
    </property>

    <property
        symbol      = "RTD"
        title       = "Real-time display diagnostic channel list"
        type        = "INT8U"
        flags       = "HIDE INDIRECT_N_ELS"
        get         = "DiagChan"
        set         = "Integer"
        numels      = "(uintptr_t) &amp; diag.list_rtd_len"
        maxels      = "FGC_DIAG_RTD_LIST_LEN"
        value_fgc2  = "diag.list_rtd"
        value_fgc3  = "diag.list_rtd"
    >
        <doc><![CDATA[
            This property defines which diagnostic data fields to show on the real time
            display option line, if the option line control {PROP RTD} is set to DIAG.
            Channel addressing is in diagnostic bus order, meaning the values set in this
            property are the indexes of the diagnostic data fields in property {PROP DIAG.DATA}.
        ]]></doc>
    </property>

    <property
        symbol      = "LIST_OFFSET"
        title       = "Diagnostic channel list offsets"
        type        = "INT8S"
        flags       = "HIDE DONT_SHRINK"
        get         = "Integer"
        set         = "Integer"
        numels      = "FGC_DIAG_N_LISTS"
        value_fgc2  = "diag.list_offset"
        value_fgc3  = "diag.list_offset"
    >
        <doc><![CDATA[
            This property defines the offset to be added to the channels in the four real-time
            diagnostic lists, if the channel address is less than 192 and is therefore a DIM
            channel.
        ]]></doc>
    </property>

    <property
        symbol      = "GAINS"
        title       = "Analogue DIM channel gains"
        type        = "FLOAT"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        get         = "Float"
        numels      = "FGC_N_DIAG_ANA_CHANS"
        value_fgc2  = "dim_list.gains"
        value_fgc3  = "dim_collection.gains"
        value_92    = "equipdev.device[0].diag.gain"
    >
        <doc><![CDATA[
            This property contains the gains for the all the analogue DIM channels.
            The value can be used with the offset to convert the unsigned short
            integer values received for a channel into a floating point signal value:
            <p align="center">Signal = GAIN * Raw_signal + OFFSET</p>
        ]]></doc>
    </property>

    <property
        symbol      = "OFFSETS"
        title       = "Analogue DIM channel offsets"
        type        = "FLOAT"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        get         = "Float"
        numels      = "FGC_N_DIAG_ANA_CHANS"
        value_fgc2  = "dim_list.offsets"
        value_fgc3  = "dim_collection.offsets"
        value_92    = "equipdev.device[0].diag.offset"
    >
        <doc><![CDATA[
            This property contains the gains for the all the analogue DIM channels.
            The value can be used with the gain to convert the unsigned short
            integer values received for a channel into a floating point signal value:
            <p align="center">Signal = GAIN * Raw_signal + OFFSET</p>
        ]]></doc>
    </property>

    <property
        symbol    = "DIM_NAMES"
        title     = "DIM names for each channel"
        type      = "STRING"
        get       = "Dim"
        maxels    = "FGC_MAX_DIMS"
    >
        <doc><![CDATA[
            This property contains the list of three colon separated fields:
            <ul>
            <li>Field 1 : DIM name</li>
            <li>Field 2 : DIM index (Logical addressing 0-19)</li>
            <li>Field 3 : DIM bus address (Physical addressing 0x00 - 0x1F)</li>
            </ul>
            The bus address is provided for debugging purposes.
        ]]></doc>
    </property>

    <property
        symbol      = "BUS_ADDRESSES"
        title       = "DIM bus flat address [0..32] for each channel"
        type        = "INT32U"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        get         = "Integer"
        numels      = "FGC_MAX_DIMS"
        value_fgc2  = "dim_list.flat_qspi_board_number"
        value_fgc3  = "dim_collection.flat_qspi_board_number"
        value_92    = "equipdev.device[0].diag.bus_address"
    >
        <doc><![CDATA[
            This property contains the list of dim bus addresses for DIM channel.
        ]]></doc>
    </property>

    <property
        symbol      = "ANA_LBLS"
        title       = "Analog channel labels"
        type        = "PARENT"
        get         = "Parent"
        maxels      = "FGC_MAX_DIMS"
        flags       = "HIDE"
        value_fgc2  = "diag.dim_props"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            This property contains the dynamically defined properties for the analogue labels
            for each DIM.  Each dim has four analogue inputs and for each input the label is
            reported as a string containing three colon separated fields: Offset:Label:Units.
        ]]></doc>

        &dims;           <!-- Include list of DIMs -->

    </property>

    <property
        symbol      = "DIG_LBLS"
        title       = "Digital DIM bank channel labels"
        type        = "PARENT"
        get         = "Parent"
        maxels      = "FGC_MAX_DIMS"
        flags       = "HIDE"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            This property contains the dynamically defined properties for the input channel labels. For each bank there are 12 digital inputs and for each the label and interpretation are provided in a string with five colon separated fields:
            <p>MASK:TYPE:LABEL:ZERO:ONE
        ]]></doc>

        &dims;           <!-- Include list of DIMs -->

    </property>


    <property
        symbol      = "DIG80_LBLS"
        title       = "First digital DIM bank channel labels"
        type        = "PARENT"
        get         = "Parent"
        maxels      = "FGC_MAX_DIMS"
        flags       = "HIDE"
        value_fgc2  = "diag.dim_props"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            This property contains the dynamically defined properties for the input channel labels
            for the first digital bank on each DIM.  The channel offset for this bank is 0x80.
            For each bank there are 12 digital inputs and for each the label and interpretation
            are provided in a string with five colon separated fields:
            <p>MASK:TYPE:LABEL:ZERO:ONE
        ]]></doc>

        &dims;           <!-- Include list of DIMs -->

    </property>

    <property
        symbol      = "DIGA0_LBLS"
        title       = "Second digital DIM bank channel labels"
        type        = "PARENT"
        get         = "Parent"
        maxels      = "FGC_MAX_DIMS"
        flags       = "HIDE"
        value_fgc2  = "diag.dim_props"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            This property contains the dynamically defined properties for the input channel labels
            for the second digital bank on each DIM.  The channel offset for this bank is 0xA0.
            For each bank there are 12 digital inputs and for each the label and interpretation
            are provided in a string with five colon separated fields:
            <p>MASK:TYPE:LABEL:ZERO:ONE
        ]]></doc>

        &dims;           <!-- Include list of DIMs -->

    </property>

    <property
        symbol      = "DIG"
        title       = "Digital DIM channel values"
        type        = "PARENT"
        get         = "Parent"
        flags       = "HIDE"
        maxels_fgc2 = "FGC_MAX_DIMS"
        maxels_fgc3 = "FGC_MAX_DIMS"
        value_fgc2  = "diag.dim_props"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            This property contains the dynamically defined properties for the digital inputs
            for the DIMs connected to the FGC.
        ]]></doc>

        <property
            symbol      = "DUMMY"
            title       = "Dummy dim"
            type        = "INT32U"
            get         = "Integer"
            flags       = "HIDE"
            value_fgc2  = "dpcom.dsp.run_code"
        >
            <doc><![CDATA[
                Dummy property
            ]]></doc>
        </property>

        <property
            symbol      = "DATA"
            title       = "Digital DIM channel values"
            type        = "STRING"
            get         = "DiagDig"
        >
            <doc><![CDATA[
                <p>Values of the digital inputs for the DIMs connected to the FGC.</p>
                <p>{PROP DIAG.DIG.DATA} is a replacement for the previous scheme where DIM values
                were defined as leaf properties which were created at runtime. This scheme is
                deprecated due to the complexity of keeping the DIM definitions and XML definitions
                in sync. The new scheme simply returns an array of values without creating any
                dynamic properties.</p>
                <p>The values exposed in this property shows the current values on the DIM.</p>
                <p>The LT flag means that this digital input caused the DIM to trigger. However, it is possible that the current fault is not active.</p>
                <p>The UL flag means that the DIM trigger input is still high, and therefore at least one of the 
                   trigger inputs is still high. The UL flag applies to all the digital input trigger of a 
                   given DIM</p>
            ]]></doc>
        </property>
    </property>

    <property
        symbol      = "ANA"
        title       = "Analogue DIM channel values"
        type        = "PARENT"
        get         = "Parent"
        flags       = "HIDE"
        maxels_fgc2 = "FGC_MAX_DIMS"
        maxels_fgc3 = "FGC_MAX_DIMS"
        value_fgc2  = "diag.dim_props"
        value_fgc3  = "diag.dim_props"
    >
        <doc><![CDATA[
            <p>This property contains the dynamically defined properties for the analogue inputs
            for the DIMs connected to the FGC.</p>
            <p>The leaf properties of {PROP DIAG.ANA} contain colon separated values and can not 
            be used for logging. Therefore, {PROP DIAG.ANASIGS} array should be used instead.</p>   
        ]]></doc>
        
        <property
            symbol      = "DUMMY"
            title       = "Dummy dim"
            type        = "INT32U"
            get         = "Integer"
            flags       = "HIDE"
            value_fgc2  = "dpcom.dsp.run_code"
        >
            <doc><![CDATA[
                Dummy property
            ]]></doc>
        </property>

        <property
            symbol      = "DATA"
            title       = "Analogue DIM channel values"
            type        = "STRING"
            get         = "DiagAna"
        >
            <doc><![CDATA[
                <p>Values of the analogue inputs for the DIMs connected to the FGC.</p>
                <p>{PROP DIAG.ANA.DATA} is a replacement for the previous scheme where DIM values
                were defined as leaf properties which were created at runtime. This scheme is
                deprecated due to the complexity of keeping the DIM definitions and XML definitions
                in sync. The new scheme simply returns an array of values without creating any
                dynamic properties.</p>
                <p>In both the old and new schemes, {PROP DIAG.ANA} cannot be used for logging, as it
                contains colon separated values. Therefore, {PROP DIAG.ANASIGS} array should be used
                instead.</p>   
            ]]></doc>
        </property>
    </property>

    <property
        symbol      = "FAULTS"
        title       = "Digital diagnostic inputs in fault"
        operational = "1"
        flags       = "HIDE"
        group       = "OPERATIONAL"
        type        = "STRING"
        get         = "Dim"
        maxels      = "FGC_MAX_DIMS"
        kt          = "1"
    >
        <doc><![CDATA[
            This property contains the list of digital DIM inputs that are active faults.
        ]]></doc>
    </property>

    <property
        symbol      = "ANASIGS"
        title       = "Analogue DIM channel values"
        type        = "FLOAT"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        get         = "Float"
        numels      = "FGC_N_DIAG_ANA_SIGS"
        value_fgc2  = "dim_list.analogue_in_physical"
        value_fgc3  = "dim_collection.analogue_in_physical"
        value_92    = "equipdev.device[0].diag.anasigs"
    >
        <doc><![CDATA[
            This property is used internally by the FGC software to make the real-time
            values of the analogue DIM signals available via the {PROP DIAG.ANA} properties.
            The mapping between Analog DIM signals and {PROP DIAG.ANASIGS} array elements is:
            <pre>
                DIAG.ANASIGS[DIM_INDEX + 20*CHANNEL_INDEX]
            </pre>
        ]]></doc>
    </property>

    <property
        symbol      = "TRIGUS"
        title       = "DIM trigger time in microseconds"
        type        = "INT32U"
        flags       = "HIDE"
        dsp_fgc2    = "fgc"
        get         = "Integer"
        numels      = "FGC_MAX_DIMS"
        value_fgc2  = "dim_list.trig_us"
        value_fgc3  = "dim_collection.trig_us"
        value_92    = "equipdev.device[0].diag.trigus"
    >
        <doc><![CDATA[
            This debugging property reports the trigger time in microseconds
            within the second for the last trigger received for each DIM.
        ]]></doc>
    </property>

    <property
        symbol      = "LOGSTAT"
        title       = "DIM log status after a log readout"
        type        = "INT32U"
        flags       = "HIDE"
        get         = "Integer"
        numels      = "12"
        value_fgc2  = "diag.dim_log_status"
        value_fgc3  = "dbg_dim_log_status[0]"
    >
        <doc><![CDATA[
            This property contains the DIM log status following the readout of a DIM logs by
            Spy.  For each DIM there are four values:
            <ul>
            <li>samples_to_acq</li>
            <li>last_sample_idx</li>
            <li>Unix time of last sample</li>
            <li>Millisecond time of last sample</li>
            </ul>
            Up to 3 DIMs can be accessed.
        ]]></doc>
    </property>
</property>

<!-- EOF -->
