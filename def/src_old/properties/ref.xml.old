<?xml version='1.0'?>

<!DOCTYPE REF [
    <!ENTITY ref_tlp             SYSTEM "properties/inc/ref/ref_tlp.xml">
    <!ENTITY ref_direct          SYSTEM "properties/inc/ref/ref_direct.xml">
    <!ENTITY ref_event           SYSTEM "properties/inc/ref/ref_event.xml">
    <!ENTITY ref_time            SYSTEM "properties/inc/ref/ref_time.xml">
    <!ENTITY ref_func            SYSTEM "properties/inc/ref/ref_func.xml">
    <!ENTITY ref_ilc             SYSTEM "properties/inc/ref/ref_ilc.xml">
    <!ENTITY ref_cyc             SYSTEM "properties/inc/ref/ref_cyc.xml">
    <!ENTITY ref_error           SYSTEM "properties/inc/ref/ref_error.xml">
    <!ENTITY ref_info            SYSTEM "properties/inc/ref/ref_info.xml">
    <!ENTITY ref_direct          SYSTEM "properties/inc/ref/ref_direct.xml">
    <!ENTITY ref_defaults        SYSTEM "properties/inc/ref/ref_defaults.xml">
    <!ENTITY ref_rate            SYSTEM "properties/inc/ref/ref_rate.xml">
    <!ENTITY ref_start           SYSTEM "properties/inc/ref/ref_start.xml">
    <!ENTITY ref_first_plateau   SYSTEM "properties/inc/ref/ref_first_plateau.xml">
    <!ENTITY ref_external        SYSTEM "properties/inc/ref/ref_external.xml">
    <!ENTITY ref_v_feed_fwd      SYSTEM "properties/inc/ref/ref_v_feed_fwd.xml">
    <!ENTITY ref_v_harmonics     SYSTEM "properties/inc/ref/ref_v_harmonics.xml">
    <!ENTITY ref_economy         SYSTEM "properties/inc/ref/ref_economy.xml">
    <!ENTITY ref_armed           SYSTEM "properties/inc/ref/ref_armed.xml">

    <!ENTITY ref_cubexp          SYSTEM "properties/inc/ref/ref_cubexp.xml">
    <!ENTITY ref_plep            SYSTEM "properties/inc/ref/ref_plep.xml">
    <!ENTITY ref_pppl            SYSTEM "properties/inc/ref/ref_pppl.xml">
    <!ENTITY ref_prbs            SYSTEM "properties/inc/ref/ref_prbs.xml">
    <!ENTITY ref_pulse           SYSTEM "properties/inc/ref/ref_pulse.xml">
    <!ENTITY ref_ramp            SYSTEM "properties/inc/ref/ref_ramp.xml">
    <!ENTITY ref_table           SYSTEM "properties/inc/ref/ref_table.xml">
    <!ENTITY ref_trim            SYSTEM "properties/inc/ref/ref_trim.xml">
    <!ENTITY ref_test            SYSTEM "properties/inc/ref/ref_test.xml">
]>

<property
    symbol          = "REF"
    type            = "PARENT"
    title           = "Reference Function Generator Properties"
    flags           = "TRANSACTIONAL"
    group           = "OPERATIONAL"
    get             = "Parent"
    set             = "Ref"
    setif           = "RefOk"
    class_symlist   = "REF"
>
    <doc><![CDATA[
        <p>This property contains the properties related to the generation of the reference.
        The reference function generator can be armed for non-CYCLING operation when in IDLE state or
        for CYCLING operation when in any state except IDLE.  The units of the reference depend upon the
        reference function regulation mode ({PROP REG.MODE} or {PROP REG.MODE_CYC}).</p>
        <p>If the FGC is configured for non-PPM cycling operation by {PROP DEVICE.PPM} then
        the non-PPM reference must be used for both non-CYCLING operation and CYCLING non-PPM
        operation.</p>
        <p>Arming a reference starts by setting the properties associated with the type of reference
        function that is required (except for the ZERO function which needs no properties):
        <ul>
        <li>{PROP REF.RAMP} : RAMP</li>
        <li>{PROP REF.PULSE} : PULSE</li>
        <li>{PROP REF.PLEP} : PLEP</li>
        <li>{PROP REF.PPPL} : PPPL</li>
        <li>{PROP REF.CUBEXP} : CUBEXP</li>
        <li>{PROP REF.TABLE} : TABLE</li>
        <li>{PROP REF.TRIM} : CTRIM, LTRIM</li>
        <li>{PROP REF.TEST} : SINE, COSINE, OFFCOS, SQUARE, STEPS</li>
        <li>{PROP REF.PRBS} : PRBS</li>
        </ul>
        The regulation mode is taken from {PROP REG.MODE} when in IDLE state or {PROP REG.MODE_CYC} when arming
        functions to play in CYCLING. Finally the function
        is armed by setting the {PROP REF.FUNC.TYPE} to the type of function to arm.</p>
        <p>When in IDLE state, many of the functions can be armed with a single command by setting the REF
        property. For example:</p>
        <p style="text-align:center"><b>S REF PLEP, 100, 10, 50</b></p>
        <p>The first parameter is the type of function to arm and it is followed by one or more numerical parameters.
        First, these are parsed to set the assocaited REF properties and then {PROP REF.FUNC.TYPE} is set to the
        function type given as the first parameter.
        Consult the documentation below for each type of function to learn the order of the parameters.
        If a parameter value is skipped then a default value will be used, except for the first numeric
        parameter, which has no default value.</p>
        <p>If more parameters than expected are supplied, a {ERR bad_array_len} error will be reported. If any
        parameters are not a valid number or are out of limits then the error will be {ERR bad_parameter}.</p>
        <p>A special function called "NOW" is provided to make it easy to ramp the reference to a given level
        when in IDLE state.
        It combine S REF RAMP, <i>final_ref</i> (new classes) or S REF PLEP, <i>final_ref</i> (old classes)
        with S REF.RUN. In other words, it both arms and runs a function that can smoothly change the reference to the
        final reference value. For example, to ramp the reference smoothly to 10 units
        (Volts, Amps or Gauss according to the regulation mode), it is sufficient to run the command:</p>
        <p style="text-align:center"><b>S REF NOW, 10</b></p>
        <p>This will use the default values for acceleration, linear rate limit and deceleration.
        It is possible to override these default values but adding extra parameters. For example:</p>
        <p style="text-align:center"><b>S REF NOW, 10, , 100</b></p>
        <p>This will ramp to 10 units with the default acceleration and deceleration but with a
        linear rate limit of 100 units per second.</p>
    ]]></doc>

    <!-- REF top level properties ======================================================================== -->

    &ref_tlp;

    <!-- REF event properties ============================================================================ -->

    &ref_event;

    <!-- REF run/abort/running/remaining properties ====================================================== -->

    &ref_time;

    <!-- REF.ILC -======================================================================================== -->

    &ref_ilc;

    <!-- REF.FUNC ======================================================================================== -->

    &ref_func;

    <!-- REF.CYC ========================================================================================= -->

    &ref_cyc;

    <!-- REF.ERROR ======================================================================================= -->

    &ref_error;

    <!-- REF.INFO ======================================================================================== -->

    &ref_info;

    <!-- DIRECT properties =============================================================================== -->

    &ref_direct;

    <!-- DEFAULT properties ============================================================================== -->

    &ref_defaults;

    <!-- REF.RATE properties = Legacy properties for STARTING, STOPPING and going to ON_STANDBY ========== -->

    &ref_rate;

    <!-- REF.START ======================================================================================= -->

    &ref_start;

    <!-- REF.FIRST_PLATEAU =============================================================================== -->

    &ref_first_plateau;

    <!-- REF.EXTERNAL ==================================================================================== -->

    &ref_external;

    <!-- REF.REF.V_FEED_FWD ============================================================================== -->

    &ref_v_feed_fwd;

    <!-- REF.REF.V_HARMONICS ============================================================================= -->

    &ref_v_harmonics;

    <!-- REF.ECONOMY ===================================================================================== -->

    &ref_economy;

    <!-- REF.ARMED ======================================================================================= -->

    &ref_armed;

    <!-- LIBFG Reference Functions ======================================================================= -->

    &ref_ramp;
    &ref_pulse;
    &ref_plep;
    &ref_pppl;
    &ref_cubexp;
    &ref_table;
    &ref_trim;
    &ref_test;
    &ref_prbs;

</property>

<!-- EOF -->
