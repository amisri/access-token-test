<?xml version='1.0'?>

<property
    symbol  = "CALSYS"
    title   = "Current Calibration System properties"
    type    = "PARENT"
    get     = "Parent"
>
    <doc><![CDATA[
        Properties related to the Current Calibration System.
    ]]></doc>

    <property
        symbol  = "DCM"
        title   = "Data Connection Module properties"
        type    = "PARENT"
        get     = "Parent"
    >
        <doc><![CDATA[
            Properties related to the Data Connection Module.
        ]]></doc>

        <property
            symbol          = "INIT"
            title           = "DCM initialisation"
            type            = "INT16U"
            set             = "Integer"
            value_calsys    = ""
        >
            <doc><![CDATA[
                Setting this property will initialise the DCM flash.
            ]]></doc>
        </property>

        <property
            symbol          = "SELECT"
            title           = "DCM operation mode"
            group           = "CONFIG"
            type            = "INT8U"
            symlist         = "CALSYS_DCM_SELECT"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                This property specifies the operation mode of the DCM card.  It must be set when the card
                is installed in a new device.
            ]]></doc>
        </property>

        <property
            symbol  = "ADC"
            title   = "ADC properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the ADC.
            ]]></doc>

            <property
                symbol          = "CAL"
                title           = "ADC calibration state"
                type            = "INT16U"
                symlist         = "CALSYS_CAL"
                get             = "Integer"
                set             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the calibration state of the ADC.  Setting this property with
                    no parameters will trigger an ADC calibration.  The ADC calibration uses the value in
                    {PROP CALSYS.DCM.ADC.V_REF} to correct for the full scale calibration.  Since the ADC
                    channels are multiplexed, the calibration is valid for all channels.
                ]]></doc>
            </property>

            <property
                symbol          = "V_REF"
                title           = "10V voltage reference"
                type            = "FLOAT"
                get             = "Float"
                set             = "Float"
                limits          = "float"
                min             = "9"
                max             = "11"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the value in volts of the 10V voltage reference used for the
                    calibration of the ADC.
                ]]></doc>
            </property>

            <property
                symbol          = "RAW_ZERO"
                title           = "ADC zero calibration raw value"
                type            = "INT16S"
                get             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the raw value resulting from the zero calibration of the ADC.
                ]]></doc>
            </property>

            <property
                symbol          = "RAW_REF"
                title           = "ADC 10V calibration raw value"
                type            = "INT16S"
                get             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the raw value resulting from the calibration of the ADC with
                    the 10V voltage reference.  This value is the raw equivalent of the VREF value.
                ]]></doc>
            </property>

            <property
                symbol          = "VOLT"
                title           = "ADC voltage reading"
                type            = "FLOAT"
                get             = "Float"
                numels          = "16"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers an ADC reading and returns the value in volts.
                    In CDC mode, the ADC channels are differential and only 8 channels are available.
                    In SM mode, the ADC channels are single-ended and 16 channels are available.
                ]]></doc>
            </property>

            <property
                symbol          = "RAW"
                title           = "ADC raw value"
                type            = "INT16S"
                get             = "Integer"
                numels          = "16"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers an ADC reading and returns the raw value.
                    In CDC mode, the ADC channels are differential and only 8 channels are available.
                    In SM mode, the ADC channels are single-ended and 16 channels are available.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "PS05"
            title   = "5V power supply"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                5V power supply.
            ]]></doc>

            <property
                symbol          = "CAL"
                title           = "Calibration voltage"
                type            = "FLOAT"
                get             = "Float"
                set             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property specifies the voltage to use for calibration of the power supply.
                ]]></doc>
            </property>

            <property
                symbol          = "GAIN"
                title           = "ADC gain"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property reports the value of the power supply ADC gain.
                ]]></doc>
            </property>

            <property
                symbol          = "MEAS"
                title           = "Voltage measurement"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property reports the measured voltage of the power supply.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "PS15"
            title   = "15V power supply"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                15V power supply.
            ]]></doc>

            <property
                symbol  = "POS"
                title   = "Positive voltage"
                type    = "PARENT"
                get     = "Parent"
            >
                <doc><![CDATA[
                    Positive voltage.
                ]]></doc>

                <property
                    symbol          = "CAL"
                    title           = "Calibration voltage"
                    type            = "FLOAT"
                    get             = "Float"
                    set             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property specifies the voltage to use for calibration of the power supply.
                    ]]></doc>
                </property>

                <property
                    symbol          = "GAIN"
                    title           = "ADC gain"
                    type            = "FLOAT"
                    get             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property reports the value of the power supply ADC gain.
                    ]]></doc>
                </property>

                <property
                    symbol          = "MEAS"
                    title           = "Voltage measurement"
                    type            = "FLOAT"
                    get             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property reports the measured voltage of the power supply.
                    ]]></doc>
                </property>
            </property>

            <property
                symbol  = "NEG"
                title   = "Negative voltage"
                type    = "PARENT"
                get     = "Parent"
            >
                <doc><![CDATA[
                    Negative voltage.
                ]]></doc>

                <property
                    symbol          = "CAL"
                    title           = "Calibration voltage"
                    type            = "FLOAT"
                    get             = "Float"
                    set             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property specifies the voltage to use for calibration of the power supply.
                    ]]></doc>
                </property>

                <property
                    symbol          = "GAIN"
                    title           = "ADC gain"
                    type            = "FLOAT"
                    get             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property reports the value of the power supply ADC gain.
                    ]]></doc>
                </property>

                <property
                    symbol          = "MEAS"
                    title           = "Voltage measurement"
                    type            = "FLOAT"
                    get             = "Float"
                    numels          = "1"
                    value_calsys    = ""
                >
                    <doc><![CDATA[
                        This property reports the measured voltage of the power supply.
                    ]]></doc>
                </property>
            </property>
        </property>
    </property>

    <property
        symbol  = "CDC"
        title   = "CERN DCCT Calibrator properties"
        group   = "OPERATIONAL"
        type    = "PARENT"
        get     = "Parent"
    >
        <doc><![CDATA[
            Properties related to the CERN DCCT Calibrator.
        ]]></doc>

        <property
            symbol  = "STATUS"
            title   = "Status properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Status properties.
            ]]></doc>

            <property
                symbol          = "RELAYS"
                title           = "Status of relays"
                type            = "INT32U"
                flags           = "BIT_MASK"
                symlist         = "CALSYS_CDC_STATUS_RELAYS"
                get             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Status of relays.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "DAC1TW"
            title   = "DAC 1TW properties"
            group   = "CONFIG"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the DAC 1TW.
            ]]></doc>

            <property
                symbol          = "CAL"
                title           = "DAC 1TW calibration state"
                group           = "CONFIG"
                type            = "INT16U"
                symlist         = "CALSYS_CAL"
                get             = "Integer"
                set             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the calibration state of the DAC 1TW system.  Setting this
                    property with no parameters will trigger a calibration of channel 0 of the DAC.
                    The DAC calibration is not allowed in VB and requires the CDC to be on and at zero
                    current with no offset present.  The calibration of zero is done first, followed by
                    the calibration of the values corresponding to 10mA, which is measured by doing a
                    back-to-back between the DAC winding with the DAC at full-scale and the "calibration"
                    winding with the 10mA reference.
                ]]></doc>
            </property>

            <property
                symbol          = "ZERO"
                title           = "Voltage for zero current"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the value in volts of the DAC output that generates a zero
                    current in the DAC winding.
                ]]></doc>
            </property>

            <property
                symbol          = "FS"
                title           = "Voltage for 10mA current"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the value in volts of the DAC output that generates a 10mA
                    current in the DAC winding.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "RELAYS"
            title   = "CDC relay properties"
            group   = "CONFIG"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the CDC relays.
            ]]></doc>

            <property
                symbol          = "DELAY"
                title           = "Current step delay"
                type            = "INT32U"
                get             = "Integer"
                set             = "Integer"
                limits          = "int"
                min             = "100"
                max             = "10000"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This expert property contains the value of the delay between each CDC current step in
                    units of 100us.
                ]]></doc>
            </property>

            <property
                symbol          = "STEPSIZE"
                title           = "Current step size"
                type            = "INT32U"
                get             = "Integer"
                set             = "Integer"
                limits          = "int"
                min             = "1"
                max             = "100"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This expert property contains the value of the size of each CDC current step as a
                    percentage.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "NULL"
            title   = "CDC nulling properties"
            group   = "CONFIG"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the CDC nulling.
            ]]></doc>

            <property
                symbol          = "AUTO"
                title           = "Automatically null the offset"
                group           = "DEFAULT"
                type            = "INT16U"
                set             = "Integer"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Setting this property will trigger automatic nulling of the offset.
                ]]></doc>
            </property>

            <property
                symbol          = "KP"
                title           = "PI proportional gain"
                type            = "FLOAT"
                get             = "Float"
                set             = "Float"
                limits          = "float"
                min             = "0"
                max             = "100"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the proportional gain on the PI controller used for offset nulling.
                ]]></doc>
            </property>

            <property
                symbol          = "KI"
                title           = "PI integral gain"
                type            = "FLOAT"
                get             = "Float"
                set             = "Float"
                limits          = "float"
                min             = "0"
                max             = "100"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the integral gain on the PI controller used for offset nulling.
                ]]></doc>
            </property>

            <property
                symbol          = "NR"
                title           = "Number of offset nulling attempts"
                type            = "INT16U"
                get             = "Integer"
                set             = "Integer"
                limits          = "int"
                min             = "0"
                max             = "100"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the maximum number of offset nulling attempts (cycles ADC reading
                    / DAC setting).
                ]]></doc>
            </property>
        </property>

        <property
            symbol          = "RESET"
            title           = "Reset relay CPLD"
            group           = "DEFAULT"
            type            = "INT16U"
            set             = "Integer"
            value_calsys    = ""
        >
            <doc><![CDATA[
                Setting this property will trigger a CPLD reset in the primary relays card.
            ]]></doc>
        </property>

        <property
            symbol          = "OUTPUT"
            title           = "CDC output mode"
            type            = "INT16U"
            symlist         = "CALSYS_CDC_OUTPUT"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                This property contains the output mode of the CDC.  Setting this property switches the current
                calibrator off or on according to the value set.  It there is current present when the property
                is set to OFF, then the current will be set to zero before the CDC is switched-off.  In the case
                of a command to switch the CDC on, the CDC status is first verified.  If there are any active
                alarms, then the action is not performed.  Once an on command has been accepted and the relay
                configuration has been set, the CDC offset current is nulled.  This operation may require up to
                5 seconds.
            ]]></doc>
        </property>

        <property
            symbol          = "I_REF"
            title           = "CDC output current"
            type            = "FLOAT"
            get             = "Float"
            set             = "Float"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                This property contains the CDC current.
            ]]></doc>
        </property>

        <property
            symbol  = "ADC"
            title   = "ADC properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the ADC.
            ]]></doc>

            <property
                symbol          = "GAIN_1A"
                title           = "ADC gain"
                type            = "FLOAT"
                limits          = "float"
                min             = "0.5"
                max             = "1.5"
                get             = "Float"
                set             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Setting this property will trigger the calibration of the ADC channel which reads the output current.
                    This calibration can only be performed at 1A.  Getting this property will return the gain resulting
                    from the calibration
                ]]></doc>
            </property>

            <property
                symbol          = "I_MEAS"
                title           = "CDC output current measurement"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers a read of the ADC channel which measures the output current
                    of the CDC and returns a value in amps.
                ]]></doc>
            </property>

            <property
                symbol          = "V_MEAS"
                title           = "CDC output voltage measurement"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >   
                <doc><![CDATA[
                    Accessing this property triggers a read of the ADC channel which measures the output voltage
                    of the CDC and returns a value in volts.  This measurement includes the voltage across the
                    secondary windings of the CDC and across the current sense resistor.
                ]]></doc>
            </property>

            <property
                symbol          = "I_NULL_UA"
                title           = "CDC offset current measurement"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >   
                <doc><![CDATA[
                    Accessing this property triggers a read of the ADC channel which measures the offset current
                    of the CDC and returns a value in microamps.
                ]]></doc>
            </property>

            <property
                symbol          = "PBC_TEMP"
                title           = "CDC PBC temperature"
                type            = "FLOAT"
                get             = "Float"
                numels          = "1"
                value_calsys    = ""
            >   
                <doc><![CDATA[
                    Accessing this property triggers a read of the ADC channel which measures the PBC temperature
                    of the CDC and returns a value in Celsius.
                ]]></doc>
            </property>
        </property>

        <property
            symbol  = "DAC"
            title   = "DAC properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the DAC.
            ]]></doc>

            <property
                symbol          = "VOLT"
                title           = "DAC voltage"
                type            = "FLOAT"
                get             = "Float"
                set             = "Float"
                numels          = "2"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the value of the DAC output in volts.
                ]]></doc>
            </property>

            <property
                symbol          = "RAW"
                title           = "DAC raw value"
                type            = "INT16S"
                get             = "Integer"
                numels          = "2"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    This property contains the value of the DAC output in raw units.
                ]]></doc>
            </property>
        </property>
    </property>

    <property
        symbol  = "SM"
        title   = "Switching Matrix properties"
        group   = "OPERATIONAL"
        type    = "PARENT"
        get     = "Parent"
    >
        <doc><![CDATA[
            Properties related to the Switching Matrix.
        ]]></doc>

        <property
            symbol  = "STATUS"
            title   = "Status properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Status properties.
            ]]></doc>

            <property
                symbol          = "RELAYS"
                title           = "Status of relays"
                type            = "INT16U"
                flags           = "BIT_MASK"
                symlist         = "CALSYS_SM_STATUS_RELAYS"
                get             = "Integer"
                numels          = "1"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Status of relays.
                ]]></doc>
            </property>
        </property>

        <property
            symbol          = "OUTPUT"
            title           = "Switching matrix output state"
            type            = "INT16U"
            symlist         = "CTRL"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                Setting this property to ENABLED makes the switching matrix ready for calibration (i.e.
                all relays closed except the general bypass, which is open.  All select relays are open).
                This operation cannot be performed if there is current in the calibration circuit.  Setting
                this property to DISABLED makes the switching matrix safe (i.e. all bypass relays closed
                including the general bypass.  All select relays are open).
            ]]></doc>
        </property>

        <property
            symbol          = "RESET"
            title           = "Reset relays"
            group           = "DEFAULT"
            type            = "INT16U"
            set             = "Integer"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                Setting this property resets all of the relays in the switching matrix.  This is equivalent
                to going to the standby position, but with no regard to the present status of the switching
                matrix, which means that there is no verification of the current level and the switch is
                made abruptly.  This property should only be used by experts.
            ]]></doc>
        </property>

        <property
            symbol          = "DCCT"
            title           = "Switching matrix DCCT state"
            type            = "INT16U"
            symlist         = "CTRL"
            get             = "Integer"
            set             = "Integer"
            numels          = "8"
            value_calsys    = ""
        >
            <doc><![CDATA[
                This property controls the state of the switching matrix DCCT outputs.  When set to ENABLED,
                the VB selection relay, DCCT selection relay and bypass relays will be set to the required
                position.  This operation can only be performed when the current is zero and may take up to
                5 seconds.  A maximum of two channels may be active at the same time.
            ]]></doc>
        </property>

        <property
            symbol          = "MODE"
            title           = "Switching matrix mode"
            group           = "DEFAULT"
            type            = "INT16U"
            limits          = "int"
            min             = "1"
            max             = "2"
            get             = "Integer"
            set             = "Integer"
            numels          = "1"
            value_calsys    = ""
        >
            <doc><![CDATA[
                Setting this property sets the VB selection relay in either the single or double channel
                position.
            ]]></doc>
        </property>

        <property
            symbol  = "ADC"
            title   = "ADC properties"
            type    = "PARENT"
            get     = "Parent"
        >
            <doc><![CDATA[
                Properties related to the ADC.
            ]]></doc>

            <property
                symbol          = "TEMP"
                title           = "Temperature reading"
                type            = "FLOAT"
                get             = "Float"
                numels          = "8"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers a read of the ADC channel which measures the temperature
                    and returns a value in Celsius.
                ]]></doc>
            </property>

            <property
                symbol          = "V_MEAS"
                title           = "Output voltage reading"
                type            = "FLOAT"
                get             = "Float"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers reading of the output voltage of the VB and returns the
                    value in volts.
                ]]></doc>
            </property>

            <property
                symbol          = "I_NULL_UA"
                title           = "Offset current reading"
                type            = "FLOAT"
                get             = "Float"
                value_calsys    = ""
            >
                <doc><![CDATA[
                    Accessing this property triggers reading of the offset current on the calibration circuit
                    and returns the value in microamps.  The value saturates from about 10mA (2 PPM).
                ]]></doc>
            </property>
        </property>
    </property>
</property>

<!-- EOF -->
