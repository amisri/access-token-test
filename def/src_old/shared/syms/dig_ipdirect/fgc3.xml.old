    <const
        name    = "FGCPSUFLT"
        value   = "*MCU.DIG_IPDIRECT_FGCPSUFLT_MASK32"
        title   = "PSU FGC fault"
    >
        <doc><![CDATA[
            If set, the FGC PSU is reporting a fault.
        ]]></doc>
    </const>

    <const
        name    = "DCCTPSUFLT"
        value   = "*MCU.DIG_IPDIRECT_DCCTPSUFLT_MASK32"
        title   = "PSU DCCT fault"
    >
        <doc><![CDATA[
            If set, the DCCT measurement PSU is reporting a fault.
        ]]></doc>
    </const>

    <const
        name    = "VDCFLT"
        value   = "*MCU.DIG_IPDIRECT_VDCFLT_MASK32"
        title   = "VDC fault"
    >
        <doc><![CDATA[
             If set, the VDC is reporting a fault.
        ]]></doc>
    </const>

    <const
        name    = "PWRFAILURE"
        value   = "*MCU.DIG_IPDIRECT_PWRFAILURE_MASK32"
        title   = "Powering Failure interlock signal readback"
    >
        <doc><![CDATA[
            This is the readback on the power failure signal, sent
            to the PIC.
        ]]></doc>
    </const>

    <const
        name    = "ZEROINOT"
        value   = "*MCU.DIG_IPDIRECT_ZEROINOT_MASK32"
        title   = "NOT Zero I"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const>

    <const
        name    = "OPBLOCKED"
        value   = "*MCU.DIG_IPDIRECT_OPBLOCKED_MASK32"
        title   = "Converter output blocked"
    >
        <doc><![CDATA[
            If set, the output power stage is enabled.
        ]]></doc>
    </const>

    <const
        name    = "POLSWINEGNOT"
        value   = "*MCU.DIG_IPDIRECT_POLSWINEGNOT_MASK32"
        title   = "Polarity switch not negative"
    >
        <doc><![CDATA[
            If set, polarity switch is not at negative position.
        ]]></doc>
    </const>

    <const
        name    = "POLSWIPOSNOT"
        value   = "*MCU.DIG_IPDIRECT_POLSWIPOSNOT_MASK32"
        title   = "Polarity switch not positive"
    >
        <doc><![CDATA[
            If set, polarity switch is not at positive position.
        ]]></doc>
    </const>

    <const
        name    = "DCCTBFLT"
        value   = "*MCU.DIG_IPDIRECT_DCCTBFLT_MASK32"
        title   = "DCCT B Fault"
    >
        <doc><![CDATA[
            If set, the DCCT channel B is reporting a fault.
        ]]></doc>
    </const>

    <const
        name    = "DCCTAFLT"
        value   = "*MCU.DIG_IPDIRECT_DCCTAFLT_MASK32"
        title   = "DCCT A Fault"
    >
        <doc><![CDATA[
            If set, the DCCT channel A is reporting a fault.
        ]]></doc>
    </const>

    <const
        name    = "VSNOCABLE"
        value   = "*MCU.DIG_IPDIRECT_VSNOCABLE_MASK32"
        title   = "Voltage source cable missing"
    >
        <doc><![CDATA[
            If set, the FGC-VS cable is disconnected.
        ]]></doc>
    </const>

    <const
        name    = "FASTABORT"
        value   = "*MCU.DIG_IPDIRECT_FASTABORT_MASK32"
        title   = "Fast Power Abort (memorised by VS)"
    >
        <doc><![CDATA[
            If set, the voltage source has received the PC_FAST_ABORT signal.
        ]]></doc>
    </const>

    <const
        name    = "VSFAULT"
        value   = "*MCU.DIG_IPDIRECT_VSFAULT_MASK32"
        title   = "Voltage source Fault"
    >
        <doc><![CDATA[
            If set, the voltage source has an internal fault.
        ]]></doc>
    </const>

    <const
        name    = "VSEXTINTLK"
        value   = "*MCU.DIG_IPDIRECT_VSEXTINTLK_MASK32"
        title   = "Voltage source External Interlock"
    >
        <doc><![CDATA[
            If set, the voltage source has an external fault.
        ]]></doc>
    </const>

    <const
        name    = "VSREADYNOT"
        value   = "*MCU.DIG_IPDIRECT_VSREADYNOT_MASK32"
        title   = "Voltage source not ready"
    >
        <doc><![CDATA[
            If not set, the voltage source is ready.
        ]]></doc>
    </const>

    <const
        name    = "VSPOWERON"
        value   = "*MCU.DIG_IPDIRECT_VSPOWERON_MASK32"
        title   = "Voltage source Power On"
    >
        <doc><![CDATA[
            If set, the voltage source power stage is powered.
        ]]></doc>
    </const>

    <const
        name    = "VSRESERVED"
        value   = "*MCU.DIG_IPDIRECT_VSRESERVED_MASK32"
        title   = "Voltage source reserved"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const>

    <const
        name    = "DISCHARGERQ"
        value   = "*MCU.DIG_IPDIRECT_DISCHARGERQ_MASK32"
        title   = "Discharge request (mem)"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "EXTRESET"
        value   = "*MCU.DIG_IPDIRECT_EXTRESET_MASK32"
        title   = "External reset"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "RFSTATUS"
        value   = "*MCU.DIG_IPDIRECT_RFSTATUS_MASK32"
        title   = "RF status"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "MSTRSLVSTS"
        value   = "*MCU.DIG_IPDIRECT_MSTRSLVSTS_MASK32"
        title   = "Master Slave Status"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "POLSWILOCKED"
        value   = "*MCU.DIG_IPDIRECT_POLSWILOCKED_MASK32"
        title   = "Polarity switch locked"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "POLSWIFLT"
        value   = "*MCU.DIG_IPDIRECT_POLSWIFLT_MASK32"
        title   = "Polarity switch Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "PULSEPERMIT"
        value   = "*MCU.DIG_IPDIRECT_PULSEPERMIT_MASK32"
        title   = "Pulse Permit"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "NORMALOP"
        value   = "*MCU.DIG_IPDIRECT_NORMALOP_MASK32"
        title   = "Normal operation"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "PCFLT"
        value   = "*MCU.DIG_IPDIRECT_PCFLT_MASK32"
        title   = "PC Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "INCOHERENCE"
        value   = "*MCU.DIG_IPDIRECT_INCOHERENCE_MASK32"
        title   = "Incoherence"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "EARTHFLT"
        value   = "*MCU.DIG_IPDIRECT_EARTHFLT_MASK32"
        title   = "Earth Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "TEMPFLT"
        value   = "*MCU.DIG_IPDIRECT_TEMPFLT_MASK32"
        title   = "Temperature Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "LIMITFLT"
        value   = "*MCU.DIG_IPDIRECT_LIMITFLT_MASK32"
        title   = "Limit Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "DCFLT"
        value   = "*MCU.DIG_IPDIRECT_DCFLT_MASK32"
        title   = "DC Fault"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const><const
        name    = "ACFLT"
        value   = "*MCU.DIG_IPDIRECT_ACFLT_MASK32"
        title   = "AC Fault [Commercial] / Dispatch FGC Fault [Commercial Hi Volt]"
    >
        <doc><![CDATA[
            
        ]]></doc>
    </const>

<!-- EOF -->
