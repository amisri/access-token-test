<const
    name        = "FW_DIODE"
    value       = "0x1000"
    title       = "Voltage source Free Wheel Diode fault"
    action      = "Send PO piquet to converter to record which diode has failed."
    cause       = "Free Wheel Diode failure detected."
    consequence = "Converter cannot be restarted until an authorised person has reset VS.FW_DIODE property."
>
    <doc><![CDATA[
        The LHC 1-Quadrant converters have a free wheel diode system that is
        monitored for diode failures.  A diode can fail in short circuit or open circuit.
        A short circuit can be detected at any time, but an open circuit is only visible
        during a circuit discharge.  For this reason, if the converter reports a free wheel diode
        failure via the diagnostic system then the FW_DIODE fault will be asserted.  This fault
        can only be cleared by an authorised individual resetting {PROP VS.FW_DIODE}.  Before
        doing this, someone must visit the converter to record which diode has failed as this is
        only visible locally.
    ]]></doc>
</const>

<!-- EOF -->
