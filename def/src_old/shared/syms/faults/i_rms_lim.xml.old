<const
    name        = "I_RMS_LIM"
    value       = "0x2000"
    title       = "RMS current limit exceeded fault level"
    action      = "Change supercycle to reduce RMS current demand."
    cause       = "FGC has detected that the RMS current exceeds either the converter or the load fault limit."
    consequence = "The converter will stop operating."
>
    <doc><![CDATA[
        <p>This fault indicates that the FGC has detected that the filtered RMS current
        is exceeding either the converter to the load fault threshold.
        The load includes the cables and the magnet and the protection should
        match the more vulnerable of these two.</p>
        <p>The RMS current is filtered with two different first-order filters, one
        for the converter fault and fault limits and the other for the load
        fault and fault limits. The filter time constants are defined in 
        {PROP LIMITS.I.RMS_TC} and {PROP LIMITS.I.LOAD.RMS_TC}[{PROP LOAD.SELECT}].
        Setting a time constant to zero will deactivate the calculation of that RMS
        and the associated fault and fault limits.</p>
        <p>The converter RMS fault limit is specified in {PROP LIMITS.I.RMS_FAULT} and 
        the load limit in {PROP LIMITS.I.LOAD.RMS_FAULT}[{PROP LOAD.SELECT}].</p>
        <p>Setting a fault limit to zero will deactivate that fault.</p>
    ]]></doc>
</const>

<!-- EOF -->
