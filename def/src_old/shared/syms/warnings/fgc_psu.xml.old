<const
    name        = "FGC_PSU"
    value       = "0x0010"
    title       = "FGC PSU failure"
    action      = "Replace the failed power supply when possible."
    cause       = "The FGC Tri-volt power supply has partially failed or is out of tolerance."
    consequence = "Operation can continue but redundancy may have been lost - a subsequent failure might stop operation."
>
    <doc><![CDATA[
        This warning indicates that the FGC power supply might be:
        <ul>
        <li>Reporting the loss of redundant operation.  In this case the FGC_PSU_FAIL bit will be
            set in {PROP STATUS.ST_LATCHED}.  Change the PSU as soon as possible as a subsequent failure
            will stop operation of the converter.</li>
        <li>Reporting that the -48VDC input voltage has dropped below 36V.  If this is a momentary
            droppout then operation may survive as the 48VDC power module has enough energy storage
            to survive the loss of 1 phase for 100ms.  In this ase the VDC_FAIL bit will be
            set in {PROP STATUS.ST_LATCHED}.</li>
        <li>Supplying voltages that are out of limits according to the FGC.
            In this case the PSU_V_FAIL bit will be set in {PROP STATUS.ST_LATCHED}.  This could
            be a measurement problem in the FGC so swap the FGC/PSU to identify which is at fault.
            The PSU voltage measurements can be check in {PROP FGC.PSU_FGC} or with the
            command "S {PROP RTD} PSU" in a terminal window.</li>
        </ul>
    ]]></doc>
</const>

<!-- EOF -->
