    <const
        name    = "DCCT_A_FLT"
        value   = "*MCU.DIG_IP_DCCTAFLT_MASK32"
        title   = "Dcct A fault"
    >
        <doc><![CDATA[
            This signal indicates that DCCT A is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
        ]]></doc>
    </const>

    <const
        name    = "DCCT_B_FLT"
        value   = "*MCU.DIG_IP_DCCTBFLT_MASK32"
        title   = "Dcct B fault"
    >
        <doc><![CDATA[
            This signal indicates that DCCT B is in a fault state.
        If both DCCTs report a fault at the same time, the
        digital interface will latch both signals.
        ]]></doc>
    </const>

    <const
        name    = "FAST_ABORT"
        value   = "*MCU.DIG_IP_FASTABORT_MASK32"
        title   = "Fast power abort interlock"
    >
        <doc><![CDATA[
        This signal is the latched copy of the FAST_PA interlock
        signal that is received by the voltage source.
        ]]></doc>
    </const>

    <const
        name    = "INTLKSPARE"
        value   = "*MCU.DIG_IP_INTLKSPARE_MASK32"
        title   = "Spare interlock input"
    >
        <doc><![CDATA[
        The FGC has two opto-coupler inputs for interlock signals.  
        One is dedicated to the PC_PERMIT signal and the other is
        spare.  This bit indicates the state of this spare input.
        ]]></doc>
    </const>

    <const
        name    = "OP_BLOCKED"
        value   = "*MCU.DIG_IP_OPBLOCKED_MASK32"
        title   = "Output blocked status"
    >
        <doc><![CDATA[
            This signal reports the status of the output blocked signal from the voltage source.
        ]]></doc>
    </const>

    <const
        name    = "PC_PERMIT"
        value   = "*MCU.DIG_IP_PCPERMIT_MASK32"
        title   = "Power converter permit interlock"
    >
        <doc><![CDATA[
        This interlock signal is received from the PIC.  If it is
        inactive, the SLOW_ABORT signal is set.
        ]]></doc>
    </const>

    <const
        name    = "POL_SWI_NEG"
        value   = "*MCU.DIG_IP_POLSWINEG_MASK32"
        title   = "Polarity switch negative"
    >
        <doc><![CDATA[
            This signal indicates that the polarity switch is
        in the negative position.
        ]]></doc>
    </const>

    <const
        name    = "POL_SWI_POS"
        value   = "*MCU.DIG_IP_POLSWIPOS_MASK32"
        title   = "Polarity switch positive"
    >
        <doc><![CDATA[
            This signal indicates that the polarity switch is
        in the positive position.
        ]]></doc>
    </const>

    <const
        name    = "PWR_FAILURE"
        value   = "*MCU.DIG_IP_PWRFAILURE_MASK32"
        title   = "Readback of Powering Failure interlock signal"
    >
        <doc><![CDATA[
        This is the readback on the power failure signal, sent
        to the PIC.
        ]]></doc>
    </const>

    <const
        name    = "VS_EXTINTLK"
        value   = "*MCU.DIG_IP_VSEXTINTLK_MASK32"
        title   = "Voltage source external interlock"
    >
        <doc><![CDATA[
        This signal indicates that the voltage source cannot
        operate because an external interlock signal is active.
        (E.g. lack of water cooling).
        ]]></doc>
    </const>

    <const
        name    = "VS_FAULT"
        value   = "*MCU.DIG_IP_VSFAULT_MASK32"
        title   = "Voltage source fault"
    >
        <doc><![CDATA[
        This signal indicates that the voltage source cannot
        operate because an internal fault is present.
        ]]></doc>
    </const>

    <const
        name    = "VS_NO_CABLE"
        value   = "*MCU.DIG_IP_VSNOCABLE_MASK32"
        title   = "Cable to voltage source not connected"
    >
        <doc><![CDATA[
            The voltage source cable includes a sense wire to detect if
            it is connected at both ends.
        ]]></doc>
    </const>

    <const
        name    = "VS_POWER_ON"
        value   = "*MCU.DIG_IP_VSPOWERON_MASK32"
        title   = "Voltage source input power on"
    >
        <doc><![CDATA[
        This signal indicates that the voltage input power
        is on.  This may be a circuit breaker or an electronic
        switch, according to the type of converter.
        ]]></doc>
    </const>

    <const
        name    = "VS_READY"
        value   = "*MCU.DIG_IP_VSREADY_MASK32"
        title   = "Voltage loop OK"
    >
        <doc><![CDATA[
            This signal indicates that the voltage source has completed
            its start-up sequence and is ready.
        ]]></doc>
    </const>

    <const
        name    = "VS_RUN"
        value   = "*MCU.DIG_IP_VSRUN_MASK32"
        title   = "Readback of VS_RUN command"
    >
        <doc><![CDATA[
            This is the readback of the VS_RUN command.
        ]]></doc>
    </const>

    <const
        name    = "PULSEPERMIT"
        value   = "*MCU.DIG_IP_PULSEPERMIT_MASK32"
        title   = "Readback of VS_RUN command"
    >
        <doc><![CDATA[
            This signal indicates that there is a pulse permit.
        ]]></doc>
    </const>

    <const
        name    = "POLSWILOCKED"
        value   = "*MCU.DIG_IP_POLSWILOCKED_MASK32"
        title   = "Polarity Switch Locked"
    >
        <doc><![CDATA[
            This signal indicates that the polarity switch is locked.
        ]]></doc>
    </const>

    <const
        name    = "POLSWIFLT"
        value   = "*MCU.DIG_IP_POLSWIFLT_MASK32"
        title   = "Polarity Switch Fault"
    >
        <doc><![CDATA[
            This signal indicates a polarity switch fault.
        ]]></doc>
    </const>

<!-- EOF -->
