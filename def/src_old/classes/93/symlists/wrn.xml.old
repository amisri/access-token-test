<?xml version='1.0'?>

<symlist
    title   = "Class 93 (ISEG) Warnings"
    meaning = "WARNING"
>
    <doc><![CDATA[
	This bitmask symbol list is used to identify all the warnings
	that may be reported by class 93 (ISEG).
	All warnings are linked to alarms in the LASER alarm system.
	]]></doc>

    <const
	    name        = "MODULE_WARNING"
	    value       = "0x0001"
	    title       = "ISEG module hardware warning"
	    action      = "Check status of the module."
	    cause       = "Hardware problem in the module."
	    consequence = "Warning."
	>
	    <doc><![CDATA[
	        The power converter's iseg module has a hardware warning.
	    ]]></doc>    
    </const>
    
    <const
	    name        = "TEMPERATURE"
	    value       = "0x0002"
	    title       = "Temperature warning."
	    action      = "Check the status of the ISEG's hardware."
	    cause       = "High temperature detected in the module."
	    consequence = "Warning."
	>
	    <doc><![CDATA[
	        High temperature detected in the module. 
	    ]]></doc>      
    </const>
    
    <const
	    name        = "VS_WARNING"
	    value       = "0x0004"
	    title       = "Voltage source warning"
	    action      = "Check the status of the ISEG's hardware."
	    cause       = "The voltage source warning due to internal conditions."
	    consequence = "Warning."
	>
	    <doc><![CDATA[
	        The voltage source warning due to an internal condition. For example: temperature, service needed, bad input command, etc.
	    ]]></doc>     
    </const>
    
    <const
        name        = "V_MEAS"
        value       = "0x0008"
        title       = "Voltage measurement is invalid."
        action      = "Check the state of the channel's module."
        cause       = "The module is not measuring correctly the voltage."
        consequence = "Converter stopped and cannot be restarted until fault has been reset."
    >
        <doc><![CDATA[
            This fault indicates that the voltage measurement is invalid and cannot be correctly interpreted.
        ]]></doc>
    </const> 
    
	<const
	    name        = "CONFIG"
	    value       = "0x0010"
	    title       = "Invalid or changed configuration"
	    action      = "Check configuration parameters and correct bad value or synchronise the FGCD or DB."
	    cause       = "Configuration is inconsistent or has failed."
	    consequence = "Converter behaviour may be impaired."
	>
	    <doc><![CDATA[
	        This warning indicates there may be problem concerned with the configuration of the ISEG HW.
	    ]]></doc>
	</const>  
    
    <const
	    name        = "SIMULATION"
	    value       = "0x0020"
	    title       = "Simulated voltage source"
	    priority    = "3"
	    action      = "No action required."
	    cause       = "FGC is simulating power converter."
	    consequence = "The real power converter cannot be operated."
	>
	    <doc><![CDATA[
	        This warning indicates that FGC is simulating the
	        behaviour of the voltage source. Operation may appear
	        completely normal despite the fact that the circuit is
	        not powered.
	    ]]></doc>
    </const>
    
	<const
	    name        = "SERVICE_NEEDED"
	    value       = "0x0040"
	    title       = "Service/Maintenance needed on module."
	    action      = "Replace module."
	    cause       = "Service needed. Hardware failure."
	    consequence = "Warning."
	>
	    <doc><![CDATA[
	        This warning indicates that the channel's module needs service or maintenance, and needs to be replaced.
	    ]]></doc>
	</const>  
	
	<const
	    name        = "INPUT_ERROR"
	    value       = "0x0080"
	    title       = "Invalid input sent to power converter"
	    action      = "No action is required."
	    cause       = "Invalid input"
	    consequence = "Warning."
	>
	    <doc><![CDATA[
	        This warning indicates that a bad input data has been sent to the power converter.
	    ]]></doc>
	</const>  
	
	<const
        name        = "COMMS"
        value       = "0x0100"
        title       = "Communication fault"
        action      = "Check that the ISEG Converter is powered and that the Ethernet bus operates correctly."
        cause       = "The ISEG Converter is not responding on the Ethernet bus."
        consequence = "The power converter cannot be controlled."
    >
        <doc><![CDATA[
            This fault indicates that communication with the ISEG Converter over the Ethernet bus has been lost.
        ]]></doc>
    </const>
    
    <const
        name        = "TIMING_EVT"
        value       = "0x0200"
        title       = "PPM timing event warning"
        action      = "Check the MTG operation and configuration. Check the device pulse length and the device voltage rate."
        cause       = "Device failed to reach the required voltage in cycling operation."
        consequence = "The converter will continue operation but a bad voltage level might have been delivered to the load during the event."
    >
        <doc><![CDATA[
            This warning indicates that the device failed to reach the required voltage for the event in cycling
            operation. It means that the event warning time is not enough to allow the previous pulse to finish and bring the voltage
            to the PPM voltage reference (REF.PULSE.REF) in time. It can be caused by a too short distance between the timing events, by too long pulses
            (REF.PULSE.DURATION), or by a slow voltage rate.
        ]]></doc>
    </const>
	

</symlist>

<!-- EOF -->
