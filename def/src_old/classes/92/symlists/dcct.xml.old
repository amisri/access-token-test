<?xml version='1.0'?>

<symlist
    title   = "DCCT Status"
>
    <doc><![CDATA[
	The DCCT status provides details of the state a DCCT.  Five different types
	of DCCT are used in LHC power converters, and only one of them produces all the status signals.
	This table shows which signals are available according to the type of converter:
	<p>
	<table class="embedded" style="width: 100%">
	<tr>
	<th class="alt2">DCCT</th>
	<th class="alt2" width="16%">Ritz<br>120A</th>
	<th class="alt2" width="16%">Hitec<br>600A</th>
	<th class="alt2" width="16%">Hitec<br>4-13kA</th>
	<th class="alt2" width="16%">Hitec<br>800A-8kA</th>
	<th class="alt2" width="16%">Holec<br>SH 20kA</th>
	</tr>
	<tr>
	<th class="alt2" height="45">Converters</th>
	<td class="alt2">&plusmn;60A<br>&plusmn;120A</td>
	<td class="alt2">&plusmn;600A &plusmn;10V<br>&plusmn;600A &plusmn;40V</td>
	<td class="alt2">4-13kA<br>Atlas solenoid</td>
	<td class="alt2">LHCb dipole<br>Alice dipole</td>
	<td class="alt2">Atlas toroid<br>CMS solenoid </td>
	</tr>
	<tr>
	<th class="alt2" height="45">ZERO_I</th>
	<td class="alt2">No</td>
	<td class="alt2">Yes<br>&lt; ~0.01%</td>
	<td class="alt2">Yes<br>&lt; ~0.01%</td>
	<td class="alt2">Yes<br>&lt; ~0.1%</td>
	<td class="alt2">Yes<br>&lt; ~0.02%</td>
	</tr>
	<tr>
	<th class="alt2" height="45">NO_HEAD</th>
	<td class="alt2">No</td>
	<td class="alt2">Yes</td>
	<td class="alt2">Yes</td>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	</tr>
	<tr>
	<th class="alt2" height="45">FAULT</th>
	<td class="alt2">Yes</td>
	<td class="alt2">Yes</td>
	<td class="alt2">Yes</td>
	<td class="alt2">Yes</td>
	<td class="alt2">Yes</td>
	</tr>
	<tr>
	<th class="alt2" height="45">TEMP_FLT</th>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	<td class="alt2">Yes</td>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	</tr>
	<tr>
	<th class="alt2" height="45">LOW_I*</th>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	<td class="alt2">Yes<br>&lt; 1-10%</td>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	</tr>
	<tr>
	<th class="alt2" height="45">MCB_OFF</th>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	<td class="alt2">Yes</td>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	</tr>
	<tr>
	<th class="alt2" height="45">Dallas device in<br>DCCT electronics</th>
	<td class="alt2">No</td>
	<td class="alt2">ID +<br>Temp</td>
	<td class="alt2">ID +<br>Temp</td>
	<td class="alt2">ID +<br>Temp</td>
	<td class="alt2">ID +<br>Temp</td>
	</tr>
	<tr>
	<th class="alt2" height="45">Dallas device in<br>DCCT head</th>
	<td class="alt2">No&nbsp;</td>
	<td class="alt2">ID only</td>
	<td class="alt2">ID +<br>Temp</td>
	<td class="alt2">No</td>
	<td class="alt2">No</td>
	</tr>
	</table></p>
	<p>* The LOW_I was found to be of no practical use and is not mapped to a bit in the
	DCCT status property.</p>
    ]]></doc>

<!-- The bits DCCT_ZERO_I, DCCT_NO_HEAD, DCCT_TEMP_FLT, DCCT_LOW_I, DCCT_MCB_OFF are received in this -->
<!-- order from the DIM on the FGC DIPS interface board.  The bit assignments must not be changed.    -->
<!-- DCCT_FAULT is received via the digital interface and is reported in this bit mask.               -->
<!-- DCCT_LOW_I (0x10) is not interesting and is not used in the symbol list.                         -->

    <const
	name    = "ZERO_I"
        value   = "0x01"
	title   = "DCCT zero current reported"
    >
	<doc><![CDATA[
		This flag indicates that the DCCT is reporting zero current.
		Not implemented in FGClite.
	]]></doc>
    </const>

    <const
	name    = "NO_HEAD"
        value   = "0x02"
	title   = "No DCCT head connected"
    >
	<doc><![CDATA[
		This flag indicates that the DCCT is reporting that its	head is not connected.
		Not implemented in FGClite.
	]]></doc>
    </const>

    <const
	name    = "FAULT"
        value   = "0x04"
	title   = "DCCT global fault reported"
    >
	<doc><![CDATA[
                This flag indicates that the DCCT is reporting a global fault and the current
                measurement cannot be used. This value is taken from the DCCTA_FLT and DCCT_FLT
                bits in the CONVERTER_INPUT field in the FGClite critical data.
	]]></doc>
    </const>

    <const
	name    = "TEMP_FLT"
        value   = "0x08"
	title   = "DCCT temperature fault reported"
    >
	<doc><![CDATA[
                This flag indicates that the DCCT is reporting that its temperature is out of the
                safe range and the measurement of the current may not be accurate. This value is
                read from the CCLIBS status.
	]]></doc>
    </const>

    <const
	name    = "MEAS_OK"
        value   = "0x10"
	title   = "DCCT measurement valid"
    >
	<doc><![CDATA[
		Not implemented or used in class 51 or class 92.
	]]></doc>
    </const>

    <const
	name    = "MCB_OFF"
        value   = "0x20"
	title   = "DCCT main circuit breaker open reported"
    >
	<doc><![CDATA[
		This flag indicates that the DCCT is reporting that its main circuit breaker is open.
                Not implemented in FGClite.
	]]></doc>
    </const>

    <const
	name    = "ID_FLT"
        value   = "0x40"
	title   = "DCCT ID fault"
    >
	<doc><![CDATA[
                This flag indicates that the Dallas ID for the DCCT head or electronics was expected
                and is either not detectable or is not responding correctly. This value is read from
                the 1-wire subsystem.
	]]></doc>
    </const>

    <const
	name    = "CAL_FLT"
        value   = "0x80"
	title   = "DCCT calibration fault"
    >
	<doc><![CDATA[
                This flag indicates that the calibration values for the DCCT are invalid. This value
                is read from the CCLIBS status.
	]]></doc>
    </const>
</symlist>

<!-- EOF -->
