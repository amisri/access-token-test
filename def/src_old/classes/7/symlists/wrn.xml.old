<?xml version='1.0'?>

<!DOCTYPE warnings [
    <!ENTITY fgcd_warnings  SYSTEM "platforms/fgcd/shared_syms/warnings.xml">
]>

<symlist
    title   = "FGC gateway warnings"
    meaning = "WARNING"
>
    <doc><![CDATA[
	This bitmask symbol list is used to identify the warnings that may be reported by an FGC gateway.
    ]]></doc>

    &fgcd_warnings;
    
    <const
        name        = "COMMS_ERRORS"
        title       = "Ethernet fieldbus fault"
        value       = "0x0001"
        action      = "If the error persists check that the crate controller is powered, the crate is ON, check the Ethernet cable and the configuration."
        cause       = "The fieldbus is not in a proper running condition, the crate is OFF, or the Ethernet configuration is bad."
        consequence = "Communication is not possible with the ISEG crate over the Ethernet fieldbus."
    >
        <doc><![CDATA[
            Errors during the Ethernet communication with the crate controller. 
        ]]></doc>
    </const>
    
    <const
        name        = "CRATE_SERVICE_NEEDED"
        title       = "Service is needed for the crate"
        value       = "0x0020"
        action      = "Replace the crate."
        cause       = "Hardware failure in the crate."
        consequence = "Operation of the installed power converters is not possible."
    >
        <doc><![CDATA[
            Crate needs service. Hardware failure detected. 
        ]]></doc>
    </const>
    
    <const
        name        = "SAFETY_LOOP"
        title       = "At least one module's safety loop is opened."
        value       = "0x0040"
        action      = "Fix the cause of the aperture of the safety loop."
        cause       = "At least one module's safety loop has opened: external interlock."
        consequence = "Operation of the module's power converters is not possible."
    >
        <doc><![CDATA[
            At least one safety loop is open due to an external interlock.
        ]]></doc>
    </const>

    <const
        name        = "CRATE_TEMPERATURE"
        title       = "High temperature in the crate."
        value       = "0x0080"
        action      = "Check the status of the crate and the fans."
        cause       = "Temperature too high, fans not working properly."
        consequence = ""
    >
        <doc><![CDATA[
            High temperature in the crate. 
        ]]></doc>
    </const>  
    
</symlist>

<!-- EOF -->
