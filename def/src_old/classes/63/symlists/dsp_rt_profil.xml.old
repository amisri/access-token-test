<?xml version='1.0'?>

<symlist
    title   = "DSP real-time profile points"
>
    <doc><![CDATA[
        The DSP includes profile points that can be monitored with the signals 
        ACQ_MPX.DSP_RT_PROF_0 and ACQ_MPX.DSP_RT_PROF_1. This symbol list defines 
        these point within the real-time processing.
    ]]></doc>

    <const
        name    = "START"
        value   = "0"
        title   = "Start of real-time processing"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            start of the real-time processing.
        ]]></doc>
    </const>

    <const
        name    = "SIGNALS"
        value   = "1"
        title   = "Measurement signal processing complete"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            completion of the processing of measurement signals by libsig.
        ]]></doc>
    </const>

    <const
        name    = "REF_REG"
        value   = "2"
        title   = "Reference regulation completed"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            completion of the call to refRtRegulation().
        ]]></doc>
    </const>

    <const
        name    = "REF_SENT"
        value   = "3"
        title   = "Actuation reference sent"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            transmission of the actuation refernce.
            It is specially important to verify that the value has been written into the SPIVS interface
            before the transmission starts.
        ]]></doc>
    </const>

    <const
        name    = "REF_STATE"
        value   = "4"
        title   = "Reference state processing completed"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            completion of the call to refRtState(). This runs the reference state machine, iterative learning
            controller and circuit simulation.
        ]]></doc>
    </const>

    <const
        name    = "INTERLOCK"
        value   = "5"
        title   = "Interlock values updated"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            completion of the interlock tests in libintlk:intlkRT().
        ]]></doc>
    </const>

    <const
        name    = "LOGGING"
        value   = "6"
        title   = "Signal logged"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            completion of signal logging.
        ]]></doc>
    </const>

    <const
        name    = "END"
        value   = "7"
        title   = "End of real-time processing"
    >
        <doc><![CDATA[
            This DSP real-time point profiles the time since the start of the iteration in microseconds of the
            end of the real-time processing.
        ]]></doc>
    </const>

</symlist>

<!-- EOF -->
