<?xml version='1.0'?>

<symlist
    title   = "ADC internal signal types"
>
    <doc><![CDATA[
	This symbol list is used to select the type of the input signal.
    ]]></doc>

    <const
        name    = "I_DCCT_A"
        value   = "0"
        title   = "Load current measurement A"
        assert  = "SIG_TRANSDUCER_DCCT_A"
    >
        <doc><![CDATA[
            Load current measurement from probe A.
        ]]></doc>
    </const>

    <const
        name    = "I_DCCT_B"
        value   = "1"
        title   = "Load current measurement B"
        assert  = "SIG_TRANSDUCER_DCCT_B"
    >
        <doc><![CDATA[
            Load current measurement from probe B.
        ]]></doc>
    </const>

    <const
        name    = "B_PROBE_A"
        value   = "2"
        title   = "Load field measurement A"
        assert  = "SIG_TRANSDUCER_B_PROBE_A"
    >
        <doc><![CDATA[
            Load field measurment measurement from probe A.
        ]]></doc>
    </const>

    <const
        name    = "B_PROBE_B"
        value   = "3"
        title   = "Load field measurement B"
        assert  = "SIG_TRANSDUCER_B_PROBE_B"
    >
        <doc><![CDATA[
            Load field measurment measurement from probe B.
        ]]></doc>
    </const>

    <const
        name    = "V_MEAS"
        value   = "4"
        title   = "Load voltage measurement"
        assert  = "SIG_TRANSDUCER_V_MEAS"
    >
        <doc><![CDATA[
            Load voltage measurement.
        ]]></doc>
    </const>

    <const
        name    = "V_CAPA"
        value   = "5"
        title   = "Capacitor voltage"
        assert  = "SIG_TRANSDUCER_V_CAPA"
    >
        <doc><![CDATA[
            Capacitor voltage measurement.
        ]]></doc>
    </const>

    <const
        name    = "V_AC"
        value   = "6"
        title   = "AC voltage"
        assert  = "SIG_TRANSDUCER_V_AC"
    >
        <doc><![CDATA[
            AC voltage measurement. The gain is fixed at 1, so this is directly the ADC voltage (&plusmn;10V).
        ]]></doc>
    </const>

    <const
        name    = "I_CAPA"
        value   = "7"
        title   = "Capacitor current"
        assert  = "SIG_TRANSDUCER_I_CAPA"
    >
        <doc><![CDATA[
            Capacitor current measurement.
        ]]></doc>
    </const>

    <const
        name    = "REF"
        value   = "8"
        title   = "External reference"
        assert  = "SIG_TRANSDUCER_EXT_REF"
    >
        <doc><![CDATA[
            Analogue external reference.
        ]]></doc>
    </const>

    <const
        name    = "AUX"
        value   = "9"
        title   = "Auxiliary measurement"
        assert  = "SIG_TRANSDUCER_AUX"
    >
        <doc><![CDATA[
            Auxiliary input of the FGC3.
        ]]></doc>
    </const>

    <const
        name    = "NONE"
        value   = "99"
        title   = "No measurement"
        assert  = "SIG_NOT_IN_USE"
    >
        <doc><![CDATA[
            ADC not attached to any transducer.
        ]]></doc>
    </const>

</symlist>

<!-- EOF -->
