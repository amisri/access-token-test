<?xml version='1.0'?>

<symlist
    title   = "Configuration mode"
>
    <doc><![CDATA[
	The configuration parameters are maintained in a central database
	and local copies are also stored in non-volatile memory in the FGC.
	The user can control the synchronisation of the parameters between the FGC
	and the database using the property {PROP CONFIG.MODE}.
    ]]></doc>

    <const
        name    = "SYNC_NONE"
	    value	= "0"
        title   = "No synchronisation of configuration required"
    >
        <doc><![CDATA[
            This will indicate that no synchronisation of the configuration properties
	    is required.  {PROP CONFIG.MODE} will be reset automatically to this value
	    if the property {PROP CONFIG.STATE} is reset.  This will be done by the
	    configuration manager after a synchronisation.

        ]]></doc>
    </const>

   <const
        name    = "SYNC_DB"
	    value	= "1"
        title   = "Synchronise configuration properties from the FGC to the database"
    >
        <doc><![CDATA[
            This will lock the configuration properties and will signal to
	    the FGC manager that the database should be set to match the values
	    in the FGC.  When the manager finishes reading out the property
	    values, it will reset {PROP CONFIG.STATE} which will reset {PROP CONFIG.MODE}
	    to SYNC_NONE.
        ]]></doc>
    </const>

    <const
        name    = "SYNC_FGC"
	    value	= "2"
        title   = "Synchronise configuration properties from the database to the FGC"
    >
        <doc><![CDATA[
            This will signal to
	    the FGC manager that the FGC configuration should be set to match
	    the values in the database.  When the manager finishes setting the
	    properties, it will set the config mode back to locked to clear
	    the sync FGC request.  If an FGC is connected to the WorldFIP,
	    {PROP CONFIG.MODE} will automatically be set to
	    SYNC_FGC a few seconds after a reset, once the Dallas IDs have been
	    read.
        ]]></doc>
    </const>

    <const
        name    = "SYNC_DB_CAL"
	    value	= "4"
        title   = "Synchronise internal ADC error properties from the FGC to the database"
    >
        <doc><![CDATA[
            This will signal to
	    the FGC manager that the internal ADC error properties need to be
	    synchronised in the database following an automatic calibration
	    procedure.  This might be triggered by a user or automatically by the FGC.
        ]]></doc>
    </const>

    <const
        name    = "SYNC_FAILED"
	    value	= "8"
        title   = "Synchronisation failed"
    >
        <doc><![CDATA[
            The FGC configuration manager program will set {PROP CONFIG.MODE} to
	    SYNC_FAILED if it was unabled to complete the synchronisation process.
	    This might be due to finding an unknown component connected to the FGC.
        ]]></doc>
    </const>

</symlist>

<!-- EOF -->
