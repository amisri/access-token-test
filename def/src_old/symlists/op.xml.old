<?xml version='1.0'?>

<symlist
    title   = "Operational State symbol list"
>
    <doc><![CDATA[
        The operational state machine {PROP STATE.OP} defines the global state of
	an FGC.  Some of the states can be controlled by the user via the
	property {PROP MODE.OP} and some result from external events and conditions.
    ]]></doc>

    <const
        name    = "UNCONFIGURED"
        value   = "0"
        title   = "FGC is unconfigured"
    >
        <doc><![CDATA[
            The FGC is unconfigured, which means that at least one configuration
	    property has never been set.  To see which you should get {PROP CONFIG.UNSET}.
        ]]></doc>
    </const>

    <const
        name    = "NORMAL"
        value   = "1"
        title   = "FGC is in normal operating mode"
    >
        <doc><![CDATA[
            The FGC is ready to operate.
        ]]></doc>
    </const>

    <const
        name    = "SIMULATION"
        value   = "2"
        title   = "FGC is simulating the voltage source"
    >
        <doc><![CDATA[
            The FGC is simulating the voltage source.  It may also be simulating the
            interlock signals.  This is controlled by {PROP VS.SIM_INTLKS}.
        ]]></doc>
    </const>

    <const
        name    = "CALIBRATING"
        value   = "3"
        title   = "FGC is calibrating its ADCs or DAC(s)"
    >
        <doc><![CDATA[
            The FGC is calibrating its ADCs or DAC(s).  This maybe due to a user
            request (See {PROP CAL}) or can be automatic since the internal ADCs are
            calibrated once per 24 hours.
        ]]></doc>
    </const>

    <const
        name    = "TEST"
        value   = "4"
        title   = "FGC is in test mode"
    >
        <doc><![CDATA[
            The FGC is in test mode.  This follows a user request to set {PROP MODE.OP}
            to TEST.  It allows certain self-tests to be conducted.
        ]]></doc>
    </const>

    <const
        name    = "BOOT"
        value   = "5"
        title   = "FGC in the boot"
    >
        <doc><![CDATA[
            When an FGC is reset, it starts by running the Boot program.  This will
            perform various self tests and will check if all the installed codes are
            up to date.  If any need updating from the network, it will switch to
            the PROGRAMMMING state and will wait for the new codes to be sent by
            the gateway.
        ]]></doc>
    </const>

    <const
        name    = "PROGRAMMING"
        value   = "6"
        title   = "FGC in programming state"
    >
        <doc><![CDATA[
            If the FGC Boot program determines that some of its installed codes are not
            up to date, it will enter the PROGRAMMING state and will wait for the gateway
            to send new codes.  Once all the codes are up to date, it will return to the BOOT
            state and if all is fine, it will then run the main program.
        ]]></doc>
    </const>

</symlist>

<!-- EOF -->
