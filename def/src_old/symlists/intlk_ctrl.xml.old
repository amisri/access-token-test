<?xml version='1.0'?>

<symlist
    title   = "Interlock channel control"
>
    <doc><![CDATA[
        This symbol list specifies the options for {PROP INTERLOCK.CONTROL}.
    ]]></doc>

    <const
        name    = "OFF"
        value   = "0"
        assert  = "INTLK_CHAN_OFF"
        title   = "Off"
    >
        <doc><![CDATA[
            The channel is off. The parameters will not be process and the tests will not be executed.
            The logs will not be available in PowerSpy.
        ]]></doc>
    </const>

    <const
        name    = "NOT_OK"
        value   = "1"
        assert  = "INTLK_CHAN_NOT_OK"
        title   = "Not OK"
    >
        <doc><![CDATA[
            The channel is active but will always be forced to be NOT OK.
        ]]></doc>
    </const>

    <const
        name    = "OK"
        value   = "2"
        assert  = "INTLK_CHAN_OK"
        title   = "OK"
    >
        <doc><![CDATA[
            The channel is active but will always be forced to be OK.
        ]]></doc>
    </const>

    <const
        name    = "CONDITIONAL"
        value   = "3"
        assert  = "INTLK_CHAN_CONDITIONAL"
        title   = "Conditional"
    >
        <doc><![CDATA[
            <p>The channel is active and the status of the channel will be the result of the six tests:</p> 
            <ol>
            <li>REF_STATE</li>
            <li>ABS_REG_ERR</li>
            <li>V_MEAS</li>
            <li>ABS_V_RATE</li>
            <li>I_MEAS</li>
            <li>ABS_I_RATE</li>
            </ol>
            <p>All six tests must be OK or overridden for the channel to be OK.</p>
        ]]></doc>
    </const>    
</symlist>

<!-- EOF -->
