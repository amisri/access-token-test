<?xml version='1.0'?>

<symlist
    title   = "DCCT measurement selector"
>
    <doc><![CDATA[
        This symbol list selects DCCT signals for the current measurement.
    ]]></doc>

    <const
        name    = "A"
        value   = "0"
        assert  = "SIG_A"
        title   = "Current measurement derived from DCCT A"
    >
        <doc><![CDATA[
        In this mode, Imeas is based on I_DCCT_A.
        If I_DCCT_A becomes invalid, the converter will be stopped with an I_MEAS fault.
        ]]></doc>
    </const>

    <const
        name    = "B"
        value   = "1"
        assert  = "SIG_B"
        title   = "Current measurement derived from DCCT B"
    >
        <doc><![CDATA[
        In this mode, Imeas is based on I_DCCT_B.
        If I_DCCT_B becomes invalid, the converter will be stopped with an I_MEAS fault.
        ]]></doc>
    </const>

    <const
        name    = "AB"
        value   = "2"
        assert  = "SIG_AB"
        title   = "Currents measurement derived from DCCT A and/or B"
    >
        <doc><![CDATA[
        In this mode, the average of I_DCCT_A and I_DCCT_B is used as Imeas
        if both I_DCCT_A and I_DCCT_B are valid.
        If one of the signal becomes invalid, Imeas will continue to be calculated using
        the remaining signal and an I_MEAS warning will be activated.  If both signals
        become invalid, the converter will be stopped with an I_MEAS fault.
        ]]></doc>
    </const>

</symlist>

<!-- EOF -->
