<!--	FGC3 DSP UTC memmap definition -->

<!-- ===================================================================================================== -->

<zone
    name     = "UTC"
    offset   = "0x90004100B"
    size     = "0x20B"
    comment  = "[PLD] UTC Time registers"
>
    <doc><![CDATA[
	    This zone contains the registers related to absolute UTC time.  Time is received from the gateway by
	    the MCU which is responsible for setting the time in the FPGA.
    ]]></doc>

    <!-- C0 =============================================================================================== -->

    <zone
        name    = "C0"
        offset  = "0B"
        size    = "8B"
        comment = "UTC time stamp for the start of cycle"
    >
        <doc><![CDATA[
            These registers will capture the UTC time on C0 (the start of the cycle).
        ]]></doc>

        <zone
            name    = "UNIXTIME"
            offset  = "0B"
            size    = "4B"
            type    = "volatile uint32_t"
            consts  = "near"
            access  = "R"
            comment = "UTC Unix time at the start of the cycle"
        >

            <doc><![CDATA[
                This register will latch the UTC Unix time on C0 (the start of the cycle).
            ]]></doc>
        </zone>

        <zone
            name    = "MS"
            offset  = "4B"
            size    = "4B"
            type    = "volatile uint32_t"
            consts  = "near"
            access  = "R"
            comment = "UTC millisecond time at the start of the cycle"
        >

            <doc><![CDATA[
                This register will latch the UTC millisecond time on C0 (the start of the cycle).
            ]]></doc>
        </zone>
    </zone>

    <!-- DSP ============================================================================================== -->

    <zone
        name    = "DSP"
        offset  = "8B"
        size    = "8B"
        comment = "UTC time stamp for the DSP interrupt"
    >
        <doc><![CDATA[
            These registers will capture the UTC time on the DSP interrupt.
        ]]></doc>

        <zone
            name    = "UNIXTIME"
            offset  = "0B"
            size    = "4B"
            type    = "volatile uint32_t"
            consts  = "near"
            access  = "R"
            comment = "UTC Unix time at the time of the DSP interrupt"
        >

            <doc><![CDATA[
                This register will latch the UTC Unix time of DSP interrupt.
            ]]></doc>
        </zone>

        <zone
            name    = "US"
            offset  = "4B"
            size    = "4B"
            type    = "volatile uint32_t"
            consts  = "near"
            access  = "R"
            comment = "UTC microsecond time at the time of the DSP interrupt"
        >

            <doc><![CDATA[
                This register will latch the UTC microsecond time of the DSP interrupt.
            ]]></doc>
        </zone>
    </zone>

    <!-- ================================================================================================= -->

</zone>

<!-- EOF -->
