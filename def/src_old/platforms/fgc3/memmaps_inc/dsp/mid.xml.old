<!--    FGC3 Module IDentifier memmap definition -->

<!-- SRAM ===================================================================================== -->

<zone
    name     = "DSP_MID"
    offset   = "0x90004000B"
    size     = "64B"
    type     = "volatile uint32_t"
    access   = "R"
    consts   = "32bit"
    comment  = "[PLD] Module IDentifier"
>
    <doc><![CDATA[
        These are 16 x 32 bit read only registers in the [PLD] zone.<br>
        Only the 16 lower bits are used. The upper word is set to 0.<br>
        The information is the same as for <A HREF="Platform_fgc3_memmap_MCU_MID.htm">MCU</A>
    ]]></doc>

    <zone
        name     = "FGCTYPE"
        offset   = "0B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "FGC TYPE (V3.0, V3.1, ...)"
    >
        <doc><![CDATA[
            This is the Hardware version of the FGC.
        ]]></doc>
    </zone>

    <zone
        name     = "FGCVER"
        offset   = "4B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "FGC VERSION (0, 1, ...)"
    >
        <doc><![CDATA[
            This is the Hardware revision of the FGC.
        ]]></doc>
    </zone>

    <zone
        name     = "PLDTYPE"
        offset   = "8B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "PLD TYPE (0x0700 = XC3S700AN)"
    >
        <doc><![CDATA[
            The PLD TYPE describe which type of PLD is implemented on the mainboard.
        ]]></doc>
    </zone>

    <zone
        name     = "PLDVER"
        offset   = "12B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "PLD VERSION (0, 1, ...)"
    >
        <doc><![CDATA[
            This is the logiware version of the PLD.
        ]]></doc>
    </zone>

    <zone
        name     = "CRATETYPE"
        offset   = "16B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "Equipement type connected to the FGC"
    >
    <doc><![CDATA[
        This value is coming from the BUS connected to the FGC and not from
        the ROM memory.
        The BUS Type defined which kind of equipement is connected to the FGC.
        This value must be comprised beetwen 0x0001 and 0x003F.<br>
        The values are described at <PROP CRATE.TYPE><br>
        All bits to 1 is unknown equipement or disconnected.
    ]]></doc>
    </zone>

    <zone
        name     = "NETTYPE"
        offset   = "20B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "Network Interface type"
    >
    <doc><![CDATA[
        This value is coming from the network interface connected to the FGC and not from
        the ROM memory.
        The NETTYPE defines which kind of interface is connected to the FGC.
        This value must be comprised beetwen 0x0000 and 0x000F.
        All bits to 0 or 1 is unknown analog interface.
    ]]></doc>
    </zone>

    <zone
        name     = "ANATYPE"
        offset   = "24B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "Analog Interface type"
    >
        <doc><![CDATA[
            This value is coming from the analog interface connected to the FGC and not from
            the ROM memory.
            The ANA Type defined which kind of analog interface is connected to the FGC.
            This value must be comprised beetwen 0x0000 and 0x000F.
            All bits to 0 or 1 is unknown analog interface.
        ]]></doc>
    </zone>

    <zone
        name     = "EOMID"
        offset   = "60B"
        size     = "4B"
        type     = "volatile uint32_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "End Of MID"
    >
        <doc><![CDATA[
            This is a constant of 0xA55A which points the end of the Module IDentifier.
            This value must be present to validate the MID datas.
        ]]></doc>
    </zone>
</zone>

<!-- EOF -->
