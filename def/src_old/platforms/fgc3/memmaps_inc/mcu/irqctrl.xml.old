<!-- FGC3 Interrupt Request Controller registers memmap definition -->

<!-- IRQCTRL ===================================================================================== -->

<zone
    name     = "IRQCTRL"
    offset   = "0x04080040B"
    size     = "40B"
    type     = "volatile uint16_t"
    access   = "R"
    consts   = "16bit 32bit near"
    comment  = "[PLD] Interrupt Request Controller"
>
    <doc><![CDATA[
        
    ]]></doc>

    <zone
        name     = "PENDINGIRQ"
        offset   = "0B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "Pending interrupt register"
    >
        <doc><![CDATA[
            The MCU ISR will read this to know the highest priority level pending (i.e. the lowest level) and acknowledge and reset that level. If no interrupt is pending, it will return a value greater than 15 (null).
        ]]></doc>
    </zone>

    <zone
        name     = "MISSEDIRQS"
        offset   = "2B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "R"
        consts   = "32bit near"
        comment  = "Missed interrupts register"
    >
        <doc><![CDATA[
            All bits of this register should always be 0. Any non-zero bit means that one or more occurrences of the IRQ corresponding to that specific level has not been timely handled.
        ]]></doc>
    </zone>

    <zone
        name     = "MSTICKLVL"
        offset   = "4B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "Millisecond tick interrupt register"
    >
        <doc><![CDATA[
            Interrupt level for the start of millisecond tick.
        ]]></doc>
    </zone>

    <zone
        name     = "INTERFGCLVL"
        offset   = "6B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "INTER_FGC millisecond tick interrupt register"
    >
        <doc><![CDATA[
            Interrupt level for the INTER_FGC millisecond tick (offset by a pre-defined amount from mstick).
        ]]></doc>
    </zone>

    <zone
        name     = "SCIVSLVL"
        offset   = "8B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "SCIVS packet reception interrupt register"
    >
        <doc><![CDATA[
            Interrupt level for the reception of SCIVS packets.
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ1LVL"
        offset   = "16B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 1 register"
    >
        <doc><![CDATA[
            MCU Software Interrupt level 1 register. This level is set at initialization time. The Interrupt can be triggered by writing to 0x4080060.
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ2LVL"
        offset   = "18B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 2 register"
    >
        <doc><![CDATA[
            MCU Software Interrupt level 2 register. This level is set at initialization time. The Interrupt can be triggered by writing to 0x4080062.
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ3LVL"
        offset   = "20B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 3 register"
    >
        <doc><![CDATA[
            MCU Software Interrupt level 3 register. This level is set at initialization time. The Interrupt can be triggered by writing to 0x4080064.
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ4LVL"
        offset   = "22B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 4 register"
    >
        <doc><![CDATA[
            MCU Software Interrupt level 4 register. This level is set at initialization time. The Interrupt can be triggered by writing to 0x4080066.
        ]]></doc>
    </zone>

    <zone
        name     = "DSPSWIRQ1LVL"
        offset   = "24B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "DSP Software Interrupt level 1 register"
    >
        <doc><![CDATA[
            DSP Software Interrupt level 1 register.
        ]]></doc>
    </zone>

    <zone
        name     = "DSPSWIRQ2LVL"
        offset   = "26B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "DSP Software Interrupt level 2 register"
    >
        <doc><![CDATA[
            DSP Software Interrupt level 2 register.
        ]]></doc>
    </zone>

    <zone
        name     = "DSPSWIRQ3LVL"
        offset   = "28B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "RW"
        consts   = "32bit near"
        comment  = "DSP Software Interrupt level 3 register"
    >
        <doc><![CDATA[
            DSP Software Interrupt level 3 register.
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ1TRIG"
        offset   = "32B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "W"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 1 trigger register"
    >
        <doc><![CDATA[
            By writing to this register, the MCU triggers a software interrupt of a priority level set at the respective IRQCTRL_MCUSWIRQ1LVL register (0x4080050).
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ2TRIG"
        offset   = "34B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "W"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 2 trigger register"
    >
        <doc><![CDATA[
            By writing to this register, the MCU triggers a software interrupt of a priority level set at the respective IRQCTRL_MCUSWIRQ2LVL register (0x4080052).
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ3TRIG"
        offset   = "36B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "W"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 3 trigger register"
    >
        <doc><![CDATA[
            By writing to this register, the MCU triggers a software interrupt of a priority level set at the respective IRQCTRL_MCUSWIRQ3LVL register (0x4080054).
        ]]></doc>
    </zone>

    <zone
        name     = "MCUSWIRQ4TRIG"
        offset   = "38B"
        size     = "2B"
        type     = "volatile uint16_t"
        access   = "W"
        consts   = "32bit near"
        comment  = "MCU Software Interrupt level 4 trigger register"
    >
        <doc><![CDATA[
            By writing to this register, the MCU triggers a software interrupt of a priority level set at the respective IRQCTRL_MCUSWIRQ4LVL register (0x4080056).
        ]]></doc>
    </zone>
</zone>

<!-- EOF -->