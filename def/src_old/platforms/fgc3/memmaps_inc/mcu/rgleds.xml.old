<!--	FGC2/3 RED/GREEN LEDS memmap definition

	Notes: This memmap defines a single register which controls the front panel LEDs.
	The FGC3 has BLUE leds which are controlled by a second register that is unique to the
	FGC3.
-->

    <doc><![CDATA[
	Both FGC2 and FGC3 have six Red/Green LEDS for indicating the status
	of the FGC and the power converter.
	Contrary to FGC2, FGC3 has no Fresnel lens. For this reason, it can be hard to distinguish between orange (warning)
	and red (fault). To emphasize the fault signal, we blink the red led in case of a fault
	(at the same time it makes the frontpanel partly colorblind friendly).
    ]]></doc>

<!-- LEDS - FIP ================================================================================ -->

    <zone
	name     = "NET"
	offset   = "8b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "WorldFIP fieldbus status LED"
    >
	<doc><![CDATA[
	    <p>The NET status LED provides information about the state of the WorldFIP or FGC_Ether network connection.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>
		<li>FGC not powered</li>
		<li>FGC not connected to the network (standalone mode)</li></td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>Connected to network but no traffic detected</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>
		<li>FGC in RESET</li>
		<li>Either the external sync signal is missing, ot there are not enough packets to lock the FGC's phased lock loop</li></td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>WorldFIP functioning correctly and PLL locked</td>
	    </tr>
	    </table>
	    </div>
	    <p>Note: The RED LED is controlled by software while the GREEN is activated automatically
	    by the FIP IRQ.</p>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "FIP status RED LED"
	>
	    <doc><![CDATA[
		Setting this bit will light the RED FIP status LED.

		The HC16 program will use this LED to indicate that the Phase Locked Loop is NOT locked.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    access  = "R"
	    consts  = "mask16"
	    comment = "FIP status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN FIP status LED will be lit.
		The bit will be set by FIP IRQ active transition and will be reset
		automatically 16ms later - it is not writable by software.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - FGC ================================================================================ -->

    <zone
	name     = "FGC"
	offset   = "10b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "Function Generator/Controller status LED"
    >
	<doc><![CDATA[
	    <p>The FGC status LED provides information about the state of the FGC.  It will flash
	    at 1Hz when the FGC is running.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>FGC is not powered</td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>FGC HW fault(s) detected</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>
		<li>FGC in RESET</li>
		<li>FGC HW warning(s) detected</li>
		</td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>FGC running normally</td>
	    </tr>
	    </table>
	    </div>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    access  = "R"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "FGC status RED LED"
	>
	    <doc><![CDATA[
		When this bit is set, the RED FGC status LED will be lit.
		This bit will be set when FGC_HW fault or warning is detected.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    access  = "R"
	    consts  = "mask16"
	    comment = "FGC status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN FGC status LED will be lit.
		This bit will be set when the FGC_HW fault is not present.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - PSU ================================================================================ -->

    <zone
	name     = "PSU"
	offset   = "12b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "Power Supply Unit status LED"
    >
	<doc><![CDATA[
	    <p>The PSU status LED provides information about the state of the FGC power supply.
	    The FGC psu's are designed to support redundant operation, and if implemented,
	    they can partly fail without loss of operation.  This LED will indicate this
	    state, which leaves the system vulnerable to a subsequent failure.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>FGC is not powered</td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>PSU fault and/or voltages out of tolerance</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>
		<li>FGC in RESET</li>
		<li>Voltage in tolerance but PSU fault is present</li></td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>PSU running normally and voltages in tolerance</td>
	    </tr>
	    </table>
	    </div>
	    <p>Note: This LED is completely controlled by software.</p>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "PSU status RED LED"
	>
	    <doc><![CDATA[
		When this bit is set, the RED PSU status LED will be lit.
		The HC16 software will set this bit if any of the power supply voltages
		(+5, +/-15) are out of tolerance, or if the PSU is reporting a fault via
		its status signal.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "PSU status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN PSU status LED will be lit.
		The HC16 software will set this bit if all the of the power supply
		voltages (+5, +/-15) are within tolerance.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - VS ================================================================================= -->

    <zone
	name     = "VS"
	offset   = "14b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "Voltage Source status LED"
    >
	<doc><![CDATA[
	    <p>The Voltage Source status LED provides information about the state of the
	    Voltage Source controlled by the FGC.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>FGC is not powered or voltage source is off</td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>Voltage source is reporting a fault.  Note that this signal is
		latched, so the source of the fault may no longer be present.</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>Voltage source is running AND is reporting a fault.  This should
		never be seen if the Voltage source is correctly connected.  It might indicate a
		cable or connector fault.</td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>Voltage is running.</td>
	    </tr>
	    </table>
	    </div>
	    <p>Note: This LED is completely controlled by software.</p>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "VS status RED LED"
	>
	    <doc><![CDATA[
		When this bit is set, the RED VS status LED will be lit.
		This bit will be set by the HC16 software when the VS fault input is active.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "VS status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN VS status LED will be lit.
		This bit will be set by the HC16 software when the VS_ON input is active.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - DCCT =============================================================================== -->

    <zone
	name     = "DCCT"
	offset   = "0b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "DCCTs status LED"
    >
	<doc><![CDATA[
	    <p>The DCCTs status LED provides information about the state of the
	    DCCTs used by the FGC to measure the current in the circuit.
	    Each DCCT provides a digital status signal which indicates if the analogue signal
	    can be trusted.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>FGC is not powered</td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>Both DCCT A and B are/were reporting faults.  Note that this
		condition is latched by the FGC digital interface.</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>One DCCT is reporting a fault and the other is okay.</td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>Both DCCTs are okay.</td>
	    </tr>
	    </table>
	    </div>
	    <p>Note: This LED is completely controlled by software.</p>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "DCCT status RED LED"
	>
	    <doc><![CDATA[
		When this bit is set, the RED DCCT status LED will be lit.
		This bit will be set by the HC16 software if either DCCT A or DCCT B is
		reporting a fault.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "DCCT status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN DCCT status LED will be lit.
		This bit will be set by the HC16 software if either DCCT A or DCCT B is
		not reporting a fault.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - PIC ================================================================================ -->

    <zone
	name     = "PIC"
	offset   = "2b"
	size     = "2b"
	type     = "volatile uint16_t"
	comment  = "Powering Interlock Controller status LED"
    >
	<doc><![CDATA[
	    <p>The Powering Interlock Controller status LED provides information about the
	    FGC interlock signals.</p>
	    <DIV class="center">
	    <table class="embedded">
	    <tr>
		<th class="alt" nowrap>LED Colour</th>
		<th class="alt">Interpretation</th>
	    </tr>
	    <tr>
		<th>BLACK</td>
		<td>The FGC is not receiving a Power Permit signal and is not
		generating a Powering Failure signal.</td>
	    </tr>
	    <tr>
		<th>RED</td>
		<td>The FGC is generating a Power Failure signal.</td>
	    </tr>
	    <tr>
		<th>ORANGE</td>
		<td>The FGC is receiving a Power Permit AND is generating a Powering
		Failure signal.  This should never happen and indicates either a fault in the PIC or
		a cabling or connector fault.</td>
	    </tr>
	    <tr>
		<th>GREEN</td>
		<td>The FGC is receiving the Power Permit signal.</td>
	    </tr>
	    </table>
	    </div>
	    <p>Note: This LED is completely controlled by software.</p>
	]]></doc>

	<zone
	    name    = "RED"
	    offset  = "0b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "PIC status RED LED"
	>
	    <doc><![CDATA[
		When this bit is set, the RED PIC status LED will be lit.
		This bit will be set by the HC16 software when it is generating a Powering Failure
		signal.
	    ]]></doc>
	</zone>

	<zone
	    name    = "GREEN"
	    offset  = "1b"
	    size    = "1b"
	    type    = "volatile uint16_t"
	    consts  = "mask16"
	    comment = "PIC status GREEN LED"
	>
	    <doc><![CDATA[
		When this bit is set, the GREEN PIC status LED will be lit.
		This bit will be set by the HC16 software when the Power Permit interlock input is active.
	    ]]></doc>
	</zone>
    </zone>

<!-- LEDS - TEST ================================================================================ -->

    <zone
	name     = "TEST"
	offset   = "4b"
	size     = "1b"
	type     = "volatile uint16_t"
	access	 = "R"
	consts	 = "mask16"
	comment  = "TEST LEDS button input"
    >
	<doc><![CDATA[
	    The button labelled TEST LEDS that will turn on all six
	    bi-colour LEDS.  This bit enables the software to detect the state of this button:
	    <ul>
	    <li> 1 - Button is pressed
	    <li> 0 - Button is not pressed
	    </ul>
	    This will be used by the boot to stop execution of the main program.
	]]></doc>
    </zone>

<!-- EOF -->
