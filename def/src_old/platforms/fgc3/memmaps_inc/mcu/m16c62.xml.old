<!-- FGC3 M16C62 link registers memmap definition -->

<zone
    name    = "C62"
    offset  = "0x04080600B"
    size    = "2B"
    type    = "volatile uint16_t"
    consts  = "32bit near"
    comment = "[PLD] M16C62 microcontroller communications register"
>
    <doc><![CDATA[
	This register is provided to allow the MCU to communicate with the M16C62 microcontroller.<br>
	<br>
	The MCU send a byte to the M16C62 by writing the value in the low byte of the register.<br>
	Note that the MCU must always do a word write to this register.<br>
	<br>
	The write by MCU to the register :
	<ul>
	    <li>
		will <b>clear</b> DATARDY bit
	    </li>
	    <li>
		will <b>clear</b> CMDERR bit
	    </li>
	    <li>
		will <b>set</b> CMDBUSY bit
	    </li>
	    <li>
		will <b>fire</b> an interrupt (INT3) in the M16C62, which will read and process the byte
	    </li>
	</ul>
	<br>
	At the M16C62 side, the bits on the register can only
	<ul>
	    <li>
		<b>set</b> DATARDY bit, by a transition from high to low of p3.0 signal
	    </li>
	    <li>
		<b>set</b> CMDERR bit, by a transition from high to low of p3.1 signal
	    </li>
	    <li>
		<b>clear</b> CMDBUSY bit, by a transition from high to low of p1.7 signal
	    </li>
	</ul>
	<br>

	<p>
	    The byte may be either a command byte or an argument byte.<br>
	    The list of commands and response error codes are defined in this
	    <A HREF="https://gitlab.cern.ch/ccs/fgc/blob/master/sw/fgc/dallas/inc/m16c62_link.h">header file</A>.<br>
	    In either case, the M16C62 will repond to the received byte by returning a response byte in
	    the low byte of the register, together with either the
	    <A HREF="Platform_fgc3_memmap_MCU_C62_DATARDY.htm">DATARDY</A>
	    or <A HREF="Platform_fgc3_memmap_MCU_C62_CMDERR.htm">CMDERR</A> bit set to one.
	    The interpretation of the response byte
	    depends upon the context:
	    <ul>
		<li>C62_GET_RESPONSE command - if
		<A HREF="Platform_fgc3_memmap_MCU_C62_DATARDY.htm">DATARDY</A>
		is set the response byte will be the next byte
		from the response data buffer. If CMDERR is set, the response byte will be an error code
		and the response data buffer will contain an error message.  The response data buffer
		is where commands prepare their results or error reports for subsequent readout by the
		MCU using the C62_GET_RESPONSE command.</li>
		<li>All other commands - if
		<A HREF="Platform_fgc3_memmap_MCU_C62_DATARDY.htm">DATARDY</A>
		is set the response byte will be the number of
		argument bytes still to be sent (modulo 0x100).  If
		<A HREF="Platform_fgc3_memmap_MCU_C62_CMDERR.htm">CMDERR</A>
		is set, the response byte
		will be an error code and the response data buffer will contain an error message.
	    </ul>
	    Command requiring longer process time, are launched in the background and the
	    <A HREF="Platform_fgc3_memmap_MCU_C62_CMDBUSY.htm">CMDBUSY</A>
	    bit will stay at one until the
	    command completes.  The results of the command will then be available in the response
	    data buffer.
	<p>
	Word writes only are supported, while word or byte reads are permitted.
    ]]></doc>

    <zone
	name        = "DATA"
	offset      = "1B"
	size        = "1B"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 communications data byte"
    >
    <doc><![CDATA[
	The low byte of the M16C62 register provides the data channel between the Rx610 and M16C62,
	however, all reads and writes to the C62 register must be by word.  Byte access is not
	supported.
	<p>
	The MCU can write a
	<A HREF="https://gitlab.cern.ch/ccs/fgc/blob/master/sw/fgc/dallas/inc/m16c62_link.h">command byte</A>
	followed by arguments (if required for the command).
	Every time a byte is written the handshake lines will be set as follows:
	<ul>
	<li><A HREF="Platform_fgc3_memmap_MCU_C62_DATARDY.htm">DATARDY</A> = 0</li>
	<li><A HREF="Platform_fgc3_memmap_MCU_C62_CMDERR.htm">CMDERR</A> = 0</li>
	<li><A HREF="Platform_fgc3_memmap_MCU_C62_CMDBUSY.htm">CMDBUSY</A> = 1</li>
	</ul>
	Within XX us, the M16C62 will respond by set either
	<A HREF="Platform_fgc3_memmap_MCU_C62_DATARDY.htm">DATARDY</A> or
	<A HREF="Platform_fgc3_memmap_MCU_C62_CMDERR.htm">CMDERR</A> to 1 and the response
	byte will be available in this byte.
	<p>
	If no additional processing is required,
	<A HREF="Platform_fgc3_memmap_MCU_C62_CMDBUSY.htm">CMDBUSY</A> will be cleared at the
	same time.  However, if the command takes more than 50 us, the processing will be done
	in the background and
	<A HREF="Platform_fgc3_memmap_MCU_C62_CMDBUSY.htm">CMDBUSY</A> will stay at 1 until
	it has completed.  The results will then be available using the C62_GET_RESPONSE command.
    ]]></doc>
    </zone>
    <zone
	name        = "DATARDY"
	offset      = "0b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 data ready flag"
	access      = "R"
    >
    <doc><![CDATA[
	<ul>
	<li><b>Bit = 1</b>: M16C62 ISR finished OK, the received byte was accepted.
	<li><b>Bit = 0</b>: the C62 register was written.
	</ul>
	This bit is reset when the Rx610 writes in the C62 register.
	It is set when the M16C62 drops Port3.0 from 1 to 0 (falling edge).<br>
	This will indicate that the M16C62 has successfully processed the command or argument byte and a
	response byte is available in the low byte of the register.<br>
	<br>
	Note: this is not completely true, as it is used in the M16C62 it is more ISR finished OK than DATA READY,
	with a slow (background) command we can exit the ISR with CMD BUSY active and DATA READY active !!!!
    ]]></doc>
    </zone>
    <zone
	name        = "CMDERR"
	offset      = "1b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 command error flag"
	access      = "R"
    >
    <doc><![CDATA[
	<ul>
	<li><b>Bit = 1</b>: the M16C62 signals an error.
	<li><b>Bit = 0</b>: the C62 register was written.
	</ul>
	This bit is reset when the Rx610 writes in the C62 register.
	It is set when the M16C62 drops Port3.1 from 1 to 0 (falling edge).<br>
	This will indicate that the M16C62 has been unable to process the command or argument byte.
	The response byte will
	contain an error code and an error message will be available in the response data buffer.
    ]]></doc>
    </zone>
    <zone
	name        = "CMDBUSY"
	offset      = "2b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 command busy flag"
	access      = "R"
    >
    <doc><![CDATA[
	This bit indicates that the M16C62 has finished the requested command.<br>
	<ul>
	<li><b>Bit = 1</b>: set at the M16C62-ISR entry, when the command is received.
	<li><b>Bit = 0</b>: the M16C62 is available again, after finished the request (or error).
	</ul>
    ]]></doc>
    </zone>
    <zone
	name        = "STANDALONE"
	offset      = "3b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 standalone mode flag"
	access      = "R"
    >
    <doc><![CDATA[
	This bit indicates the M16C62 STANDALONE switch state.<br>
	<ul>
	<li><b>Bit = 1</b>: the M16C62 is in standalone mode.
	<li><b>Bit = 0</b>: the M16C62 is in normal mode.
	</ul>
    ]]></doc>
    </zone>
    <zone
	name        = "PROG"
	offset      = "4b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	comment     = "M16C62 prog mode flag"
	access      = "RW"
    >
    <doc><![CDATA[
	This bit indicates the M16C62 PROGRAM switch state.<br>
	<ul>
	<li><b>Bit = 1</b>: the M16C62 boots to Renesas-Boot-Loader.
	<li><b>Bit = 0</b>: the M16C62 boots to user program.
	</ul>
	When the M16C62 PROG switch is closed and the M16C62 starts-up, it will run the Renesas-Boot-Loader program and not the normal users program.
	This mode is used by the FGC program (running on the Rx610) to flash the M16C62
	flash communicating via the M16C62-UART1 using the Renesas-Boot-Loader specific protocol.
    ]]></doc>
    </zone>
    <zone
	name        = "POWER"
	offset      = "6b"
	size        = "1b"
	type        = "volatile uint16_t"
	consts      = "mask16"
	access      = "R"
	comment     = "M16C62 power status"
    >
    <doc><![CDATA[
	This bit is the readback state of the M16C62 +5V.<br>
	<ul>
	<li><b>Bit = 1</b>: the M16C62 and all the other Dallas components are powered.
	<li><b>Bit = 0</b>: the M16C62 and all the other Dallas components are NOT powered.
	</ul>
    ]]></doc>
    </zone>
</zone>
<!-- EOF -->
