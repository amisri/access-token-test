<?xml version="1.0"?>

<!-- FGC3 DSP Memory Map - Reference include files -->

<!DOCTYPE memmap [
    <!ENTITY ana                   SYSTEM "platforms/fgc3/memmaps_inc/dsp/ana.xml"                >
    <!ENTITY ana_card              SYSTEM "platforms/fgc3/memmaps_inc/dsp/ana_card.xml"           >
    <!ENTITY ana_card_adc_ctrl     SYSTEM "platforms/fgc3/memmaps_inc/dsp/ana_card_adc_ctrl.xml"  >
    <!ENTITY ana_ext_adc           SYSTEM "platforms/fgc3/memmaps_inc/dsp/ana_ext_adc.xml"        >
    <!ENTITY dig_sup               SYSTEM "platforms/fgc3/memmaps_inc/dsp/dig_sup.xml"            >
    <!ENTITY dpram                 SYSTEM "platforms/fgc3/memmaps_inc/dsp/dpram.xml"              >
    <!ENTITY dsp_ram               SYSTEM "platforms/fgc3/memmaps_inc/dsp/dsp_ram.xml"            >
    <!ENTITY dsp_rom               SYSTEM "platforms/fgc3/memmaps_inc/dsp/dsp_rom.xml"            >
    <!ENTITY emif                  SYSTEM "platforms/fgc3/memmaps_inc/dsp/emif.xml"               >
    <!ENTITY mid                   SYSTEM "platforms/fgc3/memmaps_inc/dsp/mid.xml"                >
    <!ENTITY irqctrl               SYSTEM "platforms/fgc3/memmaps_inc/dsp/irqctrl.xml"            >
    <!ENTITY sdram                 SYSTEM "platforms/fgc3/memmaps_inc/dsp/sdram.xml"              >
    <!ENTITY spivs                 SYSTEM "platforms/fgc3/memmaps_inc/dsp/spivs.xml"              >
    <!ENTITY bis                   SYSTEM "platforms/fgc3/memmaps_inc/dsp/bis.xml"                >
    <!ENTITY spy                   SYSTEM "platforms/fgc3/memmaps_inc/dsp/spy.xml"                >
    <!ENTITY time                  SYSTEM "platforms/fgc3/memmaps_inc/dsp/time.xml"               >
    <!ENTITY utc                   SYSTEM "platforms/fgc3/memmaps_inc/dsp/utc.xml"                >
    <!ENTITY wr                    SYSTEM "platforms/fgc3/memmaps_inc/dsp/wr.xml"                 >
]>

<!-- FGC3 DSP Memory Map Top level zone -->

<zone
    name	= ""
    offset	= "0x00000000B"
    endian	= "little"
    size	= "4G"
    comment	= "FGC3 DSP Memory Map"
>
    <doc><![CDATA[
	    The Texas Instruments TMS320C6727 Digital Signal Processor manages
	    the function generation, measurements and regulation in the FGC3.
	    Two chip selects are used:
    	<ul style="font-family:'Courier';font-size:80%">
    	    <li>0x80000000 - 0x8FFFFFFF : CS0 - 64MB SDRAM</li>
    	    <li>0x90000000 - 0x9FFFFFFF : CS2 - 20KB PLD for DPRAM and Peripherals</li>
    	</ul>
        The other processor in the FGC3.1 is a Renasas Rx610.  It has its own
        <a href="Platform_fgc3_memmap_MCU.htm">memory map here</a>.
    ]]></doc>

<!-- Include zones -->

    &dsp_rom;   <!-- TMS320C6727 internal ROM (256K + 128K)     -->
    &dsp_ram;   <!-- TMS320C6727 internal RAM (256K)            -->
    &sdram;     <!-- 64 MByte SDRAM                             -->
    &dpram;     <!-- TMS320C6727 & Rx610 dpram                  -->
    &mid;       <!-- Module IDentifier                          -->
    &irqctrl;   <!-- Interrupt Request Controller               -->
    &ana;       <!-- ANA interface                              -->
    &dig_sup;   <!-- DIG supplemental ports                     -->
    &emif;      <!-- EMIF interface                             -->
    &spivs;     <!-- SPI interface                              -->
    &spy;       <!-- SPY interface                              -->
    &bis;       <!-- BIS interface                              -->
    &time;      <!-- TIME registers                             -->
    &utc;       <!-- UTC registers                              -->
    &wr;        <!-- WhiteRabbit registers                      -->

    <zone
	name    = "DSP_GPIODAT1"
	offset  = "0x43000014B"
	size    = "1b"
	comment = "[DSP] GPIO to control the PLD DPRAM access from the DSP side"
    >
	<doc><![CDATA[
	    GPIO to control the [PLD] DPRAM access from the DSP side<br>
	    <ul>
		<li>1 : 32 bits access.</li>
		<li>0 : 16 bits access (default after reset).</li>
	    </ul>
	]]></doc>
    </zone>
</zone>

<!-- EOF -->
