<!--	FGC Non-volatile storage index memmap definition (FGC2) -->

<!-- NVS index @ 0xFE000 -->

<zone
    name    = "NVSIDX"
    offset  = "0xFE000B"
    size    = "0x1700B"
    consts  = "32bit sizewords"
    comment = "Non-volatile storage index zone"
>

    <doc><![CDATA[
    	This zone only contains the non-volatile property storage index.
    	<p><i>Note that in the FGC2 the FRAM can only be accessed by WORD -
    	BYTE access will result in a bus error!</i></p>
    ]]></doc>

    <zone
        name    = "MAGIC"
        offset  = "0x0000B"
        type    = "volatile uint16_t"
        size    = "2B"
        consts  = "16bit near"
        comment = "NVS is formatted magic number"
    >
        <doc><![CDATA[
            This location is set to a magic number (0x1566) when the non-volatile
            has been formatted within the NVS.  Clearing the magic number will retrigger
            the erasure of the non-volatile after the next reset.
        ]]></doc>
    </zone>

    <zone
        name    = "SWVER"
        offset  = "0x0002B"
        type    = "volatile uint32_t"
        size    = "4B"
        consts  = "near"
        comment = "Software version"
    >
        <doc><![CDATA[
            This location holds the main program version that configured the NVS.
            If the version running is different, then it must reformat the NVS while preserving
            the data for all non-volatile properties that continue to exist in the new version.
        ]]></doc>
    </zone>

    <zone
        name    = "NUMPROPS"
        offset  = "0x0006B"
        type    = "volatile uint16_t"
        size    = "2B"
        consts  = "near"
        comment = "Number of NVS properties in FRAM"
    >
        <doc><![CDATA[
            This location holds the number of NVS properties in the FRAM.
        ]]></doc>
    </zone>

    <zone
        name     = "REC"
        offset   = "0x0008B"
        size     = "40B"
        type	 = "volatile uint16_t"
        consts   = "32bit sizebytes sizewords"
        comment  = "NVS property index record"
    >
    <doc><![CDATA[
        This zone contains the first record of the NVS property index structures.
    ]]></doc>

        <zone
            name     = "PROP"
            offset   = "0x0B"
            size     = "2B"
            type     = "volatile uint16_t"
            consts   = "array"
            comment  = "Non-volatile property structure pointer"
        >
        <doc><![CDATA[
            This address maps the array of non-volatile property structures.
        ]]></doc>
        </zone>

        <zone
            name     = "DATA"
            offset   = "0x2B"
            size     = "4B"
            type     = "volatile uint32_t"
            consts   = "array"
            comment  = "Non-volatile property data address"
        >
        <doc><![CDATA[
            This address maps the array of pointers for the non-volatile property data records.
            The data contains either 1 or 64 data buffers
            according to whether the property is multiplexed or not.  Each data buffer has a two
            byte prefix containing the number of elements of data, followed by the data.  The
            data buffer must be word aligned to avoid bus errors.
        ]]></doc>
        </zone>

        <zone
            name     = "BUFSIZE"
            offset   = "0x6B"
            size     = "2B"
            type     = "volatile uint16_t"
            consts   = "array"
            comment  = "Non-volatile property size (bytes)"
        >
        <doc><![CDATA[
            This address maps the array of non-volatile property data buffer sizes.
            The data size BUFSIZE is equal to EL_SIZE*MAX_ELEMENTS+2 rounded up to the upper
            word length. The "+2" corresponds to the word containing the number of elements
            which preceeds the data itself.  For a PPM property there will be 32 blocks of
            BUFSIZE consecutively in the NVSDATA zone. For a non-PPM property there is
            just one. BUFSIZE must be an integer number of words to avoid bus errors so if the
            property data is an odd number of bytes in length, a padding byte is included.
        ]]></doc>
        </zone>

        <zone
            name     = "NAME"
            offset   = "0x8B"
            size     = "32B"
            type     = "volatile uint16_t"
            consts   = "32bit sizebytes sizewords"
            comment  = "Non-volatile property name"
        >
        <doc><![CDATA[
            This address maps the array of non-volatile property names.
        ]]></doc>
        </zone>

    </zone>

</zone>

<!-- EOF: nvsidx.xml -->
