<!--    HC16 Queued serial module (QSM) registers memmap definition

    qak   11/02/2004    Documentation added
-->

<!-- QSM ===================================================================================== -->

<zone
    name    = "QSM"
    offset  = "0xFFC00B"
    size    = "336B"
    comment = "HC16 Queued Serial Module registers"
>
<doc><![CDATA[
    The QSM is an on-chip sub-component of the HC16 processor.  It includes a
    conventional serial port used for the FGC RS232 interface, and a queued
    serial module port, used for the FGC diagnostic interface.  For full details,
    consult the Motorola MC68HC16Z1 User's manual.
]]></doc>

    <zone
        name     = "MCR"
        offset   = "0x0000B"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = ""
        comment  = "QSM module configuration"
    />
    <zone
        name     = "QILR"
        offset   = "0x0004B"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = ""
        comment  = "QSM interrupt level register"
    />
    <zone
        name     = "QIVR"
        offset   = "0x0005B"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = ""
        comment  = "QSM interrupt vector register"
    />
    <zone
        name     = "SCCR0"
        offset   = "0x0008B"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "16bit near"
        comment  = "SCI control 0 (Baud rate control)"
    >
        <doc><![CDATA[
            This register sets the baud rate for the HC16 SCI serial interface.
            The value should be set using the formula:
            <ul>
            <li> SCCR0 = Fclk / (32 * baud_rate)
            </ul>
            where Fclk is the system clock frequency.  For the FGC, Fclk=16M, baud_rate=9600, which
            means that SCCR0 should be set to 52 (0x34).
        ]]></doc>
    </zone>
    <zone
        name     = "SCCR1"
        offset   = "0x000AB"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "16bit near"
        comment  = "SCI control 1"
    >
        <doc><![CDATA[
            This register configures the HC16 SCI serial interface.
            For the FGC, the SCI should work with Tx and Rx enabled, no parity,
            1 start & 1 stop bit.  For this the register should be set to 0x000C.
        ]]></doc>
    </zone>
    <zone
        name     = "SCSR"
        offset   = "0x000CB"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "16bit near"
        comment  = "SCI status word"
    />
    <zone
        name     = "SCSRH"
        offset   = "0x000CB"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "16bit near"
        comment  = "SCI status word high byte"
    >
        <zone
            name     = "TDRE"
            offset   = "0b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "Transmit Data Register Empty"
        >
            <doc><![CDATA[
                This bit of the SCI Status Register indicates the state
                of the Transmit Data Register:
                <ul>
                <li> 0 = Transmit Data Register still contains data to be sent
                <li> 1 = Transmit Data Register is empty
                </ul>
            ]]></doc>
        </zone>
    </zone>
    <zone
        name     = "SCSRL"
        offset   = "0x000DB"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "16bit near"
        comment  = "SCI status word low byte"
    >
        <zone
            name     = "RDRF"
            offset   = "6b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "Receive Data Register Full"
        >
            <doc><![CDATA[
                This bit of the SCI Status Register indicates the state
                of the Receive Data Register:
                <ul>
                <li> 0 = Receive Data Register contains no new data
                <li> 1 = Receive Data Register contains new data
                </ul>
            ]]></doc>
        </zone>
    </zone>
    <zone
        name     = "SCDR"
        offset   = "0x000EB"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = ""
        comment  = "SCI data register word"
    />
    <zone
        name     = "SCDRL"
        offset   = "0x000FB"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "16bit near"
        comment  = "SCI data register low byte"
    />
    <zone
        name     = "PORTQS"
        offset   = "0x0015B"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "16bit near"
        comment  = "Port Q data"
    >
        <zone
            name     = "SPAREOUT"
            offset   = "1b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "MOSI: Diag spare output"
        >
            <doc><![CDATA[
                The QSPI MOSI (Master Output Slave Input) is driven by the QSM
                (which is running as the serial master) when performing a scan.
                For testing purposes between scans it can be set/reset using a
                boot menu function.
            ]]></doc>
        </zone>

        <zone
            name     = "CLK"
            offset   = "2b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "SCK: QSPI clock"
        >
            <doc><![CDATA[
                The QSPI SCK (Serial Clock) is driven by the QSM when performing a scan.
                Between scans, it can be set/reset to reset all the DIMs or using a boot
                menu function for testing purposes.
            ]]></doc>
        </zone>

        <zone
            name     = "DIPSDIMAD3"
            offset   = "4b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "PCS1: DIPS DIM ADC Address 3"
        >
            <doc><![CDATA[
                The PCS1 line is used as an output to control the top address
                line of the ADC used by the DIM on the DIPS board.  The ADC
                has an 8-way multiplexor, but on DIM boards, only 4 inputs are
                used, so the AD3 line is not normally controlled.  On the DIPS
                boards, however, all eight channels are used, and the HC16 is
                responsible for setting the top address bit using this output.
                Note that the ADC latches the multiplexer channel one sample
                in advance.
            ]]></doc>
        </zone>

        <zone
            name     = "DIAGBUSA"
            offset   = "5b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "PCS2: Diag Bus A select"
        >
            <doc><![CDATA[
                This QSPI chip select line is used to enable the Diag Bus A when high.
                It is routed to the DIPS board via internal link line 1 and can be
                read back via the secondary digital input
                <A HREF="Platform_fgc2_memmap_MCU_DIG_IP2_PCS2.htm">PCS2</A>.
                <p>
                Both buses can be selected at the same time to assert a reset on both
                buses simultaneously.
            ]]></doc>
        </zone>

        <zone
            name     = "DIAGBUSB"
            offset   = "6b"
            size     = "1b"
            type     = "volatile uint8_t"
            consts   = "mask8"
            comment  = "PCS3: Diag Bus B select"
        >
            <doc><![CDATA[
                This QSPI chip select line is used to enable the Diag Bus B when high.
                It is routed to the DIPS board via internal link line 0 and can be
                read back via the secondary digital input
                <A HREF="Platform_fgc2_memmap_MCU_DIG_IP2_PCS3.htm">PCS3</A>.
                <p>
                Both buses can be selected at the same time to assert a reset on both
                buses simultaneously.
            ]]></doc>
        </zone>
    </zone>

    <zone
        name     = "PQSPAR"
        offset   = "0x0016B"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "near"
        comment  = "Port Q pin assignment"
    />
    <zone
        name     = "DDRQS"
        offset   = "0x0017B"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "near"
        comment  = "Port Q data direction"
    />
    <zone
        name     = "SPCR0"
        offset   = "0x0018B"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "near"
        comment  = "SPI control 0"
    />
    <zone
        name     = "SPCR1"
        offset   = "0x001AB"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "near"
        comment  = "SPI control 1"
    />
    <zone
        name     = "SPCR2"
        offset   = "0x001CB"
        size     = "2B"
        type     = "volatile uint16_t"
        consts   = "near"
        comment  = "SPI control 2"
    />
    <zone
        name     = "SPCR3"
        offset   = "0x001EB"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = ""
        comment  = "SPI control 3"
    />
    <zone
        name     = "SPSR"
        offset   = "0x001FB"
        size     = "1B"
        type     = "volatile uint8_t"
        consts   = "near"
        comment  = "SPI status"
    />
    <zone
        name     = "RR"
        offset   = "0x0100B"
        size     = "32B"
        type     = "volatile uint16_t"
        consts   = "16bit 32bit near"
        comment  = "Receive data RAM  [16]"
    >
        <zone
            name     = "CH1"
            offset   = "2B"
            size     = "2B"
            type     = "volatile uint16_t"
            consts   = "near"
            comment  = "Receive data RAM channel 1"
        />
    </zone>
    <zone
        name     = "TR"
        offset   = "0x0120B"
        size     = "32B"
        type     = "volatile uint16_t"
        consts   = "array"
        comment  = "Transmit data RAM [16]"
    />
    <zone
        name     = "CR"
        offset   = "0x0140B"
        size     = "16B"
        type     = "volatile uint8_t"
        consts   = "array"
        comment  = "Command RAM [16]"
    />
</zone>

<!-- EOF -->
