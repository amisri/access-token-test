<?xml version='1.0'?>

<!-- PLEASE KEEP SET FUNCTIONS IN ALPHABETICAL ORDER -->

<set
    pseudoroot = "1"
>
    <set
        name    = "AbsTime"
        title   = "Set Absolute Time property"
        types   = "ABSTIME ABSTIME8"
    >
        <doc><![CDATA[
            This function is specific to {TYPE ABSTIME} or {TYPE ABSTIME8} (absolute time)
            properties.  The default value will always be the {PROP TIME.NOW}
            property, so:
            <pre>
            S {PROP REF.RUN}
            </pre>
            will set {PROP REF.RUN} to the time now, which in this
            case will start a reference change if the power converter state is ARMED.</p>
            <p>The full absolute time value can be supplied:
            <pre>
            S {PROP REF.RUN} 1162113608.245000
            </pre>
            or in scientific format:
            <pre>
            S {PROP REF.RUN} 1.1621136082456425e+09
            </pre>
            Absolute time is in seconds, with microsecond resolution. When connected to the
            Gateway, absolute time will be Unix time, otherwise it is the time since the last
            reset or restart of the FGC.
        ]]></doc>
    </set>

    <set
        name    = "AnaVrefTemp"
        title   = "Set Analogue Card Vref Target Temperature (FGC3)"
        types   = "FLOAT"
        getopts = "TYPE RANGE"
    >
        <doc><![CDATA[
            This function is specific to {PROP ADC.INTERNAL.VREF_TEMP.REF} on FGC3.
        ]]></doc>
    </set>

    <set
        name    = "Bin"
        title   = "Set binary properties"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function allows raw binary data to be written to a property.
        ]]></doc>
    </set>

    <set
        name    = "Cal"
        title   = "Set Calibration properties"
        types   = "FLOAT PARENT"
    >
        <doc><![CDATA[
            This function is specific to DAC, ADC and DCCT calibration error properties.
        ]]></doc>
    </set>

    <set
        name    = "CalDccts"
        title   = "Set Calibration properties for DCCTs"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function is specific to DCCT calibration error properties.
        ]]></doc>
    </set>

    <set
        name    = "Char"
        title   = "Set Character property"
        types   = "CHAR"
    >
        <doc><![CDATA[
            This function is specific to {TYPE CHAR} properties.  It allows an array of characters
            to be written into a property, or part of a property. Restrictions:
            <ul>
            <li>The array may not contain spaces.</li>
            <li>Chars can be comma-delimited or not (a string-like parameter is treated as a single token, see below)</li>
            <li>A single token cannot be longer than 32 chars (current token buffer size)</li>
            <li>Total number of chars cannot be greater than the array size</li>
            <li>The function follows the standard array specifier rules, however,
                the start of the array range may not be higher than the number of elements in the property.
                This prevents holes from being created in the data.</li>
            </ul>

            Here are some examples:
            <pre>
             S {PROP DEVICE.NAME} TestString
             G {PROP DEVICE.NAME}
             TestString
             S {PROP DEVICE.NAME}[4,] Message
             G {PROP DEVICE.NAME}
             TestMessage
             S {PROP DEVICE.NAME}[0,3] Last
             G {PROP DEVICE.NAME}
             LastMessage;
            </pre>
            The last example illustrates how part of a string may be changed.  For this to work
            the number of characters supplied must match the number of characters in the array range.
        ]]></doc>
    </set>

    <set
        name    = "Config"
        title   = "Local configuration control"
        types   = "PARENT"
    >
        <doc><![CDATA[
            This function provides support for the management of the local configuration data.
            It can be used to prepare the FGC to erase its configuration in the case where
            it has become corrupted, or to cancel the erase request.
        ]]></doc>
    </set>

    <set
        name    = "Core"
        title   = "Activate core dump"
        types   = "PARENT"
    >
        <doc><![CDATA[
            This function is specific to the {PROP FGC.DEBUG} property and will activate the
            core dump system.  It will first check if the total length of all the
            memory segments specified by {PROP FGC.DEBUG.CORE_LEN_W} is greater than the space
            available (4KB) and if so, it will reset the lengths to zero, which
            deactivates the core dump system.  If the lengths are valid, then it will
            record the table in the FRAM memory which will activate the system.
        ]]></doc>
    </set>

    <set
        name    = "Defaults"
        title   = "Assert default values"
        types   = "PARENT NULL"
    >
        <doc><![CDATA[
            This function is used to set the reference/function properties to their
            default values.
        ]]></doc>
    </set>

    <set
        name    = "DevicePPM"
        title   = "Device PPM control"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function is specific to the {PROP DEVICE.PPM} property.  It allows the
            device to be enabled or disabled for PPM operation.
        ]]></doc>
    </set>

    <set
        name    = "DevName"
        title   = "handling of inter-FGC device names"
        types   = "DEV_NAME INT16U"
    >
        <doc><![CDATA[
            This function is specific to the Inter-FGC properties that store device addresses.
        ]]></doc>
    </set>

    <set
        name    = "DiagChan"
        title   = "Diagnostic channel"
        types   = "NULL INT32U"
    >
        <doc><![CDATA[
            This function sets a diagnostic channel.
        ]]></doc>
    </set>

    <set
        name    = "DirectCommand"
        title   = "Control Direct Digital Command outputs"
        types   = "INT16U"
    >
        <doc><![CDATA[
            This function is specific to the specialist {PROP DIG.COMMANDS}
            property. It provides a way to manually write to the Direct Command register. There are 8
            command lines, of which six are allocated.
            Control of the command register is via a set/reset register pair. To set the bit in the
            command register, the equivalent bit must be set in the set register, and to clear it,
            the equivalent bit must be set in the reset register. In both cases, an unlock
            sequence is written first and the operational state {PROP STATE.OP}
            must not be SIMULATION for the request to be validated.
            The command accepts an unsigned 16 bit integer parameter. The low byte is the set mask,
            and the high byte is the reset mask, so it's easiest to enter the value in hex
            (prefix with "0x" or "0X").
            Note that the hardware does not allow bits to be set and reset at the same time
            so low and high bytes may not both be non-zero.

        ]]></doc>
    </set>

    <set
        name    = "Event"
        title   = "Send a WorldFIP event"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function sends an event over the fieldbus for testing.
        ]]></doc>
    </set>

    <set
        name    = "FGCCode"
        title   = "FGC code"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function is used to set code for download to the FGC's via the WorldFIP.
        ]]></doc>
    </set>

    <set
        name    = "FGCLogSync"
        title   = "Send FGC log synchronisation signal"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function sends the FGC log synchronisation signal.
        ]]></doc>
    </set>

    <set
        name    = "FgFunc"
        title   = "Set function generator"
        types   = "PARENT"
    >
        <doc><![CDATA[
            This function is specific to the {PROP FGFUNC} property.
            It is rather special because it effectively enables the user to set a
            composite property, for example, all the sub-properties of {PROP FGFUNC.PLP} can
            be set with one command.  This is provided to make it quicker to operate an FGC
            via the serial port.  It is not normally used for applications that communicate
            via the WorldFIP.
        ]]></doc>
    </set>

    <set
        name    = "FgFuncType"
        title   = "Set function generator type"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function is specific to the {PROP FGFUNC.TYPE} property.
            It will attempt to arm the function generator for a given channel using the specified function.
        ]]></doc>
    </set>

    <set
        name    = "Fifo"
        title   = "Flush FIFO log"
        types   = "FIFOADC"
    >
        <doc><![CDATA[
            This function ignores any argments and flushes the Fifo log.
        ]]></doc>
    </set>

    <set
        name    = "Float"
        title   = "Set floating point property"
        types   = "FLOAT FL_POINT POINT"
    >
        <doc><![CDATA[
            This function is specific to {TYPE FLOAT} properties:
            <ul>
                <li>    If the property size is greater than 1 element then multiple values may be supplied
                        and should be comma separated.</li>
                <li>    Values must be decimal and comply with the standard floating point respresentation,
                        e.g. -1.234E-14.</li>
                <li>    All numeric values must be within the min-max limits for the property.</li>
                <li>    If a value is missed, the corresponding element in the property is left unchanged.</li>
                <li>    If an array range is not specified:

                <ul>
                        <li>    the first value will set the first element of the property, and so on.</li>
                        <li>    the length of the property will be set to the number of
                                values supplied (including "missing values"), unless
                                the {FLAG DONT_SHRINK} flag is set.</li>
                </ul>
                </li>
                <li>    If an array range is specified:
                <ul>
                        <li>    the number of values supplied (including "missing values") may not
                                exceed the length of the array range (end-start+1).</li>
                        <li>    if the number of values supplied (including "missing values") equals
                                the length of the array range, then the length of the property will
                                not be reduced.</li>
                        <li>    if the number of values supplied (including "missing values") is less
                                than the length of the array range, then the length of the property will
                                be set so that the last value supplied becomes the last value in the
                                property (unless the {FLAG DONT_SHRINK} flag is set).</li>
                        <li>    if the start of the array range is greater than the length of the property
                                then the values in the "hole" will be cleared to zero.</li>
                </ul>
                </li>
            </ul>
        ]]></doc>
    </set>

    <set
        name    = "ConfigMode"
        title   = "Sets the configuration mode"
        types   = "INT16U"
    >
        <doc><![CDATA[This function is used to configure either the FGC or the DB]]></doc>
    </set>

    <set
        name    = "Id"
        title   = "Set ID"
        types   = "STRING"
    >
        <doc><![CDATA[
            This function is specific to setting the associations between the Dallas IDs and the barcodes.
        ]]></doc>
    </set>

    <set
        name    = "Integer"
        title   = "Set integer property"
        types   = "INT8S INT8U INT16S INT16U INT32S INT32U"
    >
        <doc><![CDATA[
            This function is generic to all standard integer properties:
            <ul>
                    <li>    If the property size is greater than 1 element then multiple values may be supplied
                            and should be comma separated.</li>
                    <li>    If the property has the {FLAG SYM_LST} flag set, the values
                            must be symbolic (case is not significant).</li>
                    <li>    If the property does <EM>not</EM> have the {FLAG SYM_LST}">SYM_LST</A> flag
                            set, the values must be numeric:
                    <ul>
                            <li>    decimal is the default for all types.</li>
                            <li>    hexadecimal is acceptable for the unsigned types {TYPE INT8U}">INT8U</A>,
                                    {TYPE INT16U}">INT16U</A> and {TYPE INT32U}">INT32U</A>, using
                                    the "0x" or "0X" prefix.</li>
                            <li>    All numeric values must be within the min-max limits for the property.</li>
                    </ul>
                    </li>
                    <li>    If a value is missed, the corresponding element in the property is left unchanged.</li>
                    <li>    If an array range is not specified:
                    <ul>
                            <li>    the first value will set the first element of the property, and so on.</li>
                            <li>    the length of the property will be set to the number of
                                    values supplied (including "missing values"), unless
                                    the {FLAG DONT_SHRINK}">DONT_SHRINK</A> flag is set.</li>
                    </ul>
                    </li>
                    <li>    If an array range is specified:
                    <ul>
                            <li>    the number of values supplied (including "missing values") may not
                                    exceed the length of the array range (end-start+1).</li>
                            <li>    if the number of values supplied (including "missing values") equals
                                    the length of the array range, then the length of the property will
                                    not be reduced.</li>
                            <li>    if the number of values supplied (including "missing values") is less
                                    than the length of the array range, then the length of the property will
                                    be set so that the last value supplied becomes the last value in the
                                    property (unless the {FLAG DONT_SHRINK}">DONT_SHRINK</A> flag is set).</li>
                            <li>    if the start of the array range is greater than the length of the property
                                    then the values in the "hole" will be cleared to zero.</li>
                    </ul>
                    </li>
            </ul>
        ]]></doc>
    </set>

    <set
        name    = "InternalAdcMpx"
        title   = "Set Internal ADC Multiplexing"
        types   = "INT16U"
    >
        <doc><![CDATA[
            This function is specific to the property {PROP ADC.FILTER.MPX}.
        ]]></doc>
    </set>

    <set
        name    = "LoadSwitch"
        title   = "Control load switch"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function is used to move load switches.
        ]]></doc>
    </set>

    <set
        name    = "LogCaptureUser"
        title   = "Set log capture user"
        types   = "INT32S"
    >
        <doc><![CDATA[
            This function is specific to the property {PROP LOG.CAPTURE.USER}.
        ]]></doc>
    </set>

    <set
        name    = "PC"
        title   = "Control power converter state machine"
        types   = "INT8U INT16U"
    >
        <doc><![CDATA[
            This function is specific to the {PROP STATE.PC} property (and its alias {PROP PC}).
            It allows the user to specify a target state for the Converter state machine.
            Allowed target states are: OFF (OF), ON_STANDBY (SB), IDLE (IL), CYCLING (CY)
            and SLOW_ABORT (SA). For example:
            <pre>
            S {PROP STATE.PC} OFF
            S {PROP STATE.PC} ON_STANDBY
            S {PROP STATE.PC} IDLE
            </pre>
            The same targets work with the alias {PROP PC}, which is provided to make
            it quicker to type the commands when working via a terminal. In addition, the abbreviated state
            names are accepted, for example:
            <pre>
            S {PROP PC} OF
            S {PROP PC} SA
            S {PROP PC} CY
            </pre>
            Note that the target state may be impossible to reach at a given moment, depending
            upon the current state and other external factors (e.g. external interlocks, machine
            state, voltage source state). In some cases the attempt to set the property may be
            rejected, and in others the Controller may attempt to move to the target state, but
            fail due external factors.
        ]]></doc>
    </set>

    <set
        name    = "PCSimplified"
        title   = "Control power converter simplified state machine"
        types   = "INT8U INT16U"
    >
        <doc><![CDATA[
            This function is specific for the properties {PROP STATE.PC_SIMPLIFIED} and
            {PROP MODE.PC_SIMPLIFIED}.
            It allows the user to specify a simplified target state for the Converter state machine.
            Allowed target states are: OFF (OF, ST, SP), BLOCKING (BK) and ON (SA, TS, SB, IL, AR,
            RN ,AB ,TC, CY, PL). For example:
            <pre>
            S {PROP STATE.PC_SIMPLIFIED} OFF
            S {PROP STATE.PC_SIMPLIFIED} BLOCKING
            S {PROP STATE.PC_SIMPLIFIED} ON
            </pre>
            The same targets work with the abbreviated state names, for example:
            <pre>
            S {PROP STATE.PC_SIMPLIFIED} OF
            S {PROP STATE.PC_SIMPLIFIED} BK
            S {PROP STATE.PC_SIMPLIFIED} ON
            </pre>
            Note that the target state may be impossible to reach at a given moment, depending
            upon the current state and other external factors (e.g. external interlocks, machine
            state, voltage source state). In some cases the attempt to set the property may be
            rejected, and in others the Controller may attempt to move to the target state, but
            fail due external factors.
        ]]></doc>
    </set>

    <set
        name    = "PeriodIters"
        title   = "Set regulation period"
        types   = "INT32U"
    >
        <doc><![CDATA[
            Validate and set {PROP REG.I.PERIOD_ITERS}.
        ]]></doc>
    </set>

    <set
        name    = "Point"
        title   = "Set Point property"
        types   = "POINT FL_POINT"
    >
        <doc><![CDATA[
            This function is specific to {TYPE POINT} properties and {TYPE FL_POINT} properties. A point combines a time and
            a reference value which must be delimited by a pipe character (|) with no spaces.
            For example: 1.2345|-4.25E+3.
        ]]></doc>
    </set>

    <set
        name    = "PolaritySwitch"
        title   = "Control polarity switch"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function is used to move polarity switches.
        ]]></doc>
    </set>

    <set
        name    = "RBACToken"
        title   = "Set RBAC token"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function allows the setting of tokens for Role-Based Access Control (RBAC) for TCP clients.
        ]]></doc>
    </set>

    <set
        name    = "RawParameter"
        title   = "Set raw REGFGC3 parameters"
        types   = "INT32U INT32S FLOAT"
        getopts = "TYPE IDX NOIDX HEX RANGE"
    >
        <doc><![CDATA[
            This function is specific to REGFGC3 parameter properties.  It will
            set raw value of a given block at a given slot
        ]]></doc>
    </set>

    <set
        name    = "RegFgc3CrateCacheScan"
        title   = "Scans RegFGC3 crate and builds parameters cache"
        types   = "STRING"
    >

        <doc><![CDATA[
            This function triggers the scan of the RegFGC3 crate and also builds the RegFGC3 parameter cache.
        ]]></doc>
    </set>

    <set
        name    = "RegFgc3ProgMode"
        title   = "Sets the reprogramming mode"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function sets the property REGFGC3.PROG.MODE to one of the following
            allowed values: PROG_NONE (when reprogramming was successful) or PROG_FAILED (when it wasn't).
            PROG_NONE clears the PROGRAM warning. PROG_FAILED sets a XXXXX fault in the FGC3.
            Both values clear the SYNC_REGFGC3 flag, so that the program manager does not take into consideration
            a faulty FGC3.
            ]]>
        </doc>
    </set>

    <set
        name    = "RegFgc3ProgFsmMode"
        title   = "Sets the reprogramming FSM mode"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function sets the property REGFGC3.PROG.FSM.MODE to the given value.
            This property reflects the transition of the RegFGC3 reprogramming FSM.
            Before setting it, the target state is validated against the possible values in regfgc3_state.xml symlist.
            The function also makes sure that the transition from the current to the target state is allowed.
            ]]>
        </doc>
    </set>

    <set
        name    = "RegFgc3ProgAction"
        title   = "Individual actions on cards"
        types   = "INT16U"
    >
        <doc><![CDATA[
            Added temporarily for the RegPM commissioning
            ]]>
        </doc>
    </set>

    <set
        name    = "ReadCodes"
        title   = "Read FGC codes"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function re-reads the FGC code files into the gateway.
        ]]></doc>
    </set>

    <set
        name    = "ReadNames"
        title   = "Read device names"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function re-reads device names into the gateway.
        ]]></doc>
    </set>

    <set
        name    = "ReadRBAC"
        title   = "Read RBAC access map"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function re-reads a RBAC access map into the gateway.
        ]]></doc>
    </set>

    <set
        name    = "ReadUsers"
        title   = "Read allowed user names"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function re-reads lists of allowed users into the gateway.
        ]]></doc>
    </set>

    <set
        name    = "Ref"
        title   = "Set reference function"
        types   = "PARENT INT32U"
    >
        <doc><![CDATA[
            <p>This function is used by the {PROP REF} and {PROP REF.FUNC.TYPE} properties.
            It is special because it enables the user to set a
            composite property, for example, all the sub-properties of {PROP REF.PLEP} can
            be set with one command:</p>
            <p style="text-align:center"><b>S REF PLEP, <i>AMPLITUDE, ACCELERATION, LINEAR_RATE, EXP_TC, EXP_FINAL</i></b></p>
            <p>This is provided to make it quicker to operate an FGC via the terminal.
            This feature is not normally used for applications that communicate via the fieldbus.</p>
        ]]></doc>
    </set>

    <set
        name    = "RefControlValue"
        title   = "Set the control value when running in Direct"
        types   = "FLOAT INT32U"
    >
        <doc><![CDATA[
            Verifies that the control value ({PROP REF.DIRECT.B.VALUE}, {PROP REF.DIRECT.I.VALUE} or {PROP REF.DIRECT.V.VALUE})
            are within the configured limits.
        ]]></doc>
    </set>

    <set
        name    = "RefPulse"
        title   = "Set the pulse reference"
        types   = "FLOAT"
        getopts = "TYPE NOIDX HEX RANGE"
    >
        <doc><![CDATA[
            This function verifies that the pulse reference (either {PROP.REF.PULSE.REF}
            or {PROP REF.PULSE.REF_LOCAL}) falls within the limits specified by the limits
            {PROP LIMITS.PULSE.POS} > ref > {PROP LIMITS.PULSE.MIN}, whereas negative
                values must be within the following limits
                {PROP LIMITS.PULSE.NEG} < ref < {PROP LIMITS.PULSE.MIN}.
        ]]></doc>
    </set>

    <set
        name    = "PulseOffset"
        title   = "Set the pulse offset in microseconds"
        types   = "INT32S"
        getopts = "TYPE NOIDX HEX RANGE"
    >
        <doc><![CDATA[
            This function sets the pulse offset in microseconds with respect
            to the event time. It is needed to force storing the value in NVS
            since {PROP REF.PULSE.OFFSET_US} is a transactional property.
        ]]></doc>
    </set>

    <set
        name    = "RegMode"
        title   = "Set regulation mode"
        types   = "INT32U"
        getopts = "TYPE NOIDX HEX RANGE"
    >
        <doc><![CDATA[
            The function sets regulation mode.
        ]]></doc>
    </set>

    <set
        name    = "RelTime"
        title   = "Set relative time property"
        types   = "FLOAT"
    >
        <doc><![CDATA[
            This function is specific to relative time properties. This
            data type holds a time in seconds within the range 0-400000 with 0.1ms resolution.
            For example:
            <pre>
            S {PROP REF.RUN_DELAY} 1.2345
            </pre>
        ]]></doc>
    </set>

    <set
        name    = "Reset"
        title   = "Reset component"
        types   = "PARENT INT16U"
    >
        <doc><![CDATA[
            This function is used to reset a component.  It is used by:
            <ul>
            <li>{PROP CONFIG.STATE}- Reset {PROP CONFIG.MODE}.  This can only be done by the FGC manager.</li>
            <li>{PROP DIAG}- Reset diagnostic bus.  This is done automatically following a DIM trigger.</li>
            <li>{PROP FGC.DLS} - Reset Dallas ID bus system.  This will clear {PROP FGC.DLS} error properties.</li>
            <li>{PROP FGSTATUS} - Reset Function Generator faults. This is specified to class 59.</li>
            <li>{PROP FGSTATS} - Reset Function Generator interface. This is specified to class 59.</li>
            <li>{PROP CODE.FLASH_LOCKS} - Reset code flash locks.  After the next reset all the locks will be removed.</li>
            <li>{PROP FGC.ID} - Reset the ID system.  The Dallas bus will be rescanned.  This will leave
            the system unsynchronised.</li>
            <li>{PROP LOG} - Reset POST_MORTEM bit in unlatched status.
                This is done automatically by the gateway once the PM logs have been read</li>
            <li>{PROP FGC.NUM_RESETS} - Reset the device reset counters.</li>
            <li>{PROP SPY} - Reset {PROP SPY.MPX} to the default values</li>
            <li>{PROP STATUS.ST_LATCHED} - Reset the latched status bits</li>
            <li>{PROP PAL} - Reset the PAL link</li>
        <li>{PROP VS} - Reset the voltage source</li>
            </ul>
            In all cases the command must be given with the value "RESET", for example:
            <pre>
            S {PROP LOG} RESET
            </pre>
        ]]></doc>
    </set>

    <set
        name    = "ResetEpicFaults"
        title   = "Reset the latched faults"
        types   = "INT8U"
    >
        <doc><![CDATA[
            This function is specific to the EPIC.DEVICE_n properties.
        ]]></doc>
    </set>

    <set
        name    = "ResetFaults"
        title   = "Reset the latched faults and send reset signal to VS"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function is used to reset internal and external faults
            by typing S {PROP VS.RESET}. It is the same as typing S {PROP VS} RESET.
        ]]></doc>
    </set>

    <set
        name    = "Rterm"
        title   = "Start rterm"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function starts a remote terminal between the calling client and a channel.  Binary data may not be
            transmitted over a terminal because the rterm close character will cause a transmission to stop prematurely.
        ]]></doc>
    </set>

    <set
        name    = "RtermLock"
        title   = "Start a locked rterm"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function starts a remote terminal between the calling client and a channel.  The rterm cannot be closed
            without closing the connection (is locked).  This allows binary data to be transmitted over the terminal.
        ]]></doc>
    </set>

    <set
        name    = "SubPeriod"
        title   = "UDP subscription period"
        types   = "INT32S"
    >
        <doc><![CDATA[
            This function is used to set a UDP subscription period.
        ]]></doc>
    </set>

    <set
        name    = "TableFunc"
        title   = "Set REF.TABLE.FUNC.VALUE property"
        types   = "POINT FL_POINT"
    >
        <doc><![CDATA[
            <p>This function sets and arms the TABLE function or function list.
            It takes an array of {TYPE POINT}s or {TYPE FL_POINT}s. The
            first element of each point is the reference time in seconds. The second element of each
            point is the reference value in physical units (amps, gauss or volts according to the
            regulation mode).</p>
            <p>The function performs the following steps:</p>
            <ol>
            <li>Parse the function data and insert into {PROP REF.TABLE.FUNC.VALUE} and {PROP REF.TABLE.FUNCLIST}</li>
            <li>Attempt to arm the function/function list.</li>
            <li>If arming fails (and we are not in IDLE state), roll back to the previous function data</li>
            </ol>
            <p>When arming the table in IDLE state, the reference value of the first point must be
            within 1.0E-5 of the positive limit of the actual reference value. The time of the
            first point must always be zero and the time of all the following points must
            increment by at least the iteration period of the FGC, except when it is the first point of a function in
            function list. This is indicated by repeating the time of the previous point.
            The level of the reference and the rate of change of every segment will be checked
            against the appropriate I, B, V, dI/dt, dB/dt and dV/dt limits.</p>
        ]]></doc>
    </set>


    <set
        name    = "Terminate"
        title   = "Terminate device control"
        types   = "NULL"
    >
        <doc><![CDATA[
            This function is used by the control properties that can terminate
            the operation of the device:
            <ul>
            <li>{PROP DEVICE.RESET}</li>
            <li>{PROP DEVICE.BOOT}</li>
            <li>{PROP DEVICE.PWRCYC}</li>
            <li>{PROP DEVICE.CRASH}</li>
            </ul>
        ]]></doc>
    </set>

    <set
        name    = "GwTerminate"
        title   = "Reset device"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function is like SetTerminate, except that it is a function in the gateway which
            can reset or power cycle a FGClite device (even one that is offline).
        ]]></doc>
    </set>

    <set
        name    = "VsFiring"
        title   = "VS firing mode"
        types   = "INT32U"
    >
        <doc><![CDATA[
            This function encodes the VS firing mode in DIG SUP.
        ]]></doc>
    </set>

    <set
        name    = "CycSim"
        title   = "Cycle simulation setting"
        types   = "INT16U"
    >
        <doc><![CDATA[
            This function is used to simulate a super-cycle.
        ]]></doc>
    </set>

    <set
        name    = "TransactionAckCommitFail"
        title   = "Acknowledge the failure of a commit of a transaction"
        types   = "INT16U NULL"
    >
        <doc><![CDATA[
            This function acknowledges that the commit of a transaction failed and resets the transaction state.
        ]]></doc>
    </set>

    <set
        name    = "TransactionCommit"
        title   = "Commit a transaction"
        types   = "INT16U NULL"
    >
        <doc><![CDATA[
            This function commits a transaction.
        ]]></doc>
    </set>

    <set
        name    = "TransactionCommitTest"
        title   = "Test a transaction commit event"
        types   = "INT16U"
    >
        <doc><![CDATA[
            This function tests a transaction commit as if it came from a timing event.
        ]]></doc>
    </set>

    <set
        name    = "TransactionId"
        title   = "Transaction ID"
        types   = "INT16U NULL"
    >
        <doc><![CDATA[
            This function sets the transaction ID and opens a new transaction.
        ]]></doc>
    </set>

    <set
        name    = "TransactionRollback"
        title   = "Rollback a transaction"
        types   = "INT16U NULL"
    >
        <doc><![CDATA[
            This function rolls back transactional settings and aborts a transaction.
        ]]></doc>
    </set>

    <set
        name    = "TransactionTest"
        title   = "Test a transaction"
        types   = "INT16U NULL"
    >
        <doc><![CDATA[
            This function tests whether the settings within a transaction are expected to succeed.
        ]]></doc>
    </set>

    <set
        name    = "Dpram"
        title   = "Set integer to DPRAM memoryproperty"
        types   = "INT32U"
    >
          <doc><![CDATA[
            This function allows to overwrite an element located in DPRAM. Only used for debugging.
        ]]></doc>
    </set>


</set>

<!-- PLEASE KEEP SET FUNCTIONS IN ALPHABETICAL ORDER -->

<!-- EOF -->
