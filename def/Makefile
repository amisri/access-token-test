# -----------------------------------------------------------------------------------------------------------
# Filename: Makefile
#
# Purpose:  Makefile for def
# -----------------------------------------------------------------------------------------------------------

build_home        = ..

# Path to modules to be used by the parser and perl scripts

perl_lib_path     = ../sw/clients/perl:../sw/lib/extern/perl
pyparser          = $(build_home)/def/pyparser
fgc_parser_schemas= $(build_home)/sw/clients/python/fgc_parser_schemas

# Directories for automatically generated files

PARSE_HEADER_ROOT      = $(build_home)/sw/inc
PARSE_PERL_ROOT        = $(build_home)/sw/clients/perl/FGC
PARSE_PYTHON_ROOT      = $(build_home)/sw/clients/python/fgc/fgc
PARSE_PYTHON_DEC_ROOT  = $(build_home)/sw/clients/python/pyfgc_decoders/pyfgc_decoders
UTILITIES_FGC_DIR      = $(build_home)/sw/utilities/perl/databases/fgc
UTILITIES_CTRL_DIR     = $(build_home)/sw/utilities/perl/databases/controls
CODE_PATH              = $(build_home)/sw/fgc/codes

EPICS_UDP_DB_DIR       = $(build_home)/sw/kt/fgcepics/fgcudpSup/Db
EPICS_UDP_SRC_DIR      = $(build_home)/sw/kt/fgcepics/fgcudpSup/src
EPICS_CMD_DB_DIR       = $(build_home)/sw/kt/fgcepics/fgccmdSup/Db

TANGO_DEFS_DIR         = $(build_home)/sw/kt/fgctango/fgctango/defs


# Generated REG and EPIC property files

PROPS_INC_PATH    = src/properties/inc
REG_B_YAML         = $(PROPS_INC_PATH)/reg/reg_b.yaml
EPIC_DEVICE_YAMLS  = $(foreach var,1 2 3 4 5 6 7 8,$(PROPS_INC_PATH)/epic/device_$(var).yaml)
CLASS_65_EPIC_DEVICES = src/classes/65/epic/devices.yaml

# assemble a list of all parser generated files for cleanup

CLASS_GEN_FILES         = *_stat.h defconst.h defconst_assert.h definfo.h defprops.h defprops*.h defsyms.h fgc_stat_consts.h pmx_*.h sub_defconst.h sub_defsyms.h spy_class.h
COMMON_GEN_FILES        = fgc_codes_gen.h fgc_consts_gen.h fgc_errs.h fgc_parser_consts.h fgc_runlog_entries.h
PLATFORMS_GEN_FILES     = memmap_*.h menuconsts.h menutree.h
UTILITIES_GEN_FILES     = fault_codes
GEN_PERL_MODULES        = Components.pm Errors.pm Properties.pm RegFGC3_parameters.pm Runlog_entries.pm Class/*/Auto.pm Platform/*/Auto.pm Class/*/API.pm
GEN_PYTHON_MODULES      = properties.py converters.py parameters.py
GEN_PYTHON_DEC_MODULES  = classes/fgc_*.py

EPICS_UDP_DB_FILES  = fgcudp_class_*.substitutions Makefile
EPICS_UDP_SRC_FILES = classes/* common/inc/class_selector.h common/inc/pub_data.h Makefile
EPICS_CMD_DB_FILES  = fgccmd_class_*.substitutions Makefile

TANGO_DEFS_FILES    = class_*_prop_info.json


GEN_FILES  = $(wildcard ../website/gendoc)
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_HEADER_ROOT)/classes/*/%, $(CLASS_GEN_FILES))) $(wildcard src/classes/*/evtlog_symlists.csv)
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_HEADER_ROOT)/common/%, $(COMMON_GEN_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_HEADER_ROOT)/platforms/*/%, $(PLATFORMS_GEN_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_HEADER_ROOT)/platforms/*/boot/%, $(PLATFORMS_GEN_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(UTILITIES_CTRL_DIR)/%, $(UTILITIES_GEN_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_PERL_ROOT)/%, $(GEN_PERL_MODULES)))
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_PYTHON_ROOT)/%, $(GEN_PYTHON_MODULES)))
GEN_FILES += $(wildcard $(patsubst %, $(PARSE_PYTHON_DEC_ROOT)/%, $(GEN_PYTHON_DEC_MODULES)))
GEN_FILES += $(REG_B_YAML)
GEN_FILES += $(EPIC_DEVICE_YAMLS)
GEN_FILES += $(wildcard $(patsubst %, $(EPICS_UDP_DB_DIR)/%, $(EPICS_UDP_DB_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(EPICS_UDP_SRC_DIR)/%, $(EPICS_UDP_SRC_FILES)))
GEN_FILES += $(wildcard $(patsubst %, $(EPICS_CMD_DB_DIR)/%, $(EPICS_CMD_DB_FILES)))
GEN_FILES += $(build_home)/sw/fgcd/inc/sub_cmwdata_gen.h $(build_home)/sw/fgcd/src/sub_cmwdata_gen.cpp
GEN_FILES += $(wildcard $(patsubst %, $(TANGO_DEFS_DIR)/%, $(TANGO_DEFS_FILES)))
GEN_FILES += $(CLASS_65_EPIC_DEVICES)


# exclude hand edited files from the list
GEN_FILES := $(filter-out $(wildcard $(PARSE_HEADER_ROOT)/classes/51/pmx_51.h), $(GEN_FILES))
GEN_FILES := $(filter-out $(wildcard $(PARSE_PERL_ROOT)/Class/61/*), $(GEN_FILES))
GEN_FILES := $(filter-out $(wildcard $(PARSE_PYTHON_DEC_ROOT)/classes/fgc_common.py), $(GEN_FILES))

yaml_files     := $(shell find src -name '*.yaml' | sort) $(CLASS_65_EPIC_DEVICES)
perl_files     := $(shell find . -name '*.pl' -o -name '*.pm')
python_files   := $(shell find . -name '*.py' -o -name '*.j2') $(shell find $(fgc_parser_schemas) -name '*.py')
xml_files      := $(shell find src -name '*.xml' | sort)


# generation targets
GEN_TARGETS := cclibs config cpp docs epics fgccode perl python tango vhdl
GEN_TARGETS_FILES := $(addprefix .target.,$(GEN_TARGETS))

all: .cclibs_headers .translate .superdict.pickle .parser

test:

# Include cclibs variables and targets
include Makefile.cclibs

venv: venv/touchfile

venv/touchfile: pyparser/pyparser/requirements.txt
ifeq ("$(wildcard ./venv/.)","")
ifneq ("$(wildcard /acc/local/share/python/acc-py/pro/setup.sh)","")
	. /acc/local/share/python/acc-py/pro/setup.sh && python -m venv venv
else
	python3 -m venv venv
endif
endif
	. venv/bin/activate; pip install -r pyparser/pyparser/requirements.txt
	touch venv/touchfile

.translate: venv $(xml_files) $(python_files)
	. ./venv/bin/activate; PYTHONPATH=$(PYTHONPATH):$(pyparser):$(fgc_parser_schemas) python -m pyparser --translate ComponentLabels --translate Components --translate DimNames --translate DimTypes --translate DimsInc --translate Systems --translate SystemsInc --translate RegFGC3 --skip-parsing --skip-generation --skip-tests
	touch .translate

.superdict.pickle: venv .translate $(yaml_files) $(log_csv_files) $(log_consts_yaml_files) $(REG_B_YAML) $(EPIC_DEVICE_YAMLS) $(perl_files) $(python_files)
	. ./venv/bin/activate; PYTHONPATH=$(PYTHONPATH):$(pyparser):$(fgc_parser_schemas) python -m pyparser --dump-schema-to-pickle .superdict.pickle --skip-generation

.parser: venv .superdict.pickle
	. ./venv/bin/activate; PYTHONPATH=$(PYTHONPATH):$(pyparser):$(fgc_parser_schemas) python -m pyparser --load-schema-from-pickle .superdict.pickle
	@echo "Check Config Properties (CSV vs. XML)..."
	PERL5LIB=$(perl_lib_path) perl ${UTILITIES_FGC_DIR}/check_config.pl

	touch .parser

.target.% : .superdict.pickle
	. ./venv/bin/activate; PYTHONPATH=$(PYTHONPATH):$(pyparser):$(fgc_parser_schemas) python -m pyparser --load-schema-from-pickle .superdict.pickle --generate $(subst .target.,,$@)
	touch $@

$(GEN_TARGETS): %: .target.%

round_trip_parser: venv .translate
	. ./venv/bin/activate; PYTHONPATH=$(PYTHONPATH):$(pyparser):$(fgc_parser_schemas) python -m pyparser --yaml-parser=round-trip

# Updates the components xmls with info from the controls database
update_xml:
	@# read db username and pw from poccdev's dbconfig file
	$(eval DB_CONFIG := $(shell ssh poccdev@cs-ccr-teepc2 'cat ~/accsoft/dbconfig.txt'))
	$(eval DB_USER := $(shell echo "$(DB_CONFIG)" | cut -d ',' -f1))
	$(eval DB_PW := $(shell echo "$(DB_CONFIG)" | cut -d ',' -f2))

	# sync CCDB and our XMLs with data from AlimDB
	@PERL5LIB=$(perl_lib_path) ${UTILITIES_FGC_DIR}/sync_alimdb_ccdb.pl --db_user=$(DB_USER) --db_pw=$(DB_PW)
	@PERL5LIB=$(perl_lib_path) ${UTILITIES_FGC_DIR}/update_component_labels_xml.pl src/component_labels.xml
	@PERL5LIB=$(perl_lib_path) ${UTILITIES_FGC_DIR}/update_components_xml.pl src/components.xml

# Commits the components xmls if they have been updated
commit_xml:
	git remote prune origin         # Make sure that local references to eventually deleted remote branches are cleaned up
	git fetch origin master         # Get remote commits
	git diff --staged --quiet       # Check that there are no uncommited staged changes

	# Auto-commit changes to git repository if there have been any changes in the xml files
	git add src/components.xml src/component_labels.xml

	git diff --staged --quiet || ( \
		echo "xml files changed, updating repo"; \
		git commit -m "Automatic update of components.xml, component_labels.xml"; \
		git push;\
		);

# properties/inc/reg/reg_b.yaml is generated by replacing some terms in properties/inc/reg/reg_i.yaml
$(REG_B_YAML): $(PROPS_INC_PATH)/reg/reg_i.yaml $(PROPS_INC_PATH)/reg/gen_reg_b.sh
	$(PROPS_INC_PATH)/reg/gen_reg_b.sh

# properties/inc/epic/device_*.yaml is generated fromn properties/inc/epic/device_in.yaml
$(EPIC_DEVICE_YAMLS): $(PROPS_INC_PATH)/epic/device_in.yaml $(PROPS_INC_PATH)/epic/gen_epic.bash
	$(PROPS_INC_PATH)/epic/gen_epic.bash

# property list epic/devices.yaml is generated from epic/device.in.yaml
$(CLASS_65_EPIC_DEVICES): src/classes/65/epic/device.in.yaml src/classes/65/epic/gen_devices.pl
	PERL5LIB=$(perl_lib_path) perl src/classes/65/epic/gen_devices.pl $< $@

# Here we delete all the auto-generated headers and Perl files using the patterns in .gitignore.
# There is one exception: Class/52/Auto.pm is not automatically generated but matches a .gitignore
# pattern, so we specifically exclude it from the list of files to be deleted
clean:
	$(RM) .parser
	$(RM) .translate
	$(RM) .superdict.pickle
	$(RM) .target.*
	@echo "Deleting auto generated modules and header files..."
	$(RM) -r $(GEN_FILES)

.PHONY: all test update_xml commit_xml clean round_trip_parser $(GEN_TARGETS)

# EOF
