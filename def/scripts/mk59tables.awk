BEGIN {

    for(i=0;i<16;i++)
    {
        printf "\n        <property"
        printf "\n            symbol  = \"CHAN%i\"",i
        printf "\n            type    = \"PARENT\""
        printf "\n            title   = \"Channel %d Table properties\"",i
        printf "\n            get     = \"Parent\""
        printf "\n            flags   = \"HIDE\""
        printf "\n        >"
        printf "\n            <doc><![CDATA["
        printf "\n                This property contains the table function property for channel %i.",i
        printf "\n            ]]></doc>"
        print
        printf "\n            <property"
        printf "\n                symbol      = \"FUNCTION\""
        printf "\n                title       = \"Array of points (time|ref) for Channel %i table function\"",i
        printf "\n                type        = \"POINT\""
        printf "\n                flags       = \"INDIRECT_N_ELS\""
        printf "\n                dsp_fgc2    = \"fgc\""
        printf "\n                get         = \"Point\""
        printf "\n                set         = \"Point\""
        printf "\n                setif       = \"FgTableOk\""
        printf "\n                maxels      = \"FGC_FG_TABLE_LEN\""
        printf "\n                numels      = \"(uintptr_t) &amp; property.fgfunc.table.function_n_els[%i]\"",i
        printf "\n                value_fgc2  = \"TABLE_FUNCTION(%i)\"",i
        printf "\n            >"
        printf "\n                <doc><![CDATA["
        printf "\n                    This property contains the time vector for the table for channel %i.",i
        printf "\n                ]]></doc>"
        printf "\n            </property>"
        print
        printf "\n            <property"
        printf "\n                symbol      = \"T\""
        printf "\n                title       = \"Channel %i table function time vector (s)\"",i
        printf "\n                type        = \"FLOAT\""
        printf "\n                flags       = \"INDIRECT_N_ELS\""
        printf "\n                dsp_fgc2    = \"fgc\""
        printf "\n                get         = \"RelTime\""
        printf "\n                set         = \"RelTime\""
        printf "\n                setif       = \"FgTableOk\""
        printf "\n                limits      = \"float\""
        printf "\n                min         = \"0.0\""
        printf "\n                max         = \"4.0E+5\""
        printf "\n                maxels      = \"FGC_FG_TABLE_LEN\""
        printf "\n                numels      = \"(uintptr_t) &amp; property.fgfunc.table.t_n_els[%i]\"",i
        printf "\n                value_fgc2  = \"TABLE_TIME(%i)\"",i
        printf "\n            >"
        printf "\n                <doc><![CDATA["
        printf "\n                    This property contains the time vector for the table for channel %i.",i
        printf "\n                ]]></doc>"
        printf "\n            </property>"
        print
        printf "\n            <property"
        printf "\n                symbol      = \"R\""
        printf "\n                title       = \"Channel %i table function value vector\"",i
        printf "\n                flags       = \"INDIRECT_N_ELS\""
        printf "\n                dsp_fgc2    = \"fgc\""
        printf "\n                type        = \"FLOAT\""
        printf "\n                set         = \"Float\""
        printf "\n                setif       = \"FgTableOk\""
        printf "\n                get         = \"Float\""
        printf "\n                maxels      = \"FGC_FG_TABLE_LEN\""
        printf "\n                numels      = \"(uintptr_t) &amp; property.fgfunc.table.r_n_els[%i]\"",i
        printf "\n                value_fgc2  = \"TABLE_VALUE(%i)\"",i
        printf "\n            >"
        printf "\n                <doc><![CDATA["
        printf "\n                    This property contains the absolute function values for the table for channel %i.",i
        printf "\n                ]]></doc>"
        printf "\n            </property>"
        printf "\n        </property>\n"
    }
}
