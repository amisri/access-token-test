#! /usr/local/bin/awk -f

# This little helper script will prepare the C code for the function spyClassGetSignal()
# in spy_class.c based on the spy.xml class symbol list.
#
# For example, for Class 63:
#
# $ ./spytemplate.awk ../src/classes/63/symlists/spy.xml
#
# will generate content on stdout that can be pasted into spyClassGetSignal().
#
# The value returned will not always be correct and will have to be edited in some cases.

BEGIN {
    value = 0
}
// {
    if($1 == "<!--" && $NF == "-->")
    {
        $1 = ""
        $NF = ""
        $(NF-1) = ""

        printf "\n        // %s\n\n", $0
    }
    else if($1 == "name")
    {
        gsub("\"","",$3)

        if($3 in names)
        {
            print "Repeated name at line " NR " : " $3
            exit -1
        }

        names[$3] = 1

        name = $3

        source = "regulationGetVarValue(" $3 ")"
    }
    else if($1 == "source")
    {
        split($0, a, "=")
        split(a[2], b, "\"")

        source = b[2]

        delete a
        delete b

        sub("&amp;","\\&",source)
    }
    else if($1 == "/>" || $1 == "</const>")
    {
        printf "        case FGC_SPY_%-23sreturn (float)(%s);\n", name ":", source
    }
}

# EOF
