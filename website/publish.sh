#! /bin/bash

echo Installing to proj-fgc site under pclhc account

rsync -avzhe ssh --delete --exclude '.DS_Store' gendoc index.html default.htm style.css doc images static pclhc@cs-ccr-teepc2.cern.ch:/user/pclhc/public_html/proj-fgc

echo Installing to KT-FGC cernbox

rsync -avh --delete --exclude '.DS_Store' gendoc default.htm style.css doc images static ~/cernbox/Knowledge_Transfer/KT-FGC/FGC_Website/fgc

# EOF
