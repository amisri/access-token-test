#!/bin/bash

#*****************************************************************
if ! ./build_1_binutils.sh
   then
       echo "Error in step 1"
       exit 1
fi

if ! ./build_2_gmp.sh
   then
       echo "Error in step 2"
       exit 1
fi

if ! ./build_3_mpfr.sh
   then
       echo "Error in step 3"
       exit 1
fi

if ! ./build_4_mpc.sh
   then
       echo "Error in step 4"
       exit 1
fi

if ! ./build_5_gcc.sh
   then
       echo "Error in step 5"
       exit 1
fi

if ! ./build_6_newlib.sh
   then
       echo "Error in step 6"
       exit 1
fi

if ! ./build_7_gcc_final.sh
   then
       echo "Error in step 7"
       exit 1
fi
#*****************************************************************

