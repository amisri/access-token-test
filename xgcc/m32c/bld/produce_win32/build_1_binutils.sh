#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM != --host=$WANTED_COMPILER_PLATFORM
# and we use an extra compiler build by ourself
# we don't use the native gcc in this machine
#************************************************************************
if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_DIR ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_DIR
fi

cd $WANTED_COMPILER_PLATFORM_BUILD_DIR

if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_BINUTILS ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_BINUTILS
fi
#************************************************************************
# to access our extra compiler
export ORIG_PATH=$PATH
export PATH=$PATH:$EXTRA_CROSS_COMPILER_PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#************************************************************************
echo "Build Binutils? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
	echo "Building Binutils..."

	cd $WANTED_COMPILER_PLATFORM_BUILD_BINUTILS

#       make clean
        make distclean

# to access our extra compiler
#--with-as=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#--with-ld=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
# the following prevents native assembler on certain architectures. (for others, these do not have any effects)
#--with-gnu-as 
#--with-gnu-ld 

#--with-build-time-tools=dir
# When you use this option, you should ensure that dir includes ar, as, ld, nm, ranlib and strip if necessary, and possibly objdump.

	echo "Configuring..."
	if ! $SRC_DIR_BINUTILS/configure --build=$THIS_PLATFORM --host=$WANTED_COMPILER_PLATFORM --target=$WANTED_COMPILER_CODE --prefix=$WANTED_COMPILER_PLATFORM_PREFIX --disable-werror > configure.out 2>&1
	   then
	       echo "Error in binutils configure..."
	       exit 1
	fi

	echo "Make..."
	if ! make > make.out 2>&1
	   then
	       echo "Error in binutils make..."
	       exit 1
	fi

	echo "Make install..."
	if ! make install > makeinstall.out 2>&1
	   then
	       echo "Error in binutils make install..."
	       exit 1
	fi
	cd ..
fi

export PATH=$ORIG_PATH
echo
echo "Finished building binutils"
#************************************************************************
