#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM != --host=$WANTED_COMPILER_PLATFORM
# and we use an extra compiler build by ourself
# we don't use the native gcc in this machine
#************************************************************************
if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_DIR ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_DIR
fi

cd $WANTED_COMPILER_PLATFORM_BUILD_DIR

if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_MPFR ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_MPFR
fi
#************************************************************************
# to access our extra compiler
export ORIG_PATH=$PATH
export PATH=$PATH:$EXTRA_CROSS_COMPILER_PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#************************************************************************
echo "Build MPFR ? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
       echo "Building MPFR..."

       cd $WANTED_COMPILER_PLATFORM_BUILD_MPFR

#      make clean
       make distclean

# to access our extra compiler
#--with-as=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#--with-ld=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
# the following prevents native assembler on certain architectures. (for others, these do not have any effects)
#--with-gnu-as 
#--with-gnu-ld 

#--with-build-time-tools=dir
# When you use this option, you should ensure that dir includes ar, as, ld, nm, ranlib and strip if necessary, and possibly objdump.

       echo "Configuring..."
       if ! $SRC_DIR_MPFR/configure --disable-shared --build=$THIS_PLATFORM --host=$WANTED_COMPILER_PLATFORM --with-gmp=$WANTED_COMPILER_PLATFORM_PREFIX_GMP --prefix=$WANTED_COMPILER_PLATFORM_PREFIX_MPFR > configure.out 2>&1
          then
              echo "Error in mpfr configure..."
              exit 1
       fi

       echo "Make..."
       if ! make > make.out 2>&1
          then
              echo "Error in mpfr make..."
              exit 1
       fi

#************************************************************************
# can't check this as it is Windows EXE and we are running under Linux
#     echo "Make check..."
#     if ! make check > makecheck.out 2>&1
#        then
#            echo "Error in mpfr make check..."
#            exit 1
#     fi
#************************************************************************

       echo "Make install..."
       if ! make install > makeinstall.out 2>&1
          then
              echo "Error in mpfr make install..."
              exit 1
       fi
       cd ..
fi
export PATH=$ORIG_PATH
echo
#************************************************************************
