#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM != --host=$WANTED_COMPILER_PLATFORM
# and we use an extra compiler build by ourself
# we don't use the native gcc in this machine
#************************************************************************
if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_DIR ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_DIR
fi

cd $WANTED_COMPILER_PLATFORM_BUILD_DIR

if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_NEWLIB ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_NEWLIB
fi
#************************************************************************
# to access our extra compiler
# also
# it needs the already build compiler to build the library
export ORIG_PATH=$PATH
# it also needs the wanted compiler rx-elf-gcc of the platform we are using for building right now ("native")
# this is used to compile the libraries
export PATH=$PATH:$EXTRA_CROSS_COMPILER_PATH:$EXTRA_CROSS_COMPILER_PATH/bin:$EXTRA_NATIVE_CROSS_COMPILER_PATH/bin
#************************************************************************
echo "Build Newlib? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
	echo "Building Newlib..."

	cd $WANTED_COMPILER_PLATFORM_BUILD_NEWLIB

#       make clean
        make distclean

#--enable-target-optspace
#--enable-newlib-hw-fp

# we need to modify  /src/newlib/newlib/configure.host to add in 
# the last [case "${host}" in]
# the case

#  rx-*-*)
#  default_newlib_io_long_long="yes"
#  newlib_cflags="${newlib_cflags} -Os -DREENTRANT_SYSCALLS_PROVIDED -DINTERNAL_NEWLIB -DDEFINE_MALLOC -DDEFINE_FREE -DDEFINE_REALLOC -DDEFINE_CALLOC"
#  syscall_dir=
#  ;;

# to access our extra compiler
#--with-as=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#--with-ld=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
# the following prevents native assembler on certain architectures. (for others, these do not have any effects)
#--with-gnu-as 
#--with-gnu-ld 

#--with-build-time-tools=dir
# When you use this option, you should ensure that dir includes ar, as, ld, nm, ranlib and strip if necessary, and possibly objdump.

	echo "Configuring..."
	if ! $SRC_DIR_NEWLIB/configure --disable-libquadmath --disable-libada --disable-libssp --build=$THIS_PLATFORM --host=$WANTED_COMPILER_PLATFORM --target=$WANTED_COMPILER_CODE --prefix=$WANTED_COMPILER_PLATFORM_PREFIX > configure.out 2>&1
	   then
	       echo "Error in newlib configure..."
	       exit 1
	fi

	echo "Make..."
	if ! make > make.out 2>&1
	   then
	        echo "Error in newlib make..."
	        exit 1
	fi

	echo "Make install..."
	if ! make install > makeinstall.out 2>&1
	   then
	       echo "Error in newlib make install..."
	       exit 1
	fi
	cd ..
fi

export PATH=$ORIG_PATH
echo
echo "Finished building newlib"
#************************************************************************
