#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM == --host=$WANTED_COMPILER_PLATFORM
# and we are not using an extra compiler, we use the native gcc in this machine
#************************************************************************
# it needs the already build compiler
export ORIG_PATH=$PATH
export PATH=$PATH:$THIS_PLATFORM_PREFIX_GCC/bin

#************************************************************************
cd $THIS_PLATFORM_BUILD_DIR

echo "Build final gcc? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
       echo "Building final Gcc..."

       cd $THIS_PLATFORM_BUILD_GCC
       echo "Make all-target..."
       if ! make all-target > makealltarget.out 2>&1
          then
              echo "Error in gcc make..."
              exit 1
       fi

       echo "Make install..."
       if ! make install > makeinstalllast.out 2>&1
          then
              echo "Error in gcc make install..."
              exit 1
       fi
fi

export PATH=$ORIG_PATH
echo
echo "Finished building Gcc final"
#************************************************************************
