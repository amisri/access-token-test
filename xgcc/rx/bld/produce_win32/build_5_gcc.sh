#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM != --host=$WANTED_COMPILER_PLATFORM
# and we use an extra compiler build by ourself
# we don't use the native gcc in this machine
#************************************************************************
if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_DIR ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_DIR
fi

cd $WANTED_COMPILER_PLATFORM_BUILD_DIR

if [ ! -d $WANTED_COMPILER_PLATFORM_BUILD_GCC ]
   then
        mkdir $WANTED_COMPILER_PLATFORM_BUILD_GCC
fi
#************************************************************************
# to access our extra compiler
export ORIG_PATH=$PATH

# it also needs the wanted compiler rx-elf-gcc of the platform we are using for building right now ("native")
# this is used to compile the libraries
export PATH=$PATH:$EXTRA_CROSS_COMPILER_PATH:$EXTRA_CROSS_COMPILER_PATH/bin:$EXTRA_NATIVE_CROSS_COMPILER_PATH/bin
#************************************************************************
echo "Build gcc? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
	echo "Building Gcc..."

	cd $WANTED_COMPILER_PLATFORM_BUILD_GCC

#       make clean
        make distclean

# --disable-threads 
# --disable-libmudflap 
# --disable-libssp 
# --disable-libgomp 
# --disable-libquadmath 
# --disable-target-libiberty 
# --disable-target-zlib
# --without-headers

# to access our extra compiler
#--with-as=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
#--with-ld=$PATH:$EXTRA_CROSS_COMPILER_PATH/bin
# the following prevents native assembler on certain architectures. (for others, these do not have any effects)
#--with-gnu-as 
#--with-gnu-ld 

#--with-build-time-tools=dir
# When you use this option, you should ensure that dir includes ar, as, ld, nm, ranlib and strip if necessary, and possibly objdump.

	echo "Configuring..."
        if ! $SRC_DIR_GCC/configure -v --disable-shared --with-newlib=yes --enable-lto --enable-gold --disable-libstdcxx-pch --build=$THIS_PLATFORM --host=$WANTED_COMPILER_PLATFORM --enable-languages=$COMPI_LANGUAGES --target=$WANTED_COMPILER_CODE --with-gmp=$WANTED_COMPILER_PLATFORM_PREFIX_GMP --with-mpfr=$WANTED_COMPILER_PLATFORM_PREFIX_MPFR  --with-mpc=$WANTED_COMPILER_PLATFORM_PREFIX_MPC --prefix=$WANTED_COMPILER_PLATFORM_PREFIX > configure.out 2>&1
	   then
	       echo "Error in gcc configure..."
	       exit 1
	fi

       echo "Make all-host..."
       if ! make all-host > makeallhosts.out 2>&1
	   then
              echo "Error in gcc make all-host..."
	       exit 1
	fi

       echo "Make install-host..."
       if ! make install-host > makeinstalhosts.out 2>&1
	   then
              echo "Error in gcc make install-host..."
	       exit 1
	fi
	cd ..
fi

export PATH=$ORIG_PATH
echo
echo "Finished building Gcc"
#************************************************************************
