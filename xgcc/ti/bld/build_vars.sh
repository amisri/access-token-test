#!/bin/bash

#*****************************************************************
# --build   The machine you are building on               i386 
# --host    The machine you are building for              i686  
# --target  The machine that gcc will produce code for    rx-elf m32c-elf
#
# when --build, --host, --target are all the same this is called a "native"
# when --build, --host, --target are all the different this is called a "canadian"
# when --build, --host are the same but --target is different this is called a "cross"
# when --host, --target are the same but --build is different you are using a cross-compiler to build a native for a different system. Some people call this a host-x-host, crossed native, or cross-built native
# when --build, --target are the same but --host is different you are using a cross compiler to build a cross compiler that produces code for the machine you're building on
#
# target libraries
# when --build, --host are the same but --target is different the GCC you are building will also be used to build the target libraries (like libstdc++)
# when --build and --host are different, you must have already built and installed a cross compiler that will be used to build the target libraries (if you configured with --target=foo-bar, this compiler will be called foo-bar-gcc).
# In the case of target libraries, the machine you're building for is the machine you specified with --target. So, build is the machine you're building on (no change there), host is the machine you're building for (the target libraries are built for the target, so host is the target you specified), and target doesn't apply (because you're not building a compiler, you're building libraries). The configure/make process will adjust these variables as needed. It also sets $with_cross_host to the original --host value in case you need it. 
#*****************************************************************
TIME_READ_PATH=/home/dcalcoen/ti/bld

# --build, the building ON machine
THIS_PLATFORM=i686-pc-linux-gnu

# --host, the building FOR machine
WANTED_COMPILER_PLATFORM=THIS_PLATFORM

# --target, the machine that gcc will produce code for    
WANTED_COMPILER_CODE=c6x-elf
#*****************************************************************
COMPI_LANGUAGES=c,c++
MAKE_LANGUAGES="c c++"
#*****************************************************************
SRC_DIR_NEWLIB=/home/dcalcoen/gitMirror/newlib
SRC_DIR_BINUTILS=/home/dcalcoen/gitMirror/binutils
SRC_DIR_GCC=/home/dcalcoen/gitMirror/gcc
SRC_DIR_GMP=/home/dcalcoen/src/gmp-5.1.2
SRC_DIR_MPFR=/home/dcalcoen/src/mpfr-3.1.2
SRC_DIR_MPC=/home/dcalcoen/src/mpc-1.0.1
#*****************************************************************
THIS_PLATFORM_PREFIX=/home/dcalcoen/ti/pre/linux
THIS_PLATFORM_PREFIX_BINUTILS=/home/dcalcoen/ti/pre/linux
THIS_PLATFORM_PREFIX_GCC=/home/dcalcoen/ti/pre/linux
THIS_PLATFORM_PREFIX_NEWLIB=/home/dcalcoen/ti/pre/linux

THIS_PLATFORM_PREFIX_GMP=/home/dcalcoen/ti/pre/linux/gmp
THIS_PLATFORM_PREFIX_MPFR=/home/dcalcoen/ti/pre/linux/mpfr
THIS_PLATFORM_PREFIX_MPC=/home/dcalcoen/ti/pre/linux/mpc

THIS_PLATFORM_BUILD_DIR=/home/dcalcoen/ti/bld/linux
THIS_PLATFORM_BUILD_BINUTILS=binutils
THIS_PLATFORM_BUILD_GCC=gcc
THIS_PLATFORM_BUILD_NEWLIB=newlib
THIS_PLATFORM_BUILD_GMP=gmp
THIS_PLATFORM_BUILD_MPFR=mpfr
THIS_PLATFORM_BUILD_MPC=mpc

#*****************************************************************
export TIMELIMIT=6
export TIME_READ_PATH

export THIS_PLATFORM
export WANTED_COMPILER_CODE

export SRC_DIR_BINUTILS
export SRC_DIR_GCC
export SRC_DIR_NEWLIB
export SRC_DIR_GMP
export SRC_DIR_MPFR
export SRC_DIR_MPC

export THIS_PLATFORM_PREFIX
export THIS_PLATFORM_PREFIX_BINUTILS
export THIS_PLATFORM_PREFIX_GCC
export THIS_PLATFORM_PREFIX_NEWLIB
export THIS_PLATFORM_PREFIX_GMP
export THIS_PLATFORM_PREFIX_MPFR
export THIS_PLATFORM_PREFIX_MPC
export THIS_PLATFORM_BUILD_DIR
export THIS_PLATFORM_BUILD_BINUTILS
export THIS_PLATFORM_BUILD_GCC
export THIS_PLATFORM_BUILD_NEWLIB
export THIS_PLATFORM_BUILD_GMP
export THIS_PLATFORM_BUILD_MPFR
export THIS_PLATFORM_BUILD_MPC

#*****************************************************************