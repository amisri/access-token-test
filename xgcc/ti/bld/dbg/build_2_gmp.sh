#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM == --host=$WANTED_COMPILER_PLATFORM
# and we are not using an extra compiler, we use the native gcc in this machine
#************************************************************************
if [ ! -d $THIS_PLATFORM_BUILD_DIR ]
   then
        mkdir $THIS_PLATFORM_BUILD_DIR
fi

cd $THIS_PLATFORM_BUILD_DIR

if [ ! -d $THIS_PLATFORM_BUILD_GMP ]
   then
        mkdir $THIS_PLATFORM_BUILD_GMP
fi
#************************************************************************
echo "Build gmp? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
       echo "Building GMP..."

       cd $THIS_PLATFORM_BUILD_GMP

#      make clean
       make distclean

       echo "Configuring..."
       if ! $SRC_DIR_GMP/configure --disable-shared --prefix=$THIS_PLATFORM_PREFIX_GMP > configure.out 2>&1
          then
              echo "Error in gmp configure..."
              exit 1
       fi

       echo "Make..."
       if ! make > make.out 2>&1
          then
              echo "Error in gmp make..."
              exit 1
       fi

       echo "Make check..."
       if ! make check > makecheck.out 2>&1
          then
              echo "Error in gmp make check..."
              exit 1
       fi

       echo "Make install..."
       if ! make install > makeinstall.out 2>&1
          then
              echo "Error in gmp make install..."
              exit 1
       fi
       cd ..
fi
#************************************************************************
