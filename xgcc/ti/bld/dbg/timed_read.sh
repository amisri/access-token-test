#!/bin/bash
#TIMELIMIT = 10 # Three seconds in this instance, may be set to different value.
echo $data

PrintAnswer()
{
    if [ "$answer" = TIMEOUT ]
	then
	    retdata=y
	else # Don't want to mix up the two instances.
	    kill -9 $! # Kills no longer needed TimerOn function running in background.
	    # $! is PID of last job running in background.
    fi
}

TimerOn()
{
    sleep $TIMELIMIT && kill -s 14 $$ &
    # Waits 3 seconds, then sends sigalarm to script.
}

Int14Vector()
{
    answer="TIMEOUT"
    PrintAnswer
    exit 14
}

trap Int14Vector 14 # Timer interrupt (14) subverted for our purposes.
TimerOn
read answer
PrintAnswer
retdata=$answer

export retdata

if [ "$answer" = n ] || [ "answer" = N ]
    then
	exit 0
    else
	exit 1
fi

