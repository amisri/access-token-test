#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM == --host=$WANTED_COMPILER_PLATFORM
# and we are not using an extra compiler, we use the native gcc in this machine
#************************************************************************
if [ ! -d $THIS_PLATFORM_BUILD_DIR ]
   then
        mkdir $THIS_PLATFORM_BUILD_DIR
fi

cd $THIS_PLATFORM_BUILD_DIR

if [ ! -d $THIS_PLATFORM_BUILD_NEWLIB ]
   then
        mkdir $THIS_PLATFORM_BUILD_NEWLIB
fi
#************************************************************************

# it needs the already build compiler to build the library
export ORIG_PATH=$PATH
export PATH=$PATH:$THIS_PLATFORM_PREFIX_GCC/bin
#************************************************************************
echo "Build Newlib? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
       echo "Building Newlib..."

       cd $THIS_PLATFORM_BUILD_NEWLIB

#      make clean
       make distclean

#--enable-target-optspace
#--enable-newlib-hw-fp

# we need to modify  /src/newlib/newlib/configure.host to add in 
# the last [case "${host}" in]
# the case

#  rx-*-*)
#  default_newlib_io_long_long="yes"
#  newlib_cflags="${newlib_cflags} -Os -DREENTRANT_SYSCALLS_PROVIDED -DINTERNAL_NEWLIB -DDEFINE_MALLOC -DDEFINE_FREE -DDEFINE_REALLOC -DDEFINE_CALLOC"
#  syscall_dir=
#  ;;

       echo "Configuring..."
       if ! $SRC_DIR_NEWLIB/configure --disable-libquadmath --disable-libada --disable-libssp --target=$WANTED_COMPILER_CODE --prefix=$THIS_PLATFORM_PREFIX_NEWLIB > configure.out 2>&1
          then
              echo "Error in newlib configure..."
              exit 1
       fi

       echo "Make..."
       if ! make > make.out 2>&1
          then
              echo "Error in newlib make..."
              exit 1
       fi

       echo "Make install..."
       if ! make install > makeinstall.out 2>&1
          then
              echo "Error in newlib make install..."
              exit 1
       fi
       cd ..
fi

export PATH=$ORIG_PATH
echo
echo "Finished building newlib"
#************************************************************************
