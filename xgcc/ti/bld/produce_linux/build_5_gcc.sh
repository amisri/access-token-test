#!/bin/bash

source build_vars.sh

#export CFLAGS="-O0 -v -Xlinker -M"
#************************************************************************
# for this case 
# --build=$THIS_PLATFORM == --host=$WANTED_COMPILER_PLATFORM
# and we are not using an extra compiler, we use the native gcc in this machine
#************************************************************************
if [ ! -d $THIS_PLATFORM_BUILD_DIR ]
   then
        mkdir $THIS_PLATFORM_BUILD_DIR
fi

cd $THIS_PLATFORM_BUILD_DIR

if [ ! -d $THIS_PLATFORM_BUILD_GCC ]
   then
        mkdir $THIS_PLATFORM_BUILD_GCC
fi
#************************************************************************
export ORIG_PATH=$PATH

#it also needs the wanted compiler c6x-elf-gcc
export PATH=$PATH:$THIS_PLATFORM_PREFIX_BINUTILS/bin
#************************************************************************
echo "Build gcc? (y/n)"
$TIME_READ_PATH/timed_read.sh
retdata=$?
if [ $retdata = 14 ] || [ $retdata = 1 ]
   then
       echo "Building Gcc..."

       cd $THIS_PLATFORM_BUILD_GCC

#      make clean
       make distclean

# --disable-threads 
# --disable-libmudflap 
# --disable-libssp 
# --disable-libgomp 
# --disable-libquadmath 
# --disable-target-libiberty 
# --disable-target-zlib
# --without-headers

       echo "Configuring..."
       if ! $SRC_DIR_GCC/configure -v --disable-shared --with-newlib=yes --enable-lto --enable-gold --disable-libstdcxx-pch --enable-languages=$COMPI_LANGUAGES --target=$WANTED_COMPILER_CODE --with-gmp=$THIS_PLATFORM_PREFIX_GMP --with-mpfr=$THIS_PLATFORM_PREFIX_MPFR --with-mpc=$THIS_PLATFORM_PREFIX_MPC --prefix=$THIS_PLATFORM_PREFIX_GCC > configure.out 2>&1
          then
              echo "Error in gcc configure..."
              exit 1
       fi

       echo "Make all-host..."
       if ! make all-host > makeallhosts.out 2>&1
          then
              echo "Error in gcc make all-host..."
              exit 1
       fi

       echo "Make install-host..."
       if ! make install-host > makeinstalhosts.out 2>&1
          then
              echo "Error in gcc make install-host..."
              exit 1
       fi
       cd ..
fi

export PATH=$ORIG_PATH
echo
echo "Finished building Gcc"
#************************************************************************
